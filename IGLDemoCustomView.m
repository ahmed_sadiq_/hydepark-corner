//
//  IGLDemoCustomView.m
//  IGLDropDownMenuDemo
//
//  Created by Galvin Li on 2016-02-08.
//  Copyright © 2016 Galvin Li. All rights reserved.
//

#import "IGLDemoCustomView.h"

@interface IGLDemoCustomView ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation IGLDemoCustomView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [self initView];
}

- (void)initView
{
    self.layer.borderColor = [UIColor colorWithRed:62/255 green:75/255 blue:87/255 alpha:0.7].CGColor;
    self.layer.borderWidth = 1.2;
    self.layer.masksToBounds = NO;
    self.backgroundColor = [UIColor clearColor];
    self.alpha = 1;
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeLeft;
    [self addSubview:imageView];
    self.imageView = imageView;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.imageView.image = image;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.imageView.frame = self.bounds;
    self.layer.cornerRadius = frame.size.height / 2.0;
}

@end
