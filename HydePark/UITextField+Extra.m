//
//  UITextField+Extra.m
//  Wits
//
//  Created by Osama on 08/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "UITextField+Extra.h"
#import "DataContainer.h"
@implementation UITextField (Extra)
- (void)naturalTextAligment {
    DataContainer *sharedManager = [DataContainer sharedManager];
    if (sharedManager.languageCode == 1) {
        self.textAlignment = NSTextAlignmentLeft;
    } else {
        self.textAlignment = NSTextAlignmentRight;
    }
}
@end
