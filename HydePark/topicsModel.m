//
//  topicsModel.m
//  HydePark
//
//  Created by Mr on 26/06/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "topicsModel.h"
#import "VideoModel.h"
@implementation topicsModel
@synthesize beams_count,topic_id,topic_image,topic_name,topics_array,images_array,names_array,beams_array;
- (id)initWithDictionary:(NSDictionary *) responseData
{
    self = [super init];
    if (self) {
        self.beams_array = [[NSMutableArray alloc] init];
        self.beams_count = [responseData objectForKey:@"beams_count"];
        self.topic_id = [responseData objectForKey:@"id"];
        self.topic_image = [responseData objectForKey:@"image"];
        self.topic_name = [responseData objectForKey:@"name"];
        NSArray *postsArray = [NSArray new];
        if ([responseData objectForKey:@"posts"]) {
            postsArray = [responseData objectForKey:@"posts"];
            
            for(int i=0; i < postsArray.count ; i++){
                NSDictionary *postDict = (NSDictionary *)[postsArray objectAtIndex:i];
                VideoModel *tObj = [[VideoModel alloc] initWithDictionary:postDict];
                [self.beams_array addObject:tObj];
            }
        }
        
    }
    return self;
}
@end
