//
//  MyCornerVC.m
//  HydePark
//
//  Created by Osama on 01/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "MyCornerVC.h"
#import "NewHomeCells.h"
#import "Constants.h"
#import "VideoModel.h"
#import "Utils.h"
#import "UIImageView+RoundImage.h"
#import "UIImageView+WebCache.h"
#import "NavigationHandler.h"
#import "CoreDataManager.h"
#import "CommentsVC.h"
#import "CornerHeaderCell.h"
#import "Followings.h"
#import "FriendsVC.h"
#import "UIImage+JS.h"
#import "ApiManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "CustomLoading.h"
#import "GIBadgeView.h"
#import "HeaderCollectionCell.h"
#import "VideoViewCell.h"
#import "EmptyCollectionCell.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage+animatedGIF.h"
#import "FLAnimatedImageView+PINRemoteImage.h"
#import "DBCommunicator.h"
#import "OfflineDataModel.h"
#import "EXPhotoViewer.h"



@interface MyCornerVC (){
    UIRefreshControl *refreshControl;
    NSMutableDictionary *serviceParams;
    
    UIImage *chosenImage;
    BOOL justChangedDp;
    CGRect cellRect;
    NSString *name;
    int completedIndex;
    NSString *uploadIndex;
    UICollectionViewLeftAlignedLayout *flowLayout;
    UICollectionViewLayout *normalLayout;
    GIBadgeView *badge;
    
    int selectedTag;
   
}
@end

@implementation MyCornerVC
@synthesize progressView;
- (id)init
{
    self = [super initWithNibName:@"MyCornerVC" bundle:Nil];
    sharedManager = [DataContainer sharedManager];
    [self registerLocalNotificaiton];
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    indexWithProgress = 1;
    [_collectionHome registerNib:[UINib nibWithNibName:@"HeaderCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"collectionHead"];
    
    [_collectionHome registerNib:[UINib nibWithNibName:@"VideoViewCell" bundle:nil] forCellWithReuseIdentifier:@"collectionCell"];
    
    [_collectionHome registerNib:[UINib nibWithNibName:@"EmptyCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"emptyCell"];
    _backgroundCount = 0;
    _firstTime = YES;
    isScrolling = false;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(adjustForNightMode)
                                                 name:SET_NIGHT_MODE
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(clearNightMode)
                                                 name:CLEAR_NIGHT_MODE
                                               object:nil];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongCellPress:)];
    lpgr.minimumPressDuration = 1.0;
    [self.collectionHome addGestureRecognizer:lpgr];
    // Do any additional setup after loading the view from its nib.
    self.collectionHome.delegate = self;
    self.collectionHome.dataSource = self;
     badge = [GIBadgeView new];
    _offlineThumbnailArray = [[NSMutableArray alloc] init];
    completedIndex = -1;
    _progressViewValue = 0.0;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateMyCornerArray" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMyCollection) name:@"internetDisconnected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMyCollection) name:@"justReload" object:nil];

    isNightMode = false;
    self.collectionHome.backgroundColor = [UIColor clearColor];
    self.collectionHome.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    normalLayout = self.collectionHome.collectionViewLayout;
    
    if ([sharedManager.channelVideos count] <= 1){
        
        flowLayout=[[UICollectionViewLeftAlignedLayout alloc]init];
        self.collectionHome.collectionViewLayout = flowLayout;
    
    }else{
    
        self.collectionHome.collectionViewLayout = normalLayout;
        
    }
    //UICollectionViewLayout *layout = [[UICollectionViewLayout alloc] init];

    //self.collectionHome.collectionViewLayout = layout;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recieveUpadtecall:) name:@"updateMyCornerArray" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMyChannel) name:@"updateRequestCount" object:nil];
    [self initFooterViewForHome];
    [self addrefreshControl];
    if(APP_DELEGATE.hasInet && [sharedManager.channelVideos count] == 0)
    {
        indicator.hidden = false;
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
            {
                indicator.color = [UIColor lightGrayColor];
            }
        }
    }
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"shouldLoadMyCorner"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"shouldLoadMyCorner"];
        [self refreshCall];
    }
    
    
    // Instantiating appdelegate change by FA

    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //end
}

-(void)adjustForNightMode
{
    refreshControl.tintColor = [UIColor grayColor];
}

-(void)clearNightMode
{
    refreshControl.tintColor = [UIColor whiteColor];
}


-(void)reloadMyCollection
{
    [self.collectionHome reloadData];
}

-(void)checkIfContentFetched:(NSTimer *)timer {
    //do smth
    if(APP_DELEGATE.myCornerDataSuccess) {
        //[self getMyChannel];
        
        [fetchTimer invalidate];
        fetchTimer = nil;
        //[self.collectionHome reloadData];
    }
}
#pragma mark OVERLOADED METHODS
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)adjustViewColours
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"]) /////IF YES ADJUST FOR NIGHTMODE
    {
        isNightMode = true;
    }
    else
    {
        isNightMode = false;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        [self adjustViewColours];
    }
    [self performSelector:@selector(startGifOnCell) withObject:nil afterDelay:0.2];

    DataContainer *sharedManager = [DataContainer sharedManager];
    [_collectionHome registerNib:[UINib nibWithNibName:@"HeaderCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"collectionHead"];
    
    [_collectionHome registerNib:[UINib nibWithNibName:@"VideoViewCell" bundle:nil] forCellWithReuseIdentifier:@"collectionCell"];
    
    [_collectionHome registerNib:[UINib nibWithNibName:@"EmptyCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"emptyCell"];
    
    name = [[NSUserDefaults standardUserDefaults] stringForKey:@"User_Name"];
    if(APP_DELEGATE.fetchingContent || APP_DELEGATE.myCornerDataSuccess) {
        if(APP_DELEGATE.myCornerDataSuccess ) {
            if(sharedManager.offlineArrayCount <= 0){
                [self.collectionHome reloadData];
            }
        }
        else {
        }
    }
    else if(!APP_DELEGATE.myCornerDataSuccess) {
//        [self getMyChannel];
    }
    
    
    NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
    
    for (int i = 0; i < offlineArray.count; i++)
    {
        OfflineDataModel *offlineVideoModel = offlineArray[i];
        if ([offlineVideoModel.isComment isEqualToString:@"1"]){
            [offlineArray removeObjectAtIndex:i];
            i--;
            sharedManager.offlineArrayCount = sharedManager.offlineArrayCount - 1;
            sharedManager.isFromBackground = NO;
        }
    }
    
    if(APP_DELEGATE.hasInet && offlineArray.count <= 0) {
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, .01);
        dispatch_async(queue, ^{
            //do your work in the background here
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //tell the main UI thread here
//                [self getMyChannel];
            });
        });
        
        
    }
//    else if(APP_DELEGATE.hasInet && offlineArray.count > 0)
//    {
//        [self setOfflineContent];
//    }
    
    if(sharedManager.offlineArrayCount <= 0){
        [self.collectionHome reloadData];
    }
    if(sharedManager.isMyCornerViewChanged) {
        sharedManager.isMyCornerViewChanged = NO;
        [_collectionHome reloadData];
    }
    if(sharedManager.isReloadCornerCollection) {
        [_collectionHome reloadData];
        sharedManager.isReloadCornerCollection = YES;
    }
    
    //Observe Notification for beamCounterUpdate
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCall) name:@"Comment" object:nil];
    //ENd
}
    

-(void) setOfflineContent{
    DataContainer *shareobj = [DataContainer sharedManager];
//    [[DBCommunicator sharedManager] clearDB];
    NSArray *tempArray = [[DBCommunicator sharedManager] retrieveAllRows];
    if(tempArray.count > 0){
        
        for (int i = 0; i < tempArray.count; i++)
        {
            
            OfflineDataModel *model = tempArray[i];
            
                VideoModel *videos     = [[VideoModel alloc] init];
                NSString *userId = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"id"];
                NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Name"];
            
                [videos setCurrent_datetime:model.currentTime];
                [videos setUser_id: userId];
                videos.is_anonymous = @"0";
                videos.userName             = userName;
                videos.user_id              = userId;
//                videos.profileImageData     = model.thumbnail;
                videos.isLocal = true;
                
                
                
//                commentAllowed = model.allowedReply;
//                textToshare = model.caption;
//                anony = model.anonyCheck;
//                if([anony isEqualToString: @"1"]) {
//                    isAnonymous = YES;
//                } else {
//                    isAnonymous = NO;
//                }
//                video_duration = model.duration;
//                postID = model.postId;
//                ParentCommentID = model.parentId;
//                privacySelected = model.privacyCheck;
//                timestamp = model.currentTime;
//                NSData *videoData = [self getVideoFromDocuments:model.data];
//                playBtnThumbnail = [self getThumbnailFromDocuments:model.thumbnail];
//                profileData = playBtnThumbnail;
            
                
            [[sharedManager channelVideos] insertObject:videos atIndex:0];
        }
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(sharedManager.isReloadMyCorner) {
        sharedManager.myCornerPageNum = 1;
//        [self getMyChannel];
        sharedManager.isReloadMyCorner = NO;
    }
}

#pragma mark ---
-(void)updateCornerArray:(VideoModel *)vObj{
    
    CoreDataManager *coreMngr = [CoreDataManager sharedManager];
    DataContainer *shareobj = [DataContainer sharedManager];
    
    if ([sharedManager.channelVideos count] == 0){
        
        flowLayout=[[UICollectionViewLeftAlignedLayout alloc]init];
        self.collectionHome.collectionViewLayout = flowLayout;
        
    }else{
        
//        self.collectionHome.collectionViewLayout = normalLayout;
        
    }
    
    if(/*!self.callFailed &&*/ APP_DELEGATE.myCornerDataSuccess){
        int p=0;
        for (int i=0;i<[shareobj.channelVideos count];i++ ) {
            VideoModel *v = [shareobj.channelVideos objectAtIndex:i];
            ////////////// FOR OFFLINE MODULE /////////////////
            if(_offlineArrayCount > 0) {
                if(_offlineArrayCount-1 < shareobj.channelVideos.count)
                    [shareobj.channelVideos replaceObjectAtIndex:_offlineArrayCount-1 withObject:vObj];
                p=1;
                break;
            } ////////////////////
            else if (v.isLocal) {
                [shareobj.channelVideos replaceObjectAtIndex:_onlineArrayCount-1 withObject:vObj];
                p=1;
                break;
            }
        }
        if (p==0) {
            [sharedManager.channelVideos insertObject:vObj atIndex:0];
        }
        int c = [sharedManager._profile.beams_count intValue];
        c++;
        sharedManager._profile.beams_count = [NSString stringWithFormat:@"%d",c];
       // [self.collectionHome reloadData];
    }else{
        cannotScrollMyCorner = NO;
        [self.roundedPView removeFromSuperview];
        self.roundedPView = nil;
        if(_offlineArrayCount > 0) {
            if(_offlineArrayCount-1 < shareobj.channelVideos.count)
                [shareobj.channelVideos replaceObjectAtIndex:_offlineArrayCount-1 withObject:vObj];
            if(sharedManager.isReloadFromBackground && shareobj.offlineArrayCount <= 0) {
                [self getMyChannel];
                sharedManager.isReloadFromBackground = NO;
            }
            return;
        }
        int index = (int)coreMngr.offlineContentArray.count;
        
        if(sharedManager.channelVideos.count<index || sharedManager.channelVideos.count == 0){
            
            [sharedManager.channelVideos insertObject:vObj atIndex:0];
            
        }else{
            
            VideoModel *uploadedObject = sharedManager.channelVideos[index];
            
            uploadedObject.isLocal = false;
            
            [sharedManager.channelVideos replaceObjectAtIndex:index withObject:vObj];
            
            index+=1;
            
            
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            
            VideoViewCell *currentCell = (VideoViewCell*)[self.collectionHome cellForItemAtIndexPath:indexPath];
            
            //        NSLog(@"%@", currentCell);
            
            
            
            //[self.roundedPView setProgress:progres];
            
            [currentCell.cellProgress removeFromSuperview];
            
            sharedManager.myCornerPageNum = 1;
        }
        if(shareobj.offlineArrayCount == 0){
            [self getMyChannel];
        }else{
        }
    }
    
    if(shareobj.offlineArrayCount <= 0 && shareobj.onlineVideoCount <= 0) {
            [self.collectionHome reloadData];
    }
}
-(void)showProgress:(NSNotification *) notification{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"MoveToMyCorner"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"MoveToCornerIndex"
     object:nil];
    [self.collectionHome reloadData];
    [self.collectionHome setContentOffset:CGPointZero animated:YES];
}




-(void)registerLocalNotificaiton{
    self.beamUpload = [[BeamUploadVC alloc]init];
    [[NSNotificationCenter defaultCenter] removeObserver:self.beamUpload name:@"uploadData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self.beamUpload
                                             selector:@selector(upLoadOflineContents:)
                                                 name:@"uploadData"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showProgress:)
                                                 name:@"ShowProgress"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProgressBar:)
                                                 name:@"updateProgress"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showStaticThumbnail:)
                                                 name:@"updateThumbnail"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showStaticThumbnailOnUpload:)
                                                 name:@"updateThumbnailOnUpload"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateOnlineVideo:)
                                                 name:@"updateOnlineVideo"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(completeProgress:)
                                                 name:@"completeProgress"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recieveUpadtecall:) name:@"GetChannelAgain"
                                               object:nil];
}

- (void)recieveUpadtecall:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@"updateMyCornerArray"])
    {
        if(!self.callFailed && APP_DELEGATE.myCornerDataSuccess){
            NSDictionary *data = notification.object;
            VideoModel *video = data[@"data"];
            // [sharedManager.channelVideos insertObject:video atIndex:0];
            int p=0;
            for (int i=0;i<[sharedManager.channelVideos count];i++ ) {
                VideoModel *v = [sharedManager.channelVideos objectAtIndex:i];
                if (v.isLocal) {
                    [sharedManager.channelVideos replaceObjectAtIndex:i withObject:video];
                    p=1;
                    break;
                }
            }
            if (p==0) {
                [sharedManager.channelVideos insertObject:video atIndex:0];
            }
            int c = [sharedManager._profile.beams_count intValue];
            c++;
            sharedManager._profile.beams_count = [NSString stringWithFormat:@"%d",c];
            [self.collectionHome reloadData];
        }else{
            cannotScrollMyCorner = NO;
            sharedManager.myCornerPageNum = 1;
            [self getMyChannel];
        }
    }
    else if([[notification name] isEqualToString:@"GetChannelAgain"]){
//        BOOL isAllDone = NO;
//        for(int i = 0; i < sharedManager.channelVideos.count ; i++){
//            VideoModel *tempVid = (VideoModel *)[sharedManager.channelVideos objectAtIndex:i];
//            if([tempVid.videoID isEqualToString:APP_DELEGATE.videObj.videoID]){
//                [sharedManager.channelVideos replaceObjectAtIndex:i withObject:APP_DELEGATE.videObj];
//                isAllDone = YES;
//                break;
//            }
//            if(isAllDone)
//                break;
//        }
//        [self.collectionHome reloadData];
        cannotScrollMyCorner = NO;
        sharedManager.myCornerPageNum = 1;
        [self getMyChannel];
    }
    else{
        [self.collectionHome reloadData];
    }
}

-(void)addrefreshControl{
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            refreshControl.tintColor = [UIColor grayColor];
        }
        else
        {
            refreshControl.tintColor = [UIColor whiteColor];
        }
    }
    else
    {
        refreshControl.tintColor = [UIColor whiteColor];
    }
    [refreshControl addTarget:self
                       action:@selector(refreshCall)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionHome addSubview:refreshControl];
    self.collectionHome.alwaysBounceVertical = YES;

}

#pragma mark - ThumbnailDuringLoading

-(void)updateOnlineVideo:(NSNotification *)notification {

    NSDictionary *prog = notification.object;
    NSData *thumbnailData = prog[@"data"];
    NSMutableArray *reverseArray = [[NSMutableArray alloc] init];
    [reverseArray addObject:thumbnailData];
    [reverseArray addObjectsFromArray:_offlineThumbnailArray];
    _offlineThumbnailArray = reverseArray;
    [_collectionHome reloadData];
    
}


-(void)showStaticThumbnail:(NSNotification *)notification {
    
    if(APP_DELEGATE.myCornerDataSuccess){
        NSDictionary *prog = notification.object;
        NSData *thumbnailData = prog[@"data"];
        NSArray *offlineModelArray = [[DBCommunicator sharedManager] retrieveAllRows];
        int currentIndex = [prog[@"index"] intValue];
        for (int i = 0; i < offlineModelArray.count; i++) {
            
            if(_offlineThumbnailArray.count == 0) {
                [_offlineThumbnailArray addObject:thumbnailData];
            }
            else if(i == offlineModelArray.count-1 && i < currentIndex) {
                NSMutableArray *reverseArray = [[NSMutableArray alloc] init];
                [reverseArray addObject:thumbnailData];
                [reverseArray addObjectsFromArray:_offlineThumbnailArray];
                _offlineThumbnailArray = reverseArray;
            }
        }
        
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
        
        VideoViewCell *currentCell = (VideoViewCell*)[self.collectionHome cellForItemAtIndexPath:indexPath];
        currentCell.mainThumbnail.hidden = NO;
        
        
        if(_offlineThumbnailArray.count > 0)
            currentCell.mainThumbnail.image = [UIImage imageWithData:thumbnailData];
    }

}

-(void)showStaticThumbnailOnUpload:(NSNotification *)notification {
    
    NSArray *offlineModelArray = [[DBCommunicator sharedManager] retrieveAllRows];
    for (int i = 0; i < offlineModelArray.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i+1 inSection:0];
        VideoViewCell *currentCell = (VideoViewCell*)[self.collectionHome cellForItemAtIndexPath:indexPath];
        currentCell.mainThumbnail.hidden = NO;
        if(_offlineThumbnailArray.count > 0 && i < _offlineThumbnailArray.count)
            currentCell.mainThumbnail.image = [UIImage imageWithData:_offlineThumbnailArray[i]];
    }
}




#pragma mark - WDUploadProgressView Delegate Methods
-(void)updateProgressBar:(NSNotification*)notification
{
    // Disable Grid adn List view button when uploading
//    NSIndexPath *headerindexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//
//    HeaderCollectionCell *headerCell = (HeaderCollectionCell*)[self.collectionHome cellForItemAtIndexPath:headerindexPath];
//    headerCell.gridListBtn.enabled = NO;
    
    //////////////
    
    NSDictionary *prog = notification.object;
    float progres = [prog[@"progress"] floatValue];
    _progressViewValue = progres;
//    if(progres == 0.0) {
//       headerCell.gridListBtn.enabled = YES;
//    }
//    int currentIndex = [prog[@"index"] intValue];

    NSString *targetTimeStamp = prog[@"index"];
    //currentIndex = (int)sharedManager.uploadCount;
    
//    if (currentIndex == 0){
//
//        currentIndex = 1;
//    }
    
 //   _offlineThumbnailArray = [[DBCommunicator sharedManager] retrieveAllRows];

//    if(currentIndex > 1) {
//
//        NSIndexPath *firstindexPath = [NSIndexPath indexPathForRow:1 inSection:0];
//
//        VideoViewCell *firstCell = (VideoViewCell*)[self.collectionHome cellForItemAtIndexPath:firstindexPath];
//        [firstCell.cellProgress setProgress:0.0];
//    }
    
    int currentIndex = 1;
    
    for(int i = 0; i < [sharedManager channelVideos].count; i ++)
    {
        VideoModel *eachVideo = [[sharedManager channelVideos] objectAtIndex:i];
        NSDate *dateObj = [self getDateFromString:eachVideo.current_datetime];
        NSTimeInterval timeStamp1 = [dateObj timeIntervalSince1970];
        NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp1];
        NSString *eachTimeStamp = timeStampObj.stringValue;
        if([eachTimeStamp isEqualToString:targetTimeStamp])
        {
            currentIndex = i + 1;
            break;
        }
    }
    
//    if (currentIndex == 0){
//
//        currentIndex = 1;
//    }

    indexWithProgress = currentIndex;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
    
    VideoViewCell *currentCell = (VideoViewCell*)[self.collectionHome cellForItemAtIndexPath:indexPath];
    
    NSData *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"myCornerThumbnail"];
    currentCell.mainThumbnail.hidden = NO;
    currentCell.mainThumbnail.image = [UIImage imageWithData:temp];
    
    [currentCell.cellProgress setProgress:progres];

}

-(NSDate *) getDateFromString :(NSString *)targetString
{
    NSDateFormatter *formatter;
    NSDate        *dateObj;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    dateObj = [formatter dateFromString:targetString];
    
    return dateObj;
}


-(void)completeProgress:(NSNotification*)notification
{
    _progressViewValue = 0.0;
    ///////////////
    NSDictionary *prog = notification.object;
//    int currentIndex = [prog[@"index"] intValue];
//    if (currentIndex == 0){
//        currentIndex = 1;
//    }
//    if(currentIndex > 1) {
//
//        NSIndexPath *firstindexPath = [NSIndexPath indexPathForRow:1 inSection:0];
//
//        VideoViewCell *firstCell = (VideoViewCell*)[self.collectionHome cellForItemAtIndexPath:firstindexPath];
//        [firstCell.cellProgress setProgress:0.0];
//    }
    
    NSString *targetTimeStamp = prog[@"index"];
    
    int currentIndex = 1;
    
    for(int i = 0; i < [sharedManager channelVideos].count; i ++)
    {
        VideoModel *eachVideo = [[sharedManager channelVideos] objectAtIndex:i];
        NSDate *dateObj = [self getDateFromString:eachVideo.current_datetime];
        NSTimeInterval timeStamp1 = [dateObj timeIntervalSince1970];
        NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp1];
        NSString *eachTimeStamp = timeStampObj.stringValue;
        if([eachTimeStamp isEqualToString:targetTimeStamp])
        {
            currentIndex = i + 1;
            break;
        }
    }
    
//    if (currentIndex == 0){
//
//        currentIndex = 1;
//    }
    
    NSIndexPath *indexPath ;
    indexPath = [NSIndexPath indexPathForItem:indexWithProgress inSection:0];
//    indexPath = [NSIndexPath indexPathForItem:currentIndex inSection:0];
    VideoViewCell *currentCell = (VideoViewCell*)[self.collectionHome cellForItemAtIndexPath:indexPath];
    [currentCell.cellProgress removeFromSuperview];
    [self.roundedPView removeFromSuperview];
}

- (void)uploadDidFinish:(WDUploadProgressView *)progressview {
    [progressview removeFromSuperview];
    //[self.collectionHome setTableHeaderView:nil];
}

- (void)uploadDidCancel:(WDUploadProgressView *)progressview {
    [progressview removeFromSuperview];
    //[self.collectionHome setTableHeaderView:nil];
}

#pragma mark Refresh Control
- (void)refreshCall
{
    [[DBCommunicator sharedManager] createDB];
    NSArray *offlineModelArray = [[DBCommunicator sharedManager] retrieveAllRows];
    
    if(!APP_DELEGATE.isUploading && offlineModelArray.count == 0){
        NSLog(@"yes refreshing...");
        cannotScrollMyCorner = NO;
        sharedManager.myCornerPageNum = 1;
        [self getMyChannel];
    } else {
        [refreshControl endRefreshing];
    }
}

#pragma mark footerView
-(void)initFooterViewForHome{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat width = mainScreen.bounds.size.width;
    footerViewHome = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, 70.0)];
    UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    actInd.tag = 790;
    actInd.frame = CGRectMake(width/2 - 10 ,40.0, 20.0, 20.0);
    actInd.hidesWhenStopped = YES;
    // [footerViewHome addSubview:actInd];
    //self.collectionHome.tableFooterView = footerViewHome;
}

#pragma mark Server Call
- (void) getMyChannel{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
            NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
            if(indexPath.item > 0){
                cell.mainThumbnail.hidden = NO;
                cell.CH_Video_Thumbnail.hidden = YES;
                [cell.CH_Video_Thumbnail stopAnimating];
            }
        }
    });
    
    CoreDataManager   *coreMngr = [CoreDataManager sharedManager];
    justChangedDp = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.fetchingContent = TRUE;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",sharedManager.myCornerPageNum];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_MY_CHENNAL,@"method",
                              token,@"session_token",pageStr,@"page_no",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        indicator.hidden = true;

        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [refreshControl endRefreshing];
            
            self.callFailed = NO;
            APP_DELEGATE.myCornerDataSuccess= YES;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSDictionary *posts = [result objectForKey:@"profile"];
            if(success == 1) {
                
                [self parseVideoResponse:result posts:posts coreManager:coreMngr];
            }
            else{
                [refreshControl endRefreshing];
                APP_DELEGATE.myCornerDataSuccess = true;
                self.fetchingContent = FALSE;
            }
        }else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [refreshControl endRefreshing];
            self.callFailed = YES;
//            NSLog(@"NO internet");
        }
    }];
}

- (void) updateMyCorner{
    NSString *prevPostDate;
    if(_prevPost >= 0) {
        VideoModel *prevPost = [[VideoModel alloc]init];
        prevPost  = [sharedManager.channelVideos objectAtIndex:_prevPost];
        prevPostDate = prevPost.order_timestamp;
    } else {
        prevPostDate = @"0000-00-00 00:00:00.000";
    }
    
    NSString *nextPostDate;
    if(_nextPost < sharedManager.channelVideos.count) {
        VideoModel *nextPost = [[VideoModel alloc]init];
        nextPost  = [sharedManager.channelVideos objectAtIndex:_nextPost];
        nextPostDate = nextPost.order_timestamp;
    } else {
        nextPostDate = @"0000-00-00 00:00:00.000";
    }
    
    VideoModel *currentPost = [[VideoModel alloc]init];
    currentPost  = [sharedManager.channelVideos objectAtIndex:_currentPost];
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_UPDATE_MY_CORNER,@"method",
                              token,@"session_token",currentPost.videoID,@"post_id",prevPostDate,@"prev_post",nextPostDate,@"next_post",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
           
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                NSLog(@"success");
            }
            else {
                NSLog(@"Fail");
            }
        }
        else{
            
            NSLog(@"Fail");
        }
        
    }];
    
    
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

#pragma mark Tableview Delegates
//-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
//                    withVelocity:(CGPoint)velocity
//             targetContentOffset:(inout CGPoint *)targetContentOffset{
//    if (velocity.y > 0){
//        self.isDownwards = YES;
//        // [self hideBottomBar];
//    }
//    if (velocity.y < 0){
//        self.isDownwards = NO;
//        //[self showBottomBar];
//    }
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    int count;
//    count  = (sharedManager.channelVideos.count / 3 == 0) ? 0 : 1;
//    BOOL endOfTable = (scrollView.contentOffset.y >= ((sharedManager.channelVideos.count/3 - count* 130.0f) - scrollView.frame.size.height)); // Here 150 is row height
//    if (endOfTable && !self.fetchingContent && !scrollView.dragging && !scrollView.decelerating && self.isDownwards && !cannotScrollMyCorner)
//    {
//        //        if(self.collectionHome.tableFooterView == nil){
//        //            self.collectionHome.tableFooterView = footerViewHome;
//        //            [(UIActivityIndicatorView *)[footerViewHome viewWithTag:790] startAnimating];
//        //        }
//        //        else{
//        //            self.collectionHome.tableFooterView = Nil;
//        //        }
//    }
//}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        isScrolling = true;
        for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
            NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
            if(indexPath.item > 0){
                cell.mainThumbnail.hidden = NO;
                cell.CH_Video_Thumbnail.hidden = YES;
                [cell.CH_Video_Thumbnail stopAnimating];
            }
        }
    });
}
-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                    withVelocity:(CGPoint)velocity
             targetContentOffset:(inout CGPoint *)targetContentOffset{
    NSLog(@"here");
    if (velocity.y > 0){
        self.isDownwards = YES;
        // [self hideBottomBar];
    }
    if (velocity.y < 0){
        self.isDownwards = NO;
        //[self showBottomBar];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"here 2");
    isScrolling = false;
    if(!self.fetchingContent)
    {
        [self startGifOnCell];
    }
    /// TO PLAY GIF FOR VISIBLE CELLS
    //    for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
    //        /// TO PLAY GIF FOR VISIBLE CELLS
    //        NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
    //        if(indexPath.item > 0){
    //            [cell.CH_Video_Thumbnail startAnimating];
    //        }
    //    }
    //    ///////
    //    BOOL endOfTable = (scrollView.contentOffset.y >= ((sharedManager.forumsVideo.count/3 * 130.0f) - scrollView.frame.size.height)); // Here 150 is row height
    //    if (endOfTable && !self.fetchingContent && !scrollView.dragging && !scrollView.decelerating && !cannotscroll && self.isDownwards){
    //        self.tblHome.tableFooterView = footerViewHome;
    //        [(UIActivityIndicatorView *)[footerViewHome viewWithTag:790] startAnimating];
    //    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    /// TO PLAY GIF FOR VISIBLE CELLS
    if(!decelerate) {
        //        for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
        //            NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
        //            if(indexPath.item > 0){
        //                [cell.CH_Video_Thumbnail startAnimating];
        //            }
        //        }
        //
        //
        //                            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC));
        //                            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"here 333");
        isScrolling = false;
        if(!self.fetchingContent)
        {
            [self startGifOnCell];
        }
        //                            });
    }
    //    for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
    //        NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
    //        if(indexPath.item > 0){
    //            [cell.CH_Video_Thumbnail startAnimating];
    //        }
    //    }
}

- (void)startGifOnCell {
    
    if(!self.fetchingContent && !isScrolling)
    {
        NSLog(@"starting gifs...");
        dispatch_async(dispatch_get_main_queue(), ^{
            int counter = 0;
            for (counter = 0; counter < [self.collectionHome visibleCells].count; counter ++) {
                
                VideoViewCell *cell = [[self.collectionHome visibleCells] objectAtIndex:counter];
                NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
                VideoModel *tempVideos = [[VideoModel alloc]init];
                if(indexPath.row > 0 && indexPath.row < sharedManager.channelVideos.count)
                {
                    tempVideos  = [sharedManager.channelVideos objectAtIndex:indexPath.row - 1];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if(cell.CH_Video_Thumbnail.animatedImage == nil)
                                {
                                    cell.mainThumbnail.hidden = NO;
                                    cell.CH_Video_Thumbnail.hidden = YES;
                                    [cell.CH_Video_Thumbnail stopAnimating];
                                }
                                else
                                {
                                    cell.mainThumbnail.hidden = YES;
                                    cell.CH_Video_Thumbnail.hidden = NO;
                                    [cell.CH_Video_Thumbnail startAnimating];
                                }
                                
                            });
                            
                        }];
                        
                    });
                }
            }
        });
    }
}



#pragma mark TABLE VIEW DELEGATES

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //pagination
    int count;
    count  = (sharedManager.channelVideos.count % 3 == 0) ? 0 : 1;
    if(!self.fetchingContent && indexPath.row  == sharedManager.channelVideos.count/3 - count && APP_DELEGATE.myCornerDataSuccess && !cannotScrollMyCorner) {
        sharedManager.myCornerPageNum++;
        [self getMyChannel];
    }
    //    if (cannotScrollMyCorner){
    //        self.collectionHome.tableFooterView = Nil;
    //    }
}

-(void)collectionView:(UICollectionView *)collectionView
willDisplayCell:(UICollectionViewCell *)cell
forItemAtIndexPath:(NSIndexPath *)indexPath{

    int count;
    count  = (sharedManager.channelVideos.count == 0) ? 0 : 1;
    if(!self.fetchingContent && indexPath.row  == sharedManager.channelVideos.count- count && APP_DELEGATE.myCornerDataSuccess && !cannotScrollMyCorner) {
        sharedManager.myCornerPageNum++;
        [self getMyChannel];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    float returnValue;
    if(indexPath.section == 0){
        if(IS_IPAD){
            returnValue = 320;
        }else{
            returnValue = 220;
        }
    }
    else{
        if (IS_IPAD)
            returnValue = 260.0f;
        else if(IS_IPHONE_5)
            returnValue = 110.0f;
        else if(IS_IPHONE_6Plus){
            returnValue = 145.0;
        }
        else
            returnValue = 130.0f;
    }
    return returnValue;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return 1;
    }
    else{
        int rows = (int)([sharedManager.channelVideos count] / 3);
        if([sharedManager.channelVideos count] % 3  == 1 || [sharedManager.channelVideos count] % 3 == 2) {
            rows = rows + 1;
        }
        //plus one for header
        return rows;
    }
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 40;
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"videoCells";
    static NSString *CornerHeader = @"cornerHeader";
    NewHomeCells *cell;
    CornerHeaderCell *chCell;
    currentIndexHome = (indexPath.row  * 3);
    if(indexPath.section == 0){
        if(chCell == nil){
            NSArray *topLevelObjects;
            if(IS_IPAD){
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CornerHeaderCell_iPad" owner:self options:nil];
            }else{
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CornerHeaderCell" owner:self options:nil];
            }
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            chCell = [topLevelObjects objectAtIndex:0];
        }
        else{
            chCell  = [tableView dequeueReusableCellWithIdentifier:CornerHeader];
        }
        
        int isCelebrity = [Utils userIsCeleb];
      
        if(isCelebrity){
            chCell.celebBottomView.hidden = NO;
            chCell.normalUserBottomView.hidden = YES;
            // [chCell.celebBeamsBtn addTarget:self action:@selector(takeToArchive:) forControlEvents:UIControlEventTouchUpInside];
            [chCell.celebFollowersBtn addTarget:self action:@selector(showFilteredFollowers:) forControlEvents:UIControlEventTouchUpInside];
            [chCell.celebFriendsBtn addTarget:self action:@selector(showfilterFriends:) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
            chCell.celebBottomView.hidden = YES;
            chCell.normalUserBottomView.hidden = NO;
        }
        if(sharedManager._profile == Nil){
            if(![Utils userIsCeleb]){
                chCell.celebBeamsBtn.hidden = YES;
                chCell.noOfFriendslbl.text = [Utils getchachedFriendsCount];
                chCell.noOfBeamslbl.text = [Utils getcachedBeamsCount];
                chCell.inboxCount.text = [Utils getCachedInboxCount];
                if(![[Utils getCachedPendingrReqCount] isEqualToString:@"0"]){
                    chCell.pendingReqCount.hidden = NO;
                    chCell.pendingReqCount.text = [Utils getCachedPendingrReqCount];
                }else{
                    chCell.pendingReqCount.hidden = YES;
                }
                chCell.normalUserBottomView.hidden = NO;
            }else{
                chCell.cFriendslbl.text = [Utils getchachedFriendsCount];
                chCell.cBeamslbl.text =   [Utils getCachedViewsCount];
                chCell.cFollowersLbl.text = [Utils getCachedFollowersCount];
                chCell.normalUserBottomView.hidden = YES;
                chCell.celebBeamsBtn.hidden = NO;
            }
        }else{
            NSString *bCount = [Utils abbreviateNumber:sharedManager._profile.profile_view_count withDecimal:1];
            NSString *beamsC = [Utils abbreviateNumber:sharedManager._profile.beams_count withDecimal:1];
            NSString *inboxC = [Utils abbreviateNumber:sharedManager._profile.inboxCount withDecimal:1];
            NSString *pendingReqC = [Utils abbreviateNumber:sharedManager._profile.pendingReqsCount withDecimal:1];
            
            if(![pendingReqC isEqualToString:@"0"] && ![pendingReqC isEqual: [NSNull null]]){
                chCell.pendingReqCount.hidden = NO;
                chCell.pendingReqCount.text = pendingReqC;
            }else{
                chCell.pendingReqCount.hidden = YES;
            }
            [Utils setCachedPendingReqCount:pendingReqC];
            [Utils setCachedBeamsCount:beamsC];
            [Utils setcachedViewsCount:bCount];
            [Utils setCachedInboxCount:inboxC];
            if([sharedManager._profile.beams_count integerValue] == 1){
                chCell.noOfBeamslbl.text = [[NSString alloc]initWithFormat:@"%@",beamsC];
                chCell.cBeamslbl.text = [[NSString alloc]initWithFormat:@"%@",bCount];
            }
            else{
                chCell.noOfBeamslbl.text = [[NSString alloc]initWithFormat:@"%@",beamsC];
                chCell.cBeamslbl.text = [[NSString alloc]initWithFormat:@"%@",bCount];
            }
            if([[self getfriendsCount] intValue] == 1){
                chCell.noOfFriendslbl.text = [NSString stringWithFormat:@"%@",[self getfriendsCount]];
                chCell.cFriendslbl.text = [NSString stringWithFormat:@"%@",[self getfriendsCount]];
            }else{
                chCell.noOfFriendslbl.text = [NSString stringWithFormat:@"%@",[self getfriendsCount]];
                chCell.cFriendslbl.text = [NSString stringWithFormat:@"%@",[self getfriendsCount]];
            }
            if([[self getFollowersCount] intValue]== 1){
                chCell.cFollowersLbl.text = [NSString stringWithFormat:@"%@",[self getFollowersCount]];
            }else{
                chCell.cFollowersLbl.text = [NSString stringWithFormat:@"%@",[self getFollowersCount]];
            }
//            GIBadgeView *badge = [GIBadgeView new];
            [chCell.noOfFriendslbl addSubview:badge];
            badge.badgeValue = [pendingReqC intValue];
            chCell.inboxCount.text = inboxC;
            CGRect labelRect = [inboxC
                                boundingRectWithSize:chCell.inboxCount.frame.size
                                options:NSStringDrawingUsesLineFragmentOrigin
                                attributes:@{
                                             NSFontAttributeName : chCell.inboxCount.font
                                             }
                                context:nil];
            
            CGRect newFrame = chCell.inboxCount.frame;
            CGFloat point = newFrame.size.width - labelRect.size.width;
            if(labelRect.size.width > 13){
                point += 10;
            }
            if(labelRect.size.width > 25){
                point += 25;
            }
            
            chCell.dotforInboxNotifications.frame = CGRectMake(point, chCell.dotforInboxNotifications.frame.origin.y, 6, 6);
            int unreadCount = [sharedManager._profile.unreadInboxCount intValue];
            if(unreadCount > 0){
                [chCell.dotforInboxNotifications setHidden:NO];
            }
        }
        chCell.nameLbl.text = name;
        //[chCell.beamsPressed addTarget:self action:@selector(takeToArchive:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.inboxPressed addTarget:self action:@selector(taketoInbox:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.friendsPressed addTarget:self action:@selector(showFriends:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.editProfileBtn addTarget:self action:@selector(editProfilePressed:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.changeDp addTarget:self action:@selector(changeProfilePic:) forControlEvents:UIControlEventTouchUpInside];
        chCell.viewToRound.layer.cornerRadius = chCell.viewToRound.frame.size.width / 4;
        chCell.viewToRound.layer.masksToBounds = YES;
        chCell.viewToRound.clipsToBounds = YES;
        chCell.picBorder.layer.cornerRadius = chCell.picBorder.frame.size.width / 4;
        chCell.picBorder.clipsToBounds = YES;
        chCell.picBorder.layer.masksToBounds = NO;
        
        if(justChangedDp){
            chCell.profileImage.image = chosenImage;
            [self saveImage:chosenImage];
        }
        else {
            UIImage *placeholderImg = [self loadImage];
            if(!placeholderImg){
                placeholderImg = [UIImage imageNamed:@"pH"];
            }
            [chCell.profileImage sd_setImageWithURL:[NSURL URLWithString:sharedManager._profile.profile_image] placeholderImage:placeholderImg completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [self saveImage:image];
            }];
        }
        
        [chCell setBackgroundColor:[UIColor clearColor]];
        chCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return chCell;
        
    }
    else{
        
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects;
            if(IS_IPAD){
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells_iPad" owner:self options:nil];
            }else if(IS_IPHONE_5){
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
            }else{
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
            }
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
        }else{
            cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        }
    }
    
    VideoModel *tempVideos = [[VideoModel alloc]init];
    tempVideos  = [sharedManager.channelVideos objectAtIndex:currentIndexHome];
    if(tempVideos.beamType == 1){
        cell.rebeam1.hidden = NO;
    }
    else{
        cell.rebeam1.hidden = YES;
    }
    
    cell.CH_userName.text = [Utils decodeForEmojis:tempVideos.title];
    cell.CH_userName.text = [Utils decodeForEmojis:cell.CH_userName.text];

    cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
    cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
    if([tempVideos.comments_count isEqualToString:@"0"])
    {
        cell.CH_CommentscountLbl.hidden = YES;
        cell.leftreplImg.hidden = YES;
    }
    else{
        cell.CH_CommentscountLbl.text = tempVideos.comments_count;
    }
    
    cell.CH_heartCountlbl.text = tempVideos.like_count;
    cell.CH_seen.text = tempVideos.seen_count;
    cell.Ch_videoLength.text = tempVideos.video_length;
    
    if([tempVideos.is_anonymous  isEqualToString: @"0"]){
        cell.dummyLeft1.hidden  = YES;
    }
    else{
        //        cell.dummyLeft1.image = [UIImage imageNamed:@"btanonymous.png"];
        cell.annonyOverlayLeft.hidden = NO;
        cell.annonyImgLeft.hidden = NO;
        //cell.CH_userName.text = @"Anonymous";
        cell.dummyLeft1.hidden  = NO;
    }
    if(tempVideos.beamType == 1){
        cell.CH_heart.hidden = NO;
    }
    else if(tempVideos.beamType == 2){
        cell.CH_heart.hidden = YES;
        NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

    }
    else{
        cell.CH_heart.hidden = YES;
    }
    [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
    cell.CH_commentsBtn.enabled = YES;
    if (tempVideos.isLocal && !APP_DELEGATE.hasInet) {
        cell.CH_commentsBtn.backgroundColor = [UIColor colorWithRed:111/255 green:113/255 blue:121/255 alpha:1];
        cell.CH_commentsBtn.alpha = 0.55;
        cell.CH_VideoTitle.text = tempVideos.current_datetime;
        cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
        cell.CH_CommentscountLbl.text = @"!";
        cell.CH_commentsBtn.enabled = NO;
        
    }
    else if(tempVideos.isLocal){
        cell.CH_commentsBtn.enabled = NO;
        cell.leftreplImg.hidden =  YES;
        cell.CH_VideoTitle.text = tempVideos.current_datetime;
        cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
        
        
        
        if(IS_IPHONE_6Plus){
            self.roundedPView = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x , cell.view1.frame.origin.y, cell.view1.frame.size.width + 11 , cell.view1.frame.size.height  + 13)];
        }
        else if(IS_IPHONE_5){
            self.roundedPView = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x , cell.view1.frame.origin.y, 95 , 90)];
        }
        else{
            self.roundedPView = [[PWProgressView alloc] initWithFrame:cell.view1.frame];
        }
        self.roundedPView.layer.cornerRadius = cell.view1.frame.size.width / 6.2f;
        self.roundedPView.clipsToBounds = YES;
        [cell.contentView addSubview:self.roundedPView];
    }else{
        cell.CH_commentsBtn.enabled = YES;
        
        if([self.roundedPView isDescendantOfView:cell.contentView]){
            
            [self.roundedPView removeFromSuperview];
            //[cell.contentView willRemoveSubview:self.roundedPView];
        
        }
    }
    
    if(IS_IPAD)
        //cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /7.4f;
    cell.imgContainer.layer.masksToBounds = YES;
    [cell.CH_Video_Thumbnail roundCorners];
    [cell.view1 setBackgroundColor:[UIColor clearColor]];
    
    
    
    cell.CH_RcommentsBtn.enabled = YES;
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0;
    [cell.CH_commentsBtn addGestureRecognizer:lpgr];
    [lpgr.view setTag:currentIndexHome];
    [cell.CH_heart setTag:currentIndexHome];
    [cell.CH_playVideo setTag:currentIndexHome];
    
    [cell.CH_flag setTag:currentIndexHome];
    [cell.CH_commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.CH_commentsBtn setTag:currentIndexHome];
    
    currentIndexHome++;
    if(currentIndexHome < sharedManager.channelVideos.count)
    {
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [sharedManager.channelVideos objectAtIndex:currentIndexHome];
        if(tempVideos.beamType == 1){
            cell.rebeam2.hidden = NO;
        }
        else{
            cell.rebeam2.hidden = YES;
        }
        [cell.CH_RcommentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CH_RcommentsBtn setTag:currentIndexHome];
        [cell.CH_RplayVideo setTag:currentIndexHome];
        [cell.CH_Rheart setTag:currentIndexHome];
        cell.CH_RVideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.CH_Rseen.text = tempVideos.seen_count;
        cell.rightTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
        if(IS_IPAD)
            cell.RimgContainer.layer.cornerRadius  = cell.RimgContainer.frame.size.width /7.4f;
        cell.RimgContainer.layer.masksToBounds = YES;
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPress:)];
        lpgr.minimumPressDuration = 1.0;
        [cell.CH_RcommentsBtn addGestureRecognizer:lpgr];
        [cell.CH_heart setTag:currentIndexHome];
        cell.CH_RheartCountlbl.text  = tempVideos.like_count;
        cell.CH_RuserName.text = [Utils decodeForEmojis:tempVideos.title];
        cell.CH_RuserName.text = [Utils decodeForEmojis:cell.CH_RuserName.text];
        
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.CH_RCommentscountLbl.hidden = YES;
            cell.rightreplImg.hidden = YES;
        }
        else{
            cell.CH_RCommentscountLbl.text = tempVideos.comments_count;
        }
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.dummyright1.hidden  = YES;
        }
        else{
            //            cell.dummyright1.image = [UIImage imageNamed:@"btanonymous.png"];
            cell.anonyImgRight.hidden = NO;
            cell.anonyOverlayRight.hidden = NO;
            //cell.CH_RuserName.text = @"Anonymous";
            cell.dummyright1.hidden  = NO;
        }
        [cell.CH_RVideo_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        
        [cell.CH_RVideo_Thumbnail roundCorners];
        if(tempVideos.isLocal && APP_DELEGATE.hasInet){
            cell.rightreplImg.hidden =  YES;
            cell.CH_RVideoTitle.text = tempVideos.current_datetime;
            cell.CH_RVideo_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
            if(IS_IPHONE_6Plus){
                self.roundedPView = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view2.frame.origin.x , cell.view2.frame.origin.y, cell.view2.frame.size.width + 11 , cell.view2.frame.size.height  + 12)];
            }
            else if(IS_IPHONE_5){
                self.roundedPView = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view2.frame.origin.x , cell.view2.frame.origin.y, 95 , 90)];
            }
            else{
                self.roundedPView = [[PWProgressView alloc] initWithFrame:cell.view2.frame];
            }
            self.roundedPView.layer.cornerRadius = cell.view2.frame.size.width / 6.2f;
            self.roundedPView.clipsToBounds = YES;
            [cell.contentView addSubview:self.roundedPView];
        }
        else if (tempVideos.isLocal ) {
            cell.CH_RcommentsBtn.backgroundColor = [UIColor colorWithRed:111/255 green:113/255 blue:121/255 alpha:1];
            cell.CH_RCommentscountLbl.text = @"!";
            cell.CH_RcommentsBtn.alpha = 0.55;
            cell.CH_RVideoTitle.text = tempVideos.current_datetime;
            cell.CH_RVideo_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
        }
        
        if(tempVideos.beamType == 1){
            cell.CH_Rheart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            cell.CH_Rheart.hidden = YES;
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.CH_RuserName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        else{
            cell.CH_Rheart.hidden = YES;
        }
        currentIndexHome++;
    }else{
        cell.rightreplImg.hidden = YES;
        cell.CH_RCommentscountLbl.hidden = YES;
        cell.view2.hidden = YES;
    }
    if(currentIndexHome < sharedManager.channelVideos.count){
        
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [sharedManager.channelVideos objectAtIndex:currentIndexHome];
        if(tempVideos.beamType == 1){
            cell.rebeam3.hidden = NO;
        }
        else{
            cell.rebeam3.hidden = YES;
        }
        [cell.showCommentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.showCommentsBtn setTag:currentIndexHome];
        [cell.CH_RplayVideo setTag:currentIndexHome];
        [cell.CH_Rheart setTag:currentIndexHome];
        cell.rightDate.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.CH_Rseen.text = tempVideos.seen_count;
        cell.rightTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
        if(IS_IPAD)
            cell.containerExtRight.layer.cornerRadius  = cell.containerExtRight.frame.size.width /7.4f;
        cell.containerExtRight.layer.masksToBounds = YES;
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPress:)];
        lpgr.minimumPressDuration = 1.0;
        [cell.showCommentsBtn addGestureRecognizer:lpgr];
        [cell.CH_heart setTag:currentIndexHome];
        cell.CH_RheartCountlbl.text  = tempVideos.like_count;
        
        cell.rightUsername.text = [Utils decodeForEmojis:tempVideos.title];
        cell.rightUsername.text = [Utils decodeForEmojis:cell.rightUsername.text];
        
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.replyCountlbl.hidden = YES;
            cell.replyRedBg.hidden = YES;
        }
        else{
            cell.replyCountlbl.text = tempVideos.comments_count;
        }
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.dummy1.hidden  = YES;
        }
        else{
            //            cell.dummy1.image = [UIImage imageNamed:@"btanonymous.png"];
            cell.anonyImgExtRight.hidden = NO;
            cell.anonyOverlayExtRight.hidden = NO;
            //cell.rightUsername.text = @"Anonymous";
            cell.dummy1.hidden  = NO;
        }
        [cell.rightThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        
        [cell.rightThumbnail roundCorners];
        if(tempVideos.isLocal && APP_DELEGATE.hasInet){
            cell.replyRedBg.hidden =  YES;
            cell.rightDate.text = tempVideos.current_datetime;
            cell.rightThumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
            if(IS_IPHONE_6Plus){
                self.roundedPView = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view3.frame.origin.x , cell.view3.frame.origin.y, cell.view3.frame.size.width + 11 , cell.view3.frame.size.height  + 12)];
            }
            else if(IS_IPHONE_5){
                self.roundedPView = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view3.frame.origin.x , cell.view3.frame.origin.y, 95 , 90)];
            }
            else{
                self.roundedPView = [[PWProgressView alloc] initWithFrame:cell.view3.frame];
            }
            self.roundedPView.layer.cornerRadius = cell.view3.frame.size.width / 6.2f;
            self.roundedPView.clipsToBounds = YES;
            [cell.contentView addSubview:self.roundedPView];
        }
        else if (tempVideos.isLocal ) {
            cell.showCommentsBtn.backgroundColor = [UIColor colorWithRed:111/255 green:113/255 blue:121/255 alpha:1];
            cell.replyCountlbl.text = @"!";
            cell.replyRedBg.hidden = false;
            //cell.replyCountlbl.layer.cornerRadius = cell.replyCountlbl.bounds.size.width/2;
            //cell.replyCountlbl.backgroundColor = [UIColor redColor];
            cell.replyCountlbl.hidden = false;
            cell.showCommentsBtn.alpha = 0.55;
            cell.rightDate.text = tempVideos.current_datetime;
            cell.CH_RVideo_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
        }
        
        if(tempVideos.beamType == 1){
            cell.CH_Rheart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            cell.CH_Rheart.hidden = YES;
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.rightUsername.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        else{
            cell.CH_Rheart.hidden = YES;
        }
        currentIndexHome++;
    }
    else{
        cell.view3.hidden = YES;
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(IBAction)changeProfilePic:(id)sender{


    // Remove Image - Change By FA
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle: UIAlertControllerStyleActionSheet];

    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Change profile photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        
        if (appDelegate.hasInet) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:NULL];
            
        }
        else {
            
            NSString *message;
            NSString *title;
            message = NSLocalizedString(@"You are not connected with the internet.", @"");
            title   = NSLocalizedString(@"Sorry!", @"");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                           delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                                  otherButtonTitles:nil, nil];
            [alert show];
        }
       
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Remove photo" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
    
        if (appDelegate.hasInet){
            chosenImage = [UIImage imageNamed:@"place_holder"];
            chosenImage =  [chosenImage imageWithScaledToSize:CGSizeMake(204, 204)];
            NSDictionary *imgObJ = @{@"IMG": chosenImage};
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"updateUserProfileImg"
             object:imgObJ];
            
            [self UpdateProfilePic];
        }
        else{
            NSString *message;
            NSString *title;
            message = NSLocalizedString(@"You are not connected with the internet.", @"");
            title   = NSLocalizedString(@"Sorry!", @"");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                           delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                                  otherButtonTitles:nil, nil];
            [alert show];
            
        }
        
 
    }]];
    
   
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    //End by FA
}

- (IBAction)takeToArchive:(id)sender {
    [[NavigationHandler getInstance]MoveToMyBeam];
}
- (IBAction)taketoInbox:(id)sender {
    [[NavigationHandler getInstance]MoveToChat];
}

- (IBAction)gridListBtnPressed:(id)sender {
    if(sharedManager.isListView) {
        sharedManager.isListView = NO;
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"listView"];
    } else {
        sharedManager.isListView = YES;
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"listView"];
    }
    sharedManager.isSpeakerViewChanged = YES;
    sharedManager.isFriendViewChanged = YES;
    [_collectionHome reloadData];
}
#pragma  mark FriendsCount
-(NSString *)getfriendsCount
{
//    int totalCount = 0;
//    for (int i =0; i< sharedManager.followers.count ; i++){
//        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
//        if([tempObj.status isEqualToString:@"FRIEND" ]){
//            totalCount ++;
//        }
//    }
    NSString *countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.friends_count];
    if([countSuffix isEqualToString:@"(null)"])
    {
        countSuffix = @"0";
    }
    countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
    [Utils setChacedFriendsCount:countSuffix];
    return countSuffix;
}
-(NSString *)getFollowersCount
{
//    int totalCount = 0;
//    for (int i =0; i< sharedManager.followers.count ; i++){
//        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
//        if(tempObj.isFollowed == 1){
//            totalCount ++;
//        }
//    }
    NSString *countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.likes_count];
    countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
    [Utils setCachedFollowersCount:countSuffix];
    return countSuffix;
}

#pragma mark -  IMAGE Delegate Method
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    //Change by FA
    if (appDelegate.hasInet) {
        chosenImage = info[UIImagePickerControllerOriginalImage];
        chosenImage =  [chosenImage imageWithScaledToSize:CGSizeMake(204, 204)];
        NSDictionary *imgObJ = @{@"IMG": chosenImage};
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"updateUserProfileImg"
         object:imgObJ];
        
        [self UpdateProfilePic];

    }else{
         [self dismissViewControllerAnimated:YES completion:nil];
        NSString *message;
        NSString *title;
        message = NSLocalizedString(@"You are not connected with the internet.", @"");
        title   = NSLocalizedString(@"Sorry!", @"");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                              otherButtonTitles:nil, nil];
        [alert show];

    }
    //END
}

-(void) UpdateProfilePic{
    
    //Change by FA
    if (!appDelegate.hasInet) {
        NSString *message;
        NSString *title;
        message = NSLocalizedString(@"You are not connected with the internet.", @"");
        title   = NSLocalizedString(@"Sorry!", @"");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                              otherButtonTitles:nil, nil];
        [alert show];
        
    }else {
        justChangedDp = YES;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        //[self.collectionHome reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        
        [self.collectionHome reloadItemsAtIndexPaths:indexPaths];
        NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
        NSData *profileDatas = UIImagePNGRepresentation(chosenImage);
        
        
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:METHOD_UPDATE_PROFILE forKey:@"method"];
        [serviceParams setValue:token forKey:@"session_token"];
        
        
        [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:profileDatas progress:^(float progress) {
        }
                         name:@"profile_link" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                             
                             
                             int flag = [[data objectForKey:@"success"] intValue];
                             if(flag){
                                 justChangedDp = YES;
                                 //Update profile Link (Change by FA)
                                 NSDictionary *profile = [data objectForKey:@"profile"];
                                 NSString *userImg = [profile objectForKey:@"profile_link"];
                                 [[NSUserDefaults standardUserDefaults] setObject:userImg forKey:@"user_img"];
                                 //End
                             }
                         } failure:^(NSError *error) {
                             
                         }];
        
    }
    
    //ENd
    
   
}

#pragma mark HANDLE LONG PRESS
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    NSInteger tag = gestureRecognizer.view.tag;
    CGPoint p = [gestureRecognizer locationInView:self.collectionHome];
    cellRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height, 200, 100);
    NSIndexPath *indexPath = [self.collectionHome indexPathForItemAtPoint:p];
    if (indexPath == nil) {
        // NSLog(@"long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        VideoModel *temp  = [sharedManager.channelVideos objectAtIndex:tag];
        if (temp.isLocal) {
        }
        else{
            currentSelectedIndex = tag;
            [self presentAll:YES];
        }
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        //NSLog(@"ENDED");
        
    }
}
- (IBAction)btnOptionsPressed:(UIButton *)sender {
    currentSelectedIndex = sender.tag-1;
    [self presentAll:YES];
}

-(void)handleLongCellPress:(UILongPressGestureRecognizer *)gestureRecognizer
{

     CGPoint p = [gestureRecognizer locationInView:self.collectionHome];
    NSIndexPath *indexPath = [self.collectionHome indexPathForItemAtPoint:p];
    VideoViewCell *cell = (VideoViewCell *)[self.collectionHome cellForItemAtIndexPath:indexPath];
    NSLog(@"indexpath %@",indexPath);
    NSArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
    if(offlineArray.count <= 0 && APP_DELEGATE.hasInet){
        switch (gestureRecognizer.state) {
            case UIGestureRecognizerStateBegan:
                if(indexPath.item != 0){
                    _movingIndexPath = indexPath;
                    cellRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height, 200, 100);
                    cell.blueOverlay.hidden = NO;
                    
                    [_collectionHome beginInteractiveMovementForItemAtIndexPath:indexPath];
                }
                break;
            case UIGestureRecognizerStateChanged:
                if(indexPath.item != 0){
                    [self.collectionHome updateInteractiveMovementTargetPosition:p];
                }
                break;
            case UIGestureRecognizerStateEnded:
                if(indexPath.item == 0) {
                    VideoViewCell *cell = (VideoViewCell *)[self.collectionHome cellForItemAtIndexPath:_movingIndexPath];
                    cell.blueOverlay.hidden = YES;
                    [self.collectionHome cancelInteractiveMovement];
                } else if(indexPath.item > sharedManager.channelVideos.count){
                    [self.collectionHome cancelInteractiveMovement];
                }
                else {
                    cell.blueOverlay.hidden = YES;
                    _prevPost = indexPath.item-2;
                    _nextPost = indexPath.item;
                    [self.collectionHome endInteractiveMovement];
                    [self updateMyCorner];
                }
                break;
                
            default:
                [self.collectionHome cancelInteractiveMovement];
                break;
        }
    }
    
}

- (UIView *)customSnapshotFromView:(UIView *)inputView {
    
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}

-(void)presentAll:(BOOL)isForMycorner{
    int isCelebrity = [Utils userIsCeleb];
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* Share = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"share", @"")
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [self shareDialog];
                                [view dismissViewControllerAnimated:YES completion:nil];
                                
                            }];
    UIAlertAction *sharewithInApp = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"inbox_to_friend", @"")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         if(!isCelebrity) {
                                             VideoModel *tempVideo;
                                             tempVideo = [sharedManager.channelVideos objectAtIndex:currentSelectedIndex];
                                             [self showToshare:tempVideo.videoID];
                                             [view dismissViewControllerAnimated:YES completion:nil];
                                         }
                                     }];
    UIAlertAction *postOnFriendsCorner = [UIAlertAction
                                          actionWithTitle:NSLocalizedString(@"post_on_friend_corner", @"")
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              if(!isCelebrity) {
                                                  VideoModel *tempVideo;
                                                  tempVideo = [sharedManager.channelVideos objectAtIndex:currentSelectedIndex];
                                                  [self shareOnFriendsCorner:tempVideo.videoID];
                                                  [view dismissViewControllerAnimated:YES completion:nil];
                                              }
                                          }];
    
    UIAlertAction *downloadBeam = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"save_to_gallery", @"")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action){
                                       VideoModel *temp  = [sharedManager.channelVideos objectAtIndex:currentSelectedIndex];
                                       [Utils DownloadVideo:temp.video_link];
                                       [view dismissViewControllerAnimated:YES completion:nil];
                                   }];
    
    UIAlertAction* reportBeam = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"delete_beam", @"")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self DeleteBtn:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
    UIAlertAction *rebeam = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"rebeam", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 VideoModel *tempVideo;
                                 tempVideo = [sharedManager.channelVideos objectAtIndex:currentSelectedIndex];
                                 [self shareOnMyCorner:tempVideo.videoID];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                             }];
    UIAlertAction* blockP = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"edit_beam", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self editBeam:self];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"cancel", @"")
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    [view addAction:Share];
    [view addAction:rebeam];
    [view addAction:postOnFriendsCorner];
    [view addAction:sharewithInApp];
    [view addAction:downloadBeam];
    [view addAction:blockP];
    [view addAction:reportBeam];
    
    [view addAction:cancel];
    view.popoverPresentationController.sourceView = self.view;
    view.popoverPresentationController.sourceRect = cellRect;
    [self presentViewController:view animated:YES completion:nil];
}


#pragma mark Beam Actions
-(void)shareDialog{
    [self shareUrlCall];
}

- (IBAction)editBeam:(id)sender{
    VideoModel *tempVideos  = [sharedManager.channelVideos objectAtIndex:currentSelectedIndex];
    NSString *postIDs = tempVideos.videoID;
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.video_thumbnail = tempVideos.video_thumbnail_link;
    uploadController.postID = postIDs;
    uploadController.isEdit = YES;
    uploadController.caption = tempVideos.title;
    APP_DELEGATE.hasbeenEdited = TRUE;
    uploadController.privacyToShow = tempVideos.privacy;
    uploadController.replyContToShow = tempVideos.reply_count;
    [[self navigationController] pushViewController:uploadController animated:YES];
}

-(IBAction)DeleteBtn:(id)sender{
    VideoModel *tempVideos  = [sharedManager.channelVideos objectAtIndex:currentSelectedIndex];
    if (tempVideos.isLocal) {
    }
    else{
        [CustomLoading showAlertMessage];
        NSString *postIDs = tempVideos.videoID;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
        NSURL *url = [NSURL URLWithString:SERVER_URL];
        NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"deletePost",@"method",
                                  token,@"session_token",postIDs,@"post_id",nil];
        NSData *postData = [Utils encodeDictionary:postDict];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                [CustomLoading DismissAlertMessage];
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                int success = [[result objectForKey:@"success"] intValue];
                if(success == 1){
                    int c = [sharedManager._profile.beams_count intValue];
                    c--;
                    sharedManager._profile.beams_count = [NSString stringWithFormat:@"%d",c];
                    if(currentSelectedIndex < sharedManager.channelVideos.count)
                        [sharedManager.channelVideos removeObjectAtIndex:currentSelectedIndex];
                    
                    if ([sharedManager.channelVideos count] <= 1){
                        
                        flowLayout=[[UICollectionViewLeftAlignedLayout alloc]init];
                        //[self.collectionHome.collectionViewLayout invalidateLayout];
                        //self.collectionHome.collectionViewLayout = flowLayout;
                        //[self.collectionHome setCollectionViewLayout:flowLayout];
                        
                    }else{
                        
                        
                        self.collectionHome.collectionViewLayout = normalLayout;
                    }
                    
                    [self.collectionHome reloadData];
                }
            }
            else{
                [CustomLoading DismissAlertMessage];
            }
        }];
    }
}

#pragma mark Get Sharing Content
-(void)shareUrlCall{
    VideoModel *tempVideo;
    __block NSString *urlToShare;
    tempVideo = [sharedManager.channelVideos objectAtIndex:currentSelectedIndex];
    [self showShareSheet:tempVideo.beam_share_url];
    
    //    if(tempVideo.isThumbnailReq == 1){
    //        serviceParams = [NSMutableDictionary dictionary];
    //        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
    //        [serviceParams setValue:tempVideo.videoID forKey:@"post_id"];
    //        [serviceParams setValue:@"0" forKey:@"is_comment"];
    //
    //        [Utils downloadImageWithURL:[NSURL URLWithString:tempVideo.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
    //            if (succeeded) {
    //                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
    //                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
    //                NSData *imgData;
    //                imgData = UIImagePNGRepresentation(im);
    //                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
    //
    //                }name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
    //                    int flag = [[data objectForKey:@"success"] intValue];
    //                    if(flag){
    //                        urlToShare = (NSString *)[data objectForKey:@"deep_link"];
    //                        [self showShareSheet:urlToShare];
    //                    }
    //                } failure:^(NSError *error) {
    //                }];
    //            }
    //        }];
    //    }else{
    //        urlToShare =  [NSString stringWithFormat:@"%@", tempVideo.deep_link];
    //        [self showShareSheet:urlToShare];
    //        //
    //    }
}

-(void)showShareSheet:(NSString *)shareURL{
    NSURL *url = [NSURL URLWithString:shareURL];
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[url]
     applicationActivities:nil];
    controller.excludedActivityTypes = @[UIActivityTypePrint,
                                         UIActivityTypeMail,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop,
                                         @"com.apple.mobilenotes.SharingExtension",
                                         @"com.apple.reminders.RemindersEditorExtension",
                                         ];
    [self presentViewController:controller animated:YES completion:nil];
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            // user shared an item
//            NSLog(@"We used activity type%@", activityType);
            if([activityType isEqualToString:@"com.apple.UIKit.activity.CopyToPasteboard"]){
                [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
            }
        } else {
            // user cancelled
//            NSLog(@"We didn't want to share anything after all.");
        }
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}

- (IBAction)btnCopyLink:(id)sender {
    VideoModel *tempVideo;
    __block NSString *urlToShare;
    tempVideo = [sharedManager.channelVideos objectAtIndex:currentSelectedIndex];
    if(tempVideo.isThumbnailReq == 1){
        tempVideo.isThumbnailReq = 0;
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:tempVideo.videoID forKey:@"post_id"];
        [serviceParams setValue:@"0" forKey:@"is_comment"];
        
        [Utils downloadImageWithURL:[NSURL URLWithString:tempVideo.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                    
                }name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                    int flag = [[data objectForKey:@"success"] intValue];
                    if(flag){
                        urlToShare = (NSString *)[data objectForKey:@"deep_link"];
                        UIPasteboard *pb = [UIPasteboard generalPasteboard];
                        [pb setString: [NSString stringWithFormat:@"%@", urlToShare ]];
                        [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
                    }
                } failure:^(NSError *error) {
                    
                }];
            }
        }];
    }else{
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString: [NSString stringWithFormat:@"%@", tempVideo.deep_link ]];
        [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
    }
}

- (IBAction)shareOnFb:(id)sender {
    VideoModel *tempVideo;
    __block NSString *urlToShare;
    tempVideo = [sharedManager.channelVideos objectAtIndex:currentSelectedIndex];
    if(tempVideo.isThumbnailReq == 1){
        tempVideo.isThumbnailReq = 0;
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:tempVideo.videoID forKey:@"post_id"];
        [serviceParams setValue:@"0" forKey:@"is_comment"];
        
        [Utils downloadImageWithURL:[NSURL URLWithString:tempVideo.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                }
                                 name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                                     
                                     int flag = [[data objectForKey:@"success"] intValue];
                                     if(flag){
                                         urlToShare = (NSString *)[data objectForKey:@"deep_link"];
                                         FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                                         content.contentURL = [NSURL URLWithString:urlToShare];
                                         //content.imageURL   = [NSURL URLWithString:urlToShare];
                                         content.contentTitle = tempVideo.userName;
                                         content.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
                                         
                                         FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
                                         dialog.fromViewController = self;
                                         dialog.shareContent = content;
                                         dialog.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
                                         if (![dialog canShow]) {
                                             // fallback presentation when there is no FB app
                                             dialog.mode = FBSDKShareDialogModeFeedBrowser;
                                         }
                                         [dialog show];
                                     }
                                 } failure:^(NSError *error) {
                                     
                                 }];
            }
        }];
    }else{
        FBSDKShareLinkContent *contentw = [[FBSDKShareLinkContent alloc] init];
        contentw.contentURL = [NSURL URLWithString:tempVideo.deep_link];
        //content.imageURL   = [NSURL URLWithString:urlToShare];
        contentw.contentTitle = tempVideo.userName;
        contentw.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
        FBSDKShareDialog *dialogw = [[FBSDKShareDialog alloc] init];
        dialogw.fromViewController = self;
        dialogw.shareContent = contentw;
        dialogw.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
        if (![dialogw canShow]) {
            // fallback presentation when there is no FB app
            dialogw.mode = FBSDKShareDialogModeFeedBrowser;
        }
        [dialogw show];
    }
}

- (IBAction)shareOnTwitter:(id)sender {
    VideoModel *tempVideos;
    tempVideos = [sharedManager.channelVideos objectAtIndex:currentSelectedIndex];
    NSURL *imageURL = [NSURL URLWithString:tempVideos.video_thumbnail_link];
    [CustomLoading showAlertMessage];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            UIImage *thumbnailImg = [UIImage imageWithData:imageData];
            [CustomLoading DismissAlertMessage];
            
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                [mySLComposerSheet setInitialText:@"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech."];
                [mySLComposerSheet addURL:[NSURL URLWithString:tempVideos.beam_share_url]];
                [mySLComposerSheet addImage:thumbnailImg];
                [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                    
                    switch (result) {
                        case SLComposeViewControllerResultCancelled:
                            NSLog(@"Post Canceled");
                            break;
                        case SLComposeViewControllerResultDone:
                            NSLog(@"Post Sucessful");
                            break;
                            
                        default:
                            break;
                    }
                }];
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            else {
                
                NSString *message;
                NSString *title;
                message = NSLocalizedString(@"setupTwitterAccount", @"");
                title   = NSLocalizedString(@"accountConfiguration", @"");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                               delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                                      otherButtonTitles:nil, nil];
                [alert show];
            }
        });
    });
}


#pragma mark Get Comments

-(UIViewController*) topMostController
{
    NSArray *controllers = [self.navigationController viewControllers];

    return [controllers lastObject];
}

-(void) ShowCommentspressed:(UIButton *)sender{
    UIButton *CommentsBtn = (UIButton *)sender;
    currentIndexHome = CommentsBtn.tag;
    VideoModel *_model = [sharedManager.channelVideos objectAtIndex:currentIndexHome];
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    
    commentController.commentsObj   = Nil;
    commentController.postArray     = _model;
    commentController.cPostId       =  _model.videoID;
    [[NSUserDefaults standardUserDefaults] setObject:_model.videoID forKey:@"commentparentID"];
    commentController.isFirstComment = TRUE;
    commentController.isComment     = FALSE;
    
    if(![[self topMostController] isKindOfClass:[CommentsVC class]])
    {
        [[self navigationController] pushViewController:commentController animated:YES];
    }
}

#pragma mark Show Friends
- (IBAction)showFriends:(id)sender {
    FriendsVC *commentController;    //= [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    
    commentController.friendsArray = [Utils getSortedArray:sharedManager.followers];
//    sharedManager.followers = commentController.friendsArray;
    commentController.friendsDict = [Utils getSortedDictionary:sharedManager.followers];
    //commentController.friendsArray  = [sharedManager.followers mutableCopy];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.isSearch = true;
    commentController.loadFollowings= 2;
    commentController.userId        = @"";
    commentController.shudFetchFromServerOnly = YES;

    [[self navigationController] pushViewController:commentController animated:YES];
}

#pragma mark Show Filtered Friends For Celeb
//-(IBAction)showFilteredFollowers:(id)sender{
//    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
////    if (IS_IPHONEX){
////        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
////    }else{
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
////    }
//    commentController.friendsArray  = [[self getFilteredFollowers] mutableCopy];
//    commentController.titles        = NSLocalizedString(@"followers", @"");
//    commentController.NoFriends     = FALSE;
//    commentController.loadFollowings= 2;
//    commentController.userId        = @"";
//    [[self navigationController] pushViewController:commentController animated:YES];
//}

- (IBAction)showFilteredFollowers:(id)sender {
    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    //    if (IS_IPHONEX){
    //        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
    //    }else{
    commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    //    }
    commentController.friendsArray  = nil;
    commentController.titles        = NSLocalizedString(@"followers", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= FALSE;
    commentController.userId        = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"id"];
    [[self navigationController] pushViewController:commentController animated:YES];
}

#pragma mark Action Sheets Action
-(void)showToshare:(NSString *)pOstID{
    FriendsVC *commentController;  //  = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = [Utils getFriendsToShareBeam];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 5;
    commentController.userId        = @"";
    commentController.postId = pOstID;
    commentController.isCommentVideo = @"0";
    commentController.isLocalSearch = YES;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(void)shareOnFriendsCorner:(NSString *)postId{
    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = [Utils getFriendsToShareBeam];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 6;
    commentController.userId        = @"";
    commentController.postId = postId;
    commentController.isCommentVideo = @"0";
    commentController.isLocalSearch = YES;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(void)shareOnMyCorner:(NSString *)pOstID{
    [Utils shareBeamOnCorner:pOstID isComment:@"0" friendId:@"" sharingOnFriendsCorner:NO];
}

#pragma mark ---

-(IBAction)showfilterFriends:(id)sender{
    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = [[self getFilteredFriends] mutableCopy];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 2;
    commentController.userId        = @"";
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(NSMutableArray *)getFilteredFriends{
    NSMutableArray *filteredArray = [[NSMutableArray alloc] init];
    for (int i =0; i< sharedManager.followers.count ; i++){
        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
        if([tempObj.status isEqualToString:@"FRIEND"] || [tempObj.status isEqualToString:@"PENDING"] ){
            [filteredArray addObject:tempObj];
        }
    }
    return filteredArray;
}

-(NSMutableArray *)getFilteredFollowers{
    NSMutableArray *filteredArray = [[NSMutableArray alloc] init];
    for (int i =0; i< sharedManager.followers.count ; i++){
        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
        if(tempObj.isFollowed == 1){
            [filteredArray addObject:tempObj];
        }
    }
    return filteredArray;
}

-(NSMutableArray *)getSortedArray:(NSMutableArray *)arrayTobeSorted{
    NSMutableArray *respondArray = [[NSMutableArray alloc] init];
    NSMutableArray *cancelArray = [[NSMutableArray alloc] init];
    NSMutableArray *addFriendArray = [[NSMutableArray alloc] init];
    NSMutableArray *friendArr = [[NSMutableArray alloc] init];
    NSArray *items = @[@"ACCEPT_REQUEST", @"PENDING", @"FRIEND",@"ADD_FRIEND"];
    for(int i = 0; i < arrayTobeSorted.count; i++){
        Followings *tempObj = [arrayTobeSorted objectAtIndex:i];
        NSInteger item = [items indexOfObject:tempObj.status];
        switch (item) {
            case 0:
                [respondArray   addObject:tempObj];
                break;
            case 1:
                [cancelArray    addObject:tempObj];
                break;
            case 2:
                [friendArr      addObject:tempObj];
                break;
            case 3:
                [addFriendArray addObject:tempObj];
                break;
            default:
                break;
        }
    }
    NSMutableArray *parentArray = [[NSMutableArray alloc] init];
    if(respondArray.count > 0)
        [parentArray addObjectsFromArray:respondArray];
    if(cancelArray.count > 0)
        [parentArray addObjectsFromArray:cancelArray];
    if(addFriendArray.count > 0)
        [parentArray addObjectsFromArray:addFriendArray];
    if(friendArr.count > 0)
        [parentArray addObjectsFromArray:friendArr];
    return [parentArray mutableCopy];
}

-(IBAction)editProfilePressed:(id)sender{
    [[NavigationHandler getInstance]MoveToProfile];
}

-(void)parseVideoResponse:(NSDictionary*) result posts:(NSDictionary*)posts coreManager:(CoreDataManager*)coreMngr{
    NSArray *tempArray = [result objectForKey:@"posts"];
    sharedManager._profile = [[myChannelModel alloc] init];
    if(sharedManager.myCornerPageNum == 1){
        NSArray *freidns = [result objectForKey:@"friends"];
        sharedManager.followers = [[NSMutableArray alloc] init];
        if([freidns isKindOfClass:[NSArray class]]){
            for(NSDictionary *tempDict in freidns){
                Followings *_responseData = [[Followings alloc] init];
                _responseData.f_id = [tempDict objectForKey:@"id"];
                _responseData.fullName = [tempDict objectForKey:@"full_name"];
                _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                _responseData.status = [tempDict objectForKey:@"state"];
                _responseData.isFollowed = [[tempDict objectForKey:@"is_followed"] intValue];
                [sharedManager.followers addObject:_responseData];
            }
        }
    }
    sharedManager._profile.beams_count = [posts objectForKey:@"beams_count"];
    sharedManager._profile.friends_count = [posts objectForKey:@"following_count"];
    sharedManager._profile.full_name = [posts objectForKey:@"full_name"];
    sharedManager._profile.user_id = [posts objectForKey:@"id"];
    sharedManager._profile.profile_image = [posts objectForKey:@"profile_link"];
    sharedManager._profile.likes_count = [posts objectForKey:@"followers_count"];
    sharedManager._profile.gender = [posts objectForKey:@"gender"];
    sharedManager._profile.email = [posts objectForKey:@"email"];
    sharedManager._profile.is_celeb = [posts objectForKey:@"is_celeb"];
    sharedManager._profile.cover_image = [posts objectForKey:@"cover_link"];
    sharedManager._profile.inboxCount = [posts objectForKey:@"inbox_count"];
    sharedManager._profile.unreadInboxCount = [posts objectForKey:@"unread_inbox_count"];
    sharedManager._profile.pendingReqsCount = [posts objectForKey:@"new_requests_count"];
    sharedManager._profile.accountType = [posts objectForKey:@"account_type"];
    NSString *bio = [posts objectForKey:@"bio"];
    if((NSString *)[NSNull null] != bio) {
        sharedManager._profile.bio = bio;
    } else {
        sharedManager._profile.bio = @"";
    }
    if([posts valueForKey:@"total_beams_views"] != nil) {
        // The key existed...
        sharedManager._profile.profile_view_count = [posts objectForKey:@"total_beams_views"];
    }
    sharedManager._profile.profileDeepLink = [posts objectForKey:@"deep_link"];
    sharedManager._profile.inviteUrl = [posts objectForKey:@"invite_url"];
    sharedManager._profile.inviteText = [posts objectForKey:@"invite_text"];
    BOOL isC = [sharedManager._profile.is_celeb boolValue];
    [[NSUserDefaults standardUserDefaults] setBool:isC forKey:@"isCeleb"];
    [[NSUserDefaults standardUserDefaults] setObject:sharedManager._profile.profile_image forKey:@"user_img"];
    [[NSUserDefaults standardUserDefaults] setObject:sharedManager._profile.full_name forKey:@"User_Name"];
    name = sharedManager._profile.full_name;
    if(sharedManager._profile.userName != nil)
    {
        [[NSUserDefaults standardUserDefaults] setObject:sharedManager._profile.userName forKey:@"User_Name"];
    }
    [[NSUserDefaults standardUserDefaults] setObject:sharedManager._profile.profile_image forKey:@"User_Img"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSArray *chPostArray = [NSArray new];
    if(tempArray.count == 0 && sharedManager.myCornerPageNum == 1 && sharedManager.channelVideos.count == 0){
        NSLog(@"array being cleared 1");
        sharedManager.channelVideos = [[NSMutableArray alloc] init];
    }
    if(tempArray.count > 0 || coreMngr.offlineContentArray.count>0)
    {
        chPostArray = [result objectForKey:@"posts"];
        if(sharedManager.myCornerPageNum == 1){
//            if(sharedManager.channelVideos.count == 0)
            {
                NSLog(@"array being cleared 2");
                sharedManager.channelVideos = [[NSMutableArray alloc] init];
            }
            //                        [[NSNotificationCenter defaultCenter]
            //                         postNotificationName:@"uploadData"
            //                         object:nil];
        }
        
        for(NSDictionary *tempDict in chPostArray){
            
            VideoModel *_Videos = [[VideoModel alloc] initWithDictionary:tempDict];
            bool found = false;
            
            for(VideoModel *eachVid in sharedManager.channelVideos)
            {
                if([eachVid.videoID isEqualToString:_Videos.videoID])
                {
                    found = true;
                }
            }
            if(!found)
            {
                [sharedManager.channelVideos addObject:_Videos];
            }
        }
        
        
//        ///////////////////////////MERGING OFFLINE BEAMS IN ONLINE ARRAY
//
//        NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
//        for (int i = 0; i < offlineArray.count; i++)
//        {
//            OfflineDataModel *offlineVideoModel = offlineArray[i];
//            VideoModel *_Videos     = [[VideoModel alloc] init];
//
//            _Videos.video_thumbnail_link = offlineVideoModel.thumbnail;
//            _Videos.video_link = offlineVideoModel.data;
//            _Videos.is_anonymous = offlineVideoModel.anonyCheck;
//            _Videos.uploaded_date = offlineVideoModel.currentTime;
//            _Videos.comments_count = @"0";
//            _Videos.current_datetime = offlineVideoModel.currentTime;
//            // _Videos.uploaded_date = offlineVideoModel.
//            [sharedManager.channelVideos insertObject:_Videos atIndex:0];
//        }
        self.fetchingContent = FALSE;

        if(!self.fetchingContent){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionHome reloadData];
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    //                        NSLog(@"Do some work");
                    if(!isScrolling && !self.fetchingContent)
                    {
                        [self startGifOnCell];
                    }
                    //                        [self myTestProcess];
                });
                
            });
        }
        
    }
    else {
        //                    self.collectionHome.tableFooterView = nil;
        cannotScrollMyCorner = YES;
        self.fetchingContent = FALSE;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        //[self.collectionHome reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self.collectionHome reloadItemsAtIndexPaths:indexPaths];
//        });
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionHome reloadData];
            
        });
        
    }
    
    
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - uicollectionViewDelegates

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float returnValue;
    if(indexPath.row == 0) {
        if(IS_IPAD){
            returnValue = 320;
        } else {
            returnValue = 315;
        }
    }
    else{
       if (IS_IPHONE_5)
        returnValue = ((collectionView.frame.size.width-12)/3) ;
        else
        returnValue = ((collectionView.frame.size.width-12)/3) ;
    }

    
    if (indexPath.row == 0){
        
        return CGSizeMake(collectionView.frame.size.width, returnValue);
    
    }else{
        _progressViewFrame = returnValue;
        if(sharedManager.isListView) {
            return CGSizeMake(_collectionHome.frame.size.width, 250);
        }
        return CGSizeMake(returnValue, returnValue);
    
    }

}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 6.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
//    if(sharedManager.isFromBackground) {
//
//        sharedManager.isFromBackground = NO;
//        return offlineArray.count+1;
//    }
//    if ([sharedManager.channelVideos count] == 1){
//
//        return [sharedManager.channelVideos count]+2;
//    }
    
    
    return [sharedManager.channelVideos count]+1;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"collectionCell";
    static NSString *CornerHeader = @"collectionHead";
//    if(indexPath.row > 1 && [sharedManager.channelVideos count] == 1){
//
//        EmptyCollectionCell *emptyCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"emptyCell" forIndexPath:indexPath];
//        return emptyCell;
//
//    }else
    if( indexPath.row == 0){
    
        HeaderCollectionCell *chCell;
        
        chCell  = [collectionView dequeueReusableCellWithReuseIdentifier:CornerHeader forIndexPath:indexPath];
        
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
            {
                chCell.separatorView.backgroundColor = [UIColor lightGrayColor];
            }
            else
            {
                chCell.separatorView.backgroundColor = [UIColor whiteColor];
            }
        }
        else
        {
            chCell.separatorView.backgroundColor = [UIColor whiteColor];
        }
        
        if(IS_IPHONE_5) {
            chCell.celebUnreadCount.frame = CGRectMake(245, chCell.celebUnreadCount.frame.origin.y, chCell.celebUnreadCount.frame.size.width, chCell.celebUnreadCount.frame.size.height);
             chCell.celebDotforInboxNotifications.frame = CGRectMake(245, chCell.celebDotforInboxNotifications.frame.origin.y, chCell.celebDotforInboxNotifications.frame.size.width, chCell.celebDotforInboxNotifications.frame.size.height);
        }
        self.userImg = chCell.profileImage;
        [chCell.zoomBtn addTarget:self action:@selector(zoomImage:) forControlEvents:UIControlEventTouchUpInside];
        if(_firstTime) {
//            CAGradientLayer *layer = [CAGradientLayer layer];
//            layer.colors = @[(id)[UIColor clearColor].CGColor, (id)[UIColor blackColor].CGColor];
//            layer.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, chCell.cover.frame.size.height);
//            [chCell.cover.layer addSublayer:layer];
            _firstTime = NO;
        }
        //}
        if(sharedManager.isListView) {
            chCell.gridImage.image = [UIImage imageNamed:@"Grid"];
            chCell.gridListLbl.textColor = [UIColor lightGrayColor];
        } else {
            chCell.gridImage.image = [UIImage imageNamed:@"Grid_blue"];
            chCell.gridListLbl.textColor = [UIColor colorWithRed:54/255.0 green:78/255.0 blue:141/255.0 alpha:1.0];
        }
        int isCelebrity = [Utils userIsCeleb];
        if(isCelebrity){
            chCell.blueProfileView.hidden = NO;
            chCell.tickImg.hidden = NO;
            chCell.verifiedLbl.hidden = NO;
            chCell.celebBottomView.hidden = NO;
            chCell.normalUserBottomView.hidden = YES;
            
            [chCell.celebFollowersBtn addTarget:self action:@selector(showFilteredFollowers:) forControlEvents:UIControlEventTouchUpInside];
            [chCell.celebFriendsBtn addTarget:self action:@selector(showfilterFriends:) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
            chCell.blueProfileView.hidden = YES;
            chCell.tickImg.hidden = YES;
            chCell.verifiedLbl.hidden = YES;
            chCell.celebBottomView.hidden = YES;
            chCell.normalUserBottomView.hidden = NO;
        }
        if(sharedManager._profile == Nil){
            if(![Utils userIsCeleb]){
                chCell.celebBeamsBtn.hidden = YES;
//                chCell.noOfFriendslbl.text = [Utils getchachedFriendsCount];
//                chCell.noOfBeamslbl.text = [Utils getcachedBeamsCount];
                
                chCell.noOfFriendslbl.text = [self getfriendsCount];
                chCell.noOfBeamslbl.text = [Utils abbreviateNumber:sharedManager._profile.beams_count withDecimal:1];

                
                
                chCell.inboxCount.text = [Utils getCachedInboxCount];
                if(![[Utils getCachedPendingrReqCount] isEqualToString:@"0"]){
                    chCell.pendingReqCount.hidden = NO;
                    chCell.pendingReqCount.text = [Utils getCachedPendingrReqCount];
                }else{
                    chCell.pendingReqCount.hidden = YES;
                }
                chCell.normalUserBottomView.hidden = NO;
            }else{
                chCell.blueProfileView.hidden = NO;
                chCell.tickImg.hidden = NO;
                chCell.verifiedLbl.hidden = NO;
                chCell.cFriendslbl.text = [Utils getchachedFriendsCount];
                chCell.cBeamslbl.text =   [Utils getCachedViewsCount];
                chCell.cFollowersLbl.text = [Utils getCachedFollowersCount];
                chCell.normalUserBottomView.hidden = YES;
                chCell.celebBeamsBtn.hidden = NO;
            }
            [chCell.cover yy_setImageWithURL:[NSURL URLWithString:sharedManager._profile.cover_image] placeholder:[UIImage imageNamed:@"cover_holder"]];
            chCell.bioLbl.text = sharedManager._profile.bio;
        }else{
            [chCell.cover yy_setImageWithURL:[NSURL URLWithString:sharedManager._profile.cover_image] placeholder:[UIImage imageNamed:@"cover_holder"]];
            NSString *bCount = [Utils abbreviateNumber:sharedManager._profile.profile_view_count withDecimal:1];
            NSString *beamsC = [Utils abbreviateNumber:sharedManager._profile.beams_count withDecimal:1];
            NSString *inboxC = [Utils abbreviateNumber:sharedManager._profile.inboxCount withDecimal:1];
            NSString *pendingReqC = [Utils abbreviateNumber:sharedManager._profile.pendingReqsCount withDecimal:1];
            
            if(![pendingReqC isEqualToString:@"0"] && ![pendingReqC isEqual: [NSNull null]]){
                chCell.pendingReqCount.hidden = NO;
                chCell.pendingReqCount.text = pendingReqC;
            }else{
                chCell.pendingReqCount.hidden = YES;
                chCell.pendingReqCount.text = pendingReqC;
            }
            [Utils setCachedPendingReqCount:pendingReqC];
            [Utils setCachedBeamsCount:beamsC];
            [Utils setcachedViewsCount:bCount];
            [Utils setCachedInboxCount:inboxC];
            if([sharedManager._profile.beams_count integerValue] == 1){
                chCell.noOfBeamslbl.text = [[NSString alloc]initWithFormat:@"%@",beamsC];
                chCell.cBeamslbl.text = [[NSString alloc]initWithFormat:@"%@",bCount];
                chCell.celebCount.text = [[NSString alloc] initWithFormat:@"%@",beamsC]; /// celeb beams count
            }
            else{
                chCell.noOfBeamslbl.text = [[NSString alloc]initWithFormat:@"%@",beamsC];
                chCell.cBeamslbl.text = [[NSString alloc]initWithFormat:@"%@",bCount];
                chCell.celebCount.text = [[NSString alloc] initWithFormat:@"%@",beamsC]; /// celeb beams count
            }
            if([[self getfriendsCount] intValue] == 1){
                chCell.noOfFriendslbl.text = [NSString stringWithFormat:@"%@",[self getfriendsCount]];
                chCell.cFriendslbl.text = [NSString stringWithFormat:@"%@",[self getfriendsCount]];
            }else{
                chCell.noOfFriendslbl.text = [NSString stringWithFormat:@"%@",[self getfriendsCount]];
                chCell.cFriendslbl.text = [NSString stringWithFormat:@"%@",[self getfriendsCount]];
            }
            if([[self getFollowersCount] intValue]== 1){
                chCell.cFollowersLbl.text = [NSString stringWithFormat:@"%@",[self getFollowersCount]];
            }else{
                chCell.cFollowersLbl.text = [NSString stringWithFormat:@"%@",[self getFollowersCount]];
            }
            
            if(![pendingReqC isEqualToString:@"0"] && ![pendingReqC isEqual: [NSNull null]] && !sharedManager.changeFriendCount){
               
                [chCell.noOfFriendslbl addSubview:badge];
                badge.badgeValue = [pendingReqC intValue];
            } else {
                sharedManager.changeFriendCount = NO;
                [badge removeFromSuperview];
            }
            chCell.inboxCount.text = inboxC;
            CGRect labelRect = [inboxC
                                boundingRectWithSize:chCell.inboxCount.frame.size
                                options:NSStringDrawingUsesLineFragmentOrigin
                                attributes:@{
                                             NSFontAttributeName : chCell.inboxCount.font
                                             }
                                context:nil];
            
            CGRect newFrame = chCell.inboxCount.frame;
            CGFloat point = newFrame.size.width - labelRect.size.width;
            if(labelRect.size.width > 13){
                point += 10;
            }
            if(labelRect.size.width > 25){
                point += 25;
            }
            
//            chCell.dotforInboxNotifications.frame = CGRectMake(point, chCell.dotforInboxNotifications.frame.origin.y, 6, 6);
            
        }
        int unreadCount = [sharedManager._profile.unreadInboxCount intValue];
        if(unreadCount > 0){
            if(isCelebrity) {
                [chCell.celebDotforInboxNotifications setHidden:NO];
                chCell.celebUnreadCount.hidden = NO;
                chCell.celebUnreadCount.text = [NSString stringWithFormat:@"%d",unreadCount];
            } else {
                [chCell.dotforInboxNotifications setHidden:NO];
                chCell.inboxCount.text = [NSString stringWithFormat:@"%d",unreadCount];
            }
        } else {
            if(isCelebrity) {
                [chCell.celebDotforInboxNotifications setHidden:YES];
                chCell.celebUnreadCount.hidden = YES;
            } else {
                [chCell.dotforInboxNotifications setHidden:YES];
                chCell.inboxCount.text = @"0";
            }
        }
        chCell.nameLbl.text = name;
        //[chCell.beamsPressed addTarget:self action:@selector(takeToArchive:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.inboxPressed addTarget:self action:@selector(taketoInbox:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.inboxBtn addTarget:self action:@selector(taketoInbox:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.friendsPressed addTarget:self action:@selector(showFriends:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.editProfileBtn addTarget:self action:@selector(editProfilePressed:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.changeDp addTarget:self action:@selector(changeProfilePic:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.gridListBtn addTarget:self action:@selector(gridListBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        chCell.viewToRound.layer.cornerRadius = chCell.viewToRound.frame.size.width / 4;
        chCell.viewToRound.layer.masksToBounds = YES;
        chCell.viewToRound.clipsToBounds = YES;
        chCell.picBorder.layer.cornerRadius = chCell.picBorder.frame.size.width / 4;
        chCell.picBorder.clipsToBounds = YES;
        chCell.picBorder.layer.masksToBounds = NO;
        
        if(justChangedDp){
            chCell.profileImage.image = chosenImage;
            [self saveImage:chosenImage];
        }
        else {
            UIImage *placeholderImg = [self loadImage];
            if(!placeholderImg){
                placeholderImg = [UIImage imageNamed:@"place_holder"];
            }
            [chCell.profileImage sd_setImageWithURL:[NSURL URLWithString:sharedManager._profile.profile_image] placeholderImage:placeholderImg completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [self saveImage:image];
            }];
        }
        
       
        chCell.bioLbl.text = [Utils decodeForEmojis:sharedManager._profile.bio];
        chCell.bioLbl.text = [Utils decodeForEmojis:chCell.bioLbl.text];

        [chCell setBackgroundColor:[UIColor clearColor]];
        //chCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return chCell;
        
    }else{
        
        VideoViewCell *cell;
        currentIndexHome = indexPath.row - 1;
        cell  = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [sharedManager.channelVideos objectAtIndex:currentIndexHome];
        cell.representedObject = tempVideos;
       
        if(sharedManager.isListView) {
            cell.CH_userName.font = [UIFont fontWithName:@"Montserrat-Regular" size:11];
            cell.CH_VideoTitle.font = [UIFont fontWithName:@"Montserrat-Regular" size:11];
            cell.durationLbl.hidden = NO;
            cell.durationLbl.text = tempVideos.video_length;
            cell.dotsImg.hidden = NO;
            [cell.btnOptions addTarget:self action:@selector(btnOptionsPressed:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnOptions.tag = indexPath.item;
            
        } else {
            cell.CH_userName.font = [UIFont fontWithName:@"Montserrat-Regular" size:9];
            cell.CH_VideoTitle.font = [UIFont fontWithName:@"Montserrat-Regular" size:9];
            cell.durationLbl.hidden = YES;
            cell.dotsImg.hidden = YES;
        }

        
        if(tempVideos.beamType == 1){
            if(sharedManager.isListView) {
                cell.listRebeam.hidden = NO;
                cell.rebeam1.hidden = YES;
            } else {
                cell.listRebeam.hidden = YES;
                cell.rebeam1.hidden = NO;
            }
        }
        else{
            cell.listRebeam.hidden = YES;
            cell.rebeam1.hidden = YES;
        }
        
        cell.CH_userName.text = [Utils decodeForEmojis:tempVideos.title];
        cell.CH_userName.text = [Utils decodeForEmojis:cell.CH_userName.text];
        
        if(cell.CH_userName.text == nil)
        {
            cell.CH_userName.text = [Utils decodeForEmojis:tempVideos.title];
        }

        if([cell.CH_userName.text isEqualToString:@"Anonymous"]) {
            cell.CH_userName.text = NSLocalizedString(@"anonymous_small", nil);
        }
        cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
        if([cell.CH_VideoTitle.text rangeOfString:@"null"].length > 0)
        {
            cell.CH_VideoTitle.text = [Utils returnDateForInProgressOfMyCorner:tempVideos.uploaded_date];
        }
        cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.CH_CommentscountLbl.hidden = YES;
            cell.leftreplImg.hidden = YES;
        }
        else{
            cell.CH_CommentscountLbl.hidden = NO;
            cell.leftreplImg.hidden = NO;
            cell.CH_CommentscountLbl.text = tempVideos.comments_count;
        }
        
        cell.CH_heartCountlbl.text = tempVideos.like_count;
        cell.CH_seen.text = tempVideos.seen_count;
        cell.Ch_videoLength.text = tempVideos.video_length;
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.annonyOverlayLeft.hidden = YES;
            cell.annonyImgLeft.hidden = YES;
            //cell.CH_userName.text = @"Anonymous";
            cell.dummyLeft1.hidden  = YES;
        }
        else{
            //        cell.dummyLeft1.image = [UIImage imageNamed:@"btanonymous.png"];
            cell.annonyOverlayLeft.hidden = NO;
            cell.annonyImgLeft.hidden = NO;
            //cell.CH_userName.text = @"Anonymous";
            cell.dummyLeft1.hidden  = NO;
        }
        if(tempVideos.beamType == 1){
            cell.CH_heart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            cell.CH_heart.hidden = YES;
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];
        }
        else{
            cell.CH_heart.hidden = YES;
        }
        cell.mainThumbnail.hidden = NO;
        cell.CH_Video_Thumbnail.hidden = YES;
        NSData *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"myCornerThumbnail"];
        cell.mainThumbnail.hidden = NO;
        
        ///////// GIF Image Download ////////
        
        // first download thumbnail and then download gif if it exists
        
//        [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage: [UIImage imageWithData:temp]];
//        [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage: [UIImage imageWithContentsOfFile:tempVideos.video_thumbnail_link]];
        [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (!error) {
                tempVideos.imageThumbnail = image;
                if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
                    
   
                    if (tempVideos.imageThumbnail && tempVideos.animatedImage) {
                        cell.mainThumbnail.hidden = YES;
                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                        cell.CH_Video_Thumbnail.hidden = NO;
                    }else if (tempVideos.imageThumbnail && !tempVideos.animatedImage) {
                        [cell.mainThumbnail setImage:tempVideos.imageThumbnail];
                        
                        
                        
                        
//                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                            
//                            [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                            }];
//                            
//                        });
                        
                        
                        
                        
                    }else if (!tempVideos.imageThumbnail && tempVideos.animatedImage) {
                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                        cell.CH_Video_Thumbnail.hidden = NO;
                    }else if (!tempVideos.imageThumbnail && !tempVideos.animatedImage) {
                        
                        [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            if (!error) {
                                
                                tempVideos.imageThumbnail = image;
//                                [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
//                                     cell.mainThumbnail.hidden = NO;
//                                    tempVideos.animatedImage = result.animatedImage;
//                                    [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
//                                    cell.mainThumbnail.hidden = NO;
//                                    cell.CH_Video_Thumbnail.hidden = NO;
//                                }];
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    
                                    [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            
                                            cell.mainThumbnail.hidden = YES;
                                            cell.CH_Video_Thumbnail.hidden = NO;
                                            [cell.CH_Video_Thumbnail startAnimating];
                                        });
                                        
                                    }];
                                    
                                });
                            }else{
//                                [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
//                                    cell.mainThumbnail.hidden = NO;
//                                    tempVideos.animatedImage = result.animatedImage;
//                                    [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
//                                    cell.mainThumbnail.hidden = NO;
//                                    cell.CH_Video_Thumbnail.hidden = NO;
//                                }];
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    
                                    [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            
                                            cell.mainThumbnail.hidden = YES;
                                            cell.CH_Video_Thumbnail.hidden = NO;
                                            [cell.CH_Video_Thumbnail startAnimating];
                                        });
                                        
                                    }];
                                    
                                });
                            }
                        }];
                        
                    
                        
                    }
                    
                }else{
                    
                    
                    cell.mainThumbnail.hidden = NO;
                    cell.CH_Video_Thumbnail.hidden = YES;
                    
                    [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if (!error) {
                            tempVideos.imageThumbnail = image;
                           
                        }else{
                            
                            NSLog(@"%@", error);
                            
                        }
                      
                    }];
                  
                }
              
            }else{
                
                NSLog(@"%@", error);
                
            }
            
        }];
        
        //////////////////////
  
        cell.CH_commentsBtn.enabled = YES;
        if (tempVideos.isLocal && !APP_DELEGATE.hasInet) {
            [cell.cellProgress removeFromSuperview];
            cell.CH_commentsBtn.backgroundColor = [UIColor colorWithRed:111/255 green:113/255 blue:121/255 alpha:1];
            cell.CH_commentsBtn.alpha = 0.55;
//            NSString *dateToParse = [NSString stringWithFormat:@"%@:00",tempVideos.current_datetime];
//            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//            dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
//            NSDate *yourDate = [dateFormatter dateFromString:dateToParse];
            cell.CH_VideoTitle.text = tempVideos.current_datetime;
            cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
            cell.CH_CommentscountLbl.text = @"!";
            cell.CH_CommentscountLbl.hidden = NO;
            cell.leftreplImg.hidden =  NO;
            cell.CH_commentsBtn.enabled = NO;
            cell.mainThumbnail.hidden = NO;
            if(_offlineThumbnailArray.count != 0 && currentIndexHome < _offlineThumbnailArray.count)
            cell.mainThumbnail.image = [UIImage imageWithData:_offlineThumbnailArray[currentIndexHome]];
            if (cell.cellProgress!=nil){
                
                cell.cellProgress.hidden = true;
                [cell.cellProgress removeFromSuperview];
                cell.cellProgress = nil;
            }
        }
        else if(tempVideos.isLocal){
 
            cell.CH_commentsBtn.enabled = NO;
            cell.leftreplImg.hidden =  YES;
            cell.CH_VideoTitle.text = tempVideos.current_datetime;
            cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
            
            if(![cell.cellProgress isDescendantOfView:cell.contentView]) {
                if(!sharedManager.isListView) {
//                    if(IS_IPHONE_5) {
//                        cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, 95, 90)];
//                    } else if(IS_IPAD){
//                        cell.cellProgress = [[PWProgressView alloc] init];
//                        cell.cellProgress.frame = cell.view1.frame;
//                    } else if(IS_IPHONE_6Plus ){
//                        cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, 126, 121)];
//                    }
//                    else {
                        cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, ((collectionView.frame.size.width-12)/3) - 8, ((collectionView.frame.size.width-12)/3) - 16)];
//                    }
                    if(IS_IPHONE_6Plus) {
                        cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 7.0f;
                    } else{
                        cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 6.2f;
                    }
                } else {
                    if(IS_IPHONE_6Plus) {
                        cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x-1.5, cell.view1.frame.origin.y-1.5, _collectionHome.frame.size.width - 6, 238)];
                        cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 19.0f;

                    } else {
                        cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x-1.5, cell.view1.frame.origin.y-1.5, _collectionHome.frame.size.width - 6, 238)];
                        cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 15.4f;
                    }
                }
                cell.cellProgress.clipsToBounds = YES;
                
                cell.cellProgress.hidden = false;
                [cell.contentView addSubview:cell.cellProgress];
                 NSArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
                if(offlineArray.count <= 0)
                    [cell.cellProgress setProgress:_progressViewValue];
            }
        }
        else{
            cell.CH_commentsBtn.enabled = YES;
            cell.CH_commentsBtn.backgroundColor = [UIColor clearColor];
            
            if (cell.cellProgress!=nil){
               
                cell.cellProgress.hidden = true;
                [cell.cellProgress removeFromSuperview];
                cell.cellProgress = nil;
                
            }
            
        }
        
        
        
        if(IS_IPAD)
            //cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /7.4f;
        cell.imgContainer.layer.masksToBounds = YES;
        if(sharedManager.isListView) {
           
        } else {
//             [cell.CH_Video_Thumbnail roundCorners];
        }
        [cell.view1 setBackgroundColor:[UIColor clearColor]];
        
        
        
        //cell.CH_RcommentsBtn.enabled = YES;
//        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
//                                              initWithTarget:self action:@selector(handleLongPress:)];
//        lpgr.minimumPressDuration = 1.0;
//        [cell.CH_commentsBtn addGestureRecognizer:lpgr];
//        [lpgr.view setTag:currentIndexHome];
        [cell.CH_heart setTag:currentIndexHome];
        [cell.CH_playVideo setTag:currentIndexHome];
        
        [cell.CH_flag setTag:currentIndexHome];
        [cell.CH_commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CH_commentsBtn setTag:currentIndexHome];
        
        
        [cell setBackgroundColor:[UIColor clearColor]];
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(!tempVideos.isLocal){
        
            [cell.cellProgress removeFromSuperview];
            
        }
        
        cell.blueOverlay.hidden = YES;
        if(sharedManager.channelVideos.count == 1 && !sharedManager.isListView) {
            cell.frame = CGRectMake(5, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
        }
      
        cell.mainThumbnail.hidden = NO;
        cell.CH_Video_Thumbnail.hidden = YES;
        [cell.CH_Video_Thumbnail stopAnimating];
        
        return cell;
        
        
    }
    
    
    
    return nil;
    
}

                        ///// TO DRAG AND DROP CELLS //////
- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
    if(offlineArray.count <= 0 && APP_DELEGATE.hasInet){
        VideoViewCell *cell = (VideoViewCell *)[self.collectionHome cellForItemAtIndexPath:destinationIndexPath];
        cell.blueOverlay.hidden = YES;
        if(sourceIndexPath.item != 0 && destinationIndexPath.item != 0) {
            _currentPost = destinationIndexPath.item - 1;
            VideoModel *model = [sharedManager.channelVideos objectAtIndex:sourceIndexPath.item-1];
            [sharedManager.channelVideos removeObjectAtIndex:sourceIndexPath.item-1];
            [sharedManager.channelVideos insertObject:model atIndex:destinationIndexPath.item-1];
        }
    }
}
                    /////////////////////////////////////////////////////////


- (void)saveImage: (UIImage*)image
{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:@"profile.png" ];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
    }
}
- (UIImage*)loadImage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"profile.png" ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

- (void)removeLoading:(NSIndexPath *)indexpath {
    VideoViewCell *currentCell = (VideoViewCell*)[self.collectionHome cellForItemAtIndexPath:indexpath];
    
    //    NSLog(@"%@", currentCell);
    
    
    
    //[self.roundedPView setProgress:progres];
    
    [currentCell.cellProgress removeFromSuperview];
    
    [self.roundedPView removeFromSuperview];
    NSLog(@"indexpath: %@",indexpath);
}

#pragma mark ZoomImage
-(IBAction)zoomImage:(id)sender{
    [EXPhotoViewer showImageFrom:self.userImg];
}


@end
