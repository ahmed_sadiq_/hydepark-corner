//
//  VideoPlayerVC.m
//  HydePark
//
//  Created by Apple on 25/02/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "VideoPlayerVC.h"
#import "NavigationHandler.h"
#import "VideoCells.h"
#import "Constants.h"
#import "UIImageView+RoundImage.h"
#import <AVFoundation/AVPlayer.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "Utils.h"
#import "CommentsVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "CustomLoading.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "ApiManager.h"
#import "UIImage+JS.h"



#import "PBJVideoPlayerController.h"
@interface VideoPlayerVC ()<PBJVideoPlayerControllerDelegate>
{
    NSString *urlToShare;
    NSString *statusTxt;
    NSMutableDictionary *serviceParams;
    NSArray *adsImagesArray;
    PBJVideoPlayerController *_videoPlayerController;
}
@property (strong, nonatomic) AsyncImageView *thumbnail;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *adsView;
@end

@implementation VideoPlayerVC
@synthesize videoObjs,indexToDisplay,isComment,cPostId,isFirst;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    cache = [[NSMutableDictionary alloc] init];
    VideoPLayerTable.opaque = NO;
    VideoPLayerTable.backgroundColor = [UIColor clearColor];
    CommentsModelObj = [[CommentsModel alloc]init];
    videoModel = [[VideoModel alloc]init];
    playerArray = [[NSMutableArray alloc] init];
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
//    //init Ads
//    adsImagesArray = @[@"corner.png",@"Pringles.png",@"mcDonal.png",@"Pepsilogo.png"];
//    [self animateAds];
//    [self animateAds2];
//    [self animateAds3];
//    [self animateAds4];
}
-(void)viewDidLayoutSubviews{
   // [VideoPLayerTable reloadData];
}

- (void)playerWillEnterFullscreen {
    [UIView animateWithDuration:0.5
                     animations:^{
                         if(IS_IPAD)
                             upperView.frame = CGRectMake(upperView.frame.origin.x, upperView.frame.origin.y - 100, upperView.frame.size.width, upperView.frame.size.height);
                         else
                             upperView.frame = CGRectMake(upperView.frame.origin.x, upperView.frame.origin.y - 60, upperView.frame.size.width, upperView.frame.size.height);
                     }];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
}

- (void)playerWillLeaveFullscreen {
    [UIView animateWithDuration:0.2
                     animations:^{
                         if(IS_IPAD)
                             upperView.frame = CGRectMake(upperView.frame.origin.x, upperView.frame.origin.y + 100, upperView.frame.size.width, upperView.frame.size.height);
                         else
                             upperView.frame = CGRectMake(upperView.frame.origin.x, upperView.frame.origin.y + 60, upperView.frame.size.width, upperView.frame.size.height);
                     }];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}
- (void)playerDidEndPlaying {
    [_shareView setHidden:NO];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //    NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:indexToDisplay inSection:0];
    //    [VideoPLayerTable scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //[VideoPLayerTable reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(id)sender {
    [animationTimer invalidate
     ];
    animationTimer = nil;
    [animationTimer2 invalidate
     ];
    animationTimer2 = nil;
    [animationTimer3 invalidate
     ];
    animationTimer3 = nil;
    [animationTimer4 invalidate
     ];
    animationTimer4 = nil;

    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark ----------------------
#pragma mark TableView Data
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tableView.frame.size.height;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(IS_IPHONE_6Plus)
//        return  675;
//    return 617;
//}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [_videoPlayerController stop];
    [_videoPlayerController.view removeFromSuperview];
    _videoPlayerController = nil;
    VideoCells *cell;
    
    if (IS_IPAD) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VideoCells_Iphone6Plus" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else if(IS_IPHONE_5 || IS_IPHONE_6){
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VideoCells" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VideoCells" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    //VideoModel *tempVideo = [videoObjs objectAtIndex:indexPath.row];
    cell.profileImage.imageURL = [NSURL URLWithString:videoObjs.video_thumbnail_link];
    NSURL *url = [NSURL URLWithString:videoObjs.video_thumbnail_link];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    [cell.profileImage roundImageCorner];
    cell.activityind.hidden = false;
    [cell.activityind startAnimating];
    cell.VideoTitle.text =  [Utils decodeForEmojis:videoObjs.title];
    cell.timerLabel.text = [Utils returnDate:videoObjs.uploaded_date];
    statusTxt = videoObjs.title;
    cell.VideoTitle.numberOfLines = 0;
    [cell.VideoTitle sizeToFit];
    
    if([videoObjs.is_anonymous isEqualToString:@"0"]){
        cell.username.text = videoObjs.userName;
    }
    else{
        cell.username.text = @"Anonymous";
    }
    
    if ([videoObjs.like_by_me isEqualToString:@"1"]) {
        [cell.CH_heart setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
    }else{
        [cell.CH_heart setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
    }
    
    cell.CH_heart.enabled = YES;
    cell.CH_commentsBtn.enabled = YES;
    [cell.CH_commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.CH_commentsBtn setTag:indexPath.row];
    cell.likesCount.text = [Utils abbreviateNumber:videoObjs.like_count withDecimal:1];
    cell.commentsCount.text = [Utils abbreviateNumber:videoObjs.comments_count withDecimal:1];
    
    [cell.CH_heart addTarget:self action:@selector(LikeHearts:) forControlEvents:UIControlEventTouchUpInside];
    [cell.CH_heart setTag:indexPath.row];
    if(self.isfromChat){
        [cell.CH_heart setHidden:YES];
        [cell.CH_commentsBtn setHidden:YES];
        [cell.likesCount setHidden:YES];
        [cell.commentsCount setHidden:YES];
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,50, cell.contentView.frame.size.width, 480)];
    frameForSix = bgView.frame;
    if(IS_IPHONE_6Plus){
        bgView.frame = CGRectMake(0,55, cell.contentView.frame.size.width + 40, 500);
        frameForSix = bgView.frame;
    }else if(IS_IPAD)
    {
        bgView.frame = CGRectMake(0,85, cell.contentView.frame.size.width + 350 , 700);
        frameForSix = bgView.frame;
    }else if(IS_IPHONE_5)
    {
        bgView.frame = CGRectMake(0,50, 320, 380);
        frameForSix = bgView.frame;
    }
    _shareView.frame = frameForSix;
    bgView.backgroundColor = [UIColor blackColor];
    [cell.contentView addSubview:bgView];
    
    //JWConfig *config = [[JWConfig alloc] init];
    NSString *freshUrl;
    if(videoObjs.m3u8_video_link.length > 1) {
      //  config.file = videoObjs.m3u8_video_link;
        freshUrl = videoObjs.m3u8_video_link;
    }
    else {
        //config.file = videoObjs.video_link;
        freshUrl = videoObjs.video_link;
    }
    
    //config.size = CGSizeMake(bgView.frame.size.width, bgView.frame.size.height);
//    self.adsView.frame =CGRectMake(0, bgView.frame.origin.y + bgView.frame.size.height, self.adsView.frame.size.width, self.adsView.frame.size.height);
    
//    _player = [[JWPlayerController alloc] initWithConfig:config];
//    _player.view.frame = bgView.frame;
//    [cell.contentView addSubview:_player.view];
//    
//    config.image = @"/image.jpg";           //title image
//    config.title = @"";        // video title
//    config.controls = YES;  //default       //show/hide controls
//    config.repeat = NO;   //default     //repeat after completion
//    config.premiumSkin = JWPremiumSkinGlow;    //JW premium skin
//    config.cssSkin = @"http://p.jwpcdn.com/iOS/Skins/nature01/nature01.css";
//    config.autostart = YES;
    
    _videoPlayerController = [[PBJVideoPlayerController alloc] init];
    _videoPlayerController.delegate = self;
    _videoPlayerController.view.frame = bgView.frame;
    NSURL *bipbopUrl = [[NSURL alloc] initWithString:freshUrl];
    _videoPlayerController.asset = [[AVURLAsset alloc] initWithURL:bipbopUrl options:nil];
   // _videoPlayerController.videoPath = freshUrl;
    [cell.contentView addSubview:_videoPlayerController.view];
    
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicator.alpha = 1.0;
    _activityIndicator.center = CGPointMake(frameForSix.size.width/2, frameForSix.size.height/2);
    _activityIndicator.hidesWhenStopped = YES;
    [cell.contentView addSubview:_activityIndicator];
    [_activityIndicator startAnimating];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)scrollViewDidScroll:(UIScrollView *)sender
{
    //[self checkWhichVideoToEnable];
}

-(void)checkWhichVideoToEnable
{
    
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(isloadingOfCells) {
        
    }
    
}
- (void)LikeHearts:(UIButton*)sender{
    UIButton *LikeBtn = (UIButton *)sender;
    currentSelectedIndex = LikeBtn.tag;
    postID = videoObjs.videoID;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    VideoCells *cell = [VideoPLayerTable cellForRowAtIndexPath:indexPath];
    
    if([videoObjs.like_by_me isEqualToString:@"1"])
    {
        [LikeBtn setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
        
        NSInteger likeCount = [videoObjs.like_count intValue];
        likeCount--;
        videoObjs.like_count = [NSString stringWithFormat: @"%ld", likeCount];
        cell.likesCount.text = [NSString stringWithFormat: @"%ld", likeCount];
        videoObjs.like_by_me = @"0";
    }
    else{
        [LikeBtn setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
        NSInteger likeCount = [videoObjs.like_count intValue];
        likeCount++;
        videoObjs.like_count = [NSString stringWithFormat: @"%ld", likeCount];
        cell.likesCount.text = [NSString stringWithFormat: @"%ld", likeCount];
        videoObjs.like_by_me = @"1";\
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateMainHeader" object:nil];
    if(isComment)
        [self LikeComment:currentSelectedIndex];
    else
        [self LikePost:currentSelectedIndex];
}
#pragma mark - Like Post
- (void) LikePost:(NSUInteger )indexToLike{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_LIKE_POST,@"method",
                              token,@"session_token",postID,@"post_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                if ([message isEqualToString:@"Post is Successfully liked."]) {
                    appDelegate.timeToupdateHome = TRUE;
                }
                else if ([message isEqualToString:@"Post is Successfully unliked by this user."])
                {
                    appDelegate.timeToupdateHome = TRUE;
                }
            }
        }
    }];
}
#pragma mark - Like Post
- (void) LikeComment:(NSUInteger )indexToLike{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_LIKE_COMMENT,@"method",
                              token,@"session_token",postID,@"comment_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                if ([message isEqualToString:@"Comment is Successfully Liked."]) {
                   
                }
                else if ([message isEqualToString:@"User have Successfully Unliked the comment"])
                {
                    
                }
            }
            else{
                
            }
        }
    }];
}
#pragma mark Get Comments

-(void) ShowCommentspressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:NO];
}
-(void) GetCommnetsOnPost{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_COMMENTS_BY_PARENT_ID,@"method",
                              token,@"Session_token",@"1",@"page_no",ParentCommentID,@"parent_id",cPostId,@"post_id", nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int success = [[result objectForKey:@"success"] intValue];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if(success == 1) {
                //////Comments Videos Response //////
                CommentsArray = [result objectForKey:@"comments"];
                CommentsModelObj.CommentsArray = [[NSMutableArray alloc] init];
                CommentsModelObj.mainArray = [[NSMutableArray alloc]init];
                CommentsModelObj.ImagesArray = [[NSMutableArray alloc]init];
                CommentsModelObj.ThumbnailsArray = [[NSMutableArray alloc]init];
                for(NSDictionary *tempDict in CommentsArray){
                    
                    CommentsModel *_comment = [[CommentsModel alloc] init];
                    
                    _comment.title = [tempDict objectForKey:@"caption"];
                    _comment.comments_count = [tempDict objectForKey:@"comment_count"];
                    _comment.comment_like_count = [tempDict objectForKey:@"comment_like_count"];
                    _comment.userName = [tempDict objectForKey:@"full_name"];
                    _comment.topic_id = [tempDict objectForKey:@"topic_id"];
                    _comment.user_id = [tempDict objectForKey:@"user_id"];
                    _comment.profile_link = [tempDict objectForKey:@"profile_link"];
                    _comment.liked_by_me = [tempDict objectForKey:@"liked_by_me"];
                    _comment.mute = [tempDict objectForKey:@"mute"];
                    _comment.video_link = [tempDict objectForKey:@"video_link"];
                    _comment.m3u8_video_link = [tempDict objectForKey:@"m3u8_video_link"];
                    _comment.video_thumbnail_link = [tempDict objectForKey:@"video_thumbnail_link"];
                    _comment.image_link = [tempDict objectForKey:@"image_link"];
                    _comment.VideoID = [tempDict objectForKey:@"id"];
                    _comment.video_length = [tempDict objectForKey:@"video_length"];
                    _comment.timestamp = [tempDict objectForKey:@"timestamp"];
                    _comment.is_anonymous = [tempDict objectForKey:@"is_anonymous"];
                    _comment.seen_count   = [tempDict objectForKey:@"seen_count"];
                    _comment.reply_count = [tempDict objectForKey:@"reply_count"];
                    
                    [CommentsModelObj.ImagesArray addObject:_comment.profile_link];
                    [CommentsModelObj.ThumbnailsArray addObject:_comment.video_thumbnail_link];
                    [CommentsModelObj.mainArray addObject:_comment.video_link];
                    [CommentsModelObj.CommentsArray addObject:_comment];
                    
                }
                CommentsVC *commentController ;
                if(IS_IPAD)
                    commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
                else  if (IS_IPHONEX){
                    commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
                }
                else
                    commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
                commentController.commentsObj = CommentsModelObj;
                commentController.postArray = videoModel;
                commentController.cPostId = cPostId;
                commentController.isFirstComment = isFirst;
                [[self navigationController] pushViewController:commentController animated:YES];
            }
        }
        else{
            
        }
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}
-(void)presentAll:(int)state{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* copyLink = [UIAlertAction
                               actionWithTitle:@"Copy Link"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self btnCopyLink:self];
                                   [view dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    UIAlertAction* shareOnFb = [UIAlertAction
                                actionWithTitle:@"Share on Facebook"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self fbshareBtn:self];
                                    [view dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    UIAlertAction* ShareonTwt = [UIAlertAction
                                 actionWithTitle:@"Share on Twitter"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self twitterShareBtn:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [view addAction:copyLink];
    [view addAction:shareOnFb];
    [view addAction:ShareonTwt];
    [view addAction:cancel];
    view.popoverPresentationController.sourceView = self.view;
    view.popoverPresentationController.sourceRect = CGRectMake(400, 100, 200, 200);
    [self presentViewController:view animated:YES completion:nil];
}

-(IBAction)sharePressed:(id)sender{
    [self presentAll:0];
    //[_fbAndTwitterView setHidden:NO];
    // tapper.enabled = YES;
}
-(IBAction)replayPressed:(id)sender{
    [_shareView setHidden:YES];
}
- (IBAction)btnCopyLink:(id)sender {
    __block NSString *urlToShares;
    if(videoObjs.isThumbnailReq == 1){
        videoObjs.isThumbnailReq = 0;
        serviceParams = [NSMutableDictionary dictionary];
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:videoObjs.videoID forKey:@"post_id"];
        if(self.isComment)
            [serviceParams setValue:@"1" forKey:@"is_comment"];
        else
            [serviceParams setValue:@"0" forKey:@"is_comment"];
        [Utils downloadImageWithURL:[NSURL URLWithString:videoObjs.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                }
                                 name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                                     int flag = [[data objectForKey:@"success"] intValue];
                                     if(flag){
                                         urlToShares = (NSString *)[data objectForKey:@"deep_link"];
                                         UIPasteboard *pb = [UIPasteboard generalPasteboard];
                                         [pb setString: [NSString stringWithFormat:@"%@", urlToShare]];
                                         [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
                                     }
                                 } failure:^(NSError *error) {
                                     
                                 }];
            }
        }];
    }else{
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString: [NSString stringWithFormat:@"%@",videoObjs.deep_link]];
        [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
    }
}
-(IBAction)fbshareBtn:(id)sender
{
    __block NSString *urlToShares;
    if(videoObjs.isThumbnailReq == 1){
        videoObjs.isThumbnailReq = 0;
        
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:videoObjs.videoID forKey:@"post_id"];
        if(isComment)
            [serviceParams setValue:@"1" forKey:@"is_comment"];
        else
            [serviceParams setValue:@"0" forKey:@"is_comment"];
        [Utils downloadImageWithURL:[NSURL URLWithString:videoObjs.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                }
                                 name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                                     int flag = [[data objectForKey:@"success"] intValue];
                                     if(flag){
                                         urlToShares = (NSString *)[data objectForKey:@"deep_link"];
                                         FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                                         content.contentURL = [NSURL URLWithString:urlToShares];
                                         //content.imageURL   = [NSURL URLWithString:urlToShar];
                                         content.contentTitle = videoObjs.userName;
                                         content.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
                                         
                                         FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
                                         dialog.fromViewController = self;
                                         dialog.shareContent = content;
                                         dialog.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
                                         if (![dialog canShow]) {
                                             // fallback presentation when there is no FB app
                                             dialog.mode = FBSDKShareDialogModeFeedBrowser;
                                         }
                                         [_fbAndTwitterView setHidden:YES];
                                         [dialog show];
                                     }
                                 } failure:^(NSError *error) {
                                     
                                 }];
            }
        }];
    }
    else{
        [_fbAndTwitterView setHidden:YES];
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentURL = [NSURL URLWithString:videoObjs.deep_link];
        //content.imageURL   = [NSURL URLWithString:videoObjs.video_thumbnail_link];
        content.contentTitle = videoObjs.userName;
        content.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
        
        FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
        dialog.fromViewController = self;
        dialog.shareContent = content;
        dialog.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
        if (![dialog canShow]) {
            // fallback presentation when there is no FB app
            dialog.mode = FBSDKShareDialogModeFeedBrowser;
        }
        [dialog show];
    }
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self removeView];
}
-(void) removeView {
    [_fbAndTwitterView setHidden:YES];
}
-(IBAction)twitterShareBtn:(id)sender
{
    [_fbAndTwitterView setHidden:YES];
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [mySLComposerSheet setInitialText:statusTxt];
        [mySLComposerSheet addURL:[NSURL URLWithString:urlToShare]];
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else {
        
        NSString *message;
        NSString *title;
        NSString *cancel;
        message = NSLocalizedString(@"setupTwitterAccount", @"");
        title   = NSLocalizedString(@"accountConfiguration", @"");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
        [alert show];
    }
    
    NSURL *imageURL = [NSURL URLWithString:videoObjs.video_thumbnail_link];
    [CustomLoading showAlertMessage];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            UIImage *thumbnailImg = [UIImage imageWithData:imageData];
            [CustomLoading DismissAlertMessage];
            
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                [mySLComposerSheet setInitialText:@"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech."];
                [mySLComposerSheet addURL:[NSURL URLWithString:urlToShare]];
                [mySLComposerSheet addImage:thumbnailImg];
                [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                    
                    switch (result) {
                        case SLComposeViewControllerResultCancelled:
                            NSLog(@"Post Canceled");
                            break;
                        case SLComposeViewControllerResultDone:
                            NSLog(@"Post Sucessful");
                            break;
                            
                        default:
                            break;
                    }
                }];
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            else {
                
                NSString *message;
                NSString *title;
                NSString *cancel;
                message = NSLocalizedString(@"setupTwitterAccount", @"");
                title   = NSLocalizedString(@"accountConfiguration", @"");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                               delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        });
    });
}

#pragma mark ANIMATE ADS
-(void)animateAds{
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 0; i < adsImagesArray.count; i++) {
        [images addObject:[UIImage imageNamed:[adsImagesArray objectAtIndex:i]]];
    }
    self.ad1ImgView.animationImages = images;
    self.ad1ImgView.animationDuration = 12.0f;
    self.ad1ImgView.animationRepeatCount = 0;
    animationTimer= [NSTimer timerWithTimeInterval:3.0
                                            target:self
                                          selector:@selector(onTimerImage)
                                          userInfo:nil
                                           repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:animationTimer forMode:NSRunLoopCommonModes];
    [animationTimer fire];
    [self.ad1ImgView startAnimating];
}
-(void)onTimerImage{
    [UIView animateWithDuration:3.0 animations:^{
        self.ad1ImgView.alpha = 0.0;
    }];
    [UIView animateWithDuration:3.0 animations:^{
        self.ad1ImgView.alpha = 1.0;
    }];
}
-(void)animateAds2{
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 0; i < adsImagesArray.count; i++) {
        [images addObject:[UIImage imageNamed:[adsImagesArray objectAtIndex:i]]];
    }
    self.ad2ImgView.animationImages = images;
    self.ad2ImgView.animationDuration = 12.0f;
    self.ad2ImgView.animationRepeatCount = 0;
    animationTimer2= [NSTimer timerWithTimeInterval:3.0
                                             target:self
                                           selector:@selector(onTimerImage2)
                                           userInfo:nil
                                            repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:animationTimer2 forMode:NSRunLoopCommonModes];
    [animationTimer2 fire];
    [self.ad2ImgView startAnimating];
    
    
}
-(void)onTimerImage2{
    [UIView animateWithDuration:3.0 animations:^{
        self.ad2ImgView.alpha = 0.0;
    }];
    [UIView animateWithDuration:3.0 animations:^{
        self.ad2ImgView.alpha = 1.0;
    }];
}
-(void)animateAds3{
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 0; i < adsImagesArray.count; i++) {
        [images addObject:[UIImage imageNamed:[adsImagesArray objectAtIndex:i]]];
    }
    self.ad3ImgView.animationImages = images;
    self.ad3ImgView.animationDuration = 12.0f;
    self.ad3ImgView.animationRepeatCount = 0;
    animationTimer3= [NSTimer timerWithTimeInterval:3.0
                                             target:self
                                           selector:@selector(onTimerImage3)
                                           userInfo:nil
                                            repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:animationTimer3 forMode:NSRunLoopCommonModes];
    [animationTimer3 fire];
    [self.ad3ImgView startAnimating];
    
    
}
-(void)onTimerImage3{
    [UIView animateWithDuration:3.0 animations:^{
        self.ad3ImgView.alpha = 0.0;
    }];
    [UIView animateWithDuration:3.0 animations:^{
        self.ad3ImgView.alpha = 1.0;
    }];
}
-(void)animateAds4{
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 0; i < adsImagesArray.count; i++) {
        [images addObject:[UIImage imageNamed:[adsImagesArray objectAtIndex:i]]];
    }
    self.ad4ImgView.animationImages = images;
    self.ad4ImgView.animationDuration = 12.0f;
    self.ad4ImgView.animationRepeatCount = 0;
    animationTimer4= [NSTimer timerWithTimeInterval:3.0
                                             target:self
                                           selector:@selector(onTimerImage4)
                                           userInfo:nil
                                            repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:animationTimer4 forMode:NSRunLoopCommonModes];
    [animationTimer4 fire];
    [self.ad4ImgView startAnimating];
    
    
}
-(void)onTimerImage4{
    [UIView animateWithDuration:3.0 animations:^{
        self.ad4ImgView.alpha = 0.0;
    }];
    [UIView animateWithDuration:3.0 animations:^{
        self.ad4ImgView.alpha = 1.0;
    }];
}
#pragma mark - PBJVideoPlayerControllerDelegate

- (void)videoPlayerReady:(PBJVideoPlayerController *)videoPlayer
{
    //NSLog(@"Max duration of the video: %f", videoPlayer.maxDuration);
    
    [_videoPlayerController playFromBeginning];
}

- (void)videoPlayerPlaybackStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
}

- (void)videoPlayerPlaybackWillStartFromBeginning:(PBJVideoPlayerController *)videoPlayer
{
    
}

- (void)videoPlayerPlaybackDidEnd:(PBJVideoPlayerController *)videoPlayer
{
   
    [_videoPlayerController playFromBeginning];
}

- (void)videoPlayerBufferringStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
    switch (videoPlayer.bufferingState) {
     case PBJVideoPlayerBufferingStateUnknown:
     NSLog(@"Buffering state unknown!");
     break;
     
     case PBJVideoPlayerBufferingStateReady:
            [_activityIndicator stopAnimating];
            [_activityIndicator removeFromSuperview];
     NSLog(@"Buffering state Ready! Video will start/ready playing now.");
     break;
     
     case PBJVideoPlayerBufferingStateDelayed:
     NSLog(@"Buffering state Delayed! Video will pause/stop playing now.");
     break;
     default:
     break;
     }
}

- (void)videoPlayer:(PBJVideoPlayerController *)videoPlayer didUpdatePlayBackProgress:(CGFloat)progress {
}

- (CMTime)videoPlayerTimeIntervalForPlaybackProgress:(PBJVideoPlayerController *)videoPlayer {
    return CMTimeMake(5, 25);
}
@end
