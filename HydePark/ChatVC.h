//
//  ChatVC.h
//  HydePark
//
//  Created by Samreen Noor on 28/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource> {
    BOOL search;
    UIRefreshControl *refreshControl;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *centrLine;
@property (strong, nonatomic) NSMutableArray *friends;
@property (strong, nonatomic) NSMutableArray *searchFriends;
@property (strong, nonatomic) IBOutlet UITextField *searchField;
- (IBAction)searchPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewStartChat;
@property (weak, nonatomic) IBOutlet UILabel *noUsersFoundlbl;
@property (weak, nonatomic) IBOutlet UIImageView *background;
-(void)getFriends;

@end
