//
//  NSMutableArray+QueueFunctions.h
//  HydePark
//
//  Created by Osama on 20/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (QueueFunctions)
- (id)dequeue;
- (void)enqueue:(id)obj;
- (id)peek;
@end
