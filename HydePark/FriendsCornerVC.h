//
//  FriendsCornerVC.h
//  HydePark
//
//  Created by Osama on 01/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataContainer.h"
//#import <StreamaxiaSDK/StreamaxiaSDK.h>
//#import "StreamViewController.h"

@interface FriendsCornerVC : UIViewController<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>{
    NSUInteger currentIndexHome;
    DataContainer *sharedManager;
    UIView * footerViewHome;
    BOOL cannotscroll;
    NSUInteger currentSelectedIndex;
    IBOutlet UIActivityIndicatorView *indicator;
    BOOL isScrolling;
}
@property (nonatomic, weak) IBOutlet UICollectionView *collectionHome;
@property (nonatomic, weak) IBOutlet UICollectionView *storiesCollectionView;
@property (nonatomic, weak) IBOutlet UITableView *tblHome;
@property (nonatomic, assign) BOOL fetchingContent;
@property (nonatomic, assign) BOOL isDownwards;
@property (nonatomic, weak) IBOutlet UIView *noBeamsView;
@property (nonatomic, strong) NSMutableArray *friendsStoriesArray;
@property (nonatomic, strong) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLbl;
@property (weak, nonatomic) IBOutlet UILabel *followPeopleLbl;
@property (weak, nonatomic) IBOutlet UIButton *findFriendsBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *footerIndicator;

@end
