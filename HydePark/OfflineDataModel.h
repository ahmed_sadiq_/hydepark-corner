//
//  OfflineDataModel.h
//  HydePark
//
//  Created by Ahmed Sadiq on 23/11/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfflineDataModel : NSObject
@property (strong, nonatomic) NSString *rowID;
@property (strong, nonatomic) NSString *method;
@property (strong, nonatomic) NSString *sessionToken;
@property (strong, nonatomic) NSString *allowedReply;
@property (strong, nonatomic) NSString *caption;
@property (strong, nonatomic) NSString *anonyCheck;
@property (strong, nonatomic) NSString *muteCheck;
@property (strong, nonatomic) NSString *duration;
@property (strong, nonatomic) NSString *postId;
@property (strong, nonatomic) NSString *parentId;
@property (strong, nonatomic) NSString *privacyCheck;
@property (strong, nonatomic) NSString *langCode;
@property (strong, nonatomic) NSString *currentTime;
@property (strong, nonatomic) NSString *data;
@property (strong, nonatomic) NSString *isAudio;
@property (strong, nonatomic) NSString *isComment;
@property (strong, nonatomic) NSString *thumbnail;
@end
