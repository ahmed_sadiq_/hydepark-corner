////
////  SettingsVC.m
////  HydePark
////
////  Created by Osama on 27/02/2017.
////  Copyright © 2017 TxLabz. All rights reserved.
////
//
#import "SettingsVC.h"
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>
#import "CustomLoading.h"
#import "UserShareModel.h"
#import <Contacts/Contacts.h>
#import "Person.h"
#import "NavigationHandler.h"
#import "Utils.h"
#import "AppDelegate.h"
#import "FriendsVC.h"
#import "ChangePasswordVC.h"
#import <SafariServices/SafariServices.h>
#import "ContactUsFormVC.h"
#import "LangaugeSelectionVC.h"
#import "SVProgressHUD.h"
#import "Constants.h"

@interface SettingsVC ()<SFSafariViewControllerDelegate>{
    ACAccount *myAccount;
    NSMutableString *paramString;
    NSMutableArray *resultFollowersNameList;
    AppDelegate *appDelegate;
}
@end

@implementation SettingsVC
//
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.searchDisplayController.searchBar.placeholder = NSLocalizedString(@"search", @"");
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, 600)];
    sharedManager = [DataContainer sharedManager];
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    BOOL isCelebrity = [Utils userIsCeleb];
    if (!isCelebrity){
        [UIView performWithoutAnimation:^{
            self.analyticsView.hidden = YES;
            [Utils moveViewPosition:270.0 onView:self.signOutView completion:^(BOOL finished) {
            }];
        }];
    }
    
    inviteOptionsArray = [NSMutableArray array];
    profileOptionsArray = [NSMutableArray array];

    [inviteOptionsArray addObject:NSLocalizedString(@"facebook", @"")];
    [inviteOptionsArray addObject:NSLocalizedString(@"twitter", @"")];
    [inviteOptionsArray addObject:NSLocalizedString(@"contacts", @"")];
    
    [profileOptionsArray addObject:NSLocalizedString(@"edit_profile", @"")];
    [profileOptionsArray addObject:NSLocalizedString(@"change_password", @"")];
    [profileOptionsArray addObject:NSLocalizedString(@"blocked_users", @"")];
    
    if(isCelebrity)
    {
        [profileOptionsArray addObject:@"Analytics"];
    }
    
    [profileOptionsArray addObject:NSLocalizedString(@"language", @"")];
    [profileOptionsArray addObject:NSLocalizedString(@"night_mode", @"")];
    [profileOptionsArray addObject:NSLocalizedString(@"sign_out", @"")];

    nightSwitch = [[UISwitch alloc] initWithFrame: CGRectZero];
    nightSwitch.userInteractionEnabled = false;
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        [nightSwitch setOn:![[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"] animated:NO];
        if(nightSwitch.isOn)
        {
            tableView.backgroundColor = [UIColor clearColor];
        }
        else
        {
            tableView.backgroundColor = [UIColor whiteColor];
        }
    }
    else
    {
        [nightSwitch setOn:true animated:NO];
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Facebook  Share
- (IBAction)shareToMessanger:(id)sender{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:sharedManager._profile.inviteUrl];
    content.contentTitle = sharedManager._profile.inviteText;
    
    FBSDKMessageDialog *messageDialog = [[FBSDKMessageDialog alloc] init];
    messageDialog.delegate = self;
    [messageDialog setShareContent:content];
    
    if ([messageDialog canShow])
    {
        [messageDialog show];
    }
    else
    {
        // Messenger isn't installed. Redirect the person to the App Store.
        [Utils showAlert:NSLocalizedString(@"MessengerNotInstalled", @"")];
    }
    
}

#pragma mark FB Delegates
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"didCompleteWithResults");
    
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError ::: %@" , error);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    NSLog(@"sharerDidCancel");
    
}

#pragma mark Twitter Sharing
-(IBAction)getTwitterDetails:(id)sender{
    sharingstate = 1;
    [self getTwitterAccounts];
}

/******To check whether More then Twitter Accounts setup on device or not *****/

-(void)getTwitterAccounts {
//    dispatch_async(dispatch_get_main_queue(), ^{
//    [CustomLoading showAlertMessage];
//    });
    
    [SVProgressHUD show];
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    

    
    [accountStore requestAccessToAccountsWithType:accountType
                            withCompletionHandler:^(BOOL granted, NSError *error) {
                                
                                if (granted && !error) {
                                    NSArray  *accountsList = [accountStore accountsWithAccountType:accountType];
                                    if(accountsList.count > 0){
                                        myAccount = [accountsList objectAtIndex:0];
                                        [self getTwitterFriendsIDListForThisAccount];
                                    }
                                    else{
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            NSString *message;
                                            NSString *title;
                                            message = NSLocalizedString(@"setupTwitterAccount", @"");
                                            title   = NSLocalizedString(@"accountConfiguration", @"");
                                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                                                           delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                                                                  otherButtonTitles:nil, nil];
                                            [alert show];
                                            [SVProgressHUD dismiss];
                                        });
                                        
                                    }
                                }
                                else
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSString *message;
                                        NSString *title;
                                        message = NSLocalizedString(@"setupTwitterAccount", @"");
                                        title   = NSLocalizedString(@"accountConfiguration", @"");
                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                                                              otherButtonTitles:nil, nil];
                                        [alert show];
                                        [SVProgressHUD dismiss];
                                    });
                                }
                            } ];
    
}


/************* getting followers/friends ID list code start here *******/
// so far we have instnce of current account, that is myAccount //

-(void) getTwitterFriendsIDListForThisAccount{
    
    /*** url for all friends *****/
    // NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/friends/ids.json"];
    
    /*** url for Followers only ****/
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/followers/ids.json"];
    
    NSDictionary *p = [NSDictionary dictionaryWithObjectsAndKeys:myAccount.username, @"screen_name", nil];
    
    TWRequest *twitterRequest = [[TWRequest alloc] initWithURL:url parameters:p requestMethod:TWRequestMethodGET];
    [twitterRequest setAccount:myAccount];
    [twitterRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResposnse, NSError *error)
     {
         if (error) {
             
         }
         paramString = [[NSMutableString alloc] init];
         NSError *jsonError = nil;
         // Convert the response into a dictionary
         NSDictionary *twitterFriends = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONWritingPrettyPrinted error:&jsonError];
         
         NSArray *IDlist = [twitterFriends objectForKey:@"ids"];
         _users = [[NSMutableArray alloc] init];
         
         NSInteger count = IDlist.count;
         
         for (int i=0; i<count; i++ ) {
             UserShareModel *uModel = [[UserShareModel alloc] init];
             NSInteger uId = [[IDlist objectAtIndex:i] integerValue];
             uModel.userID = [NSString stringWithFormat:@"%ld",(long)uId];
             [paramString appendFormat:@"%@",[IDlist objectAtIndex:i]];
             if ((i <count-2)  && i< 80) {
                 NSString *delimeter = @",";
                 [paramString appendString:delimeter];
                 [_users addObject:uModel];
             }
         }
         [self getFollowerNameFromID:paramString];
     }
     ];
    
}


-(void) getFollowerNameFromID:(NSString *)ID{
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/users/lookup.json"];
    NSDictionary *p = [NSDictionary dictionaryWithObjectsAndKeys:ID, @"user_id",nil];
    TWRequest *twitterRequest = [[TWRequest alloc] initWithURL:url
                                                    parameters:p
                                                 requestMethod:TWRequestMethodGET];
    [twitterRequest setAccount:myAccount];
    [twitterRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (error) {
            
        }
        NSError *jsonError = nil;
        NSDictionary *friendsdata = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONWritingPrettyPrinted error:&jsonError];
        resultFollowersNameList = [friendsdata valueForKey:@"name"];
        NSArray *screenNamesArray = [friendsdata valueForKey:@"screen_name"];
        for(int i=0; i<resultFollowersNameList.count; i++) {
            UserShareModel *uModel = [_users objectAtIndex:i];
            uModel.fullName = [resultFollowersNameList objectAtIndex:i];
            uModel.screenName = [screenNamesArray objectAtIndex:i];
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            _inviteListView.hidden = false;
            [_inviteListTblView reloadData];
            [SVProgressHUD dismiss];
            [CustomLoading DismissAlertMessage];
            
        }];
    }];
}
- (IBAction)inviteBackPressed:(id)sender {
    _inviteListView.hidden = true;
}

#pragma mark - Text Share Methods
- (IBAction)viaText:(id)sender {
    sharingstate = 2;
    [self getTextAccounts];
}

-(void)getTextAccounts {
//    [CustomLoading showAlertMessage];
    [SVProgressHUD show];
    self.usersText = [[NSMutableArray alloc] init];
    
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                });
                NSLog(@"error fetching contacts %@", error);
            } else {
                NSString *phone;
                NSString *fullName;
                NSString *firstName;
                NSString *lastName;
                UIImage *profileImage;
                NSMutableArray *contactNumbersArray;
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    else{
                        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    }
                    UIImage *image = [UIImage imageWithData:contact.imageData];
                    if (image != nil) {
                        profileImage = image;
                    }else{
                        profileImage = [UIImage imageNamed:@"person-icon.png"];
                    }
                    for (CNLabeledValue *label in contact.phoneNumbers) {
                        phone = [label.value stringValue];
                        if ([phone length] > 0) {
                            
                            Person *pTemp = [[Person alloc] init];
                            pTemp.firstName = contact.givenName;
                            pTemp.lastName = contact.familyName;
                            pTemp.fullName = fullName;
                            pTemp.number = phone;
                            [self.usersText addObject:pTemp];
                            [contactNumbersArray addObject:phone];
                        }
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CustomLoading DismissAlertMessage];
                    _inviteListView.hidden = false;
                    [SVProgressHUD dismiss];
                    [_inviteListTblView reloadData];
                });
            }
        }
        else{
//            [SVProgressHUD dismiss];
//            [CustomLoading DismissAlertMessage];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *message;
                NSString *title;
                message = NSLocalizedString(@"setupContact", @"");
                title   = NSLocalizedString(@"accountConfiguration", @"");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                               delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                                      otherButtonTitles:nil, nil];
                [alert show];
                [SVProgressHUD dismiss];
            });
        }
    }];
}

#pragma mark - Table View  Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0)
    {
        return inviteOptionsArray.count;
    }
    return profileOptionsArray.count;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40.0f)];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            [view setBackgroundColor:[UIColor lightGrayColor]];
        }
    }
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 100, 40)];
    [lbl setFont:[UIFont systemFontOfSize:18]];
    [lbl setTextColor:[UIColor whiteColor]];
    [lbl setBackgroundColor:[UIColor clearColor]];
    [lbl setFont:[UIFont fontWithName:@"Montserrat" size:15]];
    
    if(section == 0)
    {
        lbl.text = NSLocalizedString(@"invite", @"");
    }
    else
    {
        lbl.text = NSLocalizedString(@"profile", @"");
    }

    [view addSubview:lbl];
    return view;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    UITableViewCell *cell = [_searchTblView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    
    [cell.textLabel setFont:[UIFont fontWithName:@"Montserrat" size:14]];

    
    if(indexPath.section == 0)
    {
        cell.textLabel.text = inviteOptionsArray[indexPath.row];
    }
    else
    {
        cell.textLabel.text = profileOptionsArray[indexPath.row];
    }

    if(indexPath.section == 1)
    {
        if([Utils userIsCeleb])
        {
            if(indexPath.row == 5)
            {
                nightSwitch.frame = CGRectMake(cell.contentView.frame.size.width + 30, 10, nightSwitch.frame.size.width, nightSwitch.frame.size.height);
                if(IS_IPHONEX)
                {
                    nightSwitch.frame = CGRectMake(cell.contentView.frame.size.width - 10, 10, nightSwitch.frame.size.width, nightSwitch.frame.size.height);
                }
                if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
                {
                    [nightSwitch setOn:![[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"] animated:NO];
                }
                [cell.contentView addSubview:nightSwitch];
            }
        }
        else
        {
            if(indexPath.row == 4)
            {
                nightSwitch.frame = CGRectMake(cell.contentView.frame.size.width + 30, 10, nightSwitch.frame.size.width, nightSwitch.frame.size.height);
                if(IS_IPHONEX)
                {
                    nightSwitch.frame = CGRectMake(cell.contentView.frame.size.width - 10, 10, nightSwitch.frame.size.width, nightSwitch.frame.size.height);
                }
                if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
                {
                    [nightSwitch setOn:![[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"] animated:NO];
                }
                [cell.contentView addSubview:nightSwitch];
            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == 0)
    {
        switch (indexPath.row) {
            case 0:
                [self shareToMessanger:nil];
                break;
            case 1:
                [self getTwitterDetails:nil];
                break;
            case 2:
                [self viaText:nil];
                break;
                
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row) {
            case 0:
                [self editProfile:nil];
                break;
            case 1:
                [self changePassword:nil];
                break;
            case 2:
                [self BlockedUser:nil];
                break;
            case 3:
                if([Utils userIsCeleb])
                {
                    [self openAnalytics:nil];
                }
                else
                {
                    [self langaugeSelection:nil];
                }
                break;
            case 4:
                if([Utils userIsCeleb])
                {
                    [self langaugeSelection:nil];
                }
                else
                {
                    [self switchValueChanged:nil];
                }
                break;
            case 5:
                if([Utils userIsCeleb])
                {
                    [self switchValueChanged:nil];
                }
                else
                {
                    [self signOut:nil];
                }
                break;
            case 6:
                if([Utils userIsCeleb])
                {
                    [self signOut:nil];
                }
                break;
                
            default:
                break;
        }
    }
}


-(IBAction)switchValueChanged:(id)sender
{
    if(nightSwitch.isOn) ///// MAKE APP CHANGE TO WHITE BACKGROUND
    {
        [nightSwitch setOn:false animated:YES];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"nightMode"];
        [[NSNotificationCenter defaultCenter] postNotificationName:SET_NIGHT_MODE object:nil];
        [UIView animateWithDuration:0.5 animations:^(void) {
            tableView.backgroundColor = [UIColor whiteColor];
        }];
    }
    else
    {
        [nightSwitch setOn:true animated:YES];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"nightMode"];
        [[NSNotificationCenter defaultCenter] postNotificationName:CLEAR_NIGHT_MODE object:nil];
        [UIView animateWithDuration:0.5 animations:^(void) {
            tableView.backgroundColor = [UIColor clearColor];

        }];
    }
    [tableView performSelector:@selector(reloadData) withObject:nil afterDelay:0.2];
}


#pragma mark - Search Bar Delegates Methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [self.searchResult removeAllObjects];
    //    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.fullname contains[c] %@", searchText];
    //
    //    self.searchResult = [NSMutableArray arrayWithArray: [self.tableData filteredArrayUsingPredicate:resultPredicate]];
    
    isSearch = true;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.fullName contains[c] %@", searchText];
    
    if(sharingstate == 1) {
        self.searchResult = [NSMutableArray arrayWithArray: [_users filteredArrayUsingPredicate:predicate]];
    }
    else {
        self.searchResult = [NSMutableArray arrayWithArray: [self.usersText filteredArrayUsingPredicate:predicate]];
    }
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView  {
    
    tableView.frame = _inviteListTblView.frame;
    _searchTblView = _inviteListTblView;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.navigationController.navigationBar.hidden = YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller {
    isSearch = false;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [_inviteListTblView reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark EDIT PROFILE : CHANGE PASSWORD : BLOKCED USERS
-(IBAction)editProfile:(id)sender{
    [[NavigationHandler getInstance]MoveToProfile];
}

-(IBAction)changePassword:(id)sender{
    //
    if([sharedManager._profile.accountType  isEqualToString:@"CUSTOM"]){
        ChangePasswordVC *commentController; //   = [[ChangePasswordVC alloc] initWithNibName:@"ChangePasswordVC" bundle:nil];
        if (IS_IPHONEX){
            commentController    = [[ChangePasswordVC alloc] initWithNibName:@"ChangePasswordVC_IphoneX" bundle:nil];
        }else{
            commentController    = [[ChangePasswordVC alloc] initWithNibName:@"ChangePasswordVC" bundle:nil];
        }
        
        [[self navigationController] pushViewController:commentController animated:YES];
    }
    else{
        [Utils showAlert:NSLocalizedString(@"user_is_social", @"")];
    }
}

-(IBAction)BlockedUser:(id)sender{
    FriendsVC *friendVC;
    
//    if (IS_IPHONEX){
//        friendVC    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        friendVC    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    friendVC.titles        = NSLocalizedString(@"blocked_users", @"");
    friendVC.NoFriends     = FALSE;
    friendVC.loadFollowings = 7;
    friendVC.isLocalSearch = YES;
    [[self navigationController] pushViewController:friendVC animated:YES];
}

#pragma mark - Message Delegates Methods
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller1 didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
        {
            break;
        }
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"The SMS was not sent.  Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
        {
            break;
        }
        default:
            break;
    }
    _inviteListView.hidden = true;
    [self dismissViewControllerAnimated:false completion:nil];
    [self.navigationController popViewControllerAnimated:true];
}
-(void)postMessageToFriend:(NSString *)ID withName : (NSString*)fullName{
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        
        if (granted && !error) {
            
            NSArray *accountsList = [accountStore accountsWithAccountType:accountType];
            
            NSString *textToInvite = [NSString stringWithFormat:@"%@ %@",sharedManager._profile.inviteText,sharedManager._profile.inviteUrl];
            
            int NoOfAccounts = [accountsList count];
            if (NoOfAccounts >0) {
                
                NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/direct_messages/new.json"];
                ACAccount *twitterAccount = [accountsList lastObject];
                
                NSDictionary *p = [NSDictionary dictionaryWithObjectsAndKeys:
                                   
                                   textToInvite, @"text",
                                   fullName, @"screen_name",
                                   nil
                                   ];
                SLRequest *postRequest = [SLRequest  requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:url parameters:p];
                [postRequest setAccount:twitterAccount];
                [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResposnse, NSError *error){
                    
                    NSError *jsonError = nil;
                    
                    NSDictionary *friendsdata = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONWritingPrettyPrinted error:&jsonError];
                    
                    NSLog(@"response value is: %@ %ld ",friendsdata,(long)[urlResposnse statusCode]);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Utils showAlert:NSLocalizedString(@"", @"")];
                        _inviteListView.hidden = true;
                        [SVProgressHUD dismiss];
                        [CustomLoading DismissAlertMessage];
                    });
                    
                }];
            }
        }
    }];
}
#pragma mark SIGN OUT
-(IBAction)signOut:(id)sender{
    appDelegate.myCornerDataSuccess = NO;
    [Utils removeProfileImg];
    [Utils clearUserDefaults];
    [[NavigationHandler getInstance]LogoutUser];
    [sharedManager dellocDate];
}

-(IBAction)openAnalytics:(id)sender{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"https://hydeparkcorner.com/stats?session_token=%@",token]];
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = self;
            [self presentViewController:sfvc animated:YES completion:nil];
        }
    } else {
        // will have a nice alert displaying soon.
    }
}

- (IBAction)openLink:(id)sender{
    
    ContactUsFormVC *commentController    = [[ContactUsFormVC alloc] initWithNibName:@"ContactUsFormVC" bundle:nil];
    [[self navigationController] pushViewController:commentController animated:YES];
}
- (IBAction)langaugeSelection:(id)sender{
    LangaugeSelectionVC *commentController;   // = [[LangaugeSelectionVC alloc] initWithNibName:@"LangaugeSelectionVC" bundle:nil];
    
    if(IS_IPHONEX){
        commentController    = [[LangaugeSelectionVC alloc] initWithNibName:@"LangaugeSelectionVC_IphoneX" bundle:nil];
    }else{
        commentController    = [[LangaugeSelectionVC alloc] initWithNibName:@"LangaugeSelectionVC" bundle:nil];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self navigationController] pushViewController:commentController animated:YES];
    });
}
- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [self dismissViewControllerAnimated:true completion:nil];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
