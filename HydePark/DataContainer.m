//
//  DataContainer.m
//  HydePark
//
//  Created by TxLabz on 30/06/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "DataContainer.h"

@implementation DataContainer
@synthesize myArchiveArray,notificationsArray,whoToFollow,famousUsersFetchedFirst,notifcationsFetcedFirst,whoToFollowPageNum,notificationsPageNum,followings,followers,whoToFollowPreviousCount,notificationsPreviousCount,newsfeedsVideos,channelVideos,forumsVideo,_profile,myCornerPageNum,pageNum, forumPageNumber,searchPageNum,thumbToUpload,topicsArray,languageCode,commentsDict,newRequestsPageNum,sentRequestsPageNum,friendsPageNum,pendingArray,reqsArray,friendsForTagging;
#pragma mark Singleton Methods

+ (id)sharedManager {
    static DataContainer *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        myArchiveArray              = [[NSMutableArray alloc] init];
        notificationsArray          = [[NSMutableArray alloc] init];
        whoToFollow                 = [[NSMutableArray alloc] init];
        followers                   = [[NSMutableArray alloc] init];
        followings                  = [[NSMutableArray alloc] init];
        newsfeedsVideos                  = [[NSMutableArray alloc] init];
        channelVideos                  = [[NSMutableArray alloc] init];
        forumsVideo                  = [[NSMutableArray alloc] init];
        _uploadedChannlVideos        =[[NSMutableArray alloc]init];
        _friendsArray               =[[NSMutableArray alloc]init];
        friendsForTagging = [[NSMutableArray alloc] init];
        _friendsVcArray               =[[NSMutableArray alloc]init];
        pendingArray               =[[NSMutableArray alloc]init];
        reqsArray               =[[NSMutableArray alloc]init];

        
        commentsDict = [[NSMutableDictionary alloc]init];
        topicsArray = [[NSMutableArray alloc] init];
        famousUsersFetchedFirst     = TRUE;
        notifcationsFetcedFirst     = TRUE;
      _moveFromMenu=false;
        whoToFollowPageNum          = 1;
        notificationsPageNum        = 1;
        whoToFollowPreviousCount    = 0;
        notificationsPreviousCount  = 0;
        
        searchPageNum               = 1;
        forumPageNumber             = 1;
        pageNum                     = 1;
        myCornerPageNum             = 1;
        
        newRequestsPageNum = 1;
        sentRequestsPageNum = 1;
        friendsPageNum = 1;
        
        self.onlineVideoCount = 0;
        self.commentsCount = -1;
        self.commentsCurrentCount = 0;
        _isChaVC                    = false;

    }
    return self;
}

-(void)dellocDate{
    [myArchiveArray removeAllObjects];
    [notificationsArray removeAllObjects];
    [whoToFollow    removeAllObjects];
    [followers      removeAllObjects];
    [followings removeAllObjects];
    [friendsForTagging removeAllObjects];
    [newsfeedsVideos removeAllObjects];
    [channelVideos removeAllObjects];
    [forumsVideo removeAllObjects];
    [_friendsArray removeAllObjects];
    _profile = nil;
    
    famousUsersFetchedFirst     = TRUE;
    notifcationsFetcedFirst     = TRUE;
    whoToFollowPageNum          = 1;
    notificationsPageNum        = 1;
    whoToFollowPreviousCount    = 0;
    notificationsPreviousCount  = 0;
    
    searchPageNum               = 1;
    forumPageNumber             = 1;
    pageNum                     = 1;
    myCornerPageNum             = 1;
    
    newRequestsPageNum = 1;
    sentRequestsPageNum = 1;
    friendsPageNum = 1;
}

@end
