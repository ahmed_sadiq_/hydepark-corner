//
//  LiveViewController.m
//  HydePark
//
//  Created by Babar Hassan on 10/13/17.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "LiveViewController.h"
#import "NELivePlayerController.h"



@interface LiveViewController ()
@property (strong, nonatomic) MPMoviePlayerController *streamPlayer;

@property (nonatomic, strong) NELivePlayerController *player;

@end

@implementation LiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.player = [[NELivePlayerController alloc] initWithContentURL:[NSURL URLWithString:_urlString]];
    [self.player SetBufferStrategy:NELPAntiJitter];
    [self.player setHardwareDecoder:NO];
    //[self.player setScalingMode:NELPMovieScalingModeFill];
    [self.player setScalingMode:NELPMovieScalingModeAspectFit];
    [self.player setPauseInBackground:NO];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    self.player.view.frame = CGRectMake(0, 0, width, height);
    [self.view addSubview:self.player.view];
    [self.player setShouldAutoplay:YES];
    [self.player prepareToPlay];
    
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}





- (IBAction)videoFinished:(id)sender {
    
    

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
