//
//  LiveViewController.h
//  HydePark
//
//  Created by Babar Hassan on 10/13/17.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveViewController : UIViewController
@property (weak, nonatomic) NSString * urlString;

@end
