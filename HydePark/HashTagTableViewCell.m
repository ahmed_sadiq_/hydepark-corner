//
//  HashTagTableViewCell.m
//  HydePark
//
//  Created by apple on 6/30/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import "HashTagTableViewCell.h"

@implementation HashTagTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
                    [_hashTagCollectionView registerNib:[UINib nibWithNibName:@"HashTagsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"HashTagsCollectionViewCell"];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
