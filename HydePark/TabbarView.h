//
//  TabbarView.h
//  Wits
//
//  Created by TxLabz on 26/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TabbarViewDelegate
-(void)tabbarBtnClicked:(id)sender;

@end

@interface TabbarView : UIView
- (IBAction)homePressed:(id)sender;
- (IBAction)friednsPressed:(id)sender;
- (IBAction)storePressed:(id)sender;
- (IBAction)settingsPressed:(id)sender;
- (IBAction)morePressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIButton *beamBtn;
@property (weak, nonatomic) IBOutlet UIButton *audioBtn;
@property (weak, nonatomic) IBOutlet UIButton *anonymousBtn;
@property (weak, nonatomic) IBOutlet UIView *bgView;


@property (weak, nonatomic) IBOutlet UILabel *audioLbl;
@property (weak, nonatomic) IBOutlet UILabel *anonymousLbl;
@property (nonatomic, assign) id<TabbarViewDelegate> delegate;
@end
