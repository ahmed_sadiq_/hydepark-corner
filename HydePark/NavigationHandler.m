//
//  NavigationHandler.m
//  Yolo
//
//  Created by Salman Khalid on 13/06/2014.
//  Copyright (c) 2014 Xint Solutions. All rights reserved.
//

#import "NavigationHandler.h"
#import "HomeVC.h"
#import "Constants.h"
#import "ViewController.h"
#import "Topics.h"
#import "MyBeam.h"
#import "SignUpVC.h"
#import "ProfileVC.h"
#import "NotificationsVC.h"
#import "SearchFriendsVC.h"
#import "Constants.h"
#import "Utils.h"
#import "SVProgressHUD.h"
#import "PopularUsersVC.h"
#import "CommentsVC.h"
#import "UserChannel.h"
#import "DEMOLeftMenuViewController.h"
#import "HomeVC.h"
#import "DEMORightMenuViewController.h"
#import "NavigationHandler.h"
#import "ChatVC.h"
#import "FriendChatVC.h"
#import "HomeViewController.h"
#import "UIColor+Style.h"
#import "SettingsVC.h"
@implementation NavigationHandler

- (id)initWithMainWindow:(UIWindow *)_tempWindow{
    
    if(self = [super init])
    {
        _window = _tempWindow;
    }
    instance = self;
    return self;
}

static NavigationHandler *instance= NULL;

+(NavigationHandler *)getInstance
{
    if (instance == nil) {
        instance = [[super alloc] init];
    }
    return instance;
}

-(UINavigationController*) getNavigationController {
    return navController;
}

-(void)loadFirstVC{
    
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    [navController setNavigationBarHidden:NO];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"])
    {
        HomeViewController *homeVC1 = [[HomeViewController alloc] initExampleViewController];
        navController = [[UINavigationController alloc] initWithRootViewController:homeVC1];
        navController.navigationBar.translucent = NO;
        navController.navigationBar.barTintColor = [UIColor navColor];
        DEMOLeftMenuViewController *leftMenuViewController;
        if (IS_IPAD) {
            leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController_Ipad" bundle:nil];
            
        }
        else {
            leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController" bundle:nil];
        }
        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navController
                                                                        leftMenuViewController:leftMenuViewController
                                                                       rightMenuViewController:nil];
        sideMenuViewController.backgroundImage = [UIImage imageNamed:@"Stars"];
        _window.rootViewController = sideMenuViewController;
    }
    else{
        ViewController *_mainVC = [[ViewController alloc] init];
        navController = [[UINavigationController alloc] initWithRootViewController:_mainVC];
        DEMOLeftMenuViewController *leftMenuViewController;
        if (IS_IPAD) {
            leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController_Ipad" bundle:nil];
            
        }
        else {
            leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController" bundle:nil];
        }
        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navController
                                                                        leftMenuViewController:nil
                                                                       rightMenuViewController:nil];
        sideMenuViewController.backgroundImage = [UIImage imageNamed:@"Stars"];
        _window.rootViewController = sideMenuViewController;
        [navController setNavigationBarHidden:YES];
    }
}

-(void)PopToRootViewController{
    //      [navController popViewControllerAnimated:YES];
    NSArray *array = [navController viewControllers];
    [navController popToViewController:[array objectAtIndex:0] animated:YES];
}


-(void)MoveToHomeScreen {
    for (UIViewController *controller in navController.viewControllers) {
        //Do not forget to import
        if ([controller isKindOfClass:[HomeViewController class]]) {
            navController.navigationBar.barTintColor = [UIColor navColor];
            navController.navigationBar.translucent = NO;
            [navController setNavigationBarHidden:NO];
            [navController popToViewController:controller
                                      animated:YES];
            break;
        }
    }
}

-(void)NavigateToHomeScreen{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"newSignup"]) {
//        PopularUsersVC *users = [[PopularUsersVC alloc] init];
//        navController = [[UINavigationController alloc] initWithRootViewController:users];
//        [[[UIApplication sharedApplication]delegate] window].rootViewController = navController;
        [self MoveToPopularUsers];
    }else{
        navController = nil;
        if (IS_IPAD) {
//            HomeVC *homeVC1 = [[HomeVC alloc] initWithNibName:@"HomeVC_iPad" bundle:nil];
//            navController = [[UINavigationController alloc] initWithRootViewController:homeVC1];
            HomeViewController *homeVC1 = [[HomeViewController alloc] initExampleViewController];
            navController = [[UINavigationController alloc] initWithRootViewController:homeVC1];
        }
        else{
            HomeViewController *homeVC1 = [[HomeViewController alloc] initExampleViewController];
            navController = [[UINavigationController alloc] initWithRootViewController:homeVC1];
        }
        navController.navigationBar.translucent = NO;
        navController.navigationBar.barTintColor = [UIColor navColor];
        DEMOLeftMenuViewController *leftMenuViewController;
        if (IS_IPAD) {
            leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController_Ipad" bundle:nil];
            
        }
        else {
            leftMenuViewController = [[DEMOLeftMenuViewController alloc] initWithNibName:@"DEMOLeftMenuViewController" bundle:nil];
        }
        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navController
                                                                        leftMenuViewController:leftMenuViewController
                                                                       rightMenuViewController:nil];
        sideMenuViewController.backgroundImage = [UIImage imageNamed:@"Stars"];
        _window.rootViewController = sideMenuViewController;
    }
}

-(void)NavigateToLoginScreen{
    
    navController = nil;
    if (IS_IPAD) {
        
        ViewController *LoginVC1 = [[ViewController alloc] initWithNibName:@"ViewController_iPad" bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:LoginVC1];
        [navController popToViewController:LoginVC1 animated:YES];
        
    }
    
    else{
        
        ViewController *LoginVC2 = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:LoginVC2];
        [[[UIApplication sharedApplication]delegate] window].rootViewController = navController;
        
        [navController popToViewController:LoginVC2 animated:YES];
    }
    
    _window.rootViewController = navController;
    [navController setNavigationBarHidden:YES];
    
}

-(void)NavigateToSignUpScreen{
    
    [navController popToRootViewControllerAnimated:YES];
    
    if (IS_IPAD) {
        
        SignUpVC *_main1 = [[SignUpVC alloc] initWithNibName:@"SignUpVC_iPad" bundle:nil];
        [navController pushViewController:_main1 animated:YES];
    }else {
        SignUpVC *_main2 = [[SignUpVC alloc] initWithNibName:@"iPhone_5_6" bundle:nil];
        [navController pushViewController:_main2 animated:YES];
    }
    
    
}
-(void)MoveToTopics{
    
    [navController popToRootViewControllerAnimated:NO];
    
    if (IS_IPAD)
    {
        
        Topics *topic = [[Topics alloc] initWithNibName:@"Topics_iPad" bundle:nil];
        [navController pushViewController:topic animated:YES];
    }
    
    else
    {
        
        Topics *topic1 = [[Topics alloc] initWithNibName:@"Topics" bundle:nil];
        [navController pushViewController:topic1 animated:YES];
        
    }
    
}
-(void)MoveToChat{
    //[navController popToRootViewControllerAnimated:NO];
    [navController setNavigationBarHidden:YES];
    if (IS_IPAD) {
        
        ChatVC *myChat = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
        [navController pushViewController:myChat animated:YES];
    }else if (IS_IPHONEX){
        ChatVC *myChat = [[ChatVC alloc] initWithNibName:@"ChatVC_IphoneX" bundle:nil];
        [navController pushViewController:myChat animated:YES];
    }
    else{
        
        ChatVC *myChat = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
        [navController pushViewController:myChat animated:YES];
    }
}
-(void)MoveToMyBeam{
    [navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        MyBeam *myBeam = [[MyBeam alloc] initWithNibName:@"MyBeam_iPad" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
    else if (IS_IPHONEX){
        MyBeam *myBeam = [[MyBeam alloc] initWithNibName:@"MyBeam_IphoneX" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        
        MyBeam *myBeam = [[MyBeam alloc] initWithNibName:@"MyBeam" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
}

-(void)NavigateToRoot{
    
    [navController popToRootViewControllerAnimated:YES];
}

-(void)MoveToPlayer{
    
}
-(void)MoveToProfile{
    
    //[navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        ProfileVC *myBeam = [[ProfileVC alloc] initWithNibName:@"ProfileVC_iPad" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }else if (IS_IPHONEX){
        ProfileVC *myBeam = [[ProfileVC alloc] initWithNibName:@"ProfileVC_IphoneX" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
    else{
        
        ProfileVC *myBeam = [[ProfileVC alloc] initWithNibName:@"ProfileVC" bundle:nil];
        [navController pushViewController:myBeam animated:YES];
    }
}
-(void)MoveToComments{
    
    [navController popToRootViewControllerAnimated:NO];
    //    if (IS_IPAD) {
    //
    //        ProfileVC *myBeam = [[ProfileVC alloc] initWithNibName:@"ProfileVC_iPad" bundle:nil];
    //        [navController pushViewController:myBeam animated:YES];
    //    }
    //    else{
    //
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    [navController pushViewController:commentController animated:YES];
}

-(void)MoveToNotifications{
    
    [navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        NotificationsVC *notification = [[NotificationsVC alloc] initWithNibName:@"NotificationsVC_iPad" bundle:nil];
        [navController pushViewController:notification animated:YES];
    }else if (IS_IPHONEX){
        NotificationsVC *notification = [[NotificationsVC alloc] initWithNibName:@"NotificationsVC_IphoneX" bundle:nil];
        [navController pushViewController:notification animated:YES];
    }
    else{
        
        NotificationsVC *notification = [[NotificationsVC alloc] initWithNibName:@"NotificationsVC" bundle:nil];
        [navController pushViewController:notification animated:YES];
    }
}


-(void)MoveToSearchFriends{
    
    [navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        
        SearchFriendsVC *search = [[SearchFriendsVC alloc] initWithNibName:@"SearchFriendsVC_iPad" bundle:nil];
        [navController pushViewController:search animated:NO];
    }
    else{
        
        SearchFriendsVC *search = [[SearchFriendsVC alloc] initWithNibName:@"SearchFriendsVC" bundle:nil];
        [navController pushViewController:search animated:NO];
    }
}

-(void)MoveToPopularUsers{
    
    [navController popToRootViewControllerAnimated:NO];
    if (IS_IPAD) {
        PopularUsersVC *users = [[PopularUsersVC alloc] initWithNibName:@"PopularUsersVC_iPad" bundle:nil];
        [navController pushViewController:users animated:YES];
    }else if (IS_IPHONEX){
        PopularUsersVC *users = [[PopularUsersVC alloc] initWithNibName:@"PopularUsersVC_iPhoneX" bundle:nil];
        [navController pushViewController:users animated:YES];
    }
    else if(IS_IPHONE_6 || IS_IPHONE_5 || IS_IPHONE_6Plus){
        
        PopularUsersVC *users = [[PopularUsersVC alloc] initWithNibName:@"PopularUsersVC_iPhone6" bundle:nil];
        [navController pushViewController:users animated:YES];
    }
    
    else{
        
        PopularUsersVC *users = [[PopularUsersVC alloc] initWithNibName:@"PopularUsersVC" bundle:nil];
        [navController pushViewController:users animated:YES];
    }
}

#pragma mark MOVE TO SETTINGS
-(void)moveToSettingsVC{
    [navController popToRootViewControllerAnimated:NO];
    SettingsVC *settingsVC;
    if (IS_IPHONEX){
        settingsVC = [[SettingsVC alloc] initWithNibName:@"SettingsVC_IphoneX" bundle:nil];
    }else{
        settingsVC = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:nil];
    }

    
    [navController pushViewController:settingsVC animated:YES];
}

-(void)LogoutUser{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_LOGOUT,@"method",
                              token,@"Session_token",uniqueIdentifier,@"device_id", nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [self NavigateToLoginScreen];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        NSLog(@"%ld",(long)[(NSHTTPURLResponse *)response statusCode]);
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            [SVProgressHUD dismiss];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                
                appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                appDelegate.isLoggedIn = false;
                /**/
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"User_Name"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"User_Img"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"lastAnonymusBeam"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logged_in"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
}

-(void)MoveToBeamComment:(NSString*)postID andParentCommentID:(NSString *)pCmntId isComment:(BOOL)isComment{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopAllPlayingMedia" object:nil];
    
    CommentsVC *commentController ;
    if(IS_IPAD){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.cPostId       = postID;
        commentController.isFirstComment = !isComment;
        commentController.pID = pCmntId;
        commentController.isComment = isComment;
        commentController.isFromDeepLink = YES;
        [navController performSelector:@selector(pushViewController:animated:) withObject:commentController afterDelay:0.5];
//        [navController pushViewController:commentController animated:YES];
    }
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.cPostId       = postID;
        commentController.isFirstComment = !isComment;
        commentController.isComment = isComment;
        commentController.pID = pCmntId;
        commentController.isFromDeepLink = YES;
//        [navController pushViewController:commentController animated:YES];
        [navController performSelector:@selector(pushViewController:animated:) withObject:commentController afterDelay:0.5];
    }
    else{
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.cPostId       = postID;
        commentController.isFirstComment = !isComment;
        commentController.isComment = isComment;
        commentController.pID = pCmntId;
        commentController.isFromDeepLink = YES;
//        [navController pushViewController:commentController animated:YES];
        [navController performSelector:@selector(pushViewController:animated:) withObject:commentController afterDelay:0.5];
    }
}


-(void)MoveToLikeBeam:(VideoModel*)videoModel{
    
    CommentsVC *commentController ;
    if(IS_IPAD){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.postArray     = videoModel;
        commentController.cPostId       = videoModel.videoID;
        commentController.isFirstComment = TRUE;
        commentController.isComment     = FALSE;
        [navController pushViewController:commentController animated:YES];
    }
    if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.postArray     = videoModel;
        commentController.cPostId       = videoModel.videoID;
        commentController.isFirstComment = TRUE;
        commentController.isComment     = FALSE;
        [navController pushViewController:commentController animated:YES];
    }
    else{
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.postArray     = videoModel;
        commentController.cPostId       = videoModel.videoID;
        commentController.isFirstComment = TRUE;
        commentController.isComment     = FALSE;
        [navController pushViewController:commentController animated:YES];
    }
}
-(void)MoveToCommentsNotifi:(VideoModel *)videoModel second:(NSString*)parentID{
    CommentsVC *commentController ;
    if(IS_IPAD){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.postArray     = videoModel;
        commentController.cPostId       = videoModel.videoID;
        commentController.isFirstComment= TRUE;
        commentController.isComment     = FALSE;
        commentController.pID           = parentID;
        [navController performSelector:@selector(pushViewController:animated:) withObject:commentController afterDelay:0.5];
    }
   else if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
       commentController.commentsObj   = Nil;
       commentController.postArray     = videoModel;
       commentController.cPostId       = videoModel.videoID;
       commentController.isFirstComment= TRUE;
       commentController.isComment     = FALSE;
       commentController.pID           = parentID;
       [navController performSelector:@selector(pushViewController:animated:) withObject:commentController afterDelay:0.5];
    }
    else{
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.postArray     = videoModel;
        commentController.cPostId       = videoModel.videoID;
        commentController.isFirstComment= TRUE;
        commentController.isComment     = FALSE;
        commentController.pID           = parentID;
//        [navController pushViewController:commentController animated:YES];
        [navController performSelector:@selector(pushViewController:animated:) withObject:commentController afterDelay:0.5];

    }
}
-(void)MoveToCommentsOnCommentsNotifi:(VideoModel *)videoModel second:(NSString*)parentID{
    CommentsVC *commentController ;
    if(IS_IPAD){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.postArray     = videoModel;
        commentController.cPostId       = videoModel.cpost_id;
        commentController.isFirstComment= FALSE;
        commentController.isComment     = TRUE;
        commentController.pID           = parentID;
        [navController pushViewController:commentController animated:YES];
    }
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.postArray     = videoModel;
        commentController.cPostId       = videoModel.cpost_id;
        commentController.isFirstComment= FALSE;
        commentController.isComment     = TRUE;
        commentController.pID           = parentID;
        [navController pushViewController:commentController animated:YES];
    }
    else{
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.postArray     = videoModel;
        commentController.cPostId       = videoModel.cpost_id;
        commentController.isFirstComment= FALSE;
        commentController.isComment     = TRUE;
        commentController.pID           = parentID;
        [navController pushViewController:commentController animated:YES];
    }
}

-(void)MoveToUserChannel:(NSString*)FriendID friendModel:(Followings *)friendModel{
    UserChannel *commentController;
    if(IS_IPAD){
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
    }else if (IS_IPHONEX){
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
    }
    else
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
    commentController.ChannelObj = nil;
    commentController.friendID   = FriendID;
    commentController.currentUser = friendModel;
    [navController pushViewController:commentController animated:YES];
}

-(void)MoveToFriendChat:(Followings *)frnd{
    //    [navController setNavigationBarHidden:YES];
    FriendChatVC *friendChatVC; //= [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    if (IS_IPHONEX){
        friendChatVC = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC_IphoneX" bundle:nil];
    }else{
        friendChatVC = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    }
    friendChatVC.seletedFollowing = frnd;
    friendChatVC.isFriend  =YES;
    friendChatVC.isLocalSearch = YES;
    [navController pushViewController:friendChatVC animated:YES];
}

@end

