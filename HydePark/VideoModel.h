//
//  VideoModel.h
//  HydePark
//
//  Created by Apple on 19/02/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDHydepark.h"
#import "FLAnimatedImageView+PINRemoteImage.h"

@interface VideoModel : NSObject
@property (nonatomic) BOOL isLocal;

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSData *profileImageData;
@property (nonatomic, retain) NSString *comments_count;
@property (nonatomic, retain) NSString *userName;
@property (nonatomic, retain) NSString *topic_id;
@property (nonatomic, retain) NSString *user_id;
@property (nonatomic, retain) NSString *profile_image;
@property (strong, nonatomic) NSString *like_count;
@property (strong, nonatomic) NSString *like_by_me;
@property (nonatomic, retain) NSString *seen_count;
@property (nonatomic, retain) NSString *video_thumbnail_link;
@property (nonatomic, retain) NSString *post_seen_time;
@property (nonatomic, retain) NSString *viewed_by_me;
@property (nonatomic, retain) NSString *video_link;
@property (nonatomic, retain) NSString *m3u8_video_link;
@property (nonatomic, retain) NSString *video_length;
@property (nonatomic, retain) NSString *Tags;
@property (nonatomic, retain) NSString *message_Thread;
@property (nonatomic, retain) NSString *image_link;
@property (nonatomic, retain) NSString *is_anonymous;
@property (nonatomic, retain) NSString *videoID;
@property (nonatomic, retain) NSString *reply_count;
@property (nonatomic, retain) NSString *current_datetime;
@property (nonatomic, retain) NSString *uploaded_date;
@property (nonatomic, retain) NSString *isMute;
@property (nonatomic, retain) NSString *beam_share_url;
@property (nonatomic, retain) NSString *deep_link;
@property (nonatomic, retain) NSString *cpost_id;
@property (nonatomic, assign) int isThumbnailReq;
@property (nonatomic, strong) NSString *isCelebrity;
@property (nonatomic, strong) NSString *video_thumbnail_gif;
@property (nonatomic) NSInteger video_angle;
@property (nonatomic, retain) NSString *parentId;
@property (nonatomic, retain) NSMutableArray *likesArray;
@property (nonatomic, retain) NSString *privacy;
@property (nonatomic, strong) NSDictionary *originalBeamData;
@property (assign) int beamType;
@property (assign) int canDeleteComment;
@property (nonatomic, retain) CDHydepark *cdHydeparkModelObj;
@property  (nonatomic, strong) NSString *isViwedByMe;
@property  (nonatomic, strong) NSString *timestamp;
@property  (nonatomic, strong) NSString *order_timestamp;

@property (nonatomic, strong) UIImage *imageThumbnail;
@property (nonatomic, strong) FLAnimatedImage *animatedImage;

-(id)initWithDictionary:(NSDictionary *)_dictionary;
@end
