//
//  Utils.h
//  HydePark
//
//  Created by Ahmed Sadiq on 14/05/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Utils : NSObject

typedef void(^Completion)(BOOL finished);

+ (NSData*)encodeDictionary:(NSDictionary*)dictionary;
+ (NSString*)getTimeDifference:(NSString*)time1 time2:(NSString*)time2;
+ (NSString*)getFollowersRange:(NSString*)followersCount;
+ (NSString *) returnDate : (NSString*) dateToParse;
+ (NSString*)suffixNumber:(NSString*)numb;
+(NSString *)abbreviateNumber:(NSString *)numb withDecimal:(int)dec;
+(UIImage*) drawImage:(UIImage*) watermarkImage
              inImage:(UIImage*) backgroundImage
              atPoint:(CGPoint)  point;
+(void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock;
+(UIAlertController *) showCustomAlert:(NSString *) title andMessage:(NSString *)message yesButton:(NSString *)yesButton cancelButton:(NSString *)cancelButton;
+(NSString *)decodeForEmojis:(NSString *)toDecode;
+(NSString *)encodeForEmojis:(NSString *)toEncode;
+(void)sendUserToken;
+(void)showAlert:(NSString *)message;
+(NSString *)currentDeviceVersion;
+(BOOL)userIsCeleb;
+(NSMutableArray *)getSortedArray:(NSMutableArray *)arrayTobeSorted;
+(NSMutableDictionary *)getSortedDictionary:(NSMutableArray *)arrayTobeSorted;
+(void)removeProfileImg;
+(void)setChacedFriendsCount:(NSString *)count;
+(NSString *)getchachedFriendsCount;
+(void)clearUserDefaults;
+(void)setCachedBeamsCount:(NSString *)count;
+(NSString *)getcachedBeamsCount;
+(void)setcachedViewsCount:(NSString *)count;
+(NSString *)getCachedViewsCount;
+(void)setCachedFollowersCount:(NSString *)count;
+(NSString *)getCachedFollowersCount;
+(NSString *)getCachedInboxCount;
+(void)setCachedInboxCount:(NSString *)count;
+(void)setCachedPendingReqCount:(NSString *)count;
+(NSString *)getCachedPendingrReqCount;
+(NSMutableArray *)getFriendsToShareBeam;
+(void)shareBeamOnCorner:(NSString *)postId isComment:(NSString *)isComment friendId:(NSString *)friendId sharingOnFriendsCorner:(BOOL)sharingOnFriendsCorner;
+(NSString *)getTrimmedString:(NSString *)Name;
+(NSString *)getLanguageCode;
+(void)DownloadVideo:(NSString *)urlToDownload;
+(NSString *)simpleAbbNumber:(NSString *)numb withDecimal:(int)dec;
+(void)moveViewPosition:(CGFloat)yPostition onView:(UIView *)view completion:(Completion) completion;
+(NSString *) returnDateForInProgress : (NSString*) dateToParse;
+(NSString *) returnDateForInProgressOfMyCorner : (NSString*) dateToParse;
+(NSString *) getDecryptedTextFor : (NSString *)encryptedText;
+(NSMutableArray *)getSortedArrayDecrypted:(NSMutableArray *)arrayTobeSorted;
@end
