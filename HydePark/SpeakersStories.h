//
//  SpeakersStories.h
//  HydePark
//
//  Created by Osama on 11/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakersStories : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UIImageView *redBg;
@property (weak, nonatomic) IBOutlet UILabel *celebName;
@end
