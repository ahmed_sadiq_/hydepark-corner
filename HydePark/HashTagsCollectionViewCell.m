//
//  HashTagsCollectionViewCell.m
//  HydePark
//
//  Created by apple on 6/30/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import "HashTagsCollectionViewCell.h"

@implementation HashTagsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.outerLine.hidden = NO;
        }
        else
        {
            self.outerLine.hidden = YES;
        }
    }
    else
    {
        self.outerLine.hidden = YES;
    }
    self.outerLine.layer.masksToBounds = YES;
    self.outerLine.layer.cornerRadius = self.outerLine.frame.size.width / 9.3f;
    self.outerLine.layer.borderColor = [UIColor blueColor].CGColor;
    self.outerLine.layer.borderWidth = 1.0f;
}

-(void)prepareForReuse{
    [super prepareForReuse];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.outerLine.hidden = NO;
        }
        else
        {
            self.outerLine.hidden = YES;
        }
    }
    else
    {
        self.outerLine.hidden = YES;
    }
    
    // Then Reset here back to default values that you want.
}

@end
