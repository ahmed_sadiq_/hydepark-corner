
//  Created by TxLabz on 25/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.

//

#import "UITextField+Style.h"

@implementation UITextField (Style)

- (void) setLeftSpaceSize:(CGFloat) size
{
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size, 40.0)];
    [spaceView setBackgroundColor:[UIColor clearColor]];
    self.leftView = spaceView;
    self.leftViewMode = UITextFieldViewModeAlways;
    
}

- (void) setRightSpaceSize:(CGFloat) size
{
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size, 40.0)];
    [spaceView setBackgroundColor:[UIColor clearColor]];
    self.rightView = spaceView;
    self.rightViewMode = UITextFieldViewModeAlways;
}

@end
