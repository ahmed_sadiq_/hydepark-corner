//
//  ContactUsFormVC.m
//  HydePark
//
//  Created by Osama on 26/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "ContactUsFormVC.h"
#import "CustomLoading.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Utils.h"
@interface ContactUsFormVC ()
@property (weak, nonatomic) IBOutlet UITextView *feedbackView;
@end

@implementation ContactUsFormVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.feedbackView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(IBAction)sendBtnPressed:(id)sender{
    [self.view endEditing:YES];
    if(_feedbackView.text.length > 1){
        [CustomLoading showAlertMessage];
        [self serverCall];
    }else{
        [Utils showAlert:NSLocalizedString(@"pleaseProvideFeedback", @"")];
    }
}
#pragma mark ServerCall
-(void)serverCall{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"send_feedback",@"method",
                              token,@"session_token",self.feedbackView.text,@"feedback_text",nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [CustomLoading DismissAlertMessage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        if(success){
            [Utils showAlert:NSLocalizedString(@"yourFeedBackHelps", @"")];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            NSString *message = [responseObject objectForKey:@"message"];
            [Utils showAlert:message];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        [CustomLoading DismissAlertMessage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
