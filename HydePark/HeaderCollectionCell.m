//
//  HeaderCollectionCell.m
//  HydePark
//
//  Created by Babar Hassan on 10/4/17.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "HeaderCollectionCell.h"
#import <QuartzCore/QuartzCore.h>


@implementation HeaderCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.pendingReqCount.layer.masksToBounds = true;
    self.pendingReqCount.layer.cornerRadius = self.pendingReqCount.frame.size.height/2;
}

@end
