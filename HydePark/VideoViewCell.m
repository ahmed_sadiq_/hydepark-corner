//
//  VideoViewCell.m
//  HydePark
//
//  Created by Babar Hassan on 10/4/17.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "VideoViewCell.h"

@implementation VideoViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.view1.layer.masksToBounds = YES;
    self.view1.layer.cornerRadius = self.view1.frame.size.width / 6.2f;
   
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.outerLine.hidden = NO;
        }
        else
        {
            self.outerLine.hidden = YES;
        }
    }
    else
    {
        self.outerLine.hidden = YES;
    }
    self.outerLine.layer.masksToBounds = YES;
    self.outerLine.layer.cornerRadius = self.outerLine.frame.size.width / 6.2f;
    self.outerLine.layer.borderColor = [UIColor blueColor].CGColor;
    self.outerLine.layer.borderWidth = 1.0f;

    
}

-(void)prepareForReuse{
    [super prepareForReuse];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.outerLine.hidden = NO;
        }
        else
        {
            self.outerLine.hidden = YES;
        }
    }
    else
    {
        self.outerLine.hidden = YES;
    }
    
    // Then Reset here back to default values that you want.
}

@end
