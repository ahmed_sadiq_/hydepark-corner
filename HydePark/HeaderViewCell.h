//
//  HeaderViewCell.h
//  HydePark
//
//  Created by Ahmed Sadiq on 20/11/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *mainView;
@end
