//
//  ChatCell.h
//  HydePark
//
//  Created by Samreen Noor on 28/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface ChatCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *borderLine;
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIImageView *countBg;
@property (strong, nonatomic) IBOutlet UILabel *countLbl;
@property (strong, nonatomic) IBOutlet UIView *outline;

@end
