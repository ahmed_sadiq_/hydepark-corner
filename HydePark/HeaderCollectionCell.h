//
//  HeaderCollectionCell.h
//  HydePark
//
//  Created by Babar Hassan on 10/4/17.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface HeaderCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet AsyncImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIView *viewToRound;
@property (weak, nonatomic) IBOutlet UIView *picBorder;
@property (weak, nonatomic) IBOutlet UIButton *changeDp;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *noOfBeamslbl;
@property (weak, nonatomic) IBOutlet UILabel *noOfFriendslbl;
@property (weak, nonatomic) IBOutlet UILabel *inboxCount;
@property (weak, nonatomic) IBOutlet UILabel *celebCount;
@property (weak, nonatomic) IBOutlet UILabel *celebBeam;
@property (weak, nonatomic) IBOutlet UILabel *bioLbl;
@property (weak, nonatomic) IBOutlet UILabel *verifiedLbl;
@property (weak, nonatomic) IBOutlet UILabel *gridListLbl;
@property (weak, nonatomic) IBOutlet UIImageView *tickImg;
@property (weak, nonatomic) IBOutlet UIButton *beamsPressed;
@property (weak, nonatomic) IBOutlet UIButton *editProfileBtn;
@property (weak, nonatomic) IBOutlet UIButton *friendsPressed;
@property (weak, nonatomic) IBOutlet UIButton *inboxPressed;
@property (weak, nonatomic) IBOutlet UIButton *gridListBtn;
@property (weak, nonatomic) IBOutlet UIButton *inboxBtn;
@property (weak, nonatomic) IBOutlet UIButton *zoomBtn;
@property (weak, nonatomic) IBOutlet UIView *celebBottomView;
@property (weak, nonatomic) IBOutlet UIView *normalUserBottomView;
@property (weak, nonatomic) IBOutlet UIView *blueProfileView;
@property (weak, nonatomic) IBOutlet UIView *separatorView;


#pragma mark CelebView Outlets
@property (weak, nonatomic) IBOutlet UIButton *celebFriendsBtn;
@property (weak, nonatomic) IBOutlet UIButton *celebFollowersBtn;
@property (weak, nonatomic) IBOutlet UIButton *celebBeamsBtn;
@property (weak, nonatomic) IBOutlet UILabel *cFriendslbl;
@property (weak, nonatomic) IBOutlet UILabel *cBeamslbl;
@property (weak, nonatomic) IBOutlet UILabel *cFollowersLbl;
@property (weak, nonatomic) IBOutlet UILabel *celebUnreadCount;
@property (weak, nonatomic) IBOutlet UIImageView *celebDotforInboxNotifications;

@property (weak, nonatomic) IBOutlet UILabel *pendingReqCount;
@property (weak, nonatomic) IBOutlet UIImageView *dotforInboxNotifications;
@property (weak, nonatomic) IBOutlet UIImageView *gridImage;
@property (weak, nonatomic) IBOutlet UIImageView *inboxImage;
@property (weak, nonatomic) IBOutlet UIImageView *blackOverlay;
@property (weak, nonatomic) IBOutlet UIImageView *cover;


@end
