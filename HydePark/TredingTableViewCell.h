//
//  TredingTableViewCell.h
//  HydePark
//
//  Created by Osama on 31/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TredingTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *heading;
@property (strong, nonatomic) IBOutlet UILabel *desc;
@property (strong, nonatomic) IBOutlet UIButton *btnPressed;
@end
