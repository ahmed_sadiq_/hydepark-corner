//
//  FriendsCornerVC.m
//  HydePark
//
//  Created by Osama on 01/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "FriendsCornerVC.h"
#import "NewHomeCells.h"
#import "Constants.h"
#import "VideoModel.h"
#import "Utils.h"
#import "UIImageView+RoundImage.h"
#import "UIImageView+WebCache.h"
#import "NavigationHandler.h"
#import "CommentsVC.h"
#import "UIImage+JS.h"
#import "ApiManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "CustomLoading.h"
#import "FriendsVC.h"
#import "SpeakersStories.h"
#import "UIImageView+AFNetworking.h"
#import "LiveViewController.h"
#import <AVKit/AVKit.h>
#import "UIImage+animatedGIF.h"
#import "FLAnimatedImageView+PINRemoteImage.h"
#import "PreviewStreamController.h"
#import <Photos/Photos.h>
#import "VideoViewCell.h"
#import "StoriesCollectionViewCell.h"
#import "NSData+AES.h"
#import "StoriesViewController.h"


@interface FriendsCornerVC (){
    UIRefreshControl *refreshControl;
    NSMutableDictionary *serviceParams;
    CGRect cellRect;
    AVPlayerViewController *songPlayer;
    UICollectionView *tempCollectionView;
}
@end

@implementation FriendsCornerVC
- (id)init
{
    self = [super initWithNibName:@"FriendsCornerVC" bundle:Nil];
    sharedManager = [DataContainer sharedManager];
    sharedManager.pageNum = 1;
    [self getHomeContent];
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_collectionHome registerNib:[UINib nibWithNibName:@"VideoViewCell" bundle:nil] forCellWithReuseIdentifier:@"collectionCell"];
    [_collectionHome registerNib:[UINib nibWithNibName:@"StoriesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"storiescell"];
    self.collectionHome.delegate = self;
    self.collectionHome.dataSource = self;
    self.collectionHome.alwaysBounceVertical = YES;
    //[self.collectionHome addSubview:self.bgView];
    isScrolling = false;
    songPlayer = [[AVPlayerViewController alloc]init];

    _welcomeLbl.text = NSLocalizedString(@"welcome_to_your_timeline", nil);
//    _followPeopleLbl.text = NSLocalizedString(@"follow_people_personalize", nil);
    [_findFriendsBtn setTitle:NSLocalizedString(@"find_friends", nil) forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getHomeContent) name:@"ReloadSpeakers" object:nil];
    [self initFooterViewForHome];
    [self addrefreshControl];
    [self.storiesCollectionView registerNib:[UINib nibWithNibName:@"SpeakersStories" bundle:nil] forCellWithReuseIdentifier:@"speakersCell"];
    self.tblHome.tableHeaderView = self.bgView;
    [self.storiesCollectionView reloadData];
    self.tblHome.tableFooterView = footerViewHome;
    if(APP_DELEGATE.hasInet)
    {
        indicator.hidden = false;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(adjustForNightMode)
                                                 name:SET_NIGHT_MODE
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(clearNightMode)
                                                 name:CLEAR_NIGHT_MODE
                                               object:nil];
  //  self.tblHome.hidden = YES;
   // [(UIActivityIndicatorView *)[footerViewHome viewWithTag:790] startAnimating];
    [self refreshCall];
    
    
}

//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:YES];
//    for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
//        NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
//        if(indexPath.item > 0){
//            [cell.CH_Video_Thumbnail stopAnimating];
//        }
//    }
//}

-(void)adjustForNightMode
{
    refreshControl.tintColor = [UIColor grayColor];
    [self.collectionHome reloadData];
}

-(void)clearNightMode
{
    refreshControl.tintColor = [UIColor whiteColor];
    [self.collectionHome reloadData];
}


#pragma mark Find Friends
-(IBAction)findCornerBtnAction:(id)sender{
    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = nil;
    commentController.titles        = @"";
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 4;
    commentController.titles        = NSLocalizedString(@"action_search", nil);
    commentController.shudFetchFromServerOnly = true;
    commentController.isSearch = true;
    commentController.userId        = NSLocalizedString(@"action_search", nil);
    [[self navigationController] pushViewController:commentController animated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [self performSelector:@selector(startGifOnCell) withObject:nil afterDelay:0.2];
     [_collectionHome registerNib:[UINib nibWithNibName:@"VideoViewCell" bundle:nil] forCellWithReuseIdentifier:@"collectionCell"];
    [_collectionHome registerNib:[UINib nibWithNibName:@"StoriesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"storiescell"];
    if(sharedManager.isFriendViewChanged) {
        sharedManager.isFriendViewChanged = NO;
        [self refreshCall];
       // [self getHomeContent];
//        [_collectionHome reloadData];
    }
    
    if(sharedManager.isReloadMyCorner) {
        [self refreshCall];
        sharedManager.isReloadMyCorner = NO;
    }
    
    [tempCollectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addrefreshControl{
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            refreshControl.tintColor = [UIColor grayColor];
        }
        else
        {
            refreshControl.tintColor = [UIColor whiteColor];
        }
    }
    else
    {
        refreshControl.tintColor = [UIColor whiteColor];
    }
    [refreshControl addTarget:self
                       action:@selector(refreshCall)
             forControlEvents:UIControlEventValueChanged];
    //[self.tblHome addSubview:refreshControl];
    [self.collectionHome addSubview:refreshControl];
}

#pragma mark ActionSheet
-(void)handleLongPressForFriendsCorner:(UILongPressGestureRecognizer *)gestureRecognizer
{
    NSInteger tag = gestureRecognizer.view.tag;
    
    CGPoint p = [gestureRecognizer locationInView:self.collectionHome];//[gestureRecognizer locationInView:self.tblHome];
    NSIndexPath *indexPath = [self.collectionHome indexPathForItemAtPoint:p];
    cellRect = cellRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height, 200, 100);
    if (indexPath == nil) {
        
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        currentSelectedIndex = tag;
        [self presentAll:NO];
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        //NSLog(@"ENDED");
    }
}
- (IBAction)btnOptionsPressed:(UIButton *)sender {
    currentSelectedIndex = sender.tag;
    [self presentAll:NO];
}
- (IBAction)btnCopyLink:(id)sender {
    VideoModel *tempVideo;
    __block NSString *urlToShare;
    tempVideo = [sharedManager.newsfeedsVideos objectAtIndex:currentSelectedIndex];
    if(tempVideo.isThumbnailReq == 1){
        tempVideo.isThumbnailReq = 0;
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:tempVideo.videoID forKey:@"post_id"];
        [serviceParams setValue:@"0" forKey:@"is_comment"];
        
        [Utils downloadImageWithURL:[NSURL URLWithString:tempVideo.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                    
                }name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                    int flag = [[data objectForKey:@"success"] intValue];
                    if(flag){
                        urlToShare = (NSString *)[data objectForKey:@"deep_link"];
                        UIPasteboard *pb = [UIPasteboard generalPasteboard];
                        [pb setString: [NSString stringWithFormat:@"%@", urlToShare ]];
                        [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
                    }
                } failure:^(NSError *error) {
                    
                }];
            }
        }];
    }else{
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString: [NSString stringWithFormat:@"%@", tempVideo.deep_link ]];
        [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
    }
    
}

- (IBAction)shareOnFb:(id)sender {
    VideoModel *tempVideo;
    __block NSString *urlToShare;
    tempVideo = [sharedManager.newsfeedsVideos objectAtIndex:currentSelectedIndex];
    if(tempVideo.isThumbnailReq == 1){
        tempVideo.isThumbnailReq = 0;
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:tempVideo.videoID forKey:@"post_id"];
        [serviceParams setValue:@"0" forKey:@"is_comment"];
        
        [Utils downloadImageWithURL:[NSURL URLWithString:tempVideo.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                }
                                 name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                                     
                                     int flag = [[data objectForKey:@"success"] intValue];
                                     if(flag){
                                         urlToShare = (NSString *)[data objectForKey:@"deep_link"];
                                         FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                                         content.contentURL = [NSURL URLWithString:urlToShare];
                                         //content.imageURL   = [NSURL URLWithString:urlToShare];
                                         content.contentTitle = tempVideo.userName;
                                         content.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
                                         
                                         FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
                                         dialog.fromViewController = self;
                                         dialog.shareContent = content;
                                         dialog.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
                                         if (![dialog canShow]) {
                                             // fallback presentation when there is no FB app
                                             dialog.mode = FBSDKShareDialogModeFeedBrowser;
                                         }
                                         [dialog show];
                                     }
                                 } failure:^(NSError *error) {
                                     
                                 }];
            }
        }];
    }else{
        FBSDKShareLinkContent *contentw = [[FBSDKShareLinkContent alloc] init];
        contentw.contentURL = [NSURL URLWithString:tempVideo.deep_link];
        //content.imageURL   = [NSURL URLWithString:urlToShare];
        contentw.contentTitle = tempVideo.userName;
        contentw.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
        FBSDKShareDialog *dialogw = [[FBSDKShareDialog alloc] init];
        dialogw.fromViewController = self;
        dialogw.shareContent = contentw;
        dialogw.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
        if (![dialogw canShow]) {
            // fallback presentation when there is no FB app
            dialogw.mode = FBSDKShareDialogModeFeedBrowser;
        }
        [dialogw show];
    }
}

- (IBAction)shareOnTwitter:(id)sender {
    VideoModel *tempVideos;
    tempVideos = [sharedManager.newsfeedsVideos objectAtIndex:currentSelectedIndex];
    NSURL *imageURL = [NSURL URLWithString:tempVideos.video_thumbnail_link];
    [CustomLoading showAlertMessage];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            UIImage *thumbnailImg = [UIImage imageWithData:imageData];
            [CustomLoading DismissAlertMessage];
            
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                [mySLComposerSheet setInitialText:@"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech."];
                [mySLComposerSheet addURL:[NSURL URLWithString:tempVideos.beam_share_url]];
                [mySLComposerSheet addImage:thumbnailImg];
                [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                    
                    switch (result) {
                        case SLComposeViewControllerResultCancelled:
                            NSLog(@"Post Canceled");
                            break;
                        case SLComposeViewControllerResultDone:
                            NSLog(@"Post Sucessful");
                            break;
                            
                        default:
                            break;
                    }
                }];
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            else {
                
                NSString *message;
                NSString *title;
                NSString *cancel;
                message = NSLocalizedString(@"setupTwitterAccount", @"");
                title   = NSLocalizedString(@"accountConfiguration", @"");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                               delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        });
    });
}

-(void)presentAll:(BOOL)isForMycorner{
    int isCelebrity = [Utils userIsCeleb];
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* Share = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"share", @"")
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [self shareDialog];
                                [view dismissViewControllerAnimated:YES completion:nil];
                                
                            }];
    
    UIAlertAction *rebeam = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"rebeam", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 VideoModel *tempVideo;
                                 tempVideo = [sharedManager.newsfeedsVideos objectAtIndex:currentSelectedIndex];
                                 [self shareOnMyCorner:tempVideo.videoID];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                             }];
    UIAlertAction *postOnFriendsCorner = [UIAlertAction
                                          actionWithTitle:NSLocalizedString(@"post_on_friend_corner", @"")
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              if(!isCelebrity) {
                                                  VideoModel *tempVideo;
                                                  tempVideo = [sharedManager.newsfeedsVideos objectAtIndex:currentSelectedIndex];
                                                  [self shareOnFriendsCorner:tempVideo.videoID];
                                                  [view dismissViewControllerAnimated:YES completion:nil];
                                              }
                                          }];
    UIAlertAction *sharewithInApp = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"inbox_to_friend", @"")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         if(!isCelebrity) {
                                         VideoModel *tempVideo;
                                             tempVideo = [sharedManager.newsfeedsVideos objectAtIndex:currentSelectedIndex];
                                             [self showToshare:tempVideo.videoID];
                                             [view dismissViewControllerAnimated:YES completion:nil];
                                         }
                                     }];
    
    UIAlertAction* reportBeam = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"report_beam", @"")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self scReportUserPressed:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
    UIAlertAction* blockP   = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"block_user", @"")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self scBlockUser:self];
                                   [view dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"cancel", @"")
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction *downloadBeam = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"save_to_gallery", @"")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action){
                                       VideoModel *temp  = [sharedManager.newsfeedsVideos objectAtIndex:currentSelectedIndex];
                                       [Utils DownloadVideo:temp.video_link];
                                       [view dismissViewControllerAnimated:YES completion:nil];
                                   }];
    [view addAction:Share];
    [view addAction:rebeam];
    [view addAction:postOnFriendsCorner];
    [view addAction:sharewithInApp];
    [view addAction:downloadBeam];
    [view addAction:reportBeam];
    [view addAction:blockP];
    [view addAction:cancel];
    view.popoverPresentationController.sourceView = self.view;
    view.popoverPresentationController.sourceRect = cellRect;
    [self presentViewController:view animated:YES completion:nil];
}

#pragma mark Sheet Actions
-(void)showToshare:(NSString *)pOstID{
    FriendsVC *fVc;   // = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
//    if (IS_IPHONEX){
//        fVc    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        fVc    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    
    fVc.friendsArray  = [Utils getFriendsToShareBeam];
    fVc.titles        = NSLocalizedString(@"friends", @"");
    fVc.NoFriends     = FALSE;
    fVc.loadFollowings= 5;
    fVc.userId        = @"";
    fVc.postId = pOstID;
    fVc.isCommentVideo = @"0";
    fVc.isLocalSearch = YES;
    [[self navigationController] pushViewController:fVc animated:YES];
}

-(void)shareOnFriendsCorner:(NSString *)postId{
    FriendsVC *commentController; //  = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = [Utils getFriendsToShareBeam];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 6;
    commentController.userId        = @"";
    commentController.postId = postId;
    commentController.isCommentVideo = @"0";
    commentController.isLocalSearch = YES;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(void)shareOnMyCorner:(NSString *)pOstID{
    [Utils shareBeamOnCorner:pOstID isComment:@"0" friendId:@"" sharingOnFriendsCorner:NO];
}

#pragma mark Get Sharing Content
-(void)shareDialog{
    [self shareUrlCall];
}

-(void)shareUrlCall{
    VideoModel *tempVideo;
    tempVideo = [sharedManager.newsfeedsVideos objectAtIndex:currentSelectedIndex];
    [self showShareSheet:tempVideo.beam_share_url description:tempVideo.description];
    

}

-(void)showShareSheet:(NSString *)shareURL description:(NSString*)description{
    NSURL *url = [NSURL URLWithString:shareURL];
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[url]
     applicationActivities:nil];
    controller.excludedActivityTypes = @[UIActivityTypePrint,
                                         UIActivityTypeMail,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop,
                                         @"com.apple.mobilenotes.SharingExtension",
                                         @"com.apple.reminders.RemindersEditorExtension",
                                         ];
    controller.popoverPresentationController.sourceView = self.view;
    controller.popoverPresentationController.sourceRect = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300);
    [self presentViewController:controller animated:YES completion:nil];
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            // user shared an item
//            NSLog(@"We used activity type%@", activityType);
            if([activityType isEqualToString:@"com.apple.UIKit.activity.CopyToPasteboard"]){
                [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
            }
        } else {
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}


- (IBAction)scBlockUser:(id)sender {
    VideoModel *tempVideos ;
    tempVideos = [sharedManager.newsfeedsVideos objectAtIndex:currentSelectedIndex];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"blockUser",@"method",
                              token,@"session_token",tempVideos.user_id,@"blocking_user_id",language,@"language",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
                [alert show];            }
            
        }
        
        else{
        }
    }];
    
}

- (IBAction)scReportUserPressed:(id)sender {
    VideoModel *tempVideos ;
    tempVideos = [sharedManager.newsfeedsVideos objectAtIndex:currentSelectedIndex];
    [self reportUserForFriendsCorner:tempVideos];
}
- (void) reportUserForFriendsCorner :(VideoModel*) vModel {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"reportPost",@"method",
                              token,@"session_token",vModel.videoID,@"post_id",@"For No Reason",@"reason",language,@"language",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
}

#pragma mark Refresh Control
- (void)refreshCall
{
    if(APP_DELEGATE.hasInet) {
        cannotscroll = NO;
        sharedManager.pageNum = 1;
        [self getHomeContent];
    } else {
        [refreshControl endRefreshing];
    }
}

#pragma mark footerView
-(void)initFooterViewForHome{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat width = mainScreen.bounds.size.width;
    footerViewHome = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, 100.0)];
    UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    actInd.tag = 790;
    actInd.frame = CGRectMake(width/2 - 10 ,40.0, 20.0, 20.0);
    actInd.hidesWhenStopped = YES;
    [footerViewHome addSubview:actInd];
    actInd = nil;
}

#pragma mark Server Call
- (void) getHomeContent{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
            NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
            if(indexPath.item > 0){
                cell.mainThumbnail.hidden = NO;
                cell.CH_Video_Thumbnail.hidden = YES;
                [cell.CH_Video_Thumbnail stopAnimating];
            }
        }
    });
    self.fetchingContent = true;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",sharedManager.pageNum];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_HOME_CONTENTS,@"method",
                              token,@"session_token",pageStr,@"page_no",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        indicator.hidden = true;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            _footerIndicator.hidden = YES;
            [_footerIndicator stopAnimating];
            [refreshControl endRefreshing];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
            
//                [self parseVideoResponse:result];
                [self performSelector:@selector(parseVideoResponse:) withObject:result afterDelay:0.4];

            }
            if(success == 3){
                _footerIndicator.hidden = YES;
                [_footerIndicator stopAnimating];
                [self logoutUser];
                
            }
            
            if ([sharedManager.newsfeedsVideos count] == 0) {
                
                
            }else{
                _footerIndicator.hidden = YES;
                [_footerIndicator stopAnimating];
                
            }
        }
        else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            self.fetchingContent = false;
        }
        
    }];
    
    
}
#pragma mark COLLECTION VIEW DELEGATES
-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == self.collectionHome) {
        if(!self.fetchingContent && indexPath.row == sharedManager.newsfeedsVideos.count && !cannotscroll) {
            _footerIndicator.hidden = NO;
            [_footerIndicator startAnimating];
            sharedManager.pageNum++;
            [self getHomeContent];
        }
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if(collectionView == self.collectionHome)
        return 6.0;
    else
        return 0.0;
}

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//
//    return 0.0;
//}
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == tempCollectionView) {
         return self.friendsStoriesArray.count;
    }
//    if(collectionView == self.storiesCollectionView) {
//        return 0;// return self.friendsStoriesArray.count;
//    }
    else {
        return [sharedManager.newsfeedsVideos count] + 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if(collectionView == tempCollectionView) {
//        SpeakersStories *cell;
//        if (IS_IPAD) {
//            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"speakersCell" forIndexPath:indexPath];
//        }
//        else{
//            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"speakersCell" forIndexPath:indexPath];
//        }
//
//        VideoModel *vModel = [[VideoModel alloc]init];
//
//        vModel  = [self.friendsStoriesArray objectAtIndex:indexPath.row];
//        if(vModel.userName.length > 11){
//            cell.celebName.text = [Utils getTrimmedString:vModel.userName];
//        }
//        else{
//            cell.celebName.text = vModel.userName;
//        }
//
//        if([vModel.is_anonymous isEqualToString:@"1"]){
//            cell.profilePic.image = SET_IMAGE(@"anonymousDp.png");
//            cell.celebName.text = NSLocalizedString(@"anonymous_small", nil);
//
//            [cell.profilePic roundImageCorner];
//            [cell.profilePic addShadow];
//        }
//        else{
//            [cell.profilePic setImageWithURL:[NSURL URLWithString:vModel.profile_image] placeholderImage:nil];
//            [cell.profilePic roundImageCorner];
//            [cell.profilePic addShadow];
//    }
//    return cell;
//    }
    if(collectionView == self.collectionHome){
    
    //////////////////////// HOME COLLECTION VIEW (TAG = 2)//////////////////////
        if(indexPath.item == 0) {
            self.bgView.hidden = YES;
            StoriesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"storiescell" forIndexPath:indexPath];
            cell.collectionCell.delegate = self;
            cell.collectionCell.dataSource = self;
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
            {
                if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
                {
                    cell.viewBackGroundView.backgroundColor = [UIColor whiteColor];
                    cell.view1.hidden = false;
                    cell.view2.hidden = false;
                }
                else
                {
                }
            }
            else
            {
            }
            [cell.collectionCell registerNib:[UINib nibWithNibName:@"SpeakersStories" bundle:nil] forCellWithReuseIdentifier:@"speakersCell"];
            tempCollectionView = cell.collectionCell;
            return cell;
        }

        NSString *CellIdentifier = @"collectionCell";
        VideoViewCell *cell;
        currentIndexHome = indexPath.item-1;

        cell  = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        //}
        
       

        VideoModel *tempVideos = [[VideoModel alloc]init];
        if(sharedManager.newsfeedsVideos.count > 0){
            tempVideos  = [sharedManager.newsfeedsVideos objectAtIndex:currentIndexHome];
            cell.representedObject = tempVideos;
            if(sharedManager.isListView) {
                cell.CH_userName.font = [UIFont fontWithName:@"Montserrat-Regular" size:11];
                cell.CH_VideoTitle.font = [UIFont fontWithName:@"Montserrat-Regular" size:11];

                cell.durationLbl.hidden = NO;
                cell.durationLbl.text = tempVideos.video_length;
                cell.dotsImg.hidden = NO;
                [cell.btnOptions addTarget:self action:@selector(btnOptionsPressed:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnOptions.tag = indexPath.item;
            } else {
                cell.CH_userName.font = [UIFont fontWithName:@"Montserrat-Regular" size:9];
                cell.CH_VideoTitle.font = [UIFont fontWithName:@"Montserrat-Regular" size:9];
                cell.durationLbl.hidden = YES;
                cell.dotsImg.hidden = YES;
            }
            
            UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                  initWithTarget:self action:@selector(handleLongPressForFriendsCorner:)];
            lpgr.minimumPressDuration = 1.0;
            [cell.CH_commentsBtn addGestureRecognizer:lpgr];
            
            
            if(tempVideos.beamType == 1){
                if(sharedManager.isListView) {
                    cell.listRebeam.hidden = NO;
                    cell.rebeam1.hidden = YES;
                } else {
                    cell.listRebeam.hidden = YES;
                    cell.rebeam1.hidden = NO;
                }
            }
            else{
                cell.listRebeam.hidden = YES;
                cell.rebeam1.hidden = YES;
            }

            
            
            cell.CH_userName.text = [Utils getDecryptedTextFor:tempVideos.userName];
            
//            cell.CH_userName.text = tempVideos.userName;//[Utils decodeForEmojis:tempVideos.userName];
            if([tempVideos.is_anonymous isEqualToString:@"1"]) {
                cell.CH_userName.text = NSLocalizedString(@"anonymous_small", nil);
            }
            cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
            cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
            if([tempVideos.comments_count isEqualToString:@"0"])
            {
                cell.CH_CommentscountLbl.hidden = YES;
                cell.leftreplImg.hidden = YES;
            }
            else{
                cell.CH_CommentscountLbl.hidden = NO;
                cell.leftreplImg.hidden = NO;
                cell.CH_CommentscountLbl.text = tempVideos.comments_count;
            }
            
            cell.CH_heartCountlbl.text = tempVideos.like_count;
            cell.CH_seen.text = tempVideos.seen_count;
            cell.Ch_videoLength.text = tempVideos.video_length;
            if([tempVideos.is_anonymous  isEqualToString: @"0"]){
                cell.annonyOverlayLeft.hidden = YES;
                cell.annonyImgLeft.hidden = YES;
                //cell.CH_userName.text = @"Anonymous";
                cell.dummyLeft1.hidden  = YES;
            }
            else{
                //        cell.dummyLeft1.image = [UIImage imageNamed:@"btanonymous.png"];
                cell.annonyOverlayLeft.hidden = NO;
                cell.annonyImgLeft.hidden = NO;
                //cell.CH_userName.text = @"Anonymous";
                cell.dummyLeft1.hidden  = NO;
            }
            if(tempVideos.beamType == 1){
                cell.CH_heart.hidden = NO;
            }
            else if(tempVideos.beamType == 2){
                cell.CH_heart.hidden = YES;
                NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//                cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
                cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

            }
            else{
                cell.CH_heart.hidden = YES;
            }
            cell.mainThumbnail.hidden = NO;
            cell.CH_Video_Thumbnail.hidden = YES;
            NSData *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"myCornerThumbnail"];
            cell.mainThumbnail.hidden = NO;
            // cell.mainThumbnail.image = [UIImage imageWithData:temp];
            
            [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage: [UIImage imageWithData:temp]];
            [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (!error) {
                    tempVideos.imageThumbnail = image;
                    if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
                        
                        //  cell.CH_Video_Thumbnail.hidden = YES;
                        
                        if (tempVideos.imageThumbnail && tempVideos.animatedImage) {
                            cell.mainThumbnail.hidden = YES;
                            [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                            cell.CH_Video_Thumbnail.hidden = NO;
                        }else if (tempVideos.imageThumbnail && !tempVideos.animatedImage) {
                            [cell.mainThumbnail setImage:tempVideos.imageThumbnail];
//                            [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
//                                cell.mainThumbnail.hidden = NO;
//                                tempVideos.animatedImage = result.animatedImage;
//                                [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
//                                cell.CH_Video_Thumbnail.hidden = NO;
//                            }];
//                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//
//                                [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                                }];
//
//                            });
                        }else if (!tempVideos.imageThumbnail && tempVideos.animatedImage) {
                            //                        cell.mainThumbnail.hidden = YES;
                            [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                            cell.CH_Video_Thumbnail.hidden = NO;
                        }else if (!tempVideos.imageThumbnail && !tempVideos.animatedImage) {
                            
                            [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                if (!error) {
                                    
                                    tempVideos.imageThumbnail = image;
//                                    [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
//                                        cell.mainThumbnail.hidden = NO;
//                                        tempVideos.animatedImage = result.animatedImage;
//                                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
//                                        cell.mainThumbnail.hidden = YES;
//                                        cell.CH_Video_Thumbnail.hidden = NO;
//                                    }];
                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                        
                                        [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                
                                                cell.mainThumbnail.hidden = YES;
                                                cell.CH_Video_Thumbnail.hidden = NO;
                                                [cell.CH_Video_Thumbnail startAnimating];
                                            });
                                            
                                        }];
                                        
                                    });
                                }else{
//                                    [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
//                                        cell.mainThumbnail.hidden = NO;
//                                        tempVideos.animatedImage = result.animatedImage;
//                                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
//                                        cell.mainThumbnail.hidden = NO;
//                                        cell.CH_Video_Thumbnail.hidden = NO;
//                                    }];
                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                        
                                        [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                
                                                cell.mainThumbnail.hidden = YES;
                                                cell.CH_Video_Thumbnail.hidden = NO;
                                                [cell.CH_Video_Thumbnail startAnimating];
                                            });
                                            
                                        }];
                                        
                                    });
                                }
                            }];
                            
                            
                            
                        }
                        
                        
                        
                        
                    }else{
                        
                        
                        cell.mainThumbnail.hidden = NO;
                        cell.CH_Video_Thumbnail.hidden = YES;
                        
                        [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            if (!error) {
                                tempVideos.imageThumbnail = image;
                                
                            }else{
                                
                                NSLog(@"%@", error);
                                
                            }
                            
                        }];
                        
                    }
                    
                }else{
                    
                    NSLog(@"%@", error);
                    
                }
                
            }];
            
            
            
            cell.CH_commentsBtn.enabled = YES;
            if (tempVideos.isLocal && !APP_DELEGATE.hasInet) {
                
                [cell.cellProgress removeFromSuperview];
                cell.CH_commentsBtn.backgroundColor = [UIColor colorWithRed:111/255 green:113/255 blue:121/255 alpha:1];
                cell.CH_commentsBtn.alpha = 0.55;
                cell.CH_VideoTitle.text = tempVideos.current_datetime;
                cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
                cell.CH_CommentscountLbl.text = @"!";
                cell.leftreplImg.hidden =  NO;
                cell.CH_commentsBtn.enabled = NO;
                cell.mainThumbnail.hidden = NO;
                // cell.mainThumbnail.image = [UIImage imageWithData:_offlineThumbnailArray[currentIndexHome]];
                if (cell.cellProgress!=nil){
                    
                    cell.cellProgress.hidden = true;
                    [cell.cellProgress removeFromSuperview];
                    cell.cellProgress = nil;
                }
            }
            else if(tempVideos.isLocal){
                
                //self.roundedPView.hidden = NO;
                
                
                cell.CH_commentsBtn.enabled = NO;
                cell.leftreplImg.hidden =  YES;
                cell.CH_VideoTitle.text = tempVideos.current_datetime;
                cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
                
                if(![cell.cellProgress isDescendantOfView:cell.contentView]) {
                    
                    //                cell.cellProgress = [[PWProgressView alloc] initWithFrame:cell.view1.frame];
                    
                    if(IS_IPHONE_5) {
                        if(sharedManager.isListView) {
                            cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, _collectionHome.frame.size.width, 250)];
                        } else {
                            cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, 100, 90)];
                        }
                    } else if(IS_IPAD){
                        cell.cellProgress = [[PWProgressView alloc] init];
                        cell.cellProgress.frame = cell.view1.frame;
                    }
                    else {
                        if(sharedManager.isListView) {
                            cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, _collectionHome.frame.size.width, 250)];
                        } else {
                            cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, 127, 125)];
                        }
                    }
                    if(!sharedManager.isListView) {
                        cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 6.2f;
                        cell.cellProgress.clipsToBounds = YES;
                    }
                    
                    cell.cellProgress.hidden = false;
                    
                    
                    
                    //self.roundedPView = cell.cellProgress;
                    
                    [cell.contentView addSubview:cell.cellProgress];
                    
                }else{
                    
                    
                }
                
                
            }
            else{
                cell.CH_commentsBtn.enabled = YES;
                cell.CH_commentsBtn.backgroundColor = [UIColor clearColor];
                
                if (cell.cellProgress!=nil){
                    
                    cell.cellProgress.hidden = true;
                    [cell.cellProgress removeFromSuperview];
                    cell.cellProgress = nil;
                    
                }
                
            }
            
            
            
            if(IS_IPAD)
                //cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /7.4f;
                cell.imgContainer.layer.masksToBounds = YES;
            if(sharedManager.isListView) {
                
            } else {
//                [cell.CH_Video_Thumbnail roundCorners];
            }
//            [cell.CH_Video_Thumbnail roundCorners];
            [cell.view1 setBackgroundColor:[UIColor clearColor]];
            
            [cell.CH_heart setTag:currentIndexHome];
            [cell.CH_playVideo setTag:currentIndexHome];
            
            [cell.CH_flag setTag:currentIndexHome];
            [cell.CH_commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CH_commentsBtn setTag:currentIndexHome];
            
            
            [cell setBackgroundColor:[UIColor clearColor]];
            //cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if(!tempVideos.isLocal){
                
                [cell.cellProgress removeFromSuperview];
                
            }
            
            cell.blueOverlay.hidden = YES;
            if(sharedManager.newsfeedsVideos.count == 1 && !sharedManager.isListView) {
                cell.frame = CGRectMake(5, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
            }
            [cell.CH_Video_Thumbnail stopAnimating];
        }
        
        cell.mainThumbnail.hidden = NO;
        cell.CH_Video_Thumbnail.hidden = YES;
        [cell.CH_Video_Thumbnail stopAnimating];
        
        return cell;
        
        
    } else{
        
        SpeakersStories *cell;
        if (IS_IPAD) {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"speakersCell" forIndexPath:indexPath];
        }
        else{
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"speakersCell" forIndexPath:indexPath];
        }
        
        VideoModel *vModel = [[VideoModel alloc]init];
        NSInteger itemIndex = indexPath.row;
        if(!indexPath.row || indexPath.row >= self.friendsStoriesArray.count) {
            itemIndex = 0;
        }
        vModel  = [self.friendsStoriesArray objectAtIndex:itemIndex];
        
        NSString *decodedString = [Utils getDecryptedTextFor:vModel.userName];

        if(decodedString.length > 11){
            cell.celebName.text = [Utils getTrimmedString:decodedString];
        }
        else{
            cell.celebName.text = decodedString;
        }
        
//        if(vModel.userName.length > 11){
//            cell.celebName.text = [Utils getTrimmedString:vModel.userName];
//        }
//        else{
//            cell.celebName.text = vModel.userName;
//        }
        
        if([vModel.is_anonymous isEqualToString:@"1"]){
            cell.profilePic.image = SET_IMAGE(@"anonymousDp.png");
            cell.celebName.text = NSLocalizedString(@"anonymous_small", nil);
            
            [cell.profilePic roundImageCorner];
            [cell.profilePic addShadow];
        }
        else{
            [cell.profilePic setImageWithURL:[NSURL URLWithString:vModel.profile_image] placeholderImage:[UIImage imageNamed:@"place_holder"]];
            [cell.profilePic roundImageCorner];
            [cell.profilePic addShadow];
        }
        
        
        if([vModel.viewed_by_me isEqualToString:@"1"]){
            cell.redBg.image = [UIImage imageNamed:@"bg_light_gray_cicle.png"];
        }
        else
        {
            cell.redBg.image = [UIImage imageNamed:@"bg_gradient_red.png"];
        }
        
        return cell;
        
    }
    
    
    
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize sz;
    float returnValue;
    if(collectionView == self.storiesCollectionView) {
        sz = CGSizeMake(90, 100);
        return sz;
    } else if(collectionView == self.collectionHome){
        if(indexPath.item == 0) {
            return CGSizeMake(collectionView.frame.size.width, 100);
        }
        if (IS_IPHONE_5)
            returnValue = ((collectionView.frame.size.width-12)/3) ;
        else
            returnValue = ((collectionView.frame.size.width-12)/3) ;
        if(sharedManager.isListView) {
            return CGSizeMake(_collectionHome.frame.size.width, 250);
        }
        return CGSizeMake(returnValue, returnValue);
    }
    return CGSizeMake(90, 100);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView == self.collectionHome) {
        VideoModel *_model = [sharedManager.newsfeedsVideos objectAtIndex:indexPath.row];
        CommentsVC *commentController;
        if(IS_IPAD)
            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
        else  if (IS_IPHONEX){
            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
        }
        else
            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
        commentController.commentsObj   = Nil;
        commentController.postArray     = _model;
        commentController.cPostId       =  _model.videoID;;
        commentController.isFirstComment = TRUE;
        commentController.isComment     = FALSE;
        
        [[self navigationController] pushViewController:commentController animated:YES];
    } else {
//        VideoModel *_model = [self.friendsStoriesArray objectAtIndex:indexPath.row];
//        CommentsVC *commentController;
//        if(IS_IPAD)
//            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
//        else  if (IS_IPHONEX){
//            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
//        }
//        else
//            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
//        commentController.commentsObj   = Nil;
//        commentController.postArray     = _model;
//        commentController.cPostId       =  _model.videoID;;
//        commentController.isFirstComment = TRUE;
//        commentController.isComment     = FALSE;
//
//        [[self navigationController] pushViewController:commentController animated:YES];
        
        VideoModel *_model = [self.friendsStoriesArray objectAtIndex:indexPath.row];
        StoriesViewController * vc = [[StoriesViewController alloc] init];
        vc.storiesArray = self.friendsStoriesArray;
        vc.videoModel = _model;
        vc.isFromSpeakers = false;
        vc.isFirstComment = true;
        vc.indexRow = indexPath.row;
        [[self navigationController] pushViewController:vc animated:YES];

    }

}



#pragma mark ScrollView Delegates
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if (CGRectGetMaxY(scrollView.bounds) == scrollView.contentSize.height) {
//        _footerIndicator.hidden = NO;
//    } else {
//        _footerIndicator.hidden = YES;
//        [_footerIndicator stopAnimating];
//    }
//}
//-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
//                    withVelocity:(CGPoint)velocity
//             targetContentOffset:(inout CGPoint *)targetContentOffset{
//
//
//    if (velocity.y > 0){
//        self.isDownwards = YES;
//        // [self hideBottomBar];
//    }
//    if (velocity.y < 0){
//        self.isDownwards = NO;
//        //[self showBottomBar];
//    }
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    /// TO PLAY GIF FOR VISIBLE CELLS
//    for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
//        NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
//        if(indexPath.item > 0){
//            [cell.CH_Video_Thumbnail startAnimating];
//        }
//    }
//    ///////
//    BOOL endOfTable = (scrollView.contentOffset.y >= ((sharedManager.newsfeedsVideos.count/3 * 130.0f) - scrollView.frame.size.height)); // Here 150 is row height
//    if (endOfTable && !self.fetchingContent && !scrollView.dragging && !scrollView.decelerating && !cannotscroll && self.isDownwards){
//        self.tblHome.tableFooterView = footerViewHome;
//        [(UIActivityIndicatorView *)[footerViewHome viewWithTag:790] startAnimating];
//    }
//    if (cannotscroll){
//        self.tblHome.tableFooterView = Nil;
//    }
//}
//
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    /// TO PLAY GIF FOR VISIBLE CELLS
//    if(!decelerate) {
//        for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
//            NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
//            if(indexPath.item > 0){
//                [cell.CH_Video_Thumbnail startAnimating];
//            }
//        }
//    }
//}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        isScrolling = true;
        for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
            NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
            if(indexPath.item > 0){
                cell.mainThumbnail.hidden = NO;
                cell.CH_Video_Thumbnail.hidden = YES;
                [cell.CH_Video_Thumbnail stopAnimating];
            }
        }
    });
}
-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                    withVelocity:(CGPoint)velocity
             targetContentOffset:(inout CGPoint *)targetContentOffset{
    NSLog(@"here");
    if (velocity.y > 0){
        self.isDownwards = YES;
        // [self hideBottomBar];
    }
    if (velocity.y < 0){
        self.isDownwards = NO;
        //[self showBottomBar];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"here 2");
    isScrolling = false;
    if(!self.fetchingContent)
    {
        [self startGifOnCell];
    }
    /// TO PLAY GIF FOR VISIBLE CELLS
    //    for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
    //        /// TO PLAY GIF FOR VISIBLE CELLS
    //        NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
    //        if(indexPath.item > 0){
    //            [cell.CH_Video_Thumbnail startAnimating];
    //        }
    //    }
    //    ///////
    //    BOOL endOfTable = (scrollView.contentOffset.y >= ((sharedManager.forumsVideo.count/3 * 130.0f) - scrollView.frame.size.height)); // Here 150 is row height
    //    if (endOfTable && !self.fetchingContent && !scrollView.dragging && !scrollView.decelerating && !cannotscroll && self.isDownwards){
    //        self.tblHome.tableFooterView = footerViewHome;
    //        [(UIActivityIndicatorView *)[footerViewHome viewWithTag:790] startAnimating];
    //    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    /// TO PLAY GIF FOR VISIBLE CELLS
    if(!decelerate) {
        //        for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
        //            NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
        //            if(indexPath.item > 0){
        //                [cell.CH_Video_Thumbnail startAnimating];
        //            }
        //        }
        //
        //
        //                            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC));
        //                            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"here 333");
        isScrolling = false;
        if(!self.fetchingContent)
        {
            [self startGifOnCell];
        }
        //                            });
    }
    //    for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
    //        NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
    //        if(indexPath.item > 0){
    //            [cell.CH_Video_Thumbnail startAnimating];
    //        }
    //    }
}

- (void)startGifOnCell {
    
    if(!self.fetchingContent && !isScrolling)
    {
        NSLog(@"starting gifs...");
        dispatch_async(dispatch_get_main_queue(), ^{
            int counter = 0;
            for (counter = 0; counter < [self.collectionHome visibleCells].count; counter ++) {
                
                VideoViewCell *cell = [[self.collectionHome visibleCells] objectAtIndex:counter];
                NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
                VideoModel *tempVideos = [[VideoModel alloc]init];
                if(indexPath.row > 0 && indexPath.row < sharedManager.newsfeedsVideos.count)
                {
                    tempVideos  = [sharedManager.newsfeedsVideos objectAtIndex:indexPath.row - 1];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if(cell.CH_Video_Thumbnail.animatedImage == nil)
                                {
                                    cell.mainThumbnail.hidden = NO;
                                    cell.CH_Video_Thumbnail.hidden = YES;
                                    [cell.CH_Video_Thumbnail stopAnimating];
                                }
                                else
                                {
                                    cell.mainThumbnail.hidden = YES;
                                    cell.CH_Video_Thumbnail.hidden = NO;
                                    [cell.CH_Video_Thumbnail startAnimating];
                                }
                                
                            });
                            
                        }];
                        
                    });
                }
            }
        });
    }
}

#pragma mark TABLE VIEW DELEGATES


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!self.fetchingContent && indexPath.row == sharedManager.newsfeedsVideos.count / 3 - 1 && !cannotscroll) {
        sharedManager.pageNum++;
        [self getHomeContent];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    float returnValue;
    if (IS_IPAD)
        returnValue = 260.0f;
    else if(IS_IPHONE_5)
        returnValue = 110.0f;
    else if(IS_IPHONE_6Plus){
        returnValue = 145.0;
    }
    else
        returnValue = 130.0f;
    return returnValue;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    int rows = (int)([sharedManager.newsfeedsVideos count]/3);
//    if([sharedManager.newsfeedsVideos count] % 3 == 1 || [sharedManager.newsfeedsVideos count] % 3 == 2) {
//        rows = rows + 1;
//    }
//    return rows;
    return 0;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 40;
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"videoCells";
    NewHomeCells *cell;
    currentIndexHome = (indexPath.row * 3);
//     cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (!cell) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects;
        if(IS_IPAD){
//            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells_iPad" owner:self options:nil];
        }else if(IS_IPHONE_5){

            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
        }else{
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
        }
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
        //            cell.leftreplImg.hidden =  NO;
        //            cell.rightreplImg.hidden = NO;
    }else{
        cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    
    VideoModel *tempVideos = [[VideoModel alloc]init];
    tempVideos  = [sharedManager.newsfeedsVideos objectAtIndex:currentIndexHome];
    if(tempVideos.beamType == 1){
        cell.rebeam1.hidden = NO;
    }
    else{
        cell.rebeam1.hidden = YES;
    }
    cell.CH_userName.text = tempVideos.userName;
    
    cell.Ch_videoLength.text = tempVideos.video_length;
    cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
    cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
    
    if([tempVideos.comments_count isEqualToString:@"0"])
    {
        cell.CH_CommentscountLbl.hidden = YES;
        cell.leftreplImg.hidden = YES;
    }
    else{
        cell.CH_CommentscountLbl.text = tempVideos.comments_count;
    }
    cell.CH_heartCountlbl.text = tempVideos.like_count;
    cell.CH_seen.text = tempVideos.seen_count;
    //[cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
    
    if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
        
        
        
        cell.thumbNail1.hidden = YES;
        cell.CH_Video_Thumbnail.hidden = NO;
        
          [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif]];
        
//        });
        
        
        
        
    }else{
        
        
        cell.thumbNail1.hidden = NO;
        cell.CH_Video_Thumbnail.hidden = YES;
        
        [cell.thumbNail1 sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        
        
        
    }
    if([tempVideos.is_anonymous  isEqualToString: @"0"]){
        cell.dummyLeft1.hidden = YES;
        cell.annonyOverlayLeft.hidden = YES;
        cell.annonyImgLeft.hidden = YES;
    }
    else{
        
        cell.annonyOverlayLeft.hidden = NO;
        cell.annonyImgLeft.hidden = NO;
        cell.dummyLeft1.hidden = NO;
        cell.CH_userName.text = @"Anonymous";
        cell.userProfileView.enabled = false;
    }
    if(tempVideos.beamType == 1){
        cell.CH_heart.hidden = NO;
    }
    else if(tempVideos.beamType == 2){
        cell.CH_heart.hidden = YES;
        NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

    }
    else{
        cell.CH_heart.hidden = YES;
    }
    
    //    cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /6.2f;
    if(IS_IPAD)
        //cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /7.4f;
    cell.imgContainer.layer.masksToBounds = YES;
//    [cell.CH_Video_Thumbnail roundCorners];
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPressForFriendsCorner:)];
    lpgr.minimumPressDuration = 1.0;
    [cell.CH_commentsBtn addGestureRecognizer:lpgr];
    [cell.CH_playVideo setTag:currentIndexHome];
    cell.CH_commentsBtn.enabled = YES;
    cell.CH_RcommentsBtn.enabled = YES;
    [cell.CH_commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.CH_commentsBtn setTag:currentIndexHome];
    currentIndexHome++;
    if(currentIndexHome < sharedManager.newsfeedsVideos.count)
    {
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [sharedManager.newsfeedsVideos objectAtIndex:currentIndexHome];
        if(tempVideos.beamType == 1){
            cell.rebeam2.hidden = NO;
        }
        else{
            cell.rebeam2.hidden = YES;
        }
        [cell.CH_RcommentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CH_RcommentsBtn setTag:currentIndexHome];
        cell.rightTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
        cell.CH_RVideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.CH_Rseen.text = tempVideos.seen_count;
        if(IS_IPAD)
            cell.RimgContainer.layer.cornerRadius  = cell.RimgContainer.frame.size.width /7.4f;
        cell.RimgContainer.layer.masksToBounds = YES;
        [cell.CH_RVideo_Thumbnail roundCorners];
        
        
        cell.CH_RheartCountlbl.text             = tempVideos.like_count;
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.CH_RCommentscountLbl.hidden = YES;
            cell.rightreplImg.hidden = YES;
        }
        else{
            cell.CH_RCommentscountLbl.text = tempVideos.comments_count;
        }
        cell.CH_RuserName.text = tempVideos.userName;
        
        //[cell.CH_RVideo_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        
        if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){



            cell.thumbNail2.hidden = YES;
            cell.CH_RVideo_Thumbnail.hidden = NO;

//            [cell.CH_RVideo_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif]];
            @try {
                FLAnimatedImageView __block *gifImage = nil;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [gifImage pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        cell.CH_RVideo_Thumbnail.image = gifImage.image;
                        
                    });
                });
            }
            @catch (NSException *exception) {
                NSLog(@"Exception :%@",exception.debugDescription);
            };



        }else{
        
            
            cell.thumbNail2.hidden = NO;
            cell.CH_RVideo_Thumbnail.hidden = YES;
            
            [cell.thumbNail2 sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"pH"]];
            
        }
    
        
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPressForFriendsCorner:)];
        lpgr.minimumPressDuration = 1.0;
        [cell.CH_RcommentsBtn addGestureRecognizer:lpgr];
        
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.dummyright1.hidden = YES;
            cell.anonyImgRight.hidden = YES;
            cell.anonyOverlayRight.hidden = YES;
        }
        else{
            //            cell.dummyright1.image = [UIImage imageNamed:@"btanonymous.png"];
            cell.anonyImgRight.hidden = NO;
            cell.anonyOverlayRight.hidden = NO;
            cell.dummyright1.hidden = NO;
            cell.CH_RuserName.text = @"Anonymous";
        }
        if(tempVideos.beamType == 1){
            cell.CH_Rheart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            cell.CH_Rheart.hidden = YES;
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.CH_RuserName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        else{
            cell.CH_Rheart.hidden = YES;
        }
        
        currentIndexHome++;
    }
    else{
        cell.view2.hidden = YES;
        cell.rightreplImg.hidden     = YES;
        cell.CH_RCommentscountLbl.hidden = YES;
    }
    if(currentIndexHome < sharedManager.newsfeedsVideos.count){
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [sharedManager.newsfeedsVideos objectAtIndex:currentIndexHome];
        if(tempVideos.beamType == 1){
            cell.rebeam3.hidden = NO;
        }
        else{
            cell.rebeam3.hidden = YES;
        }
        cell.rightDate.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.rightUsername.text = tempVideos.userName;
        //[cell.rightThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        
        if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
            
            
            
            cell.thumbNail3.hidden = YES;
            cell.rightThumbnail.hidden = NO;
            
//            [cell.rightThumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif]];
            @try {
                FLAnimatedImageView __block *gifImage = nil;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [gifImage pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        cell.rightThumbnail.image = gifImage.image;
                        
                    });
                });
            }
            @catch (NSException *exception) {
                NSLog(@"Exception :%@",exception.debugDescription);
            };
            
        }else{
            
            
            cell.thumbNail3.hidden = NO;
            cell.rightThumbnail.hidden = YES;
            
            [cell.thumbNail3 sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
            
        }
        
        [cell.rightThumbnail roundCorners];
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.replyRedBg.hidden = YES;
            cell.replyCountlbl.hidden = YES;
        }
        else{
            cell.replyRedBg.hidden = NO;
            cell.replyCountlbl.hidden = NO;
            cell.replyCountlbl.text = tempVideos.comments_count;
        }
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.dummyright1.hidden = YES;
            cell.anonyImgExtRight.hidden = YES;
            cell.anonyOverlayRight.hidden = YES;
        }
        else{
            cell.anonyImgExtRight.hidden = NO;
            cell.anonyOverlayRight.hidden = NO;
            cell.dummyright1.hidden = NO;
            cell.rightUsername.text = @"Anonymous";
        }
        if(tempVideos.beamType == 1){
            cell.CH_Rheart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.rightUsername.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        
        
        if([tempVideos.isMute isEqualToString:@"0"] && [tempVideos.is_anonymous isEqualToString: @"1"]){
            //            cell.dummyLeft1.image        = [UIImage imageNamed:@"btanonymous.png"];
            
            cell.dummy1.hidden       = NO;
        }
        else if([tempVideos.isMute isEqualToString:@"1"] && [tempVideos.is_anonymous isEqualToString: @"1"])
        {
            cell.dummy1.hidden = NO;
            cell.dummy2.hidden = NO;
            cell.dummy2.image  = [UIImage imageNamed:@"speaker-mute.png"];
            //            cell.dummyLeft1.image  = [UIImage imageNamed:@"btanonymous.png"];
            
        }
        else if([tempVideos.isMute isEqualToString: @"1"] && [tempVideos.is_anonymous isEqualToString: @"0"]){
            cell.dummy1.hidden = NO;
            cell.dummy1.image  = [UIImage imageNamed:@"speaker-mute.png"];
        }
        else{
            cell.dummy2.hidden = YES;
            cell.dummy1.hidden = YES;
        }
        
        [cell.showCommentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.showCommentsBtn setTag:currentIndexHome];
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPressForFriendsCorner:)];
        lpgr.minimumPressDuration = 1.0;
        [cell.showCommentsBtn addGestureRecognizer:lpgr];
        currentIndexHome++;
    }
    else{
        cell.view3.hidden = YES;
    }
    if([tempVideos.is_anonymous isEqualToString:@"1"]) {
        cell.CH_userName.text = NSLocalizedString(@"anonymous_small", nil);
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(UIViewController*) topMostController
{
    NSArray *controllers = [self.navigationController viewControllers];
    
    return [controllers lastObject];
}

#pragma mark Get Comments
-(void) ShowCommentspressed:(UIButton *)sender{
    UIButton *CommentsBtn = (UIButton *)sender;
    currentIndexHome = CommentsBtn.tag;
    VideoModel *_model = [sharedManager.newsfeedsVideos objectAtIndex:currentIndexHome];
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    commentController.commentsObj   = Nil;
    commentController.postArray     = _model;
    commentController.cPostId       =  _model.videoID;;
    commentController.isFirstComment = TRUE;
    commentController.isComment     = FALSE;
    
    
    if(![[self topMostController] isKindOfClass:[CommentsVC class]])
    {
        [[self navigationController] pushViewController:commentController animated:YES];
    }
}

-(void) logoutUser{

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logged_in"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"User_Name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userName"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"User_Img"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [sharedManager dellocDate];
    [[NavigationHandler getInstance]LogoutUser];
    
}

- (void) parseVideoResponse:(NSDictionary*)result{
    
    {
        NSArray *newsfeedPostArray = [NSArray new];
        NSArray *tempArray = [result objectForKey:@"posts"];
        NSArray *latestBeams = [NSArray new];
        if (sharedManager.pageNum == 1){
            latestBeams = [result objectForKey:@"latest_beams"];
            if (latestBeams.count < 1) {
                self.bgView.hidden = YES;
            }
        }
        if(latestBeams.count > 0 && sharedManager.pageNum == 1){
            self.bgView.hidden = NO;
            self.friendsStoriesArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < latestBeams.count ; i++){
                NSDictionary *videoDict = (NSDictionary *)[latestBeams objectAtIndex:i];
                VideoModel *vModel = [[VideoModel alloc] initWithDictionary:videoDict];
                [self.friendsStoriesArray addObject:vModel];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
                StoriesCollectionViewCell *cell = (StoriesCollectionViewCell *)[self.collectionHome cellForItemAtIndexPath:indexPath];
                [cell.collectionCell reloadData];
                [self.storiesCollectionView reloadData];
            });
        }
        
        self.fetchingContent = false;
        if(tempArray.count == 0 && sharedManager.pageNum == 1){
            sharedManager.newsfeedsVideos = [[NSMutableArray alloc] init];
//            self.friendsStoriesArray = [[NSMutableArray alloc] init];
            self.friendsStoriesArray = nil;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tblHome reloadData];
                [self.collectionHome reloadData];
                [tempCollectionView reloadData];
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    //                        NSLog(@"Do some work");
                    if(!isScrolling && !self.fetchingContent)
                    {
                        [self startGifOnCell];
                    }
                    //                        [self myTestProcess];
                });
            });
            self.noBeamsView.hidden = NO;
        }else{
            self.noBeamsView.hidden = YES;
        }
        if(tempArray.count> 0) {
            newsfeedPostArray = [result objectForKey:@"posts"];
            if(sharedManager.pageNum == 1){
                sharedManager.newsfeedsVideos = [[NSMutableArray alloc] init];
            }
            for(NSDictionary *tempDict in newsfeedPostArray){
                VideoModel *_Videos     = [[VideoModel alloc] initWithDictionary:tempDict];
                
                [sharedManager.newsfeedsVideos addObject:_Videos];
            }
            if(!self.fetchingContent){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tblHome reloadData];
                    [self.collectionHome reloadData];
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        //                        NSLog(@"Do some work");
                        if(!isScrolling && !self.fetchingContent)
                        {
                            [self startGifOnCell];
                        }
                        //                        [self myTestProcess];
                    });
                });
            }
        }
        else
        {
            self.tblHome.tableFooterView = nil;
            cannotscroll = true;
        }
    }
    
    

    
    
    
}


#pragma mark liveStream
- (IBAction)goLive:(id)sender {
    
    
//    StreamViewController *streamController = [[StreamViewController alloc] initWithNibName:@"StreamViewController" bundle:nil];
//
//    streamController.delegate = self;
//    //[self presentViewController:streamController animated:YES completion:nil];
//
//    [[self navigationController] pushViewController:streamController animated:YES];

    
}

- (IBAction)viewLive:(id)sender {
    
    //rtmp://rtmp.streamaxia.com/streamaxia/abhassan123
    
    LiveViewController *liveController = [[LiveViewController alloc] initWithNibName:@"LiveViewController" bundle:nil];
    
    liveController.urlString = @"rtmp://rtmp.streamaxia.com/streamaxia/tx123";
    
    [[self navigationController] pushViewController:liveController animated:YES];
    
//    NSURL *streamURL = [NSURL URLWithString:@"http://play.streamaxia.com/abhassan123"];
//
//    
//    AVPlayer *player = [AVPlayer playerWithURL:streamURL];
//    
//    AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
//    
//    controller.player = player;
//    [self presentViewController:controller animated:YES completion:nil];
//    [player play];
    
//    [self Play];
    
//
//    MPMoviePlayerViewController *mpvc = [[MPMoviePlayerViewController alloc] initWithContentURL:streamURL];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(videoFinished:)
//                                                 name:MPMoviePlayerPlaybackDidFinishNotification
//                                               object:nil];
//    
//    mpvc.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
//    
//    [self presentMoviePlayerViewControllerAnimated:mpvc];
    
    
    
}


-(void)Play{
    
    //rtmp://rtmp.streamaxia.com/streamaxia/
    //NSString *radioURL = @"http://play.streamaxia.com/abhassan123"; //this url must valid
    NSString *radioURL = @"rtmp://rtmp.streamaxia.com/streamaxia/abhassan123"; //this url must valid
    AVPlayer *player = [[AVPlayer alloc]initWithURL:[NSURL URLWithString:radioURL]];
    songPlayer.player = player;    //self.songPlayer is a globle object of avplayer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[songPlayer.player currentItem]];
    [songPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];
    [self presentViewController:songPlayer animated:YES completion:nil];
    [songPlayer.player play];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (object == songPlayer && [keyPath isEqualToString:@"status"]) {
        if (songPlayer.player.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
            
        } else if (songPlayer.player.status == AVPlayerStatusReadyToPlay) {
//            NSLog(@"AVPlayerStatusReadyToPlay");
            [songPlayer.player play];
            
            
        } else if (songPlayer.player.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
            
        }
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    //  code here to play next sound file
    
}

- (IBAction)videoFinished:(id)sender {
    
    
    
}


//-(void)setUpLiveStram{
//
//
//    AXStreamaxiaSDK *sdk = [AXStreamaxiaSDK sharedInstance];
//    [sdk setupSDKWithCompletion:^(BOOL success, AXError *error){
//
//
//        [sdk debugPrintSDKStatus];
//
//
//    }];
//
//    NSURL *bundleURL = [[NSBundle mainBundle] URLForResource:@"certificate" withExtension:@"bundle"];
//    NSBundle *bundle = [NSBundle bundleWithURL:bundleURL];
//    [sdk setupSDKWithURL:bundle.bundleURL withCompletion:^(BOOL success, AXError *error) {
//
//        [sdk debugPrintSDKStatus];
//
//    }];
//
//    AXStreamInfo *info = [AXStreamInfo streamInfo];
//    info.useSecureConnection = NO; info.serverAddress = @"23.235.227.213"; info.applicationName = @"test"; info.streamName = @"demo"; info.username = @"";
//
//    info.password = @"";
//    // The default recorder settings
//    AXRecorderSettings *settings = [AXRecorderSettings recorderSettings];
//
//    AXRecorder *recorder = [AXRecorder recorderWithStreamInfo:info settings:settings]; recorder.recorderDelegate = self;
//    AXError *error;
//    // Enable adaptive bitrate
//    // Video quality will be adjusted based on available network and hardware resources [recorder activateFeatureAdaptiveBitRateWithError:&error];
//    if (error) {
//        // Handle error
//    } else {
//        // Succes
//    }
//    // Enable local save
//    // The broadcast will be saved to the users camera roll when finished [recorder activateFeatureSaveLocallyWithError:&error];
//    if (error) {
//        // Handle error
//    } else {
//        // Succes
//    }
//
//
//
//}



- (void)saveStream{
    
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"showLastVideo"
     object:nil];


}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
