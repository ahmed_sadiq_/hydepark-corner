//
//  StoriesCollectionViewCell.m
//  HydePark
//
//  Created by Ahmed Sadiq on 19/12/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "StoriesCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation StoriesCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _view1.layer.masksToBounds = NO;
    _view1.layer.shadowOffset = CGSizeMake(0, 1);
    _view1.layer.shadowRadius = 2;
    _view1.layer.shadowOpacity = 0.7;
}

@end
