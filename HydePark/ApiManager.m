//
//  ApiManager.m
//  UVL
//
//  Created by Osama on 06/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ApiManager.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "CustomLoading.h"
@implementation ApiManager

+(void)postRequest:(NSMutableDictionary *)params success:(returnSuccess)success failure:(returnFailure)failure{
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD show];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [manager POST:SERVER_URL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];
        NSLog(@"responseObject: %@", responseObject);
        //
        //         BOOL _successCall = [[responseObject valueForKey:@"status"] boolValue] ;
        //
        //         if (_successCall){
        //             success(responseObject);
        //         }
        //         else
        //         {
        //             NSError *error = [self createErrorMessageForObject:responseObject];
        //             failure(error);
        //         }
        success(responseObject);
        
    }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}

+(void)getRequest:(NSMutableDictionary *)params success:(returnSuccess)success failure:(returnFailure)failure{
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD show];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager GET:SERVER_URL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];
        
        NSLog(@"responseObject: %@", responseObject);
        //
        //         BOOL _successCall = [[responseObject valueForKey:@"status"] boolValue] ;
        //
        //         if (_successCall){
        //             success(responseObject);
        //         }
        //         else
        //         {
        //             NSError *error = [self createErrorMessageForObject:responseObject];
        //             failure(error);
        //         }
        success(responseObject);
        
    }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}

+(void)uploadURL:(NSString *)urlStr parametes:(NSMutableDictionary *)param
        fileData:(NSData *)data
        progress:(ProgressBlock)progressHandler name:(NSString *)name
        fileName:(NSString *)fileName mimeType:(NSString *)mimeType
         success:(returnSuccess)success failure:(returnFailure)failure{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain", nil];
    [manager POST:urlStr parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:data name:name fileName:fileName mimeType:mimeType];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            float progressFloat = 1.0 * uploadProgress.completedUnitCount / uploadProgress.totalUnitCount;
                progressHandler(progressFloat);
        });
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [CustomLoading DismissAlertMessage];
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [CustomLoading DismissAlertMessage];
        failure(error);
    }];
}

+(void)downloadFileWithUrl:(NSString *)url completionBlock:(void(^)(NSString *filePath,BOOL isFinished))completion{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL* (NSURL* targetPath, NSURLResponse* response) {
        
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
        
    } completionHandler:^(NSURLResponse* response, NSURL* filePath, NSError *error) {
        
        completion(filePath.absoluteString,YES);
        NSLog(@"File downloaded to: %@", filePath);
        
    }];
    [downloadTask resume];
    
}
@end
