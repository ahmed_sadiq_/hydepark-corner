//
//  CreateNewChat.h
//  HydePark
//
//  Created by Samreen Noor on 25/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateNewChat : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSMutableArray *friendsArray;
@end
