//
//  NewHomeCells.m
//  HydePark
//
//  Created by Apple on 09/03/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "NewHomeCells.h"

@implementation NewHomeCells
@synthesize CH_userName,CH_profileImage,CH_commentsBtn,CH_VideoTitle,CH_RCommentscountLbl,CH_CommentscountLbl,playImage,dummyLeft1,dummyLeft2,dummyright1,dummyright2;
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.view1.layer.masksToBounds = YES;
    self.view1.layer.cornerRadius = self.view1.frame.size.width / 6.2f;
    self.view2.layer.masksToBounds = YES;
    self.view2.layer.cornerRadius = self.view2.frame.size.width / 6.2f;
    self.view3.layer.masksToBounds = YES;
    self.view3.layer.cornerRadius = self.view2.frame.size.width / 6.2f;
    
    
    self.outline1.layer.masksToBounds = YES;
    self.outline1.layer.cornerRadius = self.outline1.frame.size.width / 6.2f;
    self.outline2.layer.masksToBounds = YES;
    self.outline2.layer.cornerRadius = self.outline2.frame.size.width / 6.2f;
    self.outline3.layer.masksToBounds = YES;
    self.outline3.layer.cornerRadius = self.outline3.frame.size.width / 6.2f;
    
    self.outline1.layer.borderColor = [UIColor blueColor].CGColor;
    self.outline1.layer.borderWidth = 1.0f;
    self.outline2.layer.borderColor = [UIColor blueColor].CGColor;
    self.outline2.layer.borderWidth = 1.0f;
    self.outline3.layer.borderColor = [UIColor blueColor].CGColor;
    self.outline3.layer.borderWidth = 1.0f;
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.outline1.hidden = NO;
            self.outline2.hidden = NO;
            self.outline3.hidden = NO;
        }
        else
        {
            self.outline1.hidden = YES;
            self.outline2.hidden = YES;
            self.outline3.hidden = YES;        }
    }
    else
    {
        self.outline1.hidden = YES;
        self.outline2.hidden = YES;
        self.outline3.hidden = YES;    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
