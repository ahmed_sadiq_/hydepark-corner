//
//  PeopleYouMayKnowCollectionViewCell.m
//  HydePark
//
//  Created by apple on 6/22/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import "PeopleYouMayKnowCollectionViewCell.h"

@implementation PeopleYouMayKnowCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
     _personImage.layer.cornerRadius =   5;
    _personImage.clipsToBounds = YES;
    // Initialization code
}

@end
