//
//  UITextField+Extra.h
//  Wits
//
//  Created by Osama on 08/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Extra)
- (void)naturalTextAligment;
@end
