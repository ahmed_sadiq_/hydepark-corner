//
//  CornerHeaderCell.m
//  HydePark
//
//  Created by Osama on 23/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CornerHeaderCell.h"
#import <QuartzCore/QuartzCore.h>
@implementation CornerHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
 
    self.pendingReqCount.layer.masksToBounds = true;
    self.pendingReqCount.layer.cornerRadius = self.pendingReqCount.frame.size.height/2;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
