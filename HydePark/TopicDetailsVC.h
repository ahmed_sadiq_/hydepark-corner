//
//  TopicDetailsVC.h
//  HydePark
//
//  Created by Osama on 31/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "topicsModel.h"
@interface TopicDetailsVC : UIViewController{
    NSUInteger currentIndex;
    UIView * footerView;
}
@property (strong, nonatomic) IBOutlet UIImageView *background;
@property (strong, nonatomic) topicsModel *tModel;
@property (strong, nonatomic) NSString *tittle;
@property int pageNum;
@property BOOL fetchingContent;
@property BOOL cannotScrollMore;
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic)   IBOutlet UILabel *titleLabel;
@end
