//
//  CommentsHeaderCell.h
//  HydePark
//
//  Created by Osama on 05/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "FLAnimatedImageView+PINRemoteImage.h"

@interface CommentsHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AsyncImageView *hThumbnail;
@property (weak, nonatomic) IBOutlet UIView *hviewToRound;
@property (weak, nonatomic) IBOutlet UILabel *hTitle;
@property (weak, nonatomic) IBOutlet UILabel *hVideoLength;
@property (weak, nonatomic) IBOutlet UILabel *hTimeStamp;
@property (weak, nonatomic) IBOutlet UILabel *hUserName;

@property (weak, nonatomic) IBOutlet UIButton *hLikeBTn;
@property (weak, nonatomic) IBOutlet UIButton *hEditBtn;

@property (weak, nonatomic) IBOutlet UIButton *hPlayBtn;
@property (weak, nonatomic) IBOutlet UIButton *hGetLikesbtn;
@property (weak, nonatomic) IBOutlet UIButton *hUserChannelBtn;
@property (weak, nonatomic) IBOutlet UILabel *hLikes;
@property (weak, nonatomic) IBOutlet UILabel *hViews;
@property (weak, nonatomic) IBOutlet UILabel *hReplies;
@property (strong, nonatomic) IBOutlet UIImageView *anonyImg;
@property (strong, nonatomic) IBOutlet UIImageView *muteImg;
@property (weak, nonatomic) IBOutlet UIImageView *rebeamedImage;

@property (weak, nonatomic) IBOutlet UIImageView *anonyNewImg;
@property (weak, nonatomic) IBOutlet FLAnimatedImageView *CH_video_thumbnail;
@property (nonatomic, strong) FLAnimatedImageView *animatedImage;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;

@end
