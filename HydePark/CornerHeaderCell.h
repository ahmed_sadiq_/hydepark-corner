//
//  CornerHeaderCell.h
//  HydePark
//
//  Created by Osama on 23/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface CornerHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsyncImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIView *viewToRound;
@property (weak, nonatomic) IBOutlet UIView *picBorder;
@property (weak, nonatomic) IBOutlet UIButton *changeDp;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *noOfBeamslbl;
@property (weak, nonatomic) IBOutlet UILabel *noOfFriendslbl;
@property (weak, nonatomic) IBOutlet UILabel *inboxCount;
@property (weak, nonatomic) IBOutlet UIButton *beamsPressed;
@property (weak, nonatomic) IBOutlet UIButton *editProfileBtn;
@property (weak, nonatomic) IBOutlet UIButton *friendsPressed;
@property (weak, nonatomic) IBOutlet UIButton *inboxPressed;
@property (weak, nonatomic) IBOutlet UIView *celebBottomView;
@property (weak, nonatomic) IBOutlet UIView *normalUserBottomView;

#pragma mark CelebView Outlets
@property (weak, nonatomic) IBOutlet UIButton *celebFriendsBtn;
@property (weak, nonatomic) IBOutlet UIButton *celebFollowersBtn;
@property (weak, nonatomic) IBOutlet UIButton *celebBeamsBtn;
@property (weak, nonatomic) IBOutlet UILabel *cFriendslbl;
@property (weak, nonatomic) IBOutlet UILabel *cBeamslbl;
@property (weak, nonatomic) IBOutlet UILabel *cFollowersLbl;

@property (weak, nonatomic) IBOutlet UILabel *pendingReqCount;
@property (weak, nonatomic) IBOutlet UIImageView *dotforInboxNotifications;
@end
