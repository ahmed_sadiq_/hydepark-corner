//
//  FriendsVC.h
//  HydePark
//
//  Created by Apple on 04/04/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ViewController.h"
#import "UserChannelModel.h"
#import "DataContainer.h"

@interface FriendsVC : UIViewController<UIGestureRecognizerDelegate,UISearchControllerDelegate,UICollectionViewDataSource, UICollectionViewDelegate>
{
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *NoFollowers;
    NSString         *friendId;
    NSUInteger        currentSelectedIndex;
    NSArray *chPostArray;
    NSArray *chVideosArray;
    NSArray *chArrImage;
    NSArray *chArrThumbnail;
    BOOL resultsFinished;
    NSArray *searchedArray;
    DataContainer *sharedManager;
    
    BOOL cannotScrollSearch;
    NSTimer *startSearchTimer;
    id t1;
    id t2;
    id t3;
}


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *searchIndicator;
@property (weak, nonatomic) IBOutlet UITableView *follwersAndFollwings;
@property (strong, nonatomic)   NSMutableArray *friendsArray;
@property (weak, nonatomic) IBOutlet UITableView *hashTagsTableView;

@property (nonatomic) BOOL shudFetchFromServerOnly;
@property (nonatomic) BOOL isSearch;

@property (strong, nonatomic)   NSMutableDictionary *friendsDict;
@property (strong, nonatomic)    NSString *titles;
@property (nonatomic)            BOOL *NoFriends;
@property (strong, nonatomic)    NSString *userId;
@property (nonatomic)             NSInteger loadFollowings;
@property (nonatomic)              BOOL isComment;
@property (nonatomic,assign)      BOOL searchServerCall;
@property int pageNum;
@property int pageNum2;
@property int flagSectionCount;

//for sharing beam as private message
@property (strong, nonatomic) NSString *postId;
@property (assign,nonatomic) NSString *isCommentVideo;
#pragma mark TrendingTable
@property (nonatomic, strong)   IBOutlet UITableView *trendingtblView;
@property (nonatomic, strong) NSArray *FollowingsArray;
@property (nonatomic, strong) NSMutableArray *searchedContent;
@property (strong, nonatomic) IBOutlet UILabel *noBlockedUserslbl;
@property (assign, nonatomic) float btnOriginX;
@property (assign, nonatomic) float btnOriginY;
@property (weak, nonatomic) IBOutlet UIView *segmentView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;


/// To Inbox Beam to friend from Beam Upload VC
@property (assign, nonatomic) BOOL isFromUploadBeam;
@property (assign, nonatomic) BOOL isAudio;
@property (assign, nonatomic) BOOL isLocalSearch;
@property (strong, nonatomic) NSData *beamData;
@property (strong, nonatomic) NSData *thumbnailData;
@property (strong,nonatomic) NSString *videoDuration;
@end
