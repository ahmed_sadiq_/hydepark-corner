//
//  SegmentHeaderView.h
//  HydePark
//
//  Created by Ahmed Sadiq on 15/12/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SegmentHeaderView : UIView
@property (nonatomic, weak) IBOutlet UIView *segmentView;

+ (SegmentHeaderView *)headerView;
@end
