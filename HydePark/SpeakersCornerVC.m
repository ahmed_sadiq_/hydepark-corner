//
//  SpeakersCornerVC.m
//  HydePark
//
//  Created by Osama on 02/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "SpeakersCornerVC.h"
#import "NewHomeCells.h"
#import "Constants.h"
#import "VideoModel.h"
#import "Utils.h"
#import "UIImageView+RoundImage.h"
#import "UIImageView+WebCache.h"
#import "NavigationHandler.h"
#import "CommentsVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "CustomLoading.h"
#import "UIImage+JS.h"
#import "ApiManager.h"
#import "FriendsVC.h"
#import "SpeakersStories.h"
#import "UIImageView+AFNetworking.h"
#import "UserChannel.h"
#import "UIImage+animatedGIF.h"
#import "FLAnimatedImageView+PINRemoteImage.h"
#import "StoriesViewController.h"
#import "StoriesCollectionViewCell.h"
#import "VideoViewCell.h"
#import "FLAnimatedImage.h"
#import "NSData+AES.h"
#import "AFNetworking.h"


@interface SpeakersCornerVC (){
    UIRefreshControl *refreshControl;
    NSMutableDictionary *serviceParams;
    NSMutableArray *speakersStoriesArray;
    CGRect cellRect;
    UICollectionView *tempCollectionView;
}
@end

@implementation SpeakersCornerVC
- (id)init{
    self = [super initWithNibName:@"SpeakersCornerVC" bundle:Nil];
    sharedManager = [DataContainer sharedManager];
    sharedManager.forumPageNumber = 1;
    sharedManager.forumsVideo = [[NSMutableArray alloc] init];
    if(APP_DELEGATE.hasInet) {
         [self getHomeContent];
    }
   
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    DataContainer *sharedInstance = [DataContainer sharedManager];
    sharedInstance.commentsCount = -1;
    self.collectionHome.delegate = self;
    self.collectionHome.dataSource = self;
    self.collectionHome.alwaysBounceVertical = YES;
    [self initFooterViewForHome];
    [self addrefreshControl];
    isScrolling = false;
    self.tblHome.tableFooterView = footerViewHome;
    popOverFrames = [[NSMutableArray alloc] init];
    [self.storiesCollectionView registerNib:[UINib nibWithNibName:@"SpeakersStories" bundle:nil] forCellWithReuseIdentifier:@"speakersCell"];
    self.tblHome.tableHeaderView = self.bgView;
    [self.storiesCollectionView reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getHomeContent) name:@"ReloadSpeakers" object:nil];
//    [(UIActivityIndicatorView *)[footerViewHome viewWithTag:790] startAnimating];
    if(APP_DELEGATE.hasInet) {
        indicator.hidden = false;
        self.view.userInteractionEnabled = false;
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
            {
                indicator.color = [UIColor lightGrayColor];
            }
        }
        
        [(UIActivityIndicatorView *)[footerViewHome viewWithTag:790] startAnimating];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(adjustForNightMode)
                                                 name:SET_NIGHT_MODE
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(clearNightMode)
                                                 name:CLEAR_NIGHT_MODE
                                               object:nil];
    
//    [self checkSpeed];
}

-(void)checkSpeed
{
    
    NSMutableDictionary *postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:METHOD_SPEED_TEST,@"method",nil];
    
   
//    UIImage *audioImage =  [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ic_speed_test" ofType:@"png"]];
    
//    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"testImage" ofType:@"png"];
//    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"Resource" ofType:@"bundle"];
    NSString *imageString = [[NSBundle bundleWithPath:bundlePath] pathForResource:@"testImage" ofType:@"png"];
    
    UIImage *retrievedImage = [[UIImage alloc] initWithContentsOfFile: imageString];
    
    NSData *imageData = UIImagePNGRepresentation(retrievedImage);

    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:SERVER_URL parameters:postDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"filename" fileName:[NSString stringWithFormat:@"%@.png",@"myImage"] mimeType:@"recording/video"];
    } error:nil];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                          if (error)
                          {
                              
                          }
                          else
                          {
                              
                              NSLog(@"UPLOAD SUCCESSFULL");
                    
                          }
                      });
                  }];
    [uploadTask resume];
}

-(void)adjustForNightMode
{
    refreshControl.tintColor = [UIColor grayColor];
    [_collectionHome reloadData];
}

-(void)clearNightMode
{
    refreshControl.tintColor = [UIColor whiteColor];
    [_collectionHome reloadData];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_collectionHome registerNib:[UINib nibWithNibName:@"VideoViewCell" bundle:nil] forCellWithReuseIdentifier:@"collectionCell"];
    [_collectionHome registerNib:[UINib nibWithNibName:@"StoriesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"storiescell"];
    DataContainer *sharedInstance = [DataContainer sharedManager];
    speakersStoriesArray = sharedInstance.speakersStoriesArray;
    self.navigationController.navigationBar.hidden = NO;
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isViwedByMe  != %@",@"1"];
//    [speakersStoriesArray filterUsingPredicate:predicate];
    [_collectionHome reloadData];
    if(sharedManager.isSpeakerViewChanged) {
        sharedManager.isSpeakerViewChanged = NO;
        [_collectionHome reloadData];
    }
    [self performSelector:@selector(startGifOnCell) withObject:nil afterDelay:0.2];
    if(speakersStoriesArray.count > 0){
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        StoriesCollectionViewCell *cell = (StoriesCollectionViewCell *)[self.collectionHome cellForItemAtIndexPath:indexPath];
        [cell.collectionCell reloadData];
        [self.storiesCollectionView reloadData];
        [tempCollectionView reloadData];
        
    }
    else if(speakersStoriesArray){
        if(APP_DELEGATE.hasInet) {
       
            [self getHomeContent];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addrefreshControl{
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            refreshControl.tintColor = [UIColor grayColor];
        }
        else
        {
            refreshControl.tintColor = [UIColor whiteColor];
        }
    }
    else
    {
        refreshControl.tintColor = [UIColor whiteColor];
    }
    [refreshControl addTarget:self
                       action:@selector(refreshCall)
             forControlEvents:UIControlEventValueChanged];
    //[self.tblHome addSubview:refreshControl];
    [self.collectionHome addSubview:refreshControl];
}

#pragma mark Refresh Control
- (void)refreshCall
{
    if(APP_DELEGATE.hasInet) {
        cannotscroll = NO;
        sharedManager.forumPageNumber = 1;
        [self getHomeContent];
    } else {
        [refreshControl endRefreshing];
    }
}

#pragma mark footerView
-(void)initFooterViewForHome{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat width = mainScreen.bounds.size.width;
    footerViewHome = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, 100.0)];
    UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    actInd.tag = 790;
    actInd.frame = CGRectMake(width/2 - 10 ,40.0, 20.0, 20.0);
    actInd.hidesWhenStopped = YES;
    [footerViewHome addSubview:actInd];
    actInd = nil;
}

#pragma mark Server Call
- (void) getHomeContent{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
            NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
            if(indexPath.item > 0){
                cell.mainThumbnail.hidden = NO;
                cell.CH_Video_Thumbnail.hidden = YES;
                [cell.CH_Video_Thumbnail stopAnimating];
            }
        }
    });
    
    self.fetchingContent = true;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",sharedManager.forumPageNumber];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_TRENDING_VIDEOS,@"method",
                              token,@"session_token",pageStr,@"page_no",language,@"language",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        indicator.hidden = true;
        self.view.userInteractionEnabled = true;

        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            _footerIndicator.hidden = YES;
            [_footerIndicator stopAnimating];
            [refreshControl endRefreshing];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
            
//                [self parseVideoResponse:result];
                [self performSelector:@selector(parseVideoResponse:) withObject:result afterDelay:0.4];

            }
            if(success == 3){
                _footerIndicator.hidden = YES;
                [_footerIndicator stopAnimating];
                [self logoutUser];
            }
            
            if ([sharedManager.forumsVideo count] == 0) {
                //                [noBeamsView setHidden:NO];
                //                _findfreindsBtn.hidden =  NO;
            }else{
                //                noBeamsView.hidden = YES;
                //                _findfreindsBtn.hidden =  YES;
            }
        }
        else{
            _footerIndicator.hidden = YES;
            [_footerIndicator stopAnimating];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            self.fetchingContent = false;
            // _homeRefreshBtn.hidden = NO;
        }
        
    }];
    
    
}



#pragma mark COLLECTION VIEW DELEGATES
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == self.collectionHome) {
        if(!self.fetchingContent && indexPath.row == [sharedManager.forumsVideo count] - 1 && !cannotscroll) {
            _footerIndicator.hidden = NO;
            [_footerIndicator startAnimating];
            sharedManager.forumPageNumber++;
            [self getHomeContent];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.item > 0) {
//        VideoViewCell *videoCell = (VideoViewCell *)[self.collectionHome cellForItemAtIndexPath:indexPath];
//        [videoCell.CH_Video_Thumbnail stopAnimating];
//        NSLog(@"index %ld", indexPath.row);

    }
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if(collectionView == self.collectionHome)
        return 6.0;
    else
        return 0.0;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == tempCollectionView) {
        return speakersStoriesArray.count;
    }
    if(collectionView == self.storiesCollectionView) {
        return 0;// return self.friendsStoriesArray.count;
    } else {
        return [sharedManager.forumsVideo count] + 1;
    }
    ///
    //return speakersStoriesArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    VideoModel *vModel = [[VideoModel alloc]init];
//    vModel  = [speakersStoriesArray objectAtIndex:indexPath.row];
//    SpeakersStories *cell;
//
//    if (IS_IPAD) {
//        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"speakersCell" forIndexPath:indexPath];
//    }
//    else{
//        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"speakersCell" forIndexPath:indexPath];
//    }
//    if(vModel.userName.length > 11){
//        cell.celebName.text = [Utils getTrimmedString:vModel.userName];
//    }
//    else{
//        cell.celebName.text = vModel.userName;
//    }
//
//    if([cell.celebName.text isEqualToString:@"Anonymous"]) {
//        cell.celebName.text = NSLocalizedString(@"anonymous_small", nil);
//    }
//    [cell.profilePic setImageWithURL:[NSURL URLWithString:vModel.profile_image] placeholderImage:nil];
//    [cell.profilePic roundImageCorner];
//    if([vModel.viewed_by_me isEqualToString:@"0"]){
//        cell.profilePic.layer.borderColor = [UIColor redColor].CGColor;
//        cell.profilePic.layer.borderWidth = 2.0;
//    } else {
//        cell.profilePic.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        cell.profilePic.layer.borderWidth = 2.0;
//    }
//    [cell.profilePic addShadow];
    
    ///////////////////////////////////////////////////////////////
    
    ////// NEWS FEED COLLECTION VIEW ////////////
    if(collectionView == self.collectionHome){
        
        //////////////////////// HOME COLLECTION VIEW (TAG = 2)//////////////////////
        if(indexPath.item == 0) {
            /// ADDING STORIES COLLECTION VIEW ON INDEX 0
            self.bgView.hidden = YES;
            StoriesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"storiescell" forIndexPath:indexPath];
            cell.collectionCell.delegate = self;
            cell.collectionCell.dataSource = self;
            
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
            {
                if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
                {
                    cell.viewBackGroundView.backgroundColor = [UIColor whiteColor];
                    cell.view1.hidden = false;
                    cell.view2.hidden = false;
                }
                else
                {
                }
            }
            else
            {
            }
            
            [cell.collectionCell registerNib:[UINib nibWithNibName:@"SpeakersStories" bundle:nil] forCellWithReuseIdentifier:@"speakersCell"];
            tempCollectionView = cell.collectionCell;
            return cell;
        }
        
        NSString *CellIdentifier = @"collectionCell";
        VideoViewCell *cell;
        currentIndexHome = indexPath.row-1;
        
        cell  = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        //}
        
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [sharedManager.forumsVideo objectAtIndex:currentIndexHome];
        cell.representedObject = tempVideos;

        if(sharedManager.isListView) {
            cell.CH_userName.font = [UIFont fontWithName:@"Montserrat-Regular" size:11];
            cell.CH_VideoTitle.font = [UIFont fontWithName:@"Montserrat-Regular" size:11];
            cell.durationLbl.hidden = NO;
            cell.durationLbl.text = tempVideos.video_length;
            cell.dotsImg.hidden = NO;
            [cell.btnOptions addTarget:self action:@selector(btnOptionsPressed:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnOptions.tag = indexPath.item;

        } else {
            cell.CH_userName.font = [UIFont fontWithName:@"Montserrat-Regular" size:9];
            cell.CH_VideoTitle.frame = CGRectMake(cell.CH_VideoTitle.frame.origin.x, cell.CH_VideoTitle.frame.origin.y, cell.CH_VideoTitle.frame.size.width, cell.CH_VideoTitle.frame.size.height);
            cell.CH_VideoTitle.font = [UIFont fontWithName:@"Montserrat-Regular" size:9];
            cell.durationLbl.hidden = YES;
            cell.dotsImg.hidden = YES;
        }

        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPressForFriendsCorner:)];
        lpgr.minimumPressDuration = 1.0;
        [cell.CH_commentsBtn addGestureRecognizer:lpgr];
        if(tempVideos.beamType == 1){
            if(sharedManager.isListView) {
                cell.listRebeam.hidden = NO;
                cell.rebeam1.hidden = YES;
            } else {
                cell.listRebeam.hidden = YES;
                cell.rebeam1.hidden = NO;
            }
        }
        else{
            cell.listRebeam.hidden = YES;
            cell.rebeam1.hidden = YES;
        }
        
        cell.CH_userName.text = [Utils getDecryptedTextFor:tempVideos.userName];

        if([tempVideos.is_anonymous isEqualToString:@"1"]) {
            cell.CH_userName.text = NSLocalizedString(@"anonymous_small", nil);
        }
        cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.CH_CommentscountLbl.hidden = YES;
            cell.leftreplImg.hidden = YES;
        }
        else{
            cell.CH_CommentscountLbl.hidden = NO;
            cell.leftreplImg.hidden = NO;
            cell.CH_CommentscountLbl.text = tempVideos.comments_count;
        }
        
        cell.CH_heartCountlbl.text = tempVideos.like_count;
        cell.CH_seen.text = tempVideos.seen_count;
        cell.Ch_videoLength.text = tempVideos.video_length;
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.annonyOverlayLeft.hidden = YES;
            cell.annonyImgLeft.hidden = YES;
            //cell.CH_userName.text = @"Anonymous";
            cell.dummyLeft1.hidden  = YES;
        }
        else{
            //        cell.dummyLeft1.image = [UIImage imageNamed:@"btanonymous.png"];
            cell.annonyOverlayLeft.hidden = NO;
            cell.annonyImgLeft.hidden = NO;
            //cell.CH_userName.text = @"Anonymous";
            cell.dummyLeft1.hidden  = NO;
        }
        if(tempVideos.beamType == 1){
            cell.CH_heart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            cell.CH_heart.hidden = YES;
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        else{
            cell.CH_heart.hidden = YES;
        }
        cell.mainThumbnail.hidden = NO;
        cell.CH_Video_Thumbnail.hidden = YES;
        NSData *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"myCornerThumbnail"];
        cell.mainThumbnail.hidden = NO;
        // cell.mainThumbnail.image = [UIImage imageWithData:temp];
        
        [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage: [UIImage imageWithData:temp]];
        [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (!error) {
                tempVideos.imageThumbnail = image;
                if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
                    
                    //  cell.CH_Video_Thumbnail.hidden = YES;
                    
                    if (tempVideos.imageThumbnail && tempVideos.animatedImage) {
                        cell.mainThumbnail.hidden = YES;
                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                        cell.CH_Video_Thumbnail.hidden = NO;
                    }else if (tempVideos.imageThumbnail && !tempVideos.animatedImage) {
                        [cell.mainThumbnail setImage:tempVideos.imageThumbnail];
                        
                        cell.mainThumbnail.hidden = NO;
                        cell.CH_Video_Thumbnail.hidden = YES;
                        [cell.CH_Video_Thumbnail stopAnimating];
                        
//                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//
//                            [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                            }];
//                        });
                        
                        
                    }else if (!tempVideos.imageThumbnail && tempVideos.animatedImage) {
                        //                        cell.mainThumbnail.hidden = YES;
                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                        cell.CH_Video_Thumbnail.hidden = NO;
                    }else if (!tempVideos.imageThumbnail && !tempVideos.animatedImage) {
                        
                        [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            if (!error) {
                                
                                tempVideos.imageThumbnail = image;
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    
                                    [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            
                                            cell.mainThumbnail.hidden = YES;
                                            cell.CH_Video_Thumbnail.hidden = NO;
                                        });
                                        
                                    }];
                                    
                                });
                            }else{
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    
                                    [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            
                                            cell.mainThumbnail.hidden = YES;
                                            cell.CH_Video_Thumbnail.hidden = NO;
                                        });
                                        
                                    }];
                                    
                                });
                            }
                        }];
                        
                        
                        
                    }
                    
                    
                    
                    
                }else{
                    
                    
                    cell.mainThumbnail.hidden = NO;
                    cell.CH_Video_Thumbnail.hidden = YES;
                    
                    [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if (!error) {
                            tempVideos.imageThumbnail = image;
                            
                        }else{
                            
                            NSLog(@"%@", error);
                            
                        }
                        
                    }];
                    
                }
                
            }else{
                
                NSLog(@"%@", error);
                
            }
            
        }];
        
        
        
        cell.CH_commentsBtn.enabled = YES;
        if (tempVideos.isLocal && !APP_DELEGATE.hasInet) {
            
            [cell.cellProgress removeFromSuperview];
            cell.CH_commentsBtn.backgroundColor = [UIColor colorWithRed:111/255 green:113/255 blue:121/255 alpha:1];
            cell.CH_commentsBtn.alpha = 0.55;
            cell.CH_VideoTitle.text = tempVideos.current_datetime;
            cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
            cell.CH_CommentscountLbl.text = @"!";
            cell.leftreplImg.hidden =  NO;
            cell.CH_commentsBtn.enabled = NO;
            cell.mainThumbnail.hidden = NO;
            // cell.mainThumbnail.image = [UIImage imageWithData:_offlineThumbnailArray[currentIndexHome]];
            if (cell.cellProgress!=nil){
                
                cell.cellProgress.hidden = true;
                [cell.cellProgress removeFromSuperview];
                cell.cellProgress = nil;
            }
        }
        else if(tempVideos.isLocal){
            
            //self.roundedPView.hidden = NO;
            
            
            cell.CH_commentsBtn.enabled = NO;
            cell.leftreplImg.hidden =  YES;
            cell.CH_VideoTitle.text = tempVideos.current_datetime;
            cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
            
            if(![cell.cellProgress isDescendantOfView:cell.contentView]) {
                
                //                cell.cellProgress = [[PWProgressView alloc] initWithFrame:cell.view1.frame];
                
                if(IS_IPHONE_5) {
                    cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, 100, 90)];
                } else if(IS_IPAD){
                    cell.cellProgress = [[PWProgressView alloc] init];
                    cell.cellProgress.frame = cell.view1.frame;
                }
                else {
                    cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, 127, 125)];
                }
                
                cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 6.2f;
                cell.cellProgress.clipsToBounds = YES;
                
                cell.cellProgress.hidden = false;
                
                
                
                //self.roundedPView = cell.cellProgress;
                
                [cell.contentView addSubview:cell.cellProgress];
                
            }else{
                
                
            }
            
            
        }
        else{
            cell.CH_commentsBtn.enabled = YES;
            cell.CH_commentsBtn.backgroundColor = [UIColor clearColor];
            
            if (cell.cellProgress!=nil){
                
                cell.cellProgress.hidden = true;
                [cell.cellProgress removeFromSuperview];
                cell.cellProgress = nil;
                
            }
            
        }
        
        
        
        if(IS_IPAD)
            //cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /7.4f;
            cell.imgContainer.layer.masksToBounds = YES;
        if(sharedManager.isListView) {
            
        } else {
//            [cell.CH_Video_Thumbnail roundCorners];
        }
//        [cell.CH_Video_Thumbnail roundCorners];
        [cell.view1 setBackgroundColor:[UIColor clearColor]];
        
        [cell.CH_heart setTag:currentIndexHome];
        [cell.CH_playVideo setTag:currentIndexHome];
        
        [cell.CH_flag setTag:currentIndexHome];
        [cell.CH_commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CH_commentsBtn setTag:currentIndexHome];
        
        
        [cell setBackgroundColor:[UIColor clearColor]];
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(!tempVideos.isLocal){
            
            [cell.cellProgress removeFromSuperview];
            
        }
        
        cell.blueOverlay.hidden = YES;
        if(sharedManager.forumsVideo.count == 1 && !sharedManager.isListView) {
            cell.frame = CGRectMake(5, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
        }
       
        
        cell.mainThumbnail.hidden = NO;
        cell.CH_Video_Thumbnail.hidden = YES;
        [cell.CH_Video_Thumbnail stopAnimating];
        
        return cell;
        
        
    } else {

            ///// STORIES COLLECTION VIEW ////////

        VideoModel *vModel = [[VideoModel alloc]init];
        vModel  = [speakersStoriesArray objectAtIndex:indexPath.row];
        SpeakersStories *cell;
        
        if (IS_IPAD) {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"speakersCell" forIndexPath:indexPath];
        }
        else{
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"speakersCell" forIndexPath:indexPath];
        }
        
        
        
        NSString *decodedString = [Utils getDecryptedTextFor:vModel.userName];
        
        if(decodedString.length > 11){
            cell.celebName.text = [Utils getTrimmedString:decodedString];
        }
        else{
            cell.celebName.text = decodedString;
        }
        
        
//        if(vModel.userName.length > 11){
//            cell.celebName.text = [Utils getTrimmedString:vModel.userName];
//        }
//        else{
//            cell.celebName.text = vModel.userName;
//        }
        
        
        
        if([cell.celebName.text isEqualToString:@"Anonymous"]) {
            cell.celebName.text = NSLocalizedString(@"anonymous_small", nil);
        }
        [cell.profilePic setImageWithURL:[NSURL URLWithString:vModel.profile_image] placeholderImage:[UIImage imageNamed:@"place_holder"]];
        [cell.profilePic roundImageCorner];
        
        if([vModel.viewed_by_me isEqualToString:@"1"]){
//            cell.profilePic.layer.borderColor = [UIColor redColor].CGColor;
//            cell.profilePic.layer.borderWidth = 2.0;
//            cell.profilePic.layer.borderColor = [UIColor clearColor].CGColor;
            cell.redBg.image = [UIImage imageNamed:@"bg_light_gray_cicle.png"];
        }
        else
        {
            cell.redBg.image = [UIImage imageNamed:@"bg_gradient_red.png"];
            
//            cell.redBg.hidden = YES;
//            cell.profilePic.layer.borderColor = [UIColor lightGrayColor].CGColor;
//            cell.profilePic.layer.borderWidth = 2.0;
        }
        [cell.profilePic addShadow];
        return cell;
        
    }
    
    //return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    CGSize sz;
//    sz = CGSizeMake(90, 100);
//    return sz;
    
    /////////////////////
    CGSize sz;
    float returnValue;
    if(collectionView == self.storiesCollectionView) {
        sz = CGSizeMake(90, 100);
        return sz;
    } else if(collectionView == self.collectionHome){
        if(indexPath.item == 0) {
            return CGSizeMake(collectionView.frame.size.width, 100);
        }
        if (IS_IPHONE_5)
            returnValue = ((collectionView.frame.size.width-12)/3) ;
        else
            returnValue = ((collectionView.frame.size.width-12)/3) ;
        if(sharedManager.isListView) {
            return CGSizeMake(_collectionHome.frame.size.width, 250);
        }
        return CGSizeMake(returnValue, returnValue);
    }
    return CGSizeMake(90, 100);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    VideoModel *_model = [speakersStoriesArray objectAtIndex:indexPath.row];
//    UserChannel *userChannelObj;
//    if(IS_IPAD){
//        userChannelObj = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
//    }
//    else{
//        userChannelObj = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
//    }
//    userChannelObj.ChannelObj = nil;
//    Followings *fData  = [[Followings alloc] init];
//    BOOL isCl= true;
//    fData.is_celeb = [NSString stringWithFormat:@"%d",isCl];
//    userChannelObj.friendID   = _model.user_id;
//    userChannelObj.currentUser = fData;
//    [self.navigationController pushViewController:userChannelObj animated:YES];
    if(collectionView == tempCollectionView)
    {
        StoriesViewController *vc = [[StoriesViewController alloc] init];
        vc.storiesArray = speakersStoriesArray.mutableCopy;
        vc.videoModel = _model;
        vc.isFirstComment = true;
        vc.isFromSpeakers = true;
        vc.indexRow = indexPath.row;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark ScrollViewDelegates
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if (CGRectGetMaxY(scrollView.bounds) == scrollView.contentSize.height) {
//        _footerIndicator.hidden = NO;
//    } else {
//        _footerIndicator.hidden = YES;
//        [_footerIndicator stopAnimating];
//    }
//}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        isScrolling = true;
        for (VideoViewCell *cell in [self.collectionHome visibleCells]) {
            NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
            if(indexPath.item > 0){
                cell.mainThumbnail.hidden = NO;
                cell.CH_Video_Thumbnail.hidden = YES;
                [cell.CH_Video_Thumbnail stopAnimating];
            }
        }
    });
}
-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                    withVelocity:(CGPoint)velocity
             targetContentOffset:(inout CGPoint *)targetContentOffset{
    NSLog(@"here");
    if (velocity.y > 0){
        self.isDownwards = YES;
        // [self hideBottomBar];
    }
    if (velocity.y < 0){
        self.isDownwards = NO;
        //[self showBottomBar];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"here 2");
    isScrolling = false;
    if(!self.fetchingContent)
    {
        [self startGifOnCell];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        isScrolling = false;
        if(!self.fetchingContent)
        {
            [self startGifOnCell];
        }
    }
}

- (void)startGifOnCell {
    
    if(!self.fetchingContent && !isScrolling)
    {
        NSLog(@"starting gifs...");
        dispatch_async(dispatch_get_main_queue(), ^{
            int counter = 0;
            for (counter = 0; counter < [self.collectionHome visibleCells].count; counter ++) {
                
                VideoViewCell *cell = [[self.collectionHome visibleCells] objectAtIndex:counter];
                NSIndexPath *indexPath = [self.collectionHome indexPathForCell:cell];
                VideoModel *tempVideos = [[VideoModel alloc]init];
                if(indexPath.row > 0)
                {
                    tempVideos  = [sharedManager.forumsVideo objectAtIndex:indexPath.row - 1];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if(cell.CH_Video_Thumbnail.animatedImage == nil)
                                {
                                    cell.mainThumbnail.hidden = NO;
                                    cell.CH_Video_Thumbnail.hidden = YES;
                                    [cell.CH_Video_Thumbnail stopAnimating];
                                }
                                else
                                {
                                    cell.mainThumbnail.hidden = YES;
                                    cell.CH_Video_Thumbnail.hidden = NO;
                                    [cell.CH_Video_Thumbnail startAnimating];
                                }

                            });
                            
                        }];
                        
                    });
                }
            }
        });
    }
}

#pragma mark TABLE VIEW DELEGATES

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!self.fetchingContent && indexPath.row == [sharedManager.forumsVideo count] / 3 - 1 && !cannotscroll) {
        sharedManager.forumPageNumber++;
        [self getHomeContent];
    }
    if(cannotscroll)
    {
        self.tblHome.tableFooterView = nil;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    float returnValue;
    if (IS_IPAD)
        returnValue = 260.0f;
    else if(IS_IPHONE_5)
        returnValue = 110.0f;
    else if(IS_IPHONE_6Plus){
        returnValue = 145.0;
    }
    else
        returnValue = 130.0f;
    return returnValue;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    int rows = (int)([sharedManager.forumsVideo count]/3);
//    if([sharedManager.forumsVideo count] % 3 == 1 || [sharedManager.forumsVideo count] % 3 == 2) {
//        rows = rows + 1;
//    }
//    return rows;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"videoCells";
    NewHomeCells *cell;
    currentIndexHome = (indexPath.row * 3);
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects;
        if(IS_IPAD){
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells_iPad" owner:self options:nil];
        }else if(IS_IPHONE_5){
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
        }else{
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
        }
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
        //            cell.leftreplImg.hidden =  NO;
        //            cell.rightreplImg.hidden = NO;
    }else{
        cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    
    VideoModel *tempVideos = [[VideoModel alloc]init];
    tempVideos  = [sharedManager.forumsVideo objectAtIndex:currentIndexHome];
    if(tempVideos.beamType == 1){
        cell.rebeam1.hidden = NO;
    }
    else{
        cell.rebeam1.hidden = YES;
    }
    cell.CH_userName.text = tempVideos.userName;
    
    cell.Ch_videoLength.text = tempVideos.video_length;
    cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
    cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
    if([tempVideos.comments_count isEqualToString:@"0"])
    {
        cell.CH_CommentscountLbl.hidden = YES;
        cell.leftreplImg.hidden = YES;
    }
    else{
        cell.CH_CommentscountLbl.text = tempVideos.comments_count;
    }
    cell.CH_heartCountlbl.text = tempVideos.like_count;
    cell.CH_seen.text = tempVideos.seen_count;
    
    //[cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
    
//    if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
//
//
//
//        cell.thumbNail1.hidden = YES;
//        cell.CH_Video_Thumbnail.hidden = NO;
//
//        [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif]];
//
//
//
//
//    }else{
//
//
//        cell.thumbNail1.hidden = NO;
//        cell.CH_Video_Thumbnail.hidden = YES;
//
//        [cell.thumbNail1 sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
//
//
//
//    }

    if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
        
        
        
        cell.thumbNail1.hidden = YES;
        cell.CH_Video_Thumbnail.hidden = NO;
        
        [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif]];
        
        
        
        
    }else{
        
        
        cell.thumbNail1.hidden = NO;
        cell.CH_Video_Thumbnail.hidden = YES;
        
        [cell.thumbNail1 sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        
        
        
    }
    if([tempVideos.is_anonymous  isEqualToString: @"0"]){
        cell.dummyLeft1.hidden = YES;
        cell.annonyOverlayLeft.hidden = YES;
        cell.annonyImgLeft.hidden = YES;
    }
    else{
        cell.dummyLeft1.hidden = NO;
        cell.annonyOverlayLeft.hidden = NO;
        cell.annonyImgLeft.hidden = NO;
        cell.CH_userName.text = @"Anonymous";
        cell.userProfileView.enabled = false;
    }
    if(tempVideos.beamType == 1){
        cell.CH_heart.hidden = NO;
    }
    else if(tempVideos.beamType == 2){
        cell.CH_heart.hidden = YES;
        NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

    }
    else{
        cell.CH_heart.hidden = YES;
    }
    
//    cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /6.2f;
    if(IS_IPAD)
        //cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /7.4f;
    cell.imgContainer.layer.masksToBounds = YES;
    
    [cell.CH_Video_Thumbnail roundCorners];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPressForFriendsCorner:)];
    lpgr.minimumPressDuration = 1.0;
    [cell.CH_commentsBtn addGestureRecognizer:lpgr];
    
//    [array addObject:[NSValue valueWithCGRect:CGRectMake(0,0,10,10)]];
//    CGRect someRect = [[array objectAtIndex:0] CGRectValue];
    
    
    if(![popOverFrames containsObject:cell])
    {
        [popOverFrames insertObject:cell atIndex:indexPath.row];
    }
    cell.userProfileView.tag = currentIndexHome;
    [cell.CH_heart setTag:currentIndexHome];
    [cell.CH_playVideo setTag:currentIndexHome];
    
    [cell.CH_flag setTag:currentIndexHome];
    cell.CH_commentsBtn.enabled = YES;
    cell.CH_RcommentsBtn.enabled = YES;
    [cell.CH_commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.CH_commentsBtn setTag:currentIndexHome];
    
    currentIndexHome++;
    if(currentIndexHome < sharedManager.forumsVideo.count)
    {
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [sharedManager.forumsVideo objectAtIndex:currentIndexHome];
        if(tempVideos.beamType == 1){
            cell.rebeam2.hidden = NO;
        }
        else{
            cell.rebeam2.hidden = YES;
        }
        [cell.CH_RcommentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CH_RcommentsBtn setTag:currentIndexHome];
        [cell.CH_RplayVideo setTag:currentIndexHome];
        [cell.CH_Rheart setTag:currentIndexHome];
        cell.rightTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
        cell.CH_RVideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.CH_Rseen.text = tempVideos.seen_count;
//        cell.RimgContainer.layer.cornerRadius  = cell.RimgContainer.frame.size.width /6.2f;
        if(IS_IPAD)
            //cell.RimgContainer.layer.cornerRadius  = cell.RimgContainer.frame.size.width /7.4f;
        cell.RimgContainer.layer.masksToBounds = YES;
        [cell.CH_RVideo_Thumbnail roundCorners];
        
        
        cell.CH_RheartCountlbl.text             = tempVideos.like_count;
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.CH_RCommentscountLbl.hidden = YES;
            cell.rightreplImg.hidden = YES;
        }
        else{
            cell.CH_RCommentscountLbl.text = tempVideos.comments_count;
        }
        cell.CH_RuserName.text = tempVideos.userName;
        
        //[cell.CH_RVideo_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        
        if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
            
            
            
            cell.thumbNail2.hidden = YES;
            cell.CH_RVideo_Thumbnail.hidden = NO;
            
            [cell.CH_RVideo_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif]];
            
            
        }else{
            
            
            cell.thumbNail2.hidden = NO;
            cell.CH_RVideo_Thumbnail.hidden = YES;
            
            [cell.thumbNail2 sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
            
        }
        
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPressForFriendsCorner:)];
        lpgr.minimumPressDuration = 1.0;
        [cell.CH_RcommentsBtn addGestureRecognizer:lpgr];

        if(![popOverFrames containsObject:cell])
        {
            [popOverFrames insertObject:cell atIndex:indexPath.row];
        }
        
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.dummyright1.hidden = YES;
            cell.anonyImgRight.hidden = YES;
            cell.anonyOverlayRight.hidden = YES;
        }
        else{
            cell.anonyImgRight.hidden = NO;
            cell.anonyOverlayRight.hidden = NO;
            cell.dummyright1.hidden = NO;
            cell.CH_RuserName.text = @"Anonymous";
        }
        if(tempVideos.beamType == 1){
            cell.CH_Rheart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            cell.CH_Rheart.hidden = YES;
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.CH_RuserName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        else{
            cell.CH_Rheart.hidden = YES;
        }
        currentIndexHome++;
    }
    else{
        cell.view2.hidden = YES;
//        cell.rightClock.hidden        = YES;
//        cell.rightTimeStamp.hidden   = YES;
//        cell.CH_RprofileImage.hidden = YES;
//        cell.CH_Rseen.hidden         = YES;
//        cell.CH_RcommentsBtn.hidden  = YES;
//        cell.CH_RuserName.hidden     = YES;
//        cell.CH_Rheart.hidden        = YES;
//        cell.RimgContainer.hidden    = YES;
//        cell.CH_RplayVideo.hidden    = YES;
//        cell.Rtransthumb.hidden      = YES;
//        cell.CH_RVideoTitle.hidden   = YES;
        cell.rightreplImg.hidden     = YES;
        cell.CH_RCommentscountLbl.hidden = YES;
//        cell.playImage.hidden        = YES;
    }
    
    if(currentIndexHome < sharedManager.forumsVideo.count){
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [sharedManager.forumsVideo objectAtIndex:currentIndexHome];
        if(tempVideos.beamType == 1){
            cell.rebeam3.hidden = NO;
        }
        else{
            cell.rebeam3.hidden = YES;
        }
        cell.rightDate.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.rightUsername.text = tempVideos.userName;
        //[cell.rightThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        
        if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
            
            
            
            cell.thumbNail3.hidden = YES;
            cell.rightThumbnail.hidden = NO;
            
            [cell.rightThumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif]];
            
            
        }else{
            
            
            cell.thumbNail3.hidden = NO;
            cell.rightThumbnail.hidden = YES;
            
            [cell.thumbNail3 sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
            
        }
        
        [cell.rightThumbnail roundCorners];
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.replyRedBg.hidden = YES;
            cell.replyCountlbl.hidden = YES;
        }
        else{
            cell.replyRedBg.hidden = NO;
            cell.replyCountlbl.hidden = NO;
            cell.replyCountlbl.text = tempVideos.comments_count;
        }
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.dummyright1.hidden = YES;
            cell.anonyImgExtRight.hidden = YES;
            cell.anonyOverlayRight.hidden = YES;
        }
        else{
            cell.anonyImgExtRight.hidden = NO;
            cell.anonyOverlayRight.hidden = NO;
            cell.dummyright1.hidden = NO;
            cell.rightUsername.text = @"Anonymous";
        }
        if(tempVideos.beamType == 1){
            cell.CH_Rheart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.rightUsername.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        
        if([tempVideos.isMute isEqualToString:@"0"] && [tempVideos.is_anonymous isEqualToString: @"1"]){
            //            cell.dummyLeft1.image        = [UIImage imageNamed:@"btanonymous.png"];
            
            cell.dummy1.hidden       = NO;
        }
        else if([tempVideos.isMute isEqualToString:@"1"] && [tempVideos.is_anonymous isEqualToString: @"1"])
        {
            cell.dummy1.hidden = NO;
            cell.dummy2.hidden = NO;
            cell.dummy2.image  = [UIImage imageNamed:@"speaker-mute.png"];
            //            cell.dummyLeft1.image  = [UIImage imageNamed:@"btanonymous.png"];
            
        }
        else if([tempVideos.isMute isEqualToString: @"1"] && [tempVideos.is_anonymous isEqualToString: @"0"]){
            cell.dummy1.hidden = NO;
            cell.dummy1.image  = [UIImage imageNamed:@"speaker-mute.png"];
        }
        else{
            cell.dummy2.hidden = YES;
            cell.dummy1.hidden = YES;
        }
        
        
        [cell.showCommentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.showCommentsBtn setTag:currentIndexHome];
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPressForFriendsCorner:)];
        lpgr.minimumPressDuration = 1.0;
        [cell.showCommentsBtn addGestureRecognizer:lpgr];

        if(![popOverFrames containsObject:cell])
        {
            [popOverFrames insertObject:cell atIndex:indexPath.row];
        }
        currentIndexHome++;
    }
    else{
        cell.view3.hidden = YES;
    }
    if([tempVideos.is_anonymous isEqualToString:@"1"]) {
        cell.CH_userName.text = NSLocalizedString(@"anonymous_small", nil);
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

#pragma mark Get Comments

-(UIViewController*) topMostController
{
    NSArray *controllers = [self.navigationController viewControllers];
    
    
    return [controllers lastObject];
}


-(void) ShowCommentspressed:(UIButton *)sender{
    UIButton *CommentsBtn = (UIButton *)sender;
    currentIndexHome = CommentsBtn.tag;
    VideoModel *_model = [sharedManager.forumsVideo objectAtIndex:currentIndexHome];
    CommentsVC *commentController;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    commentController.commentsObj   = Nil;
    commentController.postArray     = _model;
    commentController.cPostId       =  _model.videoID;;
    commentController.isFirstComment = TRUE;
    commentController.isComment     = FALSE;
    
    if(![[self topMostController] isKindOfClass:[CommentsVC class]])
    {
        [[self navigationController] pushViewController:commentController animated:YES];
    }
}

#pragma mark ActionSheet
-(void)handleLongPressForFriendsCorner:(UILongPressGestureRecognizer *)gestureRecognizer
{
    NSInteger tag = gestureRecognizer.view.tag;
    CGPoint p = [gestureRecognizer locationInView:self.view];
//    cellRect = cellRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height, 200, 100);
//    cellRect = gestureRecognizer.view.frame;

//    for(NewHomeCells * cell in popOverFrames)
//    {
//        NSLog(@"%d",cell.CH_commentsBtn.tag);
//        if(cell.showCommentsBtn.tag == tag)
//        {
//            cellRect = cell.frame;
//        }
//    }
    
//    if(IS_IPAD)
//    {
//        cellRect = gestureRecognizer.view.frame;
//    }
    
    cellRect = CGRectMake(p.x, p.y, 200, 100);
    NSIndexPath *indexPath = [self.collectionHome indexPathForItemAtPoint:p];//[self.tblHome indexPathForRowAtPoint:p];
    if (indexPath == nil) {
        
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        currentSelectedIndex = tag;
        [self presentAll:NO];
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        //NSLog(@"ENDED");
    }
}

- (IBAction)btnOptionsPressed:(UIButton *)sender {
    currentSelectedIndex = sender.tag-1;
    [self presentAll:NO];
}

- (IBAction)btnCopyLink:(id)sender {
    VideoModel *tempVideo;
    __block NSString *urlToShare;
    tempVideo = [sharedManager.forumsVideo objectAtIndex:currentSelectedIndex];
    if(tempVideo.isThumbnailReq == 1){
        tempVideo.isThumbnailReq = 0;
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:tempVideo.videoID forKey:@"post_id"];
        [serviceParams setValue:@"0" forKey:@"is_comment"];
        
        [Utils downloadImageWithURL:[NSURL URLWithString:tempVideo.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                    
                }name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                    int flag = [[data objectForKey:@"success"] intValue];
                    if(flag){
                        urlToShare = (NSString *)[data objectForKey:@"deep_link"];
                        UIPasteboard *pb = [UIPasteboard generalPasteboard];
                        [pb setString: [NSString stringWithFormat:@"%@", urlToShare ]];
                        [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
                    }
                } failure:^(NSError *error) {
                    
                }];
            }
        }];
    }else{
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString: [NSString stringWithFormat:@"%@", tempVideo.deep_link ]];
        [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
    }
    
}

- (IBAction)shareOnFb:(id)sender {
    VideoModel *tempVideo;
    __block NSString *urlToShare;
    tempVideo = [sharedManager.forumsVideo objectAtIndex:currentSelectedIndex];
    if(tempVideo.isThumbnailReq == 1){
        tempVideo.isThumbnailReq = 0;
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:tempVideo.videoID forKey:@"post_id"];
        [serviceParams setValue:@"0" forKey:@"is_comment"];
        
        [Utils downloadImageWithURL:[NSURL URLWithString:tempVideo.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                }
                                 name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                                     
                                     int flag = [[data objectForKey:@"success"] intValue];
                                     if(flag){
                                         urlToShare = (NSString *)[data objectForKey:@"deep_link"];
                                         FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                                         content.contentURL = [NSURL URLWithString:urlToShare];
                                         //content.imageURL   = [NSURL URLWithString:urlToShare];
                                         content.contentTitle = tempVideo.userName;
                                         content.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
                                         
                                         FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
                                         dialog.fromViewController = self;
                                         dialog.shareContent = content;
                                         dialog.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
                                         if (![dialog canShow]) {
                                             // fallback presentation when there is no FB app
                                             dialog.mode = FBSDKShareDialogModeFeedBrowser;
                                         }
                                         [dialog show];
                                     }
                                 } failure:^(NSError *error) {
                                     
                                 }];
            }
        }];
    }else{
        FBSDKShareLinkContent *contentw = [[FBSDKShareLinkContent alloc] init];
        contentw.contentURL = [NSURL URLWithString:tempVideo.deep_link];
        //content.imageURL   = [NSURL URLWithString:urlToShare];
        contentw.contentTitle = tempVideo.userName;
        contentw.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
        FBSDKShareDialog *dialogw = [[FBSDKShareDialog alloc] init];
        dialogw.fromViewController = self;
        dialogw.shareContent = contentw;
        dialogw.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
        if (![dialogw canShow]) {
            // fallback presentation when there is no FB app
            dialogw.mode = FBSDKShareDialogModeFeedBrowser;
        }
        [dialogw show];
    }
}

- (IBAction)shareOnTwitter:(id)sender {
    VideoModel *tempVideos;
    tempVideos = [sharedManager.forumsVideo objectAtIndex:currentSelectedIndex];
    NSURL *imageURL = [NSURL URLWithString:tempVideos.video_thumbnail_link];
    [CustomLoading showAlertMessage];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            UIImage *thumbnailImg = [UIImage imageWithData:imageData];
            [CustomLoading DismissAlertMessage];
            
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                [mySLComposerSheet setInitialText:@"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech."];
                [mySLComposerSheet addURL:[NSURL URLWithString:tempVideos.beam_share_url]];
                [mySLComposerSheet addImage:thumbnailImg];
                [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                    
                    switch (result) {
                        case SLComposeViewControllerResultCancelled:
                            NSLog(@"Post Canceled");
                            break;
                        case SLComposeViewControllerResultDone:
                            NSLog(@"Post Sucessful");
                            break;
                            
                        default:
                            break;
                    }
                }];
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            else {
                
                NSString *message;
                NSString *title;
                NSString *cancel;
                message = NSLocalizedString(@"setupTwitterAccount", @"");
                title   = NSLocalizedString(@"accountConfiguration", @"");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                               delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        });
    });
}

-(void)presentAll:(BOOL)isForMycorner{
    int isCelebrity = [Utils userIsCeleb];
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* Share = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"share", @"")
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [self shareDialog];
                                [view dismissViewControllerAnimated:YES completion:nil];
                                
                            }];
    UIAlertAction *sharewithInApp = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"inbox_to_friend", @"")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         if(!isCelebrity) {
                                             VideoModel *tempVideo;
                                             tempVideo = [sharedManager.forumsVideo objectAtIndex:currentSelectedIndex];
                                             [self showToshare:tempVideo.videoID];
                                             [view dismissViewControllerAnimated:YES completion:nil];
                                         }
                                     }];
    
    UIAlertAction *postOnFriendsCorner = [UIAlertAction
                                          actionWithTitle:NSLocalizedString(@"post_on_friend_corner", @"")
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              if(!isCelebrity) {
                                                  VideoModel *tempVideo;
                                                  tempVideo = [sharedManager.forumsVideo objectAtIndex:currentSelectedIndex];
                                                  [self shareOnFriendsCorner:tempVideo.videoID];
                                                  [view dismissViewControllerAnimated:YES completion:nil];
                                              }
                                          }];
    
    UIAlertAction* reportBeam = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"report_beam", @"")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self scReportUserPressed:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
    UIAlertAction *rebeam = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"rebeam", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 VideoModel *tempVideo;
                                 tempVideo = [sharedManager.forumsVideo objectAtIndex:currentSelectedIndex];
                                 [self shareOnMyCorner:tempVideo.videoID];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                             }];
    UIAlertAction* blockP   = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"block_user", @"")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self scBlockUser:self];
                                   [view dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"cancel", @"")
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction *downloadBeam = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"save_to_gallery", @"")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action){
                                       VideoModel *temp  = [sharedManager.forumsVideo objectAtIndex:currentSelectedIndex];
                                       [Utils DownloadVideo:temp.video_link];
                                       [view dismissViewControllerAnimated:YES completion:nil];
                                   }];
    [view addAction:Share];
    [view addAction:rebeam];
    [view addAction:postOnFriendsCorner];
    [view addAction:sharewithInApp];
    [view addAction:downloadBeam];
    [view addAction:reportBeam];
    [view addAction:blockP];
    [view addAction:cancel];
    view.popoverPresentationController.sourceView = self.view;
    view.popoverPresentationController.sourceRect = cellRect;
    [self presentViewController:view animated:YES completion:nil];
}

#pragma mark Sheet Actions

-(void)showToshare:(NSString *)pOstID{
    FriendsVC *commentController;   // = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
    
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = [Utils getFriendsToShareBeam];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 5;
    commentController.userId        = @"";
    commentController.postId = pOstID;
    commentController.isCommentVideo = @"0";
    commentController.isLocalSearch = YES;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(void)shareOnFriendsCorner:(NSString *)postId{
    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = [Utils getFriendsToShareBeam];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 6;
    commentController.userId        = @"";
    commentController.postId = postId;
    commentController.isCommentVideo = @"0";
    commentController.isLocalSearch = YES;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(void)shareOnMyCorner:(NSString *)pOstID{
    [Utils shareBeamOnCorner:pOstID isComment:@"0" friendId:@"" sharingOnFriendsCorner:NO];
}

#pragma mark Get Sharing Content
-(void)shareDialog{
    [self shareUrlCall];
}

-(void)shareUrlCall{
    VideoModel *tempVideo;
    tempVideo = [sharedManager.forumsVideo objectAtIndex:currentSelectedIndex];
    [self showShareSheet:tempVideo.beam_share_url];
    
}

-(void)showShareSheet:(NSString *)shareURL{
    NSURL *url = [NSURL URLWithString:shareURL];
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[url]
     applicationActivities:nil];
    controller.excludedActivityTypes = @[UIActivityTypePrint,
                                         UIActivityTypeMail,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop,
                                         @"com.apple.mobilenotes.SharingExtension",
                                         @"com.apple.reminders.RemindersEditorExtension",
                                         ];
    controller.popoverPresentationController.sourceView = self.view;
    controller.popoverPresentationController.sourceRect = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300);
    
    [self presentViewController:controller animated:YES completion:nil];
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            // user shared an item
//            NSLog(@"We used activity type%@", activityType);
            if([activityType isEqualToString:@"com.apple.UIKit.activity.CopyToPasteboard"]){
                [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
            }
        } else {
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}
- (IBAction)scBlockUser:(id)sender {
    VideoModel *tempVideos ;
    tempVideos = [sharedManager.forumsVideo objectAtIndex:currentSelectedIndex];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"blockUser",@"method",
                              token,@"session_token",tempVideos.user_id,@"blocking_user_id",language,@"language",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message
                                                               delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message
                                                               delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }
        
        else{
        }
    }];
    
}

- (IBAction)scReportUserPressed:(id)sender {
    VideoModel *tempVideos ;
    tempVideos = [sharedManager.forumsVideo objectAtIndex:currentSelectedIndex];
    [self reportUserForFriendsCorner:tempVideos];
}
- (void) reportUserForFriendsCorner :(VideoModel*) vModel {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"reportPost",@"method",
                              token,@"session_token",vModel.videoID,@"post_id",@"For No Reason",@"reason",language,@"language",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message
                                                               delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                                      otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message
                                                               delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
}


- (void) logoutUser{

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logged_in"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"User_Name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userName"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"User_Img"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NavigationHandler getInstance]LogoutUser];
    [sharedManager dellocDate];
    
}

- (void) parseVideoResponse:(NSDictionary*)result{

    {
        NSArray *newsfeedPostArray = [NSArray new];
        NSArray *latestBeams = [NSArray new];
        if (sharedManager.forumPageNumber == 1){
            latestBeams = [result objectForKey:@"latest_beams"];
            if (latestBeams.count < 1 ) {
                self.bgView.hidden = YES;
                self.tblHome.tableHeaderView = nil;
            }
        }
        NSArray *tempArray = [result objectForKey:@"posts"];
        if(latestBeams.count > 0 && sharedManager.forumPageNumber == 1){
            self.bgView.hidden = NO;
            self.tblHome.tableHeaderView = self.bgView;
            speakersStoriesArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < latestBeams.count ; i++){
                NSDictionary *videoDict = (NSDictionary *)[latestBeams objectAtIndex:i];
                VideoModel *vModel = [[VideoModel alloc] initWithDictionary:videoDict];
                [speakersStoriesArray addObject:vModel];
            }
           
            sharedManager.speakersStoriesArray = speakersStoriesArray;
//            for (int i = 0; i < speakersStoriesArray.count; i++) {
//                VideoModel *model = speakersStoriesArray[i];
//                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//                NSDate *date = [dateFormat dateFromString:model.post_seen_time];
//
//                for (int j = 0; j < speakersStoriesArray.count; j++) {
//                    VideoModel *tempmodel = speakersStoriesArray[j];
//                    NSDate *date2 = [dateFormat dateFromString:tempmodel.post_seen_time];
//                    if([date compare:date2]){
//                        NSLog(@"Greater");
//                    } else {
//                        NSLog(@"lower");
//                    }
//                }
//            }
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
            StoriesCollectionViewCell *cell = (StoriesCollectionViewCell *)[self.collectionHome cellForItemAtIndexPath:indexPath];
            [cell.collectionCell reloadData];
           // [self playGif];
            [self.storiesCollectionView reloadData];
        }
        if(tempArray.count> 0)
        {
            newsfeedPostArray = [result objectForKey:@"posts"];
            if(sharedManager.forumPageNumber == 1){
                sharedManager.forumsVideo = [[NSMutableArray alloc] init];
            }
            for(NSDictionary *tempDict in newsfeedPostArray){
                VideoModel *_Videos     = [[VideoModel alloc] initWithDictionary:tempDict];
                
                [sharedManager.forumsVideo addObject:_Videos];
            }
            self.fetchingContent = false;

            if(!self.fetchingContent){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tblHome reloadData];
                    [self.collectionHome reloadData];
                   
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                        NSLog(@"Do some work");
                        if(!isScrolling && !self.fetchingContent)
                        {
                            [self startGifOnCell];
                        }
//                        [self myTestProcess];
                    });

                  //  [self startGifOnCell];
                });
            }
        }
        
        else
        {
            self.tblHome.tableFooterView = nil;
            cannotscroll = true;
        }
    }

}

-(void)myTestProcess
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSLog(@"Downloading Started");
        for(int i = 0; i < sharedManager.forumsVideo.count; i ++)
        {
            VideoModel *tempVideos = sharedManager.forumsVideo[i];
            NSString *urlToDownload = tempVideos.video_thumbnail_gif;
            
            
            NSArray *allComps = [urlToDownload componentsSeparatedByString:@"/"];
            NSString *targetGigName = @"test.gif";
            if(allComps.count > 0)
            {
                targetGigName = [allComps lastObject];
            }
            
            NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,targetGigName];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
            
            
            if(!fileExists)
            {
                NSURL  *url = [NSURL URLWithString:urlToDownload];
                NSData *urlData = [NSData dataWithContentsOfURL:url];
                if ( urlData )
                {
                    //saving is done on main thread
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [urlData writeToFile:filePath atomically:YES];
                        NSLog(@"File Saved !");
                    });
                }
            }
        }
    });
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
