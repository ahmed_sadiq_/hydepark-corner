//
//  LangaugeSelectionVC.m
//  HydePark
//
//  Created by Osama on 16/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "LangaugeSelectionVC.h"
#import "NSBundle+Language.h"
#import "AppDelegate.h"
#import "NavigationHandler.h"
#import "DataContainer.h"
@interface LangaugeSelectionVC ()

@end

@implementation LangaugeSelectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.lang2.text = NSLocalizedString(@"english", nil);
    self.lang1.text = NSLocalizedString(@"arabic", nil);
    self.lang3.text = NSLocalizedString(@"spanish", nil);
    self.lang4.text = NSLocalizedString(@"persian", nil);
    self.lang5.text = NSLocalizedString(@"portuguese", nil);
    
    self.headerLabel.text = NSLocalizedString(@"select_language", nil);
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.background.image = nil;
            self.background.backgroundColor = [UIColor whiteColor];
            self.separatorBg.hidden = false;
        }
        else
        {
        }
    }
    else
    {
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)backPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)arabicPressed:(id)sender{
    
    [NSBundle setLanguage:@"ar"];
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"languageCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self reloadScreen];
}

-(IBAction)spanish:(id)sender{
    [NSBundle setLanguage:@"es"];
    [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"languageCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self reloadScreen];
}

-(IBAction)english:(id)sender{
    
    [NSBundle setLanguage:@"en"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"languageCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self reloadScreen];
}

-(IBAction)persian:(id)sender{
    [NSBundle setLanguage:@"fa"];
    [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"languageCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self reloadScreen];
}

-(IBAction)portugese:(id)sender{
    
    [NSBundle setLanguage:@"pt"];
    [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"languageCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self reloadScreen];
}

-(void)reloadScreen{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NavigationHandler *navHandler = [[NavigationHandler alloc] initWithMainWindow:delegate.window];
    DataContainer *sharedInstance = [DataContainer sharedManager];
    [sharedInstance.notificationsArray removeAllObjects];
    [sharedInstance.topicsArray removeAllObjects];
    sharedInstance = nil;
    [navHandler loadFirstVC];
}

@end
