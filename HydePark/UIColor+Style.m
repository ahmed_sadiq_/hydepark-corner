//
//  UIColor+Style.m
//  Wits
//
//  Created by TxLabz on 08/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "UIColor+Style.h"

@implementation UIColor (Style)
+ (UIColor *) orangeCutomColor {
    return [UIColor colorWithRed:253.0/255.0 green:156.0/255.0 blue:62.0/255.0 alpha:1.0];
}

+ (UIColor *) customBlueColor {
    return [UIColor colorWithRed:41/255.0 green:58/255.0 blue:123/255.0 alpha:1.0];
}
+ (UIColor *) navColor {
    return [UIColor colorWithRed:54/255.0 green:78/255.0 blue:141/255.0 alpha:1.0];
}
@end
