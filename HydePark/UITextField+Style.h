
//  Created by TxLabz on 25/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.


#import <UIKit/UIKit.h>


@interface UITextField (Style)

- (void) setLeftSpaceSize:(CGFloat) size;
- (void) setRightSpaceSize:(CGFloat) size;
@end
