//
//  FriendsSegmentViewController.h
//  HydePark
//
//  Created by Ahmed Sadiq on 15/12/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsVC.h"

@interface FriendsSegmentViewController : UITableView
{
    UIRefreshControl *refreshControl;
    UIActivityIndicatorView *spinner;

}
+ (FriendsSegmentViewController *)contentTableView;
@property (nonatomic)             NSInteger loadFollowings;
@property (assign, nonatomic) float btnOriginX;
@property (assign, nonatomic) float btnOriginY;
@property (assign, nonatomic) BOOL isReloaded;
@property (strong, nonatomic) FriendsVC *vc;
@property (assign, nonatomic) BOOL canLoadMore;

@end
