//
//  PeopleYouMayKnowHeaderView.m
//  HydePark
//
//  Created by apple on 6/22/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import "PeopleYouMayKnowHeaderView.h"

@implementation PeopleYouMayKnowHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    _lblSectionHeaderTitle.layer.cornerRadius = 8;
    _lblSectionHeaderTitle.layer.masksToBounds = YES;
    // Initialization code
}

@end
