//
//  BeamUploadVC.h
//  HydePark
//
//  Created by Apple on 21/03/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ViewController.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "ASIFormDataRequest.h"
#import "Alert.h"
#import "WDClientUploadDelegate.h"
//#import "WDUploadProgressView.h"
#import "autocompleteHandle.h"
#import "IGLDropDownMenu.h"
#import "NIDropDown.h"
#import "DataContainer.h"

@protocol UpdateCornerDelegate
-(void)updateCornerArray:(VideoModel *)videoObject;
@end

@interface BeamUploadVC : ViewController<UIScrollViewDelegate,UITextViewDelegate,ASIHTTPRequestDelegate,AlertDelegate,AutocompleteHandleDelegate>
{
    IBOutlet UILabel *everyOnelbl;
    IBOutlet UILabel *onlyMelbl;
    IBOutlet UILabel *Friendslbl;
    IBOutlet UILabel *Unlimited;
    IBOutlet UILabel *noreplies;
    IBOutlet UILabel *upto60;
    IBOutlet UIView *blockerView;
    IBOutlet UIImageView *cpeveryone;
    IBOutlet UIImageView *cponlyme;
    IBOutlet UIImageView *cpfriends;
    IBOutlet UIImageView *up60;
    IBOutlet UIImageView *noreply;
    IBOutlet UIImageView *unlimited;
    BOOL uploadBeamTag;
    BOOL uploadAnonymous;
   
    NSString *IS_mute;
    NSString *commentAllowed;
    NSString *privacySelected;
    BOOL changeColorForTag;
    NSDictionary *normalAttrdict;
    NSDictionary *highlightAttrdict;
    BOOL isFirstTimeClicked;
    NSString *tagsString;
    NSString *is_Anonymous;
    NSString *timestamp;
    AppDelegate *apDelegate;
    UIGestureRecognizer *tepper;
    IBOutlet UIView *upperView;
    CGRect frameBeamscroller;
    float uploadingProgress;
    NSMutableArray *finalArray;
    NSMutableArray *usersName;
    NSMutableArray *usersId;
    NIDropDown *dropDown;
    DataContainer *sharedManager;
    BOOL isCheckBoxSelected;
    BOOL isNoReplyCheckBoxSelected;
    NSData *playBtnThumbnail;
    DataContainer *shareobj;
    BOOL hasToUpload;
    BOOL isVideoReady;
    
}


@property (weak, nonatomic) IBOutlet UIView *textView;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UIButton *CPEveryone;
@property (weak, nonatomic) IBOutlet UIButton *CPOnlyMe;
@property (weak, nonatomic) IBOutlet UIButton *CPFriends;
@property (weak, nonatomic) IBOutlet UIButton *upto60Comments;
@property (weak, nonatomic) IBOutlet UIButton *NoRepliesbtn;
@property (weak, nonatomic) IBOutlet UIButton *unlimitedRepliesbtn;
@property (weak, nonatomic) IBOutlet UIScrollView *uploadbeamScroller;
@property (strong, nonatomic) IBOutlet UITextView *statusText;
@property (strong, nonatomic) NSString *caption;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UIView *view4;
@property (weak, nonatomic) IBOutlet UIView *view5;

@property (strong, nonatomic) UIImage *thumbnailImage;
@property (strong, nonatomic) UIImage *thumbnailImage2;
@property (strong, nonatomic) UIImage *thumbnailImage3;

@property (nonatomic) BOOL isAnonymous;
@property (nonatomic) BOOL isAudio;
@property (nonatomic) BOOL isComment;

@property (nonatomic) BOOL repliesAllowed;
@property (nonatomic) BOOL isAnonymousChecked;

@property (strong,nonatomic) NSData *dataToUpload;
@property (strong,nonatomic) NSData *profileData;
@property (strong,nonatomic) NSString *postID;
@property (strong,nonatomic) NSString *ParentCommentID;
@property (strong,nonatomic) NSString *video_duration;
@property (strong,nonatomic) NSString *video_thumbnail;
@property (strong,nonatomic) NSArray  *friendsArray;
@property (strong,nonatomic) NSString *privacyToShow;
@property (strong,nonatomic) NSString *replyContToShow;

#pragma mark New Screen Beamupload
@property (strong, nonatomic) NSURL *videoPath;
@property (weak, nonatomic)    IBOutlet UIView *everoneview;
@property (weak, nonatomic)    IBOutlet UIView *unlimitedview;
@property (weak, nonatomic)    IBOutlet UIView *anonymousView;
@property (weak, nonatomic)    IBOutlet UIView *repliesView;
@property (weak, nonatomic)    IBOutlet UIView *shareView;
@property (strong, nonatomic)  IGLDropDownMenu *everyOneDD;
@property (strong, nonatomic)  IGLDropDownMenu *unlimittedDD;
@property (weak, nonatomic)    IBOutlet UIView *viewTobeRound;
@property (weak, nonatomic)    IBOutlet UIView *topBackgroundView;
@property (weak, nonatomic)    IBOutlet UIButton *everyBt;
@property (weak, nonatomic)    IBOutlet UIPickerView *picker;
@property (weak, nonatomic)    IBOutlet UILabel *privacyLbl;
@property (weak, nonatomic)    IBOutlet UILabel *repliesLbl;
@property (weak, nonatomic)    IBOutlet UILabel *statusLbl;
@property (weak, nonatomic)    IBOutlet UILabel *shareStatusLbl;
@property (weak, nonatomic)    IBOutlet UIButton *anonyCheckBtn;
@property (weak, nonatomic)    IBOutlet UIButton *noReplyCheckBtn;
@property (weak, nonatomic)    IBOutlet UILabel  *anonyDescription;
@property (strong, nonatomic) IBOutlet UILabel *anonyTitle;
@property (weak, nonatomic) IBOutlet UIToolbar *langaugeToolbar;
@property (weak, nonatomic) IBOutlet UIImageView *anonyImage;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;

@property (weak, nonatomic) IBOutlet UIButton *postBtn;
- (IBAction)anonymousCheckBox:(id)sender;
- (IBAction)uploadBeamBackPressed:(id)sender;
- (IBAction)uploadBeamPressed:(id)sender;
- (IBAction)PrivacyEveryOne:(id)sender;
- (IBAction)PrivacyOnlyMe:(id)sender;
- (IBAction)PrivacyFriends:(id)sender;
- (IBAction)upto60Pressed:(id)sender;
- (IBAction)noRepliesPressed:(id)sender;
- (IBAction)UnlimitedPressed:(id)sender;
- (IBAction)everyBtOtpions:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *privacyImg;
@property (strong, nonatomic) IBOutlet UIImageView *replyImg;

@property (strong, nonatomic) IBOutlet AutocompleteHandleTextView *tempTextView;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewTotemp;
@property (nonatomic, weak) id <UpdateCornerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *replyLbl;
@property (strong, nonatomic) IBOutlet UILabel *replyDescriptionLbl;
@property (strong, nonatomic) IBOutlet UILabel *shareWithLbl;
@property (strong, nonatomic) IBOutlet UILabel *shareWithDescriptionLbl;

@property (assign, nonatomic)  BOOL isFromGallery;
@property (assign, nonatomic)  BOOL isOffline;
@property (assign, nonatomic)  BOOL isOfflineAudio;
@property (assign, nonatomic)  NSInteger offlineUploadCount;
@property (strong, nonatomic)  NSNumber *commentUploadCount;


@property (assign, nonatomic)  BOOL increaseCommentCount;
@property (assign, nonatomic)  BOOL increaseMyCornerCount;
@property (assign, nonatomic)  BOOL isShowThumnail;
@property (assign, nonatomic)  BOOL isFromBackground;
@property (assign, nonatomic)  BOOL isEdit;
@property (assign, nonatomic)  BOOL isOnlineVideo;
@property (strong, nonatomic)  NSNumber *currentUploadsCount;
@property (strong, nonatomic)  NSNumber *currentAudioUploadCount;
@property (assign, nonatomic)  NSInteger isShowProgress;
@property (assign, nonatomic)  NSInteger orientationAngle;

@property (strong, nonatomic)  NSURL *url;
@property (strong, nonatomic)  NSString *offlineFilePath;
@property (strong, nonatomic)  NSString *offlineRowID;
-(void)callDelegate:(VideoModel *)videoObj;
- (void)uploadOfflineBeam;
- (void)uploadOfflineBeamWhenInForeground;
@end
