//
//  BeamUploadVC.m
//  HydePark
//
//  Created by Apple on 21/03/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BeamUploadVC.h"
#import "ASIFormDataRequest.h"
#import "Utils.h"
#import <AudioToolbox/AudioServices.h>
#import "CommentsModel.h"
#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "Alert.h"
#import "HomeVC.h"
#import "WDUploadProgressView.h"
#import "IGLDemoCustomView.h"
#import "CoreDataManager.h"
#import "CDHydepark.h"
#import "NSMutableArray+QueueAdditions.h"
#import "CommentsVC.h"
#import "MyCornerVC.h"
#import "HomeViewController.h"
#import "UIImage+JS.h"
#import "NavigationHandler.h"
#import "SDWebImageDownloader.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "DBCommunicator.h"
#import "OfflineDataModel.h"
#import "FriendsVC.h"

@interface BeamUploadVC ()<IGLDropDownMenuDelegate>
{
    Alert *alert;
    NSString *textToshare;
    CoreDataManager *coreMngr;
    NSString *anony;
    NSString *userName;
    NSString *userId;
    
}
@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;
@property (nonatomic, copy) NSArray *dataArray;

@property (weak, nonatomic) IBOutlet UIView *thumbnailSelectionView;
- (IBAction)thumbnailBtn1Pressed:(id)sender;
- (IBAction)thumbnailBtn2Pressed:(id)sender;
- (IBAction)thumbnailBtn3Pressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImg1;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImg2;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImg3;
@property (weak, nonatomic) UITextView *tagTextView;


@end
@implementation BeamUploadVC
@synthesize dataToUpload,isAnonymous,isAudio,thumbnailImage,profileData,videoPath,postID,ParentCommentID,video_duration,video_thumbnail,isComment,caption,friendsArray,privacyToShow,replyContToShow,everyOneDD,unlimittedDD;
@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    _url = videoPath;
    _isShowProgress = 0;
    hasToUpload = false;
    isVideoReady = true;
    
    if(isAnonymous){
        is_Anonymous = @"1";
        _anonyImage.hidden = NO;
        isAnonymous = true;
        [_anonyCheckBtn setImage:[UIImage imageNamed:@"black-check-box-with-white-check.png"] forState:UIControlStateNormal];
    }
    else{
        is_Anonymous = @"0";
        _anonyImage.hidden = YES;
    }
    
    
    if(_isEdit)
    {
        if(!self.repliesAllowed) {
            commentAllowed = @"0";
            [_noReplyCheckBtn setImage:[UIImage imageNamed:@"black-check-box-with-white-check.png"] forState:UIControlStateNormal];
        }
        else {
            commentAllowed = @"-1";
            [_noReplyCheckBtn setImage:[UIImage imageNamed:@"emptyCheckBox.png"] forState:UIControlStateNormal];
        }
        
        if(self.isAnonymousChecked) {
            isAnonymous = true;
            is_Anonymous = @"1";
            _anonyImage.hidden = NO;
            [_anonyCheckBtn setImage:[UIImage imageNamed:@"black-check-box-with-white-check.png"] forState:UIControlStateNormal];
        }
        else {
            isAnonymous = false;
            is_Anonymous = @"0";
            _anonyImage.hidden = YES;
            [_anonyCheckBtn setImage:[UIImage imageNamed:@"emptyCheckBox.png"] forState:UIControlStateNormal];
        }
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.background.hidden = YES;
            self.view1.backgroundColor = [UIColor darkGrayColor];
            self.view2.backgroundColor = [UIColor darkGrayColor];
            self.view3.backgroundColor = [UIColor darkGrayColor];
            self.view4.backgroundColor = [UIColor darkGrayColor];
            self.view5.backgroundColor = [UIColor darkGrayColor];
            
            _anonyDescription.textColor = [UIColor blackColor];
            _anonyTitle.textColor = [UIColor blackColor];
            _replyLbl.textColor = [UIColor blackColor];
            _replyDescriptionLbl.textColor = [UIColor blackColor];
            _shareWithLbl.textColor = [UIColor blackColor];
            _shareWithDescriptionLbl.textColor = [UIColor blackColor];
            _shareStatusLbl.textColor = [UIColor blackColor];
            
            UIImage *img = [_anonyCheckBtn imageForState:UIControlStateNormal];
            UIImage *newImage2 = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIGraphicsBeginImageContextWithOptions(img.size, NO, newImage2.scale);
            [[UIColor blackColor] set];
            [newImage2 drawInRect:CGRectMake(0, 0, img.size.width, newImage2.size.height)];
            newImage2 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [_anonyCheckBtn setImage:newImage2 forState:UIControlStateNormal];

            UIImage *img2 = [_noReplyCheckBtn imageForState:UIControlStateNormal];
            UIImage *newImage3 = [img2 imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIGraphicsBeginImageContextWithOptions(img2.size, NO, newImage3.scale);
            [[UIColor blackColor] set];
            [newImage3 drawInRect:CGRectMake(0, 0, img2.size.width, newImage3.size.height)];
            newImage3 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [_noReplyCheckBtn setImage:newImage3 forState:UIControlStateNormal];
            
            UIImage *newImage4 = [_arrowImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIGraphicsBeginImageContextWithOptions(newImage4.size, NO, newImage4.scale);
            [[UIColor blackColor] set];
            [newImage4 drawInRect:CGRectMake(0, 0, newImage4.size.width, newImage4.size.height)];
            newImage4 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            _arrowImage.image = newImage4;
        }
        else
        {
        }
    }
    else
    {
    }
    
    if((NSString *)[NSNull null] != privacyToShow && _isEdit) {
        _arrowImage.hidden = YES;
        _shareStatusLbl.hidden = NO;
        if([privacyToShow isEqualToString:@"PUBLIC"]){
            privacySelected = @"PUBLIC";
            _shareStatusLbl.text = NSLocalizedString(@"everyone", nil);
        } else if([privacyToShow isEqualToString:@"FRIENDS"]) {
            privacySelected = @"FRIENDS";
            _shareStatusLbl.text = NSLocalizedString(@"friends", nil);
        } else if([privacyToShow isEqualToString:@"PRIVATE"]) {
            privacySelected = @"PRIVATE";
            _shareStatusLbl.text = NSLocalizedString(@"only_me", nil);
        }
    }

    _replyLbl.text = NSLocalizedString(@"stop_reply", nil);
    _replyDescriptionLbl.text = NSLocalizedString(@"stop_reply_description", nil);
    _shareWithLbl.text = NSLocalizedString(@"share_with", nil);
    _shareWithDescriptionLbl.text = NSLocalizedString(@"share_with_description", nil);
    _anonyTitle.text = NSLocalizedString(@"anonymous_small", nil);
    
    if(IS_IPHONE_5) {
        _topBackgroundView.frame = CGRectMake(_topBackgroundView.frame.origin.x, _topBackgroundView.frame.origin.y, _topBackgroundView.frame.size.width, _topBackgroundView.frame.size.height - 20);
    }
    shareobj = [DataContainer sharedManager];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(offlineBeamNotification:)
                                                 name:@"callOfflineBeam"
                                               object:nil];
//    [[DBCommunicator sharedManager] clearDB];
    _anonyDescription.text = NSLocalizedString(@"hide_your_identity", nil);
    self.langaugeToolbar.hidden = YES;
    [self.navigationController setNavigationBarHidden:YES];
    apDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    finalArray = [[NSMutableArray alloc] init];
    normalAttrdict = [NSDictionary dictionaryWithObject:BlueThemeColor(145,151,163) forKey:NSForegroundColorAttributeName];
    highlightAttrdict = [NSDictionary dictionaryWithObject:BlueThemeColor(54,78,141) forKey:NSForegroundColorAttributeName ];
    tepper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(keyboardEnd:)];
    tepper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tepper];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    if(IS_IPHONE_5){
        [_uploadbeamScroller setContentSize:CGSizeMake(320,screenHeight)];
        everyOnelbl.frame  = CGRectMake(everyOnelbl.frame.origin.x, everyOnelbl.frame.origin.y, 50, everyOnelbl.frame.size.height);
        onlyMelbl.frame  = CGRectMake(onlyMelbl.frame.origin.x, onlyMelbl.frame.origin.y, 50, onlyMelbl.frame.size.height);
        Friendslbl.frame  = CGRectMake(Friendslbl.frame.origin.x, Friendslbl.frame.origin.y, 50, Friendslbl.frame.size.height);
        Unlimited.frame  = CGRectMake(Unlimited.frame.origin.x, Unlimited.frame.origin.y, 50, Unlimited.frame.size.height);
        noreplies.frame  = CGRectMake(noreplies.frame.origin.x, noreplies.frame.origin.y, 50, noreplies.frame.size.height);
        upto60.frame  = CGRectMake(upto60.frame.origin.x, upto60.frame.origin.y, 50, upto60.frame.size.height);
    }
    else if (IS_IPHONE_6){
        [_uploadbeamScroller setContentSize:CGSizeMake(375,580)];
    }
    else if (IS_IPHONE_6Plus){
        [_uploadbeamScroller setContentSize:CGSizeMake(375,600)];
    }
    else if(IS_IPAD){
        upperView.frame = CGRectMake(0, 0, 768, 86);
    }
    
    _thumbnailImageView.image = thumbnailImage;
    self.thumbnailImg1.image = thumbnailImage;
    self.thumbnailImg2.image = self.thumbnailImage2;
    self.thumbnailImg3.image = self.thumbnailImage3;
    
//    self.thumbnailImg1.layer.cornerRadius = self.thumbnailImg1.frame.size.width/9.0;
//    self.thumbnailImg1.clipsToBounds = YES;
    
    textToshare = NSLocalizedString(@"somethingText", @"");
    [self PrivacyEveryOne:nil];
    [self UnlimitedPressed:nil];
    commentAllowed = @"-1";
    privacySelected = @"PUBLIC";
    if(appDelegate.hasbeenEdited)
    {
        NSURL *url = [NSURL URLWithString:[video_thumbnail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                _thumbnailImageView.image = image;
            }
        }];
       // self.thumbnailSelectionView.hidden = YES;  //edited
        textToshare = [Utils decodeForEmojis:caption];
        if([replyContToShow isEqualToString:@"50"])
            [self upto60Pressed:nil];
        else if([replyContToShow isEqualToString:@"0"])
            [self noRepliesPressed:nil];
        else if([replyContToShow isEqualToString:@"-1"])
            [self UnlimitedPressed:nil];
        
        if([privacyToShow isEqualToString:@"PUBLIC"])
            [self PrivacyEveryOne:nil];
        else if([privacyToShow isEqualToString:@"PRIVATE"])
            [self PrivacyOnlyMe:nil];
        else if([privacyToShow isEqualToString:@"FRIENDS"])
            [self PrivacyFriends:nil];
    }
    
    _viewTobeRound.layer.cornerRadius  = _thumbnailImageView.frame.size.width / 9.0f;
    _viewTobeRound.layer.masksToBounds = YES;
    _picker.hidden = YES;
    self.dataArray =  @[@"Everyone", @"Friends",
                        @"Only Me"];
    sharedManager = [DataContainer sharedManager];
    //[self initCustomViewMenu];
//    AutocompleteHandle *_text;
//    if(IS_IPAD){
//        _text = [[AutocompleteHandle alloc] initWithFrame:CGRectMake(_textView.frame.origin.x, _textView.frame.origin.y, 768, 80) backgroundColor:[UIColor clearColor] textToDisp:textToshare];
//    }
//    else{
//        _text = [[AutocompleteHandle alloc] initWithFrame:_textView.frame backgroundColor:[UIColor clearColor] textToDisp:textToshare];
//    }
//    _text.delegate = self;
//    [self.view addSubview:_text];
//    IS_mute = @"NO";
//    _statusText.delegate = self;
//    usersName = [NSMutableArray array];
//    usersId = [NSMutableArray array];
//    for (NSArray *dict in sharedManager.followers) {
//        [usersName addObject:[dict valueForKey:@"fullName"]];
//        [usersId   addObject:[dict valueForKey:@"f_id"]];
//    }
//    [_text setUserNames:usersName];
    tagsString = @"";
//    if(isAnonymous){
//        is_Anonymous = @"1";
//        _anonyImage.hidden = NO;
//        isAnonymous = true;
//        [_anonyCheckBtn setImage:[UIImage imageNamed:@"black-check-box-with-white-check.png"] forState:UIControlStateNormal];
//    }
//    else{
//        is_Anonymous = @"0";
//        _anonyImage.hidden = YES;
//    }
    if(isAudio || _isEdit){
        self.thumbnailSelectionView.hidden = YES;
        _anonymousView.frame = CGRectMake(_anonymousView.frame.origin.x, _thumbnailSelectionView.frame.origin.y, _anonymousView.frame.size.width, _anonymousView.frame.size.height);
        _repliesView.frame = CGRectMake(_repliesView.frame.origin.x, _anonymousView.frame.origin.y + _anonymousView.frame.size.height + 5, _repliesView.frame.size.width, _repliesView.frame.size.height);
        _shareView.frame = CGRectMake(_shareView.frame.origin.x, _repliesView.frame.origin.y + _repliesView.frame.size.height + 5, _shareView.frame.size.width, _shareView.frame.size.height);
        _thumbnailImageView.image = [UIImage imageNamed: @"splash_audio_image.png"];
        if(_isEdit) {
            _anonyCheckBtn.userInteractionEnabled = NO;
        
        } else {
            _anonyCheckBtn.userInteractionEnabled = YES;
        }
        _anonyCheckBtn.hidden     = false;
        _anonyDescription.hidden  = false;
        _anonyTitle.hidden = false;
    }
    appDelegate.latestVideoAdded = false;
    [_postBtn setTitle:NSLocalizedString(@"post", @"") forState:UIControlStateNormal];
    
    
    
    
    
    
    if(sharedManager.friendsForTagging && sharedManager.friendsForTagging.count == 0)
    {
        [self getFollowing];
    }
    else
    {
        AutocompleteHandle *_text;
        if(IS_IPAD){
            _text = [[AutocompleteHandle alloc] initWithFrame:CGRectMake(_textView.frame.origin.x, _textView.frame.origin.y, 768, 80) backgroundColor:[UIColor clearColor] textToDisp:textToshare];
        } else if(IS_IPHONE_5) {
            
            _text = [[AutocompleteHandle alloc] initWithFrame:CGRectMake(_textView.frame.origin.x, _textView.frame.origin.y - 10, _textView.frame.size.width - 20, _textView.frame.size.height) backgroundColor:[UIColor clearColor] textToDisp:textToshare];
        } else{
            _text = [[AutocompleteHandle alloc] initWithFrame:CGRectMake(_textView.frame.origin.x, _textView.frame.origin.y, _textView.frame.size.width - 20, _textView.frame.size.height-20) backgroundColor:[UIColor clearColor] textToDisp:textToshare];
        }
        _text.delegate = self;
        [self.view addSubview:_text];
        usersName = [NSMutableArray array];
        usersId = [NSMutableArray array];
        for (Followings *eachFriend in sharedManager.friendsForTagging) {
            if([eachFriend.status isEqualToString:@"FRIEND"])
            {
                [usersName addObject:[Utils getDecryptedTextFor:eachFriend.fullName]];
                [usersId   addObject:eachFriend.f_id];
            }
        }
        [_text setUserNames:usersName];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    IS_mute = @"NO";
    _statusText.delegate = self;
}

-(void) getFollowing{
    [sharedManager.friendsForTagging removeAllObjects];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *userId = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"id"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_FOLLOWING_AND_FOLLOWERS,@"method",
                              token,@"session_token",@"1",@"page_no",userId,@"user_id",@"1",@"following",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1)
            {
                NSArray *friends = [result objectForKey:@"following"];
                if([friends isKindOfClass:[NSArray class]]){
                    for(NSDictionary *tempDict in friends){
                        Followings *_responseData = [[Followings alloc] init];
                        _responseData.f_id = [tempDict objectForKey:@"id"];
                        _responseData.fullName = [tempDict objectForKey:@"full_name"];
                        _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                        _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                        _responseData.status = [tempDict objectForKey:@"state"];
                        _responseData.isFollowed = [[tempDict objectForKey:@"is_followed"] intValue];
                        [sharedManager.friendsForTagging addObject:_responseData];
                    }
                }
                
                
                AutocompleteHandle *_text;
                if(IS_IPAD){
                    _text = [[AutocompleteHandle alloc] initWithFrame:CGRectMake(_textView.frame.origin.x, _textView.frame.origin.y, 768, 80) backgroundColor:[UIColor clearColor] textToDisp:textToshare];
                } else if(IS_IPHONE_5) {
                    
                    _text = [[AutocompleteHandle alloc] initWithFrame:CGRectMake(_textView.frame.origin.x, _textView.frame.origin.y - 10, _textView.frame.size.width - 20, _textView.frame.size.height) backgroundColor:[UIColor clearColor] textToDisp:textToshare];
                } else{
                    _text = [[AutocompleteHandle alloc] initWithFrame:CGRectMake(_textView.frame.origin.x, _textView.frame.origin.y, _textView.frame.size.width - 20, _textView.frame.size.height-20) backgroundColor:[UIColor clearColor] textToDisp:textToshare];
                }
                _text.delegate = self;
                [self.view addSubview:_text];
                usersName = [NSMutableArray array];
                usersId = [NSMutableArray array];
                for (Followings *eachFriend in sharedManager.friendsForTagging) {
                    if([eachFriend.status isEqualToString:@"FRIEND"])
                    {
                        [usersName addObject:[Utils getDecryptedTextFor:eachFriend.fullName]];
                        [usersId   addObject:eachFriend.f_id];
                    }
                }
                [_text setUserNames:usersName];
            }
        }
    }];
}

#pragma mark BACKGROUND TASK
- (void)setupBackgrounding {
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(appBackgrounding:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(appForegrounding:)
                                                 name: UIApplicationWillEnterForegroundNotification
                                               object: nil];
    
}

- (void)appBackgrounding: (NSNotification *)notification {
    [self keepAlive];
}

- (void) keepAlive {
    NSLog(@"Still Alive");
    self.backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
        self.backgroundTask = UIBackgroundTaskInvalid;
        [self keepAlive];
    }];
}

- (void)appForegrounding: (NSNotification *)notification {
    if (self.backgroundTask != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
        self.backgroundTask = UIBackgroundTaskInvalid;
    }
}

#pragma mark DONEBUTON
- (IBAction)doneForReading:(id)sender
{
    _picker.hidden = YES;
    _langaugeToolbar.hidden = YES;
}

- (void)initCustomViewMenu
{
    
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.dataArray.count; i++) {
        NSDictionary *dict = self.dataArray[i];
        
        IGLDemoCustomView *customView = [[IGLDemoCustomView alloc] init];
        
        IGLDropDownItem *item = [[IGLDropDownItem alloc] initWithCustomView:customView];
        [dropdownItems addObject:item];
    }
    IGLDemoCustomView *customView = [[IGLDemoCustomView alloc] init];
    self.everyOneDD = [[IGLDropDownMenu alloc] initWithMenuButtonCustomView:customView];
    self.everyOneDD.dropDownItems = dropdownItems;
    self.everyOneDD.menuText = @"Everyone";
    self.everyOneDD.paddingLeft = 15;
    [self.everyOneDD setFrame:CGRectMake(200, 400, 100, 30)];
    self.everyOneDD.delegate = self;
    
    [self.view addSubview:self.everyOneDD];
    [self.everyOneDD reloadView];
    
    __weak typeof(self) weakSelf = self;
    [self.everyOneDD addSelectedItemChangeBlock:^(NSInteger selectedIndex) {
        __strong typeof(self) strongSelf = weakSelf;
        IGLDropDownItem *item = strongSelf.everyOneDD.dropDownItems[selectedIndex];
        IGLDemoCustomView *selectedView = (IGLDemoCustomView*)item.customView;
        IGLDropDownItem *menuButton = strongSelf.everyOneDD.menuButton;
        IGLDemoCustomView *buttonView = (IGLDemoCustomView*)menuButton.customView;
        buttonView.image = selectedView.image;
        //        strongSelf.textLabel.text = [NSString stringWithFormat:@"Selected: %@", selectedView.title];
    }];
}

- (void)setupDropDown
{
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
    [item setIconImage:[UIImage imageNamed:@"evicon.png"]];
    [item setText:@"Everyone"];
    [item setBackgroundColor:[UIColor clearColor]];
    [dropdownItems addObject:item];
    IGLDropDownItem *item1 = [[IGLDropDownItem alloc] init];
    [item1 setIconImage:[UIImage imageNamed:@"private-network.png"]];
    [item1 setText:@"Only Me"];
    [dropdownItems addObject:item1];
    [item1 setBackgroundColor:[UIColor clearColor]];
    IGLDropDownItem *item2 = [[IGLDropDownItem alloc] init];
    [item2 setIconImage:[UIImage imageNamed:@"group-button.png"]];
    [item2 setText:@"Friends"];
    [item2 setBackgroundColor:[UIColor clearColor]];
    [dropdownItems addObject:item2];
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeCenter;
    
    
    self.everyOneDD = [[IGLDropDownMenu alloc] init];
    everyOneDD.menuText = @"Everyone";
    
    everyOneDD.menuIconImage = [UIImage imageNamed:@"evicon.png"];
    everyOneDD.backgroundColor = [UIColor clearColor];
    self.everyOneDD.dropDownItems = dropdownItems;
    
    [self.everyOneDD setFrame:CGRectMake(200, 400, 100, 30)];
    self.everyOneDD.delegate = self;
    [self.view addSubview:self.everyOneDD];
    [self.everyOneDD reloadView];
}

- (IBAction)optionPressed:(id)sender {
    self.dataArray =  @[NSLocalizedString(@"everyone", @""), NSLocalizedString(@"friends", @""),
                        NSLocalizedString(@"only_me", @"")];
    [self.picker reloadAllComponents];
    
    
    if([_privacyLbl.text isEqualToString:NSLocalizedString(@"everyone", @"")]) {
        [_picker selectRow:0 inComponent:0 animated:YES];
    }
    else if([_privacyLbl.text isEqualToString:NSLocalizedString(@"friends", @"")]) {
        [_picker selectRow:1 inComponent:0 animated:YES];
    }
    if([_privacyLbl.text isEqualToString:NSLocalizedString(@"only_me", @"")]) {
        [_picker selectRow:2 inComponent:0 animated:YES];
    }
    
    
    _picker.hidden = NO;
    self.langaugeToolbar.hidden = NO;
}

- (IBAction)repliesPressed:(id)sender {
    self.dataArray =  @[NSLocalizedString(@"unlimited", @""), NSLocalizedString(@"up_to_sixty", @""),
                        NSLocalizedString(@"no_replies", @"")];
    [self.picker reloadAllComponents];
    
    if([_repliesLbl.text isEqualToString:NSLocalizedString(@"unlimited", @"")]) {
        [_picker selectRow:0 inComponent:0 animated:YES];
    }
    else if([_repliesLbl.text isEqualToString:NSLocalizedString(@"up_to_sixty", @"")]) {
        [_picker selectRow:1 inComponent:0 animated:YES];
    }
    else if([_repliesLbl.text isEqualToString:NSLocalizedString(@"no_replies", @"")]) {
        [_picker selectRow:2 inComponent:0 animated:YES];
    }
    
    _picker.hidden = NO;
    self.langaugeToolbar.hidden = NO;
}
- (IBAction)sharePressed:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Share With" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        privacySelected = @"PUBLIC";
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"everyone", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        privacySelected = @"PUBLIC";
        _shareStatusLbl.text = NSLocalizedString(@"everyone", nil);
        _arrowImage.hidden = YES;
        _shareStatusLbl.hidden = NO;
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"just_friends", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        privacySelected = @"FRIENDS";
        _shareStatusLbl.text = NSLocalizedString(@"friends", nil);
        _arrowImage.hidden = YES;
        _shareStatusLbl.hidden = NO;
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"only_me", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        privacySelected = @"PRIVATE";
        _shareStatusLbl.text = NSLocalizedString(@"only_me", nil);
        _arrowImage.hidden = YES;
        _shareStatusLbl.hidden = NO;
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    if(!_isEdit) {
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"inbox_to_friend", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            int isCelebrity = [Utils userIsCeleb];
            if(!isCelebrity) {
                [self showToshare:dataToUpload];
                [actionSheet dismissViewControllerAnimated:YES completion:nil];
            }
            
        }]];
    }
    
    // Present action sheet.
    actionSheet.popoverPresentationController.sourceView = self.view;
    actionSheet.popoverPresentationController.sourceRect = CGRectMake(self.view.center.x - 100, [UIScreen mainScreen].bounds.size.height, 200, 200);
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

#pragma mark Sheet Actions

-(void)showToshare:(NSData *)data{
    FriendsVC *friendVC;//   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    if (IS_IPHONEX){
//        friendVC    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        friendVC    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    friendVC.friendsArray  = [Utils getFriendsToShareBeam];
    friendVC.titles        = NSLocalizedString(@"friends", @"");
    friendVC.NoFriends     = FALSE;
    friendVC.loadFollowings= 5;
    friendVC.userId        = @"";
    friendVC.isCommentVideo = @"0";
    friendVC.beamData = dataToUpload;
    friendVC.isFromUploadBeam = YES;
    friendVC.thumbnailData = profileData;
    friendVC.videoDuration = video_duration;
    friendVC.isLocalSearch = YES;
    if(isAudio) {
        friendVC.isAudio = YES;
    }
    [[self navigationController] pushViewController:friendVC animated:YES];
}

#pragma mark Picker Delegated

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:_dataArray[row] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 32;
}

-(IBAction)anonymousCheckBox:(id)sender{
    isCheckBoxSelected = !isCheckBoxSelected;
    if(isCheckBoxSelected) {
        isAnonymous = true;
        is_Anonymous = @"1";
        _anonyImage.hidden = NO;
        [_anonyCheckBtn setImage:[UIImage imageNamed:@"black-check-box-with-white-check.png"] forState:UIControlStateNormal];
    }
    else {
        isAnonymous = false;
        is_Anonymous = @"0";
        _anonyImage.hidden = YES;
        [_anonyCheckBtn setImage:[UIImage imageNamed:@"emptyCheckBox.png"] forState:UIControlStateNormal];
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            UIImage *img = [_anonyCheckBtn imageForState:UIControlStateNormal];
            UIImage *newImage2 = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIGraphicsBeginImageContextWithOptions(img.size, NO, newImage2.scale);
            [[UIColor blackColor] set];
            [newImage2 drawInRect:CGRectMake(0, 0, img.size.width, newImage2.size.height)];
            newImage2 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [_anonyCheckBtn setImage:newImage2 forState:UIControlStateNormal];
        }
    }
}

-(IBAction)noReplyCheckBox:(id)sender{
    isNoReplyCheckBoxSelected = !isNoReplyCheckBoxSelected;
    if(isNoReplyCheckBoxSelected) {
        commentAllowed = @"0";
        [_noReplyCheckBtn setImage:[UIImage imageNamed:@"black-check-box-with-white-check.png"] forState:UIControlStateNormal];
    }
    else {
        commentAllowed = @"-1";
        [_noReplyCheckBtn setImage:[UIImage imageNamed:@"emptyCheckBox.png"] forState:UIControlStateNormal];
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            UIImage *img2 = [_noReplyCheckBtn imageForState:UIControlStateNormal];
            UIImage *newImage3 = [img2 imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIGraphicsBeginImageContextWithOptions(img2.size, NO, newImage3.scale);
            [[UIColor blackColor] set];
            [newImage3 drawInRect:CGRectMake(0, 0, img2.size.width, newImage3.size.height)];
            newImage3 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [_noReplyCheckBtn setImage:newImage3 forState:UIControlStateNormal];
        }
    }
    
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    NSString *check  = [_dataArray objectAtIndex:0];
    if([check isEqualToString:NSLocalizedString(@"everyone", @"")]){
        _privacyLbl.text = _dataArray[row];
        if(row == 0) {
            privacySelected = @"PUBLIC";
            _privacyImg.image = [UIImage imageNamed:@"evicon.png"];
        }
        else if(row == 1) {
            privacySelected = @"FRIENDS";
            _privacyImg.image = [UIImage imageNamed:@"group-button.png"];
            
        }
        else if(row == 2) {
            privacySelected = @"PRIVATE";
            _privacyImg.image = [UIImage imageNamed:@"private-network.png"];
        }
    }
    else
    {
        _repliesLbl.text = _dataArray[row];
        if(row == 0) {
            commentAllowed = @"-1";
            
        }
        else if(row == 1) {
            commentAllowed = @"50";
        }
        else if(row == 2) {
            commentAllowed = @"0";
        }
    }
    //_picker.hidden   = YES;
    //self.langaugeToolbar.hidden = YES;
}

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return self.dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return self.dataArray[row];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (void)textViewDidChange:(UITextView *)textView {
    // NSLog(@"textViewDidChange %@",textView.text);
    _tagTextView = textView;
    _statusLbl.text = textView.text;
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
}

-(void)autocompleteHandleSelectedHandle:(NSString *)handle {
    NSLog(@"autocompleteHandleSelectedHandle:%@", handle);
    NSInteger index =  [usersName indexOfObject:handle];
    [finalArray addObject:[usersId objectAtIndex:index]];
    [self textViewDidChange:_tagTextView];
}

-(void)autocompleteHandleHeightDidChange:(CGFloat)height {
    NSLog(@"autocompleteHandleHeightDidChange:%f", height);
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextView*)textField up: (BOOL) up
{
    const int movementDistance = 40; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.viewTobeRound.frame = CGRectOffset(self.viewTobeRound.frame, 0, movement);
    [UIView commitAnimations];
}
- (void)keyboardEnd:(UITapGestureRecognizer *) sender
{
    _picker.hidden = YES;
    self.langaugeToolbar.hidden = YES;
    //[self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TextView Delegates
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    //[self animateTextField: textView up: NO];
    _tempTextView = textView;
    //    _statusText.text = @"";
    //    textView.text = @"";
    if(!isFirstTimeClicked &&  !appDelegate.hasbeenEdited) {
        _statusText.text = @"";
        textView.text = @"";
        isFirstTimeClicked = true;
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(BOOL)textViewShouldEndEditing:(AutocompleteHandleTextView *)textView{
    textToshare = textView.text;
    [self extractTags];
    //[self animateTextField: textView up: NO];
    [textView resignFirstResponder];
    return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
}

-(void)updateCommentsArray{
    VideoModel *videos     = [[VideoModel alloc] init];
    if(isAudio){
        [videos setUser_id: userId];
        UIImage *thumbnail = [UIImage imageNamed:@"splash_audio_image"];
        profileData = UIImagePNGRepresentation(thumbnail);
        [videos setCurrent_datetime:[self getCurrentTime]];
        videos.userName             = userName;
        videos.user_id              = userId;
        videos.profileImageData     = profileData;
        videos.isLocal = true;
        videos.is_anonymous = @"0";
    }
    else{
        [videos setCurrent_datetime:[self getCurrentTime]];
        [videos setUser_id: userId];
        videos.is_anonymous = @"0";
        videos.userName             = userName;
        videos.user_id              = userId;
        videos.profileImageData     = profileData;
        videos.isLocal = true;
    }
    
    NSInteger totalViewControllers =  [self.navigationController.viewControllers count];
    UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:totalViewControllers-2];
    //    for (UIViewController*vc in [self.navigationController viewControllers]) {
    if ([vc isKindOfClass: [CommentsVC class]]){
        CommentsVC *commentVC = (CommentsVC*)vc;
        
        
        [commentVC addLocalObjectToCommentsArray:videos];
        if(!appDelegate.hasInet){
            [self showStaticThumbnailOnComments];
        }
    }
    
   
    // }
    
}

-(void)insertLocalObjectInCornerArray{
    DataContainer *shareobj = [DataContainer sharedManager];
    userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Id"];
    NSString *user_Name = [[NSUserDefaults standardUserDefaults] stringForKey:@"userName"];
    VideoModel *videos     = [[VideoModel alloc] init];
    if(isAudio){
        [videos setUserName:user_Name];
        [videos setUser_id: userId];
        UIImage *thumbnail = [UIImage imageNamed:@"splash_audio_image"];
        profileData = UIImagePNGRepresentation(thumbnail);
        [videos setCurrent_datetime:[self getCurrentTime]];
        videos.userName             = userName;
        videos.user_id              = userId;
        videos.profileImageData     = profileData;
        videos.isLocal = true;
        videos.is_anonymous = @"0";
        if(shareobj.channelVideos.count > 0)
            [shareobj.channelVideos insertObject:videos atIndex:0];
        else
            [shareobj.channelVideos addObject:videos];
    }
    else{
        [videos setCurrent_datetime:[self getCurrentTime]];
        [videos setUserName:user_Name];
        [videos setUser_id: userId];
        videos.is_anonymous = @"0";
        videos.userName             = userName;
        videos.user_id              = userId;
        videos.profileImageData     = profileData;
        videos.isLocal = true;
        if(shareobj.channelVideos.count > 0)
            [shareobj.channelVideos insertObject:videos atIndex:0];
        else
            [shareobj.channelVideos addObject:videos];
    }
    shareobj.onlineVideoCount++;
    if(!appDelegate.hasInet){
        [self showStaticThumbnailOnMyCorner];
    }
}
- (IBAction)uploadBeamPressed:(id)sender{
    
    [self textViewDidEndEditing:_tempTextView];
    UIImage *newImg =  [thumbnailImage imageWithScaledToSize:CGSizeMake(200, 200)];
    UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
    playBtnThumbnail = UIImagePNGRepresentation(im);
    self.imgViewTotemp.image  = im;
    [self setupBackgrounding];
    
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    // NSTimeInterval is defined as double
    NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
    timestamp = timeStampObj.stringValue;
    [[DBCommunicator sharedManager] createDB];
    if(!apDelegate.hasbeenEdited){
        if(isAudio && !isComment)
            if(appDelegate.hasInet){
                DataContainer *shareobj = [DataContainer sharedManager];
                
                if(shareobj.offlineArrayCount > 0){
                    [self insertLocalObjectInCornerArray];
                    sharedManager._isOnlineVideo = YES;
                    _offlineUploadCount = _offlineUploadCount + 1;
                    _currentUploadsCount = [NSNumber numberWithInteger:_offlineUploadCount];
                    _currentAudioUploadCount = [NSNumber numberWithInteger:_offlineUploadCount];//[NSNumber numberWithInteger:[_currentAudioUploadCount integerValue] + 1];
                    
                    sharedManager.offlineArrayCount = sharedManager.offlineArrayCount + 1;
                    _isOfflineAudio = YES;
                    //   [self showNoInternetAlert];
                    anony = (isCheckBoxSelected) ? @"1" : @"0";
                    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                    NSString *langCode = [Utils getLanguageCode];
                    if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                        textToshare = @"";
                    }
                    else{
                        textToshare = [Utils encodeForEmojis:textToshare];
                    }
//                    [self saveVideoInDocuments];
                    hasToUpload = false;
                    NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
                    NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                    [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"1" isComment:@"0" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                    [[DBCommunicator sharedManager] closeDB];
                    UIImage *audioImage = [UIImage imageNamed:@"splash_audio_image.png"];
                    NSData *thumbnail = UIImagePNGRepresentation(audioImage);
                    NSDictionary *prog = @{@"data": thumbnail,@"index":[NSNumber numberWithInteger:0]};
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"updateOnlineVideo"
                     object:prog];
                    [self showStaticThumbnailOnMyCorner];
                    
                } else {
                
//                    anony = (isCheckBoxSelected) ? @"1" : @"0";
//                    [self insertLocalObjectInCornerArray];
//                    [self uploadAduio:dataToUpload];
//
                    
                    
                    _isOfflineAudio = YES;
                    anony = (isCheckBoxSelected) ? @"1" : @"0";
                    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                    NSString *langCode = [Utils getLanguageCode];
                    if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                        textToshare = @"";
                    }
                    else{
                        textToshare = [Utils encodeForEmojis:textToshare];
                    }
                    //[[DBCommunicator sharedManager] clearDB];
//                    [self saveVideoInDocuments];
                    hasToUpload = true;
                    NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
                    
                    NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                    [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"1" isComment:@"0" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                    [[DBCommunicator sharedManager] closeDB];
                    [self insertLocalObjectInCornerArray];
                    [self uploadOfflineBeam];
                    
                }
            }
            else{
                 ////////////// FOR OFFLINE MODULE /////////////////
                
                
                _isOfflineAudio = YES;
                //   [self showNoInternetAlert];
                anony = (isCheckBoxSelected) ? @"1" : @"0";
                NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                NSString *langCode = [Utils getLanguageCode];
                if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                    textToshare = @"";
                }
                else{
                    textToshare = [Utils encodeForEmojis:textToshare];
                }
                //[[DBCommunicator sharedManager] clearDB];
//                [self saveVideoInDocuments];
                hasToUpload = false;
                NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
                
                NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"1" isComment:@"0" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                [[DBCommunicator sharedManager] closeDB];
                [self insertLocalObjectInCornerArray];
                //////////////////////////////////////

            }
            else if(isComment && !isAudio){
                
                
                if (appDelegate.hasInet) {
                    
                    DataContainer *shareobj = [DataContainer sharedManager];
                    
                    if(shareobj.offlineArrayCount > 0){
                        sharedManager.offlineArrayCount = sharedManager.offlineArrayCount + 1;
                        if(isAnonymous) {
                            anony = @"1";
                        } else {
                            anony = @"0";
                        }
                        NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                        NSString *langCode = [Utils getLanguageCode];
                        if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                            textToshare = @"";
                        }
                        else{
                            textToshare = [Utils encodeForEmojis:textToshare];
                        }
                        
//                        [self saveVideoInDocuments];
                        hasToUpload = false;
                        NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];

                        NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                        [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"0" isComment:@"1" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                        [[DBCommunicator sharedManager] closeDB];
                        [self updateCommentsArray];
                    } else {
                        if(!_increaseCommentCount){
                            shareobj.commentsCurrentCount++;
                        }
                        //Test For upload  comments
                       
                        if(isAnonymous) {
                            anony = @"1";
                        } else {
                            anony = @"0";
                        }
                        NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                        NSString *langCode = [Utils getLanguageCode];
                        if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                            textToshare = @"";
                        }
                        else{
                            textToshare = [Utils encodeForEmojis:textToshare];
                        }
                        
//                        [self saveVideoInDocuments];
                        hasToUpload = true;

                        NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
//                        NSString *commentParentID = [[NSUserDefaults standardUserDefaults] valueForKey:@"commentparentID"];
//                        ParentCommentID = commentParentID;
                        NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                        [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"0" isComment:@"1" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                        [[DBCommunicator sharedManager] closeDB];
                      //  [self updateCommentsArray];
                        
                         //Test For upload  comments
                        
                        [self updateCommentsArray];
                        [self uploadOfflineBeam];
                        
                       // [self uploadComment:dataToUpload];
                    }
                }else{
//                     [self showNoInternetAlert];
                    
                     ////////////// FOR OFFLINE MODULE /////////////////
                    
                   
                    if(isAnonymous) {
                        anony = @"1";
                    } else {
                        anony = @"0";
                    }
                    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                    NSString *langCode = [Utils getLanguageCode];
                    if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                        textToshare = @"";
                    }
                    else{
                        textToshare = [Utils encodeForEmojis:textToshare];
                    }
                  
//                    [self saveVideoInDocuments];
                    hasToUpload = false;

                    NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
//                    NSString *commentParentID = [[NSUserDefaults standardUserDefaults] valueForKey:@"commentparentID"];
//                    ParentCommentID = commentParentID;
                    NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                    [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"0" isComment:@"1" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                    [[DBCommunicator sharedManager] closeDB];
                    [self updateCommentsArray];
                    /////////////////////////////////

                    
                }
                
                
                
            }
            else if (isComment && isAudio){
                
                if (appDelegate.hasInet) {
                     DataContainer *sharedInstance = [DataContainer sharedManager];
                    if(sharedInstance.offlineArrayCount > 0){
                        sharedManager.offlineArrayCount = sharedManager.offlineArrayCount + 1;
                        _isOfflineAudio = YES;
                        
                        anony = (isCheckBoxSelected) ? @"1" : @"0";
                        NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                        NSString *langCode = [Utils getLanguageCode];
                        if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                            textToshare = @"";
                        }
                        else{
                            textToshare = [Utils encodeForEmojis:textToshare];
                        }
//                        [self saveVideoInDocuments];
                        hasToUpload = false;

                        NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
                        NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                        [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"1" isComment:@"1" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                        [[DBCommunicator sharedManager] closeDB];
                        [self updateCommentsArray];
                    } else {
                        if(!_increaseCommentCount){
                            shareobj.commentsCurrentCount++;
                        }
//                        [self updateCommentsArray];
//                        [self uploadAudioComment:dataToUpload];
                        
                        
                        _isOfflineAudio = YES;
                        
                        anony = (isCheckBoxSelected) ? @"1" : @"0";
                        NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                        NSString *langCode = [Utils getLanguageCode];
                        if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                            textToshare = @"";
                        }
                        else{
                            textToshare = [Utils encodeForEmojis:textToshare];
                        }
//                        [self saveVideoInDocuments];
                        hasToUpload = true;

                        NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
                        NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                        [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"1" isComment:@"1" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                        [[DBCommunicator sharedManager] closeDB];
                        [self updateCommentsArray];
                        [self uploadOfflineBeam];
                    }
                }else{
//                     [self showNoInternetAlert];
                    
                     ////////////// FOR OFFLINE MODULE /////////////////
                    _isOfflineAudio = YES;
                    
                    anony = (isCheckBoxSelected) ? @"1" : @"0";
                    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                    NSString *langCode = [Utils getLanguageCode];
                    if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                        textToshare = @"";
                    }
                    else{
                        textToshare = [Utils encodeForEmojis:textToshare];
                    }
//                    [self saveVideoInDocuments];
                    hasToUpload = false;
                    NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
                    NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                    [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"1" isComment:@"1" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                    [[DBCommunicator sharedManager] closeDB];
                    [self updateCommentsArray];
                    ///////////////////////////////

                    
                }
            }
            else if(appDelegate.hasInet){
                
                if(sharedManager.offlineArrayCount > 0) {
                    [self insertLocalObjectInCornerArray];
                    sharedManager._isOnlineVideo = YES;
                   // sharedManager.uploadCount +=1;
//                    shareobj = [DataContainer sharedManager];
//                    _offlineUploadCount = shareobj.offlineArrayCount;
                    _offlineUploadCount = _offlineUploadCount + 1;
                    _currentUploadsCount = [NSNumber numberWithInteger:_offlineUploadCount];
                    _currentAudioUploadCount = [NSNumber numberWithInteger:_offlineUploadCount];
                    sharedManager.offlineArrayCount = sharedManager.offlineArrayCount + 1;
                    if(isAnonymous) {
                        anony = @"1";
                    } else {
                        anony = @"0";
                    }
                    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                    NSString *langCode = [Utils getLanguageCode];
                    if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                        textToshare = @"";
                    }
                    else{
                        textToshare = [Utils encodeForEmojis:textToshare];
                    }
                    
                    hasToUpload = true;

                    NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
                    NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                    [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"0" isComment:@"0" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                    [[DBCommunicator sharedManager] closeDB];
                    [self showStaticThumbnailOnMyCorner];
//                    [self insertLocalObjectInCornerArray];
//                    _currentUploadsCount = [NSNumber numberWithInteger:[_currentUploadsCount integerValue] + 1];
//                    _currentAudioUploadCount = [NSNumber numberWithInteger:[_currentAudioUploadCount integerValue] + 1];
                    
                    
                    NSData *thumbnail = [self getThumbnailFromDocuments:thumbnailPath];
                    NSDictionary *prog = @{@"data": thumbnail,@"index":[NSNumber numberWithInteger:0]};
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"updateOnlineVideo"
                     object:prog];
                    [self uploadOfflineBeam];
                } else {
//                    anony = (isAnonymous) ? @"1" : @"0";
//                    [self insertLocalObjectInCornerArray];
//                    [self uploadBeam:dataToUpload];

                    
                    
                    
                    if(isAnonymous) {
                        anony = @"1";
                    } else {
                        anony = @"0";
                    }
                    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                    NSString *langCode = [Utils getLanguageCode];
                    if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                        textToshare = @"";
                    }
                    else{
                        textToshare = [Utils encodeForEmojis:textToshare];
                    }
                    hasToUpload = true;

                    NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
                    NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                    [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"0" isComment:@"0" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                    [[DBCommunicator sharedManager] closeDB];
                    [self insertLocalObjectInCornerArray];
                    [self uploadOfflineBeam];
                    
                }
            }
            else{
                
                 ////////////// FOR OFFLINE MODULE /////////////////
                if(isAnonymous) {
                    anony = @"1";
                } else {
                    anony = @"0";
                }
                NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
                NSString *langCode = [Utils getLanguageCode];
                if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0) {
                    textToshare = @"";
                }
                else{
                    textToshare = [Utils encodeForEmojis:textToshare];
                }
                hasToUpload = false;

                NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
                NSString *thumbnailPath =  [self saveThumbnailInDocuments];
                [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:commentAllowed caption:textToshare isAnonymous:anony isMute:@"0" videoLength:video_duration postID:postID parentCommentID:ParentCommentID privacy:privacySelected language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"0" isComment:@"0" fromGallery:_isFromGallery withThumbnail:thumbnailPath];
                [[DBCommunicator sharedManager] closeDB];
                [self insertLocalObjectInCornerArray];
                
                ///////////////////////////////
                
            }
    
    }
    else{
        appDelegate.hasbeenEdited = FALSE;
        [self editUploadedBeam];
    }
    appDelegate.navigationControllersCount = (int)self.navigationController.viewControllers.count-2;
    if(isComment)
        [self.navigationController popViewControllerAnimated:YES];
    else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - Saving Offline Content

 ////////////// FOR OFFLINE MODULE /////////////////
- (void)saveVideoInDocuments {
    if([_url isKindOfClass:[NSString class]]) {
        _url = [NSURL URLWithString:(NSString *)_url];
    }
    NSData *videoData = [NSData dataWithContentsOfURL:_url];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy||HH:mm:SS"];
    NSDate *now = [[NSDate alloc] init] ;
    NSString *theDate = [dateFormat stringFromDate:now];
    theDate = [theDate stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    NSString *videopath;
    if(_isOfflineAudio) {
        videopath= [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@/%@.wav",documentsDirectory,theDate]] ;
        _isOfflineAudio = NO;
    } else {
        videopath= [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@/%@.mov",documentsDirectory,theDate]] ;
    }
    _offlineFilePath = videopath;
    [[NSUserDefaults standardUserDefaults] setObject:_offlineFilePath forKey:@"fileUrl"];
    BOOL success = [videoData writeToFile:videopath atomically:NO];
    
    NSLog(@"Successs:::: %@", success ? @"YES" : @"NO");
    NSLog(@"video path --> %@",videopath);

}

- (NSString *)saveVideoInDocumentsAndReturnPath {
    if([_url isKindOfClass:[NSString class]]) {
        _url = [NSURL URLWithString:(NSString *)_url];
    }
    
    NSData *videoData = [NSData dataWithContentsOfURL:_url];
    
    double fileSize = (float)videoData.length/1024.0f/1024.0f;
    NSLog(@"Old File size is : %.2f MB",fileSize);
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy||HH:mm:SS"];
    NSDate *now = [[NSDate alloc] init] ;
    NSString *theDate = [dateFormat stringFromDate:now];
    theDate = [theDate stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    NSString *videopathForInnerUse;
    if(_isOfflineAudio) {
        videopathForInnerUse= [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@/%@.wav",documentsDirectory,theDate]] ;
        _isOfflineAudio = NO;
    } else {
        videopathForInnerUse= [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@/%@.mov",documentsDirectory,theDate]] ;
    }
    _offlineFilePath = videopathForInnerUse;
    [[NSUserDefaults standardUserDefaults] setObject:_offlineFilePath forKey:@"fileUrl"];
    
    
    /////////////////COMPRESSING///////////////////
    
    isVideoReady = true;   //////////WAIT FOR VIDEO TO BE COMPRESSED/////////////
    
//    NSURL *outputFile = [NSURL fileURLWithPath:videopathForInnerUse];
//
//    if(fileSize > 25)
//    {
//        isVideoReady = false;
//        [SVProgressHUD show];
//        [self compressVideo:_url outputURL:outputFile handler:^(AVAssetExportSession *completion) {
//            if (completion.status == AVAssetExportSessionStatusCompleted) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                    });
//                NSData *newDataForUpload = [NSData dataWithContentsOfURL:outputFile];
//    //            NSLog(@"Size of new Video after compression is (bytes):%d",[newDataForUpload length]);
//                NSLog(@"File size is : %.2f MB",(float)newDataForUpload.length/1024.0f/1024.0f);
//                [self resumeUploadIfAny];
//            }
//        }];
//    }
//    else
//    {
        BOOL success = [videoData writeToFile:videopathForInnerUse atomically:NO];
        
        NSLog(@"Successs:::: %@", success ? @"YES" : @"NO");
        NSLog(@"video path --> %@",videopathForInnerUse);
//    }
    /////////////////////////////////////////////
    
    if([videopathForInnerUse rangeOfString:@".wav"].length > 0)
    {
        return [NSString stringWithFormat:@"%@.wav",theDate];
    }
    return [NSString stringWithFormat:@"%@.mov",theDate];
    
}

-(void)resumeUploadIfAny    /////////////////VIDEO IS READY TO BE UPLOADED///////////////////
{
    isVideoReady = true;
    if(hasToUpload)
    {
        [self uploadOfflineBeam];
    }
}

- (NSData *)getVideoFromDocuments:(NSString *)videoURL{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    videoURL = [NSString stringWithFormat:@"%@/%@",documentsPath,videoURL];
    
    NSData *video = [NSData dataWithContentsOfFile:videoURL];
    return video;
}

- (NSString *)saveThumbnailInDocuments {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy||HH:mm:SS"];
    NSDate *now = [[NSDate alloc] init] ;
    NSString *theDate = [NSString stringWithFormat:@"%@.png",[dateFormat stringFromDate:now]];
    theDate = [theDate stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:theDate]; //Add the file name
    [profileData writeToFile:filePath atomically:YES];
    [[NSUserDefaults standardUserDefaults] setObject:filePath forKey:@"thumbnailUrl"];
    return theDate;
}

- (NSData *)getThumbnailFromDocuments:(NSString *)thumbnailURL{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    thumbnailURL = [NSString stringWithFormat:@"%@/%@",documentsPath,thumbnailURL];
    
    NSData *thumbnail = [NSData dataWithContentsOfFile:thumbnailURL];
    return thumbnail;
}

//////////////////////////////////

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}
-(void) editUploadedBeam{
    
    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
    
    if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0){
        textToshare = @"";
    }
    else{
        textToshare = [Utils encodeForEmojis:textToshare];
    }
    
    NSString *tag_string;
    NSMutableDictionary *postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"editPost",@"method",userSession,@"Session_token",commentAllowed,@"reply_count",textToshare,@"caption",@"0",@"is_anonymous",@"0",@"mute",postID,@"post_id",@"-1",@"parent_comment_id",privacySelected,@"privacy",@"COLOUR",@"filter",nil];
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    // NSTimeInterval is defined as double
    NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
    timestamp = timeStampObj.stringValue;
    
    [postDict setObject:timestamp forKey:@"timestamp"];
    if(finalArray && finalArray.count > 0 ){
        tag_string = [finalArray componentsJoinedByString:@","];
        [postDict setObject:tag_string forKey:@"tag_friends"];
    }
    if(tagsString.length > 0){
        [postDict setObject:tagsString forKey:@"topic_name"];
    }
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *tempDict = [responseObject objectForKey:@"post_data"];
        VideoModel *_Videos = [[VideoModel alloc] init];
        _Videos.title           = [tempDict objectForKey:@"caption"];
        _Videos.comments_count  = [tempDict objectForKey:@"comment_count"];
        _Videos.userName        = [tempDict objectForKey:@"full_name"];
        _Videos.topic_id        = [tempDict objectForKey:@"topic_id"];
        _Videos.user_id         = [tempDict objectForKey:@"user_id"];
        _Videos.profile_image   = [tempDict objectForKey:@"profile_link"];
        _Videos.like_count      = [tempDict objectForKey:@"like_count"];
        _Videos.like_by_me      = [tempDict objectForKey:@"liked_by_me"];
        _Videos.seen_count      = [tempDict objectForKey:@"seen_count"];
        _Videos.video_angle     = [[tempDict objectForKey:@"video_angle"] intValue];
        _Videos.video_link      = [tempDict objectForKey:@"video_link"];
        _Videos.m3u8_video_link = [tempDict objectForKey:@"m3u8_video_link"];
        _Videos.video_thumbnail_link = [tempDict objectForKey:@"video_thumbnail_link"];
        _Videos.videoID         = [tempDict objectForKey:@"id"];
        _Videos.Tags            = [tempDict objectForKey:@"tag_friends"];
        _Videos.video_length    = [tempDict objectForKey:@"video_length"];
        _Videos.is_anonymous    = [tempDict objectForKey:@"is_anonymous"];
        _Videos.reply_count     = [tempDict objectForKey:@"reply_count"];
        _Videos.current_datetime= [tempDict objectForKey:@"current_datetime"];
        _Videos.uploaded_date   = [tempDict objectForKey:@"uploaded_date"];
        _Videos.isMute          = @"0";
        _Videos.privacy         = [tempDict objectForKey:@"privacy"];
        _Videos.deep_link = [tempDict objectForKey:@"deep_link"];
        appDelegate.videObj = _Videos;
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"GetChannelAgain"
         object:nil];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"updateMyArchive"
         object:nil];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"updateeditBeam"
         object:nil];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
    }];
}
-(void) extractTags{
    tagsString = @"";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    NSArray *matches = [regex matchesInString:textToshare options:0 range:NSMakeRange(0,textToshare.length)];
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:1];
        NSString* word = [textToshare substringWithRange:wordRange];
        tagsString = [tagsString stringByAppendingString:word];
        tagsString = [tagsString stringByAppendingString:@","];
    }
    if ([tagsString length] > 0)
        tagsString = [tagsString substringToIndex:[tagsString length] - 1];
}

#pragma mark Uploading FUNCTIONS
-(void) uploadBeam :(NSData*)file
{
//    if(isVideoReady)
    {
       // _increaseCommentCount = NO;
        APP_DELEGATE.isUploading = YES;
        _isShowThumnail = YES;
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
        if(_isShowProgress == 1) {
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"ShowProgress"
             object:nil];
        }
     
        NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
        if(!_isOffline) {
            if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0){
                textToshare = @"";
            }
            else{
                if([textToshare rangeOfString:@"\\"].length > 0)
                {
                    
                }
                else
                {
                    textToshare = [Utils encodeForEmojis:textToshare];
                }
            }
        }
        
        NSString *tag_string;
        NSString *language = [Utils getLanguageCode];
        NSMutableDictionary *postDict;
        if(_orientationAngle == 180 || _orientationAngle == 270) {
            NSString *angle = [NSString stringWithFormat:@"%ld",_orientationAngle];
            postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:METHOD_UPLOAD_STATUS,@"method",userSession,@"Session_token",commentAllowed,@"reply_count",textToshare,@"caption",anony,@"is_anonymous",@"0",@"mute",video_duration,@"video_length",postID,@"post_id",ParentCommentID,@"parent_comment_id",privacySelected,@"privacy",language,@"language",timestamp,@"timestamp",angle,@"rotation_flag",nil];
        } else {
            postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:METHOD_UPLOAD_STATUS,@"method",userSession,@"Session_token",commentAllowed,@"reply_count",textToshare,@"caption",anony,@"is_anonymous",@"0",@"mute",video_duration,@"video_length",postID,@"post_id",ParentCommentID,@"parent_comment_id",privacySelected,@"privacy",language,@"language",timestamp,@"timestamp",nil];
        }

        
        NSDate *dateObj = [self getDateFromString:timestamp];
        
        
        NSTimeInterval timeStamp1 = [dateObj timeIntervalSince1970];
        // NSTimeInterval is defined as double
        NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp1];
        timestamp = timeStampObj.stringValue;
        
        [postDict setObject:timestamp forKey:@"timestamp"];
        
        if(finalArray && finalArray.count > 0 ){
            tag_string = [finalArray componentsJoinedByString:@","];
            [postDict setObject:tag_string forKey:@"tag_friends"];
        }
        if(tagsString.length > 0){
            [postDict setObject:tagsString forKey:@"topic_name"];
        }
        NSLog(@"%@",postDict);
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:SERVER_URL parameters:postDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:file name:@"video_link" fileName:[NSString stringWithFormat:@"%@.mp4",@"video"] mimeType:@"recording/video"];
            [formData appendPartWithFileData:profileData name:@"video_thumbnail_link" fileName:[NSString stringWithFormat:@"%@.png",@"thumbnail"] mimeType:@"image/png"];
            [formData appendPartWithFileData:playBtnThumbnail name:@"video_thumbnail_link2" fileName:[NSString stringWithFormat:@"%@.png",@"thumbnail"] mimeType:@"image/png"];
        } error:nil];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
        
        [[NSUserDefaults standardUserDefaults] setObject:profileData forKey:@"myCornerThumbnail"];
         ////////////// FOR OFFLINE MODULE /////////////////
        
         NSArray *offlineContent = [[DBCommunicator sharedManager] retrieveAllRows];
        
        NSMutableArray *currentUploadItems = offlineContent.mutableCopy;
        
        DataContainer *sharedInstance = [DataContainer sharedManager];
        /////////////////////////////////
        __block NSNumber *currentUploadsCount;
        __block NSString *currentTimeStamp;
        currentTimeStamp = timestamp;
        if(_offlineUploadCount > 0){
            _currentUploadsCount = [NSNumber numberWithInteger:_offlineUploadCount];// + sharedInstance.onlineVideoCount];//[NSNumber numberWithInt:(int)currentUploadItems.count];
            //sharedManager.uploadCount = _offlineUploadCount;
        } else {
            _currentUploadsCount = [NSNumber numberWithInteger:sharedInstance.onlineVideoCount];
        }
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              sharedManager = [DataContainer sharedManager];
                              if(_isShowThumnail && _offlineUploadCount > 0) {
                                  if(!_isFromBackground)
                                      [self showStaticThumbnailOnMyCorner]; // To show thumbnails when beam is uploading.
                              }else {
                                  _currentUploadsCount = [NSNumber numberWithInteger:shareobj.onlineVideoCount];
                              }
                              uploadingProgress = uploadProgress.fractionCompleted;
    //                          NSDictionary *prog = @{@"progress": [NSNumber numberWithFloat:uploadingProgress],@"index":_currentUploadsCount};

                              NSDictionary *prog = @{@"progress": [NSNumber numberWithFloat:uploadingProgress],@"index":currentTimeStamp};

                              if(uploadingProgress <= 100.0  && uploadingProgress > 0.95){
                                  NSLog(@"ALMOST DONE");
                                  [[DBCommunicator sharedManager] deleteRowWithID:_offlineRowID];

                              } else {
                                  [[NSNotificationCenter defaultCenter]
                                   postNotificationName:@"updateProgress"
                                   object:prog];
                                  _isShowThumnail = NO;
                              }
                              
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          
                          NSLog(@"JUST RETURNED");
                          dispatch_async(dispatch_get_main_queue(), ^{
                              
                              if(self.backgroundTask !=  UIBackgroundTaskInvalid){
                                  self.backgroundTask = UIBackgroundTaskInvalid;
                              }
                              if (error) {
                                  APP_DELEGATE.isUploading = NO;
                                  NSDictionary *prog = @{@"progress": [NSNumber numberWithFloat:0.0],@"index":_currentUploadsCount};
    //                              [[DBCommunicator sharedManager] clearDB];
                                 
                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"internetDisconnected" object:nil];
                                  [[NSNotificationCenter defaultCenter]
                                   postNotificationName:@"updateProgress"
                                   object:prog];
                                  //                          [self removeIfnIfOffline];
                                  //                          [self saveOfflineDataIsAudio:NO];
                              } else {
                                  
                                  NSLog(@"UPLOAD SUCCESSFULL");

                                  coreMngr = [CoreDataManager sharedManager];
                                  //[coreMngr deleteSpecificRecord:0];
                                  
                                  if ([coreMngr.offlineContentArray count]>0) {
                                      [coreMngr deleteSpecificRecord:0];
                                      CDHydepark *oldcd = (CDHydepark *)[coreMngr.offlineContentArray dequeue];
                                      NSLog(@"%@", oldcd);
                                  }
                                  
                                  if(isAnonymous) {
                                      NSDate *myDate = [NSDate date];
                                      [[NSUserDefaults standardUserDefaults] setObject:myDate forKey:@"lastAnonymusBeam"];
                                      [[NSUserDefaults standardUserDefaults] synchronize];
                                  }
                                  
                                  APP_DELEGATE.isUploading = NO;
    //                               NSDictionary *completeIndex = @{@"index":_currentUploadsCount};
                                  NSDictionary *completeIndex;
                                  NSArray *tempArray = [[DBCommunicator sharedManager] retrieveAllRows];
                                  sharedManager = [DataContainer sharedManager];
                                  if(sharedManager._isOnlineVideo) {
                                      NSNumber *offlineCount = [NSNumber numberWithInteger:tempArray.count];
                                      completeIndex = @{@"index":currentTimeStamp};
                                  } else {
                                      completeIndex = @{@"index":currentTimeStamp};
                                  }
                                 
                                  
                                  if([responseObject objectForKey:@"success"]==0 ||[[responseObject objectForKey:@"success"] isEqual:@"0"]){
                                      
                                      
                                      NSLog(@"here");
                                  
                                  }else{
                                     
                                      NSDictionary *dict = [responseObject objectForKey:@"post"];
                                      VideoModel *_Videos = [[VideoModel alloc] initWithDictionary:dict];
                                     
                                  
                                      if(_Videos.uploaded_date != NULL && _Videos.current_datetime != NULL)
                                      {
                                          
                                          NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                                          if(!([_Videos.privacy rangeOfCharacterFromSet:notDigits].location == NSNotFound))
                                          {
                                              [self updateMyCorner:_Videos];
                                          }
                                          
                                          
                                          [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"myCornerThumbnail"];
                                          [[NSNotificationCenter defaultCenter]
                                           postNotificationName:@"completeProgress"
                                           object:completeIndex];
                                          appDelegate.videObj = _Videos;
                                          
                                          
                                          
                                      }
                                      
                                  }
                                  
                         ////////////// FOR OFFLINE MODULE /////////////////
                                  
                                  [[DBCommunicator sharedManager] deleteRowWithID:_offlineRowID];
                                  
                                  NSLog(@"RECORD HAS BEEN DELETED");
                                  NSArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
                                  if(offlineArray.count > 0) {
                                      [self uploadOfflineBeam];
                                  }

                        ////////////////////////////////////
                                  
                                  [self showAlert];
                                 
                                  //////////////////////////////dequeue working
                                  coreMngr = [CoreDataManager sharedManager];
                                  if ([coreMngr.offlineContentArray count]>0) {
                                      //[coreMngr deleteSpecificRecord:0];
                                      CDHydepark *cd = (CDHydepark *)[coreMngr.offlineContentArray peek];
                                      
                                      [self setupOfflineUploadingContents:cd];
                                  }
                                  
                                  
                                  
                              }
                          });
                      }];
        [uploadTask resume];
    }
}


- (void)compressVideo:(NSURL*)inputURL
            outputURL:(NSURL*)outputURL
              handler:(void (^)(AVAssetExportSession*))completion  {
    AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:urlAsset presetName:AVAssetExportPresetMediumQuality];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    exportSession.shouldOptimizeForNetworkUse = YES;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        completion(exportSession);
    }];
}



-(void) uploadComment:(NSData*)file{
    _increaseCommentCount = NO;
    _isShowThumnail = YES;
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    if(_isShowProgress == 1){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"ShowProgressComments"
         object:nil];
    }
    NSString *anony = (isAnonymous) ? @"1" : @"0";
    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
    if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")]|| textToshare.length == 0){
        textToshare = @"";
    }
    else{
        if([textToshare rangeOfString:@"\\"].length > 0)
        {
            
        }
        else
        {
            textToshare = [Utils encodeForEmojis:textToshare];
        }
    }
    
    NSString *tag_string;
    NSString *language = [Utils getLanguageCode];
    NSMutableDictionary *postDict;
    if(_orientationAngle == 180 || _orientationAngle == 270) {
         NSString *angle = [NSString stringWithFormat:@"%ld",_orientationAngle];
        postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:METHOD_COMMENTS_POST,@"method",userSession,@"Session_token",commentAllowed,@"reply_count",textToshare,@"caption",anony,@"is_anonymous",@"0",@"mute",video_duration,@"video_length",postID,@"post_id",ParentCommentID,@"parent_comment_id",privacySelected,@"privacy",@"90",@"video_angle",@"COLOUR",@"filter",language,@"language",angle,@"rotation_flag",nil];
    } else {
         postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:METHOD_COMMENTS_POST,@"method",userSession,@"Session_token",commentAllowed,@"reply_count",textToshare,@"caption",anony,@"is_anonymous",@"0",@"mute",video_duration,@"video_length",postID,@"post_id",ParentCommentID,@"parent_comment_id",privacySelected,@"privacy",@"90",@"video_angle",@"COLOUR",@"filter",language,@"language",nil];
    }
    NSDate *dateObj = [self getDateFromString:timestamp];
    
    
    NSTimeInterval timeStamp = [dateObj timeIntervalSince1970];
    // NSTimeInterval is defined as double
    NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
    timestamp = timeStampObj.stringValue;
    
    [postDict setObject:timestamp forKey:@"timestamp"];
    if(finalArray == nil || [finalArray count] == 0){
        
    }
    else
    {
        tag_string = [finalArray componentsJoinedByString:@","];
        [postDict setObject:tag_string forKey:@"tag_friends"];
    }
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:SERVER_URL parameters:postDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:file name:@"video_link" fileName:[NSString stringWithFormat:@"%@.mp4",@"video"] mimeType:@"recording/video"];
        [formData appendPartWithFileData:profileData name:@"video_thumbnail_link" fileName:[NSString stringWithFormat:@"%@.png",@"thumbnail"] mimeType:@"image/png"];
        
    } error:nil];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    NSURLSessionUploadTask *uploadTask;
    
    DataContainer *shareobj = [DataContainer sharedManager];
    
    NSMutableArray *commentsCurrent = shareobj.commentsDict[postID];
    
    [[NSUserDefaults standardUserDefaults] setObject:profileData forKey:@"commentThumbnail"];
    __block NSNumber *currentCommentsCount;
    if(_offlineUploadCount > 0) {
        currentCommentsCount = [NSNumber numberWithInteger:_offlineUploadCount]; //+ commentsCurrent.count];//[NSNumber numberWithInt:(int)commentsCurrent.count];
    } else {
        currentCommentsCount = [NSNumber numberWithInteger:shareobj.commentsCurrentCount-1];
    }
    if([currentCommentsCount integerValue] <0) {
        currentCommentsCount = [NSNumber numberWithInteger:0];
    }
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                          if(_isShowThumnail && _offlineUploadCount > 0) {
                              [self showStaticThumbnailOnComments];
                          } else {
                              currentCommentsCount = [NSNumber numberWithInteger:shareobj.commentsCurrentCount-1];
                          }
                          uploadingProgress = uploadProgress.fractionCompleted;
                          //apDelegate.progressFloat = uploadingProgress;
                          // _commentUploadCount = currentCommentsCount;
                          if(uploadingProgress <= 100.0  && uploadingProgress > 0.95){
                              NSLog(@"ALMOST DONE");
                              [[DBCommunicator sharedManager] deleteRowWithID:_offlineRowID];

                              
                          } else {
                              
                              NSDictionary *prog = @{@"progress": [NSNumber numberWithFloat:uploadingProgress],@"index": currentCommentsCount};
                              [[NSNotificationCenter defaultCenter]
                               postNotificationName:@"updateProgressComments"
                               object:prog];
                              _isShowThumnail = NO;
                          }
                          
                         
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if(self.backgroundTask !=  UIBackgroundTaskInvalid){
                          self.backgroundTask = UIBackgroundTaskInvalid;
                      }
                      if (error) {
                          
                          NSLog(@"Error %@", error);
                          NSLog(@"response %@", response);

                          
                      } else {
                          
                          coreMngr = [CoreDataManager sharedManager];
                          
                          
                          
                          
                          if(isAnonymous) {
                              NSDate *myDate = [NSDate date];
                              [[NSUserDefaults standardUserDefaults] setObject:myDate forKey:@"lastAnonymusBeam"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                          }
                          
                          VideoModel *_Videos     = [[VideoModel alloc] init];
                          NSArray *post = [responseObject objectForKey:@"coment"];
                          
                          NSInteger tempCommentsCount = [_commentUploadCount integerValue];
                          _commentUploadCount = [NSNumber numberWithInteger:tempCommentsCount-1];
                           ////////////// FOR OFFLINE MODULE /////////////////
                          
                          
                          shareobj.offlineArrayCount = shareobj.offlineArrayCount-1;
                          
                          [[DBCommunicator sharedManager] deleteRowWithID:_offlineRowID];
                          NSArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
                          
                          ///////////////////////////////////
                          
                          for(int i=0;i<post.count; i++) {
                              NSDictionary *tempDict = [post objectAtIndex:i];
                              _Videos     = [[VideoModel alloc] initWithDictionary:tempDict];
                              appDelegate.commentObj = _Videos;
                              appDelegate.hasBeenUpdated = true;
                              break;
                          }
                          
                          DataContainer *shareobj = [DataContainer sharedManager];
                          
                          NSMutableArray *comments = shareobj.commentsDict[_Videos.cpost_id];
                         
                          int commentsCount ;
                          
                           ////////////// FOR OFFLINE MODULE /////////////////
                          
                          if (offlineArray.count>0){
                              
                              [comments removeLastObject];
                              
                              
                              commentsCount=(int)offlineArray.count;
                              //shareobj.commentsDict[_Videos.parentId] = comments;
                              
                           //   [shareobj.commentsDict setObject:comments forKey:_Videos.cpost_id];
                              
                              
                              
//                              coreMngr = [CoreDataManager sharedManager];
//                              if ([coreMngr.offlineCommentsArray count]>0) {              OLD CODE
//                                  [coreMngr deleteSpecificComment:0];
//                                  CDHydepark *oldcd = (CDHydepark *)[coreMngr.offlineCommentsArray dequeue];
//
//                                  NSLog(@"%@", oldcd);
//                              }
                              
                              //if (indexComment<dataArray.count){
                          }else{
                              DataContainer *sharedInstance = [DataContainer sharedManager];
                              
                              commentsCount = (int)sharedInstance.commentsCurrentCount-1; // to get previous
                              if(commentsCount < 0) {
                                  commentsCount = 0;
                              }
//                              commentsCount = 0;
                          }
                           DataContainer *sharedInstance = [DataContainer sharedManager];
                          if(sharedInstance.commentsCurrentCount > 0) {
                              sharedInstance.commentsCurrentCount--;
                          }
                          ///////////////////////////////
                          
                          //[dataArray replaceObjectAtIndex:indexComment withObject:video];
                          
                          NSDictionary *data = @{@"data": _Videos,@"index":[NSNumber numberWithInt:commentsCount]};
                          //currentCommentsCount = [NSNumber numberWithInteger:(int)currentCommentsCount - 1];
                         // DataContainer *sharedInstance = [DataContainer sharedManager];
        
                          
                           ////////////// FOR OFFLINE MODULE /////////////////
                          
                          if(offlineArray.count > 0) {
                              [self uploadOfflineBeam];
                          }
                          
                          ////////////////////////////
                          [self showAlert];
                          [[NSNotificationCenter defaultCenter]
                           postNotificationName:@"updateCommentsArray"
                           object:data];
                          [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"commentThumbnail"];
                          
                          
                         
                          //////////////////////////////peek working
                          
                          
//                          coreMngr = [CoreDataManager sharedManager];
//                          if ([coreMngr.offlineCommentsArray count]>0) {          OLD CODE
//                              //[coreMngr deleteSpecificRecord:0];
//                              CDHydepark *cd = (CDHydepark *)[coreMngr.offlineCommentsArray peek];
//
//                              [self setupOfflineUploadingComments:cd];
//                          }
                      }
                  }];
    [uploadTask resume];
}


-(void) uploadAudioComment:(NSData*)file{
    _increaseCommentCount = NO;
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    //    [[NSNotificationCenter defaultCenter]
    //     postNotificationName:@"ShowProgressComments"
    //     object:nil];
    NSString *anony = (isAnonymous) ? @"1" : @"0";
    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
    if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")]|| textToshare.length == 0){
        textToshare = @"";
    }
    else{
        if([textToshare rangeOfString:@"\\"].length > 0)
        {
            
        }
        else
        {
            textToshare = [Utils encodeForEmojis:textToshare];
        }
    }
    
    NSString *tag_string;
    NSString *language = [Utils getLanguageCode];
    NSMutableDictionary *postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:METHOD_COMMENTS_POST,@"method",userSession,@"Session_token",commentAllowed,@"reply_count",textToshare,@"caption",anony,@"is_anonymous",@"0",@"mute",video_duration,@"video_length",postID,@"post_id",ParentCommentID,@"parent_comment_id",privacySelected,@"privacy",@"COLOUR",@"filter",language,@"language",nil];
    NSDate *dateObj = [self getDateFromString:timestamp];
    
    
    NSTimeInterval timeStamp = [dateObj timeIntervalSince1970];
    // NSTimeInterval is defined as double
    NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
    timestamp = timeStampObj.stringValue;
    
    [postDict setObject:timestamp forKey:@"timestamp"];
    if(finalArray == nil || [finalArray count] == 0){
        
    }
    else
    {
        tag_string = [finalArray componentsJoinedByString:@","];
        [postDict setObject:tag_string forKey:@"tag_friends"];
    }
    NSLog(@"%lu",(unsigned long)file.length);
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:SERVER_URL parameters:postDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:file name:@"audio_link" fileName:[NSString stringWithFormat:@"%@.wav",@"sound"] mimeType:@"audio/wav"];
        [formData appendPartWithFileData:playBtnThumbnail name:@"video_thumbnail_link2" fileName:[NSString stringWithFormat:@"%@.png",@"thumbnail"] mimeType:@"image/png"];
    } error:nil];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    NSURLSessionUploadTask *uploadTask;
    
    UIImage *audioImage = [UIImage imageNamed:@"splash_audio_image.png"];
    NSData *imageData = UIImagePNGRepresentation(audioImage);
    [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"commentThumbnail"];
//     NSArray *offlineContent = [[DBCommunicator sharedManager] retrieveAllRows];
//
//    NSMutableArray *commentsCurrent = offlineContent.mutableCopy;
    DataContainer *sharedInstance = [DataContainer sharedManager];
    DataContainer *shareobj = [DataContainer sharedManager];
    
    NSMutableArray *commentsCurrent = shareobj.commentsDict[postID];
//    NSNumber *currentCommentsCount = [NSNumber numberWithInteger:_offlineUploadCount + shareobj.commentsCount];//[NSNumber numberWithInt:(int)commentsCurrent.count];
    __block NSNumber *currentCommentsCount;
    if(_offlineUploadCount > 0) {
        currentCommentsCount = [NSNumber numberWithInteger:_offlineUploadCount]; //+ commentsCurrent.count];//[NSNumber numberWithInt:(int)commentsCurrent.count];
    } else {
        currentCommentsCount = [NSNumber numberWithInteger:shareobj.commentsCurrentCount-1];
    }
    if([currentCommentsCount integerValue] <0) {
        currentCommentsCount = [NSNumber numberWithInteger:0];
    }
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                          if(_isShowThumnail && _offlineUploadCount > 0) {
                              [self showStaticThumbnailOnComments];
                          } else {
                              currentCommentsCount = [NSNumber numberWithInteger:shareobj.commentsCurrentCount-1];
                          }

                          uploadingProgress = uploadProgress.fractionCompleted;
                          //apDelegate.progressFloat = uploadingProgress;
                          //NSDictionary *prog = @{@"progress": [NSNumber numberWithFloat:uploadingProgress]};
                         // _commentUploadCount = currentCommentsCount;
                          
                          if(uploadingProgress <= 100.0  && uploadingProgress > 0.95){
                              
                              NSLog(@"ALMOST DONE");
                              [[DBCommunicator sharedManager] deleteRowWithID:_offlineRowID];

                          } else {
                              
                              NSDictionary *prog = @{@"progress": [NSNumber numberWithFloat:uploadingProgress],@"index": currentCommentsCount};
                              
                              [[NSNotificationCenter defaultCenter]
                               postNotificationName:@"updateProgressComments"
                               object:prog];
                              _isShowThumnail = NO;
                          }
                         
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      
                      
                      if (error) {
                          
                      } else {
                          
                         
//                          coreMngr = [CoreDataManager sharedManager];
//
//
//                          if ([coreMngr.offlineCommentsArray count]>0) {              OLD CODE
//                              [coreMngr deleteSpecificComment:0];
//
//                              CDHydepark *oldcd = (CDHydepark *)[coreMngr.offlineCommentsArray dequeue];
//
//                              NSLog(@"%@", oldcd);
//                          }
                          
                          if(isAnonymous) {
                              NSDate *myDate = [NSDate date];
                              [[NSUserDefaults standardUserDefaults] setObject:myDate forKey:@"lastAnonymusBeam"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                          }
                          
                          VideoModel *_Videos     = [[VideoModel alloc] init];
                          NSArray *post = [responseObject objectForKey:@"coment"];
                          
                          for(int i=0;i<post.count; i++) {
                              NSDictionary *tempDict = [post objectAtIndex:i];
                              _Videos     = [[VideoModel alloc] initWithDictionary:tempDict];
                              appDelegate.commentObj = _Videos;
                              appDelegate.hasBeenUpdated = true;
                              break;
                              
                          }
                          
                          NSInteger tempCommentsCount = [_commentUploadCount integerValue];
                          _commentUploadCount = [NSNumber numberWithInteger:tempCommentsCount-1];
                           ////////////// FOR OFFLINE MODULE /////////////////
                          
                          shareobj.offlineArrayCount = shareobj.offlineArrayCount-1;
                          [[DBCommunicator sharedManager] deleteRowWithID:_offlineRowID];
                          NSArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
                          
                          ////////////////////////////////
                          DataContainer *shareobj = [DataContainer sharedManager];
                          
                          NSMutableArray *comments = shareobj.commentsDict[_Videos.cpost_id];
                          
                          int commentsCount ;
                          
                           ////////////// FOR OFFLINE MODULE /////////////////
                          if (offlineArray.count>0){
                              
                              [comments removeLastObject];
                              
                              
                              commentsCount=(int)offlineArray.count;
                              //shareobj.commentsDict[_Videos.parentId] = comments;
                              
                            //  [shareobj.commentsDict setObject:comments forKey:_Videos.cpost_id];
                              //if (indexComment<dataArray.count){
                          }else{
//                              DataContainer *sharecommentobj = [DataContainer sharedManager];                 OLD CODE
//                              commentsCount = (int)sharedInstance.commentsCount;//0                           OLD CODE
                              
                              DataContainer *sharedInstance = [DataContainer sharedManager];
                              
                              commentsCount = (int)sharedInstance.commentsCurrentCount-1; // to get previous
                              if(commentsCount < 0) {
                                  commentsCount = 0;
                              }
                              
                          }
                          ///////////////////////////////////////
                          //[dataArray replaceObjectAtIndex:indexComment withObject:video];
                          
                          NSDictionary *data = @{@"data": _Videos,@"index":[NSNumber numberWithInt:commentsCount]};
                          DataContainer *sharecommentobj = [DataContainer sharedManager];
                          if(sharecommentobj.commentsCurrentCount > 0) {
                              sharecommentobj.commentsCurrentCount--;
                          }
                          
                         
                          
                           ////////////// FOR OFFLINE MODULE /////////////////
                          
                          if(offlineArray.count > 0) {
                              [self uploadOfflineBeam];
                          }
                          
                          //////////////////////////////////
                          [self showAlert];
                          [[NSNotificationCenter defaultCenter]
                           postNotificationName:@"updateCommentsArray"
                           object:data];
                          
                          //////////////////////////////dequeue working
                          
                          
//                          coreMngr = [CoreDataManager sharedManager];
//                          if ([coreMngr.offlineCommentsArray count]>0) {          OLD CODE
//                              //[coreMngr deleteSpecificRecord:0];
//                              CDHydepark *cd = (CDHydepark *)[coreMngr.offlineCommentsArray peek];
//
//                              [self setupOfflineUploadingComments:cd];
//                          }
                          
                      }
                  }];
    [uploadTask resume];
}
-(void) uploadAduio:(NSData*)file{
    //testing
    //_increaseCommentCount = NO;
    APP_DELEGATE.isUploading = YES;
    _isShowThumnail = YES;
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    if(_isShowProgress == 1) {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"ShowProgress"
         object:nil];
    }
    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
    if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare == nil){
        textToshare = @"";
    }else{
        textToshare = [Utils encodeForEmojis:textToshare];
    }
    
    NSString *tag_string;
    NSString *topic;
    NSString *language = [Utils getLanguageCode];
    NSMutableDictionary *postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:METHOD_UPLOAD_STATUS,@"method",userSession,@"Session_token",commentAllowed,@"reply_count",textToshare,@"caption",anony,@"is_anonymous",@"0",@"mute",video_duration,@"video_length",postID,@"post_id",ParentCommentID,@"parent_comment_id",privacySelected,@"privacy",language,@"language",nil];
    
    NSDate *dateObj = [self getDateFromString:timestamp];
    
    
    NSTimeInterval timeStamp = [dateObj timeIntervalSince1970];
    // NSTimeInterval is defined as double
    NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
    timestamp = timeStampObj.stringValue;
    
    [postDict setObject:timestamp forKey:@"timestamp"];
    
    if(finalArray && finalArray.count > 0 ){
        tag_string = [finalArray componentsJoinedByString:@","];
        [postDict setObject:tag_string forKey:@"tag_friends"];
    } else {
        tag_string = @"empty";
    }
    if(tagsString.length > 0){
        [postDict setObject:tagsString forKey:@"topic_name"];
    } else {
        topic = @"empty";
    }
    
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:SERVER_URL parameters:postDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:file name:@"audio_link" fileName:[NSString stringWithFormat:@"%@.wav",@"sound"] mimeType:@"audio/wav"];
        
    } error:nil];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    UIImage *audioImage = [UIImage imageNamed:@"splash_audio_image.png"];
    NSData *imageData = UIImagePNGRepresentation(audioImage);
    [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"myCornerThumbnail"];
     ////////////// FOR OFFLINE MODULE /////////////////
    
    NSArray *offlineContent = [[DBCommunicator sharedManager] retrieveAllRows];
    NSMutableArray *currentUploadItems = offlineContent.mutableCopy;
    
    DataContainer *sharedInstance = [DataContainer sharedManager];
    ////////////////////////
    __block NSNumber *currentUploadsCount;
    __block NSString *currentTimeStamp;
    currentTimeStamp = timestamp;
    if(_offlineUploadCount > 0){
        _currentAudioUploadCount = [NSNumber numberWithInteger:_offlineUploadCount];// + sharedInstance.onlineVideoCount];//[NSNumber numberWithInt:(int)currentUploadItems.count];
//        sharedManager.uploadCount = _offlineUploadCount;
    } else {
        _currentAudioUploadCount = [NSNumber numberWithInteger:sharedInstance.onlineVideoCount];
    }
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                          sharedManager.isUploading = YES;
                          if(_isShowThumnail && _offlineUploadCount > 0) {
                              if(!_isFromBackground)
                                  [self showStaticThumbnailOnMyCorner]; // To show thumbnails when beam is uploading.
                             } else {
                              _currentAudioUploadCount = [NSNumber numberWithInteger:shareobj.onlineVideoCount];
                          }
                          uploadingProgress = uploadProgress.fractionCompleted;
//                          NSDictionary *prog = @{@"progress": [NSNumber numberWithFloat:uploadingProgress],@"index":_currentAudioUploadCount};
                          NSDictionary *prog = @{@"progress": [NSNumber numberWithFloat:uploadingProgress],@"index":currentTimeStamp};
                          NSLog(@"Upload Progress Index: %@, with Progress: %f",_currentAudioUploadCount, uploadingProgress);
                          
                          if(uploadingProgress <= 100.0  && uploadingProgress > 0.95){

                              NSLog(@"ALMOST DONE");
                              [[DBCommunicator sharedManager] deleteRowWithID:_offlineRowID];

                          } else {
                              [[NSNotificationCenter defaultCenter]
                               postNotificationName:@"updateProgress"
                               object:prog];
                              _isShowThumnail = NO;
                          }
                         
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if(self.backgroundTask !=  UIBackgroundTaskInvalid){
                          self.backgroundTask = UIBackgroundTaskInvalid;
                      }
                      if (error) {
                          APP_DELEGATE.isUploading = NO;
                          NSDictionary *prog = @{@"progress": [NSNumber numberWithFloat:0.0],@"index":_currentAudioUploadCount};
                          
                          [[NSNotificationCenter defaultCenter]
                           postNotificationName:@"updateProgress"
                           object:prog];
                          sharedManager.isUploading = NO;
//                          [self removeIfnIfOffline];
//                          [self saveOfflineDataIsAudio:YES];
                      } else {
                          
                          APP_DELEGATE.isUploading = NO;
//                          NSDictionary *completeIndex = @{@"index":_currentAudioUploadCount};
//
                          NSDictionary *completeIndex;
                          NSArray *tempArray = [[DBCommunicator sharedManager] retrieveAllRows];
                          sharedManager = [DataContainer sharedManager];
                          if(sharedManager._isOnlineVideo) {
                              NSNumber *offlineCount = [NSNumber numberWithInteger:tempArray.count];
                              completeIndex = @{@"index":offlineCount};
                          } else {
                              completeIndex = @{@"index":_currentAudioUploadCount};
                          }
                          
                          
                          coreMngr = [CoreDataManager sharedManager];
                          //[coreMngr deleteSpecificRecord:0];
                          
                          if ([coreMngr.offlineContentArray count]>0) {
                              [coreMngr deleteSpecificRecord:0];
                              CDHydepark *oldcd = (CDHydepark *)[coreMngr.offlineContentArray dequeue];
                              
                              NSLog(@"%@", oldcd);
                          }
                          
                          if(isAnonymous) {
                              NSDate *myDate = [NSDate date];
                              [[NSUserDefaults standardUserDefaults] setObject:myDate forKey:@"lastAnonymusBeam"];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                          }
                          
                          NSDictionary *dict = [responseObject objectForKey:@"post"];
                          VideoModel *_Videos = [[VideoModel alloc] init];
                          _Videos.title             = [dict objectForKey:@"caption"];
                          _Videos.comments_count    = [dict objectForKey:@"comment_count"];
                          _Videos.userName          = [dict objectForKey:@"full_name"];
                          _Videos.topic_id          = [dict objectForKey:@"topic_id"];
                          _Videos.user_id           = [dict objectForKey:@"user_id"];
                          _Videos.profile_image     = [dict objectForKey:@"profile_link"];
                          _Videos.like_count        = [dict objectForKey:@"like_count"];
                          _Videos.like_by_me        = [dict objectForKey:@"liked_by_me"];
                          _Videos.seen_count        = [dict objectForKey:@"seen_count"];
                          _Videos.video_angle       = [[dict objectForKey:@"video_angle"] intValue];
                          _Videos.video_link        = [dict objectForKey:@"video_link"];
                          _Videos.m3u8_video_link   = [dict objectForKey:@"m3u8_video_link"];
                          _Videos.video_thumbnail_link = [dict objectForKey:@"video_thumbnail_link"];
                          _Videos.videoID           = [dict objectForKey:@"id"];
                          _Videos.Tags              = [dict objectForKey:@"tag_friends"];
                          _Videos.video_length      = [dict objectForKey:@"video_length"];
                          _Videos.is_anonymous      = [dict objectForKey:@"is_anonymous"];
                          _Videos.reply_count       = [dict objectForKey:@"reply_count"];
                          _Videos.current_datetime  = [dict objectForKey:@"current_datetime"];
                          _Videos.uploaded_date     = [dict objectForKey:@"uploaded_date"];
                          _Videos.beam_share_url = [dict objectForKey:@"beam_share_url"];
                          _Videos.isLocal = NO;
                          _Videos.deep_link = [dict objectForKey:@"deep_link"];
                          _Videos.isThumbnailReq = 1;
                          if(isAnonymous) {
                              _Videos.is_anonymous = @"1";
                          }
                          //apDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                          
                          appDelegate.videObj = _Videos;
                          NSDictionary *data = @{@"data": _Videos};
                          
                          //                          [[NSNotificationCenter defaultCenter]
                          //                           postNotificationName:@"updateMyCornerArray"
                          //                           object:data];
                          [[NSNotificationCenter defaultCenter]
                           postNotificationName:@"completeProgress"
                           object:completeIndex];
                          sharedManager.isUploading = NO;
                          [self updateMyCorner:_Videos];
                          
                           ////////////// FOR OFFLINE MODULE /////////////////
                          
                          [[DBCommunicator sharedManager] deleteRowWithID:_offlineRowID];
                          NSArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
                          
                          if(offlineArray.count > 0) {
                              [self uploadOfflineBeam];
                          }
                          
                         ////////////////////////////////
                        
                          [self showAlert];
                         
                          //////////////////////////////dequeue working
                          coreMngr = [CoreDataManager sharedManager];
                          if ([coreMngr.offlineContentArray count]>0) {
                              CDHydepark *cd = (CDHydepark *)[coreMngr.offlineContentArray peek];
                              //  [coreMngr deleteSpecificRecord:0];
                              
                              [self setupOfflineUploadingContents:cd];
                          }
                          
                      }
                  }];
    [uploadTask resume];
}

-(void)updateMyCorner:(VideoModel *)vMObj{
    UINavigationController *nav = [[NavigationHandler getInstance] getNavigationController];
    NSArray *viewControllers = nav.viewControllers;
    for (UIViewController *anVC in viewControllers) {
        if([anVC isKindOfClass:[HomeViewController class]]){
            HomeViewController *home = (HomeViewController *)anVC;
            NSArray *vccs = home.viewControllers;
            UIViewController *topPageController = [vccs objectAtIndex:0];
            if([topPageController isKindOfClass:[MyCornerVC class]]){
                MyCornerVC *cornerVC = (MyCornerVC *)topPageController;
                
                 ////////////// FOR OFFLINE MODULE /////////////////
                
                cornerVC.offlineArrayCount = shareobj.offlineArrayCount;
                shareobj.offlineArrayCount = shareobj.offlineArrayCount-1;
                
                DataContainer *sharedInstance = [DataContainer sharedManager];
                if(sharedInstance.onlineVideoCount > 0){
                    cornerVC.onlineArrayCount = sharedInstance.onlineVideoCount;
                    sharedInstance.onlineVideoCount--;
                }
                /////////////////////////////
                [cornerVC updateCornerArray:vMObj];
//                [cornerVC getMyChannel];
            }
        }
    }
}

-(void) showStaticThumbnailOnMyCorner {
    NSNumber *i = [NSNumber numberWithInt:1];
    NSArray *offlineModelArray = [[DBCommunicator sharedManager] retrieveAllRows];
    if(APP_DELEGATE.hasInet) {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"updateThumbnailOnUpload"
         object:nil];
    } else {
        if(offlineModelArray > 0) {
            for (OfflineDataModel *offlineModel in offlineModelArray) {
                NSData *thumbnail = [self getThumbnailFromDocuments:offlineModel.thumbnail];
                if([offlineModel.isAudio isEqualToString:@"1"]) {
                    UIImage *audioImage = [UIImage imageNamed:@"splash_audio_image.png"];
                    thumbnail = UIImagePNGRepresentation(audioImage);
                }
                NSDictionary *prog = @{@"data": thumbnail,@"index":i};
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"updateThumbnail"
                 object:prog];
                if([i integerValue] !=offlineModelArray.count )
                    i = [NSNumber numberWithInt:[i intValue] + 1];
            }
        }
    }
}

-(void) showStaticThumbnailOnComments {
    NSNumber *i = [NSNumber numberWithInt:0];
    NSArray *offlineModelArray = [[DBCommunicator sharedManager] retrieveAllRows];
    if(APP_DELEGATE.hasInet) {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"updateCommentThumbnailOnUpload"
         object:nil];
    } else {
        if(offlineModelArray > 0) {
            for (OfflineDataModel *offlineModel in offlineModelArray) {
                playBtnThumbnail = [self getThumbnailFromDocuments:offlineModel.thumbnail];
                profileData = playBtnThumbnail;
                if([offlineModel.isAudio isEqualToString:@"1"]) {
                    UIImage *audioImage = [UIImage imageNamed:@"splash_audio_image.png"];
                    profileData = UIImagePNGRepresentation(audioImage);
                }
                NSDictionary *prog = @{@"data": profileData,@"index":i};
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"updateThumbnailOnComment"
                 object:prog];
                if([i integerValue] !=offlineModelArray.count )
                    i = [NSNumber numberWithInt:[i intValue] + 1];
            }
        }
    }
}

-(void) showAlert{
    alert = [[Alert alloc] initWithTitle:NSLocalizedString(@"beam_uploaded", @"")   duration:(float)2.0f completion:^{
        //
    }];
    [alert setDelegate:self];
    [alert setShowStatusBar:YES];
    [alert setAlertType:AlertTypeSuccess];
    [alert setIncomingTransition:AlertIncomingTransitionTypeSlideFromTop];
    [alert setOutgoingTransition:AlertOutgoingTransitionTypeSlideToTop];
    [alert setBounces:YES];
    [alert showAlert];
}

- (void)showNoInternetAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"no_internet", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
    [alertView show];
    
}
- (IBAction)uploadBeamBackPressed:(id)sender {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:NSLocalizedString(@"beamView_popup_message", @"")
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *y = [UIAlertAction
                        actionWithTitle:NSLocalizedString(@"cancel", @"")
                        style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction * action) {
                            //Handle your yes please button action here
                        }];
    
    UIAlertAction *n = [UIAlertAction
                        actionWithTitle:NSLocalizedString(@"discard", @"")
                        style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction * action) {
                            //Handle no, thanks button
                            if(isComment){
                                [self.navigationController popViewControllerAnimated:YES];
                            }
                            else{
                                [self.navigationController popToRootViewControllerAnimated:YES];
                            }
                        }];
    
    [alertController addAction:y];
    [alertController addAction:n];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
- (IBAction)PrivacyEveryOne:(id)sender {
    [cpeveryone setImage:[UIImage imageNamed:@"blueradio.png"]];
    [cponlyme setImage:[UIImage imageNamed:@"greyradio.png"]];
    [cpfriends setImage:[UIImage imageNamed:@"greyradio.png"] ];
    everyOnelbl.textColor = [UIColor colorWithRed:54.0/256.0 green:78.0/256.0 blue:141.0/256.0 alpha:1.0];
    onlyMelbl.textColor = [UIColor darkGrayColor];
    Friendslbl.textColor = [UIColor darkGrayColor];
    privacySelected = @"PUBLIC";
    
    _privacyLbl.text = NSLocalizedString(@"everyone", @"");
    
}

- (IBAction)PrivacyOnlyMe:(id)sender {
    [cpeveryone setImage:[UIImage imageNamed:@"greyradio.png"]];
    [cponlyme setImage:[UIImage imageNamed:@"blueradio.png"]];
    [cpfriends setImage:[UIImage imageNamed:@"greyradio.png"] ];
    onlyMelbl.textColor = [UIColor colorWithRed:54.0/256.0 green:78.0/256.0 blue:141.0/256.0 alpha:1.0];
    everyOnelbl.textColor = [UIColor darkGrayColor];
    Friendslbl.textColor = [UIColor darkGrayColor];
    privacySelected = @"PRIVATE";
    
    _privacyLbl.text = NSLocalizedString(@"only_me", @"");
}

- (IBAction)PrivacyFriends:(id)sender {
    [cpeveryone setImage:[UIImage imageNamed:@"greyradio.png"] ];
    [cponlyme setImage:[UIImage imageNamed:@"greyradio.png"]];
    [cpfriends setImage:[UIImage imageNamed:@"blueradio.png"]];
    Friendslbl.textColor = [UIColor colorWithRed:54.0/256.0 green:78.0/256.0 blue:141.0/256.0 alpha:1.0];
    onlyMelbl.textColor = [UIColor darkGrayColor];
    everyOnelbl.textColor = [UIColor darkGrayColor];
    privacySelected = @"FRIENDS";
    
    _privacyLbl.text = NSLocalizedString(@"friends", @"");
}

- (IBAction)upto60Pressed:(id)sender {
    [up60 setImage:[UIImage imageNamed:@"blueradio.png"]];
    [noreply setImage:[UIImage imageNamed:@"greyradio.png"]];
    [unlimited setImage:[UIImage imageNamed:@"greyradio.png"] ];
    upto60.textColor = [UIColor colorWithRed:54.0/256.0 green:78.0/256.0 blue:141.0/256.0 alpha:1.0];
    Unlimited.textColor = [UIColor darkGrayColor];
    noreplies.textColor = [UIColor darkGrayColor];
    commentAllowed = @"50";
    
    _repliesLbl.text = NSLocalizedString(@"up_to_sixty", @"");
    
}

- (IBAction)noRepliesPressed:(id)sender {
    [up60 setImage:[UIImage imageNamed:@"greyradio.png"]];
    [noreply setImage:[UIImage imageNamed:@"blueradio.png"]];
    [unlimited setImage:[UIImage imageNamed:@"greyradio.png"] ];
    noreplies.textColor = [UIColor colorWithRed:54.0/256.0 green:78.0/256.0 blue:141.0/256.0 alpha:1.0];
    Unlimited.textColor = [UIColor darkGrayColor];
    upto60.textColor = [UIColor darkGrayColor];
    commentAllowed = @"0";
    
    _repliesLbl.text = NSLocalizedString(@"no_replies", @"");
}

- (IBAction)UnlimitedPressed:(id)sender {
    [up60 setImage:[UIImage imageNamed:@"greyradio.png"] ];
    [noreply setImage:[UIImage imageNamed:@"greyradio.png"]];
    [unlimited setImage:[UIImage imageNamed:@"blueradio.png"]];
    Unlimited.textColor = [UIColor colorWithRed:54.0/256.0 green:78.0/256.0 blue:141.0/256.0 alpha:1.0];
    upto60.textColor = [UIColor darkGrayColor];
    noreplies.textColor = [UIColor darkGrayColor];
    commentAllowed = @"-1";
    
    _repliesLbl.text = NSLocalizedString(@"unlimited", @"");
}

#pragma mark ASI delegates
- (void)request:(ASIHTTPRequest *)request incrementUploadSizeBy:(long long)newLength {
    //    NSLog(@"data length: %lld", newLength);
    //
}
- (void)requestFinished:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    //[self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Delegate Methods

- (void)alertWillAppear:(Alert *)alert {
    
}

- (void)alertDidAppear:(Alert *)alert {
    
}

- (void)alertWillDisappear:(Alert *)alert {
    
}

- (void)alertDidDisappear:(Alert *)alert {
    
}
#pragma mark offline

//////////////////////////////////////////
-(void) setupOfflineUploadingContents:(CDHydepark *)cd{
    dataToUpload = cd.file;
    commentAllowed = cd.commentAllowed;
    textToshare = cd.textToshare;
    anony = cd.anony;
    ParentCommentID = cd.parentCommentID;
    privacySelected = cd.privacySelected;
    video_duration = cd.video_duration;
    postID = cd.postID;
    timestamp = cd.timestamp;
    if ([cd.isAudio isEqualToString:@"1"]) {
        //coreMngr = [CoreDataManager sharedManager];
        //[coreMngr deleteSpecificRecord:0];
        [self uploadAduio:dataToUpload];
    }
    else{
        //video api calling
        profileData = cd.profileData;
//        coreMngr = [CoreDataManager sharedManager];
//        [coreMngr deleteSpecificRecord:0];
        [self uploadBeam:dataToUpload];
    }
}

-(void) setupOfflineUploadingComments:(CDHydepark *)cd{
    
    if(cd.file!=nil){
        
        dataToUpload = cd.file;
        commentAllowed = cd.commentAllowed;
        textToshare = cd.textToshare;
        anony = cd.anony;
        ParentCommentID = cd.parentCommentID;
        privacySelected = cd.privacySelected;
        video_duration = cd.video_duration;
        postID = cd.postID;
        timestamp = cd.timestamp;
        if ([cd.isAudio isEqualToString:@"1"]) {
            coreMngr = [CoreDataManager sharedManager];
            //[coreMngr deleteSpecificComment:0];
            [self uploadAudioComment:dataToUpload];
        }
        else{
            //video api calling
            profileData = cd.profileData;
            coreMngr = [CoreDataManager sharedManager];
            
            [self uploadComment:dataToUpload];
        }
        
    }
}

-(void) showOfflineContent:(CDHydepark *)cd{
    
    [self addLocallyCells:cd];
    
    
}

-(void) showOfflineComments:(CDHydepark *)cd{
    
    [self addLocalyCommentsCells:cd];
    
    
}


-(void) upLoadOflineContents:(NSNotification *) notification{
    
    if ([notification.name isEqualToString:@"showOflineComments"])
    {
        coreMngr = [CoreDataManager sharedManager];
        
        NSDictionary *data = notification.object;
        
        NSMutableArray *contentArray=[NSMutableArray arrayWithArray:data[@"data"]] ;
        
        for (int i=0; i<contentArray.count; i++) {
            
            CDHydepark *cd =[contentArray objectAtIndex:i];
            [self showOfflineComments:cd];
        }
        
    }else
        
        if ([notification.name isEqualToString:@"showOflineContents"])
        {
            coreMngr = [CoreDataManager sharedManager];
            
            NSDictionary *data = notification.object;
            
            NSMutableArray *contentArray=[NSMutableArray arrayWithArray:data[@"data"]] ;
            
            for (int i=0; i<contentArray.count; i++) {
                
                CDHydepark *cd =[contentArray objectAtIndex:i];
                [self showOfflineContent:cd];
            }
            
        }
        else if ([notification.name isEqualToString:@"upLoadOflineComments"])
        {
            coreMngr = [CoreDataManager sharedManager];
            
            NSDictionary *data = notification.object;
            coreMngr.offlineCommentsArray = [data[@"data"] mutableCopy];
            CDHydepark *cd =[coreMngr.offlineCommentsArray peek];
            NSLog(@"%@",cd.isAudio);
            
            [self setupOfflineUploadingComments:cd];
        }
        else if ([notification.name isEqualToString:@"upLoadOflineContents"])
        {
            coreMngr = [CoreDataManager sharedManager];
            
            NSDictionary *data = notification.object;
            coreMngr.offlineContentArray = [data[@"data"] mutableCopy];
            CDHydepark *cd =[coreMngr.offlineContentArray peek];
            NSLog(@"%@",cd.isAudio);
            
            [self setupOfflineUploadingContents:cd];
        }
        else if([notification.name isEqualToString:@"uploadData"]){
            
            
            coreMngr = [CoreDataManager sharedManager];
            
            NSDictionary *data = notification.object;
            //coreMngr.offlineContentArray = [data[@"data"] mutableCopy];
            ////////////////////////////////////////////insert online content into channel array locally
            userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Id"];
            userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
            
            DataContainer *shareobj = [DataContainer sharedManager];
            for (CDHydepark *cd  in coreMngr.offlineContentArray) {
                if ([cd.isAudio isEqualToString:@"1"]) {
                    VideoModel *videos     = [[VideoModel alloc] init];
                    videos.userName        = userName;
                    videos.user_id         = userId;
                    UIImage *thumbnail = [UIImage imageNamed:@"splash_audio_image"];
                    
                    profileData = UIImagePNGRepresentation(thumbnail);
                    videos.profileImageData = profileData;
                    videos.isLocal = true;
                    videos.cdHydeparkModelObj = cd;
                    [videos setIs_anonymous:@"0"];
                    [shareobj.channelVideos insertObject:videos atIndex:0];
                }
                else{
                    VideoModel *videos     = [[VideoModel alloc] init];
                    videos.userName        = userName;
                    videos.user_id         = userId;
                    videos.profileImageData   =cd.profileData;
                    videos.isLocal = true;
                    videos.cdHydeparkModelObj = cd;
                    [videos setIs_anonymous:@"0"];
                    [shareobj.channelVideos insertObject:videos atIndex:0];
                }
                
            }
            //        [[NSNotificationCenter defaultCenter]
            //         postNotificationName:@"reloadMyCorner"
            //         object:nil];
            
            if(coreMngr.offlineContentArray.count > 0){
                CDHydepark *cd =[coreMngr.offlineContentArray peek];
                [self setupOfflineUploadingContents:cd];
            }
            
            
        }
}
//////////////////////////////////////////////////////////////// Offline  contents save
-(void) saveOfflineDataIsAudio:(BOOL)isaudio{
    CDHydepark *cd;
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    // NSTimeInterval is defined as double
    NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
    timestamp = timeStampObj.stringValue;
    if (isaudio) {
        coreMngr = [CoreDataManager sharedManager];
        NSString *anny = (isCheckBoxSelected) ? @"1" : @"0";
        NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
        
        

        if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0)
            textToshare = @"";
        
        
        cd=[coreMngr AddRecordToCoreDataFile:dataToUpload isAudio:@"1" withparentCommentID:ParentCommentID CommentAllowed:commentAllowed TextToshare:textToshare Anony:anny VideoDuration:video_duration PstID:postID PrivacySelected:privacySelected profileData:profileData AndUserSession:userSession AndIsComment:@"0" AndTimeStamp:timeStampObj.stringValue];
        
        timestamp = timeStampObj.stringValue;
    }
    else{
        coreMngr = [CoreDataManager sharedManager];
        NSString *anoy = (isAnonymous) ? @"1" : @"0";
        NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
        
        if([textToshare isEqualToString:NSLocalizedString(@"somethingText", @"")] || textToshare.length == 0)
            textToshare = @"";
        cd= [coreMngr AddRecordToCoreDataFile:dataToUpload isAudio:@"0" withparentCommentID:ParentCommentID CommentAllowed:commentAllowed TextToshare:textToshare Anony:anoy VideoDuration:video_duration PstID:postID PrivacySelected:privacySelected profileData:profileData AndUserSession:userSession AndIsComment:@"0" AndTimeStamp:timeStampObj.stringValue];
        
        timestamp = timeStampObj.stringValue;
    }
    
    [self addLocallyCells:cd];
}
-(void) addLocallyCells:(CDHydepark *)cd{
    coreMngr = [CoreDataManager sharedManager];
    [coreMngr.offlineContentArray enqueue:cd];
    
    
    DataContainer *shareobj = [DataContainer sharedManager];
    userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Id"];
    NSString *user_Name = [[NSUserDefaults standardUserDefaults] stringForKey:@"userName"];
    
    VideoModel *videos     = [[VideoModel alloc] init];
    if(isAudio && !isComment){
        
        [videos setUserName:user_Name];
        [videos setUser_id: userId];
        UIImage *thumbnail = [UIImage imageNamed:@"splash_audio_image"];
        
        profileData = UIImagePNGRepresentation(thumbnail);
        [videos setProfileImageData:profileData];
        [videos setIsLocal: true];
        [videos setCdHydeparkModelObj:cd];
        [videos setIs_anonymous:@"0"];
        [videos setCurrent_datetime:[self getCurrentTime]];
        //NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
        // NSTimeInterval is defined as double
        //NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];

        [videos setTimestamp:cd.timestamp];
        [shareobj.channelVideos insertObject:videos atIndex:0];
    }
    else{
        [videos setUserName:user_Name];
        [videos setUser_id: userId];
        [videos setProfileImageData:cd.profileData];
        [videos setIsLocal: true];
        [videos setCdHydeparkModelObj: cd];
        [videos setIs_anonymous:@"0"];
        [videos setCurrent_datetime:[self getCurrentTime]];
        //NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
        // NSTimeInterval is defined as double
        //NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
        
        [videos setTimestamp:cd.timestamp];
        [shareobj.channelVideos insertObject:videos atIndex:0];
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"reloadMyCorner"
     object:nil];
    
}

-(void) addLocalyCommentsCells:(CDHydepark *)cd{
    coreMngr = [CoreDataManager sharedManager];
    [coreMngr.offlineCommentsArray enqueue:cd];
    
    
    DataContainer *shareobj = [DataContainer sharedManager];
    userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Id"];
    NSString *user_Name = [[NSUserDefaults standardUserDefaults] stringForKey:@"userName"];
    
    VideoModel *videos     = [[VideoModel alloc] init];
    if(isAudio && !isComment){
        
        [videos setUserName:user_Name];
        [videos setUser_id: userId];
        UIImage *thumbnail = [UIImage imageNamed:@"splash_audio_image"];
        
        profileData = UIImagePNGRepresentation(thumbnail);
        [videos setProfileImageData:profileData];
        [videos setIsLocal: true];
        [videos setCdHydeparkModelObj:cd];
        [videos setIs_anonymous:@"0"];
        [videos setCurrent_datetime:[self getCurrentTime]];
        //NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
        // NSTimeInterval is defined as double
        //NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
        
        [videos setTimestamp:cd.timestamp];
        
        //[shareobj.channelVideos insertObject:videos atIndex:0];
        
        if ([shareobj.commentsDict objectForKey:cd.postID]!= nil){
            
            if ([[shareobj.commentsDict objectForKey:cd.postID] isKindOfClass:[NSArray class]] || [[shareobj.commentsDict objectForKey:cd.postID] isKindOfClass:[NSMutableArray class]]){
                
                NSMutableArray *objArray = [shareobj.commentsDict objectForKey:cd.postID];
                [objArray addObject:videos];
                //[objArray insertObject:videos atIndex:0];
                [shareobj.commentsDict setObject:objArray forKey:cd.postID];
                
            }
            
        }else{
            
            NSMutableArray *objArray = [NSMutableArray arrayWithObject:videos];
            [shareobj.commentsDict setObject:objArray forKey:cd.postID];
            
        }
    }
    else{
        [videos setUserName:user_Name];
        [videos setUser_id: userId];
        [videos setProfileImageData:cd.profileData];
        [videos setIsLocal: true];
        [videos setCdHydeparkModelObj: cd];
        [videos setIs_anonymous:@"0"];
        [videos setCurrent_datetime:[self getCurrentTime]];
        //NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
        // NSTimeInterval is defined as double
        //NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
        
        [videos setTimestamp:cd.timestamp];
        //[shareobj.channelVideos insertObject:videos atIndex:0];
        
        if ([shareobj.commentsDict objectForKey:cd.postID]!= nil){
            
            if ([[shareobj.commentsDict objectForKey:cd.postID] isKindOfClass:[NSArray class]] || [[shareobj.commentsDict objectForKey:cd.postID] isKindOfClass:[NSMutableArray class]]){
                
                NSMutableArray *objArray = [shareobj.commentsDict objectForKey:cd.postID];
                [objArray addObject:videos];
                //[objArray insertObject:videos atIndex:0];
                [shareobj.commentsDict setObject:objArray forKey:cd.postID];
                
            }
            
        }else{
            
            NSMutableArray *objArray = [NSMutableArray arrayWithObject:videos];
            [shareobj.commentsDict setObject:objArray forKey:cd.postID];
            
        }
    }
    
    
    
    
    //    [[NSNotificationCenter defaultCenter]
    //     postNotificationName:@"addOfflineComment"
    //     object:nil];
    
    //reload Comments
    
}

-(void)removeIfnIfOffline{
    VideoModel *Vmodel = [sharedManager.channelVideos objectAtIndex:0];
    if(Vmodel.isLocal){
        [sharedManager.channelVideos removeObjectAtIndex:0];
    }
}
-(BOOL)isVideoAlreadyPresend:(NSString *)videoId{
    for(int i=0; i< sharedManager.channelVideos.count ;i++){
        VideoModel *Vmod = [sharedManager.channelVideos objectAtIndex:i];
        if([Vmod.videoID isEqualToString:videoId]){
            return YES;
        }
    }
    return NO;
}
-(NSString *) getCurrentTime{
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    
    return dateString;
}

-(NSString *) getCurrentTimeStamp{
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    
    return dateString;
}

-(NSDate *) getDateFromString :(NSString *)targetString
{
    NSDateFormatter *formatter;
    NSDate        *dateObj;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    dateObj = [formatter dateFromString:targetString];
    
    return dateObj;
}

-(NSString *) getCurrentTimeForOfflineDB{
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    
    return dateString;
}

#pragma mark Offline Methods
 ////////////// FOR OFFLINE MODULE /////////////////

- (void)uploadOfflineBeam {
    
    APP_DELEGATE.isUploading = YES;
    _isShowProgress++;
    shareobj = [DataContainer sharedManager];   
    _offlineUploadCount = shareobj.offlineArrayCount;
    NSArray *tempArray = [[DBCommunicator sharedManager] retrieveAllRows];
    if(tempArray.count > 0){
        OfflineDataModel *model = tempArray[0];
        commentAllowed = model.allowedReply;
        textToshare = model.caption;
        anony = model.anonyCheck;
        if([anony isEqualToString: @"1"]) {
            isAnonymous = YES;
        } else {
            isAnonymous = NO;
        }
        video_duration = model.duration;
//        if([model.isComment isEqualToString:@"1"]){
//            model.parentId = @"-1";
//        }
        postID = model.postId;
        if([model.isComment isEqualToString:@"0"] || ParentCommentID == nil)
        {
            ParentCommentID = model.parentId;
        }
        privacySelected = model.privacyCheck;
        timestamp = model.currentTime;
        NSData *videoData = [self getVideoFromDocuments:model.data];
        playBtnThumbnail = [self getThumbnailFromDocuments:model.thumbnail];
        profileData = playBtnThumbnail;
        _offlineRowID = model.rowID;
        if([model.isAudio isEqualToString:@"1"] && [model.isComment isEqualToString:@"1"]) {
            _increaseCommentCount = YES;
            [self uploadAudioComment:videoData];
        } else if([model.isAudio isEqualToString:@"0"] && [model.isComment isEqualToString:@"1"]) {
            _increaseCommentCount = YES;
            [self uploadComment:videoData];
        } else if([model.isAudio isEqualToString:@"1"]) {
            _increaseCommentCount = YES;
            [self uploadAduio:videoData];
        }
        else {
            _increaseCommentCount = YES;
            [self uploadBeam:videoData];
        }
    }
}
- (void)uploadOfflineBeamWhenInForeground {
    NSArray *tempArray = [[DBCommunicator sharedManager] retrieveAllRows];
    for (OfflineDataModel *model in tempArray) {
        playBtnThumbnail = [self getThumbnailFromDocuments:model.thumbnail];
        profileData = playBtnThumbnail;
        
        if([model.isAudio isEqualToString:@"1"] && [model.isComment isEqualToString:@"1"]) {
            
        } else if([model.isAudio isEqualToString:@"0"] && [model.isComment isEqualToString:@"1"]) {
            ;
        } else if([model.isAudio isEqualToString:@"1"]) {
            isAudio = YES;
            [self insertLocalObjectInCornerArray];
        }
        else {
            [self insertLocalObjectInCornerArray];
        }
    }
    _isFromBackground = YES;
    [self uploadOfflineBeam];
}

//////////////////////////////////////


- (IBAction)thumbnailBtn1Pressed:(id)sender {
    self.thumbnailImageView.image = thumbnailImage;
    profileData = UIImagePNGRepresentation(thumbnailImage);
}

- (IBAction)thumbnailBtn2Pressed:(id)sender {
    self.thumbnailImageView.image = self.thumbnailImage2;
    profileData = UIImagePNGRepresentation(self.thumbnailImage2);
}

- (IBAction)thumbnailBtn3Pressed:(id)sender {
    self.thumbnailImageView.image = self.thumbnailImage3;
    profileData = UIImagePNGRepresentation(self.thumbnailImage3);
}
@end
