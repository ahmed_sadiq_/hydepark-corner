//
//  NewHomeCells.h
//  HydePark
//
//  Created by Apple on 09/03/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "FLAnimatedImage.h"

@interface NewHomeCells : UITableViewCell

@property (weak, nonatomic)   IBOutlet UIView *imgContainer;
@property (strong, nonatomic) IBOutlet UIButton *CH_playVideo;
@property (strong, nonatomic) IBOutlet UIImageView *CH_profileImage;
@property (strong, nonatomic) IBOutlet UIButton *CH_heart;
@property (strong, nonatomic) IBOutlet UILabel *CH_heartCountlbl;
@property (strong, nonatomic) IBOutlet UIButton *CH_commentsBtn;
@property (strong, nonatomic) IBOutlet UILabel *CH_CommentscountLbl;
@property (strong, nonatomic) IBOutlet UILabel *CH_seen;
@property (strong, nonatomic) IBOutlet UIButton *CH_flag;
@property (strong, nonatomic) IBOutlet UILabel *CH_userName;
@property (strong, nonatomic) IBOutlet UILabel *CH_VideoTitle;
@property (strong, nonatomic) IBOutlet UILabel *CH_hashTags;
@property (strong, nonatomic) IBOutlet UIImageView *transthumb;
@property (strong, nonatomic) IBOutlet FLAnimatedImageView *CH_Video_Thumbnail;
@property (strong, nonatomic) IBOutlet UILabel *Ch_videoLength;
@property (weak, nonatomic)   IBOutlet UIButton *userProfileView;
@property (weak, nonatomic)   IBOutlet UIActivityIndicatorView *activityInd;
@property (weak, nonatomic)   IBOutlet UIImageView *leftreplImg;
@property (weak, nonatomic)   IBOutlet UIImageView *dummyLeft1;
@property (weak, nonatomic)   IBOutlet UIImageView *dummyright1;
@property (weak, nonatomic)   IBOutlet UIImageView *dummyLeft2;
@property (weak, nonatomic)   IBOutlet UIImageView *dummyright2;
@property (weak, nonatomic)   IBOutlet UIImageView *dummy1;
@property (weak, nonatomic)   IBOutlet UIImageView *dummy2;

@property (weak, nonatomic)   IBOutlet UILabel     *leftTimeStamp;
@property (weak, nonatomic)   IBOutlet UILabel     *rightTimeStamp;
@property (strong, nonatomic) IBOutlet UIButton *CH_RplayVideo;
@property (weak, nonatomic)   IBOutlet UIView *RimgContainer;
@property (strong, nonatomic) IBOutlet UIImageView *CH_RprofileImage;
@property (strong, nonatomic) IBOutlet FLAnimatedImageView *CH_RVideo_Thumbnail;
@property (strong, nonatomic) IBOutlet UIImageView *Rtransthumb;
@property (strong, nonatomic) IBOutlet UIButton *CH_Rheart;
@property (strong, nonatomic) IBOutlet UIButton *CH_RcommentsBtn;
@property (strong, nonatomic) IBOutlet UILabel *CH_Rseen;
@property (strong, nonatomic) IBOutlet UILabel *CH_RuserName;
@property (strong, nonatomic) IBOutlet UILabel *CH_RVideoTitle;
@property (strong, nonatomic) IBOutlet UILabel *CH_RheartCountlbl;
@property (strong, nonatomic) IBOutlet UILabel *CH_RCommentscountLbl;
@property (weak, nonatomic)   IBOutlet UIImageView *rightreplImg;
@property (weak, nonatomic)   IBOutlet UIImageView *playImage;
@property (weak, nonatomic)   IBOutlet UIImageView *leftClock;
@property (weak, nonatomic)   IBOutlet UIImageView *rightClock;
@property (weak, nonatomic) IBOutlet UIImageView *thumbNail1;
@property (weak, nonatomic) IBOutlet UIImageView *thumbNail2;
@property (weak, nonatomic) IBOutlet UIImageView *thumbNail3;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;

@property (weak, nonatomic) IBOutlet UIView *anonyOverlayRight;
@property (weak, nonatomic) IBOutlet UIImageView *anonyImgRight;
@property (weak, nonatomic) IBOutlet UIView *annonyOverlayLeft;
@property (weak, nonatomic) IBOutlet UIImageView *annonyImgLeft;

@property (weak, nonatomic) IBOutlet UIView *anonyOverlayExtRight;
@property (weak, nonatomic) IBOutlet UIImageView *anonyImgExtRight;
@property (weak, nonatomic) IBOutlet UIView *containerExtRight;
@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UILabel *rightUsername;
@property (weak, nonatomic) IBOutlet UILabel *rightDate;
@property (weak, nonatomic) IBOutlet FLAnimatedImageView *rightThumbnail;
@property (weak, nonatomic) IBOutlet UILabel *replyCountlbl;
@property (weak, nonatomic) IBOutlet UIImageView *replyRedBg;
@property (strong, nonatomic) IBOutlet UIButton *showCommentsBtn;

@property (weak, nonatomic) IBOutlet UIImageView *rebeam1;
@property (weak, nonatomic) IBOutlet UIImageView *rebeam2;
@property (weak, nonatomic) IBOutlet UIImageView *rebeam3;

@property (weak, nonatomic)   IBOutlet UIView *outline1;
@property (weak, nonatomic)   IBOutlet UIView *outline2;
@property (weak, nonatomic)   IBOutlet UIView *outline3;

@end
