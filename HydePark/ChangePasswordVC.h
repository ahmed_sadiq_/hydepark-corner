//
//  ChangePasswordVC.h
//  HydePark
//
//  Created by Osama on 04/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordVC : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordField;
@property (weak, nonatomic) IBOutlet UITextField *PasswordField;
@property (weak, nonatomic) IBOutlet UITextField *RepeatPasswordField;
@property (weak, nonatomic) IBOutlet UIView *baseview;
@end
