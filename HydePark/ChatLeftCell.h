//
//  TestingCell.h
//  HydePark
//
//  Created by Samreen Noor on 29/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "PWProgressView.h"
#import "FLAnimatedImage.h"

@interface ChatLeftCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (strong, nonatomic) IBOutlet PWProgressView *pView;
@property (weak, nonatomic) IBOutlet UIImageView *videoImg;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet FLAnimatedImageView *CH_Video_Thumbnail;
@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (weak, nonatomic) IBOutlet UIImageView *redBG;
@property (weak, nonatomic) IBOutlet UILabel *exclamation;

@property (strong, nonatomic) PWProgressView *cellProgress;
@end
