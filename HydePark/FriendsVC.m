//
//  FriendsVC.m
//  HydePark
//
//  Created by Apple on 04/04/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "FriendsVC.h"
#import "Constants.h"
#import "Followings.h"
#import "SearchCell.h"
#import "Utils.h"
#import "UserChannel.h"
#import "AFNetworking.h"
#import "topicsModel.h"
#import "TredingTableViewCell.h"
#import "TopicDetailsVC.h"
#import "CustomLoading.h"
#import "FriendChatVC.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import <HHHorizontalPagingView.h>
#import "NewRequestViewController.h"
#import "PendingViewController.h"
#import "FriendsSegmentViewController.h"
#import "SegmentHeaderView.h"
#import "DataContainer.h"
#import "PeopleYouMayKnowCollectionViewCell.h"
#import "PeopleYouMayKnowHeaderView.h"
#import "HashTagWithVideosCollectionViewCell.h"
#import "SearchUserModel.h"
#import "UIImageView+AFNetworking.h"
#import "CommentsVC.h"
#import "PopularUsersVC.h"
#import "NavigationHandler.h"
#import "StoriesCollectionViewCell.h"
#import "HashTagTableViewCell.h"
#import "HashTagsCollectionViewCell.h"
#import "NSData+AES.h"
#import "UIImageView+WebCache.h"
#import "SVProgressHUD.h"


@interface FriendsVC (){
    BOOL showBtn;
    BOOL ivarNoResults;
    int callNumber;
    int callProcessed;
    int selectedSectionNumber;
    BOOL isDownwards;
    NSString *lasatSearchString;
    CGRect tblFrame;
    AppDelegate *appDelegate;
    NSMutableArray *searchUsersArray;
    NSMutableArray *searchTopicsArray;
    UICollectionView *tempCollectionView;
}
@end

@implementation FriendsVC
@synthesize friendsArray,titles,NoFriends,userId,loadFollowings,isComment,searchServerCall,FollowingsArray,friendsDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    searchUsersArray = [[NSMutableArray alloc] init];
    searchTopicsArray = [[NSMutableArray alloc] init];
    resultsFinished = false;
    
    self.pageNum = 1;
    self.pageNum2 = 1;
//    [self getHashTagsContent];
//    [self searchTopics];
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(IS_IPAD){
        [self.searchDisplayController.searchBar setBackgroundImage:[UIImage imageNamed:@"bg_iPad.png"]];
    }
    sharedManager = [DataContainer sharedManager];
    if(sharedManager.topicsArray.count == 0)
    {
        // [self getTopics];
    }
    self.searchedContent = [[NSMutableArray alloc] init];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    self.searchDisplayController.searchBar.placeholder = NSLocalizedString(@"search", @"");
    [self.searchDisplayController.searchBar setValue:NSLocalizedString(@"cancel", nil) forKey:@"_cancelButtonText"];
    //    [self.searchDisplayController.searchBar.placeholder sizeWithFont:[UIFont fontWithName:@"Montserrat-Regular" size:40] constrainedToSize:CGSizeZero];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.searchDisplayController.searchResultsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.searchDisplayController.searchResultsTableView setBackgroundColor:[UIColor clearColor]];
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
    self.trendingtblView.hidden = YES;
    self.hashTagsTableView.hidden = YES;
    FollowingsArray = [[NSArray alloc] init];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.backgroundImage.hidden = YES;
            self.searchDisplayController.searchBar.backgroundImage = nil;
            self.searchDisplayController.searchBar.barTintColor = [UIColor blackColor];
            [self.searchDisplayController.searchResultsTableView setBackgroundColor:[UIColor whiteColor]];
        }
        else
        {
            [self.searchDisplayController.searchResultsTableView setBackgroundView:imgView];
        }
    }
    else
    {
        [self.searchDisplayController.searchResultsTableView setBackgroundView:imgView];
    }
    
    switch (loadFollowings) {
        case 0:
            friendsArray    = [[NSMutableArray alloc] init];
            self.follwersAndFollwings.hidden = NO;
            self.segmentView.hidden = YES;
            showBtn = YES;
            [self getFollowers];
            break;
        case 1:
            showBtn = YES;
            self.follwersAndFollwings.hidden = NO;
            self.segmentView.hidden = YES;
            friendsArray    = [[NSMutableArray alloc] init];
            [self getFollowings];
            break;
        case 2:
            showBtn = YES;
            //friendsArray    = [[NSMutableArray alloc] initWithArray:[friendsDict objectForKey:@"accept"]];
//            self.follwersAndFollwings.frame = CGRectMake(0, 70, self.follwersAndFollwings.frame.size.width, self.follwersAndFollwings.frame.size.height - 200);
//            self.searchDisplayController.searchBar.hidden = YES;
//            self.segmentView.frame = CGRectMake(0, 65, self.segmentView.frame.size.width, self.segmentView.frame.size.height);
            [self.follwersAndFollwings reloadData];
            self.hashTagsTableView.hidden = YES;
            [self showFrindsTabs];
            break;
        case 3:
            self.follwersAndFollwings.hidden = NO;
            self.follwersAndFollwings.frame = CGRectMake(0, 70, self.follwersAndFollwings.frame.size.width, self.follwersAndFollwings.frame.size.height);
            self.segmentView.hidden = YES;
            self.searchDisplayController.searchBar.hidden = YES;
            friendsArray   = [[NSMutableArray alloc] init];
            showBtn = NO;
            [self getLikesonPost];
            break;
        case 4:
            showBtn = YES;
            self.trendingtblView.hidden = YES; //NO
            self.hashTagsTableView.hidden = NO;
            self.segmentView.hidden = YES;
//            if(sharedManager.topicsArray.count >0){
                [self getHashTagsContent];
                [self.trendingtblView reloadData];
//            }else{
//                [self getTopics];
//            }
            friendsArray   = [[NSMutableArray alloc] init];
            break;
        case 5:
            self.follwersAndFollwings.hidden = NO;
            self.follwersAndFollwings.frame = CGRectMake(0, 120, self.follwersAndFollwings.frame.size.width, self.follwersAndFollwings.frame.size.height);
            self.segmentView.hidden = YES;
            [self getFollowing];
            showBtn = YES;
            break;
        case 6:
            self.follwersAndFollwings.hidden = NO;
            self.segmentView.hidden = YES;
            self.follwersAndFollwings.frame = CGRectMake(0, 120, self.follwersAndFollwings.frame.size.width, self.follwersAndFollwings.frame.size.height);
            [self getFollowing];
            showBtn = YES;
            break;
        case 7:
            self.follwersAndFollwings.hidden = NO;
            self.segmentView.hidden = YES;
            showBtn = YES;
            friendsArray   = [[NSMutableArray alloc] init];
            if(appDelegate.hasInet) {
                _searchIndicator.hidden = NO;
                [_searchIndicator startAnimating];
            }
            [self getBlockedUsers];
            break;
        default:
            break;
    }
    callNumber = 1;
    callProcessed = 0;
    
    
    
    //Collection View People you may know Starts
//    [self.collectionViewPeopleYouMayKnow registerNib:[UINib nibWithNibName:@"PeopleYouMayKnowCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"peopleYouMayKnowCell"];
//    [self.collectionViewPeopleYouMayKnow registerClass:[PeopleYouMayKnowHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"peopleYouMayKnowHeaderView"];
//    //[self.collectionViewPeopleYouMayKnow reloadData];
//    _lblPeopleYouMayKnow.layer.masksToBounds = YES;
//    _lblPeopleYouMayKnow.layer.cornerRadius = 5.0;
    //Collection View People you may know Ends
    
    
    
    //Collection View Hash Tags Start
//    [self.collectionViewHashTags registerNib:[UINib nibWithNibName:@"HashTagWithVideosCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"hashTagsVideoCell"];
//    [self.collectionViewHashTags registerNib:[UINib nibWithNibName:@"PeopleYouMayKnowHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"peopleYouMayKnowHeaderView"];
//
//
//    [self.collectionViewHashTags registerNib:[UINib nibWithNibName:@"StoriesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"storiescell"];
//
//    [self.collectionViewHashTags registerNib:[UINib nibWithNibName:@"StoriesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"storiescell"];
//
    
    // [self.collectionViewHashTags registerClass:[StoriesCollectionViewCell class] forCellWithReuseIdentifier:@"storiescell"];
    
    //[self.collectionViewHashTags reloadData];
    //Collection View Hash Tags Ends
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void) getFollowing{
    [friendsArray removeAllObjects];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *userId = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"id"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_FOLLOWING_AND_FOLLOWERS,@"method",
                              token,@"session_token",@"1",@"page_no",userId,@"user_id",@"1",@"following",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1)
            {
                NSArray *friends = [result objectForKey:@"following"];
                if([friends isKindOfClass:[NSArray class]]){
                    for(NSDictionary *tempDict in friends){
                        Followings *_responseData = [[Followings alloc] init];
                        _responseData.f_id = [tempDict objectForKey:@"id"];
                        _responseData.fullName = [Utils getDecryptedTextFor:[tempDict objectForKey:@"full_name"]];
                        _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                        _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                        _responseData.status = [tempDict objectForKey:@"state"];
                        _responseData.isFollowed = [[tempDict objectForKey:@"is_followed"] intValue];
                        [friendsArray addObject:_responseData];
                    }
                }
                [_follwersAndFollwings reloadData];
            }
        }
    }];
}

- (void)showFrindsTabs {
    ///////
    UIView *sliderview1 = [[UIView alloc] initWithFrame:CGRectMake(0, 164, [[UIScreen mainScreen] bounds].size.width/3, 2)];
    sliderview1.backgroundColor = [UIColor whiteColor];
    
    UIView *sliderview2 = [[UIView alloc] initWithFrame:CGRectMake(sliderview1.frame.size.width, 164, [[UIScreen mainScreen] bounds].size.width/3, 2)];
    sliderview2.backgroundColor = [UIColor whiteColor];
    
    UIView *sliderview3 = [[UIView alloc] initWithFrame:CGRectMake(sliderview2.frame.size.width + sliderview2.frame.origin.x, 164, [[UIScreen mainScreen] bounds].size.width/3, 2)];
    sliderview3.backgroundColor = [UIColor whiteColor];
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 108, [[UIScreen mainScreen] bounds].size.width, 60)];
    background.image = [UIImage imageNamed:@"bg.png"];
    
    
    SegmentHeaderView *headerView                = [SegmentHeaderView headerView];
    NewRequestViewController *tableView1           = [NewRequestViewController contentTableView];
    
    [[NSNotificationCenter defaultCenter] removeObserver:tableView1 name:LOAD_NEW_REQUESTS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:tableView1 name:@"refreshRequests" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:tableView1 selector:@selector(loadNewRequests) name:LOAD_NEW_REQUESTS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:tableView1 selector:@selector(refreshRequests) name:@"refreshRequests" object:nil];

    tableView1.loadFollowings = loadFollowings;
    tableView1.vc = self;
    PendingViewController *tableView2           = [PendingViewController contentTableView];
    
    [[NSNotificationCenter defaultCenter] removeObserver:tableView2 name:LOAD_SENT_REQUESTS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:tableView2 name:@"refreshFriendList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:tableView2 selector:@selector(loadSentRequests) name:LOAD_SENT_REQUESTS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:tableView2 selector:@selector(refreshRequests) name:@"refreshFriendList" object:nil];

    
    tableView2.loadFollowings = loadFollowings;
    tableView2.vc = self;
    FriendsSegmentViewController *tableView3          = [FriendsSegmentViewController contentTableView];
    
    [[NSNotificationCenter defaultCenter] removeObserver:tableView3 name:LOAD_FRIENDS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:tableView3 name:@"refreshFriendList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:tableView3 selector:@selector(loadFriends) name:LOAD_FRIENDS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:tableView3 selector:@selector(refreshRequests) name:@"refreshFriendList" object:nil];

    
    tableView3.loadFollowings = loadFollowings;
    tableView3.vc = self;
    
    
    t1 = tableView1;
    t2 = tableView2;
    t3 = tableView3;
    
    DataContainer *sharedInstance = [DataContainer sharedManager];
    sharedInstance.friendsVcArray = friendsArray;
    NSMutableArray *buttonArray = [NSMutableArray array];
    for(int i = 0; i < 3; i++) {
        UIButton *segmentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        segmentButton.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.5];
        [segmentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
            {
                segmentButton.backgroundColor = [UIColor whiteColor];
                [segmentButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
            }
            else
            {
            }
        }
        else
        {
        }

        //        [segmentButton setTitle:@"Selected"  forState:UIControlStateSelected];
        if(i == 0) {
            [segmentButton setTitle:NSLocalizedString(@"new_request", nil) forState:UIControlStateNormal];
            //  [segmentButton setAttributedTitle: titleString forState:UIControlStateNormal];
        } else if(i == 1) {
            [segmentButton setTitle:NSLocalizedString(@"sent", nil) forState:UIControlStateNormal];
        } else if(i == 2) {
            [segmentButton setTitle:NSLocalizedString(@"friends", nil) forState:UIControlStateNormal];
        }
        
        [segmentButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        segmentButton.titleLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:14];
        segmentButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [buttonArray addObject:segmentButton];
    }
    HHHorizontalPagingView *pagingView = [HHHorizontalPagingView pagingViewWithHeaderView:nil headerHeight:0.f segmentButtons:buttonArray segmentHeight:50 contentViews:@[tableView1, tableView2, tableView3]];
    [self.segmentView addSubview:pagingView];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:t1 name:LOAD_NEW_REQUESTS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:t1 name:@"refreshRequests" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:t2 name:LOAD_SENT_REQUESTS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:t2 name:@"refreshFriendList" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:t3 name:LOAD_FRIENDS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:t3 name:@"refreshFriendList" object:nil];

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //    if(loadFollowings == 4){
    //        [self.searchDisplayController.searchBar becomeFirstResponder];
    //    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    titleLabel.text = titles;

    [self.navigationController setNavigationBarHidden:YES animated:YES];
    isDownwards  = YES;
    if([self isSearchDisplayControllerActice]){
        [self.searchDisplayController.searchResultsTableView reloadData];
    }else{
        [self.follwersAndFollwings reloadData];
    }
}

-(void)getBlockedUsers{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"getBlockedUsers",@"method",
                              token,@"Session_token", nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        if(success == 1){
            _searchIndicator.hidden = YES;
            [_searchIndicator stopAnimating];
            FollowingsArray = [responseObject objectForKey:@"blocked_users"];
            
            for(NSDictionary *tempDict in FollowingsArray){
                Followings *_responseData = [[Followings alloc] init];
                
                _responseData.f_id = [tempDict objectForKey:@"id"];
                _responseData.fullName = [tempDict objectForKey:@"full_name"];
                _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                _responseData.status = [tempDict objectForKey:@"state"];
                [friendsArray addObject:_responseData];
            }
            if(FollowingsArray.count == 0){
                _searchIndicator.hidden = YES;
                [_searchIndicator stopAnimating];
                self.noBlockedUserslbl.text = NSLocalizedString(@"not_blocked_any_users_msg", @"");
                self.noBlockedUserslbl.hidden = NO;
            }
            [_follwersAndFollwings reloadData];
        }
    }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        _searchIndicator.hidden = YES;
        [_searchIndicator stopAnimating];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

-(void)getTopics{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *lang = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_TOPICS,@"method",
                              token,@"Session_token",@"",@"post_id",lang,@"language", nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        if(success){
            int success = [[responseObject objectForKey:@"success"] intValue];
            NSArray *topicsArr = [[NSArray alloc] init];
            topicsArr = [responseObject objectForKey:@"topics"];
            sharedManager.topicsArray = [NSMutableArray new];
            if(success == 1 && topicsArr.count > 0) {
                for(int i=0; i < topicsArr.count ; i++){
                    NSDictionary *topicDict = (NSDictionary *)[topicsArr objectAtIndex:i];
                    topicsModel *tObj = [[topicsModel alloc] initWithDictionary:topicDict];
                    [sharedManager.topicsArray addObject:tObj];
                }
                [self.trendingtblView reloadData];
            }
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

-(void)searchTopics{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *lang = [Utils getLanguageCode];
    NSString *textToSearch =  self.searchDisplayController.searchBar.text;
    NSString *srchText = [textToSearch stringByReplacingOccurrencesOfString:@"#" withString:@""];
//    NSString *pageNum = [NSString stringWithFormat:@"%d",1];
    NSString *pageNum = [NSString stringWithFormat:@"%d",self.pageNum];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SEARCH_TOPICS,@"method",
                              token,@"Session_token",pageNum,@"page_no",srchText,@"search_key", nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        if(success){
            if(self.pageNum == 1)
            {
                [self.searchedContent removeAllObjects];
            }
            int success = [[responseObject objectForKey:@"success"] intValue];
            NSArray *topicsArr = [[NSArray alloc] init];
            topicsArr = [responseObject objectForKey:@"topics"];
            sharedManager.topicsArray = [NSMutableArray new];
            if(success == 1 && topicsArr.count > 0) {
                for(int i=0; i < topicsArr.count ; i++){
                    NSDictionary *topicDict = (NSDictionary *)[topicsArr objectAtIndex:i];
                    topicsModel *tObj = [[topicsModel alloc] initWithDictionary:topicDict];
                    [sharedManager.topicsArray addObject:tObj];
                }
                [self.trendingtblView reloadData];
                searchTopicsArray = sharedManager.topicsArray;
                self.searchedContent = searchTopicsArray;
                [self.searchDisplayController.searchResultsTableView reloadData];

            }
            else
            {
                [self.searchedContent removeAllObjects];
                [self.searchDisplayController.searchResultsTableView reloadData];
            }
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if(loadFollowings != 5 && loadFollowings != 6)
    {
        _hashTagsTableView.hidden = YES;
    }
    else if(loadFollowings != 2)
    {
        _hashTagsTableView.hidden = NO;
    }
    else
    {
        _hashTagsTableView.hidden = YES;
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchBar.text isEqualToString:@""])
    {
        if(loadFollowings != 5 && loadFollowings != 6)
        {
            _hashTagsTableView.hidden = NO;
        }
    }
}
-(void)getLikesonPost{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString * booleanString = (isComment) ? @"1" : @"0";
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"getPostLikeUsers",@"method",
                              token,@"session_token",booleanString,@"is_comment",userId,@"post_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            //            [_searchIndicator stopAnimating];
            //            _searchIndicator.hidden = YES;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1){
                FollowingsArray = [result objectForKey:@"post_like_users"];
                
                for(NSDictionary *tempDict in FollowingsArray){
                    Followings *_responseData = [[Followings alloc] init];
                    
                    _responseData.f_id = [tempDict objectForKey:@"user_id"];
                    _responseData.fullName = [tempDict objectForKey:@"full_name"];
                    _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                    _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                    _responseData.status = [tempDict objectForKey:@"state"];
                    [friendsArray addObject:_responseData];
                }
                [_follwersAndFollwings reloadData];
            }
        }
        else {
            //            [_searchIndicator stopAnimating];
            //            _searchIndicator.hidden = YES;
        }
    }];
    
}
#pragma mark TableView Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 22){
        return 80;
    }else if (tableView.tag==100){
        return 100;
    }
    else{
        if (IS_IPAD)
            return 93.0f;
        else
            return 83.0f;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==100){
            return searchTopicsArray.count;
    }else{
        return 1;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView.tag == 22){
        // _follwersAndFollwings.hidden = NO;
        return sharedManager.topicsArray.count;
    }else if (tableView.tag == 100){
        return 1;
    }
    else{
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            //return [self.searchedContent count];
            if (self.searchedContent.count == 0 && searchServerCall) {
                ivarNoResults = YES;
                return 1;
            } else {
                ivarNoResults = NO;
                return [self.searchedContent count];
            }
        }else{
            return  friendsArray.count;
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat yOffset = tableView.contentOffset.y;
    CGFloat height = tableView.contentSize.height - tableView.frame.size.height;
    CGFloat scrolledPercentage = yOffset / height;
    if(loadFollowings == 4){
        if(scrolledPercentage > .6f  && !self.searchServerCall &&!cannotScrollSearch && tableView.tag != 22 && isDownwards) {
            self.pageNum++;
            [self searchCornersNew];
        }
    }
    
    
        if(tableView.tag == 100) {
            if(indexPath.section == searchTopicsArray.count - 1 && !resultsFinished) {
                self.pageNum++;
                [self getHashTagsContent];
            }
        }
}
-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                    withVelocity:(CGPoint)velocity
             targetContentOffset:(inout CGPoint *)targetContentOffset{
    if(scrollView.tag != 22 ){
        if (velocity.y > 0){
            isDownwards = YES;
        }
        if (velocity.y < 0){
            isDownwards = NO;
            
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //put your values, this is part of my code
    if (tableView.tag == 100){
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30.0f)];
    [view setBackgroundColor:[UIColor clearColor]];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(8, 4, 0, 20)];
    [lbl setFont:[UIFont systemFontOfSize:18]];
    [lbl setTextColor:[UIColor whiteColor]];
    [lbl setBackgroundColor:[UIColor darkGrayColor]];
    if (section==0){
        lbl.text = @" People you may know ";
    }else{
         topicsModel *tObj = searchTopicsArray[section-1];
         lbl.text = tObj.topic_name;
        NSString *name=[NSString stringWithFormat:@"#%@ ",tObj.topic_name];
        lbl.text = name;
    }
    lbl.layer.masksToBounds = YES;
    lbl.layer.cornerRadius = 5.0;
    float width = lbl.intrinsicContentSize.width;
    CGRect labelFrame = [lbl frame];
    labelFrame.size.width = width;
    [lbl setFrame:labelFrame];
    [view addSubview:lbl];
    return view;
    }else{
        tableView.sectionHeaderHeight = 0;
        
        return  nil;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (tableView.tag == 100){
        static NSString *CellIdentifier = @"HashTagTableViewCell";
        HashTagTableViewCell *cell;
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects;
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"HashTagTableViewCell" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
        }else{
            cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        }
        

        cell.hashTagCollectionView.delegate = self;
        cell.hashTagCollectionView.dataSource = self;
        [cell.hashTagCollectionView registerNib:[UINib nibWithNibName:@"HashTagsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"HashTagsCollectionViewCell"];
        cell.hashTagCollectionView.tag = indexPath.section;
        [cell.hashTagCollectionView reloadData];
        return cell;
    }
    
    
    
    
    
    
    if(tableView.tag == 22){
        static NSString *CellIdentifier = @"TrendingCell";
        TredingTableViewCell *cell;
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects;
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TredingTableViewCell" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
        }else{
            cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        }
        topicsModel *tObj = [sharedManager.topicsArray objectAtIndex:indexPath.row];
        if(tObj.topic_name.length > 0){
            cell.heading.text = tObj.topic_name;
        }
        else{
            tObj.topic_name = @"#Random";
            cell.heading.text = @"#Random";
        }
        [cell.btnPressed addTarget:self action:@selector(trendingPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnPressed.tag = indexPath.row;
        if([tObj.beams_count isEqualToString:@"1"])
        {
            cell.desc.text = [NSString stringWithFormat:@"%@ %@",tObj.beams_count,NSLocalizedString(@"beam_small", nil)];
        }
        else
        {
            cell.desc.text = [NSString stringWithFormat:@"%@ %@",tObj.beams_count,NSLocalizedString(@"beams", nil)];
        }
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        
//        if (lasatSearchString.length >0)
        {
            NSString *firstLetter = @"";
            if(lasatSearchString.length >0)
            {
                firstLetter = [lasatSearchString substringToIndex:1];
            }
            if ([firstLetter  isEqual: @"#"]){
                NSLog(@"searching with hash tag");
                static NSString *CellIdentifier = @"TrendingCell";
                TredingTableViewCell *cell;
                if (cell == nil) {
                    // Load the top-level objects from the custom cell XIB.
                    NSArray *topLevelObjects;
                    topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TredingTableViewCell" owner:self options:nil];
                    // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
                    cell = [topLevelObjects objectAtIndex:0];
                }else{
                    cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                }
                topicsModel *tObj = [_searchedContent objectAtIndex:indexPath.row];
                if(tObj.topic_name.length > 0){
                    cell.heading.text = [NSString stringWithFormat:@"#%@",tObj.topic_name];
                }
                else{
                    tObj.topic_name = @"#Random";
                    cell.heading.text = @"#Random";
                }
                [cell.btnPressed addTarget:self action:@selector(trendingPressed:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnPressed.tag = indexPath.row;
                if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
                {
                    if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
                    {
                        cell.desc.textColor = cell.heading.textColor;
                    }
                    else
                    {
                        cell.desc.textColor = [UIColor whiteColor];
                    }
                }
                else
                {
                    cell.desc.textColor = [UIColor whiteColor];
                }
//                cell.desc.text = [NSString stringWithFormat:@"%@ %@",tObj.beams_count,NSLocalizedString(@"beams", nil)];
                if([tObj.beams_count isEqualToString:@"1"])
                {
                    cell.desc.text = [NSString stringWithFormat:@"%@ %@",tObj.beams_count,NSLocalizedString(@"beam_small", nil)];
                }
                else
                {
                    cell.desc.text = [NSString stringWithFormat:@"%@ %@",tObj.beams_count,NSLocalizedString(@"beams", nil)];
                }
                
                [cell setBackgroundColor:[UIColor clearColor]];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
                
                
            }else{
                
                static NSString *CellIdentifier = @"UserCell";
                SearchCell *cell;
                if (cell == nil) {
                    // Load the top-level objects from the custom cell XIB.
                    NSArray *topLevelObjects;
                    if(IS_IPAD){
                        topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SearchCell_iPad" owner:self options:nil];
                    }else{
                        topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SearchCell" owner:self options:nil];
                    }
                    // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
                    cell = [topLevelObjects objectAtIndex:0];
                    //            cell.leftreplImg.hidden =  NO;
                    //            cell.rightreplImg.hidden = NO;
                }else{
                    cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                }
                
                if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
                {
                    if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
                    {
                        cell.lowerSeparator.hidden = NO;
                    }
                    else
                    {
                        cell.lowerSeparator.hidden = YES;
                    }
                }
                else
                {
                    cell.lowerSeparator.hidden = YES;
                }
                
                Followings *tempUsers = [[Followings alloc]init];
                if(tableView == self.searchDisplayController.searchResultsTableView && ivarNoResults){
                    static NSString *cleanCellIdent = @"cleanCell";
                    UITableViewCell *ccell = [tableView dequeueReusableCellWithIdentifier:cleanCellIdent];
                    if (ccell == nil) {
                        ccell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cleanCellIdent];
                        ccell.userInteractionEnabled = NO;
                        
                    }
                    [ccell setBackgroundColor:[UIColor clearColor]];
                    ccell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return ccell;
                }
                else if (tableView == self.searchDisplayController.searchResultsTableView && !ivarNoResults) {
                    if(self.searchedContent.count > 0){
                        tempUsers = [self.searchedContent objectAtIndex:indexPath.row];
                    }else{
                        NSLog(@"search content is zero");
                        
                    }
                } else {
                    tempUsers = [friendsArray objectAtIndex:indexPath.row];
                }
                // tempUsers = [friendsArray objectAtIndex:indexPath.row];
                
//                NSString *decodedString = [Utils getDecryptedTextFor:tempUsers.fullName];//tempUsers.fullName;
                NSString *decodedString = tempUsers.fullName;
                if(!_isSearch)
                {
                    decodedString = [Utils getDecryptedTextFor:tempUsers.fullName];
                }
                
                if(![decodedString isEqualToString:@""])
                {
                    cell.friendsName.text = decodedString;
                }
                else
                {
                    cell.friendsName.text = tempUsers.fullName;
                }
                if([[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] == tempUsers.f_id ){
                    cell.statusImage.hidden = YES;
                }
                else if(!showBtn){
                    cell.statusImage.hidden = YES;
                }
                else{
                    cell.statusImage.hidden = NO;
                }
                cell.profilePic.imageURL = [NSURL URLWithString:tempUsers.profile_link];
                NSURL *url = [NSURL URLWithString:tempUsers.profile_link];
                [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
                
                cell.profilePic.layer.cornerRadius = cell.profilePic.frame.size.width / 2;
                for (UIView* subview in cell.profilePic.subviews)
                    subview.layer.cornerRadius = cell.profilePic.frame.size.width / 2;
                
                cell.profilePic.layer.shadowColor = [UIColor blackColor].CGColor;
                cell.profilePic.layer.shadowOpacity = 0.7f;
                cell.profilePic.layer.shadowOffset = CGSizeMake(0, 5);
                // cell.profilePic.layer.shadowRadius = 5.0f;
                cell.profilePic.layer.masksToBounds = NO;
                
                cell.profilePic.layer.cornerRadius = cell.profilePic.frame.size.width / 2;
                cell.profilePic.layer.masksToBounds = NO;
                cell.profilePic.clipsToBounds = YES;
                
                cell.profilePic.layer.backgroundColor = [UIColor clearColor].CGColor;
                cell.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
                cell.profilePic.layer.borderWidth = 0.0f;
                
                //cell.statusImage.hidden = false;
                cell.activityInd.hidden = true;
                [cell.activityInd stopAnimating];
                [cell.statusImage addTarget:self action:@selector(statusPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.statusImage setTag:indexPath.row];
                [cell.rejectBtn addTarget:self action:@selector(rejectRequest:) forControlEvents:UIControlEventTouchUpInside];
                [cell.rejectBtn setTag:indexPath.row];
                [cell.friendsChannelBtn addTarget:self action:@selector(OpenFriendsChannelPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.friendsChannelBtn setTag:indexPath.row];
                cell.rejectBtn.hidden = YES;
                [cell.statusImage setTitle:@"" forState:UIControlStateNormal];
                if(loadFollowings == 5){
                    [cell.statusImage removeTarget:nil
                                            action:NULL
                                  forControlEvents:UIControlEventAllEvents];
                    [cell.statusImage addTarget:self action:@selector(sharePressed:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.statusImage setTitle:NSLocalizedString(@"inbox", @"") forState:UIControlStateNormal];
                }
                else if(loadFollowings == 6){
                    [cell.statusImage removeTarget:nil
                                            action:NULL
                                  forControlEvents:UIControlEventAllEvents];
                    [cell.statusImage addTarget:self action:@selector(postPressed:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.statusImage setTitle:NSLocalizedString(@"post", @"") forState:UIControlStateNormal];
                }
                else if(loadFollowings == 7){
                    [cell.statusImage removeTarget:nil
                                            action:NULL
                                  forControlEvents:UIControlEventAllEvents];
                    [cell.statusImage addTarget:self action:@selector(unblockUser:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.statusImage setTitle:NSLocalizedString(@"unblock", @"") forState:UIControlStateNormal];
                }
                else{
                    if ([tempUsers.status isEqualToString:@"ADD_FRIEND"] && [tempUsers.is_celeb isEqualToString:@"1"]) {
                        [cell.statusImage setTitle:NSLocalizedString(@"follow", @"") forState:UIControlStateNormal];
                    }
                    else if ([tempUsers.status isEqualToString:@"FRIEND"] && [tempUsers.is_celeb isEqualToString:@"1"]){
                        [cell.statusImage setTitle:NSLocalizedString(@"following", @"") forState:UIControlStateNormal];
                    }
                    else if ([tempUsers.status isEqualToString:@"FRIEND"] && [tempUsers.is_celeb isEqualToString:@"0"]){
                        [cell.statusImage setTitle:NSLocalizedString(@"friends", @"") forState:UIControlStateNormal];
                    }
                    else if([tempUsers.status isEqualToString:@"ADD_FRIEND"] && [tempUsers.is_celeb isEqualToString:@"0"]){
                        [cell.statusImage setTitle:NSLocalizedString(@"add_friend", @"") forState:UIControlStateNormal];
                    }
                    else if([tempUsers.status isEqualToString:@"PENDING"]){
                        [cell.statusImage setTitle:NSLocalizedString(@"pending", @"") forState:UIControlStateNormal];
                    }
                    else if([tempUsers.status isEqualToString:@"ACCEPT_REQUEST"]){
                        [cell.statusImage setTitle:NSLocalizedString(@"Respond", @"") forState:UIControlStateNormal];
                        //cell.statusImage.frame = CGRectMake(cell.statusImage.frame.origin.x, cell.statusImage.frame.origin.y - 20, cell.statusImage.frame.size.width, cell.statusImage.frame.size.height);
                        cell.rejectBtn.hidden = YES;
                    }
                }
                
                [cell setBackgroundColor:[UIColor clearColor]];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
                
                
            }
            
            
        }
        
        //This peace of code will never be executed
        static NSString *CellIdentifier = @"TrendingCell";
        TredingTableViewCell *cell;
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects;
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TredingTableViewCell" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
        }else{
            cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        }
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        //This peace of code will never be executed
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}
- (void)trendingPressed:(UIButton *)sender{
    UIButton *tbtn = (UIButton *)sender;
    NSInteger buttonTag = tbtn.tag;
    topicsModel *tObj = [_searchedContent objectAtIndex:buttonTag];
    TopicDetailsVC *tDetails    = [[TopicDetailsVC alloc] initWithNibName:@"TopicDetailsVC" bundle:nil];
    tDetails.tModel = tObj;
    tDetails.tittle = tObj.topic_name;
    [[self navigationController] pushViewController:tDetails animated:YES];
    
}

#pragma mark Share With Friend
-(void)sharePressed:(UIButton *)sender{   UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    Followings *PopUser = [[Followings alloc] init];
    if ([self isSearchDisplayControllerActice]) {
        PopUser = [self.searchedContent objectAtIndex:currentSelectedIndex];
    }else{
        PopUser  = [friendsArray objectAtIndex:currentSelectedIndex];
    }
    if(_isFromUploadBeam) {
        [self shareBeamWithFriendCall:PopUser];
    } else {
        [self shareWithFriendCall:PopUser];
    }
}

-(void)postPressed:(UIButton *)sender{
    UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    Followings *PopUser = [[Followings alloc] init];
    if ([self isSearchDisplayControllerActice]) {
        PopUser = [self.searchedContent objectAtIndex:currentSelectedIndex];
    }else{
        PopUser  = [friendsArray objectAtIndex:currentSelectedIndex];
    }
    [self postOnFriendsCorner:PopUser.f_id];
}
-(void)unblockUser:(UIButton *)sender{
    UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    Followings *PopUser = [[Followings alloc] init];
    if ([self isSearchDisplayControllerActice]) {
        PopUser = [self.searchedContent objectAtIndex:currentSelectedIndex];
    }else{
        PopUser  = [friendsArray objectAtIndex:currentSelectedIndex];
    }
    [self unblockUserCall:PopUser.f_id];
}
-(void)postOnFriendsCorner:(NSString *)fId{
    [Utils shareBeamOnCorner:self.postId isComment:self.isCommentVideo friendId:fId sharingOnFriendsCorner:YES];
}

-(void)shareWithFriendCall:(Followings *)PopUser{
    [CustomLoading showAlertMessage];
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SAHREBEAM_WITH_FRIEND,@"method",
                              token,@"Session_token",self.postId,@"post_id",self.isCommentVideo,@"is_comment",PopUser.f_id,@"friend_id",nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [CustomLoading DismissAlertMessage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        
        if(success){
            FriendChatVC *frndvc = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
            frndvc.isFriend = YES;
            frndvc.seletedFollowing = PopUser;
            frndvc.isLocalSearch = YES;
            //            frndvc.isUpload = YES;
            [[self navigationController] pushViewController:frndvc animated:YES];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        [CustomLoading DismissAlertMessage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

-(void)shareBeamWithFriendCall:(Followings *)PopUser{
    FriendChatVC *frndvc = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    frndvc.isFriend = YES;
    frndvc.seletedFollowing = PopUser;
    frndvc.isLocalSearch = YES;
    
    frndvc.frndId = PopUser.f_id;
    frndvc.movieData = _beamData;
    frndvc.profileData = _thumbnailData;
    if(_isAudio) {
        frndvc.isUploadAudio = YES;
        frndvc.secondsConsumed = _videoDuration;
    } else {
        frndvc.isUpload = YES;
        frndvc.video_duration = _videoDuration;
    }
    
    
    [[self navigationController] pushViewController:frndvc animated:YES];
}

-(void)unblockUserCall:(NSString *)fId{
    [CustomLoading showAlertMessage];
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"unblockUser",@"method",
                              token,@"Session_token",fId,@"blocked_user_id",nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [CustomLoading DismissAlertMessage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        if(success){
            if ([self isSearchDisplayControllerActice]) {
                [self.searchedContent removeObjectsInArray:[self.searchedContent filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"f_id == %@",fId]]];
            }else{
                [friendsArray removeObjectsInArray:[friendsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"f_id == %@",fId]]];
            }
            [self.follwersAndFollwings reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        [CustomLoading DismissAlertMessage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
    
}

-(void)statusPressed:(UIButton *)sender{
    
    
    UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    Followings *PopUser = [[Followings alloc] init];
    if ([self isSearchDisplayControllerActice]) {
        PopUser = [self.searchedContent objectAtIndex:currentSelectedIndex];
    }else{
        PopUser  = [friendsArray objectAtIndex:currentSelectedIndex];
    }
    friendId = PopUser.f_id;
    //    [statusBtn setBackgroundImage:[UIImage imageNamed:@"follow.png"] forState:UIControlStateNormal];
    if ([PopUser.status isEqualToString:@"ADD_FRIEND"]) {
        if([PopUser.is_celeb isEqualToString:@"0"]){
            [self presentActionSheet:NSLocalizedString(@"add_friend", @"") status:1 fIndex:statusBtn.tag];
        }
        else{
            [self presentActionSheet:NSLocalizedString(@"follow", @"") status:1 fIndex:statusBtn.tag];
        }
    }
    else if([PopUser.status isEqualToString:@"FRIEND"]){
        if([PopUser.is_celeb isEqualToString:@"0"]){
            [self presentActionSheet:NSLocalizedString(@"Unfriend", @"") status:4 fIndex:statusBtn.tag];
        }
        else{
            [self presentActionSheet:NSLocalizedString(@"unfollow", @"") status:4 fIndex:statusBtn.tag];
        }
    }
    else if([PopUser.status isEqualToString:@"ACCEPT_REQUEST"]){
        [self presentActionSheet:NSLocalizedString(@"accept", @"") status:3 fIndex:statusBtn.tag];
    }
    else if([PopUser.status isEqualToString:@"PENDING"]){
        
        [self presentActionSheet:NSLocalizedString(@"delete_request", @"") status:2 fIndex:statusBtn.tag];
        
    }
}
#pragma mark Action sheet
-(void)reloadselection:(NSInteger)tagToUpdate{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tagToUpdate inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    if([self isSearchDisplayControllerActice]){
        [self.searchDisplayController.searchResultsTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    }
    else{
        [self.follwersAndFollwings reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    }
}
//status 1 = add friend status 2 = pending status 3 =  accept req status 4 = friend
-(void)presentActionSheet:(NSString *)tittleForBtn status:(int)status fIndex:(NSInteger)fIndex{
    
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    Followings *PopUser ;
    Followings *UserCopied;
    if ([self isSearchDisplayControllerActice]) {
        PopUser = [self.searchedContent objectAtIndex:fIndex];
        NSInteger index = [friendsArray indexOfObject:PopUser];
        if (index != NSNotFound) {
            UserCopied = [friendsArray objectAtIndex:index];
            
        }
    }else{
        PopUser  = [friendsArray objectAtIndex:fIndex];
    }
    friendId = PopUser.f_id;
    UIAlertAction* accept = [UIAlertAction
                             actionWithTitle:tittleForBtn
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if(status == 1){
                                     if([PopUser.is_celeb isEqualToString:@"0"]){
                                         PopUser.status = @"PENDING";
                                         
                                     }
                                     else{
                                         PopUser.status = @"FRIEND";
                                     }
                                     [self reloadselection:fIndex];
                                     [self sendFriendRequest:PopUser];
                                     
                                 }else if(status == 2){
                                     PopUser.status = @"ADD_FRIEND";
                                     [self reloadselection:fIndex];
                                     [self rejectRequest:fIndex];
                                     
                                 }else if(status == 3){
                                     PopUser.status = @"FRIEND";
                                     [self reloadselection:fIndex];
                                     [self acceptRequest];
                                     
                                 }else if(status == 4){
                                     PopUser.status = @"ADD_FRIEND";
                                     [self reloadselection:fIndex];
                                     [self sendCancelRequest];
                                 }
//                                 if(UserCopied){
//                                     UserCopied.status = PopUser.status;
//                                     NSIndexPath *indexPath = [NSIndexPath indexPathForRow:fIndex inSection:0];
//                                     NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
//                                     [self.follwersAndFollwings reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
//                                 }
                                 [self.follwersAndFollwings reloadData];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* reject = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"reject", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 PopUser.status = @"ADD_FRIEND";
                                 [self reloadselection:fIndex];
                                 [self rejectRequest:fIndex];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* cancel  = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"cancel", @"")
                              style:UIAlertActionStyleCancel
                              handler:^(UIAlertAction * action)
                              {
                                  [view dismissViewControllerAnimated:YES completion:nil];
                              }];
    
    [view addAction:accept];
    if(status == 3)
        [view addAction:reject];
    [view addAction:cancel];
    if(IS_IPAD){
        //        UITapGestureRecognizer *oneFingerTwoTaps = [[UITapGestureRecognizer alloc]init];
        //        oneFingerTwoTaps.delegate = self;
        if(_btnOriginY < 155)
        {
            _btnOriginY = 155;
        }
        view.popoverPresentationController.sourceView = self.view;
        if(fIndex == 0){
            _btnOriginY = 50;
        }
        else
            _btnOriginY = 142*fIndex;
        NSLog(@"btn %f",_btnOriginY);
        view.popoverPresentationController.sourceRect = CGRectMake(600, _btnOriginY , 200, 200);
    }
    [self presentViewController:view animated:YES completion:nil];
}



- (void)getFollowings{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"getFollowersFollowing",@"method",token,@"session_token",@"1",@"page_no",userId,@"user_id",@"1",@"following",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            // [_searchIndicator stopAnimating];
            //            [_searchIndicator stopAnimating];
            //            _searchIndicator.hidden = YES;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            
            if(success == 1){
                FollowingsArray = [result objectForKey:@"following"];
                for(NSDictionary *tempDict in FollowingsArray){
                    Followings *_responseData = [[Followings alloc] init];
                    _responseData.f_id = [tempDict objectForKey:@"id"];
                    _responseData.fullName = [tempDict objectForKey:@"full_name"];
                    _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                    _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                    _responseData.status = [tempDict objectForKey:@"state"];
                    [friendsArray addObject:_responseData];
                }
                
                [_follwersAndFollwings reloadData];
            }
        } else {
            //            [_searchIndicator stopAnimating];
            //            _searchIndicator.hidden = YES;
        }
    }];
}

- (void)getFollowers {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [SVProgressHUD show];
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"getFollowersFollowing",@"method",
                              token,@"session_token",@"1",@"page_no",userId,@"user_id",@"1",@"followers",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [SVProgressHUD dismiss];

        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            //[_searchIndicator stopAnimating];
            //            [_searchIndicator stopAnimating];
            //            _searchIndicator.hidden = YES;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1){
                FollowingsArray = [result objectForKey:@"followers"];
                
                for(NSDictionary *tempDict in FollowingsArray){
                    Followings *_responseData = [[Followings alloc] init];
                    
                    _responseData.f_id = [tempDict objectForKey:@"id"];
                    _responseData.fullName = [tempDict objectForKey:@"full_name"];
                    _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                    _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                    _responseData.status = [tempDict objectForKey:@"state"];
                    [friendsArray addObject:_responseData];
                }
                [_follwersAndFollwings reloadData];
            }
        } else {
            //            [_searchIndicator stopAnimating];
            //            _searchIndicator.hidden = YES;
        }
    }];
}
- (void)acceptRequest{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    Followings *PopUser = [[Followings alloc]init];
    if ([self isSearchDisplayControllerActice]) {
        PopUser = [self.searchedContent objectAtIndex:currentSelectedIndex];
    }else
        PopUser  = [friendsArray objectAtIndex:currentSelectedIndex];
    [self decrementNotificationCount];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_ACCEPT_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                //PopUser.status = @"FRIEND";
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
                NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                
                NSString *countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.friends_count];
                NSInteger previousCount = [countSuffix integerValue];
                previousCount ++;
                countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount];
                sharedManager._profile.friends_count = countSuffix;
                countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
                [Utils setChacedFriendsCount:countSuffix];
                
                countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.pendingReqsCount];
                previousCount = [countSuffix integerValue];
                previousCount --;
                countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount];
                sharedManager._profile.pendingReqsCount = countSuffix;
                countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
                [Utils setCachedPendingReqCount:countSuffix];
            }
        }else{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
    
}

-(void)decrementNotificationCount{
    if(loadFollowings == 2){
        NSString *pendingReqC = sharedManager._profile.pendingReqsCount;
        int value = [pendingReqC intValue];
        value = value - 1;
        pendingReqC = [NSString stringWithFormat:@"%d",value];
        sharedManager._profile.pendingReqsCount = [pendingReqC copy];
    }
}

- (void)rejectRequest:(NSInteger)indexToRequest{
    
    Followings *PopUser = [[Followings alloc]init];
    if ([self isSearchDisplayControllerActice]) {
        PopUser = [self.searchedContent objectAtIndex:indexToRequest];
    }else
        PopUser  = [friendsArray objectAtIndex:indexToRequest];
    friendId = PopUser.f_id;
    [self decrementNotificationCount];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_REJECT_REEQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                if(sharedManager.followers.count > 0){
                    NSInteger index = [sharedManager.followers indexOfObject:PopUser];
                    if (index != NSNotFound) {
                        if( [Utils userIsCeleb] && (PopUser.isFollowed == 0)){
                            [sharedManager.followers  removeObjectAtIndex:index];
                        }
                        else if(![Utils userIsCeleb]){
                            [sharedManager.followers  removeObjectAtIndex:index];
                        }
                    }
                }
                
                NSString *countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.pendingReqsCount];
                NSInteger previousCount = [countSuffix integerValue];
                previousCount --;
                countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount];
                sharedManager._profile.pendingReqsCount = countSuffix;
                countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
                [Utils setCachedPendingReqCount:countSuffix];
            }
        }else{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
}

-(BOOL)isSearchDisplayControllerActice{
    return [self.searchDisplayController isActive];
}

- (void) sendCancelRequest{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    Followings *PopUser = [[Followings alloc]init];
    if ([self isSearchDisplayControllerActice]) {
        PopUser = [self.searchedContent objectAtIndex:currentSelectedIndex];
    }else
        PopUser  = [friendsArray objectAtIndex:currentSelectedIndex];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_DELETE_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if(success == 1) {
                if(sharedManager.followers.count > 0){
                    NSInteger index = [sharedManager.followers indexOfObject:PopUser];
                    if (index != NSNotFound) {
                        if( [Utils userIsCeleb] && PopUser.isFollowed == 0){
                            [sharedManager.followers  removeObjectAtIndex:index];
                        }
                        if(![Utils userIsCeleb]){
                            [sharedManager.followers  removeObjectAtIndex:index];
                        }
                    }
                }
                
                NSString *countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.friends_count];
                NSInteger previousCount = [countSuffix integerValue];
                previousCount --;
                countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount];
                sharedManager._profile.friends_count = countSuffix;
                countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
                [Utils setChacedFriendsCount:countSuffix];
            }
        }else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
    
}

- (void) sendFriendRequest:(Followings *) pUser{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SEND_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                if([Utils userIsCeleb] && pUser.isFollowed == 0){
                    [sharedManager.followers insertObject:pUser atIndex:0];
                }
                else if(![Utils userIsCeleb]){
                    [sharedManager.followers insertObject:pUser atIndex:0];
                }
                [self isFriendAlreadyPresent:pUser];
            }
        }else{
            pUser.status = @"ADD_FRIEND";
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
            NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
            if([self isSearchDisplayControllerActice]){
                [self.searchDisplayController.searchResultsTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            }
            else{
                [self.follwersAndFollwings reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            }
        }
    }];
}

#pragma mark ISAlreadyPresent
-(BOOL)isFriendAlreadyPresent:(Followings*)fUser{
    for (int i =0; i< sharedManager.followers.count; i++){
        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
        if([tempObj.f_id isEqualToString:fUser.f_id]){
            tempObj.status = fUser.status;
            return YES;
        }
    }
    return NO;
}

-(void) OpenFriendsChannelPressed:(UIButton *)sender{
    UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    Followings *_responseData;
    if ([self isSearchDisplayControllerActice]) {
        _responseData = [self.searchedContent objectAtIndex:currentSelectedIndex];
    }else{
        _responseData =  [friendsArray objectAtIndex:currentSelectedIndex];
    }
    friendId = _responseData.f_id;
    
    UserChannel *commentController;
    if(IS_IPAD){
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
    }
    else
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
    commentController.ChannelObj = nil;
    commentController.friendID   = friendId;
    commentController.currentUser = _responseData;
    [[self navigationController] pushViewController:commentController animated:YES];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    tblFrame = self.follwersAndFollwings.frame;
}

#pragma mark Search Bar
- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView  {
    
    tableView.frame =   CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, self.follwersAndFollwings.frame.size.width,  self.follwersAndFollwings.frame.size.height);
}

-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope{
    
    if (lasatSearchString.length > 0){
        NSString *firstLetter = [lasatSearchString substringToIndex:1];
        
        if ([firstLetter  isEqual: @"#"]){
            NSLog(@"searching with hash tag");
            
            [self searchTopics];
//            NSMutableArray *topicsArray = searchTopicsArray;
//            NSString *srchText = [searchText stringByReplacingOccurrencesOfString:@"#" withString:@""];
//            NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"topic_name contains[c] %@", srchText];
//            NSArray *filteredArray = [[NSArray alloc] init];
//            filteredArray = [topicsArray filteredArrayUsingPredicate:resultPredicate];
//            self.searchedContent = [filteredArray mutableCopy];
//            [self.searchDisplayController.searchResultsTableView reloadData];
            
            
        }else{
            NSLog(@"searching with latter");
            if(_isLocalSearch && !_shudFetchFromServerOnly)
                friendsArray = [Utils getSortedArray:friendsArray];
            NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"fullName contains[c] %@", searchText];
            NSArray *filteredArray = [[NSArray alloc] init];
            filteredArray = [friendsArray filteredArrayUsingPredicate:resultPredicate];
            if(!_shudFetchFromServerOnly)
            {
                self.searchedContent = [filteredArray mutableCopy];
            }
            if(!_isLocalSearch)
            {
//                [self searchCornersNew];

                startSearchTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(searchCornersNew) userInfo:nil repeats:NO];

            }
            
        }
    }
    
}

-(void)filterFromServer:(NSString *)searchText scope:(NSString *)scope{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"fullName contains[c] %@", searchText];
    NSArray *filteredArray = [[NSArray alloc] init];
    filteredArray = [friendsArray filteredArrayUsingPredicate:resultPredicate];
    self.searchedContent = [filteredArray mutableCopy];
}
//crash

-(void)searchCornersNew{
    searchServerCall = TRUE;

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *textToSearch =  self.searchDisplayController.searchBar.text;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageNum = [NSString stringWithFormat:@"%d",self.pageNum];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SEARCH_FRIEND,@"method",
                              token,@"Session_token",pageNum,@"page_no",textToSearch,@"keyword", nil]; //pageNum
    
    NSLog(@"SEARCH STRING = %@",textToSearch);
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"SEARCH RESPONSE CAME");
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        searchServerCall = FALSE;
        if(success){
            NSArray *tempArray = [responseObject objectForKey:@"users_found"];
            if([tempArray isKindOfClass:[NSArray class]])
            {
                if(self.pageNum == 1){
                    self.searchedContent = [[NSMutableArray alloc] init];
                }
                for(NSDictionary *tempDict in tempArray){
                    Followings *_responseData = [[Followings alloc] init];
                    _responseData.f_id = [tempDict objectForKey:@"id"];
                    _responseData.fullName = [tempDict objectForKey:@"full_name"];
                    _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                    _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                    _responseData.status = [tempDict objectForKey:@"state"];
//                    if([_responseData.status isEqualToString:@"ADD_FRIEND"] || [_responseData.status isEqualToString:@"FOLLOW"] || [_responseData.status isEqualToString:@"FRIEND"]) {
                        [self.searchedContent addObject:_responseData];
//                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.searchDisplayController.searchResultsTableView reloadData];
                });
            }
            else
            {
                cannotScrollSearch = true;
                if(self.pageNum == 1)
                {
                    [self.searchedContent removeAllObjects];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.searchDisplayController.searchResultsTableView reloadData];
                });
            }
        }else{
            searchServerCall = FALSE;
            if(self.pageNum >= 1){
                [self.searchedContent removeAllObjects];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.searchDisplayController.searchResultsTableView reloadData];
            });
            cannotScrollSearch = TRUE;
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.searchedContent = [[NSMutableArray alloc] init];
        searchServerCall = FALSE;
    }];
}

-(void)SearchCorners:(int)call {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *textToSearch =  self.searchDisplayController.searchBar.text;
    searchServerCall = TRUE;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *pageNum = [NSString stringWithFormat:@"%d",self.pageNum];
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SEARCH_FRIEND,@"method",
                              token,@"Session_token",pageNum,@"page_no",textToSearch,@"keyword", nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            searchServerCall = FALSE;
            if(success == 1) {
                
                if(self.pageNum == 1){
                    self.searchedContent = [[NSMutableArray alloc] init];
                }
                if(callProcessed > call){
                    //[self.searchedContent removeAllObjects];
                    return ;
                }
                callProcessed = call;
                
                FollowingsArray = [result objectForKey:@"users_found"];
                if([FollowingsArray isKindOfClass:[NSArray class]])
                {
                    for(NSDictionary *tempDict in FollowingsArray){
                        Followings *_responseData = [[Followings alloc] init];
                        _responseData.f_id = [tempDict objectForKey:@"id"];
                        _responseData.fullName = [tempDict objectForKey:@"full_name"];
                        _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                        _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                        _responseData.status = [tempDict objectForKey:@"state"];
                        [self.searchedContent addObject:_responseData];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.searchDisplayController.searchResultsTableView reloadData];
                    });
                }
            }
            else{
                searchServerCall = FALSE;
                //                self.searchedContent = [[NSMutableArray alloc] init];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.searchDisplayController.searchResultsTableView reloadData];
                });
                cannotScrollSearch = TRUE;
            }
        }
        else{
            
            self.searchedContent = [[NSMutableArray alloc] init];
            searchServerCall = FALSE;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@""]) {
        NSLog(@"Backspace");
    }
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{
    
    NSLog(@"search string = %@",searchString);
    [startSearchTimer invalidate];
    [self.searchedContent removeAllObjects];
    for (UIView *subView in self.searchDisplayController.searchResultsTableView.subviews) {
        if ([subView isKindOfClass:[UILabel class]]) {
            UILabel *lbl = (UILabel*)subView;
            
            if([searchString isEqualToString:@"#"]){
                lbl.text = @"";
            }else{
                lbl.text = NSLocalizedString(@"no_record", nil);
            }
            
        }
    }
    //    for (UIView *subView in self.searchDisplayController.searchBar.subviews) {
    //        if ([subView isKindOfClass:[UIButton class]]) {
    //            UIButton *btn = (UIButton*)subView;
    //            [btn setTitle:@"cncl" forState:UIControlStateNormal];
    //        }
    //    }
    self.trendingtblView.hidden = YES;
    if([controller.searchBar.text isEqualToString:@""])
    {
        if(loadFollowings == 5  || loadFollowings == 6)
        {
            _hashTagsTableView.hidden = YES;
        }
        else if(loadFollowings != 2)
        {
            _hashTagsTableView.hidden = NO;
        }
        else
        {
            _hashTagsTableView.hidden = YES;
        }

    }
    else
    {
        self.hashTagsTableView.hidden = YES;
    }
    if (searchString.length < lasatSearchString.length) {
        self.pageNum = 1;
    }
    
    lasatSearchString = searchString;
    if(loadFollowings == 4){
        if(searchString.length == 0){
            callNumber = 1;
            callProcessed = 0;
            self.pageNum = 1;
        }
        callNumber ++;
        cannotScrollSearch = NO;
        self.pageNum = 1;
        
        
        NSLog(@"searching with latter");
        [self filterContentForSearchText:searchString
                                   scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                          objectAtIndex:[self.searchDisplayController.searchBar
                                                         selectedScopeButtonIndex]]];
        
        
        //        [self searchCornersNew];
    }else{
        //        [self filterContentForSearchText:searchString
        //                                   scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
        //                                          objectAtIndex:[self.searchDisplayController.searchBar
        //                                                         selectedScopeButtonIndex]]];
        //
        if(searchString.length == 0){
            callNumber = 1;
            callProcessed = 0;
            self.pageNum = 1;
        }
        callNumber ++;
        cannotScrollSearch = NO;
        self.pageNum = 1;
        [self filterContentForSearchText:searchString
                                   scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                          objectAtIndex:[self.searchDisplayController.searchBar
                                                         selectedScopeButtonIndex]]];
        //        [self searchCornersNew];
    }
    return YES;
}


#pragma mark Server Call
- (void) getHashTagsContent{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",self.pageNum];//sharedManager.forumPageNumber
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_TOPICS_V3,@"method",
                              token,@"session_token",pageStr,@"page_no",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                
                NSArray *topicsArray = [result objectForKey:@"topics"];
                if(topicsArray.count > 0)
                {
                    resultsFinished = false;
                    [self parseVideoResponse:result];
                    [self.hashTagsTableView reloadData];
                    self.hashTagsTableView.hidden = NO;
                }
                else
                {
                    resultsFinished = true;
                }
            }
            if(success == 3){
                //                _footerIndicator.hidden = YES;
                //                [_footerIndicator stopAnimating];
                //                [self logoutUser];
            }
            
            if ([sharedManager.forumsVideo count] == 0) {
                //                [noBeamsView setHidden:NO];
                //                _findfreindsBtn.hidden =  NO;
            }else{
                //                noBeamsView.hidden = YES;
                //                _findfreindsBtn.hidden =  YES;
            }
        }
        else{
            //            _footerIndicator.hidden = YES;
            //            [_footerIndicator stopAnimating];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            //  self.fetchingContent = false;
            // _homeRefreshBtn.hidden = NO;
        }
        
    }];
    
    
}

- (void) parseVideoResponse:(NSDictionary*)result{
    
    NSArray *usersArray = [NSArray new];
    NSArray *topicsArray = [NSArray new];
    
    usersArray = [result objectForKey:@"users"];
    topicsArray = [result objectForKey:@"topics"];
    
    int counter = 0;
    
    if([usersArray isKindOfClass:[NSArray class]])
    {
        for (int i = 0; i < usersArray.count ; i++){
            NSDictionary *userDict = (NSDictionary *)[usersArray objectAtIndex:i];
            //        SearchUserModel *userModel = [[SearchUserModel alloc] initWithDictionary:userDict];
            //        [searchUsersArray addObject:userModel];
            
            
            
            
            Followings *_responseData = [[Followings alloc] init];
            _responseData.f_id = [userDict objectForKey:@"id"];
            _responseData.fullName = [userDict objectForKey:@"full_name"];
            _responseData.is_celeb = [userDict objectForKey:@"is_celeb"];
            _responseData.profile_link = [userDict objectForKey:@"profile_link"];
            _responseData.status = [userDict objectForKey:@"profile_type"];
            
            [searchUsersArray addObject:_responseData];
            counter ++;
            if(counter == 10)
            {
                break;
            }
            
            
        }
    }
    NSLog(@"parsing%lu",(unsigned long)searchUsersArray.count);
    
    sharedManager.topicsArray = [NSMutableArray new];
    
    for(int i=0; i < topicsArray.count ; i++){
        NSDictionary *topicDict = (NSDictionary *)[topicsArray objectAtIndex:i];
        topicsModel *tObj = [[topicsModel alloc] initWithDictionary:topicDict];
        [searchTopicsArray addObject:tObj];
        [sharedManager.topicsArray addObject:tObj];
    }
    NSLog(@"parsing%lu",(unsigned long)searchTopicsArray.count);
    
    
    // [self playGif];
    
    
    
}



#pragma UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog(@"%ld",(long)collectionView.tag);
    
    if(collectionView.tag==0){
        return searchUsersArray.count+1;
    }
    else
    {
    topicsModel *tObj = searchTopicsArray[collectionView.tag-1];
    if([tObj.beams_count integerValue] > 4)
    {
        return tObj.beams_array.count+1;
    }
    return tObj.beams_array.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HashTagsCollectionViewCell *hashTagsCollectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HashTagsCollectionViewCell" forIndexPath:indexPath];
    
    if(collectionView.tag == 0){
        SearchUserModel *userModel = [[SearchUserModel alloc]init];
        
        if (indexPath.row==searchUsersArray.count){
            hashTagsCollectionViewCell.lblViewMore.hidden = NO;
            hashTagsCollectionViewCell.imgPlayIcon.image = [UIImage imageNamed:@""];
            hashTagsCollectionViewCell.imgUserProfile.image = [UIImage imageNamed:@""];
            hashTagsCollectionViewCell.imgUserProfile.backgroundColor = [UIColor colorWithRed:12.0/255 green:101.0/255 blue:170.0/255 alpha:1];
        }else{
            hashTagsCollectionViewCell.lblViewMore.hidden = YES;
            hashTagsCollectionViewCell.imgPlayIcon.image = [UIImage imageNamed:@""];
            userModel  = [searchUsersArray objectAtIndex:indexPath.row];
            [hashTagsCollectionViewCell.imgUserProfile setImageWithURL:[NSURL URLWithString:userModel.profile_link] placeholderImage:[UIImage imageNamed:@"place_holder"]];
            hashTagsCollectionViewCell.imgUserProfile.backgroundColor = [UIColor clearColor];

        }
        hashTagsCollectionViewCell.imgUserProfile.layer.cornerRadius =  32;
        hashTagsCollectionViewCell.imgUserProfile.clipsToBounds = YES;
        hashTagsCollectionViewCell.background.layer.cornerRadius =  32;
        hashTagsCollectionViewCell.background.clipsToBounds = YES;
        hashTagsCollectionViewCell.outerLine.hidden = YES;
        return hashTagsCollectionViewCell;
    }else{
        topicsModel *tObj = searchTopicsArray[collectionView.tag-1];
        
        if (indexPath.row==tObj.beams_array.count){
            hashTagsCollectionViewCell.lblViewMore.hidden = NO;
            hashTagsCollectionViewCell.imgPlayIcon.image = [UIImage imageNamed:@""];
            hashTagsCollectionViewCell.imgUserProfile.image = [UIImage imageNamed:@""];
            hashTagsCollectionViewCell.imgUserProfile.backgroundColor = [UIColor colorWithRed:12.0/255 green:101.0/255 blue:170.0/255 alpha:1];

        }else{
            hashTagsCollectionViewCell.lblViewMore.hidden = YES;
            VideoModel *tempVideo = tObj.beams_array[indexPath.row];
            hashTagsCollectionViewCell.imgUserProfile.backgroundColor = [UIColor clearColor];

//            [hashTagsCollectionViewCell.imgUserProfile setImageWithURL:[NSURL URLWithString:tempVideo.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"place_holder"]];

            [hashTagsCollectionViewCell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:tempVideo.video_thumbnail_link]];

        }
        hashTagsCollectionViewCell.imgUserProfile.layer.cornerRadius =  10;
        hashTagsCollectionViewCell.imgUserProfile.clipsToBounds = YES;
        
        hashTagsCollectionViewCell.background.layer.cornerRadius =  10;
        hashTagsCollectionViewCell.background.clipsToBounds = YES;
        return hashTagsCollectionViewCell;
    }
    
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0, 50);
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize sz;
    float returnValue;
    
    if (collectionView.tag==0){
        return CGSizeMake(70, 70);
    }
    else
    {
        returnValue = ((collectionView.frame.size.width-30)/4) ;
        return CGSizeMake(returnValue, returnValue);
    }
    
   
   // return CGSizeMake(70, 70);
    
}

#pragma UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//    if(collectionView == _collectionViewHashTags) {
//        if(indexPath.section == searchTopicsArray.count - 1) {
//            self.pageNum++;
//            [self getHashTagsContent];
//        }
//    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected= %ld",collectionView.tag);
//    selectedSectionNumber = indexPath.section;
//
//    if (collectionView == _collectionViewHashTags){
//        topicsModel *tObj = searchTopicsArray[indexPath.section-1];
//        VideoModel *_model = tObj.beams_array[indexPath.row];
//        CommentsVC *commentController;
//
//        if(IS_IPAD)
//            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
//        else  if (IS_IPHONEX){
//            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
//        }
//        else
//            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
//        commentController.commentsObj   = Nil;
//        commentController.postArray     = _model;
//        commentController.cPostId       =  _model.videoID;;
//        commentController.isFirstComment = TRUE;
//        commentController.isComment     = FALSE;
//
//        [[self navigationController] pushViewController:commentController animated:YES];
//
//    }
//
//    if (collectionView == _collectionViewPeopleYouMayKnow){
//
//        if (indexPath.row==searchUsersArray.count){
//            [[NavigationHandler getInstance] MoveToPopularUsers];
//        }else{
//            Followings *tempUsers  = [searchUsersArray  objectAtIndex:indexPath.row];
//            friendId = tempUsers.f_id;
//            UserChannel *commentController;
//            if(IS_IPAD){
//                commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
//            }else if (IS_IPHONEX){
//                commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
//            }
//            else
//                commentController = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
//            commentController.ChannelObj = nil;
//            commentController.friendID   = friendId;
//            commentController.currentUser = tempUsers;
//            [[self navigationController] pushViewController:commentController animated:YES];
//        }
//
//
//    }
//
    
    if(collectionView.tag == 0){
        if (indexPath.row==searchUsersArray.count){
            [[NavigationHandler getInstance] MoveToPopularUsers];
        }else{
            Followings *tempUsers  = [searchUsersArray  objectAtIndex:indexPath.row];
            friendId = tempUsers.f_id;
            UserChannel *commentController;
            if(IS_IPAD){
                commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
            }else if (IS_IPHONEX){
                commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
            }
            else
            commentController = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
            commentController.ChannelObj = nil;
            commentController.friendID   = friendId;
            commentController.currentUser = tempUsers;
            [[self navigationController] pushViewController:commentController animated:YES];
        }

    }else{
        topicsModel *tObj = searchTopicsArray[collectionView.tag-1];
        
        if (indexPath.row==tObj.beams_array.count){
            
            TopicDetailsVC *tDetails    = [[TopicDetailsVC alloc] initWithNibName:@"TopicDetailsVC" bundle:nil];
            tDetails.tModel = tObj;
            tDetails.tittle = tObj.topic_name;
            [[self navigationController] pushViewController:tDetails animated:YES];
            
//            VideoModel *_model = tObj.beams_array[indexPath.row-1];
//
//            Followings *tempUsers  = [[Followings alloc] init];
//            tempUsers.f_id = _model.user_id;
//            tempUsers.fullName = _model.userName;
//            tempUsers.is_celeb = _model.isCelebrity;
//            tempUsers.profile_link = _model.profile_image;
//
//            friendId = tempUsers.f_id;
//            UserChannel *commentController;
//            if(IS_IPAD){
//                commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
//            }else if (IS_IPHONEX){
//                commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
//            }
//            else
//                commentController = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
//            commentController.ChannelObj = nil;
//            commentController.friendID   = friendId;
//            commentController.currentUser = tempUsers;
//            [[self navigationController] pushViewController:commentController animated:YES];
            
        }else{
            VideoModel *_model = tObj.beams_array[indexPath.row];
            CommentsVC *commentController;
            
            if(IS_IPAD)
                commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
            else  if (IS_IPHONEX){
                commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
            }
            else
                commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
            commentController.commentsObj   = Nil;
            commentController.postArray     = _model;
            commentController.cPostId       =  _model.videoID;;
            commentController.isFirstComment = TRUE;
            commentController.isComment     = FALSE;
            
            [[self navigationController] pushViewController:commentController animated:YES];
        }
    }
    
}



//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
//           viewForSupplementaryElementOfKind:(NSString *)kind
//                                 atIndexPath:(NSIndexPath *)indexPath
//{
//    PeopleYouMayKnowHeaderView *headerView;
//    headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
//                  kind withReuseIdentifier:@"peopleYouMayKnowHeaderView" forIndexPath:indexPath];
//
//
//    headerView.lblSectionHeaderTitle.layer.masksToBounds = YES;
//    [headerView.lblSectionHeaderTitle setClipsToBounds:YES];
//    headerView.lblSectionHeaderTitle.layer.cornerRadius = 5.0;
//
//    if (indexPath.section==0){
//        headerView.lblSectionHeaderTitle.text =  @"  People you may know  ";
//        return headerView;
//    }else{
//        topicsModel *tObj = searchTopicsArray[indexPath.section-1];
//
//        NSMutableString *topicName = [NSMutableString stringWithString:@"  "];
//        [topicName appendString:tObj.topic_name];
//        [topicName appendString:@"  "];
//        headerView.lblSectionHeaderTitle.text =  topicName;
//
//        return headerView;
//    }
//
//
//}

//- (void)updateSectionHeader:(UICollectionReusableView *)header forIndexPath:(NSIndexPath *)indexPath
//{
//    NSString *text = [NSString stringWithFormat:@"header #%i", indexPath.row];
//}

#pragma mark Get Comments
//-(void) ShowCommentspressed:(UIButton *)sender{
//    UIButton *CommentsBtn = (UIButton *)sender;
//    currentIndexHome = CommentsBtn.tag;
//    VideoModel *_model = [sharedManager.forumsVideo objectAtIndex:currentIndexHome];
//    CommentsVC *commentController;
//    if(IS_IPAD)
//        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
//    if (IS_IPHONEX){
//        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
//    }
//    else
//        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
//    commentController.commentsObj   = Nil;
//    commentController.postArray     = _model;
//    commentController.cPostId       =  _model.videoID;;
//    commentController.isFirstComment = TRUE;
//    commentController.isComment     = FALSE;
//
//    [[self navigationController] pushViewController:commentController animated:YES];
//}

@end

