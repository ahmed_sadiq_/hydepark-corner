//
//  DBCommunicator.m
//  HydePark
//
//  Created by Ahmed Sadiq on 23/11/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "DBCommunicator.h"
#import <sqlite3.h>
#import "OfflineDataModel.h"


static DBCommunicator *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBCommunicator
+ (id)sharedManager {
    static DBCommunicator *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (void)createDB {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.documentsDirectory = [paths objectAtIndex:0];
    // Keep the database filename.
    self.databaseFilename = [[NSString alloc] initWithString:
                             [self.documentsDirectory stringByAppendingPathComponent: @"hydeparkoffline.db"]];
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: self.databaseFilename ] == NO) {
        const char *dbpath = [self.databaseFilename UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
            char *errMsg;
            const char *sql_stmt = "create table if not exists offline (id integer primary key autoincrement, Session_token text, reply_count text, caption text, is_anonymous text, mute text, video_length text, post_id text, parent_comment_id text, privacy text, language text, timestamp text, mediadata text,isAudio text,isComment text, thumbnail text)";
            
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            sqlite3_close(database);
            NSLog(@"Database created Successfully");
        } else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    } else {
        NSLog(@"file already exists");
    }
}

- (BOOL)openDB {
    const char *dbpath = [self.databaseFilename UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
        return YES;
    } else {
        return NO;
    }
}

- (void)closeDB {
    sqlite3_close(database);
}

-(void)newEntryWithSession:(NSString *)userSession replyCount:(NSString *)allowedReply caption:(NSString *)caption isAnonymous:(NSString *)anonyCheck isMute:(NSString *)muteCheck videoLength:(NSString *)duration postID:(NSString *)postId parentCommentID:(NSString *)parentId privacy:(NSString *)privacyCheck language:(NSString *)langCode timeStamp:(NSString *)currentTime mediaData:(NSString *)data isAudio:(NSString *)audioCheck isComment:(NSString *)commentCeck fromGallery:(BOOL)isFromGallery withThumbnail:(NSString *)thumbnail{
    
    if ([self openDB]) {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into offline (Session_token,reply_count,caption,is_anonymous,mute,video_length,post_id,parent_comment_id,privacy,language,timestamp,mediadata,isAudio,isComment,thumbnail) values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",userSession,allowedReply,caption,anonyCheck,muteCheck,duration,postId,parentId,privacyCheck,langCode,currentTime,data,audioCheck,commentCeck,thumbnail];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE) {
            NSLog(@"New Entry in db successfull");
        } else {
            NSLog(@"Failed To enter new entry in db");
        }
        sqlite3_reset(statement);
    } else {
        NSLog(@"Failed To Open DB");
    }
}

- (NSMutableArray *)retrieveAllRows {
    NSMutableArray *allRows = [[NSMutableArray alloc] init];
    
    
    if ([self openDB]) {
        NSString *getSQL = [NSString stringWithFormat:@"SELECT * FROM offline"];
        
        const char *get_stmt = [getSQL UTF8String];
        sqlite3_prepare_v2(database, get_stmt,-1, &statement, NULL);
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            NSDictionary *dict = [[NSDictionary alloc] init];
            OfflineDataModel *model = [[OfflineDataModel alloc] init];
            char *field0 = (char *) sqlite3_column_text(statement,0);
            NSString *rowID = [[NSString alloc] initWithUTF8String: field0];
            char *field2 = (char *) sqlite3_column_text(statement,1);
            NSString *sessionToken = [[NSString    alloc] initWithUTF8String: field2];
            char *field3 = (char *) sqlite3_column_text(statement,2);
            NSString *allowedReply = [[NSString alloc] initWithUTF8String: field3];
            char *field4 = (char *) sqlite3_column_text(statement,3);
            NSString *caption = [[NSString    alloc] initWithUTF8String: field4];
            char *field5 = (char *) sqlite3_column_text(statement,4);
            NSString *anonyCheck = [[NSString alloc] initWithUTF8String: field5];
            char *field6 = (char *) sqlite3_column_text(statement,5);
            NSString *muteCheck = [[NSString    alloc] initWithUTF8String: field6];
            char *field7 = (char *) sqlite3_column_text(statement,6);
            NSString *duration = [[NSString alloc] initWithUTF8String: field7];
            char *field8 = (char *) sqlite3_column_text(statement,7);
            NSString *postId = [[NSString    alloc] initWithUTF8String: field8];
            char *field9 = (char *) sqlite3_column_text(statement,8);
            NSString *parentId = [[NSString alloc] initWithUTF8String: field9];
            char *field10 = (char *) sqlite3_column_text(statement,9);
            NSString *privacyCheck = [[NSString    alloc] initWithUTF8String: field10];
            char *field11 = (char *) sqlite3_column_text(statement,10);
            NSString *langCode = [[NSString alloc] initWithUTF8String: field11];
            char *field12 = (char *) sqlite3_column_text(statement,11);
            NSString *currentTime = [[NSString    alloc] initWithUTF8String: field12];
            char *field13 = (char *) sqlite3_column_text(statement,12);
            NSString *data = [[NSString alloc] initWithUTF8String: field13];
            char *field14 = (char *) sqlite3_column_text(statement,13);
            NSString *isAudio = [[NSString alloc] initWithUTF8String: field14];
            char *field15 = (char *) sqlite3_column_text(statement,14);
            NSString *isComment = [[NSString alloc] initWithUTF8String: field15];
            char *field16 = (char *) sqlite3_column_text(statement,15);
            NSString *thumbnail = [[NSString alloc] initWithUTF8String: field16];
            
            model.rowID = rowID;
            model.sessionToken = sessionToken;
            model.allowedReply = allowedReply;
            model.caption = caption;
            model.anonyCheck = anonyCheck;
            model.muteCheck = muteCheck;
            model.duration = duration;
            model.postId = postId;
            model.parentId = parentId;
            model.privacyCheck = privacyCheck;
            model.langCode = langCode;
            model.currentTime = currentTime;
            model.data = data;
            model.isAudio = isAudio;
            model.isComment = isComment;
            model.thumbnail = thumbnail;
            
            
            // textv.text=str; // I don't know why your are mixing your view controller stuff's in database function.
            // Add the string in the array.
            [allRows addObject:model];
        }
        
        sqlite3_reset(statement);
    } else {
        NSLog(@"Failed To Open DB");
    }
    
    // Finally you can return your allRows
    return allRows;
}

- (void)deleteRowWithID:(NSString *)rowID {
    if ([self openDB]) {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM offline WHERE id = %@",rowID];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(database, delete_stmt,-1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE) {
            NSLog(@"Entry Deleted Successfully");
        } else {
            NSLog(@"Failed To delete entry from db");
        }
        sqlite3_reset(statement);
    } else {
        NSLog(@"Failed To Open DB");
    }
}

- (void)clearDB {
    if ([self openDB]) {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM offline"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(database, delete_stmt,-1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE) {
            NSLog(@"DB Clear Successfully");
        } else {
            NSLog(@"Failed To clear db");
        }
        sqlite3_reset(statement);
    } else {
        NSLog(@"Failed To Open DB");
    }
}


@end
