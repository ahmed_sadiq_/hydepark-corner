//
//  HashTagsCollectionViewCell.h
//  HydePark
//
//  Created by apple on 6/30/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HashTagsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgPlayIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblViewMore;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfile;
@property (weak, nonatomic) IBOutlet UIView *background;
@property (weak, nonatomic) IBOutlet UIView *outerLine;

@end
