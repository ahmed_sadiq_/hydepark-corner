//
//  StoriesViewController.h
//  HydePark
//
//  Created by Ahmed Sadiq on 14/12/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoModel.h"
#import "AVFoundation/AVFoundation.h"
#import "AppDelegate.h"
#import <HBRecorder/HBRecorder.h>
#import <AVKit/AVKit.h>

@interface StoriesViewController : UIViewController<HBRecorderProtocol>
{
    IBOutlet UIButton *closeBtnAudio;
    IBOutlet UILabel *countDownlabel;
    IBOutlet UIImageView *audioBtnImage;
    BOOL isRecording;
    NSTimer *timerToupdateLbl;
    BOOL isForName;
    NSTimer* audioTimeOut;
    int secondsLeft;
    NSString *secondsConsumed;
    BOOL uploadBeamTag;
    BOOL uploadAnonymous;
    AppDelegate *appDelegate;
    NSString *video_duration;
    NSInteger orientationAngle;
    float hiddenYposition;
    float shownYposition;
    NSTimer *optionsTimer;
    IBOutlet UISlider *mySlider;
    IBOutlet UIView *mySliderBackground;
    NSTimer *timer;
    AVPlayer* playVideo;
    AVPlayerItem* playerItem;
    AVAsset *asset;
    BOOL shudClearPlayer;
    int resumeSeconds;
    BOOL shudContinuePlayer;
    NSMutableArray *backupFriendsStories;
}
@property (strong, nonatomic) AVPlayerViewController *playerViewController;
@property (strong, nonatomic) NSData *CmovieData;
@property (strong, nonatomic) NSData *profileData;
@property (strong, nonatomic) NSURL *videoPath;
@property (strong, nonatomic) UIImage *thumbnailToUpload;
@property (strong, nonatomic) UIImage *thumbnailToUpload2;
@property (strong, nonatomic) UIImage *thumbnailToUpload3;
@property (strong, nonatomic) NSString *pID;
@property (assign)    BOOL isFirstComment;
@property (assign)    BOOL isFromDeepLink;
@property (strong, nonatomic) NSData *CaudioData;
@property(weak, nonatomic) IBOutlet UIView *lowerOptionsView;
@property(weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(strong, nonatomic) NSMutableArray *storiesArray;
@property(strong, nonatomic) NSMutableArray *removedStoriesArray;
@property(strong, nonatomic) NSMutableArray *checkArray;
@property(strong, nonatomic) VideoModel *videoModel;
@property(assign, nonatomic) NSInteger indexRow;
@property(assign, nonatomic) NSInteger replaceIndex;
@property(assign, nonatomic) NSInteger uploadIndex;
@property(assign, nonatomic) NSInteger count;
@property (strong, nonatomic) IBOutlet UIView *uploadAudioView;
@property (weak, nonatomic) IBOutlet UIButton *audioRecordBtn;
@property (weak, nonatomic) IBOutlet UIButton *audioBtn;
@property (weak, nonatomic) IBOutlet UIButton *anonymousBtn;
@property (weak, nonatomic) IBOutlet UILabel *beamLbl;
@property (weak, nonatomic) IBOutlet UILabel *anonymousLbl;
@property (weak, nonatomic) IBOutlet UILabel *audioLbl;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (assign)    BOOL isFromSpeakers;


@end
