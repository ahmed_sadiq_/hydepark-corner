//
//  FriendChat.h
//  HydePark
//
//  Created by Samreen Noor on 18/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendChat : NSObject

@property ( strong , nonatomic ) NSString *friend_id;
@property ( strong , nonatomic ) NSString *full_name;
@property ( strong , nonatomic ) NSString *profile_link;
@property ( strong , nonatomic ) NSString *total_messages;
@property ( strong , nonatomic ) NSString *seen_counts;
@property ( strong , nonatomic ) NSString *unseen_counts;
@property ( strong , nonatomic) NSString *is_friend;
@property ( strong , nonatomic) NSString *thumbnailLink;
@property int chatCount;

@end
