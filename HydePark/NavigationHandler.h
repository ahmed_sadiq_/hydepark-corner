//
//  NavigationHandler.h
//  Yolo
//
//  Created by Salman Khalid on 13/06/2014.
//  Copyright (c) 2014 Xint Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "DataContainer.h"
#import "Followings.h"
@interface NavigationHandler : UINavigationController{
    
    UINavigationController *navController;
    UIWindow *_window;
    
     AppDelegate *appDelegate;
    DataContainer *sharedManager;
}

-(id)initWithMainWindow:(UIWindow *)_tempWindow;
-(void)loadFirstVC;

+(NavigationHandler *)getInstance;
-(UINavigationController*) getNavigationController;
-(void)PopToRootViewController;
-(void)NavigateToHomeScreen;
-(void)MoveToHomeScreen;
-(void)NavigateToLoginScreen;
-(void)MoveToComments;
-(void)MoveToMyBeam;
-(void)NavigateToSignUpScreen;
-(void)NavigateToRoot;
-(void)MoveToTopics;
-(void)MoveToPlayer;
-(void)MoveToProfile;
-(void)MoveToNotifications;
-(void)MoveToSearchFriends;
-(void)MoveToPopularUsers;
-(void)LogoutUser;
-(void)MoveToChat;
-(void)MoveToLikeBeam:(VideoModel*)videoModel;
-(void)MoveToCommentsNotifi:(VideoModel*)videoModel second:(NSString*)parentId;
-(void)MoveToFriendChat:(Followings *)frnd;
-(void)MoveToCommentsOnCommentsNotifi:(VideoModel*)videoModel second:(NSString*)parentId;
-(void)MoveToUserChannel:(NSString*)FriendID friendModel:(Followings *)friendModel;
-(void)MoveToBeamComment:(NSString*)postID andParentCommentID:(NSString *)pCmntId isComment:(BOOL)isComment;
-(void)moveToSettingsVC;
-(void)deallocateAll;
@end
