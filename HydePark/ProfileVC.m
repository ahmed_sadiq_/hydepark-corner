//
//  ProfileVC.m
//  HydePark
//
//  Created by Mr on 07/05/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "ProfileVC.h"
#import "Constants.h"
#import "DrawerVC.h"
#import "NavigationHandler.h"
#import "HomeCell.h"
#import "SVProgressHUD.h"
#import "Utils.h"
#import "NIDropDown.h"
#import "RadioButton.h"
#import <QuartzCore/QuartzCore.h>
#import "CustomLoading.h"
#import "NSData+AES.h"

@interface ProfileVC ()<UIPickerViewDelegate, UIPickerViewDataSource>{
#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

    NSArray *genderArr;
    UIToolbar *toolBar;
    UIToolbar *toolBarC;
}
@property (nonatomic, weak) IBOutlet UILabel *maintitle;
@property (nonatomic, weak) IBOutlet UILabel *elbl;
@property (nonatomic, weak) IBOutlet UILabel *blbl;
@property (nonatomic, weak) IBOutlet UILabel *glbl;
@end

@implementation ProfileVC
    @synthesize countryPicker;
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    _bioLbl.text = NSLocalizedString(@"bio", nil);
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.genderPicker.frame.origin.y - 44, self.view.frame.size.width, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"save", @"")
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(doneClicked)];
    toolBar.items = @[flex, barButtonDone];
    toolBar.hidden = YES;
    [self.view addSubview:toolBar];
    UIBarButtonItem *flexC = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDoneC = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"save", @"")                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(doneClicked)];
    self.maintitle.text = NSLocalizedString(@"profile", @"");
    toolBarC = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.countryPicker.frame.origin.y - 44, self.view.frame.size.width, 44)];
    [toolBarC setBarStyle:UIBarStyleDefault];
    toolBarC.items = @[flexC, barButtonDoneC];
    toolBarC.hidden = YES;
    self.elbl.text = NSLocalizedString(@"email", @"");
    self.glbl.text = NSLocalizedString(@"gender", @"");
    self.blbl.text = NSLocalizedString(@"birthday", @"");
    [saveProfile setTitle:NSLocalizedString(@"save", @"") forState:UIControlStateNormal];
    [self.view addSubview:toolBarC];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    appdelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    // Do any additional setup after loading the view from its nib.
    _EditProfileBtn.hidden = TRUE;
    
    sharedManager = [DataContainer sharedManager];
    if(appdelegate.loaduserProfiel)
    [self getUserProfile];
    else{
        [self getProfile];
    }
    [self EditProfilePressed:self];
    genderArr = [NSArray arrayWithObjects:NSLocalizedString(@"male", @""),NSLocalizedString(@"female", @""),nil];
    // border
    [self.baseview.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.baseview.layer setBorderWidth:1.5f];
    
    // drop shadow
    [self.baseview.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.baseview.layer setShadowOpacity:0.8];
    //[self.baseview.layer setShadowRadius:3.0];
    [self.baseview.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    self.genderPicker.hidden = YES;
    beamsThumbnails = [[NSMutableArray alloc]init];
    countryPicker.delegate = self;
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    self.countryPicker.hidden = YES;
    
    
    
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    if(IS_IPHONE_5)
    [EditProfileScrollView setContentSize:CGSizeMake(screenWidth,screenHeight - 50)];
    else if (IS_IPHONE_6)
    [EditProfileScrollView setContentSize:CGSizeMake(screenWidth,screenHeight - 50)];
    else if (IS_IPHONE_6Plus)
    [EditProfileScrollView setContentSize:CGSizeMake(screenWidth,screenHeight - 50)];
    else if(IS_IPAD){
        EditProfileScrollView.frame = CGRectMake(0, 55, 768, screenHeight-50 );
        _boxView.frame=CGRectMake(0, 0, 768, 900 );
    }
    arr_gender = [[NSArray alloc] initWithObjects:@"MALE", @"FEMALE", nil];
    gender.text = @"MALE";
   // [self.usernamefld becomeFirstResponder];
    /*     ProfilePic.clipsToBounds = YES;
     ProfilePic.layer.backgroundColor = [UIColor clearColor].CGColor;
     ProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
     ProfilePic.layer.borderWidth = 2.0f; */
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
    {
        toolBar.hidden = YES;
        toolBarC.hidden = YES;
        [self.genderPicker setHidden:YES];
        [self.countryPicker setHidden:YES];
        [self.view endEditing:YES];
    }
- (void)doneClicked {
    toolBar.hidden = YES;
    toolBarC.hidden = YES;
    [self.genderPicker setHidden:YES];
    [self.countryPicker setHidden:YES];
    
}
#pragma mark Picker View Delegates
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
    {
        self.genderField.text = [genderArr objectAtIndex:row];
    }
    
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
    {
        return 200;
    }
    
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
    {
        return 35;
    }
    
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
    {
        
        return  [genderArr objectAtIndex:row];
    }
    
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
    {
        return 2;
    }
    
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
    {
        return 1;
    }
    
#pragma mark GetProfile
- (void) getUserProfile{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *user_id = appdelegate.userToView;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_USERPROFILE,@"method",token,@"session_token",user_id,@"user_id",nil];
    
    appdelegate.loaduserProfiel = false;
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *requests = [[NSMutableURLRequest alloc] init];
    [requests setURL:url];
    [requests setHTTPMethod:@"POST"];
    [requests setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:requests queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSDictionary *profile = [result objectForKey:@"profile"];
            NSArray *tempArray = [result objectForKey:@"beams_data"];
            
            if(success == 1) {
                int i = 0;
                if(tempArray.count == 0 || tempArray == nil)
                {
                    
                }
                else{
                    for(NSDictionary *tempDict in tempArray){
                        NSString *thumbnail = [tempDict objectForKey:@"video_thumbnail_link"];
                        [beamsThumbnails insertObject:thumbnail atIndex:i];
                        i++;
                    }
                }
                [self SetProfileObject:profile];
                [self setprofileData];
            }
        }
        else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"no_internet", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}
- (void) getProfile{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    _EditProfileBtn.hidden = FALSE;
    
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_PROFILE,@"method",token,@"session_token",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *requests = [[NSMutableURLRequest alloc] init];
    [requests setURL:url];
    [requests setHTTPMethod:@"POST"];
    [requests setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:requests queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
//        NSLog(@"%ld",(long)[(NSHTTPURLResponse *)response statusCode]);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int success = [[result objectForKey:@"success"] intValue];
            NSDictionary *profile = [result objectForKey:@"profile"];
            NSArray *tempArray = [result objectForKey:@"beams_data"];
            int i = 0;
            
            if(success == 1) {
                if(tempArray.count == 0 || tempArray == nil)
                {
                    
                }
                else{
                    for(NSDictionary *tempDict in tempArray){
                        NSString *thumbnail = [tempDict objectForKey:@"video_thumbnail_link"];
                        [beamsThumbnails insertObject:thumbnail atIndex:i];
                        i++;
                    }
                }
                [self SetProfileObject:profile];
                [self setprofileData];
                
            }
            
        }
        else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"no_internet", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}
-(void) SetProfileObject:(NSDictionary *)obj{
    ProfileObj = [[ProfileModel alloc]init];
    ///Saving Profile Data
    ProfileObj.user_id = [obj objectForKey:@"id"];
    ProfileObj.full_name = [obj objectForKey:@"full_name"];
    sharedManager._profile.full_name = [ProfileObj.full_name mutableCopy];
    [[NSUserDefaults standardUserDefaults] setObject:sharedManager._profile.full_name forKey:@"User_Name"];
    ProfileObj.account_type = [obj objectForKey:@"account_type"];
    ProfileObj.likes_count = [obj objectForKey:@"followers_count"];
    ProfileObj.profile_image = [obj objectForKey:@"profile_link"];
    ProfileObj.profile_type = [obj objectForKey:@"profile_type"];
    ProfileObj.session_token = [obj objectForKey:@"session_token"];
    ProfileObj.email = [obj objectForKey:@"email"];
    ProfileObj.friends_count = [obj objectForKey:@"following_count"];
    ProfileObj.cover_link = [obj objectForKey:@"cover_link"];
    ProfileObj.cover_type = [obj objectForKey:@"cover_type"];
    ProfileObj.city = [obj objectForKey:@"city"];
    ProfileObj.country = [obj objectForKey:@"country"];
    NSString *dob= [obj objectForKey:@"date_of_birth"];
    if([dob isEqualToString:@"0000-00-00"])
    ProfileObj.date_of_birth = @"YYYY-MM-DD";
    else
    ProfileObj.date_of_birth = dob;
    ProfileObj.is_celeb = [obj objectForKey:@"is_celeb"];
    ProfileObj.beams_count = [obj objectForKey:@"beams_count"];
    
    ProfileObj.gender = [obj objectForKey:@"gender"];
    ProfileObj.WorkingPlace = [obj objectForKey:@"worked_at"];
    ProfileObj.mobileNo = [obj objectForKey:@"mobile_no"];
    ProfileObj.StudiedIn = [obj objectForKey:@"study_at"];
    ProfileObj.full_name = [obj objectForKey:@"full_name"];
    if([obj objectForKey:@"bio"] != nil)
        ProfileObj.bio = [obj objectForKey:@"bio"];
}
-(void) setprofileData{
    //// Setting Profile Data
    if((NSString *)[NSNull null] != ProfileObj.bio) {
        _bioTextView.text = [Utils decodeForEmojis:ProfileObj.bio];
    }
    
    UserName.text = [Utils getDecryptedTextFor:ProfileObj.full_name];

    NSString *decodedString2 = [Utils getDecryptedTextFor:ProfileObj.email];
    
    
    userEmail.text = decodedString2;
    lblBirthday.text = ProfileObj.date_of_birth;
    txtEmail.text = ProfileObj.email;
    lblBeams.text = ProfileObj.beams_count;
    lblFriends.text = [[NSString alloc]initWithFormat:@"%@ Following",ProfileObj.friends_count];
    lblLikes.text = [[NSString alloc]initWithFormat:@"Followed by %@",ProfileObj.likes_count];
    self.genderField.text = ProfileObj.gender;
    
    NSString *decodedString3 = [Utils getDecryptedTextFor:ProfileObj.full_name];
    
    _usernamefld.text = decodedString3;
    mobileLabel.text = [[NSString alloc]initWithFormat:@"%@ ",ProfileObj.mobileNo];
    txtCountry.text = ProfileObj.country;
    lblWorkingat.text = [[NSString alloc] initWithFormat:@"Working at %@",ProfileObj.WorkingPlace];
    StudiIn.text = [[NSString alloc] initWithFormat:@"Studied at %@",ProfileObj.StudiedIn];
    LivesInField.text = ProfileObj.country;
    if(beamsThumbnails.count > 0){
        one.imageURL = [NSURL URLWithString:[beamsThumbnails objectAtIndex:0]];
        NSURL *url1 = [NSURL URLWithString:[beamsThumbnails objectAtIndex:0]];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url1];
    }
    if(beamsThumbnails.count > 1){
        two.imageURL = [NSURL URLWithString:[beamsThumbnails objectAtIndex:1]];
        NSURL *url2 = [NSURL URLWithString:[beamsThumbnails objectAtIndex:1]];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url2];
    }
    if(beamsThumbnails.count > 2){
        three.imageURL = [NSURL URLWithString:[beamsThumbnails objectAtIndex:2]];
        NSURL *url3 = [NSURL URLWithString:[beamsThumbnails objectAtIndex:2]];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url3];
    }
}
#pragma mark UpdateProfile
    
- (void) updateProfile{
    [CustomLoading showAlertMessage];
    _EditProfileBtn.hidden = FALSE;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *bioText = [Utils encodeForEmojis:_bioTextView.text] ;
    sharedManager._profile.bio = bioText;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    request = [ASIFormDataRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"application/json; encoding=utf-8"];
    [request setPostValue:token forKey:@"session_token"];
    [request setPostValue:txtName.text forKey:@"full_name"];
    [request setPostValue:txtCity.text forKey:@"city"];
    [request setPostValue:LivesInField.text forKey:@"country"];
    [request setPostValue:self.genderField.text forKey:@"gender"];
    [request setPostValue:lblBirthday.text forKey:@"date_of_birth"];
    [request setPostValue:StudiedInField.text forKey:@"study_at"];
    [request setPostValue:mobileEditField.text forKey:@"mobile_no"];
    [request setPostValue:workingAtField.text forKey:@"worked_at"];
    [request setPostValue:_usernamefld.text forKey:@"full_name"];
    [request setPostValue:bioText forKey:@"bio"];
    [request setPostValue:METHOD_UPDATE_PROFILE forKey:@"method"];
    
    [request setRequestMethod:@"POST"];
    [request setTimeOutSeconds:300];
    [request setDelegate:self];
    [request startAsynchronous];
}
    
- (void)requestFinished:(ASIHTTPRequest *)request
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        // NSString *response = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
        [CustomLoading DismissAlertMessage];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        int success = [[result objectForKey:@"success"] intValue];
        NSDictionary *profile = [result objectForKey:@"profile"];
        
        if(success == 1) {
            [self.navigationController popViewControllerAnimated:YES];
            [self  SetProfileObject:profile];
            [self setprofileData];
        }
        
    }
    
- (void)requestFailed:(ASIHTTPRequest *)theRequest {
    
    NSString *response = [[NSString alloc] initWithData:[theRequest responseData] encoding:NSUTF8StringEncoding];
    [CustomLoading DismissAlertMessage];
}
    
-(void) setUserProfileImage {
    
    ProfilePic.layer.cornerRadius = ProfilePic.frame.size.width / 2;
    
    for (UIView* subview in ProfilePic.subviews)
    subview.layer.cornerRadius = ProfilePic.frame.size.width / 2;
    
    ProfilePic.layer.shadowColor = [UIColor blackColor].CGColor;
    ProfilePic.layer.shadowOpacity = 0.7f;
    ProfilePic.layer.shadowOffset = CGSizeMake(0, 5);
    ProfilePic.layer.shadowRadius = 5.0f;
    ProfilePic.layer.masksToBounds = NO;
    
    ProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    ProfilePic.layer.borderWidth = 3.0f;
    
    ProfilePic.layer.cornerRadius = ProfilePic.frame.size.width / 2;
    
    for (UIView* subview in editprofilepic.subviews)
    subview.layer.cornerRadius = editprofilepic.frame.size.width / 2;
    
    editprofilepic.layer.shadowColor = [UIColor blackColor].CGColor;
    editprofilepic.layer.shadowOpacity = 0.7f;
    editprofilepic.layer.shadowOffset = CGSizeMake(0, 5);
    editprofilepic.layer.shadowRadius = 5.0f;
    editprofilepic.layer.masksToBounds = NO;
    
    
    NSURL *url1 = [NSURL URLWithString:ProfileObj.profile_image];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    UIImage *img1 = [[UIImage alloc] initWithData:data1];
    CGSize size1 = img1.size;
    
    
    ProfilePic.layer.cornerRadius = ProfilePic.frame.size.width / 2;
    ProfilePic.layer.masksToBounds = NO;
    ProfilePic.clipsToBounds = YES;
    
    ProfilePic.layer.backgroundColor = [UIColor clearColor].CGColor;
    ProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    ProfilePic.layer.borderWidth = 3.0f;
    
    editprofilepic.layer.cornerRadius = ProfilePic.frame.size.width / 2;
    editprofilepic.layer.masksToBounds = NO;
    editprofilepic.clipsToBounds = YES;
    
}
    
#pragma mark ----------------------
#pragma mark TableView Data Source
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float returnValue;
    if (IS_IPAD)
    returnValue = 350.0f;
    else
    returnValue = 230.0f;
    
    return returnValue;
}
    
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
    
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 10;
}
    
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    HomeCell *cell;
    
    if (IS_IPAD) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeCell_iPad" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else{
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    return cell;
}
    
#pragma mark - TableView Delegates
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
    
    
-(void)rel{
    
    dropDown = nil;
}
    
- (IBAction)GenderSelect:(id)sender {
    self.genderPicker.hidden = NO;
    dobView.hidden =  YES;
    toolBar.hidden = NO;
}
    
- (IBAction)countrySelec:(id)sender {
    self.countryPicker.hidden = NO;
    dobView.hidden =  YES;
    toolBarC.hidden = NO;
}
- (IBAction)EditProfilePressed:(id)sender {
    beamsView.hidden = YES;
    saveProfile.hidden = NO;
    LivesInField.hidden = NO;
    workingAtField.hidden = NO;
    StudiedInField.hidden = NO;
    mobileEditField.hidden = NO;
    txtgender.hidden = NO;
    birhdaybtn.hidden = NO;
    _usernamefld.hidden = NO;
    _EditProfileBtn.hidden = YES;
    StudiIn.text = @"Studied at";
    lblWorkingat.text = @"Working at";
    location.text = NSLocalizedString(@"country", @"");
    _usernameLbl.text = NSLocalizedString(@"name", @"");
    mobileLabel.text = @"";
    
}
    
- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    
    if(sender.selectedIndex == 0) {
        [txtgender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        if(isMale) {
            isMale = false;
            isFemale = true;
            
        }
        else {
            isMale = true;
            isFemale = false;
            
        }
        gender.text = @"MALE";
        strgender = @"MALE";
        [txtgender setTitle:@"MALE" forState:UIControlStateNormal];
    }
    else if (sender.selectedIndex == 1) {
        [txtgender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        if(isFemale) {
            isMale = true;
            isFemale = false;
            
        }
        else {
            isMale = false;
            isFemale = true;
            
        }
        gender.text = @"FEMALE";
        strgender = @"FEMALE";
        
    }
    
    [self rel];
}
-(IBAction)closegenderView:(id)sender
    {
        GenderView.hidden = true;
    }
    
-(IBAction)onRadioBtn:(RadioButton*)sender
    {
        genderBtn.titleLabel.text = sender.titleLabel.text;
        gender.text = sender.titleLabel.text;
        [genderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
#pragma mark countries picker delegate
- (IBAction)countryPressed:(id)sender {
    
    [self.view endEditing:YES];
    countriesView.hidden = false;
    countryPicker.hidden = false;
    countriesView.layer.borderColor = [[UIColor darkGrayColor]CGColor];
    countriesView.layer.borderWidth = 1.0f;
    
    UIButton *DateSelected = [UIButton buttonWithType:UIButtonTypeCustom];
    [DateSelected setFrame:CGRectMake(countryPicker.frame.origin.x+30, countryPicker.frame.origin.y+countryPicker.frame.size.height+220, 80, 35)];
    if(IS_IPAD) {
        [DateSelected setFrame:CGRectMake(countryPicker.frame.origin.x+30, countryPicker.frame.origin.y+countryPicker.frame.size.height+350, 120, 50)];
    }
    [DateSelected setBackgroundImage:[UIImage imageNamed:@"redbar.png"] forState:UIControlStateNormal];
    [DateSelected setTitle:NSLocalizedString(@"save", @"") forState:UIControlStateNormal];
    [DateSelected.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [DateSelected setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [DateSelected addTarget:self action:@selector(countrySelected:) forControlEvents:UIControlEventTouchUpInside];
    [countriesView addSubview:DateSelected];
    
    UIButton *CancelSelected = [UIButton buttonWithType:UIButtonTypeCustom];
    [CancelSelected setFrame:CGRectMake(DateSelected.frame.origin.x+DateSelected.frame.size.width+160, DateSelected.frame.origin.y, 80, 35)];
    if(IS_IPAD) {
        [CancelSelected setFrame:CGRectMake(DateSelected.frame.origin.x+DateSelected.frame.size.width+135, DateSelected.frame.origin.y, 120, 50)];
    }
    [CancelSelected setBackgroundImage:[UIImage imageNamed:@"redbar.png"] forState:UIControlStateNormal];
    [CancelSelected setTitle:NSLocalizedString(@"cancel", @"") forState:UIControlStateNormal];
    [CancelSelected.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [CancelSelected setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [CancelSelected addTarget:self action:@selector(countrySelectionCancelled:) forControlEvents:UIControlEventTouchUpInside];
    [countriesView addSubview:CancelSelected];
    
}
    
- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
    {
        LivesInField.text = name;
        txtCountry.text = name;
    }
    
-(void)countrySelected:(id)sender{
    
    countriesView.hidden = true;
    countryPicker.hidden = true;
    countriesView.layer.borderColor = [[UIColor darkGrayColor]CGColor];
    countriesView.layer.borderWidth = 1.0f;
}
-(void)countrySelectionCancelled:(id)sender{
    countriesView.hidden = true;
    countryPicker.hidden = true;
    
}
    
#pragma mark ---------------
#pragma mark Date Of Birth
- (IBAction)SelectDateOfBirth:(id)sender {
    [self.view endEditing:YES];
    dobPicker.hidden = NO;
    dobView.hidden = NO;
    dobView.layer.borderColor = [[UIColor grayColor]CGColor];
    dobView.layer.borderWidth = 1.0f;
    
    overlayView.hidden = false;
    
    [dobPicker setMaximumDate:[self getCurrentDate]];
    UIButton *DateSelected = [UIButton buttonWithType:UIButtonTypeCustom];
    [DateSelected setFrame:CGRectMake(dobView.frame.origin.x+70, dobView.frame.size.height-65, 80, 35)];
    
    if(IS_IPAD) {
        [dobView setFrame:CGRectMake(90, 300, 600, 500)];
        [GenderView setFrame:CGRectMake(90, 300, 600, 300)];
        [DateSelected setFrame:CGRectMake(dobView.frame.origin.x+80, dobView.frame.size.height-60, 120, 50)];
    }
    
    [DateSelected setBackgroundImage:[UIImage imageNamed:@"blueradio.png"] forState:UIControlStateNormal];
    [DateSelected setTitle:NSLocalizedString(@"save", @"") forState:UIControlStateNormal];
    [DateSelected.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [DateSelected setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [DateSelected addTarget:self action:@selector(dateSelected:) forControlEvents:UIControlEventTouchUpInside];
    [dobView addSubview:DateSelected];
    
    UIButton *CancelSelected = [UIButton buttonWithType:UIButtonTypeCustom];
    [CancelSelected setFrame:CGRectMake(DateSelected.frame.origin.x + 150, DateSelected.frame.origin.y, 80, 35)];
    
    if(IS_IPAD) {
        [CancelSelected setFrame:CGRectMake(DateSelected.frame.origin.x+DateSelected.frame.size.width+140, DateSelected.frame.origin.y, 120, 50)];
    }
    [CancelSelected setBackgroundImage:[UIImage imageNamed:@"blueradio.png"] forState:UIControlStateNormal];
    [CancelSelected setTitle:NSLocalizedString(@"cancel", @"") forState:UIControlStateNormal];
    [CancelSelected.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [CancelSelected setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [CancelSelected addTarget:self action:@selector(dateSelectionCancelled:) forControlEvents:UIControlEventTouchUpInside];
    [dobView addSubview:CancelSelected];
}
-(void)dateSelectionCancelled:(id)sender{
    dobPicker.hidden = true;
    dobView.hidden = true;
    
    overlayView.hidden = true;
}
-(void)dateSelected:(id)sender{
    
    /// IGNORING TIME COMPONENT
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSDate *dateInOldFormat = [dobPicker date];
    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:flags fromDate:dateInOldFormat];
    NSDate* dateOnly = [calendar dateFromComponents:components];
    
    /// CHANGING FORMAT
    
    NSString *_outputFormat = @"yyyy-MM-dd";
    //[dateFormatter setDateFormat:@"YYYY-M-d"];
    [dateFormatter setDateFormat:_outputFormat];
    
    NSString *dateInNewFormat = [dateFormatter stringFromDate:dateOnly];
    
    NSDate *date = [dateFormatter dateFromString:dateInNewFormat] ;
//    NSLog(@"date=%@",date) ;
    NSTimeInterval interval  = [date timeIntervalSince1970] ;
//    NSLog(@"interval=%f",interval) ;
    NSDate *methodStart = [NSDate dateWithTimeIntervalSince1970:interval] ;
    [dateFormatter setDateFormat:@"yyyy-MM-dd "] ;
//    NSLog(@"result: %@", [dateFormatter stringFromDate:methodStart]) ;
    finalDate = [dateFormatter stringFromDate:methodStart];
    [lblBirthday setText:finalDate];
    [txtBirthday setText:finalDate];
//    NSLog(@"%@",txtBirthday.text);
    dobPicker.hidden = true;
    dobView.hidden = true;
    overlayView.hidden = true;
}
    
-(NSDate *)getCurrentDate{
    
    return [NSDate date];
    
}
    
#pragma mark TextFields delegate
    
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    //    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //
    //    UITouch *touch = [[event allTouches] anyObject];
    //    if ([txtName isFirstResponder] && [touch view] != txtName) {
    //        [txtName resignFirstResponder];
    //
    //    }
    //    else if ([txtCity isFirstResponder] && [touch view] != txtCity) {
    //
    //        [txtCity resignFirstResponder];
    //
    //    }  else if ([txtCountry isFirstResponder] && [touch view] != txtCountry) {
    //
    //        [txtCountry resignFirstResponder];
    //
    //    }  [super touchesBegan:touches withEvent:event];
    //    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}
    
    
- (void)textFieldDidBeginEditing:(UITextField *)textField
    {
        [self animateTextField: textField up: YES];
    }
    
- (void)textFieldDidEndEditing:(UITextField *)textField
    {
        [self animateTextField: textField up: NO];
    }
    
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
    
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
    {
        //    const int movementDistance = 145; // tweak as needed
        //    const float movementDuration = 0.3f; // tweak as needed
        //
        //    int movement = (up ? -movementDistance : movementDistance);
        //
        //    [UIView beginAnimations: @"anim" context: nil];
        //    [UIView setAnimationBeginsFromCurrentState: YES];
        //    [UIView setAnimationDuration: movementDuration];
        //    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        //    [UIView commitAnimations];
    }

#pragma mark TextView Delegates
-(void)textViewDidBeginEditing:(UITextView *)textView {
//    if([textView.text isEqualToString:@"Write something about yourself"]){
//        textView.textColor = [UIColor blackColor];
//        textView.text=nil;
//    }
}
-(void)textViewDidEndEditing:(UITextView *)textView {
//    if([textView.text isEqual:@""]){
//        textView.textColor = [UIColor lightGrayColor];
//        [textView setText:@"Write something about yourself"];
//    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _usernamefld)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    return YES;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    const char * _char = [text cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char, "\b");
    if (isBackSpace == -8) {
        return YES;
    }else if([[textView text] length] > 100){
        return NO;
    }
    
    return YES;
}
    
- (IBAction)openDrawer:(id)sender {
    
    
    //    CGSize size = self.view.frame.size;
    //
    //    if(self.isMenuVisible) {
    //        self.isMenuVisible = false;
    //        [overlayView removeFromSuperview];
    //        [UIView animateWithDuration:0.5 animations:^{
    //            self.view.frame = CGRectMake(0, 0, size.width, size.height);
    //        }];
    //    }
    //    else {
    //        [UIView animateWithDuration:0.5 animations:^{
    //            self.view.frame = CGRectMake(236, 0, size.width, size.height);
    //        }];
    //        self.isMenuVisible = true;
    //        CGRect screenRect = [[UIScreen mainScreen] bounds];
    //        overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 43, screenRect.size.width, screenRect.size.height)];
    //        overlayView.backgroundColor = [UIColor clearColor];
    //
    //        [self.view addSubview:overlayView];
    //
    //        UISwipeGestureRecognizer* sgr = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipe:)];
    //        [sgr setDirection:UISwipeGestureRecognizerDirectionLeft];
    //        [overlayView addGestureRecognizer:sgr];
    //    }
    
    [self.navigationController popViewControllerAnimated:YES];
    //    [[DrawerVC getInstance] AddInView:self.view];
    //    [[DrawerVC getInstance] ShowInView];
}
    
- (void)leftSwipe:(UISwipeGestureRecognizer *)gesture
    {
        if(self.isMenuVisible){
            [self openDrawer:nil];
        }
    }
    
- (IBAction)editProfile:(id)sender {
    
    [self setUserProfileImage];
    
    [self.view addSubview:editProfileView];
}
    
- (IBAction)EditPic:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    [self setUserProfileImage];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    ProfilePic.image = chosenImage;
    editprofilepic.image = chosenImage;
    //   isFile = true;
    // popUpView.hidden = true;
    overlayView.hidden = true;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
    
- (IBAction)saveProfile:(id)sender {
    
    //    GenderView.hidden = YES;
    //    beamsView.hidden = NO;
    //    saveProfile.hidden = YES;
    //    LivesInField.hidden = YES;
    //    workingAtField.hidden = YES;
    //    StudiedInField.hidden = YES;
    //    mobileEditField.hidden = YES;
    //    _EditProfileBtn.hidden = YES;
    //    _usernamefld.hidden = YES;
    //    txtgender.hidden = YES;
    //    birhdaybtn.hidden = YES;
    
    if(self.usernamefld.text.length > 0){
        [self updateProfile];
    }
    else{
        [Utils showAlert:NSLocalizedString(@"Name field is mandatory", @"")] ;
         }
}
         @end
