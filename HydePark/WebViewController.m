//
//  WebViewController.m
//  HydePark
//
//  Created by Babar Hassan on 10/13/17.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "WebViewController.h"
#import "CustomLoading.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURL *url = [NSURL URLWithString:self.urlString];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    NSString *screenWidth = [NSString stringWithFormat:@"%f",_webView.frame.size.width];
    
    NSString *screenHeight = [NSString stringWithFormat:@"%f",_webView.frame.size.height];
    
    NSString *htmlString =[NSString stringWithFormat:@"<iframe width=\"%@\" height=\"%@\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"%@\"]></iframe>",screenWidth,screenHeight,_urlString] ;
    
    //Load the request in the UIWebView.
    //[_webView loadRequest:requestObj];
    [_webView loadHTMLString:htmlString baseURL:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - webView delegates

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    
    [CustomLoading showAlertMessage];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{

    [CustomLoading DismissAlertMessage];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{

    [CustomLoading DismissAlertMessage];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
