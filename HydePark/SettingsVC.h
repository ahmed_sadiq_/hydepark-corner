//
//  SettingsVC.h
//  HydePark
//
//  Created by Osama on 27/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "DataContainer.h"
#import <MessageUI/MessageUI.h>
#import <AVFoundation/AVFoundation.h>
#import <Accounts/Accounts.h>
@interface SettingsVC : UIViewController<FBSDKSharingDelegate,UITableViewDataSource,UITableViewDelegate>{
    DataContainer *sharedManager;
    int sharingstate;
    BOOL isSearch;
    MFMessageComposeViewController *controller;
    IBOutlet UISwitch *nightSwitch;
    IBOutlet UIView *nightBGView;
    IBOutlet UIView *nightBGLowerView;
    IBOutlet UIView *topLabelBG;
    IBOutlet UIView *lowerLabelBG;
    IBOutlet UITableView *tableView;
    NSMutableArray *inviteOptionsArray;
    NSMutableArray *profileOptionsArray;

}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *fbSharebtn;
@property (weak, nonatomic) IBOutlet UIView *inviteListView;
@property (weak, nonatomic) IBOutlet UITableView *inviteListTblView;
@property (weak, nonatomic) UITableView *searchTblView;


@property (strong, nonatomic) NSMutableArray *usersText;
@property (strong, nonatomic) NSMutableArray *users;
@property (strong, nonatomic) NSMutableArray *searchResult;

@property (weak, nonatomic) IBOutlet UIView *analyticsView;
@property (weak, nonatomic) IBOutlet UIView *signOutView;
@end
