//
//  ChangePasswordVC.m
//  HydePark
//
//  Created by Osama on 04/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "ChangePasswordVC.h"
#import "UITextField+Style.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Utils.h"
#import "CustomLoading.h"
@interface ChangePasswordVC ()
{
    UIGestureRecognizer *tapper;
    
}
@end

@implementation ChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    // border
    [self.baseview.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.baseview.layer setBorderWidth:1.5f];
    
    // drop shadow
    [self.baseview.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.baseview.layer setShadowOpacity:0.8];
    //[self.baseview.layer setShadowRadius:3.0];
    [self.baseview.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    self.oldPasswordField.placeholder = NSLocalizedString(@"old_password", @"");
    self.RepeatPasswordField.placeholder = NSLocalizedString(@"new_password_error", @"");
    self.PasswordField.placeholder = NSLocalizedString(@"new_password", @"");
    [self setTextfieldPading:self.view];
}
- (void)setTextfieldPading:(UIView *)view {
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    // Return if there are no subviews
    if ([subviews count] == 0) return; // COUNT CHECK LINE
    for (UIView *subview in subviews) {
        // Do what you want to do with the subview
        if ([subview isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField *)subview;
            [textField setLeftSpaceSize:15];
            [textField setRightSpaceSize:15];
        }
        // List the subviews of subview
        [self setTextfieldPading:subview];
    }
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)SavePressed:(id)sender{
    if(self.PasswordField.text.length < 1 || self.oldPasswordField.text.length < 1 || self.RepeatPasswordField.text.length < 1){
        [Utils showAlert:NSLocalizedString(@"allfieldsaremandaotry", @"")];
    }
    else if (![self.PasswordField.text isEqualToString:self.RepeatPasswordField.text])
    {
        [Utils showAlert:NSLocalizedString(@"password_not_match", @"")];
    }
    else{
        [CustomLoading showAlertMessage];
        [self serverCall];
    }
    
}

#pragma mark ServerCall
-(void)serverCall{
    NSString *language = [Utils getLanguageCode];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"resetPassword",@"method",
                              token,@"Session_token",self.oldPasswordField.text,@"old_password",self.PasswordField.text,@"new_password",language,@"language",nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [CustomLoading DismissAlertMessage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        if(success){
            [Utils showAlert:NSLocalizedString(@"passwordChanged", @"")];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            NSString *message = [responseObject objectForKey:@"message"];
            [Utils showAlert:message];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        [CustomLoading DismissAlertMessage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

-(IBAction)backPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
