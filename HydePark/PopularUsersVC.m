//
//  PopularUsersVC.m
//  HydePark
//
//  Created by Mr on 24/06/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "PopularUsersVC.h"
#import "Constants.h"
#import "SearchCell.h"
#import "NavigationHandler.h"
#import "Utils.h"
#import "SVProgressHUD.h"
#import "PopularUsersModel.h"
#import "AsyncImageView.h"
#import "UIImageView+RoundImage.h"
#import "UserChannel.h"
#import "Flurry.h"
#import "Followings.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "NSData+AES.h"

@interface PopularUsersVC (){
    UIRefreshControl *refreshControl;
}
    
@property (weak, nonatomic) IBOutlet UILabel *headerlbl;
@end

@implementation PopularUsersVC
//who to follow
- (id)init
{
    if (IS_IPAD) {
        self = [super initWithNibName:@"PopularUsersVC_iPad" bundle:Nil];
    }else if(IS_IPHONE_6 || IS_IPHONE_5 || IS_IPHONE_4){
        
        self = [super initWithNibName:@"PopularUsersVC_iPhone6" bundle:Nil];
    }
    else if (IS_IPHONE_6Plus){
        self = [super initWithNibName:@"PopularUsersVC" bundle:Nil];
    }

    return self;
}

- (void)viewWillAppear {
    [super viewWillAppear:YES];
    
    serverCall = false;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //[Flurry logEvent:@"HPC_FAMOUS_CORNERS"];
    [self setRefresh];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.PopularUserTbl.delegate = self;
    self.PopularUserTbl.dataSource = self;
    PopUsers = [[PopularUsersModel alloc]init];
    _popAr   = [[NSMutableArray alloc] init ];
    pageNum = 1;
    sharedManager = [DataContainer sharedManager];
    [searchField setValue:[UIColor whiteColor]
               forKeyPath:@"_placeholderLabel.textColor"];
    self.PopularUserTbl.backgroundColor = [UIColor clearColor];
    self.PopularUserTbl.opaque = NO;
    sharedManager.whoToFollow  = [[NSMutableArray alloc] init];
//    if(sharedManager.famousUsersFetchedFirst){
    sharedManager.whoToFollowPageNum = 1;
        [self getFamousUsers];
//    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.background.hidden = YES;
        }
        else
        {
            self.background.hidden = NO;
        }
    }
    else
    {
        self.background.hidden = NO;
    }
    
    self.headerlbl.text = NSLocalizedString(@"people_you_may_know", @"");
//    if (IS_IPHONE_5 || IS_IPHONE_6) {
//        self.view.frame = CGRectMake(0, 0, 375, 667);
//        self.PopularUserTbl.frame = CGRectMake(0, 50, 375, 620);
//        
//    }
//    else if(IS_IPHONE_6Plus)
//    {
//        self.PopularUserTbl.frame = CGRectMake(0, 55, 414, 685);
//    }
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    /*NSMutableIndexSet *discardedItems = [NSMutableIndexSet indexSet];
    Followings *item;
    NSUInteger index = 0;
    
    for (item in sharedManager.whoToFollow) {
        if ([item.status isEqualToString:@"FRIEND"])
            [discardedItems addIndex:index];
        index++;
    }
    if(index > 0){
        [sharedManager.whoToFollow removeObjectsAtIndexes:discardedItems];
        
    }*/
    
    [self.PopularUserTbl reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setRefresh{
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshCall)
             forControlEvents:UIControlEventValueChanged];
    //[self.PopularUserTbl addSubview:refreshControl];
}
#pragma mark Refresh Control
- (void)refreshCall
{
    sharedManager.whoToFollowPageNum = 1;
    [self getFamousUsers];
}
- (void) getFamousUsers{
    serverCall = TRUE;
//    [SVProgressHUD show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    
    NSString *pageStr = [NSString stringWithFormat:@"%d",sharedManager.whoToFollowPageNum];
    
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_FAMOUS_USERS,@"method",
                              token,@"session_token",pageStr,@"page_no",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
//            [SVProgressHUD dismiss];
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            serverCall = FALSE;
            if(success == 1) {
                sharedManager.famousUsersFetchedFirst = NO;
                usersArray = [result objectForKey:@"users"];
                
                if([usersArray isKindOfClass:[NSArray class]])
                {
                    if(sharedManager.whoToFollowPageNum == 1) {
                        sharedManager.whoToFollow  = [[NSMutableArray alloc] init];
                    }
                    sharedManager.whoToFollowPreviousCount = (int)sharedManager.whoToFollow.count;
                    for(NSDictionary *tempDict in usersArray){
                        Followings *_responseData = [[Followings alloc] init];
                        
                        _responseData.f_id = [tempDict objectForKey:@"id"];
                        _responseData.fullName = [tempDict objectForKey:@"full_name"];
                        _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                        _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                        _responseData.status = [tempDict objectForKey:@"state"];
                        if(![_responseData.status isEqualToString:@"FRIEND"] || ![_responseData.status isEqualToString:@"ACCEPT_REQUEST"] || ![_responseData.status isEqualToString:@"PENDING"]){
                            [sharedManager.whoToFollow addObject:_responseData];
                        }
                        /*if([_responseData.status isEqualToString:@"ADD_FRIEND"])
                            [sharedManager.whoToFollow addObject:_responseData];*/
                        
                    }
                    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                    int startIndex = sharedManager.whoToFollowPreviousCount;
                    int lastIndex = (int)(sharedManager.whoToFollow.count - sharedManager.whoToFollowPreviousCount);
                    for (int i = startIndex ; i < startIndex+lastIndex; i++) {
                        if(i<sharedManager.whoToFollow.count) {
                            [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                        }
                    }
//                    [self.PopularUserTbl beginUpdates];
//                    [self.PopularUserTbl insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
//                    [self.PopularUserTbl endUpdates];
                    [self.PopularUserTbl reloadData];
                }
                else {
                    cannotScroll = true;
                }
                
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            }
        }else{
//            [SVProgressHUD dismiss];
            serverCall = FALSE;
        }
    }];
}

#pragma mark ----------------------
#pragma mark TableView Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float returnValue;
    if (IS_IPAD)
        returnValue = 121.0f;
    else
        returnValue = 83.0f;
    
    return returnValue;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [sharedManager.whoToFollow  count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    SearchCell *cell;
    float returnValue = 83;
    if(IS_IPAD)
        returnValue = 124;
    self.PopularUserTbl.contentSize = CGSizeMake(self.PopularUserTbl.frame.size.width,sharedManager.whoToFollow.count * returnValue );
    if (IS_IPAD) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchCell_iPad" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else{
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            cell.lowerSeparator.hidden = NO;
        }
        else
        {
            cell.lowerSeparator.hidden = YES;
        }
    }
    else
    {
        cell.lowerSeparator.hidden = YES;
    }
    
    Followings *tempUsers = [[Followings alloc]init];
    tempUsers  = [sharedManager.whoToFollow  objectAtIndex:indexPath.row];
    
    
    cell.friendsName.text = [Utils getDecryptedTextFor:tempUsers.fullName];

    [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:tempUsers.profile_link] placeholderImage:[UIImage imageNamed:@"loading"]];

    [cell.profilePic roundImageCorner];
    
    cell.profilePic.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.profilePic.layer.shadowOpacity = 0.7f;
    cell.profilePic.layer.shadowOffset = CGSizeMake(0, 5);
    cell.profilePic.layer.shadowRadius = 5.0f;
    cell.profilePic.layer.masksToBounds = YES;
    
    cell.profilePic.layer.backgroundColor = [UIColor clearColor].CGColor;
    cell.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.profilePic.layer.borderWidth = 3.0f;
    
    cell.statusImage.hidden = false;
    cell.activityInd.hidden = true;
    [cell.activityInd stopAnimating];
    [[cell.statusImage layer] setBorderWidth:2.0f];
    [[cell.statusImage layer] setBorderColor:[UIColor colorWithRed:64.0/255.0f green:79.0/255.0f blue:138.0/255.0f alpha:1.0].CGColor];
    [[cell.rejectBtn layer] setBorderWidth:2.0f];
    [[cell.rejectBtn layer] setBorderColor:[UIColor colorWithRed:64.0/255.0f green:79.0/255.0f blue:138.0/255.0f alpha:1.0].CGColor];
    [cell.statusImage addTarget:self action:@selector(statusPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.statusImage setTag:indexPath.row];
    [cell.rejectBtn addTarget:self action:@selector(rejectRequest:) forControlEvents:UIControlEventTouchUpInside];
    [cell.rejectBtn setTag:indexPath.row];
    [cell.friendsChannelBtn addTarget:self action:@selector(OpenFriendsChannelPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.friendsChannelBtn setTag:indexPath.row];
    cell.rejectBtn.hidden = YES;
    
    if ([tempUsers.status isEqualToString:@"ADD_FRIEND"] && [tempUsers.is_celeb isEqualToString:@"1"]) {
        [cell.statusImage setTitle:NSLocalizedString(@"follow", @"") forState:UIControlStateNormal];
    }
    else if ([tempUsers.status isEqualToString:@"FRIEND"] && [tempUsers.is_celeb isEqualToString:@"1"]){
        [cell.statusImage setTitle:NSLocalizedString(@"following", @"") forState:UIControlStateNormal];
    }
    else if ([tempUsers.status isEqualToString:@"FRIEND"] && [tempUsers.is_celeb isEqualToString:@"0"]){
        [cell.statusImage setTitle:NSLocalizedString(@"friends", @"") forState:UIControlStateNormal];
    }
    else if([tempUsers.status isEqualToString:@"ADD_FRIEND"] && [tempUsers.is_celeb isEqualToString:@"0"]){
        [cell.statusImage setTitle:NSLocalizedString(@"add_friend", @"") forState:UIControlStateNormal];
    }
    else if([tempUsers.status isEqualToString:@"PENDING"]){
        [cell.statusImage setTitle:NSLocalizedString(@"pending", @"") forState:UIControlStateNormal];
    }
    else if([tempUsers.status isEqualToString:@"ACCEPT_REQUEST"]){
        [cell.statusImage setTitle:NSLocalizedString(@"Respond", @"") forState:UIControlStateNormal];
        //cell.statusImage.frame = CGRectMake(cell.statusImage.frame.origin.x, cell.statusImage.frame.origin.y - 20, cell.statusImage.frame.size.width, cell.statusImage.frame.size.height);
        cell.rejectBtn.hidden = YES;
    }


    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void) OpenFriendsChannelPressed:(UIButton *)sender{
    UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    Followings *tempUsers  = [sharedManager.whoToFollow  objectAtIndex:currentSelectedIndex];
    friendId = tempUsers.f_id;
    UserChannel *commentController;
    if(IS_IPAD){
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
    }else if (IS_IPHONEX){
                commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
    }
    else
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
    commentController.ChannelObj = nil;
    commentController.friendID   = friendId;
    commentController.currentUser = tempUsers;
    [[self navigationController] pushViewController:commentController animated:YES];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    NSArray *visibleRows = [self.PopularUserTbl visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    NSIndexPath *path = [self.PopularUserTbl indexPathForCell:lastVisibleCell];
    if(path.section == 0 && path.row == sharedManager.whoToFollow.count -1)
    {
        if(!cannotScroll && !serverCall) {
            if(goSearch) {
                searchPageNum++;
            }
            else {
                sharedManager.whoToFollowPageNum++;
                [self getFamousUsers];
            }
        }
        
    }
}

#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}
- (void)statusPressed:(UIButton *)sender{
    UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    Followings *PopUser = [[Followings alloc] init];
    PopUser  = [sharedManager.whoToFollow objectAtIndex:currentSelectedIndex];
    friendId = PopUser.f_id;
//    [statusBtn setBackgroundImage:[UIImage imageNamed:@"follow.png"] forState:UIControlStateNormal];
    if ([PopUser.status isEqualToString:@"ADD_FRIEND"]) {
        if([PopUser.is_celeb isEqualToString:@"0"])
            PopUser.status = @"PENDING";
        else{
            PopUser.status = @"FRIEND";
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [self.PopularUserTbl reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        
        [statusBtn setBackgroundImage:[UIImage imageNamed:@"follow.png"] forState:UIControlStateNormal];
        [self sendFriendRequest:PopUser];
        
    }
    else if([PopUser.status isEqualToString:@"FRIEND"]){
        PopUser.status = @"ADD_FRIEND";
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [self.PopularUserTbl reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        
        [statusBtn setBackgroundImage:[UIImage imageNamed:@"unfollow.png"] forState:UIControlStateNormal];
        [self sendCancelRequest];
    }
    else if([PopUser.status isEqualToString:@"ACCEPT_REQUEST"]){
        PopUser.status = @"FRIEND";
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [self.PopularUserTbl reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        
        [statusBtn setBackgroundImage:[UIImage imageNamed:@"unfollow.png"] forState:UIControlStateNormal];
        [self acceptRequest];
    }
    else if([PopUser.status isEqualToString:@"PENDING"]){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
        [self.PopularUserTbl reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        
        [statusBtn setBackgroundImage:[UIImage imageNamed:@"unfollow.png"] forState:UIControlStateNormal];
        [self rejectRequest:sender];
    }
}
- (void)acceptRequest{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    Followings *PopUser = [[Followings alloc]init];
    PopUser  = [sharedManager.whoToFollow  objectAtIndex:currentSelectedIndex];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_ACCEPT_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                //PopUser.status = @"FRIEND";
               // [sharedManager.friends addObject:PopUser];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
                NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                [self.PopularUserTbl reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];

//                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"f_id != %@", _responseData.f_id];
//                [sharedManager.followings filterUsingPredicate:predicate];
            }
        }else{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
    
}
- (void)rejectRequest:(UIButton *)sender{
    UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    
    Followings *PopUser = [[Followings alloc]init];
    PopUser  = [sharedManager.whoToFollow  objectAtIndex:currentSelectedIndex];
    friendId = PopUser.f_id;

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_REJECT_REEQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                PopUser.status = @"ADD_FRIEND";
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
                NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                [self.PopularUserTbl reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
//                _responseData.status =   @"FRIEND";
//                [sharedManager.followings addObject:_responseData];
//                _msgButton.enabled=YES;
            }
        }else{
           
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
}
- (void) sendCancelRequest{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    PopularUsersModel *PopUser = [[PopularUsersModel alloc]init];
    PopUser  = [sharedManager.whoToFollow  objectAtIndex:currentSelectedIndex];
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_DELETE_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
//        [SVProgressHUD dismiss];
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            if(success == 1) {
//                PopUser.status = @"ADD_FRIEND";
                if(sharedManager.followers.count > 0){
                    NSInteger index = [sharedManager.followers indexOfObject:PopUser];
                    if (index != NSNotFound) {
                        [sharedManager.followers  removeObjectAtIndex:index];
                    }
                }
                
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
                NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                [self.PopularUserTbl reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            }
        }else{
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
        }
    }];
    
}

- (void) sendFriendRequest : (Followings *) pUser{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SEND_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue]; 
            if(success == 1) {
                //[self getFamousUsers];
                //[self.PopularUserTbl reloadData];
                //[SVProgressHUD dismiss];
                [sharedManager.followers addObject:pUser];
                                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
                NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                [self.PopularUserTbl reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            }
        }else{
            pUser.status = @"ADD_FRIEND";
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
            NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
            [self.PopularUserTbl reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
        }
    }];
}

- (IBAction)back:(id)sender {
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"newSignup"])
    {
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"newSignup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NavigationHandler getInstance]NavigateToHomeScreen];
    }
    else{
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (IBAction)Searchbtn:(id)sender {
    
    if(searchField.text.length > 1) {
        
        goSearch = true;
        cannotScroll = false;
        searchPageNum = 1;
        NSString *pageNums = [NSString stringWithFormat:@"%d",searchPageNum];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSURL *url = [NSURL URLWithString:SERVER_URL];
        NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
        
        NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SEARCH_FRIEND,@"method",
                                  token,@"Session_token",pageNums,@"page_no",searchField.text,@"keyword", nil];
        
        NSData *postData = [Utils encodeDictionary:postDict];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                int success = [[result objectForKey:@"success"] intValue];
                
                if(success == 1) {
                    
                    usersArray = [result objectForKey:@"users_found"];
                    
                    if([usersArray isKindOfClass:[NSArray class]])
                    {
                        if(searchPageNum == 1) {
                            PopUsers.PopUsersArray = [[NSMutableArray alloc] init];
                            PopUsers.imagesArray = [[NSMutableArray alloc] init];
                        }
                        for(NSDictionary *tempDict in usersArray){
                            PopularUsersModel *_Popusers = [[PopularUsersModel alloc] init];
                            _Popusers.full_name = [tempDict objectForKey:@"full_name"];
                            _Popusers.friendID = [tempDict objectForKey:@"id"];
                            _Popusers.profile_link = [tempDict objectForKey:@"profile_link"];
                            _Popusers.profile_type = [tempDict objectForKey:@"profile_type"];
                            _Popusers.status = [tempDict objectForKey:@"state"];
                            
                            [PopUsers.imagesArray addObject:_Popusers.profile_link];
                            [PopUsers.PopUsersArray addObject:_Popusers];
                        }
                        [self.PopularUserTbl reloadData];
                    }
                    
                    
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                }
            }else{
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
            }
        }];
    }
    else {
        goSearch = false;
        pageNum = 1;
        
        [self getFamousUsers];
    }
    
}

#pragma - mark TextField Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
}




@end
