//
//  VideoViewCell.h
//  HydePark
//
//  Created by Babar Hassan on 10/4/17.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWProgressView.h"
#import "FLAnimatedImage.h"
#import <YYWebImage/YYWebImage.h>

@interface VideoViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *outerLine;

@property (weak, nonatomic) IBOutlet UIImageView *rebeam1;
@property (weak, nonatomic) IBOutlet UIImageView *listRebeam;

@property (weak, nonatomic) IBOutlet UIView *annonyOverlayLeft;

@property (weak, nonatomic)   IBOutlet UIImageView *leftClock;

@property (weak, nonatomic)   IBOutlet UILabel     *leftTimeStamp;

@property (weak, nonatomic) IBOutlet UIImageView *annonyImgLeft;


@property (weak, nonatomic)   IBOutlet UIView *imgContainer;
@property (strong, nonatomic) IBOutlet UIButton *CH_playVideo;
@property (strong, nonatomic) IBOutlet UIImageView *CH_profileImage;
@property (strong, nonatomic) IBOutlet UIButton *CH_heart;
@property (strong, nonatomic) IBOutlet UILabel *CH_heartCountlbl;
@property (strong, nonatomic) IBOutlet UIButton *CH_commentsBtn;
@property (strong, nonatomic) IBOutlet UILabel *CH_CommentscountLbl;
@property (strong, nonatomic) IBOutlet UILabel *CH_seen;
@property (strong, nonatomic) IBOutlet UIButton *CH_flag;
@property (strong, nonatomic) IBOutlet UIButton *btnOptions;
@property (strong, nonatomic) IBOutlet UILabel *CH_userName;
@property (strong, nonatomic) IBOutlet UILabel *CH_VideoTitle;
@property (strong, nonatomic) IBOutlet UILabel *CH_hashTags;
@property (strong, nonatomic) IBOutlet UILabel *durationLbl;
@property (strong, nonatomic) IBOutlet UIImageView *transthumb;
@property (strong, nonatomic) IBOutlet FLAnimatedImageView *CH_Video_Thumbnail;
@property (strong, nonatomic) IBOutlet UIImageView *mainThumbnail;
@property (strong, nonatomic) IBOutlet UILabel *Ch_videoLength;
@property (weak, nonatomic)   IBOutlet UIButton *userProfileView;
@property (weak, nonatomic)   IBOutlet UIActivityIndicatorView *activityInd;
@property (weak, nonatomic)   IBOutlet UIImageView *leftreplImg;
@property (weak, nonatomic)   IBOutlet UIImageView *dummyLeft1;
@property (weak, nonatomic)   IBOutlet UIImageView *dummyright1;
@property (weak, nonatomic)   IBOutlet UIImageView *dummyLeft2;
@property (weak, nonatomic)   IBOutlet UIImageView *dummyLeftmute;
@property (weak, nonatomic)   IBOutlet UIImageView *dummyright2;
@property (weak, nonatomic)   IBOutlet UIImageView *dummy1;
@property (weak, nonatomic)   IBOutlet UIImageView *dummy2;
@property (weak, nonatomic)   IBOutlet UIImageView *blueOverlay;
@property (weak, nonatomic)   IBOutlet UIImageView *playImg;
@property (weak, nonatomic)   IBOutlet UIImageView *dotsImg;
@property (weak, nonatomic) IBOutlet UIView *blackView;

@property (weak, nonatomic) id representedObject;
@property (strong, nonatomic) PWProgressView *cellProgress;



@end
