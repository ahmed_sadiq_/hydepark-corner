//
//  VideoModel.m
//  HydePark
//
//  Created by Apple on 19/02/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "VideoModel.h"

@implementation VideoModel
@synthesize title,comments_count,userName,topic_id,user_id,profile_image,like_count,seen_count,video_thumbnail_link,video_link,videoID,Tags,video_length,message_Thread,like_by_me,image_link,is_anonymous,reply_count,video_angle,parentId,current_datetime,uploaded_date,isMute,likesArray,privacy,beam_share_url,cdHydeparkModelObj,deep_link,isThumbnailReq,isViwedByMe,cpost_id,video_thumbnail_gif;
- (id)initWithDictionary:(NSDictionary *) responseData
{
    self = [super init];
    if (self) {
        self.title           = [responseData objectForKey:@"caption"];
        self.comments_count  = [responseData objectForKey:@"comment_count"];
        self.userName        = [responseData objectForKey:@"full_name"];
        self.topic_id        = [responseData objectForKey:@"topic_id"];
        self.user_id         = [responseData objectForKey:@"user_id"];
        self.profile_image   = [responseData objectForKey:@"profile_link"];
        self.like_count      = [responseData objectForKey:@"like_count"];
        self.like_by_me      = [responseData objectForKey:@"liked_by_me"];
        self.seen_count      = [responseData objectForKey:@"seen_count"];
        self.video_angle     = [[responseData objectForKey:@"video_angle"] intValue];
        self.beam_share_url  = [responseData objectForKey:@"beam_share_url"];
        self.deep_link       = [responseData objectForKey:@"deep_link"];
        
        self.video_link      = [responseData objectForKey:@"video_link"];
        self.m3u8_video_link = [responseData objectForKey:@"m3u8_video_link"];
        self.video_thumbnail_link = [responseData objectForKey:@"video_thumbnail_link"];
        self.videoID         = [responseData objectForKey:@"id"];
        self.Tags            = [responseData objectForKey:@"tag_friends"];
        self.video_length    = [responseData objectForKey:@"video_length"];
        self.is_anonymous    = [responseData objectForKey:@"is_anonymous"];
        self.reply_count     = [responseData objectForKey:@"reply_count"];
        self.current_datetime= [responseData objectForKey:@"current_datetime"];
        self.uploaded_date   = [responseData objectForKey:@"uploaded_date"];
        self.beam_share_url = [responseData objectForKey:@"beam_share_url"];
        self.isMute          = @"0";
        self.privacy         = [responseData objectForKey:@"privacy"];
        self.cpost_id         = [responseData objectForKey:@"post_id"];
        self.isThumbnailReq =[[responseData objectForKey:@"is_thumbnail_required"] intValue];
        self.beamType = [[responseData objectForKey:@"beam_type"] intValue];
        if([responseData objectForKey:@"original_beam_data"]){
            self.originalBeamData = [responseData objectForKey:@"original_beam_data"];
        }
        self.likesArray      = [[NSMutableArray alloc] init];
        self.isCelebrity = [responseData objectForKey:@"is_celeb"];
        self.isViwedByMe = [responseData objectForKey:@"viewed_by_me"];
        self.video_thumbnail_gif = [responseData objectForKey:@"video_thumbnail_gif"];
        self.viewed_by_me = [responseData objectForKey:@"viewed_by_me"];
        self.post_seen_time = [responseData objectForKey:@"post_seen_time"];
        self.order_timestamp = [responseData objectForKey:@"order_timestamp"];
        
    }
    return self;
}



@end
