//
//  PeopleYouMayKnowCollectionViewCell.h
//  HydePark
//
//  Created by apple on 6/22/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeopleYouMayKnowCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblViewMore;
@property (weak, nonatomic) IBOutlet UIImageView *personImage;


@end
