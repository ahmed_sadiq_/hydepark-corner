//
//  HashTagTableViewCell.h
//  HydePark
//
//  Created by apple on 6/30/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HashTagTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *hashTagCollectionView;

@end
