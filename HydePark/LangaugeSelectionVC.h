//
//  LangaugeSelectionVC.h
//  HydePark
//
//  Created by Osama on 16/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LangaugeSelectionVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lang1;
@property (weak, nonatomic) IBOutlet UILabel *lang2;
@property (weak, nonatomic) IBOutlet UILabel *lang3;
@property (weak, nonatomic) IBOutlet UILabel *lang4;
@property (weak, nonatomic) IBOutlet UILabel *lang5;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UIView *separatorBg;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@end
