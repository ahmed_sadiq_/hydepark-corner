//
//  DBCommunicator.h
//  HydePark
//
//  Created by Ahmed Sadiq on 23/11/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBCommunicator : NSObject
@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;

+ (id)sharedManager;
- (void)createDB;
- (BOOL)openDB;
- (void)closeDB;
- (void)clearDB;
- (void)newEntryWithSession:(NSString *)userSession replyCount:(NSString *)allowedReply caption:(NSString *)caption
     isAnonymous:(NSString *)anonyCheck isMute:(NSString *)muteCheck videoLength:(NSString *)duration postID:(NSString *)postId
     parentCommentID:(NSString *)parentId privacy:(NSString *)privacyCheck language:(NSString *)langCode timeStamp:(NSString *)currentTime
                  mediaData:(NSString *)data isAudio:(NSString *)audioCheck isComment:(NSString *)commentCeck fromGallery:(BOOL)isFromGallery withThumbnail:(NSString *)thumbnail;
- (NSMutableArray *)retrieveAllRows;
- (void)deleteRowWithID:(NSString *)rowID;

@end
