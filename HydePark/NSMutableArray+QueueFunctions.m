//
//  NSMutableArray+QueueFunctions.m
//  HydePark
//
//  Created by Osama on 20/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "NSMutableArray+QueueFunctions.h"

@implementation NSMutableArray (QueueFunctions)
// Queues are first-in-first-out, so we remove objects from the head
- (id) dequeue {
    // if ([self count] == 0) return nil; // to avoid raising exception (Quinn)
    id headObject = [self objectAtIndex:0];
    if (headObject != nil) {
    // so it isn't dealloc'ed on remove
        [self removeObjectAtIndex:0];
    }
    return headObject;
}

// Add to the tail of the queue (no one likes it when people cut in line!)
- (void) enqueue:(id)anObject {
    [self addObject:anObject];
    //this method automatically adds to the end of the array
}

-(id)peek{
    id item = nil;
    if([self count] != 0){
        item = [self objectAtIndex:0];
    }
    return item;
}
@end
