//
//  StoriesCollectionViewCell.h
//  HydePark
//
//  Created by Ahmed Sadiq on 19/12/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoriesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBackGroundView;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionCell;
@end
