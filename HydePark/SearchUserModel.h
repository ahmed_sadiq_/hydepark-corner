//
//  SearchUserModel.h
//  HydePark
//
//  Created by apple on 6/23/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchUserModel : NSObject
@property (nonatomic, retain) NSString *user_id;
@property (nonatomic, retain) NSData *full_name;
@property (nonatomic, retain) NSString *is_celeb;
@property (nonatomic, retain) NSString *profile_link;
@property (nonatomic, retain) NSString *profile_type;

-(id)initWithDictionary:(NSDictionary *)_dictionary;
@end
