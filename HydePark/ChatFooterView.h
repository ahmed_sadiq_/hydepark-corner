//
//  ChatFooterView.h
//  HydePark
//
//  Created by Samreen Noor on 28/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatFooterView : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UIButton *btnBeamPressed;
@property (strong, nonatomic) IBOutlet UIButton *btnAudioPressed;
@property (strong, nonatomic) IBOutlet UILabel *reply;
@property (strong, nonatomic) IBOutlet UIImageView *dots;

@end
