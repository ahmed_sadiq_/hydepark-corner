//
//  NewRequestViewController.m
//  HydePark
//
//  Created by Ahmed Sadiq on 15/12/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "NewRequestViewController.h"
#import "DataContainer.h"
#import "SearchCell.h"
#import "Followings.h"
#import "Constants.h"
#import "Utils.h"
#import "UserChannel.h"
#import "NSData+AES.h"


@interface NewRequestViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *friendsArray;
    NSUInteger        currentSelectedIndex;
    NSString         *friendId;
    DataContainer *sharedManager;
}

@end

BOOL isReloaded;

@implementation NewRequestViewController

+ (NewRequestViewController *)contentTableView {
    NewRequestViewController *contentTV = [[NewRequestViewController alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - 170) style:UITableViewStylePlain];
    contentTV.separatorStyle = UITableViewCellSeparatorStyleNone;
    contentTV.backgroundColor = [UIColor clearColor];
    contentTV.dataSource = contentTV;
    contentTV.delegate = contentTV;

    return contentTV;
}

-(void)loadNewRequests
{
    sharedManager = [DataContainer sharedManager];
    [sharedManager.reqsArray removeAllObjects];
    [self addrefreshControl];
    _canLoadMore = true;
    sharedManager.newRequestsPageNum = 1;
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            spinner.color = [UIColor lightGrayColor];
        }
    }
    
    spinner.center = self.center;
    [self addSubview:spinner];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [spinner startAnimating];
    });
    [sharedManager.reqsArray removeAllObjects];

    [self getNewRequests];
}

-(void)refreshRequests
{
    sharedManager = [DataContainer sharedManager];
    [sharedManager.reqsArray removeAllObjects];
    _canLoadMore = true;
    sharedManager.newRequestsPageNum = 1;
    [self getNewRequests];
}

-(void)addrefreshControl{
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor whiteColor];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            refreshControl.tintColor = [UIColor lightGrayColor];
        }
    }
    
    [refreshControl addTarget:self
                       action:@selector(refreshCall)
     
             forControlEvents:UIControlEventValueChanged];
    [self addSubview:refreshControl];
}

- (void)refreshCall
{
    _canLoadMore = true;
    [sharedManager.reqsArray removeAllObjects];
    sharedManager.newRequestsPageNum = 1;
    [self getNewRequests];
}


-(void)getNewRequests
{
    DataContainer *sharedManager2 = [DataContainer sharedManager];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",sharedManager2.newRequestsPageNum];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_NEW_REQUESTS,@"method",
                              token,@"session_token",pageStr,@"page_no",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    if(sharedManager2.newRequestsPageNum == 1)
    {
        sharedManager2.followers = [[NSMutableArray alloc] init];
    }
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        NSMutableArray *freshRecords = [[NSMutableArray alloc] init];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [spinner removeFromSuperview];
        });
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [refreshControl endRefreshing];
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1)
            {
                NSArray *freidns = [result objectForKey:@"following"];
                if(freidns.count < 10)
                {
                    _canLoadMore = false;
                }
                else
                {
                    _canLoadMore = true;
                }
                sharedManager2.followers = [[NSMutableArray alloc] init];
                if([freidns isKindOfClass:[NSArray class]]){
                    for(NSDictionary *tempDict in freidns){
                        Followings *_responseData = [[Followings alloc] init];
                        _responseData.f_id = [tempDict objectForKey:@"id"];
                        _responseData.fullName = [tempDict objectForKey:@"full_name"];
                        _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                        _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                        _responseData.status = [tempDict objectForKey:@"state"];
                        _responseData.isFollowed = [[tempDict objectForKey:@"is_followed"] intValue];
                        [freshRecords addObject:_responseData];
                    }
                }
            }
            else{
                [refreshControl endRefreshing];
            }
        }else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [refreshControl endRefreshing];
        }
        
//        sharedManager2.reqsArray = sharedManager2.followers;
        [sharedManager2.reqsArray addObjectsFromArray:freshRecords];

        isReloaded = NO;
        [self reloadData];
    }];
}

#pragma mark - UITableViewDataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    sharedManager = [DataContainer sharedManager];
    
    CGFloat yOffset = tableView.contentOffset.y;
    CGFloat height = tableView.contentSize.height - tableView.frame.size.height;
    CGFloat scrolledPercentage = yOffset / height;
    
    
//    NSMutableArray *filteredArray = sharedManager.friendsVcArray;
//    friendsArray = [[NSMutableArray alloc] init];
//    for (Followings *following in filteredArray) {
//        if([following.status isEqualToString:@"ACCEPT_REQUEST"]){
//            [friendsArray addObject:following];
//        }
//    }
    
    if(friendsArray.count % 10 == 0 && _canLoadMore && scrolledPercentage > .6f)
    {
        _canLoadMore = false;
        sharedManager.newRequestsPageNum ++;
        [self getNewRequests];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    static NSString *cellIdentifier = @"cell";
//
//    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if(!cell) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//    }
//    cell.textLabel.text = [NSString stringWithFormat:@"%@",@(indexPath.row)];
//    return cell;
    static NSString *CellIdentifier = @"UserCell";
    SearchCell *cell;
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects;
        if(IS_IPAD){
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SearchCell_iPad" owner:self options:nil];
        }else{
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SearchCell" owner:self options:nil];
        }
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
        //            cell.leftreplImg.hidden =  NO;
        //            cell.rightreplImg.hidden = NO;
    }else{
        cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            cell.lowerSeparator.hidden = NO;
        }
        else
        {
            cell.lowerSeparator.hidden = YES;
        }
    }
    else
    {
        cell.lowerSeparator.hidden = YES;
    }
    
    Followings *tempUsers = [[Followings alloc]init];
//    if(tableView == self.searchDisplayController.searchResultsTableView && ivarNoResults){
//        static NSString *cleanCellIdent = @"cleanCell";
//        UITableViewCell *ccell = [tableView dequeueReusableCellWithIdentifier:cleanCellIdent];
//        if (ccell == nil) {
//            ccell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cleanCellIdent];
//            ccell.userInteractionEnabled = NO;
//
//        }
//        [ccell setBackgroundColor:[UIColor clearColor]];
//        ccell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return ccell;
//    }
//    else if (tableView == self.searchDisplayController.searchResultsTableView && !ivarNoResults) {
//        if(self.searchedContent.count > 0){
//            tempUsers = [self.searchedContent objectAtIndex:indexPath.row];
//        }else{
//            NSLog(@"search content is zero");
//
//        }
//    } else {
        tempUsers = [friendsArray objectAtIndex:indexPath.row];
//    }
    // tempUsers = [friendsArray objectAtIndex:indexPath.row];
    
    
    cell.friendsName.text = [Utils getDecryptedTextFor:tempUsers.fullName];
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] == tempUsers.f_id ){
        cell.statusImage.hidden = YES;
    }
    //    else if(!showBtn){
    //        cell.statusImage.hidden = YES;
    //    }
    //    else{
    //        cell.statusImage.hidden = NO;
    //    }
    cell.profilePic.imageURL = [NSURL URLWithString:tempUsers.profile_link];
    NSURL *url = [NSURL URLWithString:tempUsers.profile_link];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    cell.profilePic.layer.cornerRadius = cell.profilePic.frame.size.width / 2;
    for (UIView* subview in cell.profilePic.subviews)
        subview.layer.cornerRadius = cell.profilePic.frame.size.width / 2;
    
    cell.profilePic.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.profilePic.layer.shadowOpacity = 0.7f;
    cell.profilePic.layer.shadowOffset = CGSizeMake(0, 5);
    // cell.profilePic.layer.shadowRadius = 5.0f;
    cell.profilePic.layer.masksToBounds = NO;
    
    cell.profilePic.layer.cornerRadius = cell.profilePic.frame.size.width / 2;
    cell.profilePic.layer.masksToBounds = NO;
    cell.profilePic.clipsToBounds = YES;
    
    cell.profilePic.layer.backgroundColor = [UIColor clearColor].CGColor;
    cell.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.profilePic.layer.borderWidth = 0.0f;
    
    //cell.statusImage.hidden = false;
    cell.activityInd.hidden = true;
    [cell.activityInd stopAnimating];
    [cell.statusImage addTarget:self action:@selector(statusPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.statusImage setTag:indexPath.row];
    [cell.rejectBtn addTarget:self action:@selector(rejectRequest:) forControlEvents:UIControlEventTouchUpInside];
    [cell.rejectBtn setTag:indexPath.row];
    [cell.friendsChannelBtn addTarget:self action:@selector(OpenFriendsChannelPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.friendsChannelBtn setTag:indexPath.row];
    cell.rejectBtn.hidden = YES;
    [cell.statusImage setTitle:@"" forState:UIControlStateNormal];
    if(_loadFollowings == 5){
        [cell.statusImage removeTarget:nil
                                action:NULL
                      forControlEvents:UIControlEventAllEvents];
        [cell.statusImage addTarget:self action:@selector(sharePressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.statusImage setTitle:NSLocalizedString(@"inbox", @"") forState:UIControlStateNormal];
    }
    else if(_loadFollowings == 6){
        [cell.statusImage removeTarget:nil
                                action:NULL
                      forControlEvents:UIControlEventAllEvents];
        [cell.statusImage addTarget:self action:@selector(postPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.statusImage setTitle:NSLocalizedString(@"post", @"") forState:UIControlStateNormal];
    }
    else if(_loadFollowings == 7){
        [cell.statusImage removeTarget:nil
                                action:NULL
                      forControlEvents:UIControlEventAllEvents];
        [cell.statusImage addTarget:self action:@selector(unblockUser:) forControlEvents:UIControlEventTouchUpInside];
        [cell.statusImage setTitle:NSLocalizedString(@"unblock", @"") forState:UIControlStateNormal];
    }
    else{
        if ([tempUsers.status isEqualToString:@"ADD_FRIEND"] && [tempUsers.is_celeb isEqualToString:@"1"]) {
            [cell.statusImage setTitle:NSLocalizedString(@"follow", @"") forState:UIControlStateNormal];
        }
        else if ([tempUsers.status isEqualToString:@"FRIEND"] && [tempUsers.is_celeb isEqualToString:@"1"]){
            [cell.statusImage setTitle:NSLocalizedString(@"following", @"") forState:UIControlStateNormal];
        }
        else if ([tempUsers.status isEqualToString:@"FRIEND"] && [tempUsers.is_celeb isEqualToString:@"0"]){
            [cell.statusImage setTitle:NSLocalizedString(@"friends", @"") forState:UIControlStateNormal];
        }
        else if([tempUsers.status isEqualToString:@"ADD_FRIEND"] && [tempUsers.is_celeb isEqualToString:@"0"]){
            [cell.statusImage setTitle:NSLocalizedString(@"add_friend", @"") forState:UIControlStateNormal];
        }
        else if([tempUsers.status isEqualToString:@"PENDING"]){
            [cell.statusImage setTitle:NSLocalizedString(@"pending", @"") forState:UIControlStateNormal];
        }
        else if([tempUsers.status isEqualToString:@"ACCEPT_REQUEST"]){
            [cell.statusImage setTitle:NSLocalizedString(@"Respond", @"") forState:UIControlStateNormal];
            //cell.statusImage.frame = CGRectMake(cell.statusImage.frame.origin.x, cell.statusImage.frame.origin.y - 20, cell.statusImage.frame.size.width, cell.statusImage.frame.size.height);
            cell.rejectBtn.hidden = YES;
        }
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DataContainer *sharedInstance = [DataContainer sharedManager];
    if(!isReloaded) {
        NSMutableArray *filteredArray = sharedInstance.reqsArray;
        friendsArray = [[NSMutableArray alloc] init];
        for (Followings *following in filteredArray) {
            if([following.status isEqualToString:@"ACCEPT_REQUEST"]){
                [friendsArray addObject:following];
            }
        }
    } else {
        isReloaded = NO;
    }
    return  friendsArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (IS_IPAD)
        return 93.0f;
    return 80.f;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
}



#pragma mark Action sheet
-(void)statusPressed:(UIButton *)sender{
    
    
    UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    Followings *PopUser = [[Followings alloc] init];
    //    if ([self isSearchDisplayControllerActice]) {
    //        PopUser = [self.searchedContent objectAtIndex:currentSelectedIndex];
    //    }else{
    PopUser  = [friendsArray objectAtIndex:currentSelectedIndex];
    //    }
    friendId = PopUser.f_id;
    //    [statusBtn setBackgroundImage:[UIImage imageNamed:@"follow.png"] forState:UIControlStateNormal];
    if ([PopUser.status isEqualToString:@"ADD_FRIEND"]) {
        if([PopUser.is_celeb isEqualToString:@"0"]){
            [self presentActionSheet:NSLocalizedString(@"add_friend", @"") status:1 fIndex:statusBtn.tag];
        }
        else{
            [self presentActionSheet:NSLocalizedString(@"follow", @"") status:1 fIndex:statusBtn.tag];
        }
    }
    else if([PopUser.status isEqualToString:@"FRIEND"]){
        if([PopUser.is_celeb isEqualToString:@"0"]){
            [self presentActionSheet:NSLocalizedString(@"Unfriend", @"") status:4 fIndex:statusBtn.tag];
        }
        else{
            [self presentActionSheet:NSLocalizedString(@"unfollow", @"") status:4 fIndex:statusBtn.tag];
        }
    }
    else if([PopUser.status isEqualToString:@"ACCEPT_REQUEST"]){
        [self presentActionSheet:NSLocalizedString(@"accept", @"") status:3 fIndex:statusBtn.tag];
    }
    else if([PopUser.status isEqualToString:@"PENDING"]){
        
        [self presentActionSheet:NSLocalizedString(@"delete_request", @"") status:2 fIndex:statusBtn.tag];
        
    }
}

#pragma mark Action sheet
-(void)reloadselection:(NSInteger)tagToUpdate{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tagToUpdate inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    //    if([self isSearchDisplayControllerActice]){
    //        [self.searchDisplayController.searchResultsTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    //    }
    //    else{
//    [self reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    //        }
    [self reloadData];
}

-(void)presentActionSheet:(NSString *)tittleForBtn status:(int)status fIndex:(NSInteger)fIndex{
    
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    Followings *PopUser ;
    Followings *UserCopied;
    //    if ([self isSearchDisplayControllerActice]) {
    //        PopUser = [self.searchedContent objectAtIndex:fIndex];
    //        NSInteger index = [friendsArray indexOfObject:PopUser];
    //        if (index != NSNotFound) {
    //            UserCopied = [friendsArray objectAtIndex:index];
    //
    //        }
    //    }else{
    PopUser  = [friendsArray objectAtIndex:fIndex];
    //    }
    friendId = PopUser.f_id;
    UIAlertAction* accept = [UIAlertAction
                             actionWithTitle:tittleForBtn
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if(status == 1){
                                     if([PopUser.is_celeb isEqualToString:@"0"]){
                                         DataContainer *sharedInstance = [DataContainer sharedManager];
                                         NSMutableArray *filteredArray = sharedInstance.reqsArray;
                                         for (Followings *user in filteredArray) {
                                             if([user.f_id isEqualToString:PopUser.f_id]) {
                                                 user.status = @"PENDING";
                                                 sharedManager.reqsArray = filteredArray;
                                                 break;
                                             }
                                         }
                                     }
                                     else{
                                         DataContainer *sharedInstance = [DataContainer sharedManager];
                                         NSMutableArray *filteredArray = sharedInstance.reqsArray;
                                         for (Followings *user in filteredArray) {
                                             if([user.f_id isEqualToString:PopUser.f_id]) {
                                                 user.status = @"FRIEND";
                                                 sharedManager.reqsArray = filteredArray;
                                                 break;
                                             }
                                         }
                                     }
                                     [self reloadselection:fIndex];
                                     [self sendFriendRequest:PopUser];
                                     
                                 }else if(status == 2){
                                     DataContainer *sharedInstance = [DataContainer sharedManager];
                                     NSMutableArray *filteredArray = sharedInstance.reqsArray;
                                     for (Followings *user in filteredArray) {
                                         if([user.f_id isEqualToString:PopUser.f_id]) {
                                             user.status = @"ADD_FRIEND";
                                             sharedManager.reqsArray = filteredArray;
                                             break;
                                         }
                                     }
                                     [self reloadselection:fIndex];
                                     [self rejectRequest:fIndex];
                                     
                                 }else if(status == 3){
                                   //  PopUser.status = @"FRIEND";
                                     DataContainer *sharedInstance = [DataContainer sharedManager];
                                     NSMutableArray *filteredArray = sharedInstance.reqsArray;
                                     for (Followings *user in filteredArray) {
                                         if([user.f_id isEqualToString:PopUser.f_id]) {
                                             user.status = @"FRIEND";
                                             sharedManager.reqsArray = filteredArray;
                                             break;
                                         }
                                     }
                                     isReloaded = YES;
                                     [self reloadselection:fIndex];
                                     [self acceptRequest];
                             //        [friendsArray removeObjectAtIndex:fIndex];
                                     
                                 }else if(status == 4){
                                     DataContainer *sharedInstance = [DataContainer sharedManager];
                                     NSMutableArray *filteredArray = sharedInstance.reqsArray;
                                     for (Followings *user in filteredArray) {
                                         if([user.f_id isEqualToString:PopUser.f_id]) {
                                             user.status = @"ADD_FRIEND";
                                             sharedManager.reqsArray = filteredArray;
                                             break;
                                         }
                                     }
                                     [self reloadselection:fIndex];
                                     [self sendCancelRequest];
                                 }
                                 if(UserCopied){
                                     UserCopied.status = PopUser.status;
                                     NSIndexPath *indexPath = [NSIndexPath indexPathForRow:fIndex inSection:0];
                                     NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                                     [self reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                                 }
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* reject = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"reject", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 DataContainer *sharedInstance = [DataContainer sharedManager];
                                 NSMutableArray *filteredArray = sharedInstance.reqsArray;
                                 for (Followings *user in filteredArray) {
                                     if([user.f_id isEqualToString:PopUser.f_id]) {
                                         user.status = @"ADD_FRIEND";
                                         sharedManager.reqsArray = filteredArray;
                                         break;
                                     }
                                 }
                                 isReloaded = YES;
                                 [self reloadselection:fIndex];
                                  [self rejectRequest:fIndex];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* cancel  = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"cancel", @"")
                              style:UIAlertActionStyleCancel
                              handler:^(UIAlertAction * action)
                              {
                                  [view dismissViewControllerAnimated:YES completion:nil];
                              }];
    
    [view addAction:accept];
    if(status == 3)
        [view addAction:reject];
    [view addAction:cancel];
    if(IS_IPAD){
        //        UITapGestureRecognizer *oneFingerTwoTaps = [[UITapGestureRecognizer alloc]init];
        //        oneFingerTwoTaps.delegate = self;
        if(_btnOriginY < 155)
        {
            _btnOriginY = 155;
        }
        view.popoverPresentationController.sourceView = self;
        if(fIndex == 0){
            _btnOriginY = 50;
        }
        else
            _btnOriginY = 142*fIndex;
        NSLog(@"btn %f",_btnOriginY);
        view.popoverPresentationController.sourceRect = CGRectMake(600, _btnOriginY , 200, 200);
    }
    [self.vc presentViewController:view animated:YES completion:nil];
}

-(void) OpenFriendsChannelPressed:(UIButton *)sender{
    UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    Followings *_responseData;
    //    if ([self isSearchDisplayControllerActice]) {
    //        _responseData = [self.searchedContent objectAtIndex:currentSelectedIndex];
    //    }else{
    _responseData =  [friendsArray objectAtIndex:currentSelectedIndex];
    //    }
    friendId = _responseData.f_id;
    
    UserChannel *commentController;
    if(IS_IPAD){
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
    }else if (IS_IPHONEX){
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
    }
    else
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
    commentController.ChannelObj = nil;
    commentController.friendID   = friendId;
    commentController.currentUser = _responseData;
    [[_vc navigationController] pushViewController:commentController animated:YES];
}

- (void) sendFriendRequest:(Followings *) pUser{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SEND_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                if([Utils userIsCeleb] && pUser.isFollowed == 0){
                    [sharedManager.followers insertObject:pUser atIndex:0];
                }
                else if(![Utils userIsCeleb]){
                    [sharedManager.followers insertObject:pUser atIndex:0];
                }
                [self isFriendAlreadyPresent:pUser];
            }
        }else{
            pUser.status = @"ADD_FRIEND";
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
            NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
            //            if([self isSearchDisplayControllerActice]){
            //                [self.searchDisplayController.searchResultsTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            //            }
            //            else{
            [self reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            //            }
        }
    }];
}

- (void)rejectRequest:(NSInteger)indexToRequest{
    
    Followings *PopUser = [[Followings alloc]init];
    //    if ([self isSearchDisplayControllerActice]) {
    //        PopUser = [self.searchedContent objectAtIndex:indexToRequest];
    //    }else
    PopUser  = [friendsArray objectAtIndex:indexToRequest];
    friendId = PopUser.f_id;
    [self decrementNotificationCount];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_REJECT_REEQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                if(sharedManager.followers.count > 0){
                    NSInteger index = [sharedManager.followers indexOfObject:PopUser];
                    if (index != NSNotFound) {
                        if( [Utils userIsCeleb] && (PopUser.isFollowed == 0)){
                            [sharedManager.followers  removeObjectAtIndex:index];
                        }
                        else if(![Utils userIsCeleb]){
                            [sharedManager.followers  removeObjectAtIndex:index];
                        }
                    }
                }
                
                
                NSString *countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.pendingReqsCount];
                NSInteger previousCount = [countSuffix integerValue];
                previousCount --;
                countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount];
                sharedManager._profile.pendingReqsCount = countSuffix;
                countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
                [Utils setCachedPendingReqCount:countSuffix];
                
                
                
                
                NSMutableArray *filteredArray = sharedManager.reqsArray;
                friendsArray = [[NSMutableArray alloc] init];
                for (Followings *following in filteredArray) {
                    if([following.status isEqualToString:@"ACCEPT_REQUEST"]){
                        [friendsArray addObject:following];
                    }
                }
                isReloaded = YES;
                [self reloadData];
               
            }
        }else{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
}

- (void)acceptRequest{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    Followings *PopUser = [[Followings alloc]init];
    //    if ([self isSearchDisplayControllerActice]) {
    //        PopUser = [self.searchedContent objectAtIndex:currentSelectedIndex];
    //    }else
    PopUser  = [friendsArray objectAtIndex:currentSelectedIndex];
    [self decrementNotificationCount];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_ACCEPT_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                //PopUser.status = @"FRIEND";
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
                NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                isReloaded = NO;
                
                NSString *countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.friends_count];
                NSInteger previousCount = [countSuffix integerValue];
                previousCount ++;
                countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount];
                sharedManager._profile.friends_count = countSuffix;
                countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
                [Utils setChacedFriendsCount:countSuffix];
                
                countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.pendingReqsCount];
                previousCount = [countSuffix integerValue];
                previousCount --;
                countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount];
                sharedManager._profile.pendingReqsCount = countSuffix;
                countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
                [Utils setCachedPendingReqCount:countSuffix];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshFriendList" object:nil];

                [self reloadData];
            }
        }else{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
    
}

- (void) sendCancelRequest{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    Followings *PopUser = [[Followings alloc]init];
    //    if ([self isSearchDisplayControllerActice]) {
    //        PopUser = [self.searchedContent objectAtIndex:currentSelectedIndex];
    //    }else
    PopUser  = [friendsArray objectAtIndex:currentSelectedIndex];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_DELETE_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if(success == 1) {
                if(sharedManager.followers.count > 0){
                    NSInteger index = [sharedManager.followers indexOfObject:PopUser];
                    if (index != NSNotFound) {
                        if( [Utils userIsCeleb] && PopUser.isFollowed == 0){
                            [sharedManager.followers  removeObjectAtIndex:index];
                        }
                        if(![Utils userIsCeleb]){
                            [sharedManager.followers  removeObjectAtIndex:index];
                        }
                    }
                }
            }
        }else{
            
        }
    }];
    
}

-(void)decrementNotificationCount{
    if(_loadFollowings == 2){
        sharedManager = [DataContainer sharedManager];
        NSString *pendingReqC = sharedManager._profile.pendingReqsCount;
        int value = [pendingReqC intValue];
        value = value - 1;
        sharedManager._profile.pendingReqsCount = [pendingReqC copy];
        sharedManager.isReloadMyCorner = YES;
        if(value <= 0){
            sharedManager.changeFriendCount = YES;
        }
        
    }
}

#pragma mark ISAlreadyPresent
-(BOOL)isFriendAlreadyPresent:(Followings*)fUser{
    for (int i =0; i< sharedManager.followers.count; i++){
        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
        if([tempObj.f_id isEqualToString:fUser.f_id]){
            tempObj.status = fUser.status;
            return YES;
        }
    }
    return NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
