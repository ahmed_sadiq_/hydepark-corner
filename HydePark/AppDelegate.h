//
//  AppDelegate.h
//  HydePark
//
//  Created by Mr on 21/04/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>
#import "CommentsModel.h"
#import "VideoModel.h"
#import "Alert.h"
#import "MJGStack.h"
#import "Reachability.h"
#import <UserNotifications/UserNotifications.h>

@class ViewController;
@class HomeVC;

#define kSocialAccountTypeKey @"SOCIAL_ACCOUNT_TYPE"

extern NSString * const FBSessionStateChangedNotification;

@interface AppDelegate : UIResponder <UIApplicationDelegate,AlertDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MJGStack *parentIdStackController;

@property ( strong , nonatomic ) UINavigationController *navigationController;
@property ( strong , nonatomic ) ViewController *viewController;
+(NSString *)getDeviceToken;
+(NSDictionary *)getBranchParams;
+(void)moveToBranchedBeam;
@property ( strong , nonatomic ) HomeVC *HomeVC;
@property BOOL latestVideoAdded;
@property BOOL loaduserProfiel;
@property NSString *userToView;
@property BOOL isLoggedIn;
@property BOOL shudStartUpload;
@property int navigationControllersCount;

@property int user_id;
@property (strong, nonatomic) NSString *profile_pic_url;
@property (strong, nonatomic) NSString *strUserId;
@property (strong, nonatomic) NSString *strSocial;
@property (strong, nonatomic) NSString *strFirstN;
@property (strong, nonatomic) NSString *strLastN;
@property (strong, nonatomic) NSString *strEmail;
@property (strong, nonatomic) NSString *strProfileImage;
@property BOOL IS_celeb;
@property (strong, nonatomic) NSArray  *friendsArray;

@property (nonatomic, retain) NSString *videotoPlay;
@property (nonatomic, retain) NSString *videotitle;
@property (nonatomic, retain) NSString *videotags;
@property (nonatomic, retain) NSString *videoUploader;
@property (nonatomic, retain) NSString *currentScreen;
@property (assign) BOOL seen;
@property BOOL hasBlockedSomeOne;
//@property (strong, nonatomic) FBSession *loggedInSession;
@property (strong, nonatomic) NSString *emailGPLus;
@property (strong, nonatomic) VideoModel *commentObj;
@property (strong, nonatomic) VideoModel *videObj;
@property (strong, nonatomic) VideoModel *videomodel;
@property  NSInteger totalNotiCount;
@property  NSInteger unseenCount;
@property BOOL hasBeenUpdated;
@property BOOL hasbeenEdited;
@property BOOL timeToupdateHome;
@property NSUInteger currentMyCornerIndex;
@property float progressFloat;
@property (strong, nonatomic) Reachability *reachability;

@property (nonatomic, assign) BOOL hasInet;
@property (nonatomic, assign) BOOL isUploading;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;

//- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;



@property (nonatomic, assign) BOOL fetchingContent;
@property (nonatomic, assign) BOOL myCornerCallFailed;
@property (nonatomic, assign) BOOL myCornerDataSuccess;
@end

