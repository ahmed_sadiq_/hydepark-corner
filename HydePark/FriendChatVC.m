//
//  FriendChatVC.m
//  HydePark
//
//  Created by Samreen on 28/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "FriendChatVC.h"
#import "FriendsCell.h"
#import "Constants.h"
#import "Utils.h"
#import "VideoModel.h"
#import "ChatLeftCell.h"
#import "ChatFooterView.h"
#import "ASIFormDataRequest.h"
#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "DataContainer.h"
#import "VideoPlayerVC.h"
#import "UIImageView+WebCache.h"
#import "NotificationsModel.h"
#import "AppDelegate.h"
#import "UserChannel.h"
#import "PlayerViewController.h"
#import "SVProgressHUD.h"
#import "SVProgressHUDCustom.h"
#import <HBRecorder/HBRecorder.h>
#import "NSData+AES.h"
#import "DBCommunicator.h"
#import "BeamUploadVC.h"
#import "OfflineDataModel.h"

@interface FriendChatVC ()<HBRecorderProtocol>
{
    UICollectionReusableView *reusableview;
    NSMutableArray *chatArray;
   // NSString *video_duration;
    DataContainer *sharedManager;
    NSString *userId;
    NSString *userName;
    BOOL isLocal;
    UIButton *beam;
    UIButton *audio;
    float upProgress;
    CGRect cellFrame;
    CGFloat uploadingProgress;
    SVProgressHUDCustom *SVCustom;
}
@end

@implementation FriendChatVC
@synthesize isFriend;
- (void)viewDidLoad {
    [super viewDidLoad];
    _isUploading = NO;
    isForRefreshOnly = false;
    SVCustom = [[SVProgressHUDCustom alloc]init];
    [SVCustom customProgressHUD];
    isLocal = false;
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.background.hidden = YES;
        }
        else
        {
            self.background.hidden = NO;
        }
    }
    else
    {
        self.background.hidden = NO;
    }
    sharedManager = [DataContainer sharedManager];
    [[DataContainer sharedManager] setIsChaVC:true];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isFriendYes:) name:@"BecameFriend" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMyCollection) name:@"internetDisconnected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getChats) name:@"ReloadInbox" object:nil];

    
    userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Id"];
    userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Name"];
    chatArray = [[NSMutableArray alloc]init];
    
    [self.collectionView registerClass:[FriendsCell class] forCellWithReuseIdentifier:@"cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatLeftCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatRightCell" bundle:nil] forCellWithReuseIdentifier:@"cell1"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatLeftCell_Ipad" bundle:nil] forCellWithReuseIdentifier:@"ipadCell1"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatRightCell_Ipad" bundle:nil] forCellWithReuseIdentifier:@"ipadCell2"];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProgressBar:)
                                                 name:@"updateProgress"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(completeProgress:)
                                                 name:@"completeProgress"
                                               object:nil];
    UINib *header;
    if(IS_IPAD){
        header = [UINib nibWithNibName:@"ChatFooterView_Ipad" bundle:nil];
    }
    else{
        header = [UINib nibWithNibName:@"ChatFooterView" bundle:nil];
    }
    [self.collectionView registerNib:header forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"Footer"];
    _collectionView.hidden =YES;
    int currentCount = [sharedManager._profile.unreadInboxCount intValue];
    int selectedCount = [self.selectedFriend.unseen_counts intValue];
    currentCount -= selectedCount;
    sharedManager._profile.unreadInboxCount = [NSString stringWithFormat:@"%d",currentCount];
    sharedManager.isReloadMyCorner = YES;
    
    NSString *decodedString = [Utils getDecryptedTextFor:_selectedFriend.full_name];
    
    if(decodedString == nil)
    {
        decodedString = [Utils getDecryptedTextFor:_seletedFollowing.fullName];
    }
    
    if (_isLocalSearch) {
        
        _lblFrndName.text = decodedString;
        _frndId=_seletedFollowing.f_id;
        [_selectedFriend setUnseen_counts:@"0"];
        [[DataContainer sharedManager] setChatFriendId:_seletedFollowing.f_id];
        [sharedManager.friendsArray removeAllObjects];
    }
    else{
        _lblFrndName.text = decodedString;
        _frndId=_selectedFriend.friend_id;
        [_selectedFriend setUnseen_counts:@"0"];
        [[DataContainer sharedManager] setChatFriendId:_selectedFriend.friend_id];
    }
    if(APP_DELEGATE.hasInet)
    {
        [self getChats];
    }
    else
    {
        [self addOfflineBeams];
        _collectionView.hidden = NO;
        [_collectionView reloadData];
        NSInteger section = [self.collectionView numberOfSections] - 1;
        NSInteger item = [self.collectionView numberOfItemsInSection:section] ;
        if(!_reloadCheck) {
            if(item > 3) {
                CGPoint bottomOffset = CGPointMake(0, self.collectionView.contentSize.height - 120+(chatArray.count - 3)  * 164);
                [self.collectionView setContentOffset:bottomOffset animated:YES];
            }
            else if (item==3){
                CGPoint bottomOffset = CGPointMake(0, self.collectionView.contentSize.height -120+(chatArray.count)  * 100);
                [self.collectionView setContentOffset:bottomOffset animated:YES];
                
            }
        }
    }
    [self setAudioRecordSettings];
    
}

-(void)addOfflineBeams
{
    sharedManager = [DataContainer sharedManager];
    [[DBCommunicator sharedManager] createDB];
    NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
    
    for (int i = 0; i < offlineArray.count; i++)
    {
        OfflineDataModel *offlineVideoModel = offlineArray[i];
        NSCharacterSet* notDigits = [NSCharacterSet decimalDigitCharacterSet];
        
        
        if ([offlineVideoModel.isComment isEqualToString:@"1"] || [offlineVideoModel.privacyCheck rangeOfCharacterFromSet:notDigits].location == NSNotFound){
            [offlineArray removeObjectAtIndex:i];
            i--;
            sharedManager.offlineArrayCount = sharedManager.offlineArrayCount - 1;
            sharedManager.isFromBackground = NO;
        }
    }
    
    for (int i = 0; i < offlineArray.count; i++)
    {
        OfflineDataModel *offlineVideoModel = offlineArray[i];
        
        if (![offlineVideoModel.privacyCheck isEqualToString:_frndId]){
            [offlineArray removeObjectAtIndex:i];
            i--;
            sharedManager.offlineArrayCount = sharedManager.offlineArrayCount - 1;
            sharedManager.isFromBackground = NO;
        }
    }
    
    ///////////////////////////MERGING OFFLINE BEAMS IN ONLINE ARRAY
    
    for (int i = 0; i < offlineArray.count; i++)
    {
        OfflineDataModel *offlineVideoModel = offlineArray[i];
        VideoModel *_Videos     = [[VideoModel alloc] init];
        
        _Videos.video_thumbnail_link = offlineVideoModel.thumbnail;
        _Videos.video_link = offlineVideoModel.data;
        _Videos.is_anonymous = offlineVideoModel.anonyCheck;
        _Videos.uploaded_date = offlineVideoModel.currentTime;
        _Videos.comments_count = @"0";
        _Videos.isLocal = YES;
        _Videos.current_datetime = offlineVideoModel.currentTime;
        _Videos.user_id = userId;
        [chatArray addObject:_Videos];
    }
}


-(void)reloadMyCollection
{
    [self.collectionView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [self.progressView removeFromSuperview];
    [self.collectionView reloadData];
}
-(void)isFriendYes:(NSNotification *)notification{
    beam.enabled= YES;
    audio.enabled = YES;
    NSString *isF= [[notification userInfo] valueForKey:@"isFriend"];
    isFriend = [isF boolValue];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}
-(void)viewDidAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addNewMsg:)
                                                 name:@"addNewMsg"
                                               object:nil];
}

-(void) addNewMsg:(NSNotification *) notification{
    if ([notification.name isEqualToString:@"addNewMsg"])
    {
        NSDictionary* userInfo = notification.object;
        VideoModel  *vObj= userInfo[@"videoObj"];
        if(![self checkIfVideoAlreadydded:vObj.videoID]) {
            [chatArray addObject:vObj];
            [_collectionView reloadData];
        }
    }
}
-(BOOL) checkIfVideoAlreadydded : (NSString *)vidID {
    
    for(int i=0; i<chatArray.count; i++) {
        VideoModel  *vObj = (VideoModel*) [chatArray objectAtIndex:i];
        if([vObj.videoID isEqualToString:vidID]) {
            return true;
        }
    }
    return false;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Collection View Methods

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    reusableview = nil;
    //osama
    if (kind == UICollectionElementKindSectionFooter) {
        ChatFooterView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"Footer" forIndexPath:indexPath];
        beam = footerview.btnBeamPressed;
        audio = footerview.btnAudioPressed;
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
            {
                footerview.reply.textColor = [UIColor blackColor];
                
                UIImage *newImage = [audio.currentBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                UIGraphicsBeginImageContextWithOptions(audio.currentBackgroundImage.size, NO, newImage.scale);
                [[UIColor colorWithRed:55.0/255 green:80.0/255 blue:138.0/255 alpha:1] set];
                [newImage drawInRect:CGRectMake(0, 0, audio.currentBackgroundImage.size.width, newImage.size.height)];
                newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                [audio setBackgroundImage:newImage forState:UIControlStateNormal];
                
                
                UIImage *img = [beam imageForState:UIControlStateNormal];

                UIImage *newImage2 = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                UIGraphicsBeginImageContextWithOptions(img.size, NO, newImage2.scale);
                [[UIColor colorWithRed:55.0/255 green:80.0/255 blue:138.0/255 alpha:1] set];
                [newImage2 drawInRect:CGRectMake(0, 0, img.size.width, newImage2.size.height)];
                newImage2 = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                [beam setImage:newImage2 forState:UIControlStateNormal];
                
                UIImage *newImage3 = [footerview.dots.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                UIGraphicsBeginImageContextWithOptions(footerview.dots.image.size, NO, newImage3.scale);
                [[UIColor lightGrayColor] set];
                [newImage3 drawInRect:CGRectMake(0, 0, footerview.dots.image.size.width, newImage3.size.height)];
                newImage3 = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                footerview.dots.image = newImage3;
            }
            else
            {
                self.background.hidden = NO;
            }
        }
        else
        {
            self.background.hidden = NO;
        }
        [footerview.btnBeamPressed addTarget:self action:@selector(sendVideo:) forControlEvents:UIControlEventTouchUpInside];
        [footerview.btnAudioPressed addTarget:self action:@selector(sendAudio:) forControlEvents:UIControlEventTouchUpInside];
        reusableview = footerview;
    }
    
    return reusableview;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    
    return chatArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    VideoModel *tempVideos = [[VideoModel alloc]init];
    if(chatArray.count > 0) {
    tempVideos  = [chatArray objectAtIndex:indexPath.row];
    }
    ChatLeftCell *cell;
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(deleteChatById:)];
    lpgr.minimumPressDuration = 1.0;
    UILongPressGestureRecognizer *lpgrS = [[UILongPressGestureRecognizer alloc]
                                           initWithTarget:self action:@selector(deleteSelfChat:)];
    lpgrS.minimumPressDuration = 1.0;
    
    
    
    if ([userId isEqualToString:tempVideos.user_id]) {
        if (IS_IPAD) {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ipadCell2" forIndexPath:indexPath];
            
        }
        else{
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell1" forIndexPath:indexPath];
        }
        [cell.cellProgress removeFromSuperview];
        cell.exclamation.hidden = YES;
        cell.redBG.hidden = YES;
        
        if (tempVideos.isLocal) {
            
            cell.lblUserName.text = [Utils getDecryptedTextFor:tempVideos.userName];
            cell.videoImg.image = [UIImage imageWithData:tempVideos.profileImageData];
            cell.lblTime.text = [Utils returnDateForInProgress:tempVideos.current_datetime];
            NSLog(@"BOX VIEW FRAME : %@ Progress View : %@",NSStringFromCGRect(cell.boxView.frame),NSStringFromCGRect(self.progressView.frame));
            if(IS_IPAD){
                cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(405,14, 256, 252)];
            }
            else if(IS_IPHONE_5){
                cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(177,20, 122, 120)];
            }
             else if(IS_IPHONE_6Plus){
                cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(243,14, 148, 135)];
            }
            else{
                cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(205,14, 147, 135)];
            }
            NSLog(@"BOX VIEW FRAME : %@ Progress View : %@",NSStringFromCGRect(cell.videoImg.frame),NSStringFromCGRect(self.progressView.frame));
            cell.cellProgress.layer.cornerRadius = 25.0f;
            cell.cellProgress.layer.masksToBounds = YES;
            cell.cellProgress.clipsToBounds = YES;
            [cell.contentView addSubview:cell.cellProgress];
            if(!APP_DELEGATE.hasInet)
            {
                cell.exclamation.hidden = NO;
                cell.redBG.hidden = NO;
                [cell.contentView bringSubviewToFront:cell.redBG];
                [cell.contentView bringSubviewToFront:cell.exclamation];
            }
        }
        else{
            [cell.cellProgress removeFromSuperview];
            cell.lblUserName.text = NSLocalizedString(@"You", "");
            cell.lblTime.text = [Utils returnDate:tempVideos.uploaded_date];
            [cell.videoImg sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageWithData:_profileData]];
            [cell.contentView addGestureRecognizer:lpgrS];
            lpgrS.view.tag = indexPath.row;
        }
    }
    else{
        
        if (IS_IPAD) {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ipadCell1" forIndexPath:indexPath];
        }
        else{
            cell = [collectionView
                    dequeueReusableCellWithReuseIdentifier:@"cell"
                    forIndexPath:indexPath
                    ];
        }
        cell.exclamation.hidden = YES;
        cell.redBG.hidden = YES;
        
        cell.lblUserName.text = [Utils getDecryptedTextFor:tempVideos.userName];
        cell.lblTime.text = [Utils returnDate:tempVideos.uploaded_date];
        [cell.videoImg sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageWithData:_profileData]];
        [cell.contentView addGestureRecognizer:lpgr];
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            cell.lblTime.textColor = [UIColor blackColor];
            cell.img1.image = nil;
            cell.img1.backgroundColor = [UIColor lightGrayColor];
            UIImage *newImage = [cell.img2.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIGraphicsBeginImageContextWithOptions(cell.img2.image.size, NO, newImage.scale);
            [[UIColor lightGrayColor] set];
            [newImage drawInRect:CGRectMake(0, 0, cell.img2.image.size.width, newImage.size.height)];
            newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            cell.img2.image = newImage;
        }
        else
        {
            cell.lblTime.textColor = [UIColor whiteColor];
        }
    }
    else
    {
        cell.lblTime.textColor = [UIColor whiteColor];
    }
    
    [cell.playBtn addTarget:self
                     action:@selector(playBeam:)
           forControlEvents:UIControlEventTouchUpInside];
    cell.playBtn.tag = indexPath.row;
    lpgr.view.tag = indexPath.row;
    
    
    ///////////
    cell.videoImg.hidden = NO;
    cell.CH_Video_Thumbnail.hidden = YES;
    NSData *tempData = [[NSUserDefaults standardUserDefaults] objectForKey:@"chatThumbnail"];
    UIImage *holderImage = [UIImage imageWithData:tempData];
    [cell.videoImg sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"pH"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (!error) {
            tempVideos.imageThumbnail = image;
            if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
                
                cell.CH_Video_Thumbnail.hidden = YES;
                
                if (tempVideos.imageThumbnail && tempVideos.animatedImage) {
                    cell.videoImg.hidden = YES;
                    [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                    cell.CH_Video_Thumbnail.hidden = NO;
                }else if (tempVideos.imageThumbnail && !tempVideos.animatedImage) {
                    [cell.videoImg setImage:tempVideos.imageThumbnail];
                    [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                        cell.videoImg.hidden = YES;
                        tempVideos.animatedImage = result.animatedImage;
                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                        cell.CH_Video_Thumbnail.hidden = NO;
                    }];
                }else if (!tempVideos.imageThumbnail && tempVideos.animatedImage) {
                    cell.videoImg.hidden = YES;
                    [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                    cell.CH_Video_Thumbnail.hidden = NO;
                }else if (!tempVideos.imageThumbnail && !tempVideos.animatedImage) {
                    
                    [cell.videoImg sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if (!error) {
                            tempVideos.imageThumbnail = image;
                            [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                                tempVideos.animatedImage = result.animatedImage;
                                [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                                cell.videoImg.hidden = YES;
                                cell.CH_Video_Thumbnail.hidden = NO;
                            }];
                        }else{
                            [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                                tempVideos.animatedImage = result.animatedImage;
                                [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                                cell.videoImg.hidden = YES;
                                cell.CH_Video_Thumbnail.hidden = NO;
                            }];
                        }
                    }];
                }
            }else{
                
                
                cell.videoImg.hidden = NO;
                cell.CH_Video_Thumbnail.hidden = YES;
                
                [cell.videoImg sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    if (!error) {
                        tempVideos.imageThumbnail = image;
                    }else{
                        
                        NSLog(@"%@", error);
                        
                    }
                    
                }];
                
            }
        }else{
            
            NSLog(@"%@", error);
            
        }
    }];
    return cell;
}

-(NSString *) getCurrentTimeStamp{
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    
    return dateString;
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize sz;
    
    if (IS_IPHONE_5) {
        sz = CGSizeMake((self.view.frame.size.width), 150.0);
    }
    else if (IS_IPAD){
        sz = CGSizeMake((self.view.frame.size.width), 300.0);
        
    }
    else
        sz = CGSizeMake((self.view.frame.size.width), 164.0);
    return sz;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
}
#pragma mark Long press
-(void)deleteChatById:(UILongPressGestureRecognizer *)gestureRecognizer
{
    NSInteger tag = gestureRecognizer.view.tag;
    VideoModel *tempModel = [[VideoModel alloc]init];
    tempModel = [chatArray objectAtIndex:tag];
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"delete", nil)
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [self deleteChatCall:tempModel.videoID deleteFrom:@"1"];
                             [chatArray removeObjectAtIndex:tag];
                             [self.collectionView reloadData];
                             [view dismissViewControllerAnimated:YES completion:nil];
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Cancel", nil)
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [view addAction:ok];
    [view addAction:cancel];
    view.popoverPresentationController.sourceView = self.view;
    view.popoverPresentationController.sourceRect = CGRectMake(0, 950, 700, 50);
    [self presentViewController:view animated:YES completion:nil];
}
-(void)deleteSelfChat:(UILongPressGestureRecognizer *)gestureRecognizer{
    NSInteger tag = gestureRecognizer.view.tag;
    VideoModel *tempModel = [[VideoModel alloc]init];
    tempModel = [chatArray objectAtIndex:tag];
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:NSLocalizedString(@"delete_beam", nil)
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* my = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"delete_for_myself", nil)
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [self deleteChatCall:tempModel.videoID deleteFrom:@"1"];
                             [chatArray removeObjectAtIndex:tag];
                             [self.collectionView reloadData];
                             [view dismissViewControllerAnimated:YES completion:nil];
                         }];
    UIAlertAction* every = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"delete_for_everyone", nil)
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                //Do some thing here
                                [self deleteChatCall:tempModel.videoID deleteFrom:@"2"];
                                [chatArray removeObjectAtIndex:tag];
                                [self.collectionView reloadData];
                                [view dismissViewControllerAnimated:YES completion:nil];
                            }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Cancel", nil)
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [view addAction:my];
    [view addAction:every];
    [view addAction:cancel];
    view.popoverPresentationController.sourceView = self.view;
    view.popoverPresentationController.sourceRect = CGRectMake(0, 900, 700, 100);
    [self presentViewController:view animated:YES completion:nil];
    
}
-(void)deleteChatCall:(NSString *)vID deleteFrom:(NSString *)deleteFrom{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"deleteChatMessage",@"method",
                              token,@"session_token",vID,@"post_id",self.frndId,@"friend_id",deleteFrom,@"delete_from",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1){
                
            }
        }
    }];
}
- (IBAction)btnBack:(id)sender {
    
    if(_isPopToRoot) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else
        [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)reloadChat:(id)sender {
    
    chatArray = [[NSMutableArray alloc]init];
    _reloadCheck = YES;
    _collectionView.hidden = YES;
    [SVProgressHUD show];
    [self getChats];
}

- (void) getChats{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_CHAT,@"method",
                              token,@"session_token",_frndId,@"friend_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    // _homeRefreshBtn.hidden = YES;
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [SVProgressHUD dismiss];
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                NSMutableArray *postArray =[[NSMutableArray alloc]init];
                postArray = [result objectForKey:@"posts"];
                for(NSDictionary *tempDict in postArray){
                    VideoModel *_Videos     = [[VideoModel alloc] init];
                    _Videos.title           = [tempDict objectForKey:@"caption"];
                    _Videos.comments_count  = [tempDict objectForKey:@"comment_count"];
                    _Videos.userName        = [tempDict objectForKey:@"full_name"];
                    _Videos.topic_id        = [tempDict objectForKey:@"topic_id"];
                    _Videos.user_id         = [tempDict objectForKey:@"user_id"];
                    _Videos.profile_image   = [tempDict objectForKey:@"profile_link"];
                    _Videos.like_count      = [tempDict objectForKey:@"like_count"];
                    _Videos.like_by_me      = [tempDict objectForKey:@"liked_by_me"];
                    _Videos.seen_count      = [tempDict objectForKey:@"seen_count"];
                    _Videos.video_angle     = [[tempDict objectForKey:@"video_angle"] intValue];
                    _Videos.video_link      = [tempDict objectForKey:@"video_link"];
                    _Videos.m3u8_video_link = [tempDict objectForKey:@"m3u8_video_link"];
                    _Videos.video_thumbnail_gif = [tempDict objectForKey:@"video_thumbnail_gif"];
                    _Videos.video_thumbnail_link = [tempDict objectForKey:@"video_thumbnail_link"];
                    _Videos.videoID         = [tempDict objectForKey:@"id"];
                    _Videos.Tags            = [tempDict objectForKey:@"tag_friends"];
                    _Videos.video_length    = [tempDict objectForKey:@"video_length"];
                    _Videos.is_anonymous    = [tempDict objectForKey:@"is_anonymous"];
                    _Videos.reply_count     = [tempDict objectForKey:@"reply_count"];
                    _Videos.current_datetime= [tempDict objectForKey:@"current_datetime"];
                    _Videos.uploaded_date   = [tempDict objectForKey:@"uploaded_date"];
                    _Videos.isMute          = @"0";
                    _Videos.privacy         = [tempDict objectForKey:@"privacy"];
                    [chatArray addObject:_Videos];
                    
                }
                [self addOfflineBeams];
                _collectionView.hidden = NO;
                [_collectionView reloadData];
                
                if(!isForRefreshOnly)
                {
                    NSInteger section = [self.collectionView numberOfSections] - 1;
                    NSInteger item = [self.collectionView numberOfItemsInSection:section] ;
                    if(!_reloadCheck) {
                        if(item > 3) {
                            CGPoint bottomOffset = CGPointMake(0, self.collectionView.contentSize.height - 120+(chatArray.count - 3)  * 164);
                            [self.collectionView setContentOffset:bottomOffset animated:YES];
                        }
                        else if (item==3){
                            CGPoint bottomOffset = CGPointMake(0, self.collectionView.contentSize.height -120+(chatArray.count)  * 100);
                            [self.collectionView setContentOffset:bottomOffset animated:YES];
                            
                        }
                    }
                }
                if(_isUpload) {
                    [self uploadBeam:_movieData];
                    _isUpload = NO;
                    _isPopToRoot = YES;
                }
                if(_isUploadAudio) {
                    [self uploadAduio:_movieData];
                    _isUploadAudio = NO;
                    _isPopToRoot = YES;
                }
            }
            [self updateNotificationsArray];
        }
        else {
            
        }
    }];
}

-(void) updateNotificationsArray {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    for(int i=0; i<sharedManager.notificationsArray.count; i++) {
        NotificationsModel *nModel = [sharedManager.notificationsArray objectAtIndex:i];
        
        if([nModel.notificationType isEqualToString:@"VIDEO_MESSAGE"] && [nModel.seen isEqualToString:@"0"] && [nModel.friend_ID isEqualToString:_frndId])
        {
            appDel.unseenCount--;
            nModel.seen = @"1";
        }
    }
}

- (IBAction)recorderTapped:(id)sender {
    _closeBtnAudio.hidden = true;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                        error:nil];
    secondsLeft = 60;
    if(!isRecording){
        [self animateImages];
        timerToupdateLbl = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
        audioTimeOut = [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self
                                                      selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: NO];
        [_audioRecorder record];
    }
    else{
        [_audioRecorder stop];
        [_audioBtnImage stopAnimating];
    }
    isRecording = true;
}

- (IBAction)AudioClosePressed:(id)sender {
    beam.enabled= YES;
    audio.enabled = YES;
    _uploadAudioView.hidden = YES;
}

-(void)sendAudio:(UIButton*)sender{
    if(isFriend){
        _uploadAudioView.hidden = NO;
    }else{
        [self presentActionAlert];
    }
//    beam.enabled= NO;
//    audio.enabled = NO;
}

-(void)sendVideo:(UIButton*)sender{
    
//    beam.enabled= NO;
//    audio.enabled = NO;
    if(isFriend){
        //  _thumbnailImageView.image = [UIImage imageNamed: @"splash_audio_image.png"];
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
//            UIImagePickerController *picker = [[UIImagePickerController alloc]init];
//            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//            picker.delegate = self;
//            picker.allowsEditing = NO;
//
//            NSArray *mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
//
//            picker.mediaTypes = mediaTypes;
//            picker.videoQuality = UIImagePickerControllerQualityTypeMedium;
//            picker.videoMaximumDuration = 60;
//
//            [self presentViewController:picker animated:YES completion:nil];
            NSBundle *bundle = [NSBundle bundleForClass:HBRecorder.class];
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HBRecorder.bundle/HBRecorder" bundle:bundle];
            
            HBRecorder *recorder = [sb instantiateViewControllerWithIdentifier:@"HBRecorder"];
            recorder.delegate = self;
            recorder.topTitle = @"";
            recorder.bottomTitle = @"";
            recorder.maxRecordDuration = 60 * 1;
            recorder.movieName = @"MyAnimatedMovie";
            
            
            recorder.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self.navigationController pushViewController:recorder animated:YES];
//            [self presentViewController:recorder animated:YES completion:nil];
            
        } else {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"I'm afraid there's no camera on this device!" delegate:nil cancelButtonTitle:@"Dang!" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    else{
        [self presentActionAlert];
    }
}
-(void)presentActionAlert{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:NSLocalizedString(@"notFriendsError", nil)
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ViewProfile = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"view_profile", nil)
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      UserChannel *commentController;
                                      if(IS_IPAD){
                                          commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
                                      }else if (IS_IPHONEX){
                                          commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
                                      }
                                      else
                                          commentController = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
                                      commentController.ChannelObj = nil;
                                      commentController.friendID   = _frndId;
                                      [[self navigationController] pushViewController:commentController animated:YES];
                                      [view dismissViewControllerAnimated:YES completion:nil];
                                      
                                  }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"ok", nil)
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    [view addAction:ViewProfile];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}


#pragma mark - HBRECORDER Recording Methods

- (void)recorder:(HBRecorder *)recorder  didFinishPickingMediaWithUrl:(NSURL *)videoUrl {
    NSURL *chosenMovie = videoUrl;
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:chosenMovie options:nil];
    
    NSTimeInterval durationInSeconds = 0.00;
    if (asset)
        durationInSeconds = CMTimeGetSeconds(asset.duration);
    
    NSUInteger dTotalSeconds = durationInSeconds;
    
    NSUInteger dSeconds =(dTotalSeconds  % 60);
    NSUInteger dMinutes = (dTotalSeconds / 60 ) % 60;
    
    _video_duration = [[NSString alloc]initWithFormat:@"%02lu:%02lu",(unsigned long)dMinutes,(unsigned long)dSeconds];
    
    _movieData = [NSData dataWithContentsOfURL:chosenMovie];
    
    // and dismiss the picker
    [self dismissViewControllerAnimated:YES completion:nil];
    
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = [asset duration];
    time.value = 0;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    
    CGImageRelease(imageRef);
    //_thumbnailImageView.image = thumbnail;
    _profileData = UIImagePNGRepresentation(thumbnail);
    [self uploadBeam:_movieData];
    
    
    
}

- (void)recorderDidCancel:(HBRecorder *)recorder {
    //    NSLog(@"Recorder did cancel..");
}

- (void)recorderOrientation:(NSInteger)orientation {
    
}

#pragma mark - Delegate Methods

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    beam.enabled= YES;
    audio.enabled = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSURL *chosenMovie = [info objectForKey:UIImagePickerControllerMediaURL];
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:chosenMovie options:nil];
    
    NSTimeInterval durationInSeconds = 0.00;
    if (asset)
        durationInSeconds = CMTimeGetSeconds(asset.duration);
    
    NSUInteger dTotalSeconds = durationInSeconds;
    
    NSUInteger dSeconds =(dTotalSeconds  % 60);
    NSUInteger dMinutes = (dTotalSeconds / 60 ) % 60;
    
    _video_duration = [[NSString alloc]initWithFormat:@"%02lu:%02lu",(unsigned long)dMinutes,(unsigned long)dSeconds];
    
    _movieData = [NSData dataWithContentsOfURL:chosenMovie];
    
    // and dismiss the picker
    [self dismissViewControllerAnimated:YES completion:nil];
    
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = [asset duration];
    time.value = 0;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    
    CGImageRelease(imageRef);
    //_thumbnailImageView.image = thumbnail;
    _profileData = UIImagePNGRepresentation(thumbnail);
    [self uploadBeam:_movieData];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Upload Beam

- (NSString *)saveVideoInDocumentsAndReturnPath {
    
    NSData *videoData = nil;
    if(isAudio)
    {
        videoData = audioData;
    }
    else
    {
        videoData = _movieData;
    }
    double fileSize = (float)videoData.length/1024.0f/1024.0f;
    NSLog(@"Old File size is : %.2f MB",fileSize);
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy||HH:mm:SS"];
    NSDate *now = [[NSDate alloc] init] ;
    NSString *theDate = [dateFormat stringFromDate:now];
    theDate = [theDate stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    NSString *videopathForInnerUse;
    if(isAudio) {
        videopathForInnerUse= [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@/%@.wav",documentsDirectory,theDate]] ;
    } else {
        videopathForInnerUse= [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@/%@.mov",documentsDirectory,theDate]] ;
    }
//    _offlineFilePath = videopathForInnerUse;
//    [[NSUserDefaults standardUserDefaults] setObject:_offlineFilePath forKey:@"fileUrl"];
  
    BOOL success = [videoData writeToFile:videopathForInnerUse atomically:NO];
    
    NSLog(@"Successs:::: %@", success ? @"YES" : @"NO");
    NSLog(@"video path --> %@",videopathForInnerUse);
    if([videopathForInnerUse rangeOfString:@".wav"].length > 0)
    {
        return [NSString stringWithFormat:@"%@.wav",theDate];
    }
    return [NSString stringWithFormat:@"%@.mov",theDate];
    
}

- (NSString *)saveThumbnailInDocuments {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy||HH:mm:SS"];
    NSDate *now = [[NSDate alloc] init] ;
    NSString *theDate = [NSString stringWithFormat:@"%@.png",[dateFormat stringFromDate:now]];
    theDate = [theDate stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:theDate]; //Add the file name
    [_profileData writeToFile:filePath atomically:YES];
    [[NSUserDefaults standardUserDefaults] setObject:filePath forKey:@"thumbnailUrl"];
    return theDate;
}

-(NSString *) getCurrentTimeForOfflineDB{
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    
    return dateString;
}

-(void)updateProgressBar:(NSNotification*)notification
{
    NSDictionary *prog = notification.object;
    float progres = [prog[@"progress"] floatValue];
    NSString *targetTimeStamp = prog[@"index"];

    int currentIndex = 0;

    for(int i = 0; i < chatArray.count; i ++)
    {
        VideoModel *eachVideo = [chatArray objectAtIndex:i];
        NSDate *dateObj = [self getDateFromString:eachVideo.current_datetime];
        NSTimeInterval timeStamp1 = [dateObj timeIntervalSince1970];
        NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp1];
        NSString *eachTimeStamp = timeStampObj.stringValue;
        if([eachTimeStamp isEqualToString:targetTimeStamp])
        {
            currentIndex = i;
            break;
        }
    }
        
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
    
    ChatLeftCell *currentCell = (ChatLeftCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    
    [currentCell.cellProgress setProgress:progres];
    currentCell.exclamation.hidden = true;
    currentCell.redBG.hidden = true;
}

-(NSDate *) getDateFromString :(NSString *)targetString
{
    NSDateFormatter *formatter;
    NSDate        *dateObj;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    dateObj = [formatter dateFromString:targetString];
    
    return dateObj;
}

-(void)completeProgress:(NSNotification*)notification
{
    NSDictionary *prog = notification.object;
    
    NSString *targetTimeStamp = prog[@"index"];
    
    int currentIndex = 0;
    
    for(int i = 0; i < chatArray.count; i ++)
    {
        VideoModel *eachVideo = [chatArray objectAtIndex:i];
        NSDate *dateObj = [self getDateFromString:eachVideo.current_datetime];
        NSTimeInterval timeStamp1 = [dateObj timeIntervalSince1970];
        NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp1];
        NSString *eachTimeStamp = timeStampObj.stringValue;
        if([eachTimeStamp isEqualToString:targetTimeStamp])
        {
            currentIndex = i;
            break;
        }
    }
    
    NSIndexPath *indexPath ;
    indexPath = [NSIndexPath indexPathForItem:currentIndex inSection:0];
    ChatLeftCell *currentCell = (ChatLeftCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    [currentCell.cellProgress removeFromSuperview];
    
    
    
    
    NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
    if(offlineArray.count == 0)
    {
        [chatArray removeAllObjects];
        isForRefreshOnly = true;
        [self getChats];
    }
}

-(void) uploadBeam :(NSData*)file {
    _isUploading = YES;
    isAudio = false;
    
    VideoModel *videos          = [[VideoModel alloc] init];
    videos.userName             = userName;
    videos.user_id              = userId;
    videos.profileImageData     = _profileData;
    videos.isLocal = true;
    videos.uploaded_date = [self getCurrentTimeStamp];
    videos.current_datetime = [self getCurrentTimeStamp];
    [chatArray addObject:videos];
    [_collectionView reloadData];
    
    NSString *anony = @"0";

    
    
    
    
    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
    NSString *langCode = [Utils getLanguageCode];
    
    NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
    NSString *thumbnailPath =  [self saveThumbnailInDocuments];
    [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:@"-1" caption:@"" isAnonymous:anony isMute:@"0" videoLength:_video_duration postID:@"-1" parentCommentID:@"-1" privacy:_frndId language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"0" isComment:@"0" fromGallery:false withThumbnail:thumbnailPath];
    [[DBCommunicator sharedManager] closeDB];
    
    
    
    NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
    BeamUploadVC *vc = [[BeamUploadVC alloc] init];
    DataContainer *shareobj = [DataContainer sharedManager];
    shareobj.offlineArrayCount = offlineArray.count;
    vc.offlineUploadCount = offlineArray.count;
    [vc uploadOfflineBeam];
    
    
    
    
    
//    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
//
//    NSMutableDictionary *postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:METHOD_UPLOAD_STATUS,@"method",userSession,@"Session_token",@"-1",@"reply_count",@"",@"caption",anony,@"is_anonymous",@"0",@"mute",_video_duration,@"video_length",@"-1",@"post_id",@"-1",@"parent_comment_id",_frndId,@"privacy",@"back",@"rotation_flag",nil];
//
//    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:SERVER_URL parameters:postDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFileData:file name:@"video_link" fileName:[NSString stringWithFormat:@"%@.mp4",@"video"] mimeType:@"recording/video"];
//        [formData appendPartWithFileData:_profileData name:@"video_thumbnail_link" fileName:[NSString stringWithFormat:@"%@.png",@"thumbnail"] mimeType:@"image/png"];
//    } error:nil];
//    [[NSUserDefaults standardUserDefaults] setObject:_profileData forKey:@"chatThumbnail"];
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
//    NSURLSessionUploadTask *uploadTask;
//    uploadTask = [manager
//                  uploadTaskWithStreamedRequest:request
//                  progress:^(NSProgress * _Nonnull uploadProgress) {
//                      dispatch_async(dispatch_get_main_queue(), ^{
//                          NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:chatArray.count-1 inSection:0];
//                          ChatLeftCell *cell = (ChatLeftCell *)[self.collectionView cellForItemAtIndexPath:indexPath1];
//                          NSData *tempData = [[NSUserDefaults standardUserDefaults] objectForKey:@"chatThumbnail"];
//
//                          cell.videoImg.image = [UIImage imageWithData:tempData];
//                          if(uploadingProgress <= 100.0  && uploadingProgress > 0.95){
//
//
//                          } else {
//                              [self.progressView setProgress:uploadProgress.fractionCompleted];
//                          }
//
//                      });
//                  }
//                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//                      if (error) {
//                      } else {
//
//
//                          NSDictionary *dict = [responseObject objectForKey:@"post"];
//                          VideoModel *_Videos = [[VideoModel alloc] init];
//                          _Videos.title             = [dict objectForKey:@"caption"];
//                          _Videos.comments_count    = [dict objectForKey:@"comment_count"];
//                          _Videos.userName          = [dict objectForKey:@"full_name"];
//                          _Videos.topic_id          = [dict objectForKey:@"topic_id"];
//                          _Videos.user_id           = [dict objectForKey:@"user_id"];
//                          _Videos.profile_image     = [dict objectForKey:@"profile_link"];
//                          _Videos.like_count        = [dict objectForKey:@"like_count"];
//                          _Videos.like_by_me        = [dict objectForKey:@"liked_by_me"];
//                          _Videos.seen_count        = [dict objectForKey:@"seen_count"];
//                          _Videos.video_angle       = [[dict objectForKey:@"video_angle"] intValue];
//                          _Videos.video_link        = [dict objectForKey:@"video_link"];
//                          _Videos.m3u8_video_link   = [dict objectForKey:@"m3u8_video_link"];
//                          _Videos.video_thumbnail_link = [dict objectForKey:@"video_thumbnail_link"];
//                          _Videos.videoID           = [dict objectForKey:@"id"];
//                          _Videos.Tags              = [dict objectForKey:@"tag_friends"];
//                          _Videos.video_length      = [dict objectForKey:@"video_length"];
//                          _Videos.is_anonymous      = [dict objectForKey:@"is_anonymous"];
//                          _Videos.reply_count       = [dict objectForKey:@"reply_count"];
//                          _Videos.current_datetime  = [dict objectForKey:@"current_datetime"];
//                          _Videos.uploaded_date     = [dict objectForKey:@"uploaded_date"];
//                          _Videos.isLocal = false;
//
////                          [chatArray replaceObjectAtIndex:chatArray.count-1 withObject:_Videos];
////
////
////                          NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:chatArray.count-1 inSection:0];
////                          // Add them in an index path array
////                          NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, nil];
////                          // Launch reload for the two index path
////
////                          [self.collectionView reloadItemsAtIndexPaths:indexArray];
//
//                          if(dict != nil)
//                          {
//                              [chatArray replaceObjectAtIndex:chatArray.count-1 withObject:_Videos];
//                              NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:chatArray.count-1 inSection:0];
//                              // Add them in an index path array
//                              NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, nil];
//                              // Launch reload for the two index path
//                              [self.collectionView reloadItemsAtIndexPaths:indexArray];
//                              ChatLeftCell *cell = (ChatLeftCell *)[self.collectionView cellForItemAtIndexPath:indexPath1];
//                              NSData *tempData = [[NSUserDefaults standardUserDefaults] objectForKey:@"chatThumbnail"];
//
//                              cell.videoImg.image = [UIImage imageWithData:tempData];
//                          }
//                          else
//                          {
//                              self.isFriend = false;
//                              [chatArray removeObjectAtIndex:chatArray.count -1];
//                              [self.collectionView reloadData];
//
//                          }
//
//                          beam.enabled= YES;
//                          audio.enabled = YES;
//
//
////                          cell.videoImg.hidden = YES;
//                      }
//                  }];
//    [uploadTask resume];
    
}

-(void)uploadOfflineBeamToServer
{
    
}

-(void) callAfterSixtySecond:(NSTimer*) t
{
    [_audioRecorder stop];
    [timerToupdateLbl invalidate];
    [audioTimeOut invalidate];
    
}
-(void) updateCountdown {
    int minutes, seconds;
    secondsLeft--;
    minutes = (secondsLeft / 60) % 60;
    seconds = (secondsLeft) % 60;
    _countDownlabel.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    _secondsConsumed = [NSString stringWithFormat:@"%02d:%02d", 00, 60 - secondsLeft];
}
-(void)animateImages{
    NSArray *loaderImages = @[@"state1.png", @"state2.png", @"state3.png"];
    NSMutableArray *loaderImagesArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < loaderImages.count; i++) {
        [loaderImagesArr addObject:[UIImage imageNamed:[loaderImages objectAtIndex:i]]];
    }
    _audioBtnImage.animationImages = loaderImagesArr;
    _audioBtnImage.animationDuration = 0.5f;
    [_audioBtnImage startAnimating];
}
-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    [timerToupdateLbl invalidate];
    [audioTimeOut invalidate];
    _closeBtnAudio.hidden = false;
    isRecording = false;
    _countDownlabel.text = @"01:00";
    secondsLeft = 60;
    audioData = [NSData dataWithContentsOfURL:_audioRecorder.url];
    if([_secondsConsumed length] == 0)
        _secondsConsumed = @"00:01";
    //    uploadController.video_duration = secondsConsumed;
    //    uploadController.ParentCommentID = @"-1";
    //    uploadController.postID = @"-1";
    //    uploadController.isAudio = true;
    //    uploadController.friendsArray = friendsArray;
    //    thumbnail = [UIImage imageNamed: @"splash_audio_image.png"];
    //    uploadController.thumbnailImage = [UIImage imageNamed: @"splash_audio_image.png"];
    //    [[self navigationController] pushViewController:uploadController animated:YES];
    
    //[self.view addSubview:_uploadBeamView];
    [self uploadAduio:audioData];
}

-(void)audioRecorderEncodeErrorDidOccur:
(AVAudioRecorder *)recorder
                                  error:(NSError *)error
{  isRecording = false;
    _closeBtnAudio.hidden = false;
    _countDownlabel.text = @"00:00";
    secondsLeft = 60;
    _secondsConsumed = 0;
    NSLog(@"Encode Error occurred");
}
#pragma mark Upload Audio
-(void)uploadAduio:(NSData*)file{
    
    isAudio = true;
    _uploadAudioView.hidden = YES;
    
    VideoModel *videos     = [[VideoModel alloc] init];
    videos.userName        = userName;
    videos.user_id         = userId;
    UIImage *thumbnail = [UIImage imageNamed:@"splash_audio_image"];
    
    audioprofileData = UIImagePNGRepresentation(thumbnail);
    
    videos.profileImageData   = audioprofileData;
    videos.isLocal = true;
    videos.uploaded_date = [self getCurrentTimeStamp];
    videos.current_datetime = [self getCurrentTimeStamp];
    [chatArray addObject:videos];
    [_collectionView reloadData];
    
    NSString *anony = @"0";

    
    
    
    
    
    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
    NSString *langCode = [Utils getLanguageCode];

    NSString *latestPath = [self saveVideoInDocumentsAndReturnPath];
    
    NSString *thumbnailPath =  [self saveThumbnailInDocuments];
    [[DBCommunicator sharedManager] newEntryWithSession:userSession replyCount:@"-1" caption:@"" isAnonymous:anony isMute:@"0" videoLength:_secondsConsumed postID:@"-1" parentCommentID:@"-1" privacy:_frndId language:langCode timeStamp:[self getCurrentTimeForOfflineDB] mediaData:latestPath isAudio:@"1" isComment:@"0" fromGallery:false withThumbnail:thumbnailPath];
    [[DBCommunicator sharedManager] closeDB];
    
    
    NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
    BeamUploadVC *vc = [[BeamUploadVC alloc] init];
    DataContainer *shareobj = [DataContainer sharedManager];
    shareobj.offlineArrayCount = offlineArray.count;
    vc.offlineUploadCount = offlineArray.count;
    [vc uploadOfflineBeam];
    
    
    
    
    
    
//    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
//
//    NSString *anony = @"0";
//    NSString *userSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"session_token"];
//
//
//    NSMutableDictionary *postDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:METHOD_UPLOAD_STATUS,@"method",userSession,@"Session_token",@"-1",@"reply_count",@"",@"caption",anony,@"is_anonymous",@"0",@"mute",_secondsConsumed,@"video_length",@"-1",@"post_id",@"-1",@"parent_comment_id",_frndId,@"privacy",nil];
//
//
//    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:SERVER_URL parameters:postDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFileData:file name:@"audio_link" fileName:[NSString stringWithFormat:@"%@.wav",@"sound"] mimeType:@"audio/wav"];
//
//    } error:nil];
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
//    NSURLSessionUploadTask *uploadTask;
//    uploadTask = [manager
//                  uploadTaskWithStreamedRequest:request
//                  progress:^(NSProgress * _Nonnull uploadProgress) {
//                      dispatch_async(dispatch_get_main_queue(), ^{
//                          [self.progressView setProgress:uploadProgress.fractionCompleted];
//                      });
//                  }
//                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//                      if (error) {
//                          NSLog(@"Error handling");
//                      } else {
//
//                          NSDictionary *dict = [responseObject objectForKey:@"post"];
//                          VideoModel *_Videos = [[VideoModel alloc] init];
//                          _Videos.title             = [dict objectForKey:@"caption"];
//                          _Videos.comments_count    = [dict objectForKey:@"comment_count"];
//                          _Videos.userName          = [dict objectForKey:@"full_name"];
//                          _Videos.topic_id          = [dict objectForKey:@"topic_id"];
//                          _Videos.user_id           = [dict objectForKey:@"user_id"];
//                          _Videos.profile_image     = [dict objectForKey:@"profile_link"];
//                          _Videos.like_count        = [dict objectForKey:@"like_count"];
//                          _Videos.like_by_me        = [dict objectForKey:@"liked_by_me"];
//                          _Videos.seen_count        = [dict objectForKey:@"seen_count"];
//                          _Videos.video_angle       = [[dict objectForKey:@"video_angle"] intValue];
//                          _Videos.video_link        = [dict objectForKey:@"video_link"];
//                          _Videos.m3u8_video_link   = [dict objectForKey:@"m3u8_video_link"];
//
//                          _Videos.video_thumbnail_link = [dict objectForKey:@"video_thumbnail_link"];
//                          _Videos.videoID           = [dict objectForKey:@"id"];
//                          _Videos.Tags              = [dict objectForKey:@"tag_friends"];
//                          _Videos.video_length      = [dict objectForKey:@"video_length"];
//                          _Videos.is_anonymous      = [dict objectForKey:@"is_anonymous"];
//                          _Videos.reply_count       = [dict objectForKey:@"reply_count"];
//                          _Videos.current_datetime  = [dict objectForKey:@"current_datetime"];
//                          _Videos.uploaded_date     = [dict objectForKey:@"uploaded_date"];
//                          _Videos.beam_share_url    = [dict objectForKey:@"beam_share_url"];
//                          _Videos.isLocal = false;
//
//                          if(dict != nil)
//                          {
//                              [chatArray replaceObjectAtIndex:chatArray.count-1 withObject:_Videos];
//                              NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:chatArray.count-1 inSection:0];
//                              // Add them in an index path array
//                              NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, nil];
//                              // Launch reload for the two index path
//                              [self.collectionView reloadItemsAtIndexPaths:indexArray];
//                          }
//                          else
//                          {
//                              self.isFriend = false;
//                              [chatArray removeObjectAtIndex:chatArray.count -1];
//                              [self.collectionView reloadData];
//
//                          }
//
//
//                          beam.enabled= YES;
//                          audio.enabled = YES;
//                      }
//                  }];
//    [uploadTask resume];
}
#pragma mark AUDIO RECORDING AND UPLOADING

-(void)setAudioRecordSettings
{
    NSArray *dirPaths;
    NSString *docsDir;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(
                                                   NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString *soundFilePath = [docsDir
                               stringByAppendingPathComponent:@"sound.caf"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    //[NSNumber numberWithInt:kAudioFormatMPEGLayer3], AVFormatIDKey,
                                    [NSNumber numberWithInt:AVAudioQualityHigh],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 1],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    NSError *error = nil;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                        error:nil];
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];
    _audioRecorder.delegate = self;
    if (error)
    {
        
        
    } else {
        [_audioRecorder prepareToRecord];
    }
}

- (void) playBeam : (id) sender {
    UIButton *btnSender = (UIButton*)sender;
    
    VideoModel *videoObj = [[VideoModel alloc]init];
    videoObj  = (VideoModel *)[chatArray objectAtIndex:btnSender.tag];
    if(videoObj.video_link != nil) {
        PlayerViewController *videoPlayer ;  //= [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
        if (IS_IPHONEX){
            videoPlayer = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController_IphoneX" bundle:nil];
        }else{
            videoPlayer = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
        }
    videoPlayer.vModel       = videoObj;
    videoPlayer.isComment       = FALSE;
    videoPlayer.isfromChat = YES;
        
    videoPlayer.postArray = videoObj;
    videoPlayer.cPostId = videoObj.videoID;
    videoPlayer.isFirstComment = false;
        
        
        
    [[self navigationController] pushViewController:videoPlayer animated:YES];
    }
}

#pragma mark ASI delegates
- (void)request:(ASIHTTPRequest *)request incrementUploadSizeBy:(long long)newLength {
    //    NSLog(@"data length: %lld", newLength);
    //
}
- (void)requestFinished:(ASIHTTPRequest *)request
{
    //[SVProgressHUD dismiss];
    //[self.navigationController popViewControllerAnimated:NO];
}

@end
