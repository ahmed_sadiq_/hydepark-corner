//
//  HomeViewController.h
//  HydePark
//
//  Created by Osama on 29/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyCornerVC.h"
#import "FriendsCornerVC.h"
#import "SpeakersCornerVC.h"
#import "LTPageManagerViewController.h"
#import "TabbarView.h"
#import "DataContainer.h"
#import "AppDelegate.h"
//#import "StreamViewController.h"

@interface HomeViewController : LTPageManagerViewController<TabbarViewDelegate,UIImagePickerControllerDelegate>{
    BOOL uploadBeamTag;
    BOOL uploadAnonymous;
    NSString *video_duration;
    NSInteger orientationAngle;
    BOOL isFromGallery;
    DataContainer *sharedManager;
    AppDelegate *appDelegate;
}
- (id) initExampleViewController;
@property (weak, nonatomic) IBOutlet TabbarView *tbView;
@property (weak, nonatomic) IBOutlet UIImageView *bgView;
@property (strong, nonatomic) NSData *CmovieData;
@property (strong, nonatomic) NSData *profileData;
@property (strong, nonatomic)  NSString *url;

@property (strong, nonatomic) MyCornerVC *myCornerVC;
@property (strong, nonatomic) FriendsCornerVC *friendsVC;
@property (strong, nonatomic) SpeakersCornerVC *speakersVC;

@property (strong, nonatomic) UIImage *thumbnailToUpload;
@property (strong, nonatomic) UIImage *thumbnailToUpload2;
@property (strong, nonatomic) UIImage *thumbnailToUpload3;
@end
