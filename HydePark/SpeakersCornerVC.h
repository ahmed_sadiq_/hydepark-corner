//
//  SpeakersCornerVC.h
//  HydePark
//
//  Created by Osama on 02/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataContainer.h"
#import "NewHomeCells.h"

@interface SpeakersCornerVC : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource,NSURLConnectionDataDelegate>{
    NSUInteger currentIndexHome;
    DataContainer *sharedManager;
    UIView * footerViewHome;
    BOOL cannotscroll;
    NSUInteger currentSelectedIndex;
    NewHomeCells *targetCellForPopover;
    NSMutableArray *popOverFrames;
    IBOutlet UIActivityIndicatorView *indicator;
    NSMutableDictionary *postDict;
    NSData *imageData;
    NSDate *startDate;
    BOOL isScrolling;
}
@property (nonatomic, weak) IBOutlet UICollectionView *collectionHome;
@property (nonatomic, weak) IBOutlet UITableView *tblHome;
@property (nonatomic, weak) IBOutlet UICollectionView *storiesCollectionView;
@property (nonatomic, assign) BOOL fetchingContent;
@property (nonatomic, assign) BOOL isDownwards;
@property (nonatomic, assign) BOOL playGif;
@property (nonatomic, strong) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *footerIndicator;
@end
