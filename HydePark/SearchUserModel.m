//
//  SearchUserModel.m
//  HydePark
//
//  Created by apple on 6/23/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import "SearchUserModel.h"

@implementation SearchUserModel
@synthesize full_name,user_id,is_celeb,profile_link,profile_type;
- (id)initWithDictionary:(NSDictionary *) responseData
{
    self = [super init];
    if (self) {
        self.user_id = [responseData objectForKey:@"id"];
        self.full_name = [responseData objectForKey:@"full_name"];
        self.is_celeb = [responseData objectForKey:@"is_celeb"];
        self.profile_link = [responseData objectForKey:@"profile_link"];
        self.profile_type = [responseData objectForKey:@"profile_type"];
    }
    return self;
}
@end
