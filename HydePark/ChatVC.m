//
//  ChatVC.m
//  HydePark
//
//  Created by Samreen Noor on 28/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ChatVC.h"
#import "ChatCell.h"
#import "FriendChatVC.h"
#import "DataContainer.h"
#import "Followings.h"
#import "NotificationsModel.h"
#import "UIImageView+AFNetworking.h"
#import "Constants.h"
#import "Utils.h"
#import "FriendChat.h"
#import "PopularUsersVC.h"
#import "CreateNewChat.h"
#import "NotificationsCell.h"
#import "UIImageView+RoundImage.h"
#import "NSData+AES.h"

@interface ChatVC ()
{
    DataContainer *sharedManager;
    CGRect screenRect;
    CGFloat screenWidth;
}
@end

@implementation ChatVC

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    sharedManager = [DataContainer sharedManager];
    if(sharedManager.isListView) {
        _collectionView.hidden = YES;
        _tableView.hidden = NO;
        _centrLine.hidden = YES;
    } else {
        _tableView.hidden = YES;
        _collectionView.hidden = NO;
    }
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if(IS_IPAD){
        [self.collectionView registerNib:[UINib nibWithNibName:@"ChatCell_Ipad" bundle:nil] forCellWithReuseIdentifier:@"Chatcell"];
    }
    else{
        [self.collectionView registerNib:[UINib nibWithNibName:@"ChatCell" bundle:nil] forCellWithReuseIdentifier:@"Chatcell"];
    }
    [self.tableView reloadData];
    [self.collectionView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addrefreshControl];
    sharedManager = [DataContainer sharedManager];
    if(sharedManager.isListView) {
        _collectionView.hidden = YES;
        _tableView.hidden = NO;
    } else {
        _tableView.hidden = YES;
        _collectionView.hidden = NO;
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.background.hidden = YES;
            _centrLine.backgroundColor = [UIColor lightGrayColor];
            _centrLine.image = nil;
            _centrLine.frame = CGRectMake( _centrLine.frame.origin.x,  _centrLine.frame.origin.y, 1,  _centrLine.frame.size.height);
        }
        else
        {
            self.background.hidden = NO;
        }
    }
    else
    {
        self.background.hidden = NO;
    }
    
    _friends = [[NSMutableArray alloc]init];
    _centrLine.hidden = YES;
    _searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"search_inbox", @"")
                                                                         attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    if(IS_IPAD){
        [self.collectionView registerNib:[UINib nibWithNibName:@"ChatCell_Ipad" bundle:nil] forCellWithReuseIdentifier:@"Chatcell"];
    }
    else{
        [self.collectionView registerNib:[UINib nibWithNibName:@"ChatCell" bundle:nil] forCellWithReuseIdentifier:@"Chatcell"];
    }
    [self getFriends];
    
    if(sharedManager.notifcationsFetcedFirst) {
        [self getNotigications];
    }
    else {
        [self assignChatCounts];
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"chatCount" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray;
        sortedArray = [_friends sortedArrayUsingDescriptors:sortDescriptors];
        
        _friends = [[NSMutableArray alloc] initWithArray:sortedArray];
    }
    if(sharedManager.isListView) {
        _collectionView.hidden = YES;
        _tableView.hidden = NO;
        _centrLine.hidden = YES;
    } else {
        _tableView.hidden = YES;
        _collectionView.hidden = NO;
        _centrLine.hidden = NO;
    }
}
-(void)viewDidLayoutSubviews{
    
    screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    if (_friends.count>0) {
        if (_friends.count%2==0) {
            if(sharedManager.isListView)
                _centrLine.hidden = YES;
            else
                _centrLine.hidden = NO;
            if(IS_IPAD){
                _centrLine.frame = CGRectMake(screenWidth/2,76, 1,(_friends.count/2)*219);
            }else if (IS_IPHONEX){
                
                if (_friends.count>4){
                      _centrLine.frame = CGRectMake(screenWidth/2,100, 1,668);
                }else{
                    _centrLine.frame = CGRectMake(screenWidth/2,100, 1,(_friends.count/2)*160);
                }

                
            }
            else{
                _centrLine.frame = CGRectMake(screenWidth/2,76, 1,(_friends.count/2)*160);
            }
        }
        else
        {
            if(IS_IPAD){
                _centrLine.frame = CGRectMake(screenWidth/2,76, 1,(1+(_friends.count/2))*219);

            }
            else if (IS_IPHONEX){
                if (_friends.count>4){
                    _centrLine.frame = CGRectMake(screenWidth/2,100, 1,668);
                }else{
                    _centrLine.frame = CGRectMake(screenWidth/2,100, 1,(_friends.count/2)*160);
                }
                
            }
            else{
                _centrLine.frame = CGRectMake(screenWidth/2,76, 1,(1+(_friends.count/2))*160);
            }
            if(sharedManager.isListView)
                _centrLine.hidden = YES;
            else
                _centrLine.hidden = NO;

        }
    }
    else{
        _centrLine.hidden = YES;
    }
    
}
-(void) assignChatCounts {
    for (FriendChat *f in _friends){
        f.chatCount = [self returnCountOfChatForUser:f.friend_id];
    }
    [self.tableView reloadData];
    [self.collectionView reloadData];
}

-(void)viewDidDisappear:(BOOL)animated{
    //  [_friends removeAllObjects];
}
#pragma mark CollectionView Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    if(sharedManager.isListView)
        return 0;
    if(search) {
        return _searchFriends.count;
    }
    else {
        return _friends.count;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    ChatCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Chatcell" forIndexPath:indexPath];
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(deleteChat:)];
    FriendChat *tempUsers = [[FriendChat alloc]init];
    self.noUsersFoundlbl.hidden = YES;
    if(sharedManager.isListView)
        _centrLine.hidden = YES;
    else
        self.centrLine.hidden = NO;
    if(search) {
        tempUsers = [_searchFriends objectAtIndex:indexPath.row];
        if(self.searchFriends.count == 0){
            self.centrLine.hidden = YES;
        }
    }
    else {
        tempUsers = [_friends objectAtIndex:indexPath.row];
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            cell.lblName.textColor = [UIColor blackColor];
        }
        else
        {
            cell.lblName.textColor = [UIColor whiteColor];
        }
    }
    else
    {
        cell.lblName.textColor = [UIColor whiteColor];
    }
    
    
    cell.lblName.text = [Utils getDecryptedTextFor:tempUsers.full_name];
     [cell.userImg setImageWithURL:[NSURL URLWithString:tempUsers.profile_link] placeholderImage:[UIImage imageNamed:@"place_holder"]];
    if([tempUsers.unseen_counts isEqualToString:@"0"]) {
        cell.countBg.hidden = true;
        cell.countLbl.hidden = true;
    }
    else
    {
        cell.countBg.hidden = false;
        cell.countLbl.hidden = false;
        cell.countLbl.text = [NSString stringWithFormat:@"%@",tempUsers.unseen_counts];
        
    }
    if(!search){
       
        lpgr.minimumPressDuration = 1.0;
        [cell.contentView addGestureRecognizer:lpgr];
    }
    lpgr.view.tag = indexPath.row;
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize sz;
    
    if(IS_IPAD){
        sz = CGSizeMake((self.view.frame.size.width/2)-20, 230.0);
    }
    else{
    sz = CGSizeMake((self.view.frame.size.width/2)-20, 170.0);
    }

    return sz;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    FriendChat *tempUsers = [[FriendChat alloc]init];
    if(search) {
        tempUsers = [_searchFriends objectAtIndex:indexPath.row];
    }
    else {
        tempUsers = [_friends objectAtIndex:indexPath.row];
    }
    int i = [tempUsers.is_friend intValue];
    
    FriendChatVC *frndvc; //= [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    
    if (IS_IPHONEX){
        frndvc = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC_IphoneX" bundle:nil];
    }else{
        frndvc = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    }
    
    frndvc.selectedFriend = tempUsers;
    frndvc.isFriend = i;
    [[self navigationController] pushViewController:frndvc animated:YES];
}

#pragma mark TableView Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float returnValue;
    if (IS_IPAD)
        returnValue = 93.0f;
    else
        returnValue = 80.0f;
    
    return returnValue;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(!sharedManager.isListView)
        return 0;
    if(search) {
        return _searchFriends.count;
    }
    else {
        return _friends.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"NotiCell";
    NotificationsCell *cell;
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects;
        if(IS_IPAD){
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NotificationsCell_iPad" owner:self options:nil];
        }else{
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NotificationsCell" owner:self options:nil];
        }
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    else{
        cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    if (IS_IPHONE_6) {
        cell.frame = CGRectMake(0, 0, 375, 667);
        cell.contentView.frame = CGRectMake(0, 0, 375, 667);
    }
    cell.Name.hidden = YES;
    cell.Time.hidden = YES;
    cell.notifyBtn.hidden = YES;
    cell.videoImage.hidden = NO;
    cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
    cell.videoImage.layer.cornerRadius=10;
    cell.videoImage.layer.masksToBounds = YES;
    [cell.notifImage roundImageCorner];
   // [cell.videoImage roundImageCorner];
     cell.bgView.backgroundColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:0.5f];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(deleteChat:)];
    FriendChat *tempUsers = [[FriendChat alloc]init];
    self.noUsersFoundlbl.hidden = YES;

    if(search) {
        tempUsers = [_searchFriends objectAtIndex:indexPath.row];
        if(self.searchFriends.count == 0){
            self.centrLine.hidden = YES;
        }
    }
    else {
        tempUsers = [_friends objectAtIndex:indexPath.row];
    }
    cell.playImage.hidden = NO;
//    cell.message.text = tempUsers.full_name;
    cell.message.text = [Utils getDecryptedTextFor:tempUsers.full_name];

    [cell.notifImage setImageWithURL:[NSURL URLWithString:tempUsers.profile_link] placeholderImage:[UIImage imageNamed:@"place_holder"]];
    if((NSString *) [NSNull null] != tempUsers.thumbnailLink )
        [cell.videoImage setImageWithURL:[NSURL URLWithString:tempUsers.thumbnailLink] placeholderImage:[UIImage imageNamed:@"pH"]];
    else{
        cell.videoImage.hidden = NO;
        cell.playImage.hidden = YES;
    }
    if([tempUsers.unseen_counts isEqualToString:@"0"]) {
        cell.countBg.hidden = true;
        cell.countLbl.hidden = true;
    }
    else
    {
        cell.countBg.hidden = false;
        cell.countLbl.hidden = false;
        cell.countLbl.text = [NSString stringWithFormat:@"%@",tempUsers.unseen_counts];

    }
    if(!search){
        
        lpgr.minimumPressDuration = 1.0;
        [cell.contentView addGestureRecognizer:lpgr];
    }
    lpgr.view.tag = indexPath.row;
    cell.bgView.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
    cell.bgView.layer.shadowOffset  = CGSizeMake(1.0f, 3.0f);
    cell.bgView.layer.shadowOpacity = 2;
    cell.bgView.layer.shadowRadius  = 4.0;
    
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendChat *tempUsers = [[FriendChat alloc]init];
    if(search) {
        tempUsers = [_searchFriends objectAtIndex:indexPath.row];
    }
    else {
        tempUsers = [_friends objectAtIndex:indexPath.row];
    }
    int i = [tempUsers.is_friend intValue];
    
    FriendChatVC *frndvc; //= [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    if (IS_IPHONEX){
        frndvc = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC_IphoneX" bundle:nil];
    }else{
        frndvc = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    }
    frndvc.selectedFriend = tempUsers;
    frndvc.isFriend = i;
    [[self navigationController] pushViewController:frndvc animated:YES];
}
#pragma mark Long press
-(void)deleteChat:(UILongPressGestureRecognizer *)gestureRecognizer
{
    NSInteger tag = gestureRecognizer.view.tag;
    FriendChat *tempUsers = [[FriendChat alloc]init];
    tempUsers = [_friends objectAtIndex:tag];
//    [self deleteChatCall:tempUsers.friend_id];
//    [_friends removeObjectAtIndex:tag];
//    [self setLine];
//    [self.collectionView reloadData];
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:NSLocalizedString(@"delete_conversation", nil)
                                 message:NSLocalizedString(@"delete_chat_message", nil)
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:NSLocalizedString(@"delete", nil)
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             int currentCount = [sharedManager._profile.unreadInboxCount intValue];
                             int selectedCount = [tempUsers.unseen_counts intValue];
                             currentCount -= selectedCount;
                             sharedManager._profile.unreadInboxCount = [NSString stringWithFormat:@"%d",currentCount];
                             sharedManager.isReloadMyCorner = YES;
                             
                             [self deleteChatCall:tempUsers.friend_id];
                             [_friends removeObjectAtIndex:tag];
                             [self setLine];
                             [self.tableView reloadData];
                             if(_friends.count == 0)
                             {
                                 _viewStartChat.hidden = NO;
                             }
                             
                             [self.collectionView reloadData];
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Cancel", nil)
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [view addAction:ok];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}
-(void)setLine{
    if (_friends.count>0) {
        if (_friends.count%2==0) {
            if(sharedManager.isListView)
                _centrLine.hidden = YES;
            else
                _centrLine.hidden = NO;

            if(IS_IPAD){
                _centrLine.frame = CGRectMake(screenWidth/2,76, 1,(_friends.count/2)*219);
            }
            else if (IS_IPHONEX){
                if (_friends.count>4){
                    _centrLine.frame = CGRectMake(screenWidth/2,100, 1,668);
                }else{
                    _centrLine.frame = CGRectMake(screenWidth/2,100, 1,(_friends.count/2)*160);
                }
                
            }
            else{
                _centrLine.frame = CGRectMake(screenWidth/2,76, 1,(_friends.count/2)*160);
            }
        }
        else
        {
            if(IS_IPAD){
                _centrLine.frame = CGRectMake(screenWidth/2,76, 1,(1+(_friends.count/2))*219);
            }
            else if (IS_IPHONEX){
                if (_friends.count>4){
                    _centrLine.frame = CGRectMake(screenWidth/2,100, 1,668);
                }else{
                    _centrLine.frame = CGRectMake(screenWidth/2,100, 1,(_friends.count/2)*160);
                }
                
            }
            else{
                _centrLine.frame = CGRectMake(screenWidth/2,76, 1,(1+(_friends.count/2))*160);
            }
            
            if(sharedManager.isListView)
                _centrLine.hidden = YES;
            else
                _centrLine.hidden = NO;

        }
    }
    else{
        _centrLine.hidden = YES;
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (int) returnCountOfChatForUser : (NSString*) userID {
    int count = 0;
    for(int i=0; i<sharedManager.notificationsArray.count; i++) {
        
        NotificationsModel *nModel = [sharedManager.notificationsArray objectAtIndex:i];
        
        int notificationSeen = [nModel.seen intValue];
        
        
        if([nModel.notificationType isEqualToString:@"VIDEO_MESSAGE"] && notificationSeen == 0 && [nModel.friend_ID isEqualToString:userID])
        {
            count++;
        }
    }
    return count;
}

-(void)addrefreshControl{
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor whiteColor];

    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            refreshControl.tintColor = [UIColor lightGrayColor];
        }
    }
    
    [refreshControl addTarget:self
                       action:@selector(refreshCall)
     
             forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    
}

- (void)refreshCall {
    [self getFriends];
}

#pragma mark Severcalls

-(void)getFriends{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_FRIENDS_CHAT,@"method",
                              token,@"session_token",@"1",@"page_no",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [_friends removeAllObjects];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1){
                NSArray *friendsArray =[result objectForKey:@"threads"];
                
                
                for(NSDictionary *tempDict in friendsArray){
                    FriendChat *responseData = [[FriendChat alloc] init];
                    
                    responseData.friend_id = [tempDict objectForKey:@"friend_id"];
                    responseData.full_name = [tempDict objectForKey:@"full_name"];
                    responseData.total_messages = [tempDict objectForKey:@"total_messages"];
                    responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                    responseData.seen_counts = [tempDict objectForKey:@"seen_counts"];
                    responseData.unseen_counts = [tempDict objectForKey:@"unseen_counts"];
                    responseData.is_friend = [tempDict objectForKey:@"is_friend"];
                    responseData.thumbnailLink = [tempDict objectForKey:@"video_thumbnail_link"];
                    [_friends addObject:responseData];
                    sharedManager.friendsArray = _friends.mutableCopy;
                }
                if (friendsArray.count>0) {
                    [_tableView reloadData];
                    [_collectionView reloadData];
                    if(sharedManager.isListView) {
                        _collectionView.hidden = YES;
                        _tableView.hidden = NO;
                    } else {
                        _collectionView.hidden= NO;
                        _tableView.hidden = YES;
                    }
                    _viewStartChat.hidden=YES;
                    CGRect screenRect = [[UIScreen mainScreen] bounds];
                    CGFloat screenWidth = screenRect.size.width;
                    if (_friends.count>0) {
                        if (_friends.count%2==0) {
                            if(IS_IPAD){
                                _centrLine.frame = CGRectMake(screenWidth/2,76, 2,(_friends.count/2)*219);
                            }
                            else if (IS_IPHONEX){
                                if (_friends.count>4){
                                    _centrLine.frame = CGRectMake(screenWidth/2,100, 2,668);
                                }else{
                                    _centrLine.frame = CGRectMake(screenWidth/2,100, 2,(_friends.count/2)*160);
                                }
                                
                            }
                            else{
                                _centrLine.frame = CGRectMake(screenWidth/2,76, 2,(_friends.count/2)*160);
                            }
                            if(sharedManager.isListView)
                                _centrLine.hidden = YES;
                            else
                                _centrLine.hidden = NO;

                        }
                        else
                        {
                            if(IS_IPAD){
                                _centrLine.frame = CGRectMake(screenWidth/2,76, 2,(1+(_friends.count/2))*219);
                            }
                            else if (IS_IPHONEX){
                                if (_friends.count>4){
                                    _centrLine.frame = CGRectMake(screenWidth/2,100, 2,668);
                                }else{
                                    _centrLine.frame = CGRectMake(screenWidth/2,100, 2,(_friends.count/2)*160);
                                }
                                
                            }
                            else{
                                _centrLine.frame = CGRectMake(screenWidth/2,76, 2,(1+(_friends.count/2))*160);
                            }
                            if(sharedManager.isListView)
                                _centrLine.hidden = YES;
                            else
                                _centrLine.hidden = NO;

                        }
                    }
                    else{
                        _centrLine.hidden = YES;
                    }
                }
                else{
                    _viewStartChat.hidden=NO;
                    
                }
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
            }
        }
        [refreshControl endRefreshing];
    }];
    
    
    
}
-(void) getNotigications{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",sharedManager.notificationsPageNum];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_NOTIFICATIONS,@"method",
                              token,@"session_token",pageStr,@"page_no",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            
            if(success == 1){
                NSArray *notificationsArray = [result objectForKey:@"notifications"];
                sharedManager.notifcationsFetcedFirst = NO;
                sharedManager.notificationsPreviousCount = (int)sharedManager.notificationsArray.count;
                if([notificationsArray isKindOfClass:[NSArray class]])
                {
                    if(sharedManager.notificationsPageNum == 1){
                        sharedManager.notificationsArray = [[NSMutableArray alloc] init];
                    }
                    for(NSDictionary *tempDict in notificationsArray){
                        NotificationsModel *_notification = [[NotificationsModel alloc] init];
                        _notification.notificationsData = [tempDict objectForKey:@"response"];
                        _notification.notif_ID          = [tempDict objectForKey:@"id"];
                        _notification.time              = [tempDict objectForKey:@"timestamp"];
                        _notification.seen              = [tempDict objectForKey:@"seen"];
                        _notification.notificationType  = [tempDict objectForKey:@"type"];
                        _notification.message           = [_notification.notificationsData objectForKey:@"message"];
                        _notification.postData          = [_notification.notificationsData objectForKey:@"post"];
                        _notification.friend_ID         = [_notification.notificationsData objectForKey:@"friend_id"];
                        
                        _notification.frndName         = [_notification.notificationsData objectForKey:@"friend_name"];
                        
                        _notification.post_ID           = [_notification.notificationsData objectForKey:@"post_id"];
                        _notification.parent_Id         = [_notification.notificationsData objectForKey:@"parent_comment_id"];
                        [sharedManager.notificationsArray addObject:_notification];
                    }
                    
                    [self assignChatCounts];
                    
                    NSSortDescriptor *sortDescriptor;
                    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"chatCount" ascending:NO];
                    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                    NSArray *sortedArray;
                    sortedArray = [_friends sortedArrayUsingDescriptors:sortDescriptors];
                    
                    _friends = [[NSMutableArray alloc] initWithArray:sortedArray];
                    [self.collectionView reloadData];
                    [self.tableView reloadData];
//                    [self.collectionView registerClass:[ChatCell class] forCellWithReuseIdentifier:@"Chatcell"];
//                    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatCell" bundle:nil] forCellWithReuseIdentifier:@"Chatcell"];
                    
                }
            }
        }
    }];
}

-(void)deleteChatCall:(NSString *)fId{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_DELETE_CHAT,@"method",
                              token,@"session_token",fId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1){
                
            }
        }
    }];
}

#pragma mark -
#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    
    if(_searchField.text.length> 0) {
        search = true;
        
        NSPredicate *aToZPredicate =
        [NSPredicate predicateWithFormat:@"CAST(SELF.full_name, 'NSString') contains[cd] %@", _searchField.text];
        NSArray *searchArray = [_friends filteredArrayUsingPredicate:aToZPredicate];
        
        _searchFriends = [[NSMutableArray alloc] initWithArray:searchArray];
        if(self.searchFriends.count == 0){
            self.centrLine.hidden = YES;
            self.noUsersFoundlbl.hidden = NO;
        }
        // [self searchPressed:nil];
        
    }
    else {
        search = false;
        
    }
    [self.tableView reloadData];
    [self.collectionView reloadData];
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}


#pragma mark -
#pragma mark Search Functionalities

- (IBAction)searchPressed:(id)sender {
    //    [self.view endEditing:YES];
    [_searchField becomeFirstResponder];
    [self.tableView reloadData];
    [self.collectionView reloadData];
    
}


- (IBAction)btnStartChat:(id)sender {
    int totalCount = 0;
    NSMutableArray *friendsArray = [[NSMutableArray alloc] init];
    for (int i =0; i< sharedManager.followers.count ; i++){
        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
        if([tempObj.status isEqualToString:@"FRIEND" ]){
            [friendsArray addObject:[sharedManager.followers objectAtIndex:i]];
            totalCount ++;
        }
    }
    
//    if (totalCount > 0){
    
        CreateNewChat *users = [[CreateNewChat alloc] initWithNibName:@"CreateNewChat" bundle:nil];
        users.friendsArray = friendsArray;
        [[self navigationController] pushViewController:users animated:YES];
//    }
//    else{
//        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"noFriend", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
//        [alertView show];
//    }

    
}

@end
