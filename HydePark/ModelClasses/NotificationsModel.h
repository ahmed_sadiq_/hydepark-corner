//
//  NotificationsModel.h
//  HydePark
//
//  Created by Mr on 15/06/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationsModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *notificationType;
@property (nonatomic, strong) NSMutableArray *notificationArray;
@property (nonatomic, strong) NSDictionary *notificationsData;
@property (nonatomic, strong) NSMutableDictionary *postData;
@property (nonatomic, strong) NSString *friend_ID;
@property (nonatomic, strong) id notif_ID ;
@property (nonatomic, strong) NSString *post_ID;
@property (nonatomic, strong) id seen;
@property (nonatomic, strong) NSString *profile_link;
@property (nonatomic, strong) NSString *parent_Id;
@property (nonatomic, strong) NSString *frndName;
@property (nonatomic, strong) NSDictionary *chatMsg;

@end
