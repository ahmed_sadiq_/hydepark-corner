//
//  TestingCell.m
//  HydePark
//
//  Created by Samreen Noor on 29/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ChatLeftCell.h"
#import "Constants.h"
@implementation ChatLeftCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _boxView.layer.cornerRadius = 25;
    _boxView.layer.masksToBounds = YES;
      if (IS_IPHONE_5) {
        if (self.tag == 1) {
            CGRect lblFrame = CGRectMake(165,20, 134, 21);
            self.boxView.frame = CGRectMake(19, 5, 120, 120);
            _lblTime.frame = lblFrame;
            
  
        }
        else{
            CGRect lblFrame = CGRectMake(80,44, 134, 21);
             self.boxView.frame = CGRectMake(240, 30, 120, 120);
            _lblTime.frame = lblFrame;

        }
        
    }
//    if (IS_IPAD) {
//        if (self.tag == 1) {
//            CGRect lblFrame = CGRectMake(400,20, 134, 21);
//            
//            _lblTime.frame = lblFrame;
//            
//        }
//        else{
//            CGRect lblFrame = CGRectMake(-200,44, 134, 21);
//            
//            _lblTime.frame = lblFrame;
//            
//        }
//        
        
//    }
}

@end
