//
//  HashTagWithVideosCollectionViewCell.m
//  HydePark
//
//  Created by apple on 6/22/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import "HashTagWithVideosCollectionViewCell.h"

@implementation HashTagWithVideosCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imgVideo.layer.cornerRadius = 10;
    _imgVideo.layer.masksToBounds = YES;
    // Initialization code
}

@end
