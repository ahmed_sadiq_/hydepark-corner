//
//  MyCornerVC.h
//  HydePark
//
//  Created by Osama on 01/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataContainer.h"
#import "WDUploadProgressView.h"
#import "AppDelegate.h"
#import "PWProgressView.h"
#import "BeamUploadVC.h"


@interface MyCornerVC : UIViewController<UIImagePickerControllerDelegate, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDragDelegate, UICollectionViewDropDelegate>{
    NSUInteger currentIndexHome;
    DataContainer *sharedManager;
    UIView * footerViewHome;
    BOOL cannotScrollMyCorner;
    NSUInteger currentSelectedIndex;
    AppDelegate *appDelegate;
    BOOL isNightMode;
    BOOL isScrolling;

    NSTimer *fetchTimer;
    IBOutlet UIActivityIndicatorView *indicator;
    
    int indexWithProgress;

}
@property (nonatomic, strong) BeamUploadVC *beamUpload;
//@property (nonatomic, weak) IBOutlet UITableView *tblHome;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionHome;
@property (strong, nonatomic) IBOutlet UIImageView *userImg;
@property (nonatomic, assign) BOOL fetchingContent;
@property (nonatomic, assign) BOOL isDownwards;
@property (nonatomic, assign) BOOL callFailed;
@property (nonatomic, assign) BOOL firstTime;
@property (nonatomic, assign) NSInteger prevPost;
@property (nonatomic, assign) NSInteger nextPost;
@property (nonatomic, assign) NSInteger currentPost;
@property (nonatomic, assign) NSInteger backgroundCount; // show other videos when it is 2
@property (nonatomic, assign) NSIndexPath *movingIndexPath;


@property (strong, nonatomic) WDUploadProgressView *progressView;
@property (nonatomic, strong) PWProgressView *roundedPView;
@property (assign, nonatomic) NSInteger offlineArrayCount;
@property (assign, nonatomic) NSInteger onlineArrayCount;
@property (assign, nonatomic) float progressViewFrame;
@property (assign, nonatomic) float progressViewValue;
@property (strong, nonatomic) NSMutableArray *offlineThumbnailArray;

-(NSString *)getfriendsCount;
- (void) getMyChannel;
-(NSString *)getFollowersCount;
-(void)updateCornerArray:(VideoModel *)vObj;
@end
