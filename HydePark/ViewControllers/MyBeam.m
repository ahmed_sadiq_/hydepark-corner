//
//  MyBeam.m
//  HydePark
//
//  Created by Mr on 22/04/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "MyBeam.h"
#import "HomeCell.h"
#import "Constants.h"
#import "DrawerVC.h"
#import "Utils.h"
#import "SVProgressHUD.h"
#import "NavigationHandler.h"
#import "CommentsCell.h"
#import "CommentsVC.h"
#import "VideoPlayerVC.h"
#import "VideoModel.h"
#import "BeamUploadVC.h"
#import "Followings.h"
#import "Flurry.h"
#import "UIImageView+WebCache.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "UIImage+JS.h"
#import "ApiManager.h"
#import "DataContainer.h"
#import "AFNetworking.h"
#import "CustomLoading.h"
#import "PlayerViewController.h"
@interface MyBeam (){
    NSMutableDictionary *serviceParams;
    DataContainer *sharedManager;
}
@end

@implementation MyBeam
@synthesize beamsData;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
 
    mybeamsObj = [[myBeamsModel alloc]init];
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    CommentsModelObj = [[CommentsModel alloc] init];
    sharedManager = [DataContainer sharedManager];
    pageNum = 1;
    
    videomodel = [[VideoModel alloc]init];
    _TableBeams.backgroundColor = [UIColor clearColor];
    videoObj = [[NSMutableArray alloc] init];
    beamsData = [[NSMutableArray alloc] init];
    _TableBeams.opaque = NO;
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.background.hidden = YES;
        }
        else
        {
            self.background.hidden = NO;
        }
    }
    else
    {
        self.background.hidden = NO;
    }
    
    if(appDelegate.unseenCount > 0 && !appDelegate.seen)
    {
        _notiCountlbl.text      = [NSString stringWithFormat:@"%ld",(long)appDelegate.unseenCount];
        _notiCountlbl.hidden    = NO;
        _notiCountImage.hidden  = NO;
    }else{
        _notiCountlbl.hidden    = YES;
        _notiCountImage.hidden  = YES;
    }
     [self getmyBeams];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [editView setHidden:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateMyArchive" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateEditedBeam:) name:@"updateMyArchive" object:nil];
    //[Flurry logEvent:@"HPC_MY_ARCHIVE"];
    pageNum = 1;
//    [beamsData removeAllObjects];
//    [self getmyBeams];
}

-(void)presentAll{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* Restore = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"restore", @"")
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [self restoreRequest];
                                [view dismissViewControllerAnimated:YES completion:nil];
                                
                            }];
  
    UIAlertAction* editBeam = [UIAlertAction
                                 actionWithTitle:@"Edit Beam"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self editBeam:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
    
    UIAlertAction* reportBeam = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"removebeam", @"")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                    [self DeleteBtn:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"cancel", @"")
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
//    [view addAction:Share];
//    [view addAction:editBeam];
    [view addAction:Restore];
    [view addAction:reportBeam];
    [view addAction:cancel];
    view.popoverPresentationController.sourceView = self.view;
    view.popoverPresentationController.sourceRect = CGRectMake(_tempFrame.origin.x, _tempFrame.origin.y, 200, 200);
    [self presentViewController:view animated:YES completion:nil];
}

#pragma mark Beam Actions
-(void)restoreRequest{
    [CustomLoading showAlertMessage];
    VideoModel *tempVideos  = [beamsData objectAtIndex:currentSelectedIndex];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"restoreFromArchive",@"method",
                              token,@"Session_token",tempVideos.videoID,@"post_id",nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [CustomLoading DismissAlertMessage];
        [beamsData removeObjectAtIndex:currentSelectedIndex];
        [self.TableBeams reloadData];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        if(success){
            sharedManager.isReloadMyCorner = YES;
            NSDictionary *post = (NSDictionary *)[responseObject objectForKey:@"post"];
            if([post count] != 0){
                VideoModel *_Videos = [[VideoModel alloc] init];
                _Videos.title = [post objectForKey:@"caption"];
                _Videos.comments_count = [post objectForKey:@"comment_count"];
                _Videos.userName = [post objectForKey:@"full_name"];
                _Videos.topic_id = [post objectForKey:@"topic_id"];
                _Videos.user_id = [post objectForKey:@"user_id"];
                _Videos.profile_image = [post objectForKey:@"profile_link"];
                _Videos.like_count = [post objectForKey:@"like_count"];
                _Videos.like_by_me = [post objectForKey:@"liked_by_me"];
                _Videos.seen_count = [post objectForKey:@"seen_count"];
                _Videos.video_angle = [[post objectForKey:@"video_angle"] intValue];
                _Videos.beam_share_url = [post objectForKey:@"beam_share_url"];
                _Videos.deep_link = [post objectForKey:@"deep_link"];
                _Videos.video_link = [post objectForKey:@"video_link"];
                _Videos.m3u8_video_link = [post objectForKey:@"m3u8_video_link"];
                _Videos.video_thumbnail_link = [post objectForKey:@"video_thumbnail_link"];
                _Videos.videoID = [post objectForKey:@"id"];
                _Videos.Tags = [post objectForKey:@"tag_friends"];
                _Videos.video_length = [post objectForKey:@"video_length"];
                _Videos.is_anonymous = [post objectForKey:@"is_anonymous"];
                _Videos.reply_count = [post objectForKey:@"reply_count"];
                _Videos.current_datetime= [post objectForKey:@"current_datetime"];
                _Videos.uploaded_date   = [post objectForKey:@"uploaded_date"];
                _Videos.beam_share_url = [post objectForKey:@"beam_share_url"];
                _Videos.isThumbnailReq =[[post objectForKey:@"is_thumbnail_required"] intValue];
                _Videos.isMute = @"0";
                _Videos.privacy          = [post objectForKey:@"privacy"];
                _Videos.isCelebrity = [post objectForKey:@"is_celeb"];
                _Videos.likesArray      = [[NSMutableArray alloc] init];
                _Videos.isLocal = NO;
                int BeamsCOunt = [sharedManager._profile.beams_count intValue];
                BeamsCOunt += 1;
                sharedManager._profile.beams_count = [NSString stringWithFormat:@"%d",BeamsCOunt];
                [sharedManager.channelVideos addObject:_Videos];
                [sharedManager.channelVideos sortUsingDescriptors:
                 @[
                   [NSSortDescriptor sortDescriptorWithKey:@"uploaded_date" ascending:NO]
                   ]];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [CustomLoading DismissAlertMessage];
//        NSLog(@"error: %@", error);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

-(void)shareDialog{
    [self shareUrlCall];
}
-(void)shareUrlCall{
    VideoModel *tempVideo;
    tempVideo =[beamsData objectAtIndex:currentSelectedIndex];
    [self showShareSheet:tempVideo.beam_share_url];
}

-(void)showShareSheet:(NSString *)shareURL{
    NSURL *url = [NSURL URLWithString:shareURL];
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[url]
     applicationActivities:nil];
    controller.excludedActivityTypes = @[UIActivityTypePrint,
                                         UIActivityTypeMail,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop,
                                         @"com.apple.mobilenotes.SharingExtension",
                                         @"com.apple.reminders.RemindersEditorExtension",
                                         ];
    [self presentViewController:controller animated:YES completion:nil];
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            // user shared an item
//            NSLog(@"We used activity type%@", activityType);
            if([activityType isEqualToString:@"com.apple.UIKit.activity.CopyToPasteboard"]){
                [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
            }
        } else {
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}

- (void)hideNotification:(NSNotification *)notification
{
    if([[notification name] isEqualToString:@"hideNotificationfromMyArchive"]){
        _notiCountlbl.hidden = YES;
        _notiCountImage.hidden =YES;
    }
}
-(void) getnotificationsCount {
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"getNotificationsCount",@"method",
                              token,@"session_token",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                NSString *totalNotifcations = [result objectForKey:@"total_notifications"];
                NSString *unseenCount       = [result objectForKey:@"unseen_count"];
                appDelegate.unseenCount     = [unseenCount integerValue];
                appDelegate.totalNotiCount  = [totalNotifcations integerValue];
                if(appDelegate.unseenCount > 0)
                {
                    _notiCountlbl.text      = unseenCount;
                    _notiCountlbl.hidden    = NO;
                    _notiCountImage.hidden  = NO;
                }
                else
                {
                    _notiCountlbl.hidden    = YES;
                    _notiCountImage.hidden  = YES;
                }
            }
        }
    }];
}
- (void) updateEditedBeam:(NSNotification *) notification
{
    [self getmyBeams];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    float fadeDuration = 0.5;
    [UIView beginAnimations:@"FadeOut" context:nil];
    [UIView setAnimationDuration:fadeDuration ];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [editView setAlpha:0];
    [UIView setAnimationDidStopSelector:@selector(removeView)];
    [UIView commitAnimations];
}
-(void) removeView {
    [editView setHidden:YES];
    
}
- (void) getmyBeams{
    self.fetchingContent = YES;
    _scrollCheck = false;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *pageStr = [NSString stringWithFormat:@"%d",pageNum];
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_MY_BEAMS,@"method",
                              token,@"session_token",pageStr,@"page_no",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            self.fetchingContent = NO;
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                
                NSArray *tempArray = [result objectForKey:@"beams"];
                
                if(tempArray.count> 0) {
                    beamsArray = [result objectForKey:@"beams"];
                    if(pageNum == 1) {
                        beamsData = [[NSMutableArray alloc] init];
                    }
                    for(NSDictionary *tempDict in beamsArray){
                        VideoModel *_Videos = [[VideoModel alloc] init];
                        _Videos.title = [tempDict objectForKey:@"caption"];
                        _Videos.comments_count = [tempDict objectForKey:@"comment_count"];
                        _Videos.userName = [tempDict objectForKey:@"full_name"];
                        _Videos.topic_id = [tempDict objectForKey:@"topic_id"];
                        _Videos.user_id = [tempDict objectForKey:@"user_id"];
                        _Videos.profile_image = [tempDict objectForKey:@"profile_link"];
                        _Videos.like_count = [tempDict objectForKey:@"like_count"];
                        _Videos.like_by_me = [tempDict objectForKey:@"liked_by_me"];
                        _Videos.seen_count = [tempDict objectForKey:@"seen_count"];
                        _Videos.video_angle = [[tempDict objectForKey:@"video_angle"] intValue];
                        _Videos.beam_share_url = [tempDict objectForKey:@"beam_share_url"];
                        _Videos.deep_link = [tempDict objectForKey:@"deep_link"];
                        _Videos.video_link = [tempDict objectForKey:@"video_link"];
                        _Videos.m3u8_video_link = [tempDict objectForKey:@"m3u8_video_link"];
                        _Videos.video_thumbnail_link = [tempDict objectForKey:@"video_thumbnail_link"];
                        _Videos.videoID = [tempDict objectForKey:@"id"];
                        _Videos.Tags = [tempDict objectForKey:@"tag_friends"];
                        _Videos.video_length = [tempDict objectForKey:@"video_length"];
                        _Videos.is_anonymous = [tempDict objectForKey:@"is_anonymous"];
                        _Videos.reply_count = [tempDict objectForKey:@"reply_count"];
                        _Videos.current_datetime= [tempDict objectForKey:@"current_datetime"];
                        _Videos.uploaded_date   = [tempDict objectForKey:@"uploaded_date"];
                        _Videos.beam_share_url = [tempDict objectForKey:@"beam_share_url"];
                        _Videos.isMute = @"0";
                        _Videos.privacy          = [tempDict objectForKey:@"privacy"];
                        NSArray *likesArray     = [tempDict objectForKey:@"post_like_users"];
                        _Videos.likesArray      = [[NSMutableArray alloc] init];
                        _Videos.isLocal = NO;
                        [beamsData addObject:_Videos];
                    }
                    if(!self.fetchingContent){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.TableBeams reloadData];
                            _scrollCheck = true;
                        });
                    }
                }
                else {
                    cannotScroll = true;
                }
                
            }
        }
        else{
            self.fetchingContent = NO;
        }
    }];
}

#pragma mark ----------------------
#pragma mark TableView Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float returnValue;
    
    if (IS_IPAD)
        returnValue = 400.0f;
    else
        returnValue = 230.0f;
    
    if(tableView.tag == 30) {
        
        if (IS_IPAD)
            returnValue = 362.0f;
        else
            returnValue = 250.0f;
    }
    
    return returnValue;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSUInteger rows;
    if (tableView.tag == 0) {
        rows =[beamsData count];
    }else{
        rows =[CommentsArray count];
    }
    return rows;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == 0) {
        
        HomeCell *cell;
        
        if (IS_IPAD) {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeCell_iPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else{
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        VideoModel *tempVideos  = [beamsData objectAtIndex:indexPath.row];
        cell.userName.text = tempVideos.userName;
        
        cell.videoTitle.text = [Utils decodeForEmojis:tempVideos.title];
        cell.CommentscountLbl.text = tempVideos.comments_count;
        cell.heartCountlbl.text = tempVideos.like_count;
        
        
        cell.videoLength.text = tempVideos.video_length;
        [cell.videoThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"loading"]];
        
        
        cell.viewToRound.layer.cornerRadius  = cell.videoThumbnail.frame.size.width /14.0f;
        cell.viewToRound.layer.masksToBounds = YES;
        cell.viewToRound.clipsToBounds = YES;
        [cell.playVideo addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        [cell.playVideo setTag:indexPath.row];
         cell.timeUploaded.text = [Utils returnDate:tempVideos.uploaded_date];
        // appDelegate.videotoPlay = [tempVideos.mainArray objectAtIndex:indexPath.row];
        cell.seenLbl.text = tempVideos.seen_count;
        [cell.heart addTarget:self action:@selector(Hearts:) forControlEvents:UIControlEventTouchUpInside];
        [cell.heart setTag:indexPath.row];
        cell.heartCountlbl.tag = indexPath.row;
        
        if ([tempVideos.like_by_me isEqualToString:@"1"]) {
            [cell.heart setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
        }else{
            [cell.heart setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
        }
        [cell.editBtn setTag:indexPath.row];
        [cell.editBtn addTarget:self action:@selector(editbtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        
       
        [cell.commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.commentsBtn setTag:indexPath.row];
        
        //        [cell.flag addTarget:self action:@selector(Flag:) forControlEvents:UIControlEventTouchUpInside];
        //          [cell.flag setTag:indexPath.row];
        cell.seenLbl.tag = indexPath.row;
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPress:)];
        lpgr.minimumPressDuration = 1.0;
        [cell.playVideo addGestureRecognizer:lpgr];
        [lpgr.view setTag:indexPath.row];
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    if(tableView.tag == 30){
        CommentsCell *cell;
        
        if (IS_IPAD) {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CommentsCell_iPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else{
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CommentsCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        CommentsModel *tempVideos = [[CommentsModel alloc]init];
        tempVideos  = [CommentsArray objectAtIndex:indexPath.row];
        cell.userName.text = tempVideos.userName;
        cell.VideoTitle.text = [Utils decodeForEmojis:tempVideos.title];
        
        cell.CommentscountLbl.text = tempVideos.comments_count;
        cell.heartCountlbl.text = tempVideos.like_count;
        cell.seenLbl.text = tempVideos.seen_count;
        
        appDelegate.videotitle = tempVideos.title;
        appDelegate.profile_pic_url = tempVideos.profile_link;
        
        tempVideos.video_link = [tempVideos.mainArray objectAtIndex:indexPath.row];
        cell.videoLength.text = tempVideos.video_length;
        [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:tempVideos.profile_link] placeholderImage:[UIImage imageNamed:@"loading"]];
        [cell.profileImagePost sd_setImageWithURL:[NSURL URLWithString:tempVideos.profile_link] placeholderImage:[UIImage imageNamed:@"loading"]];
        [cell.videoThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"loading"]];
        
        
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.size.width / 2;
        for (UIView* subview in cell.profileImage.subviews)
            subview.layer.cornerRadius = cell.profileImage.frame.size.width / 2;
        
        cell.profileImage.layer.shadowColor = [UIColor blackColor].CGColor;
        cell.profileImage.layer.shadowOpacity = 0.7f;
        cell.profileImage.layer.shadowOffset = CGSizeMake(0, 5);
        cell.profileImage.layer.shadowRadius = 5.0f;
        cell.profileImage.layer.masksToBounds = NO;
        
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.size.width / 2;
        cell.profileImage.layer.masksToBounds = NO;
        cell.profileImage.clipsToBounds = YES;
        
        cell.profileImage.layer.backgroundColor = [UIColor clearColor].CGColor;
        cell.profileImage.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.profileImage.layer.borderWidth = 3.0f;
        
        cell.profileImagePost.layer.cornerRadius = cell.profileImagePost.frame.size.width / 2;
        for (UIView* subview in cell.profileImagePost.subviews)
            subview.layer.cornerRadius = cell.profileImagePost.frame.size.width / 2;
        
        cell.profileImagePost.layer.shadowColor = [UIColor blackColor].CGColor;
        cell.profileImagePost.layer.shadowOpacity = 0.7f;
        cell.profileImagePost.layer.shadowOffset = CGSizeMake(0, 5);
        cell.profileImagePost.layer.shadowRadius = 5.0f;
        cell.profileImagePost.layer.masksToBounds = NO;
        
        cell.profileImagePost.layer.cornerRadius = cell.profileImagePost.frame.size.width / 2;
        cell.profileImagePost.layer.masksToBounds = NO;
        cell.profileImagePost.clipsToBounds = YES;
        
        cell.profileImagePost.layer.backgroundColor = [UIColor clearColor].CGColor;
        cell.profileImagePost.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.profileImagePost.layer.borderWidth = 3.0f;
        
        [cell.playVideo addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        [cell.playVideo setTag:indexPath.row];
        
        appDelegate.videotoPlay = [tempVideos.mainArray objectAtIndex:indexPath.row];
        cell.heart.enabled = YES;
        cell.commentsBtn.enabled = YES;
        [cell.heart addTarget:self action:@selector(Hearts:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([tempVideos.liked_by_me isEqualToString:@"1"]) {
            [cell.heart setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
        }else{
            [cell.heart setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
        }
        
        [cell.heart setTag:indexPath.row];
        cell.heartCountlbl.tag = indexPath.row;
        
        [cell.commentsBtn addTarget:self action:@selector(ReplyCommentpressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.commentsBtn setTag:indexPath.row];
        
        cell.seenLbl.tag = indexPath.row;
        
        if(IS_IPHONE_6){
            cell.contentView.frame = CGRectMake(0, 0, 375, 220);
        }
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
    
}
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    NSInteger tag = gestureRecognizer.view.tag;
    CGPoint p = [gestureRecognizer locationInView:self.TableBeams];
    NSIndexPath *indexPath = [self.TableBeams indexPathForRowAtPoint:p];
    if (indexPath == nil) {
        // NSLog(@"long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        //NSLog(@"begin");
//        [editView setAlpha:0];
//        [editView setHidden:NO];
//        [UIView beginAnimations:@"FadeIn" context:nil];
//        [UIView setAnimationDuration:0.5];
//        [UIView setAnimationBeginsFromCurrentState:YES];
//        [editView setAlpha:1];
//        [UIView commitAnimations];
        [self presentAll];
        currentSelectedIndex = tag;
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        //NSLog(@"ENDED");
        
    }
}

-(void)cellSwiped:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        UITableViewCell *cell = (UITableViewCell *)gestureRecognizer.view;
        NSIndexPath* index = [self.TableBeams indexPathForCell:cell];
        //..
        _optionsView.hidden = NO;
        [self.view addSubview:self.optionsView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    NSArray *visibleRows = [_TableBeams visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    NSIndexPath *path = [_TableBeams indexPathForCell:lastVisibleCell];
    if(path.section == 0 && path.row == beamsArray.count-1)
    {
        if(!cannotScroll) {
            if(goSearch) {
                searchPageNum++;
            }
            else {
                if(_scrollCheck){
                    pageNum++;
                    [self getmyBeams];
                }
            }
        }
        
    }
}


#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)playVideo:(UIButton*)sender{
    UIButton *playBtn = (UIButton *)sender;
    NSInteger btnTag = playBtn.tag;
    [videoObj removeAllObjects];
    if(beamsData.count > 0) {
        VideoModel *temp  = (VideoModel *)[beamsData objectAtIndex:btnTag];
        PlayerViewController *videoPlayer; //= [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
        
        if (IS_IPHONEX){
            videoPlayer = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController_IphoneX" bundle:nil];
        }else{
            videoPlayer = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
        }
        videoPlayer.vModel       = temp;
        videoPlayer.isComment       = FALSE;
        videoPlayer.isfromChat = YES;
        [[self navigationController] pushViewController:videoPlayer animated:YES];
    }
}

- (void)Hearts:(UIButton*)sender{
    UIButton *LikeBtn = (UIButton *)sender;
    currentSelectedIndex = LikeBtn.tag;
    LikeBtn.enabled = NO;
    VideoModel *tempVideos  = [beamsData objectAtIndex:currentSelectedIndex];
    postID = tempVideos.videoID;
    [self LikePost];
}

- (void) LikePost{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_LIKE_POST,@"method",
                              token,@"session_token",postID,@"post_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [SVProgressHUD dismiss];
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            
            if(success == 1) {
                if ([message isEqualToString:@"Post is Successfully liked."]) {
                    VideoModel *tempVideo = [beamsData objectAtIndex:currentSelectedIndex];
                    NSInteger likeCount = [tempVideo.like_count intValue];
                    likeCount++;
                    tempVideo.like_count = [NSString stringWithFormat: @"%ld", likeCount];
                    tempVideo.like_by_me = @"1";
                    [beamsData replaceObjectAtIndex:currentSelectedIndex withObject:tempVideo];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [_TableBeams beginUpdates];
                    [_TableBeams reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                    [_TableBeams endUpdates];
                }else if ([message isEqualToString:@"Post is Successfully unliked by this user."])
                {
                    VideoModel *tempVideo = [beamsData objectAtIndex:currentSelectedIndex];
                    NSInteger likeCount = [tempVideo.like_count intValue];
                    likeCount--;
                    tempVideo.like_count = [NSString stringWithFormat: @"%ld", likeCount];
                    tempVideo.like_by_me = @"0";
                    [beamsData replaceObjectAtIndex:currentSelectedIndex withObject:tempVideo];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentSelectedIndex inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [_TableBeams beginUpdates];
                    [_TableBeams reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                    [_TableBeams endUpdates];
                }
            }
            //[self getmyBeams];
            
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
}

- (void) SeenPost{
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_POST_SEEN,@"method",
                              token,@"session_token",postID,@"post_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        NSLog(@"%ld",(long)[(NSHTTPURLResponse *)response statusCode]);
        [SVProgressHUD dismiss];
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            
            if(success == 1) {
                if ([message isEqualToString:@"Post is Successfully liked."]) {
                    seenPost = YES;
                }else if ([message isEqualToString:@"Post is Successfully unliked by this user."])
                    seenPost = NO;
                
            }
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
}
#pragma mark editTapped
- (IBAction)shareOnFb:(id)sender {
    __block NSString *urlToShare ;
    VideoModel *tempVideos  = [beamsData objectAtIndex:currentSelectedIndex];
    [editView setHidden:YES];
    serviceParams = [NSMutableDictionary dictionary];
    [serviceParams setValue:@"uploadImage" forKey:@"method"];
    [Utils downloadImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
            UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
            NSData *imgData;
            imgData = UIImagePNGRepresentation(im);
            [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
//                NSLog(@"%lf",progress);
            }
                             name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
//                                 NSLog(@"%@",data);
                                 int flag = [[data objectForKey:@"success"] intValue];
                                 if(flag){
                                     urlToShare = (NSString *)[data objectForKey:@"image_url"];
                                     FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                                     content.contentURL = [NSURL URLWithString:tempVideos.beam_share_url];
                                     content.imageURL   = [NSURL URLWithString:urlToShare];
                                     content.contentTitle = tempVideos.userName;
                                     content.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
                                     
                                     FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
                                     dialog.fromViewController = self;
                                     dialog.shareContent = content;
                                     dialog.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
                                     if (![dialog canShow]) {
                                         // fallback presentation when there is no FB app
                                         dialog.mode = FBSDKShareDialogModeFeedBrowser;
                                     }
                                     [editView setHidden:YES];
                                     [dialog show];
                                 }
                             } failure:^(NSError *error) {
                                 
                             }];
        }
    }];
}

- (IBAction)shareOnTwitter:(id)sender {
    
    VideoModel *tempVideos  = [beamsData objectAtIndex:currentSelectedIndex];
    [editView setHidden:YES];
    NSURL *imageURL = [NSURL URLWithString:tempVideos.video_thumbnail_link];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            UIImage *thumbnailImg = [UIImage imageWithData:imageData];
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                [mySLComposerSheet setInitialText:@"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech."];
                [mySLComposerSheet addURL:[NSURL URLWithString:tempVideos.beam_share_url]];
                [mySLComposerSheet addImage:thumbnailImg];
                [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                    
                    switch (result) {
                        case SLComposeViewControllerResultCancelled:
                            NSLog(@"Post Canceled");
                            break;
                        case SLComposeViewControllerResultDone:
                            NSLog(@"Post Sucessful");
                            break;
                            
                        default:
                            break;
                    }
                }];
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            else {
                
                NSString *message;
                NSString *title;
                NSString *cancel;
                message = NSLocalizedString(@"setupTwitterAccount", @"");
                title   = NSLocalizedString(@"accountConfiguration", @"");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        });
    });
}


- (IBAction)btnCopyLink:(id)sender {
    VideoModel *_model = [beamsData objectAtIndex:currentSelectedIndex];
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString: [NSString stringWithFormat:@"%@", _model.beam_share_url ]];
}




-(void) editbtnTapped:(UIButton *)sender{
//    [editView setAlpha:0];
//    [editView setHidden:NO];
//    [UIView beginAnimations:@"FadeIn" context:nil];
//    [UIView setAnimationDuration:0.5];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [editView setAlpha:1];
//    [UIView commitAnimations];
    UIButton *editbtn = (UIButton *)sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:editbtn.tag inSection:0];
    HomeCell *cell = ( HomeCell *)[self.TableBeams cellForRowAtIndexPath:indexPath];
    _tempFrame = CGRectMake(editbtn.frame.origin.x, _tapOriginY, 200, 200);
    _tempFrame = CGRectMake(editbtn.frame.origin.x, cell.frame.origin.y, 200, 200);

    [self presentAll];
    
    currentSelectedIndex = editbtn.tag;
}
- (IBAction)CancelEditBtn:(id)sender{
    [editView removeFromSuperview];
}
- (IBAction)editBeam:(id)sender{
    
    VideoModel *tempVideos  = [beamsData objectAtIndex:currentSelectedIndex];
    NSString *postIDs = tempVideos.videoID;
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.video_thumbnail = tempVideos.video_thumbnail_link;
    uploadController.postID = postIDs;
    uploadController.caption      = tempVideos.title;
    appDelegate.hasbeenEdited = TRUE;
    uploadController.privacyToShow = tempVideos.privacy;
    uploadController.replyContToShow = tempVideos.reply_count;
    [[self navigationController] pushViewController:uploadController animated:YES];
}
-(IBAction)DeleteBtn:(id)sender{
    [CustomLoading showAlertMessage];
    [editView setHidden:YES];
    VideoModel *tempVideos  = [beamsData objectAtIndex:currentSelectedIndex];
    NSString *postIDs = tempVideos.videoID;
    //[self deleteBeamFromMyCornerArray:tempVideos.videoID];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"deletePost",@"method",
                              token,@"session_token",postIDs,@"post_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [CustomLoading DismissAlertMessage];
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                [beamsData removeObjectAtIndex:currentSelectedIndex];
                [self.TableBeams reloadData];
            }
        }
        else{
            [CustomLoading DismissAlertMessage];
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
}

-(void)deleteBeamFromMyCornerArray:(NSString *)videoID{
    for(int i = 0; i < sharedManager.channelVideos.count ; i++){
        VideoModel *vObj = (VideoModel *)[sharedManager.channelVideos objectAtIndex:i];
        if([videoID isEqualToString:vObj.videoID]){
            [sharedManager.channelVideos removeObjectAtIndex:i];
            break;
        }
    }

}
#pragma mark Get Comments

-(void) ShowCommentspressed:(UIButton *)sender{
    UIButton *CommentsBtn = (UIButton *)sender;
    currentSelectedIndex = CommentsBtn.tag;
    VideoModel *tempVideos  = [beamsData objectAtIndex:currentSelectedIndex];
    
    postID  = tempVideos.videoID;
    
    ParentCommentID = @"-1";
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    
    commentController.commentsObj   = Nil;
    commentController.postArray     = tempVideos;
    commentController.cPostId       = postID;
    commentController.isFirstComment = TRUE;
    commentController.isComment     = FALSE;
    [[self navigationController] pushViewController:commentController animated:YES];
    
}
- (IBAction)CommentsBack:(id)sender {
    
    CommentsArray = nil;
    [commentsView removeFromSuperview];
}



- (IBAction)backBtn:(id)sender {
    _optionsView.hidden = YES;
    [_optionsView removeFromSuperview];
}

- (IBAction)showDrawer:(id)sender {
    
    [SVProgressHUD dismiss];
    //    CGSize size = self.view.frame.size;
    //
    //    if(self.isMenuVisible) {
    //        self.isMenuVisible = false;
    //        [overlayView removeFromSuperview];
    //        [UIView animateWithDuration:0.5 animations:^{
    //            self.view.frame = CGRectMake(0, 0, size.width, size.height);
    //        }];
    //    }
    //    else {
    //        [UIView animateWithDuration:0.5 animations:^{
    //            self.view.frame = CGRectMake(236, 0, size.width, size.height);
    //        }];
    //        self.isMenuVisible = true;
    //        CGRect screenRect = [[UIScreen mainScreen] bounds];
    //        overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 43, screenRect.size.width, screenRect.size.height)];
    //        overlayView.backgroundColor = [UIColor clearColor];
    //
    //        [self.view addSubview:overlayView];
    //
    //        UISwipeGestureRecognizer* sgr = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipe:)];
    //        [sgr setDirection:UISwipeGestureRecognizerDirectionLeft];
    //        [overlayView addGestureRecognizer:sgr];
    //   }
    
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (void)leftSwipe:(UISwipeGestureRecognizer *)gesture
{
    if(self.isMenuVisible){
        [self showDrawer:nil];
    }
}
- (void)rightSwipe:(UISwipeGestureRecognizer *)gesture
{
    if(!self.isMenuVisible){
        [self showDrawer:nil];
    }
}

- (IBAction)showHidesearchbar:(id)sender {
    [SVProgressHUD dismiss];
    [[NavigationHandler getInstance] MoveToSearchFriends];
}


- (IBAction)editBtn:(id)sender {
}

//- (IBAction)DeleteBtn:(id)sender {
//    [_TableBeams deleteRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationRight];
//}
@end
