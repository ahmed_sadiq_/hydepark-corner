//
//  SignUpVC.m
//  HydePark
//
//  Created by Mr on 05/05/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "SignUpVC.h"
#import "NavigationHandler.h"
#import "HomeVC.h"
#import "Constants.h"
#import "Utils.h"
#import "SVProgressHUD.h"
#import "DLRadioButton.h"
#import "Flurry.h"
#import <SafariServices/SafariServices.h>
@interface SignUpVC ()<SFSafariViewControllerDelegate>
#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

@property (strong, nonatomic) IBOutletCollection(DLRadioButton) NSArray *topRadioButtons;

@end

@implementation SignUpVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)init
{
    
    if (IS_IPAD) {
        
        self = [super initWithNibName:@"SignUpVC_iPad.xib" bundle:Nil];
    }
    
    else{
        self = [super initWithNibName:@"iPhone_5_6.xib" bundle:Nil];
    }
       return self;
}

#pragma mark - Helpers

//- (void)showSelectedButton:(NSArray *)radioButtons {
//   btnTitle = [(DLRadioButton *)radioButtons[0] selectedButton].titleLabel.text;
//    [accountType setTitle:btnTitle forState:UIControlStateNormal];
//    accounttypeView.hidden = YES;
// 
//}
//
//- (IBAction)touched:(id)sender {
//    [self showSelectedButton:self.topRadioButtons];
//}




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    SVCustom = [[SVProgressHUDCustom alloc]init];
    [SVCustom customProgressHUD];
    accountTypes = [[NSArray alloc] initWithObjects:@"Normal",@"Public Figure", nil];
    [self setPrivacylbl];
    UIColor *color = [UIColor whiteColor];
    txtUserName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"User Name" attributes:@{NSForegroundColorAttributeName: color}];
    txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedStringWithDefaultValue(@"email",                                                                                                                    @"Localizable",                                                                                                                        [NSBundle mainBundle] ,                                                                                                                        @"Email ",@"") attributes:@{NSForegroundColorAttributeName: color}];
     txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedStringWithDefaultValue(@"password",                                                                                                                    @"Localizable",                                                                                                                        [NSBundle mainBundle] ,                                                                                                                        @"Password ",@"") attributes:@{NSForegroundColorAttributeName: color}];
     txtDisplayName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedStringWithDefaultValue(@"full_name",                                                                                                                    @"Localizable",                                                                                                                        [NSBundle mainBundle] ,                                                                                                                        @"FULL NAME ",@"") attributes:@{NSForegroundColorAttributeName: color}];
//    if(IS_IPHONE_6Plus)
//    {
//         self.view.frame = CGRectMake(0, 0, 414, 736);
//    }
//    if (IS_IPHONE_5) {
//        self.view.autoresizingMask = UIViewAutoresizingNone;
//        bg.frame = CGRectMake(0, 0, 320, 568);
//         self.view.frame = CGRectMake(0, 0, 320, 568);
//        
//    }else if ([[UIScreen mainScreen] bounds].size.height == 667){
//         self.view.autoresizingMask = UIViewAutoresizingNone;
//      self.view.frame = CGRectMake(0, 0, 375, 667);
//       
//        imageBg.image = [UIImage imageNamed:@"signup-bg.png"];
//        imageBg.frame = CGRectMake(0, 0, 375, 667);
//       
//   }
    
}

-(void)setPrivacylbl{
    NSMutableAttributedString *privayString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringWithDefaultValue(@"terms_text_1",
                                                                                                                                  @"Localizable", [NSBundle mainBundle] ,@"While using Hyde Park Corner, you agree to the ",@"Window title")];
    NSMutableAttributedString *termsConditions = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringWithDefaultValue(@"terms_and_conditions",
                                                                                                                                     @"Localizable", [NSBundle mainBundle] ,@" terms and conditions",@"")];
    [termsConditions addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,termsConditions.length}];
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
    // if labelView is not set userInteractionEnabled, you must do so
    [_plbl setUserInteractionEnabled:YES];
    [_plbl addGestureRecognizer:gesture];
    [privayString appendAttributedString:termsConditions];
    _plbl.attributedText = [privayString copy];
}
#pragma mark CLICKABLE LABEL
- (void)userTappedOnLink:(UIGestureRecognizer*)gestureRecognizer{
    NSURL *witsUrl = [NSURL URLWithString:@"https://hydeparkcorner.com/privacy-policy/"];
    SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:witsUrl];
    sfvc.delegate = self;
    [self presentViewController:sfvc animated:YES completion:nil];
}
- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [self dismissViewControllerAnimated:true completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)SignUp:(id)sender {
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
    [txtDisplayName resignFirstResponder];
    if (txtEmail.text.length > 0 ) {
        
        if(![self validateEmail:txtEmail.text])
        {
            
           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"error_email", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil];            
            [alertView show];
            return;
            
        }else{
            if (!(txtPassword.text.length > 0)){
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"error_password", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil];
                [alertView show];
                [txtPassword resignFirstResponder];
            }
            else if (!(txtDisplayName.text.length >0)){
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"error_full_name", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil];
                [alertView show];
                [txtDisplayName resignFirstResponder];
            }//            else if([accountType.titleLabel.text isEqualToString:@"Account Type"])
//            {
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Empty Field" message:@"Please select account type." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
//                [alertView show];
//            }
            else {
                [self sendSignUpCall];
            }
        }
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"error_email", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil];
        [alertView show];
        return;
    }
}
-(IBAction)normalBtnPressed:(id)sender{
    is_celeb = @"0";
    accounttypeView.hidden = TRUE;
}
-(IBAction)CelebBtnPressed:(id)sender{
    is_celeb = @"1";
    accounttypeView.hidden = TRUE;
}
-(void) sendSignUpCall {
   // [SVProgressHUD showWithStatus:@"Signing Up..."];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD show];
    NSString *language = [Utils getLanguageCode];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SIGN_UP,@"method",
                            txtDisplayName.text,@"username", txtDisplayName.text,@"full_name",txtEmail.text,@"email",txtPassword.text, @"password",@"CUSTOM", @"account_type",@"0", @"is_celeb",language,@"language", nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
//        NSLog(@"%ld",(long)[(NSHTTPURLResponse *)response statusCode]);
        [SVProgressHUD dismiss];
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSDictionary *data = [result objectForKey:@"data"];
            NSDictionary *profile = [data objectForKey:@"profile"];
            NSString *sessionToken = [profile objectForKey:@"session_token"];
            messageView = [result objectForKey:@"message"];
            if(success == 1) {
                //Navigate to Home Screen
                AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                appDelegate.isLoggedIn = true;
                
                NSDictionary *data = [result objectForKey:@"data"];
                NSDictionary *profile = [data objectForKey:@"profile"];
                NSString *sessionToken = [profile objectForKey:@"session_token"];
                NSString *is_celebs = [profile objectForKey:@"is_celeb"];
                BOOL is_celebrity = [is_celebs boolValue];
                NSString *user_id = [profile objectForKey:@"id"];
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               txtEmail, @"Email",
                                               txtDisplayName, @"UserName",
                                               nil];
                
                //[Flurry logEvent:@"HPC_SIGN_UP" withParameters:articleParams];
                NSString *user_name = [profile objectForKey:@"full_name"];
                [[NSUserDefaults standardUserDefaults] setObject:user_name forKey:@"User_Name"];
                  [[NSUserDefaults standardUserDefaults] setObject:user_name forKey:@"userName"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldLoadMyCorner"];
                NSString *userImg = [profile objectForKey:@"profile_link"];
                
                [[NSUserDefaults standardUserDefaults] setObject:userImg forKey:@"user_img"];
                [[NSUserDefaults standardUserDefaults] setObject:user_id forKey:@"User_Id"];
                [[NSUserDefaults standardUserDefaults] setObject:user_id forKey:@"id"];
                [[NSUserDefaults standardUserDefaults] setBool:is_celebrity forKey:@"is_celeb"];
                [[NSUserDefaults standardUserDefaults] setObject:sessionToken forKey:@"session_token"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logged_in"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"newSignup"];
                [[NavigationHandler getInstance]NavigateToHomeScreen];
                [AppDelegate moveToBranchedBeam];
            }
            else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:messageView  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

- (IBAction)BacktoLogin:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSignup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NavigationHandler getInstance]NavigateToLoginScreen];
    
}

- (IBAction)accountType:(id)sender {
    senderForNiD = sender;
    //accounttypeView.hidden = NO;
    
    NSArray * arr = [[NSArray alloc] init];
    arr = accountTypes;
    NSArray * arrImage = [[NSArray alloc] init];
    arrImage = [NSArray arrayWithObjects:[UIImage imageNamed:@""], [UIImage imageNamed:@""], nil];
    if(dropDown == nil) {
        CGFloat f = arr.count*40;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down":true];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    
    if(sender.selectedIndex == 0) {
        [accountType setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        is_celeb = @"0";
    
    }
    else if (sender.selectedIndex == 1) {
        [accountType setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        is_celeb = @"1";
        
    }
    [self rel];
}

-(void)rel{
    dropDown = nil;
}

#pragma mark Textfields Delegate Methods

-(BOOL)validateEmail:(NSString *)candidate {
    candidate = [candidate stringByTrimmingCharactersInSet:
                 [NSCharacterSet whitespaceCharacterSet]];
    candidate = [candidate lowercaseString];
    
    /////////////////// Email Validations
    // NSString *email = [txtFldUsername.text lowercaseString];
    NSString *emailRegEx =@"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF  MATCHES %@", emailRegEx];
    BOOL signupValidations = TRUE;
    
    /*  NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,4}";
     NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; */
    
    
    return [emailTest evaluateWithObject:candidate];
}
//////////////Hiding Keyboard Touching Backgground/////////////

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([txtEmail isFirstResponder] && [touch view] != txtEmail) {
        [txtEmail resignFirstResponder];
        
    }
    else if ([txtPassword isFirstResponder] && [touch view] != txtPassword) {
        
        txtPassword.secureTextEntry=YES;
        [txtPassword resignFirstResponder];
        
    } else if ([txtEmail isFirstResponder] && [touch view] != txtEmail) {
        
        [txtEmail resignFirstResponder];
        
    }  else if ([txtDisplayName isFirstResponder] && [touch view] != txtDisplayName) {
        
        [txtDisplayName resignFirstResponder];
        
    }  [super touchesBegan:touches withEvent:event];
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [dropDown hideDropDown:senderForNiD];
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == txtDisplayName)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if([txtUserName isFirstResponder]){
        
        [txtUserName resignFirstResponder];
        [txtEmail becomeFirstResponder];
        
    }else if ([txtEmail isFirstResponder]){
        [txtEmail resignFirstResponder];
        [txtDisplayName becomeFirstResponder];
        
    }else if ([txtDisplayName isFirstResponder]){
        [txtDisplayName resignFirstResponder];
        [txtPassword becomeFirstResponder];
        
    }else if ([txtPassword isFirstResponder]){
        [txtPassword resignFirstResponder];
       
    }else{
        [txtUserName resignFirstResponder];
        [txtPassword resignFirstResponder];
        [txtEmail resignFirstResponder];
        [txtDisplayName resignFirstResponder];
     
    }
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
@end
