//
//  NotificationsVC.m
//  HydePark
//
//  Created by ME on 15/06/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "NotificationsVC.h"
#import "NotificationsCell.h"
#import "Constants.h"
#import "NotificationsModel.h"
#import "NavigationHandler.h"
#import "Utils.h"
#import "ASIFormDataRequest.h"
#import "UserChannelModel.h"
#import "UserChannel.h"
#import "CommentsCell.h"
#import "CommentsVC.h"
#import "Followings.h"
#import "Flurry.h"
#import "FriendChatVC.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+RoundImage.h"
#import <QuartzCore/QuartzCore.h>
#import "PopularUsersVC.h"

@interface NotificationsVC ()
{
    UIRefreshControl *refreshControl;
    BOOL isDownwards;
}
@property (strong , nonatomic) Followings *frnd;
@property (weak, nonatomic) IBOutlet UILabel *headerlbl;
@end

@implementation NotificationsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    [blockerView removeFromSuperview];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    //[Flurry logEvent:@"HPC_NOTIFICATIONS"];
    serverCall = false;
    appDelegate.seen = YES;
    if(sharedManager.notifcationsFetcedFirst){
        [self getNotifications];
    }
    
    [self renderNotifications];
    [self.notificationsTbl reloadData];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"hideNotification"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"hideSideBarNotification"
     object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    exist = NO;
    // Do any additional setup after loading the view from its nib.
    notifModel = [NotificationsModel alloc];
    userChannelObj = [[UserChannelModel alloc]init];
    CommentsModelObj = [[CommentsModel alloc] init];
    videomodel = [[VideoModel alloc]init];
    videoCommentModel = [[VideoModel alloc] init];
    
    pageNum = 1;
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.notificationsTbl.backgroundColor = [UIColor clearColor];
    self.notificationsTbl.opaque = NO;
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor whiteColor];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            refreshControl.tintColor = [UIColor grayColor];
        }
        else
        {
            refreshControl.tintColor = [UIColor whiteColor];
        }
    }
    else
    {
        refreshControl.tintColor = [UIColor whiteColor];
    }
    [refreshControl addTarget:self
                       action:@selector(refreshCall)
             forControlEvents:UIControlEventValueChanged];
    [self.notificationsTbl addSubview:refreshControl];
    sharedManager = [DataContainer sharedManager];
//    if(sharedManager.notificationsArray.count == 0){
        [self refreshCall];
//    }
    self.headerlbl.text = NSLocalizedString(@"notifications", @"");
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.background.hidden = YES;
        }
        else
        {
        }
    }
    else
    {
    }
}


#pragma mark Rendered Notifications
#pragma mark ServerCall
-(void)renderNotifications{
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"renderNotifications",@"method",
                              token,@"Session_token",nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        if(success){
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
    }];
}

#pragma mark Refresh Control
- (void)refreshCall
{
        sharedManager.notificationsPageNum = 1;
        [self getNotifications];
    
}
-(IBAction)backToMyCorner:(id)sender{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"MoveToSpeakerCorner"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"MoveToSpeakerIndex"
     object:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
-(void) getNotifications{
    serverCall = TRUE;
    exist = NO;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"notiCheck"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"hideNotificationfromMyArchive"
     object:nil];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *language = [Utils getLanguageCode];
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",sharedManager.notificationsPageNum];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_NOTIFICATIONS,@"method",
                              token,@"session_token",pageStr,@"page_no",language,@"language",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            [refreshControl endRefreshing];
            if(success == 1){
                serverCall = FALSE;
                notificationsArray = [result objectForKey:@"notifications"];
                sharedManager.notifcationsFetcedFirst = NO;
                sharedManager.notificationsPreviousCount = (int)sharedManager.notificationsArray.count;
                if([notificationsArray isKindOfClass:[NSArray class]])
                {
                    if(notificationsArray.count > 0){
                        if(sharedManager.notificationsPageNum == 1){
                            notifModel.notificationArray     = [[NSMutableArray alloc] init];
                            sharedManager.notificationsPreviousCount = 0;
                            [sharedManager.notificationsArray removeAllObjects];
                        }
                        for(NSDictionary *tempDict in notificationsArray){
                            NotificationsModel *_notification = [[NotificationsModel alloc] init];
                            _notification.notificationsData = [tempDict objectForKey:@"response"];
                            _notification.notif_ID          = [tempDict objectForKey:@"id"];
                            _notification.time              = [tempDict objectForKey:@"timestamp"];
                            _notification.seen              = [tempDict objectForKey:@"seen"];
                            _notification.notificationType  = [tempDict objectForKey:@"type"];
                            _notification.message           = [_notification.notificationsData objectForKey:@"notification_message"];
                            if(_notification.message == nil)
                            {
                                _notification.message           = [_notification.notificationsData objectForKey:@"message"];
                            }
                            _notification.postData          = [_notification.notificationsData objectForKey:@"post"];
                            _notification.friend_ID         = [_notification.notificationsData objectForKey:@"friend_id"];
                            
                            _notification.frndName         = [_notification.notificationsData objectForKey:@"friend_name"];
                            
                            _notification.post_ID           = [_notification.notificationsData objectForKey:@"post_id"];
                            _notification.parent_Id         = [_notification.notificationsData objectForKey:@"parent_comment_id"];
                            
                            //NotificationsModel *notifiModel = [[NotificationsModel alloc]init];
                            //notifiModel  = [sharedManager.notificationsArray objectAtIndex:indexPath.row];
                            
                            for(NotificationsModel *notifiModel in sharedManager.notificationsArray){
                                if (notifiModel.notif_ID==_notification.notif_ID){
                                    exist = YES;
                                    return;
                                }
                                else{
                                    exist = NO;
                                }
                            }
                            
                            if(!exist){
                               [sharedManager.notificationsArray addObject:_notification];
                            }
                            
                            //sharedManager.notificationsArray    = [notifModel.notificationArray mutableCopy];
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if(sharedManager.notificationsArray.count == 0)
                            {
                                emptyView.hidden = false;
                            }
                            else{
                                emptyView.hidden = true;
                            }
                            
                            [self.notificationsTbl reloadData];
                        });
//                        emptyView.hidden = true;

                    }
                    else {
                        if(sharedManager.notificationsArray.count == 0)
                        {
                            emptyView.hidden = false;
                        }
                        else{
                            emptyView.hidden = true;
                        }
                        cannotScroll = true;
//                        emptyView.hidden = false;
                    }
                }
            }
            else{
                serverCall = FALSE;
            }
        }
    }];
}

-(IBAction)MoveToPopularUsers
{
    
    if (IS_IPAD) {
        PopularUsersVC *users = [[PopularUsersVC alloc] initWithNibName:@"PopularUsersVC_iPad" bundle:nil];
        [self.navigationController pushViewController:users animated:YES];
    }else if (IS_IPHONEX){
        PopularUsersVC *users = [[PopularUsersVC alloc] initWithNibName:@"PopularUsersVC_iPhoneX" bundle:nil];
        [self.navigationController pushViewController:users animated:YES];
    }
    else if(IS_IPHONE_6 || IS_IPHONE_5 || IS_IPHONE_6Plus){
        
        PopularUsersVC *users = [[PopularUsersVC alloc] initWithNibName:@"PopularUsersVC_iPhone6" bundle:nil];
        [self.navigationController pushViewController:users animated:YES];
    }
    
    else{
        
        PopularUsersVC *users = [[PopularUsersVC alloc] initWithNibName:@"PopularUsersVC" bundle:nil];
        [self.navigationController pushViewController:users animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)seenNotification:(NSString *)noti_id{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_NOTIFICATIONS_SEEN,@"method",token,@"Session_token",noti_id,@"notification_id", nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            appDelegate.unseenCount -- ;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSDictionary *posts = [result objectForKey:@"post"];
            if(success == 1) {
                
            }
        }
    }];
}
#pragma mark ----------------------
#pragma mark TableView Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float returnValue;
    if (IS_IPAD)
        returnValue = 93.0f;
    else
        returnValue = 80.0f;
    
    return returnValue;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [sharedManager.notificationsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"NotiCell";
    NotificationsCell *cell;
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects;
        if(IS_IPAD){
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NotificationsCell_iPad" owner:self options:nil];
        }else{
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NotificationsCell" owner:self options:nil];
        }
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    else{
        cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    if (IS_IPHONE_6) {
        cell.frame = CGRectMake(0, 0, 375, 667);
        cell.contentView.frame = CGRectMake(0, 0, 375, 667);
    }
    NotificationsModel *notifiModel = [[NotificationsModel alloc]init];
    notifiModel  = [sharedManager.notificationsArray objectAtIndex:indexPath.row];
    
    NSMutableDictionary *postDict = notifiModel.postData;
    //////
    NSString *start = notifiModel.time;
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    [f setLocale:arabicLocale];
    f.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];

    NSString *end = [f stringFromDate:[NSDate date]];
    
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    f.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    start = [f stringFromDate:startDate];
    end = [f stringFromDate:endDate];
    [f setDateFormat:@"dd"];
    NSString *currentDate = [f stringFromDate:endDate];
    NSString *notiDate = [f stringFromDate:startDate];
    NSInteger current = [currentDate integerValue];
    NSInteger notificationTime = [notiDate integerValue];
    
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    if([components day] == 0) {
        [f setDateFormat:@"HH"];
        start = [f stringFromDate:startDate];
        end = [f stringFromDate:endDate];
        
        NSInteger hourStart = [start integerValue];
        NSInteger hourEnd = [end integerValue];
        NSInteger hours;
        if(current > notificationTime) {
            hourEnd += 24;
        }
        hours = hourEnd - hourStart;
        
        
//        [f setDateFormat:@"mm"];
//        start = [f stringFromDate:startDate];
//        end = [f stringFromDate:endDate];
//        NSInteger minStart = [start integerValue];
//        NSInteger minEnd = [end integerValue];
//        NSInteger minute =  minEnd - minStart;
        if(hours <= 0) {
            [f setDateFormat:@"mm"];
            start = [f stringFromDate:startDate];
            end = [f stringFromDate:endDate];
            NSInteger minStart = [start integerValue];
            NSInteger minEnd = [end integerValue];
            NSInteger minute =  minEnd - minStart;
            if(minute <= 0) {
                [f setDateFormat:@"ss"];
                start = [f stringFromDate:startDate];
                end = [f stringFromDate:endDate];
                NSInteger secStart = [start integerValue];
                NSInteger secEnd = [end integerValue];
                NSInteger second = secEnd - secStart;
                cell.Time.text = [NSString stringWithFormat:@"%ld %@",second, NSLocalizedString(@"seconds_ago", nil)];
            } else {
                cell.Time.text = [NSString stringWithFormat:@"%ld %@",minute, NSLocalizedString(@"minutes_ago", nil)];
            }
        } else {
            cell.Time.text = [NSString stringWithFormat:@"%ld %@",hours, NSLocalizedString(@"hours_ago", nil)];
        }
    } else if([components day] == 1) {
        /// YESTERDAY
        cell.Time.text = [NSString stringWithFormat:NSLocalizedString(@"yesterday", nil)];
    } else {
        cell.Time.text = [Utils returnDate:notifiModel.time];

    }
    /////
    NSString *str1 = [NSString stringWithFormat:@"%@",notifiModel.notificationType];
    cell.message.text = notifiModel.message;
//    cell.Name.text = [Utils getDecryptedTextFor:notifiModel.frndName];
    cell.Name.text = notifiModel.frndName;
    if([notifiModel.seen isEqualToString:@"1"]){
        //cell.message.textColor = [UIColor colorWithRed:53.0/255 green:80.0/255 blue:137.0/255 alpha:1.0f];
        cell.bgView.backgroundColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:0.5f];
    }
    else
    {
        cell.bgView.backgroundColor = [UIColor colorWithRed:53.0/255 green:80.0/255 blue:137.0/255 alpha:0.8f];
        
        // cell.message.textColor = [UIColor whiteColor];
    }
    
    if([notifiModel.frndName containsString:@"Anonymous"]){
        cell.notifImage.image = SET_IMAGE(@"anonymousDp.png");
    }
    else{
        NSString *profileLink = [notifiModel.notificationsData objectForKey:@"profile_link"];
        if (profileLink.length==0){
             [cell.notifImage setImageWithURL:[NSURL URLWithString:@"https://hydeparkcorner.com/api/images/no_image_profile.png"] placeholderImage:nil];
        }else{
             [cell.notifImage setImageWithURL:[NSURL URLWithString:profileLink] placeholderImage:nil];
        }
        
        
    }
    [cell.notifImage roundImageCorner];
    if ([str1 isEqualToString:@"LIKE_POST"]) {
        cell.videoImage.hidden = NO;
        cell.playImage.hidden = NO;
        cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.videoImage.layer.cornerRadius=10;
        cell.videoImage.layer.masksToBounds = YES;
        [cell.videoImage setImageWithURL:[NSURL URLWithString:[postDict objectForKey:@"video_thumbnail_link"]] placeholderImage:[UIImage imageNamed:@"pH"]];
        [cell.notifyBtn addTarget:self action:@selector(getLikePost:) forControlEvents:UIControlEventTouchUpInside];
    }else if ([str1 isEqualToString:@"LIKE_COMMENT"]) {
        cell.videoImage.hidden = NO;
        cell.playImage.hidden = NO;
        cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.videoImage.layer.cornerRadius=10;
        cell.videoImage.layer.masksToBounds = YES;
        [cell.videoImage setImageWithURL:[NSURL URLWithString:[postDict objectForKey:@"video_thumbnail_link"]] placeholderImage:[UIImage imageNamed:@"pH"]];
        [cell.notifyBtn addTarget:self action:@selector(getCommentsliked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([str1 isEqualToString:@"TAG_FRIENDS"] || [str1 isEqualToString:@"TAG_FRIENDS_COMMENT"]){
//        [cell.notifyBtn addTarget:self action:@selector(getCommentsPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.videoImage.hidden = NO;
        cell.playImage.hidden = NO;
        cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.videoImage.layer.cornerRadius=10;
        cell.videoImage.layer.masksToBounds = YES;
        [cell.videoImage setImageWithURL:[NSURL URLWithString:[postDict objectForKey:@"video_thumbnail_link"]] placeholderImage:[UIImage imageNamed:@"pH"]];
        [cell.notifyBtn addTarget:self action:@selector(getCommentsPost:) forControlEvents:UIControlEventTouchUpInside];
    }else if([str1 isEqualToString:@"COMMENT_COMMENT"]){
        cell.videoImage.hidden = NO;
        cell.playImage.hidden = NO;
        cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.videoImage.layer.cornerRadius=10;
        cell.videoImage.layer.masksToBounds = YES;
        [cell.videoImage setImageWithURL:[NSURL URLWithString:[postDict objectForKey:@"video_thumbnail_link"]] placeholderImage:[UIImage imageNamed:@"pH"]];
        [cell.notifyBtn addTarget:self action:@selector(getCommentsOnComments:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([str1 isEqualToString:@"COMMENT_POST"]){
        cell.videoImage.hidden = NO;
        cell.playImage.hidden = NO;
        cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.videoImage.layer.cornerRadius=10;
        cell.videoImage.layer.masksToBounds = YES;
        [cell.videoImage setImageWithURL:[NSURL URLWithString:[postDict objectForKey:@"video_thumbnail_link"]] placeholderImage:[UIImage imageNamed:@"pH"]];
        [cell.notifyBtn addTarget:self action:@selector(getCommentsPost:) forControlEvents:UIControlEventTouchUpInside];
    }else if ([str1  isEqualToString:@"REQUEST_RECIEVED"]){
        cell.videoImage.hidden = NO;
        cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.videoImage.layer.cornerRadius=10;
        cell.videoImage.layer.masksToBounds = YES;
        cell.videoImage.image = [UIImage imageNamed:@"noti_request"] ;
        [cell.notifyBtn addTarget:self action:@selector(getuserChannelBtn:) forControlEvents:UIControlEventTouchUpInside];
        
    }else if([str1 isEqualToString:@"REQUEST_ACCEPTED"]){
        cell.videoImage.hidden = NO;
        cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.videoImage.layer.cornerRadius=10;
        cell.videoImage.layer.masksToBounds = YES;
        cell.videoImage.image = [UIImage imageNamed:@"noti_accepted"] ;
        [cell.notifyBtn addTarget:self action:@selector(getuserChannelBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([str1 isEqualToString:@"FOLLOWED"])
    {
        cell.videoImage.hidden = NO;
        cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.videoImage.layer.cornerRadius=10;
        cell.videoImage.layer.masksToBounds = YES;
        cell.videoImage.image = [UIImage imageNamed:@"noti_followed"] ;
        [cell.notifyBtn addTarget:self action:@selector(getuserChannelBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([str1 isEqualToString:@"VIDEO_MESSAGE"]){
        cell.videoImage.hidden = NO;
        cell.playImage.hidden = NO;
        cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.videoImage.layer.cornerRadius=10;
        cell.videoImage.layer.masksToBounds = YES;
        [cell.videoImage setImageWithURL:[NSURL URLWithString:[postDict objectForKey:@"video_thumbnail_link"]] placeholderImage:[UIImage imageNamed:@"pH"]];
        _frnd =  [[Followings alloc]init];
        [_frnd setF_id:[NSString stringWithFormat:@"%@", notifiModel.friend_ID]];
        [_frnd setFullName:[NSString stringWithFormat:@"%@",notifiModel.frndName]];
        
        _frnd.fullName = [NSString stringWithFormat:@"%@",notifiModel.frndName];
        [cell.notifyBtn addTarget:self action:@selector(getChat:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if([str1 isEqualToString:@"BEAM_SHARE"]){
        cell.videoImage.hidden = NO;
        cell.playImage.hidden = NO;
        cell.videoImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.videoImage.layer.cornerRadius=10;
        cell.videoImage.layer.masksToBounds = YES;
        [cell.videoImage setImageWithURL:[NSURL URLWithString:[postDict objectForKey:@"video_thumbnail_link"]] placeholderImage:[UIImage imageNamed:@"pH"]];
        [cell.notifyBtn addTarget:self action:@selector(getCommentsPost:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    [cell.notifyBtn setTag:indexPath.row];
    cell.bgView.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
    cell.bgView.layer.shadowOffset  = CGSizeMake(1.0f, 3.0f);
    cell.bgView.layer.shadowOpacity = 2;
    cell.bgView.layer.shadowRadius  = 4.0;
    
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)cellSwiped:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        UITableViewCell *cell = (UITableViewCell *)gestureRecognizer.view;
        NSIndexPath* index = [self.notificationsTbl indexPathForCell:cell];
        //..
        [self.notificationsTbl deleteRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
        
    }
}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    CGPoint targetPoint = *targetContentOffset;
    CGPoint currentPoint = scrollView.contentOffset;
    if (targetPoint.y > currentPoint.y) {
        isDownwards = false;
    }
    else {
        isDownwards = true;
        return;
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    //    if(isDownwards){
    //        NSArray *visibleRows = [self.notificationsTbl visibleCells];
    //        UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    //        NSIndexPath *path = [self.notificationsTbl indexPathForCell:lastVisibleCell];
    //        if(path.section == 0 && path.row == sharedManager.notificationsArray.count-1)
    //        {
    //            if(!cannotScroll && !serverCall) {
    //                sharedManager.notificationsPageNum++;
    //                [self getNotifications];
    //            }
    //        }
    //    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!serverCall && indexPath.row == sharedManager.notificationsArray.count - 1 && !cannotScroll) {
        sharedManager.notificationsPageNum++;
        [self getNotifications];
    }
}

-(void)getChat:(UIButton*)sender{
    
    [self.view addSubview:blockerView];
    UIButton *LikeBtn = (UIButton *)sender;
    currentSelectedIndex = LikeBtn.tag;
    NotificationsModel *notifiModel = [[NotificationsModel alloc]init];
    notifiModel  = [sharedManager.notificationsArray  objectAtIndex:currentSelectedIndex];
    [self seenNotification:notifiModel.notif_ID];
    notifiModel.seen = @"1";
    [self.notificationsTbl reloadData];
    
    FriendChatVC *frndVC;
    
    if (IS_IPHONEX){
        frndVC = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC_IphoneX" bundle:nil];
    }else{
        frndVC = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    }
    
   // frndVC = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    frndVC.isLocalSearch = YES;
    frndVC.isFriend = YES;
    frndVC.seletedFollowing   = _frnd;
    
    [[self navigationController] pushViewController:frndVC animated:YES];
    
}

-(void)getuserChannelBtn:(UIButton*)sender{
    [self.view addSubview:blockerView];
    UIButton *LikeBtn = (UIButton *)sender;
    currentSelectedIndex = LikeBtn.tag;
    NotificationsModel *notifiModel = [[NotificationsModel alloc]init];
    notifiModel  = [sharedManager.notificationsArray  objectAtIndex:currentSelectedIndex];
    
    NSString *str1 = [NSString stringWithFormat:@"%@",notifiModel.notificationType];
    if ([str1  isEqualToString:@"REQUEST_RECIEVED"]){
       // [sharedManager.notificationsArray  removeObjectAtIndex:currentSelectedIndex];
    }
    
    [self seenNotification:notifiModel.notif_ID];
    notifiModel.seen = @"1";
    [self.notificationsTbl reloadData];
    friendID = notifiModel.friend_ID;
    Followings *fData  = [[Followings alloc] init];
    
    BOOL isCl= [[notifiModel.notificationsData objectForKey:@"is_celeb"] boolValue];
    fData.is_celeb = [NSString stringWithFormat:@"%d",isCl];
    fData.isFollowed = [[notifiModel.notificationsData objectForKey:@"is_followed"] intValue];
    fData.f_id =[notifiModel.notificationsData objectForKey:@"friend_id"];
    
    UserChannel *commentController;
    if(IS_IPAD){
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
    }else if (IS_IPHONEX){
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
    }
    else
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
    commentController.ChannelObj = nil;
    commentController.friendID   = friendID;
    commentController.currentUser = fData;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(void)getCommentsPost:(UIButton*)sender{
    [self.view addSubview:blockerView];
    UIButton *LikeBtn = (UIButton *)sender;
    currentSelectedIndex = LikeBtn.tag;
    NotificationsModel *notifiModel = [[NotificationsModel alloc]init];
    notifiModel  = [sharedManager.notificationsArray  objectAtIndex:currentSelectedIndex];
    [self seenNotification:notifiModel.notif_ID];
    notifiModel.seen = @"1";
    [self.notificationsTbl reloadData];
    postID =  notifiModel.post_ID;
    ParentCommentID = notifiModel.parent_Id;
    NSDictionary *postDate = notifiModel.postData;
    videomodel = [self returnMeVideoModel:postDate];
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    
    commentController.commentsObj   = Nil;
    commentController.postArray     = videomodel;
    commentController.cPostId       = postID;
    commentController.isFirstComment= TRUE;
    commentController.isComment     = FALSE;
    commentController.pID           = ParentCommentID;
    [[self navigationController] pushViewController:commentController animated:YES];
}
-(void)getCommentsliked:(UIButton*)sender{
    [self.view addSubview:blockerView];
    UIButton *LikeBtn = (UIButton *)sender;
    currentSelectedIndex = LikeBtn.tag;
    NotificationsModel *notifiModel = [[NotificationsModel alloc]init];
    notifiModel  = [sharedManager.notificationsArray  objectAtIndex:currentSelectedIndex];
    [self seenNotification:notifiModel.notif_ID];
    notifiModel.seen = @"1";
    [self.notificationsTbl reloadData];
    postID =  notifiModel.post_ID;
    ParentCommentID = notifiModel.parent_Id;
    NSDictionary *postDate = notifiModel.postData;
    videomodel = [self returnMeVideoModel:postDate];
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    
    commentController.commentsObj   = Nil;
    commentController.postArray     = videomodel;
    commentController.cPostId       = postID;
    commentController.isFirstComment= FALSE;
    commentController.isComment     = TRUE;
    commentController.pID           = ParentCommentID;
    [[self navigationController] pushViewController:commentController animated:YES];
}
-(void)getCommentsOnComments:(UIButton*)sender{
    [self.view addSubview:blockerView];
    UIButton *LikeBtn = (UIButton *)sender;
    currentSelectedIndex = LikeBtn.tag;
    NotificationsModel *notifiModel = [[NotificationsModel alloc]init];
    notifiModel  = [sharedManager.notificationsArray  objectAtIndex:currentSelectedIndex];
    [self seenNotification:notifiModel.notif_ID];
    notifiModel.seen = @"1";
    [self.notificationsTbl reloadData];
    postID = notifiModel.post_ID;
    ParentCommentID = notifiModel.parent_Id;
    NSDictionary *postDate = notifiModel.postData;
    videomodel = [self returnMeVideoModel:postDate];
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    
    commentController.commentsObj   = Nil;
    commentController.postArray     = videomodel;
    commentController.cPostId       = postID;
    commentController.isFirstComment = FALSE;
    commentController.isComment     = TRUE;
    commentController.pID = ParentCommentID;
    [[self navigationController] pushViewController:commentController animated:YES];
    
}
-(void)getLikePost:(UIButton*)sender{
    [self.view addSubview:blockerView];
    UIButton *LikeBtn = (UIButton *)sender;
    currentSelectedIndex = LikeBtn.tag;
    NotificationsModel *notifiModel = [[NotificationsModel alloc]init];
    notifiModel  = [sharedManager.notificationsArray  objectAtIndex:currentSelectedIndex];
    [self seenNotification:notifiModel.notif_ID];
    notifiModel.seen = @"1";
    
    [self.notificationsTbl reloadData];
    NSMutableDictionary *postDate = notifiModel.postData;
    videomodel = [self returnMeVideoModel:postDate];
    
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    
    commentController.commentsObj   = Nil;
    commentController.postArray     = videomodel;
    commentController.cPostId       = videomodel.videoID;
    commentController.isFirstComment = TRUE;
    commentController.isComment     = FALSE;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(VideoModel*)returnMeVideoModel:(NSDictionary*)postDate{
    VideoModel *videomodl          = [[VideoModel alloc]init];
    videomodl.userName             = [postDate valueForKey:@"full_name"];
    videomodl.is_anonymous         = [postDate valueForKey:@"is_anonymous"];
    videomodl.title                = [postDate valueForKey:@"caption"];
    videomodl.comments_count       = [postDate valueForKey:@"comment_count"];
    videomodl.topic_id             = [postDate valueForKey:@"topic_id"];
    videomodl.user_id              = [postDate valueForKey:@"user_id"];
    videomodl.profile_image        = [postDate valueForKey:@"profile_link"];
    videomodl.like_count           = [postDate valueForKey:@"like_count"];
    videomodl.seen_count           = [postDate valueForKey:@"seen_count"];
    videomodl.video_link           = [postDate valueForKey:@"video_link"];
    videomodl.m3u8_video_link      = [postDate valueForKey:@"m3u8_video_link"];
    videomodl.video_thumbnail_link = [postDate valueForKey:@"video_thumbnail_link"];
    videomodl.videoID              = [postDate valueForKey:@"id"];
    videomodl.Tags                 = [postDate valueForKey:@"tag_friends"];
    videomodl.video_length         = [postDate valueForKey:@"video_length"];
    videomodl.like_by_me           = [postDate valueForKey:@"liked_by_me"];
    videomodl.reply_count          = [postDate objectForKey:@"reply_count"];
    videomodl.current_datetime     = [postDate objectForKey:@"current_datetime"];
    videomodl.uploaded_date        = [postDate objectForKey:@"uploaded_date"];
    videomodl.isMute              = [postDate objectForKey:@"mute"];
    if([postDate objectForKey:@"beam_type"]){
        videomodl.beamType = [[postDate objectForKey:@"beam_type"] intValue];
    }
    if([postDate objectForKey:@"original_beam_data"]){
        videomodl.originalBeamData = [postDate objectForKey:@"original_beam_data"];
    }
    return videomodl;
}

#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
