//
//  ChatCell.m
//  HydePark
//
//  Created by Samreen Noor on 28/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ChatCell.h"

@implementation ChatCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            _outline.layer.borderWidth = 1;
            _outline.layer.borderColor = [[UIColor blueColor] CGColor];
        }
        else
        {
//            _userImg.layer.borderWidth = 1;
//            _userImg.layer.borderColor = [[UIColor blueColor] CGColor];
        }
    }
    else
    {
//        _userImg.layer.borderWidth = 1;
//        _userImg.layer.borderColor = [[UIColor blueColor] CGColor];
    }
    
    
    _userImg.layer.cornerRadius = 25;
    _userImg.layer.masksToBounds = YES;
    _outline.layer.cornerRadius = 25;
    _outline.layer.masksToBounds = YES;
}

@end
