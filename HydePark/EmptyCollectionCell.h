//
//  EmptyCollectionCell.h
//  HydePark
//
//  Created by Babar Hassan on 11/2/17.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyCollectionCell : UICollectionViewCell
@property(weak, nonatomic) UIView *blackOverlay;

@end
