//
//  StoriesViewController.m
//  HydePark
//
//  Created by Ahmed Sadiq on 14/12/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "StoriesViewController.h"
#import "PBJVideoPlayerController.h"
#import <AFNetworking.h>
#import "UIImageView+WebCache.h"
#import "Constants.h"
#import "Utils.h"
#import "DataContainer.h"
#import "UserChannel.h"
#import "BeamUploadVC.h"
#import "UIImage+JS.h"
#import "NSData+AES.h"

@interface StoriesViewController () <UIScrollViewDelegate, PBJVideoPlayerControllerDelegate> {
    UIProgressView *progressView;
    UIImageView *profileImage;
    UIButton *closeBtn;
    UILabel *nameLbl;
    UIButton *nameBtn;
    UIButton *showOptionsBtn;
    UIView *videoView;
    PBJVideoPlayerController *_videoPlayerController;
    UIActivityIndicatorView *activityIndicator;
    float duration;
    int page;   // current Page Index
    int tempPage; // Check if tempPage is not equal to page, send server call
    UIView *subview;    // contains all the scrolling views
    DataContainer *sharedInstanse;
}

@end

@implementation StoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    mySliderBackground.hidden = true;
    self.navigationController.interactivePopGestureRecognizer.enabled  = NO;
    isRecording = false;
    sharedInstanse = [DataContainer sharedManager];
    _checkArray = [[NSMutableArray alloc] init];
    _removedStoriesArray = _storiesArray;
    _count = 0;
    resumeSeconds = nil;
    shudContinuePlayer = true;
    if(_isFirstComment){
        _pID = @"-1";
    }
    
    
    backupFriendsStories = [[NSMutableArray alloc] init];
    [backupFriendsStories addObjectsFromArray:_storiesArray];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillBeomceActive:) name:UIApplicationDidBecomeActiveNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearPlayingMedia) name:@"stopAllPlayingMedia" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopPlayer) name:@"stopPlayer" object:nil];

    shudClearPlayer = false;
    [self setAudioRecordSettings];
    _playerViewController = [[AVPlayerViewController alloc] init];
    [self.scrollView addSubview:_playerViewController.view];
    
    isForName = NO;
    self.navigationController.navigationBar.hidden = YES;
    // Do any additional setup after loading the view.
}

-(void)clearPlayingMedia
{
    resumeSeconds = CMTimeGetSeconds(playVideo.currentTime);
    [playVideo pause];
}

-(void)showOptions
{
    [optionsTimer invalidate];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         _lowerOptionsView.frame = CGRectMake(0, shownYposition, _lowerOptionsView.frame.size.width, _lowerOptionsView.frame.size.height);
                     }
                     completion:nil
     ];
    optionsTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(startHidingOptions) userInfo:nil repeats:NO];
}

-(void)startHidingOptions
{
//    [UIView animateWithDuration:1.0
//                          delay:0.0
//                        options:UIViewAnimationOptionTransitionNone
//                     animations:^{
//                         _lowerOptionsView.frame = CGRectMake(0, hiddenYposition, _lowerOptionsView.frame.size.width, _lowerOptionsView.frame.size.height);
//                     }
//                     completion:nil
//     ];
}

-(void)stopPlayer
{
    shudContinuePlayer = false;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    _scrollView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , _scrollView.frame.size.height);
    
    if(!playVideo)
    {
        [self setupScrollView];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    isForName = NO;
    secondsLeft = 60;
    if(playVideo)
    {
        if(resumeSeconds)
        {
            [playVideo seekToTime:CMTimeMakeWithSeconds(resumeSeconds, 1)];
            [playVideo play];
        }
        else
        {
            [playVideo performSelector:@selector(play) withObject:nil afterDelay:1];
        }
    }
    
    if(_playerViewController.view.hidden)
    {
        [self playVideoAtIndex:page];
    }
    
}

-(void)dragStartedForSlider
{
    [timer invalidate];
//    [_videoPlayerController stop];
}
-(void)dragEndedForSlider
{
//    timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];\
//    [_videoPlayerController._playerItem seekToTime:CMTimeMakeWithSeconds((int)mySlider.value,1)];
//    [_videoPlayerController playFromCurrentTime];
}

- (IBAction)moveSlider {
    
//    [_videoPlayerController._playerItem seekToTime:CMTimeMakeWithSeconds((int)mySlider.value,1)];
}

-(void)setupScrollView{
    
    _scrollView.delegate = self;

    
    for (int i = 0; i < _storiesArray.count; i++)
    {
        VideoModel *model = [_storiesArray objectAtIndex:i];
        CGRect frame;
        frame.origin.x = _scrollView.frame.size.width * i;
        frame.origin.y = _scrollView.frame.origin.y;
        frame.size = [[UIScreen mainScreen] bounds].size;
        _scrollView.pagingEnabled = YES;
        
        subview = [[UIView alloc] initWithFrame:frame];
        subview.tag = i;
       
        progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(5, 20, frame.size.width-10, 10)];
        progressView.tintColor = [UIColor whiteColor];
        
        profileImage = [[UIImageView alloc] initWithFrame: CGRectMake(20, 40, 50, 50)];
        [profileImage sd_setImageWithURL:[NSURL URLWithString:model.profile_image] placeholderImage:[UIImage imageNamed:@"place_holder"]];
        profileImage.layer.cornerRadius=25;
        profileImage.layer.masksToBounds = YES;
        
        nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(80, 55, frame.size.width - 75, 20)];
        nameLbl.textColor = [UIColor whiteColor];
        
        NSString *decodedString = [Utils getDecryptedTextFor:model.userName];
        
//        nameLbl.text = model.userName;
        nameLbl.text = decodedString;

        nameLbl.font = [UIFont fontWithName:@"Montserrat-Regular" size:18];
        
        nameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nameBtn.frame = CGRectMake(80, 55, frame.size.width - 150, 20);
        nameBtn.tag = 424;
        [nameBtn addTarget:self action:@selector(btnUserChannel:) forControlEvents:UIControlEventTouchUpInside];
        
        showOptionsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        showOptionsBtn.frame = CGRectMake(0, 80, frame.size.width, _scrollView.frame.size.height);
        showOptionsBtn.tag = 444;
//        showOptionsBtn.backgroundColor = [UIColor redColor];
//        [showOptionsBtn addTarget:self action:@selector(showOptions) forControlEvents:UIControlEventTouchUpInside];
        
        closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width - 55, 50, 30, 30)];
        [closeBtn addTarget:self action:@selector(closeBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        
        activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(frame.size.width/2 - 10, frame.size.height/2, 40, 40)];
        activityIndicator.hidden = YES;
        
        [subview addSubview:closeBtn];
//        [subview addSubview:progressView];
        [subview addSubview:profileImage];
        [subview addSubview:nameLbl];
        [subview addSubview:nameBtn];
        [subview addSubview:activityIndicator];
//        [subview addSubview:showOptionsBtn];

        [_scrollView addSubview:subview];
    }
    
    if(_indexRow != 0)
        _scrollView.hidden = YES;
    self.scrollView.contentSize =  CGSizeMake(self.view.frame.size.width * _storiesArray.count, [[UIScreen mainScreen] bounds].size.height-20);
   
    // go to the story first time when user clicks the story
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width * _indexRow, -20) animated:YES];
    tempPage = -1;
    _uploadIndex = -1;
    page = (int)_indexRow;
    
    shownYposition = _lowerOptionsView.frame.origin.y;
    hiddenYposition = _lowerOptionsView.frame.origin.y + 70;
    
    [self playVideoAtIndex:_indexRow];
    
}

#pragma mark ScrollViewDelegates
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //[_videoPlayerController pause];
    if(_scrollView.contentOffset.x == _scrollView.frame.size.width * _indexRow){
        _scrollView.hidden = NO;
    }
    if(tempPage != page) {
        tempPage = page;
    }
}

- (IBAction)btnUserChannel:(id)sender {
    
        isForName = YES;
//    [_videoPlayerController stop];
        VideoModel *model = [_storiesArray objectAtIndex:page];
    if(!_isFromSpeakers)
    {
        model = [backupFriendsStories objectAtIndex:page];
    }
        UserChannel *commentController ;
        if(IS_IPAD){
            commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
        }else if (IS_IPHONEX){
            commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
        }
        else{
            commentController  = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
            
        }
        commentController.ChannelObj = nil;
        Followings *fData  = [[Followings alloc] init];
        BOOL isCl= [model.isCelebrity boolValue];
        fData.is_celeb = [NSString stringWithFormat:@"%d",isCl];
        commentController.friendID   = model.user_id;
        commentController.currentUser = fData;
        [self.navigationController pushViewController:commentController animated:YES];
}
-(void)animateImages{
    NSArray *loaderImages = @[@"state1.png", @"state2.png", @"state3.png"];
    NSMutableArray *loaderImagesArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < loaderImages.count; i++) {
        [loaderImagesArr addObject:[UIImage imageNamed:[loaderImages objectAtIndex:i]]];
    }
    audioBtnImage.animationImages = loaderImagesArr;
    audioBtnImage.animationDuration = 0.5f;
    [audioBtnImage startAnimating];
}
- (IBAction)recorderTapped:(id)sender {
    closeBtnAudio.hidden = true;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                        error:nil];
    if(!isRecording){
        [self animateImages];
        timerToupdateLbl = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
        audioTimeOut = [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self
                                                      selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: NO];
        [_audioRecorder record];
    }
    else{
        
        [_audioRecorder stop];
        [audioBtnImage stopAnimating];
    }
    isRecording = true;
}
-(void) callAfterSixtySecond:(NSTimer*) t
{
    [_audioRecorder stop];
    [timerToupdateLbl invalidate];
    [audioTimeOut invalidate];
    
}
-(void) updateCountdown {
    int minutes, seconds;
    secondsLeft--;
    minutes = (secondsLeft / 60) % 60;
    seconds = (secondsLeft) % 60;
    countDownlabel.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    secondsConsumed = [NSString stringWithFormat:@"%02d:%02d", 00, 60 - secondsLeft];
}
-(void)setAudioRecordSettings
{
    NSArray *dirPaths;
    NSString *docsDir;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(
                                                   NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString *soundFilePath = [docsDir
                               stringByAppendingPathComponent:@"sound.caf"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    //[NSNumber numberWithInt:kAudioFormatMPEGLayer3], AVFormatIDKey,
                                    [NSNumber numberWithInt:AVAudioQualityHigh],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 1],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    
    NSError *error = nil;
    
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];
    _audioRecorder.delegate = self;
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
        
    } else {
        [_audioRecorder prepareToRecord];
    }
}
-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    [timerToupdateLbl invalidate];
    [audioTimeOut invalidate];
    closeBtnAudio.hidden = false;
    isRecording = false;
    countDownlabel.text = @"01:00";
    secondsLeft = 60;
    _CaudioData = [NSData dataWithContentsOfURL:_audioRecorder.url];
    
//    [_uploadAudioView removeFromSuperview];
    _uploadAudioView.hidden = YES;
    
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.dataToUpload = _CaudioData;
    uploadController.videoPath = _audioRecorder.url;
    if([secondsConsumed length] == 0)
        secondsConsumed = @"00:01";
    uploadController.video_duration = secondsConsumed;
    
    uploadController.friendsArray   = appDelegate.friendsArray;
    if(!self.isFromDeepLink){
        if(_isFirstComment || appDelegate.parentIdStackController.count == 1)
            uploadController.ParentCommentID = @"-1";
        else
        {
            
            uploadController.ParentCommentID = (NSString*)[appDelegate.parentIdStackController peekObject];
        }
    }
    else{
        uploadController.ParentCommentID = _pID;
    }
    uploadController.postID = _videoModel.videoID;
    uploadController.isAudio = true;
    uploadController.isComment = TRUE;
    self.thumbnailToUpload = [UIImage imageNamed: @"splash_audio_image.png"];
    uploadController.thumbnailImage = [UIImage imageNamed: @"splash_audio_image.png"];
    [[self navigationController] pushViewController:uploadController animated:YES];
    
}

#pragma mark - HBRECORDER Recording Methods

- (void)recorder:(HBRecorder *)recorder  didFinishPickingMediaWithUrl:(NSURL *)videoUrl {
    
    _videoPath = videoUrl;
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        AVAsset *asset = [AVAsset assetWithURL:_videoPath];// url= give your url video here
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        
        imageGenerator.appliesPreferredTrackTransform = YES;
        
        CMTime time = CMTimeMake(5, 10);//it will create the thumbnail after the 5 sec of video
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
        self.thumbnailToUpload = thumbnail;
        int i = 0;
        
        if(i == 0) {
            AVAsset *asset = [AVAsset assetWithURL:videoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime thumbTime = CMTimeMake(10,5);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTime actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
            self.thumbnailToUpload2 = thumbnail;
            
            i++;
        }
        
        if(i == 1) {
            AVAsset *asset = [AVAsset assetWithURL:videoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime thumbTimes = CMTimeMake(10,1);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTimes actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
            self.thumbnailToUpload3 = thumbnail;
        }
        
        
        _profileData = UIImagePNGRepresentation(thumbnail);
        _CmovieData = [NSData dataWithContentsOfURL:videoUrl];
        
        CMTime CMduration = asset.duration;
        int totalSeconds = CMTimeGetSeconds(CMduration);
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;
        NSString *duration = @"";
        if (hours>0) {
            NSString *hoursString = [NSString stringWithFormat:@"%d hour(s)",hours];
            duration = [duration stringByAppendingString:hoursString];
        }
        if (minutes>0) {
            NSString *minString = [NSString stringWithFormat:@"%d min(s)",minutes];
            duration = [duration stringByAppendingString:minString];
        }
        if (seconds>0) {
            NSString *secString = [NSString stringWithFormat:@"%d sec(s)",seconds];
            duration = [duration stringByAppendingString:secString];
        }
        video_duration = [[NSString alloc]initWithFormat:@"%02lu:%02lu",(unsigned long)minutes,(unsigned long)seconds];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(playVideo)
            {
                [playVideo pause];
            }
            [self movetoUploadBeamController];
        });
    });
    
    
}
-(void) movetoUploadBeamController{
    
    shudContinuePlayer = false;
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.dataToUpload = _CmovieData;
    uploadController.video_duration = video_duration;
    uploadController.friendsArray   = appDelegate.friendsArray;
    uploadController.orientationAngle = orientationAngle;
    
    if(!self.isFromDeepLink){
        if(_isFirstComment || appDelegate.parentIdStackController.count == 1){
            uploadController.ParentCommentID = @"-1";
        }
        else
        {
            uploadController.ParentCommentID = (NSString*)[appDelegate.parentIdStackController peekObject];
        }
    }
    else{
        uploadController.ParentCommentID = _pID;
    }
    uploadController.postID = _videoModel.videoID;
    uploadController.isAudio = false;
    uploadController.profileData = _profileData;
    uploadController.isComment = TRUE;
    uploadController.thumbnailImage = self.thumbnailToUpload;
    uploadController.thumbnailImage2 = self.thumbnailToUpload2;
    uploadController.thumbnailImage3 = self.thumbnailToUpload3;
    uploadController.friendsArray   = appDelegate.friendsArray;
    if(uploadAnonymous)
        uploadController.isAnonymous = true;
    else
        uploadController.isAnonymous = false;
    [[self navigationController] pushViewController:uploadController animated:YES];
}
- (void)recorderDidCancel:(HBRecorder *)recorder {
    NSLog(@"Recorder did cancel..");
}

- (void)recorderOrientation:(NSInteger)orientation {
    orientationAngle = orientation;
}
-(void)audioRecorderEncodeErrorDidOccur:
(AVAudioRecorder *)recorder
                                  error:(NSError *)error
{  isRecording = false;
    closeBtnAudio.hidden = false;
    countDownlabel.text = @"00:00";
    secondsLeft = 60;
    secondsConsumed = 0;
    //    NSLog(@"Encode Error occurred");
}
- (IBAction)beamPressed:(UIButton *)sender {
    if([sender tag] == 100){
        NSDate *lastAnonymusBeamDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"lastAnonymusBeam"];
        uploadAnonymous = true;
        uploadBeamTag = false;
        
    }
    else if([sender tag ] == 101)
    {
        uploadAnonymous = false;
        uploadBeamTag = true;
    }
    if([_videoModel.reply_count intValue] == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Comments are not allowed on this Beam" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else{
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            NSBundle *bundle = [NSBundle bundleForClass:HBRecorder.class];
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HBRecorder.bundle/HBRecorder" bundle:bundle];
            
            HBRecorder *recorder = [sb instantiateViewControllerWithIdentifier:@"HBRecorder"];
            recorder.delegate = self;
            if(uploadAnonymous) {
                UIImageView *anonymousMark = [[UIImageView alloc] initWithFrame:CGRectMake(10, 64, recorder.view.frame.size.width - 20, recorder.view.frame.size.height - 150)];
                anonymousMark.image = [UIImage imageNamed:@"ano_new_icon"];
                anonymousMark.alpha = 0.5;
                [recorder.view addSubview:anonymousMark];
            }
            recorder.topTitle = @"";
            recorder.bottomTitle = @"";
            recorder.maxRecordDuration = 60 * 1;
            recorder.movieName = @"MyAnimatedMovie";
            
            
            recorder.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self.navigationController pushViewController:recorder animated:YES];
            
        } else {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"I'm afraid there's no camera on this device!" delegate:nil cancelButtonTitle:@"Dang!" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}


- (IBAction)RecorderPressed:(id)sender {
    if([_videoModel.reply_count intValue] == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Comments are not allowed on this Beam" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        resumeSeconds = CMTimeGetSeconds(playVideo.currentTime);
        [playVideo pause];
        _uploadAudioView.frame = CGRectMake(0, 0,self.scrollView.frame.size.width, self.scrollView.frame.size.height);
        _uploadAudioView.hidden = NO;
    }
    
}
- (IBAction)AudioClosePressed:(id)sender {
    _uploadAudioView.hidden = YES;
    if(playVideo)
    {
        if(resumeSeconds)
        {
            [playVideo seekToTime:CMTimeMakeWithSeconds(resumeSeconds, 1)];
            [playVideo play];
        }
        else
        {
            [playVideo performSelector:@selector(play) withObject:nil afterDelay:1];
        }
    }

}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {

    CGFloat pageWidth = _scrollView.frame.size.width;
    page = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = _scrollView.frame.size.width;
    page = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(tempPage != page && tempPage != -1){
        tempPage = page;
        [self playVideoAtIndex:page];
    } else if(tempPage == _storiesArray.count - 1){
//        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)playVideoAtIndex:(NSInteger) indexpath {
    CGRect frame;
    frame.origin.x = _scrollView.frame.size.width * indexpath;
    frame.size = [[UIScreen mainScreen] bounds].size;
    
    for (UIView *i in self.scrollView.subviews){
        if([i isKindOfClass:[UIView class]]){
//            UIView *backgroundView = (UIView *)i;
//            if(backgroundView.tag == 1001) {
//                for (UIView *j in backgroundView.subviews){
//                    if([j isKindOfClass:[UIView class]]){
//                        UIView *newView = (UIView *)j;
//                        if(newView.tag == page){
//                            /// Write your code
//                            _replaceIndex = indexpath;
//
//                            for(UIProgressView *progressview in newView.subviews) {
//                                if([progressview isKindOfClass:[UIProgressView class]]) {
//                                    progressView = progressview;
//                                    progressView.progress = 0.0;
//                                }
//                            }
//                            for(UIActivityIndicatorView *activityview in newView.subviews) {
//                                if([activityview isKindOfClass:[UIActivityIndicatorView class]]) {
//                                    activityIndicator = activityview;
//                                    activityIndicator.hidden = NO;
//                                    [activityIndicator startAnimating];
//
//                                }
//                            }
//                            _videoPlayerController = [[PBJVideoPlayerController alloc] init];
//                            _videoPlayerController.delegate = self;
//                            _videoPlayerController.view.frame = CGRectMake(frame.origin.x, profileImage.frame.origin.y + profileImage.frame.size.height + 10, frame.size.width, frame.size.height - (profileImage.frame.origin.y + profileImage.frame.size.height + 50));
//
//                            _videoModel = [_storiesArray objectAtIndex:indexpath];
//                            NSURL *bipbopUrl = [[NSURL alloc] initWithString:_videoModel.video_link];
//                            NSNumber *value =[self secondsForTimeString:_videoModel.video_length];
//                            duration = [value floatValue];
//                            _videoPlayerController.asset = [[AVURLAsset alloc] initWithURL:bipbopUrl options:nil];
//                            [_videoPlayerController playFromBeginning];
//                    }
//                }
//            }
            UIView *newView = (UIView *)i;
            if(newView.tag == page){
                /// Write your code
                _replaceIndex = indexpath;

                for(UIProgressView *progressview in newView.subviews) {
                    if([progressview isKindOfClass:[UIProgressView class]]) {
                        progressView = progressview;
                        progressView.progress = 0.0;
                    }
                }
                for(UIActivityIndicatorView *activityview in newView.subviews) {
                    if([activityview isKindOfClass:[UIActivityIndicatorView class]]) {
                        activityIndicator = activityview;
                        activityIndicator.hidden = NO;
                        [activityIndicator startAnimating];

                    }
                }
//                _videoPlayerController = [[PBJVideoPlayerController alloc] init];
//                _videoPlayerController.delegate = self;
//                _videoPlayerController.view.frame = CGRectMake(frame.origin.x, profileImage.frame.origin.y + profileImage.frame.size.height + 10, frame.size.width, frame.size.height - (profileImage.frame.origin.y + profileImage.frame.size.height + 50));

                _videoModel = [_storiesArray objectAtIndex:indexpath];
                NSURL *bipbopUrl = [[NSURL alloc] initWithString:_videoModel.video_link];
                NSNumber *value =[self secondsForTimeString:_videoModel.video_length];
                duration = [value floatValue];
//                _videoPlayerController.asset = [[AVURLAsset alloc] initWithURL:bipbopUrl options:nil];
                mySlider.transform = CGAffineTransformMakeScale(0.6, 0.6);
                mySlider.frame = CGRectMake(0, -3, [[UIScreen mainScreen] bounds].size.width, 30);
                mySlider.maximumValue = duration;
                mySlider.value = 0.0;
                
                [mySlider addTarget:self action:@selector(dragEndedForSlider)
                   forControlEvents:UIControlEventTouchUpInside];
                
                [mySlider addTarget:self action:@selector(dragStartedForSlider)
                   forControlEvents:UIControlEventTouchDown];
                
                UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTapped:)];
                [mySlider addGestureRecognizer:gr];
                
//                timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
//                [_videoPlayerController playFromBeginning];
                
                
                asset = [AVAsset assetWithURL:bipbopUrl];
                playerItem = [[AVPlayerItem alloc] initWithAsset:asset];
                playVideo = [[AVPlayer alloc] initWithPlayerItem:playerItem];
                playVideo.automaticallyWaitsToMinimizeStalling = true;
                playVideo.currentItem.preferredForwardBufferDuration = 1;
                
                NSError *error;
                [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker
                                                                   error:&error];
                if(error)
                {
                    NSLog(@"Error: AudioSession cannot use speakers");
                }
                
//                _playerViewController = [[AVPlayerViewController alloc] init];
                _playerViewController.player = playVideo;
                _playerViewController.showsPlaybackControls = false;
                playVideo.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                _playerViewController.view.frame = CGRectMake(frame.origin.x, profileImage.frame.origin.y + profileImage.frame.size.height + 20, frame.size.width, frame.size.height - (profileImage.frame.origin.y + profileImage.frame.size.height) - 170);
//                [self.scrollView addSubview:_playerViewController.view];
                [self.scrollView bringSubviewToFront:_playerViewController.view];
                _playerViewController.view.hidden = true;
                
                
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(playerItemDidReachEnd:)
                                                             name:AVPlayerItemDidPlayToEndTimeNotification
                                                           object:[playVideo currentItem]];
                
                [playVideo play];
                if(!timer)
                {
                    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkIfPlayerReady) userInfo:nil repeats:YES];
                }
//                [playVideo.currentItem addObserver:self forKeyPath:@"status" options:0 context:nil];
            }
        }
    }
}

-(void)checkIfPlayerReady
{
//    NSLog(@"%f",CMTimeGetSeconds(playVideo.currentTime));
    if(CMTimeGetSeconds(playVideo.currentTime) > 0 && _playerViewController.view.hidden)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [timer invalidate];
            timer = nil;
        });
        _playerViewController.view.hidden = false;
        _playerViewController.showsPlaybackControls = true;
        [activityIndicator removeFromSuperview];
        [self postSeenCall];
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
//    AVPlayerItem *p = [notification object];
//    [p seekToTime:kCMTimeZero];
//    [playVideo play];
    page++;
    if(page <= _storiesArray.count-1){
        [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width * page, -20) animated:YES];
        [self playVideoAtIndex:page];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if (object == playVideo.currentItem && [keyPath isEqualToString:@"status"]) {
        if (playVideo.status == AVPlayerStatusReadyToPlay)
        {
            _playerViewController.view.hidden = false;
            [playVideo play];
            [activityIndicator removeFromSuperview];
            [self postSeenCall];
        }
        else if (playVideo.status == AVPlayerStatusFailed)
        {
        }
    }
}


-(void)appWillBeomceActive:(NSNotification*)note
{
//    [playVideo play];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    resumeSeconds = CMTimeGetSeconds(playVideo.currentTime);
    [playVideo pause];
    
    if(_playerViewController.view.hidden)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [timer invalidate];
            timer = nil;
        });
        [playVideo replaceCurrentItemWithPlayerItem:nil];
        playVideo = nil;
    }

    if(shudClearPlayer)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
        _playerViewController = nil;
        playVideo = nil;
    }
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
//    [playVideo pause];
//    playVideo = nil;
}

- (void)sliderTapped:(UIGestureRecognizer *)g
{
    UISlider* s = (UISlider*)g.view;
    if (s.highlighted)
        return; // tap on thumb, let slider deal with it
    CGPoint pt = [g locationInView: s];
    CGFloat percentage = pt.x / s.bounds.size.width;
    CGFloat delta = percentage * (s.maximumValue - s.minimumValue);
    CGFloat value = s.minimumValue + delta;
    [s setValue:value animated:YES];
    
//    [_videoPlayerController._playerItem seekToTime:CMTimeMakeWithSeconds((int)mySlider.value,1)];
}

- (void)updateTime:(NSTimer *)timer {
//    mySlider.value = CMTimeGetSeconds([_videoPlayerController._playerItem currentTime]);
}

- (NSNumber *)secondsForTimeString:(NSString *)string {
    
    if([string isEqualToString:@""])
    {
        return 0;
    }
    NSArray *components = [string componentsSeparatedByString:@":"];
    NSInteger seconds;
    NSInteger hours = 0;
    NSInteger minutes = 0;
    
    if (components.count > 2){
        hours   = [[components objectAtIndex:0] integerValue];
        minutes = [[components objectAtIndex:1] integerValue];
        seconds = [[components objectAtIndex:2] integerValue];
    }
    else{
        minutes = [[components objectAtIndex:0] integerValue];
        seconds = [[components objectAtIndex:1] integerValue];
    }
    return [NSNumber numberWithInteger:(hours * 60 * 60) + (minutes * 60) + seconds];
}

#pragma mark - PBJVideoPlayerControllerDelegate
- (void)videoPlayerReady:(PBJVideoPlayerController *)videoPlayer
{

    BOOL check = false;
    NSLog(@"Max duration of the video: %f", videoPlayer.maxDuration);
    if(_uploadIndex != _replaceIndex) {
        for (int i = 0; i<_checkArray.count; i++) {
            VideoModel *model = [_checkArray objectAtIndex:i];
            if([_videoModel.videoID isEqualToString:model.videoID]) {
                check = true;
                break;
            }
        }
        if(!check){
            [self postSeenCall];
            _uploadIndex = _replaceIndex;
        }
    }
//    [_videoPlayerController.view addSubview:showOptionsBtn];
//    [self.scrollView addSubview:_videoPlayerController.view];
}

- (void)videoPlayerPlaybackStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
    switch (videoPlayer.playbackState) {
        case PBJVideoPlayerPlaybackStateStopped:
            NSLog(@"STOPPED BDDD!");
            
            break;
            
        case PBJVideoPlayerPlaybackStatePlaying:
             // _scrollView.hidden = NO;
            NSLog(@"PLAYING BDDDD");
            
            break;
            
        case PBJVideoPlayerPlaybackStatePaused:
            NSLog(@"PAUSED BDDD.");

            if(page == _storiesArray.count-1)
            {
                if(!isForName)
                {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
            if(videoPlayer.playbackState == PBJVideoPlayerPlaybackStatePaused){
//                [_videoPlayerController playFromCurrentTime];
            }
            break;
        default:
            break;
    }
}

- (void)videoPlayerPlaybackWillStartFromBeginning:(PBJVideoPlayerController *)videoPlayer
{
    NSLog(@"PLAYBACK videoPlayerPlaybackWillStartFromBeginning");
}

- (void)videoPlayerPlaybackDidEnd:(PBJVideoPlayerController *)videoPlayer
{
    NSLog(@"PLAYBACK DID END ");
    page++;
    if(page <= _storiesArray.count-1){
        [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width * page, -20) animated:YES];
        [self playVideoAtIndex:page];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
  //  [_videoPlayerController playFromBeginning];
}

- (void)videoPlayerBufferringStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
    switch (videoPlayer.bufferingState) {
        case PBJVideoPlayerBufferingStateUnknown:
            NSLog(@"Buffering state unknown!");
            break;
            
        case PBJVideoPlayerBufferingStateReady:
            //[activityIndicator removeFromSuperview];
            _scrollView.hidden = NO;
            activityIndicator.hidden = YES;
            [self startHidingOptions];
            if(videoPlayer.playbackState == PBJVideoPlayerPlaybackStatePaused){
//                [_videoPlayerController playFromCurrentTime];
            }
            mySliderBackground.hidden = false;
            NSLog(@"Buffering state Ready! Video will start/ready playing now.");
            break;
            
        case PBJVideoPlayerBufferingStateDelayed:
//            [_videoPlayerController pause];
            activityIndicator.hidden = NO;
            NSLog(@"Buffering state Delayed! Video will pause/stop playing now.");
            break;
        default:
            break;
    }
}

- (void)videoPlayer:(PBJVideoPlayerController *)videoPlayer didUpdatePlayBackProgress:(CGFloat)progress {
    //NSLog(@"Completed of the video: %f", progress);
    progressView.progress = progress/duration;
}

- (IBAction)closeBtnPressed:(id)sender {
//    [_videoPlayerController stop];
    shudClearPlayer = true;
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark Server Call
- (void) postSeenCall{
    UIButton *closeBtn = nil;
    for (UIView *i in self.scrollView.subviews){
        if([i isKindOfClass:[UIView class]]){
            UIView *newView = (UIView *)i;
            if(newView.tag == page){
                
                for(UIButton *btn in newView.subviews) {
                    if([btn isKindOfClass:[UIButton class]] && btn.tag != 424 && btn.tag != 444) {
                        closeBtn = btn;
                        closeBtn.enabled = NO;
                        closeBtn.alpha = 1.0;

                    }
                }
            }
        }
    }
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_POST_SEEN,@"method",
                            token,@"session_token",language,@"language",_videoModel.videoID,@"post_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            _uploadIndex = _replaceIndex;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1 || success == 4) {
                
                [_checkArray addObject:_videoModel];
                NSLog(@"Success");
                
                
                NSMutableArray *tempSpeakerArray = sharedInstanse.speakersStoriesArray;
                if(!_isFromSpeakers)  ////////////FOR FRIENDS STORIES///////////
                {
                    tempSpeakerArray = _storiesArray;
                }
                
                
                if(_count <= _replaceIndex)
                {
                    [tempSpeakerArray removeObjectAtIndex:_replaceIndex-_count];
                }
                [tempSpeakerArray addObject:_videoModel];
                
                VideoModel *vModel = [[VideoModel alloc]init];

                if(_isFromSpeakers)
                {
                    sharedInstanse.speakersStoriesArray = tempSpeakerArray;
                    vModel  = [sharedInstanse.speakersStoriesArray objectAtIndex:[sharedInstanse.speakersStoriesArray count]-1];
                }
                else
                {
                    _storiesArray = tempSpeakerArray;
                    vModel  = [_storiesArray objectAtIndex:[_storiesArray count]-1];
                }
                vModel.viewed_by_me = @"1";
                _count++;
                closeBtn.enabled = YES;
            }
            else {
                closeBtn.enabled = YES;
                NSLog(@"Fail");
            }
        }
        else{
            closeBtn.enabled = YES;
            NSLog(@"Fail");
        }
        
    }];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
