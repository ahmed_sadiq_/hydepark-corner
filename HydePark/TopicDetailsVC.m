//
//  TopicDetailsVC.m
//  HydePark
//
//  Created by Osama on 31/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "TopicDetailsVC.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "VideoModel.h"
#import "NewHomeCells.h"
#import "UIImageView+RoundImage.h"
#import "Utils.h"
#import "UIImageView+WebCache.h"
#import "CommentsVC.h"
#import "NSData+AES.h"

@interface TopicDetailsVC (){
    NSMutableArray *beamsArray;
}
@end

@implementation TopicDetailsVC
@synthesize tModel;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    // Do any additional setup after loading the view from its nib.
    self.titleLabel.text = self.tittle;
    self.pageNum = 1;
    [self initFooterView];
    [self fetchBeamsById];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.background.hidden = YES;
        }
        else
        {
            self.background.hidden = NO;
        }
    }
    else
    {
        self.background.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ServerCall
-(void)fetchBeamsById{
    NSString *language = [Utils getLanguageCode];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.fetchingContent = TRUE;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageNum = [NSString stringWithFormat:@"%d",self.pageNum];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"getBeamsByTopicIds",@"method",
                              token,@"Session_token",pageNum,@"page_no",language,@"language",tModel.topic_id,@"topic_ids",nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        self.fetchingContent = FALSE;
        if(success){
            if(self.pageNum == 1)
                beamsArray = [NSMutableArray new];
            NSArray *bArr = [responseObject objectForKey:@"beams"];
            if(bArr.count>0){
            for(int i = 0; i < bArr.count; i++){
                NSDictionary *tempDict = [bArr objectAtIndex:i];
                VideoModel *vModel = [VideoModel new];
                vModel.title           = [tempDict objectForKey:@"caption"];
                vModel.comments_count  = [tempDict objectForKey:@"comment_count"];
                vModel.userName        = [tempDict objectForKey:@"full_name"];
                vModel.topic_id        = [tempDict objectForKey:@"topic_id"];
                vModel.user_id         = [tempDict objectForKey:@"user_id"];
                vModel.profile_image   = [tempDict objectForKey:@"profile_link"];
                vModel.like_count      = [tempDict objectForKey:@"like_count"];
                vModel.like_by_me      = [tempDict objectForKey:@"liked_by_me"];
                vModel.seen_count      = [tempDict objectForKey:@"seen_count"];
                vModel.video_angle     = [[tempDict objectForKey:@"video_angle"] intValue];
                vModel.beam_share_url = [tempDict objectForKey:@"beam_share_url"];
                vModel.deep_link = [tempDict objectForKey:@"deep_link"];
                
                vModel.video_link      = [tempDict objectForKey:@"video_link"];
                vModel.m3u8_video_link = [tempDict objectForKey:@"m3u8_video_link"];
                vModel.video_thumbnail_link = [tempDict objectForKey:@"video_thumbnail_link"];
                vModel.videoID         = [tempDict objectForKey:@"id"];
                vModel.Tags            = [tempDict objectForKey:@"tag_friends"];
                vModel.video_length    = [tempDict objectForKey:@"video_length"];
                vModel.is_anonymous    = [tempDict objectForKey:@"is_anonymous"];
                vModel.reply_count     = [tempDict objectForKey:@"reply_count"];
                vModel.current_datetime= [tempDict objectForKey:@"current_datetime"];
                vModel.uploaded_date   = [tempDict objectForKey:@"uploaded_date"];
                vModel.beam_share_url = [tempDict objectForKey:@"beam_share_url"];
                vModel.isMute          = @"0";
                vModel.privacy         = [tempDict objectForKey:@"privacy"];
                vModel.isThumbnailReq =[[tempDict objectForKey:@"is_thumbnail_required"] intValue];
                vModel.beamType = [[tempDict objectForKey:@"beam_type"] intValue];
                if([tempDict objectForKey:@"original_beam_data"]){
                    vModel.originalBeamData = [tempDict objectForKey:@"original_beam_data"];
                }
                [beamsArray addObject:vModel];
            }
            }else{
                self.cannotScrollMore = TRUE;
            }
            [self.tblView reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        self.fetchingContent = FALSE;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

#pragma mark ScrollView Delegates
-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                    withVelocity:(CGPoint)velocity
             targetContentOffset:(inout CGPoint *)targetContentOffset{
    if (velocity.y > 0){
//        self.isDownwards = YES;
    }
    if (velocity.y < 0){
//        self.isDownwards = NO;
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{

}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!self.fetchingContent && indexPath.row == beamsArray.count/3 - 1 && !self.cannotScrollMore) {
        self.pageNum++;
        [self fetchBeamsById];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    float returnValue;
    if (IS_IPAD)
        returnValue = 260.0f;
    else if(IS_IPHONE_5)
        returnValue = 150.0f;
    else if(IS_IPHONE_6Plus){
        returnValue = 145.0;
    }
    else
        returnValue = 130.0f;
    return returnValue;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows = (int)([beamsArray count]/3 );
    if([beamsArray count] %3 != 0) {
        rows++;
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"videoCells";
    NewHomeCells *cell;
    currentIndex = (indexPath.row * 3);
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects;
        if(IS_IPAD){
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells_iPad" owner:self options:nil];
        }else if(IS_IPHONE_5){
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHome_iPhone5" owner:self options:nil];
        }else{
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
        }
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
        //            cell.leftreplImg.hidden =  NO;
        //            cell.rightreplImg.hidden = NO;
    }else{
        cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    
    if(IS_IPHONE_6Plus){
        cell.view1.frame =CGRectMake(cell.view1.frame.origin.x , cell.view1.frame.origin.y, cell.view1.frame.size.width , cell.view1.frame.size.height);
        cell.view2.frame =CGRectMake(cell.view2.frame.origin.x , cell.view2.frame.origin.y, cell.view2.frame.size.width , cell.view2.frame.size.height);
        
        cell.leftreplImg.frame = CGRectMake(cell.leftreplImg.frame.origin.x , cell.leftreplImg.frame.origin.y, cell.leftreplImg.frame.size.width, cell.leftreplImg.frame.size.width);
        cell.CH_CommentscountLbl.frame = CGRectMake(cell.CH_CommentscountLbl.frame.origin.x , cell.CH_CommentscountLbl.frame.origin.y, cell.CH_CommentscountLbl.frame.size.width , cell.CH_CommentscountLbl.frame.size.height);
        cell.rightreplImg.frame = CGRectMake(cell.rightreplImg.frame.origin.x , cell.rightreplImg.frame.origin.y, cell.rightreplImg.frame.size.width, cell.rightreplImg.frame.size.width);
        cell.CH_RCommentscountLbl.frame = CGRectMake(cell.CH_RCommentscountLbl.frame.origin.x , cell.CH_RCommentscountLbl.frame.origin.y, cell.CH_RCommentscountLbl.frame.size.width , cell.CH_RCommentscountLbl.frame.size.height);
    }
    
    VideoModel *tempVideos = [[VideoModel alloc]init];
    tempVideos  = [beamsArray objectAtIndex:currentIndex];

    
    cell.CH_userName.text = [Utils getDecryptedTextFor:tempVideos.userName];
    cell.Ch_videoLength.text = tempVideos.video_length;
    cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
    cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
    if([tempVideos.comments_count isEqualToString:@"0"])
    {
        cell.CH_CommentscountLbl.hidden = YES;
        cell.leftreplImg.hidden = YES;
    }
    else{
        cell.CH_CommentscountLbl.text = tempVideos.comments_count;
    }
    cell.CH_heartCountlbl.text = tempVideos.like_count;
    cell.CH_seen.text = tempVideos.seen_count;
    [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
    if([tempVideos.is_anonymous  isEqualToString: @"0"]){
        cell.dummyLeft1.hidden = YES;
    }
    else{
        cell.dummyLeft1.image = [UIImage imageNamed:@"btanonymous.png"];
        cell.dummyLeft1.hidden = NO;
        cell.CH_userName.text = @"Anonymous";
        cell.userProfileView.enabled = false;
    }
    
    if(IS_IPAD)
       // cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /7.4f;
    cell.imgContainer.layer.masksToBounds = YES;
    [cell.CH_Video_Thumbnail roundCorners];
  
    if(tempVideos.beamType == 1){
        cell.CH_heart.hidden = NO;
    }
    else if(tempVideos.beamType == 2){
        cell.CH_heart.hidden = YES;
        NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

    }
    else{
        cell.CH_heart.hidden = YES;
    }
    [cell.CH_playVideo setTag:currentIndex];
    cell.CH_commentsBtn.enabled = YES;
    cell.CH_RcommentsBtn.enabled = YES;
    [cell.CH_commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.CH_commentsBtn setTag:currentIndex];
    
    currentIndex++;
    if(currentIndex < beamsArray.count)
    {
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [beamsArray objectAtIndex:currentIndex];
        [cell.CH_RcommentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CH_RcommentsBtn setTag:currentIndex];
        cell.rightTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
        cell.CH_RVideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.CH_Rseen.text = tempVideos.seen_count;
        
        if(IS_IPAD)
            cell.RimgContainer.layer.cornerRadius  = cell.RimgContainer.frame.size.width /7.4f;
        cell.RimgContainer.layer.masksToBounds = YES;
        [cell.CH_RVideo_Thumbnail roundCorners];
        
        
        cell.CH_RheartCountlbl.text             = tempVideos.like_count;
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.CH_RCommentscountLbl.hidden = YES;
            cell.rightreplImg.hidden = YES;
        }
        else{
            cell.CH_RCommentscountLbl.text = tempVideos.comments_count;
        }
        
        cell.CH_RuserName.text = [Utils getDecryptedTextFor:tempVideos.userName];
        [cell.CH_RVideo_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
     
        
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.dummyright1.hidden = YES;
        }
        else{
            cell.dummyright1.image = [UIImage imageNamed:@"btanonymous.png"];
            cell.dummyright1.hidden = NO;
            cell.CH_RuserName.text = @"Anonymous";
        }
        
        if(tempVideos.beamType == 1){
            cell.CH_Rheart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            cell.CH_Rheart.hidden = YES;
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.CH_RuserName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        else{
            cell.CH_Rheart.hidden = YES;
        }
        
        currentIndex++;
    }
    else{
        cell.view2.hidden = YES;
        cell.outline2.hidden = YES;
        cell.rightreplImg.hidden     = YES;
        cell.CH_RCommentscountLbl.hidden = YES;
    }
    if(currentIndex < beamsArray.count){
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [beamsArray objectAtIndex:currentIndex];
        cell.rightDate.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.rightUsername.text = tempVideos.userName;
        [cell.rightThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        [cell.rightThumbnail roundCorners];
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.replyRedBg.hidden = YES;
            cell.replyCountlbl.hidden = YES;
        }
        else{
            cell.replyRedBg.hidden = NO;
            cell.replyCountlbl.hidden = NO;
            cell.replyCountlbl.text = tempVideos.comments_count;
        }
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.dummyright1.hidden = YES;
            cell.anonyImgExtRight.hidden = YES;
            cell.anonyOverlayRight.hidden = YES;
        }
        else{
            cell.anonyImgExtRight.hidden = NO;
            cell.anonyOverlayRight.hidden = NO;
            cell.dummyright1.hidden = NO;
            cell.rightUsername.text = @"Anonymous";
        }
        if(tempVideos.beamType == 1){
            cell.CH_Rheart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
            cell.rightUsername.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        [cell.showCommentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.showCommentsBtn setTag:currentIndex];
        
        currentIndex++;
    }
    else{
        cell.view3.hidden = YES;
        cell.outline3.hidden = YES;
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

#pragma mark Get Comments
-(void) ShowCommentspressed:(UIButton *)sender{
    UIButton *CommentsBtn = (UIButton *)sender;
    currentIndex = CommentsBtn.tag;
    VideoModel *_model = [beamsArray objectAtIndex:currentIndex];
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    commentController.commentsObj   = Nil;
    commentController.postArray     = _model;
    commentController.cPostId       = _model.videoID;;
    commentController.isFirstComment = TRUE;
    commentController.isComment     = FALSE;
    [[self navigationController] pushViewController:commentController animated:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
            // your navigation controller action goes here
    
        });
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma  mark FOOTER VIEW
-(void)initFooterView
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat width = mainScreen.bounds.size.width;
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, 50.0)];
    self.tblView.tableFooterView = footerView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
