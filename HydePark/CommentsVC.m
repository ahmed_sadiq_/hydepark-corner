//
//  CommentsVC.m
//  HydePark
//
//  Created by Apple on 18/02/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CommentsVC.h"
#import "Constants.h"
#import "CommentsCell.h"
#import "NavigationHandler.h"
#import "Utils.h"
#import "UIImageView+RoundImage.h"
#import "NewHomeCells.h"
#import "VideoPlayerVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import "QuartzCore/CALayer.h"
#import <AudioToolbox/AudioServices.h>
#import "AVFoundation/AVFoundation.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "BeamUploadVC.h"
#import "Utils.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "FriendsVC.h"
#import "Followings.h"
#import "KSToastView.h"
#import "CustomLoading.h"
#import "UIImageView+WebCache.h"
#import "CoreDataManager.h"

#import "UserChannel.h"
#import "ApiManager.h"
#import "UIImage+JS.h"
#import "CommentsHeaderCell.h"
#import "PlayerViewController.h"
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <HBRecorder/HBRecorder.h>
#import "PBJVideoPlayerController.h"
#import "DBCommunicator.h"
#import "VideoViewCell.h"
#import "OfflineDataModel.h"
#import "NSData+AES.h"



@interface CommentsVC ()<HBRecorderProtocol,PBJVideoPlayerControllerDelegate>{
    NSMutableDictionary *serviceParams;
    PBJVideoPlayerController *_videoPlayerController;
    float duration;
    UIRefreshControl *refreshControl;
    NSData *playBtnThumbnail;
}

@property (strong, nonatomic) NSURL *videoPath;
@property (nonatomic, assign) BOOL isFullscreenMode;
@property (nonatomic, assign) CGRect originFrame;
@property (strong, nonatomic) NSString *totaltime;
@property (nonatomic, assign) BOOL shouldShowHeader;
@property (strong, nonatomic) NSDateComponentsFormatter *dateComponentsFormatter;
@end

@implementation CommentsVC
@synthesize commentsObj,postArray,isComment,cPostId,isFirstComment,pID,progressView,profileData,CmovieData,CaudioData,isAnonymous;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.backgroundImage.hidden = YES;
            _tabBarBgView.alpha = 0.4;
            _tabBarBgView.backgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            self.backgroundImage.hidden = NO;
            _tabBarBgView.alpha = 1.0;
            _tabBarBgView.backgroundColor = [UIColor whiteColor];
        }
    }
    else
    {
        self.backgroundImage.hidden = NO;
        _tabBarBgView.alpha = 1.0;
        _tabBarBgView.backgroundColor = [UIColor whiteColor];
    }
    
    DataContainer *shareobj = [DataContainer sharedManager];
    NSMutableArray *comments = shareobj.commentsDict[cPostId];
    
    _sharedManager = [DataContainer sharedManager];
    _offlineThumbnailArray = [[NSMutableArray alloc] init];
    
    self.beamLbl.text =  NSLocalizedString(@"beam_small", nil);
    self.audioLbl.text = NSLocalizedString(@"audio", nil);
    self.anonymousLbl.text = NSLocalizedString(@"anonymous_small", nil);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMyCollection) name:@"internetDisconnected" object:nil];

    [commentsCollectionView registerNib:[UINib nibWithNibName:@"VideoViewCell" bundle:nil] forCellWithReuseIdentifier:@"collectionCell"];
    
    // Do any additional setup after loading the view from its nib.
    _playnstopCheck = true;
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [self initFooterView];
    self.commentsTable.tableHeaderView = self.headerView;
    CommentsModelObj = [[CommentsModel alloc]init];
    videoModel = [[VideoModel alloc]init];
    videoObj = [[NSMutableArray alloc] init];
    dataArray = [[NSMutableArray alloc] init];
    
//    if(!appDelegate.hasInet)
    {
//        if(videoModel.videoID != nil)
        {
//            [self setOfflineContent];
        }
    }
    secondsLeft = 60;
    pageNum = 1;
    self.dateComponentsFormatter = [[NSDateComponentsFormatter alloc] init];
    self.dateComponentsFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
    self.dateComponentsFormatter.allowedUnits = (NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond);
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    //[self.view addSubview:activityIndicator];
    if(IS_IPHONE_5){
        anonyLbl.frame = CGRectMake(anonyLbl.frame.origin.x, anonyLbl.frame.origin.y, 80, anonyLbl.frame.size.height);
        _uploadAudioView.frame = CGRectMake(0, 0, 500, 600);
        [self.audioRecordBtn setCenter:_uploadAudioView.center];
        [audioBtnImage setCenter:_uploadAudioView.center];
    }
    else if (IS_IPHONE_6)
    {
        _uploadAudioView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        audioBtnImage.frame = CGRectMake(207- (audioBtnImage.frame.size.width /2 ), [[UIScreen mainScreen] bounds].size.height/2-50, audioBtnImage.frame.size.width, audioBtnImage.frame.size.height);
        _audioRecordBtn.frame = CGRectMake(207 - (_audioRecordBtn.frame.size.width /2 ), [[UIScreen mainScreen] bounds].size.height/2-50, _audioRecordBtn.frame.size.width, _audioRecordBtn.frame.size.height);
        closeBtnAudio.frame = CGRectMake(330, 30, closeBtnAudio.frame.size.width, closeBtnAudio.frame.size.height);
       countDownlabel.frame = CGRectMake(207 - (countDownlabel.frame.size.width /2 ),[[UIScreen mainScreen] bounds].size.height/2 - 100,countDownlabel.frame.size.width,countDownlabel.frame.size.height);
    }
    else if(IS_IPHONE_6Plus){
        _uploadAudioView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        closeBtnAudio.frame = CGRectMake(360, 30, closeBtnAudio.frame.size.width, closeBtnAudio.frame.size.height);
        countDownlabel.frame = CGRectMake(207 - (countDownlabel.frame.size.width /2 ),[[UIScreen mainScreen] bounds].size.height/2 - 100,countDownlabel.frame.size.width,countDownlabel.frame.size.height);
        audioBtnImage.frame = CGRectMake(207- (audioBtnImage.frame.size.width /2 ), [[UIScreen mainScreen] bounds].size.height/2-50, audioBtnImage.frame.size.width, audioBtnImage.frame.size.height);
        _audioRecordBtn.frame = CGRectMake(207 - (_audioRecordBtn.frame.size.width /2 ), [[UIScreen mainScreen] bounds].size.height/2-50, _audioRecordBtn.frame.size.width, _audioRecordBtn.frame.size.height);
    }
    
    
    [self setToast];
    [self initMainView];
    [self setAudioRecordSettings];
    [self addrefreshControl];
    
}

-(void)reloadMyCollection
{
    [self.commentsTable reloadData];
    [commentsCollectionView reloadData];
}

- (NSData *)getVideoFromDocuments:(NSString *)videoURL{
    NSData *video = [NSData dataWithContentsOfFile:videoURL];
    return video;
}

- (NSData *)getThumbnailFromDocuments:(NSString *)thumbnailURL{
    NSData *thumbnail = [NSData dataWithContentsOfFile:thumbnailURL];
    return thumbnail;
}

-(void)addrefreshControl{
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            refreshControl.tintColor = [UIColor grayColor];
        }
        else
        {
            refreshControl.tintColor = [UIColor whiteColor];
        }
    }
    else
    {
        refreshControl.tintColor = [UIColor whiteColor];
    }
    [refreshControl addTarget:self
                       action:@selector(refreshCall)
             forControlEvents:UIControlEventValueChanged];
    [commentsCollectionView addSubview:refreshControl];
    commentsCollectionView.alwaysBounceVertical = YES;
    
}

- (void)refreshCall
{
    pageNum = 1;
    if(appDelegate.hasInet)
    {
        [self getCommentsOnPost];
    }
    else
    {
        [refreshControl endRefreshing];
    }
}


- (NSNumber *)secondsForTimeString:(NSString *)string {
    NSArray *components = [string componentsSeparatedByString:@":"];
    NSInteger seconds;
    NSInteger hours = 0;
    NSInteger minutes;
    
    if (components.count > 2){
        hours   = [[components objectAtIndex:0] integerValue];
        minutes = [[components objectAtIndex:1] integerValue];
        seconds = [[components objectAtIndex:2] integerValue];
    }
    else{
        minutes = [[components objectAtIndex:0] integerValue];
        seconds = [[components objectAtIndex:1] integerValue];
    }
    return [NSNumber numberWithInteger:(hours * 60 * 60) + (minutes * 60) + seconds];
}

#pragma mark DIDLAYOUTSUBVIEWS
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    activityIndicator.frame = CGRectMake(self.commentsTable.center.x, self.commentsTable.center.y, activityIndicator.frame.size.width, activityIndicator.frame.size.height);
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.topBlackView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){18.0, 18.0}].CGPath;
    
    self.topBlackView.layer.mask = maskLayer;
}
-(void)setToast{
    [KSToastView ks_setAppearanceTextFont:[UIFont boldSystemFontOfSize:17.0f]];
    [KSToastView ks_setAppearanceMaxWidth:CGRectGetWidth([UIScreen mainScreen].bounds) - 64.0f];
    [KSToastView ks_setAppearanceOffsetBottom:76.0];
    [KSToastView ks_setAppearanceMaxLines:2];
}

//-(void)completeProgress:(int) currentIndex
//{
//
//
//   // NSDictionary *prog = notification.object;
//
//    //int currentIndex = [prog[@"index"] intValue];
//
//    if (currentIndex == 0){
//
//        currentIndex = 1;
//    }
//
//    CoreDataManager *coreMngr = [CoreDataManager sharedManager];
//    //int index = coreMngr.offlineContentArray.count;
//
//    NSIndexPath *indexPath ;//= [NSIndexPath indexPathForRow:currentIndex inSection:0];
//    indexPath = [NSIndexPath indexPathForItem:currentIndex inSection:0];
//    //  [self performSelector:@selector(removeLoading:) withObject:indexPath afterDelay:1.5];
//
//    //    NSLog(@"Removing cell number : %d",currentIndex);
//
//    VideoViewCell *currentCell = (VideoViewCell*)[commentsCollectionView cellForItemAtIndexPath:indexPath];
//
//    //    NSLog(@"%@", currentCell);
//
//
//
//    //[self.roundedPView setProgress:progres];
//
//    [currentCell.cellProgress removeFromSuperview];
//
//    [self.roundedPView removeFromSuperview];
//}


- (void) updateCommentsTable:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"updateCommentsArray"])
    {
        NSDictionary *tempdata = notification.object;
        
        
//        [self completeProgress:tempindex];
        UIViewController * viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-1];
        
//        if((viewController == self) /*&& !appDelegate.latestVideoAdded */&& self.navigationController.viewControllers.count-1 == appDelegate.navigationControllersCount) {
            appDelegate.latestVideoAdded = true;
            
            if(!dataArray) {
                dataArray = [[NSMutableArray alloc] init];
            }
            //if(appDelegate.commentObj){
            NSDictionary *data = notification.object;
            VideoModel *video = data[@"data"];
            NSNumber *index =  data[@"index"];
            DataContainer *sharedManager = [DataContainer sharedManager];
     //       CoreDataManager *coreMngr = [CoreDataManager sharedManager];
            
            //NSMutableArray *comments = sharedManager.commentsDict[cPostId];
            
            int indexComment = index.intValue;
        if(indexComment < dataArray.count){
            [dataArray replaceObjectAtIndex:indexComment withObject:video];
        }
            DataContainer *sharedInstance = [DataContainer sharedManager];
            
            
            [self.commentsTable reloadData];
            [commentsCollectionView reloadData];
            
            if (indexComment == 0) {
                
                [self getCommentsOnPost];
                
            }
           
            appDelegate.commentObj = nil;
            
            
//        }
    }
    DataContainer *sharedManager = [DataContainer sharedManager];
    sharedManager.isReloadMyCorner = YES;

    NSInteger reply = [self.postArray.comments_count integerValue];
    reply = reply + 1;
    self.postArray.comments_count = [[NSString alloc] initWithFormat:@"%ld",(long)reply];
    
    if([self.postArray.comments_count integerValue] == 1){
        self.hReplies.text    =
        [NSString  stringWithFormat:@"%@ %@", self.postArray.comments_count,NSLocalizedString(@"reply", @"")];
    }else{
        self.hReplies.text    =
        [NSString  stringWithFormat:@"%@ %@",self.postArray.comments_count,NSLocalizedString(@"replies", @"")];
    }
    
    
    if([[notification name] isEqualToString:@"updateeditBeam"])
    {
        UIViewController * viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-1];
        
        if((viewController == self) && self.navigationController.viewControllers.count-1 == appDelegate.navigationControllersCount) {
            postArray =   appDelegate.videObj ;
        }
    }
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[commentsCollectionView reloadData];
    //
//    NSLog(@"STATE %ld", (long)_videoPlayerController.playbackState);
    //    if(_videoPlayerController.playbackState == PBJVideoPlayerPlaybackStatePaused){
    //        [_videoPlayerController playFromBeginning];
    //    }
    //    else if(_videoPlayerController.playbackState == PBJVideoPlayerPlaybackStateStopped){
    //        [_videoPlayerController playFromBeginning];
    //    }
    [self setOfflineContent];
    _mainPlBtn.enabled = TRUE;
    isRecording = false;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
//    NSLog(@"%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"]);
    if([[[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] isEqualToString:postArray.user_id]){
        editButto.hidden = NO;
        editImage.hidden = NO;
        usersPost = TRUE;
    }
    else{
        editButto.hidden = NO;
        editImage.hidden = NO;
        usersPost = FALSE;
    }
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] == postArray.user_id && !isComment){
        editButto.hidden = NO;
        editImage.hidden = NO;
        _flagImg.image = [UIImage imageNamed:@"delettext.png"];
        _blockThispersonlbl.text = @"Delete Beam";
        _reportThisbeamlbl.text  = @"Edit Beam";
        [_report setTitle:@"Edit Beam" forState:UIControlStateNormal];
        [_block  setTitle:@"Delete Beam" forState:UIControlStateNormal];
    }
    if(!isComment){
//        [self SeenPost];
    }
    else{
        [self CommentSeen];
    }
    editImage.hidden = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateCommentsArray" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateCommentsTable:) name:@"updateCommentsArray" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowProgressComments" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showProgressComments:)
                                                 name:@"ShowProgressComments"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProgressBarComments:)
                                                 name:@"updateProgressComments"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateMainHeader" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateHeader:)
                                                 name:@"updateMainHeader" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateeditBeam" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateCommentsTable:)
                                                 name:@"updateeditBeam" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showCommentStaticThumbnail:)
                                                 name:@"updateThumbnailOnComment" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showCommentStaticThumbnailOnUpload:)
                                                 name:@"updateCommentThumbnailOnUpload" object:nil];
    [self.commentsTable reloadData];
    [commentsCollectionView reloadData];
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [_videoPlayerController stop];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_videoPlayerController pause];
    
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Comment" object:nil];
}

-(void)updateHeader:(NSNotification *)notificaiton{
    NSInteger  likeCount = [postArray.like_count intValue];
    if( likeCount == 1)
        likeslbl.text = @"Like";
    else if(likeCount == 0)
        _getLikes.hidden = true;
    likes.text      = postArray.like_count;
    if([postArray.like_by_me isEqualToString:@"1"]){
        [likeBtn setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
    }
    else{
        [likeBtn setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
    }
}

#pragma mark FULL SCREEN CONTROLS
-(IBAction)fullScreenBtnPressed:(id)sender
{
    if (self.isFullscreenMode) {
        [UIView animateWithDuration:0.3f animations:^{
            if([postArray.is_anonymous isEqualToString: @"1"]){
                self.anonyNewImg.hidden = NO;
            }
            _playnstopBtn.enabled = NO;
            _videoPlayerController.view.frame = self.originFrame;
            self.landscapeDiscriptionView.hidden = YES;
            self.isFullscreenMode = NO;
            self.landscapeDiscriptionView.layer.zPosition = 0;
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            _videoPlayerController.videoFillMode = AVLayerVideoGravityResizeAspectFill;
            
        } completion:^(BOOL finished) {
            if(self.shouldShowHeader){
                self.moveToParentHeader.hidden = NO;
            }
            self.potraitDiscriptionView.hidden = NO;
        }];
    }
    else{
        self.anonyNewImg.hidden = YES;
        self.originFrame = _videoPlayerController.view.frame;
        _playnstopBtn.enabled = NO;
        CGFloat width = [[UIScreen mainScreen] bounds].size.width;
        CGFloat height = [[UIScreen mainScreen] bounds].size.height;
        CGRect frame = CGRectMake(0, 0, width, height);
        if(self.postArray.title.length > 0 )
        {
            self.darkDescView.hidden = NO;
        }
        [UIView animateWithDuration:0.3f animations:^{
            _videoPlayerController.videoFillMode = AVLayerVideoGravityResizeAspect;
            _videoPlayerController.view.frame = frame;
            self.potraitDiscriptionView.hidden = YES;
            self.moveToParentHeader.hidden = YES;
        } completion:^(BOOL finished) {
            //in Future
            //            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
            self.isFullscreenMode = YES;
            self.landscapeDiscriptionView.hidden = NO;
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
        }];
    }
}

- (void)orientationChanged:(NSNotification *)notification
{
    //    if ([self needToForceChangeOrientation] == false) {
    //        [self rallbackScreenIfNeeded];
    //        return;
    //    }
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if (orientation == UIDeviceOrientationLandscapeLeft) {
        self.originFrame = _videoPlayerController.view.frame;
        CGFloat height = [[UIScreen mainScreen] bounds].size.width;
        CGFloat width = [[UIScreen mainScreen] bounds].size.height;
        CGRect frame = CGRectMake((height - width) / 2, (width - height) / 2, width, height);
        [UIView animateWithDuration:0.3f animations:^{
            _videoPlayerController.view.frame = frame;
            _videoPlayerController.view.transform =CGAffineTransformMakeRotation(M_PI_2);
            _videoPlayerController.videoFillMode = AVLayerVideoGravityResizeAspect;
        } completion:^(BOOL finished) {
            
            self.isFullscreenMode = YES;
            self.landscapeDiscriptionView.hidden = NO;
        }];
    }
}

-(void)showProgressComments:(NSNotification *) notification{
    [commentsCollectionView reloadData];
    [commentsCollectionView setContentOffset:CGPointZero animated:YES];
    
}

-(void)updateProgressBarComments:(NSNotification*)notification
{
    //    NSDictionary *prog = notification.object;
    //    float progres = [prog[@"progress"] floatValue];
    //    [self.roundedPView setProgress:progres];
    
    NSDictionary *prog = notification.object;
    float progres = [prog[@"progress"] floatValue];
    int indexCurrent = [prog[@"index"] intValue];
    indexCurrent = (indexCurrent == 0) ? 1 : indexCurrent;
    
    
    DataContainer *sharedManager = [DataContainer sharedManager];
    
   // if(sharedManager.commentsDict!=nil){
//
//
//        if (sharedManager.commentsDict[cPostId] != nil){
    
            //NSMutableArray *comments = sharedManager.commentsDict[cPostId];
            
            //NSLog(@"Comments Count is %lu", (unsigned long)comments.count-1);
            
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexCurrent-1 inSection:0];
            
            VideoViewCell *currentCell = (VideoViewCell*)[commentsCollectionView cellForItemAtIndexPath:indexPath];
            
//            NSLog(@"%@", currentCell);
            
            
            
            //[self.roundedPView setProgress:progres];
        NSData *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"commentThumbnail"];
        currentCell.mainThumbnail.hidden = NO;
        currentCell.mainThumbnail.image = [UIImage imageWithData:temp];
    
            [currentCell.cellProgress setProgress:progres];
            
            //currentCell.CH_profileImage.image= [[UIImage alloc]init];
            
//        }else{
//
//            CoreDataManager *coreMngr = [CoreDataManager sharedManager];
//            //int index = coreMngr.offlineContentArray.count;
//
//
//
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:coreMngr.offlineCommentsArray.count inSection:0];
//
//            VideoViewCell *currentCell = (VideoViewCell*)[commentsCollectionView cellForItemAtIndexPath:indexPath];
//
////            NSLog(@"%@", currentCell);
//
//
//
//            //[self.roundedPView setProgress:progres];
//
//            [currentCell.cellProgress setProgress:progres];
//        }
//
//
//    }else{
//
//        CoreDataManager *coreMngr = [CoreDataManager sharedManager];
//        //int index = coreMngr.offlineContentArray.count;
//
//
//
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:coreMngr.offlineCommentsArray.count inSection:0];
//
//        VideoViewCell *currentCell = (VideoViewCell*)[commentsCollectionView cellForItemAtIndexPath:indexPath];
//
////        NSLog(@"%@", currentCell);
//
//
//
//        //[self.roundedPView setProgress:progres];
//
//        [currentCell.cellProgress setProgress:progres];
//    }
    //[progressView uploadDidUpdate:appDelegate.progressFloat];
}

#pragma mark - ThumbnailDuringLoading

-(void)showCommentStaticThumbnail:(NSNotification *)notification {
    NSDictionary *prog = notification.object;
    NSData *thumbnailData = prog[@"data"];
    NSArray *offlineModelArray = [[DBCommunicator sharedManager] retrieveAllRows];
    int currentIndex = [prog[@"index"] intValue];
    for (int i = 0; i < offlineModelArray.count; i++) {
        
        if(_offlineThumbnailArray.count == 0) {
            [_offlineThumbnailArray addObject:thumbnailData];
        }
        else if(i == offlineModelArray.count-1 && i <= currentIndex) {
            NSMutableArray *reverseArray = [[NSMutableArray alloc] init];
            [reverseArray addObject:thumbnailData];
            [reverseArray addObjectsFromArray:_offlineThumbnailArray];
            _offlineThumbnailArray = reverseArray;
        }
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
    
    VideoViewCell *currentCell = (VideoViewCell*)[commentsCollectionView cellForItemAtIndexPath:indexPath];
    currentCell.mainThumbnail.hidden = NO;
    currentCell.mainThumbnail.image = [UIImage imageWithData:thumbnailData];
}

-(void)showCommentStaticThumbnailOnUpload:(NSNotification *)notification {
    
    NSArray *offlineModelArray = [[DBCommunicator sharedManager] retrieveAllRows];
    for (int i = 0; i < offlineModelArray.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        
        VideoViewCell *currentCell = (VideoViewCell*)[commentsCollectionView cellForItemAtIndexPath:indexPath];
        currentCell.mainThumbnail.hidden = NO;
        //currentCell.mainThumbnail.image = [UIImage imageWithData:_offlineThumbnailArray[i]];
        
    }
}

#pragma mark ScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BOOL endOfTable = (scrollView.contentOffset.y >= ((dataArray.count/2 * 180.0f) - scrollView.frame.size.height)); // Here 150 is row height
    
    if (self.isDownwards && !cannotScrollMore && endOfTable && !scrollView.dragging && !scrollView.decelerating )
    {
        
        
    }
}

#pragma  mark FOOTER VIEW
-(void)initFooterView
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat width = mainScreen.bounds.size.width;
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, 50.0)];
    self.commentsTable.tableFooterView = footerView;
}

#pragma mark - WDUploadProgressView Delegate Methods
- (void)uploadDidFinish:(WDUploadProgressView *)progressview {
    [progressview removeFromSuperview];
    [self.commentsTable setTableHeaderView:nil];
}

- (void)uploadDidCancel:(WDUploadProgressView *)progressview {
    [progressview removeFromSuperview];
    [self.commentsTable setTableHeaderView:nil];
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [editView setHidden:YES];
    [_muteAndDelete setHidden:YES];
    [_muteDialog setHidden:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}

-(IBAction)shareBtnPressed:(id)sender{
    [self shareText:postArray.title andImage:nil andUrl:[NSURL URLWithString:postArray.video_link]];
}

-(void)initMainView{
    self.commentsTable.backgroundColor = [UIColor clearColor];
    self.commentsTable.opaque = NO;
    commentsCollectionView.backgroundColor = [UIColor clearColor];
    commentsCollectionView.opaque = NO;
    
    _viewToRound.layer.cornerRadius  = coverimgComments.frame.size.width /14.0f;
    _viewToRound.layer.masksToBounds = YES;
    if(postArray) {
        [self populateMainView];
    }
    else {
        //fetch Post
        [self GetPostbyPostId];
    }
}

//rana
- (void) populateMainView {
    NSString *urlToStream;
    
    if(postArray.beamType == 1){
        _rebeam.hidden = NO;
        _rebeam.image = [UIImage imageNamed:@"rebeam"];
    }
    else if([postArray.isMute isEqualToString:@"1"]){
        _rebeam.hidden = NO;
        _rebeam.image = [UIImage imageNamed:@"speaker-mute.png"];
    }
    else
        _rebeam.hidden = YES;
    if(![postArray.m3u8_video_link isKindOfClass:[NSNull class]] && postArray.m3u8_video_link.length > 1) {
        urlToStream = postArray.m3u8_video_link;
    }
    else {
        urlToStream = postArray.video_link;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,64,414, 280)];
    //    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,65,375, 460)];
    if(IS_IPHONE_6Plus){
        bgView.frame = CGRectMake(0,64,414, 280);
        
    }else if(IS_IPAD)
    {
        bgView.frame = CGRectMake(0,64,768, 480);
        
    }else if(IS_IPHONE_5)
    {
        bgView.frame = CGRectMake(0,64, 320, 280);
        
    }
    _videoPlayerController = [[PBJVideoPlayerController alloc] init];
    _videoPlayerController.delegate = self;
    _videoPlayerController.view.frame = bgView.frame;
    
    if(urlToStream != nil)
    {
        NSURL *bipbopUrl = [[NSURL alloc] initWithString:urlToStream];
        _videoPlayerController.asset = [[AVURLAsset alloc] initWithURL:bipbopUrl options:nil];
        _videoPlayerController.videoFillMode = AVLayerVideoGravityResizeAspectFill;
    }
    //[_videoPlayerController setVolume:1.0];
    //[self.view addSubview:_videoPlayerController.view];
    //[_videoPlayerController playFromBeginning];
    
    //[self.view bringSubviewToFront:self.potraitDiscriptionView];
    //[self.view bringSubviewToFront:self.landscapeDiscriptionView];
    //[self.view bringSubviewToFront:self.moveToParentHeader];
    //[self.view bringSubviewToFront:self.childContainer];
    
    self.pauseBtn.hidden =  YES;
    self.strechBtn.enabled = NO;
    if(![postArray.video_length isEqualToString:@""])
    {
        NSNumber *value =[self secondsForTimeString:postArray.video_length];
        duration = [value floatValue];
        self.totaltime = [self.dateComponentsFormatter stringFromTimeInterval:duration];
    }
    self.timerLabel.text = [NSString stringWithFormat:@"%@ / %@",@"0:00:00",self.totaltime];
    self.landscapeDesc.text = [Utils decodeForEmojis:postArray.title];
    self.landscapeDesc.text = [Utils decodeForEmojis:self.landscapeDesc.text];
    
    [self.thumbnail sd_setImageWithURL:[NSURL URLWithString:self.postArray.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
    self.isThumbnailReq = postArray.isThumbnailReq;
    
    postID = postArray.videoID;
    [appDelegate.parentIdStackController pushObject:postArray.videoID];
    
    if([postArray.like_by_me isEqualToString:@"1"]){
        self.hLikeBTn.image = SET_IMAGE(@"likeblue.png");
    }
    else{
        self.hLikeBTn.image = SET_IMAGE(@"likenew.png");
    }
    NSInteger viewsCount = [postArray.seen_count intValue];
    NSInteger repliesCount = [postArray.comments_count intValue];
    NSInteger  likeCount = [postArray.like_count intValue];
    
    NSString *abbLikesCount = [Utils abbreviateNumber:postArray.like_count withDecimal:1];
    NSString *abbRepliesCount = [Utils abbreviateNumber:postArray.comments_count withDecimal:1];
    NSString *abbViewsCount = [Utils abbreviateNumber:postArray.seen_count withDecimal:1];
    
    
    self.repliesHeader.text = [Utils getDecryptedTextFor:postArray.userName];

    
    if([self.repliesHeader.text isEqualToString:@"Anonymous"]){
        self.repliesHeader.text = NSLocalizedString(@"anonymous_small", nil);
    }
    if(likeCount == 0){
        self.hGetLikesbtn.enabled = NO;
    }else{
        self.hGetLikesbtn.enabled = YES;
    }
    if(postArray.like_count == NULL){
        self.hLikes.text = [NSString  stringWithFormat:@"0 %@",NSLocalizedString(@"likes", @"")];
    }
    else if(likeCount == 1 ){
        self.hLikes.text
        =[NSString  stringWithFormat:@"%@ %@",abbLikesCount,NSLocalizedString(@"likes", @"")];
    }else{
        
        self.hLikes.text
        =[NSString  stringWithFormat:@"%@ %@",abbLikesCount,NSLocalizedString(@"likes", @"")];
    }
    if(repliesCount == 1){
        self.hReplies.text    =
        [NSString  stringWithFormat:@"%@ %@", abbRepliesCount,NSLocalizedString(@"reply", @"")];
    }
    else
    {
        self.hReplies.text    =
        [NSString  stringWithFormat:@"%@ %@",abbRepliesCount,NSLocalizedString(@"replies", @"")];
    }
    if(viewsCount == 1){
        self.hViews.text      =
        [NSString  stringWithFormat:@"%@ %@",abbViewsCount,NSLocalizedString(@"views", @"")];
    }else{
        self.hViews.text      =
        [NSString  stringWithFormat:@"%@ %@",abbViewsCount,NSLocalizedString(@"views", @"")];
    }
    
    if([postArray.isMute isEqualToString:@"0"] && [postArray.is_anonymous isEqualToString: @"1"]){
        self.anonyNewImg.hidden = NO;
        self.repliesHeader.text  = NSLocalizedString(@"anonymous_small", @"");
    }
    else if([postArray.isMute isEqualToString:@"1"] && [postArray.is_anonymous isEqualToString: @"1"])
    {
        self.repliesHeader.text  = NSLocalizedString(@"anonymous_small", @"");
        self.anonyNewImg.hidden = NO;
    }
    else if([postArray.isMute isEqualToString: @"1"] && [postArray.is_anonymous isEqualToString: @"0"]){
        self.anonyNewImg.hidden = YES;
        self.anonyImage.hidden = YES;
    }
    else if([postArray.is_anonymous isEqualToString:@"1"]){
        self.repliesHeader.text  = NSLocalizedString(@"anonymous_small", @"");
    }
    else{
        
        self.anonyNewImg.hidden = YES;
    }
    
    _userChannelBtn.enabled = YES;
    // [UserImg roundImageCorner];
    if(isFirstComment){
        pID = @"-1";
    }
    //    if([postArray.comments_count intValue] > 0)
    [self getCommentsOnPost];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    NSInteger tag = gestureRecognizer.view.tag;
//    CGPoint p = [gestureRecognizer locationInView:self.commentsTable];
//    NSIndexPath *indexPath = [self.commentsTable indexPathForRowAtPoint:p];
    CGPoint p = [gestureRecognizer locationInView:commentsCollectionView];
    NSIndexPath *indexPath = [commentsCollectionView indexPathForItemAtPoint:p];
    if (indexPath == nil) {
        // NSLog(@"long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self presentAll:1];
        currentSelectedIndex = tag;
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        //NSLog(@"ENDED");
    }
}

-(void)handleCommentLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    NSInteger tag = gestureRecognizer.view.tag;
    //    CGPoint p = [gestureRecognizer locationInView:self.commentsTable];
    //    NSIndexPath *indexPath = [self.commentsTable indexPathForRowAtPoint:p];
    CGPoint p = [gestureRecognizer locationInView:commentsCollectionView];
    NSIndexPath *indexPath = [commentsCollectionView indexPathForItemAtPoint:p];
    if (indexPath == nil) {
        // NSLog(@"long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self presentAll:4];
        currentSelectedIndex = tag;
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        //NSLog(@"ENDED");
    }
}
-(void)handleMuteAndDelete:(UILongPressGestureRecognizer *)gestureRecognizer
{
    NSInteger tag = gestureRecognizer.view.tag;
//    CGPoint p = [gestureRecognizer locationInView:self.commentsTable];
//    NSIndexPath *indexPath = [self.commentsTable indexPathForRowAtPoint:p];
    CGPoint p = [gestureRecognizer locationInView:commentsCollectionView];
    NSIndexPath *indexPath = [commentsCollectionView indexPathForItemAtPoint:p];
    if (indexPath == nil) {
        // NSLog(@"long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self presentAll:3];
        currentSelectedIndex = tag;
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        //NSLog(@"ENDED");
    }
}
-(void)handledelete:(UILongPressGestureRecognizer *)gestureRecognizer
{
    NSInteger tag = gestureRecognizer.view.tag;
//    CGPoint p = [gestureRecognizer locationInView:self.commentsTable];
//    NSIndexPath *indexPath = [self.commentsTable indexPathForRowAtPoint:p];
    CGPoint p = [gestureRecognizer locationInView:commentsCollectionView];
    NSIndexPath *indexPath = [commentsCollectionView indexPathForItemAtPoint:p];
    if (indexPath == nil) {
        // NSLog(@"long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        [self presentAll:2];
        currentSelectedIndex = tag;
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        //NSLog(@"ENDED");
        
    }
}

- (IBAction)btnOptionshandleLongPress:(UIButton *)sender {
    currentSelectedIndex = sender.tag;
    [self presentAll:1];
}

- (IBAction)btnOptionshandleCommentLongPress:(UIButton *)sender {
    currentSelectedIndex = sender.tag;
    [self presentAll:4];
}

- (IBAction)btnOptionshandledelete:(UIButton *)sender {
    currentSelectedIndex = sender.tag;
    [self presentAll:2];
}

- (IBAction)btnOptionshandleMuteAndDelete:(UIButton *)sender {
    currentSelectedIndex = sender.tag;
    [self presentAll:3];
}
-(IBAction)getLikesPressed:(id)sender{
    FriendsVC *fVC; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
//    if (IS_IPHONEX){
//        fVC    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        fVC    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    fVC.friendsArray  = nil;
    fVC.titles        = NSLocalizedString(@"likes", @"");
    fVC.NoFriends     = FALSE;
    fVC.loadFollowings= 3;
    fVC.userId        = postID;
    fVC.isComment     = isComment;
    [[self navigationController] pushViewController:fVC animated:YES];
}

-(IBAction)backToMyCorner:(id)sender{
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:@"MoveToMyCorner"
//     object:nil];
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:@"MoveToCornerIndex"
//     object:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"MoveToSpeakerCorner"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"MoveToSpeakerIndex"
     object:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)deleteComment:(BOOL)isMainPost{
    if(!isMainPost){
        VideoModel *tempVideos  = [dataArray objectAtIndex:currentSelectedIndex];
        [dataArray removeObjectAtIndex:currentSelectedIndex];
        [self.commentsTable reloadData];
        [commentsCollectionView reloadData];
        NSInteger commentsCount = [self.postArray.comments_count integerValue];
        commentsCount--;
        self.postArray.comments_count = [NSString stringWithFormat:@"%ld" ,(long)commentsCount];
        NSString *abbRepliesCount = [Utils abbreviateNumber:postArray.comments_count withDecimal:1];
        if(commentsCount == 1){
            self.hReplies.text    =
            [NSString  stringWithFormat:@"%@ %@", abbRepliesCount,NSLocalizedString(@"reply", @"")];
        }else{
            self.hReplies.text    =
            [NSString  stringWithFormat:@"%@ %@",abbRepliesCount,NSLocalizedString(@"replies", @"")];
        }
        [self DeleteCommentRequest:tempVideos.videoID indextodelete:currentSelectedIndex];
    }
    else{
        
    }
}
-(void)MuteComment:(BOOL)isMainPost{
    if(!isMainPost){
        VideoModel *tempVideos  = [dataArray objectAtIndex:currentSelectedIndex];
        tempVideos.isMute = @"1";
        [self MuteCommentRequest:tempVideos.videoID isMainPost:isMainPost];
    }else{
        [self MuteCommentRequest:postID isMainPost:isMainPost];
    }
}
-(void)DeleteCommentRequest:(NSString *)commentId indextodelete:(NSInteger)indextodelete{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"deleteComment",@"method",
                              token,@"session_token",commentId,@"comment_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                Alert  *alert = [[Alert alloc] initWithTitle:message duration:(float)2.0f completion:^{
                    //
                }];
                [alert setShowStatusBar:YES];
                [alert setAlertType:AlertTypeSuccess];
                [alert setIncomingTransition:AlertIncomingTransitionTypeSlideFromTop];
                [alert setOutgoingTransition:AlertOutgoingTransitionTypeSlideToTop];
                [alert setBounces:YES];
                [alert showAlert];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
    
}
-(void)MuteCommentRequest:(NSString *)commentId isMainPost:(BOOL)isMainPost{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"muteComment",@"method",
                              token,@"session_token",commentId,@"comment_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                if(isMainPost)
                {
                    postArray.isMute = @"1";
                }
                Alert  *alert = [[Alert alloc] initWithTitle:message duration:(float)2.0f completion:^{
                    //
                }];
                [alert setShowStatusBar:YES];
                [alert setAlertType:AlertTypeSuccess];
                [alert setIncomingTransition:AlertIncomingTransitionTypeSlideFromTop];
                [alert setOutgoingTransition:AlertOutgoingTransitionTypeSlideToTop];
                [alert setBounces:YES];
                [alert showAlert];
                [self.commentsTable reloadData];
                [commentsCollectionView reloadData];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
}
-(IBAction)fbshareBtn:(id)sender
{
    __block NSString *urlToShare ;
    if(self.isThumbnailReq == 1){
        postArray.isThumbnailReq = 0;
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:postArray.videoID forKey:@"post_id"];
        if(self.isComment)
            [serviceParams setValue:@"1" forKey:@"is_comment"];
        else
            [serviceParams setValue:@"0" forKey:@"is_comment"];
        [Utils downloadImageWithURL:[NSURL URLWithString:postArray.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                }
                                 name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                                     int flag = [[data objectForKey:@"success"] intValue];
                                     if(flag){
                                         urlToShare = (NSString *)[data objectForKey:@"deep_link"];
                                         FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                                         content.contentURL = [NSURL URLWithString:urlToShare];
                                         //content.imageURL   = [NSURL URLWithString:urlToShare];
                                         content.contentTitle = postArray.userName;
                                         content.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
                                         
                                         FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
                                         dialog.fromViewController = self;
                                         dialog.shareContent = content;
                                         dialog.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
                                         if (![dialog canShow]) {
                                             // fallback presentation when there is no FB app
                                             dialog.mode = FBSDKShareDialogModeFeedBrowser;
                                         }
                                         [editView setHidden:YES];
                                         [dialog show];
                                     }
                                 } failure:^(NSError *error) {
                                 }];
            }
        }];
    }else{
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentURL = [NSURL URLWithString:postArray.deep_link];
        //content.imageURL   = [NSURL URLWithString:urlToShare];
        content.contentTitle = postArray.userName;
        content.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
        FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
        dialog.fromViewController = self;
        dialog.shareContent = content;
        dialog.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
        if (![dialog canShow]) {
            // fallback presentation when there is no FB app
            dialog.mode = FBSDKShareDialogModeFeedBrowser;
        }
        [editView setHidden:YES];
        [dialog show];
    }
}
-(IBAction)twitterShareBtn:(id)sender
{
    [editView setHidden:YES];
    NSURL *imageURL = [NSURL URLWithString:postArray.video_thumbnail_link];
    [CustomLoading showAlertMessage];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            UIImage *thumbnailImg = [UIImage imageWithData:imageData];
            [CustomLoading DismissAlertMessage];
            
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                
                SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                [mySLComposerSheet setInitialText:@"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech."];
                [mySLComposerSheet addURL:[NSURL URLWithString:postArray.beam_share_url]];
                [mySLComposerSheet addImage:thumbnailImg];
                [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                    
                    switch (result) {
                        case SLComposeViewControllerResultCancelled:
                            
                            break;
                        case SLComposeViewControllerResultDone:
                            
                            break;
                            
                        default:
                            break;
                    }
                }];
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            else {
                
                NSString *message;
                NSString *title;
                NSString *cancel;
                message = NSLocalizedString(@"setupTwitterAccount", @"");
                title   = NSLocalizedString(@"accountConfiguration", @"");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                               delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        });
    });
    
    
}

-(BOOL)containsAcharacter :(NSString *)str{
    NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"K"];
    NSRange range = [str rangeOfCharacterFromSet:cset];
    if (range.location == NSNotFound) {
        // no ( or ) in the string
        return false;
    } else {
        // ( or ) are present
        return true;
    }
}

//osama
#pragma mark Tableview delegates
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"videoCells";
    static NSString *HeaderIdentifier = @"CommentHeader";
    NewHomeCells *cell ;
    CommentsHeaderCell *chCell;
    currentIndex = ((indexPath.row - 1) * 3);
    if(indexPath.row == 0){
        if(chCell == nil){
            NSArray *topLevelObjects;
            if(IS_IPAD){
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CommentsHeaderCell" owner:self options:nil];
            }else{
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CommentsHeaderCell" owner:self options:nil];
            }
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            chCell = [topLevelObjects objectAtIndex:0];
        }else{
            chCell  = [tableView dequeueReusableCellWithIdentifier:HeaderIdentifier];
        }
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            chCell.hReplies.textColor = [UIColor colorWithRed:62.0/255 green:87.0/255 blue:139.0/255 alpha:1];
            chCell.hLikes.textColor = [UIColor colorWithRed:62.0/255 green:87.0/255 blue:139.0/255 alpha:1];
            chCell.hViews.textColor = [UIColor colorWithRed:62.0/255 green:87.0/255 blue:139.0/255 alpha:1];
            chCell.view1.hidden = false;
            chCell.view2.hidden = false;
            chCell.view3.hidden = false;
        }
        else
        {
            chCell.view1.hidden = true;
            chCell.view2.hidden = true;
            chCell.view3.hidden = true;
        }
        NSInteger viewsCount = [postArray.seen_count intValue];
        NSInteger repliesCount = [postArray.comments_count intValue];
        NSInteger  likeCount = [postArray.like_count intValue];
        
        NSString *abbLikesCount = [Utils abbreviateNumber:postArray.like_count withDecimal:1];
        NSString *abbRepliesCount = [Utils abbreviateNumber:postArray.comments_count withDecimal:1];
        NSString *abbViewsCount = [Utils abbreviateNumber:postArray.seen_count withDecimal:1];
        

        chCell.hUserName.text = [Utils getDecryptedTextFor:postArray.userName];
        
        
        if(likeCount == 0){
            chCell.hGetLikesbtn.enabled = NO;
        }else{
            chCell.hGetLikesbtn.enabled = YES;
        }
        if(postArray.like_count == NULL){
            chCell.hLikes.text = [NSString  stringWithFormat:@"0 %@",NSLocalizedString(@"likes", @"")];
        }
        else if(likeCount == 1 ){
            chCell.hLikes.text
            =[NSString  stringWithFormat:@"%@ %@",abbLikesCount,NSLocalizedString(@"likes", @"")];
        }else{
            
            chCell.hLikes.text
            =[NSString  stringWithFormat:@"%@ %@",abbLikesCount,NSLocalizedString(@"likes", @"")];
        }
        if(repliesCount == 1){
            chCell.hReplies.text    =
            [NSString  stringWithFormat:@"%@ %@", abbRepliesCount,NSLocalizedString(@"reply", @"")];
        }else{
            chCell.hReplies.text    =
            [NSString  stringWithFormat:@"%@ %@",abbRepliesCount,NSLocalizedString(@"replies", @"")];
        }
        if(viewsCount == 1){
            chCell.hViews.text      =
            [NSString  stringWithFormat:@"%@ %@",abbViewsCount,NSLocalizedString(@"views", @"")];
        }else{
            chCell.hViews.text      =
            [NSString  stringWithFormat:@"%@ %@",abbViewsCount,NSLocalizedString(@"views", @"")];
        }
        
        if([postArray.isMute isEqualToString:@"0"] && [postArray.is_anonymous isEqualToString: @"1"]){
            //            chCell.anonyImg.image        = [UIImage imageNamed:@"btanonymous.png"];
            chCell.anonyNewImg.hidden = NO;
            chCell.hUserName.text        = NSLocalizedString(@"anonymous_small", nil);
            chCell.anonyImg.hidden       = NO;
            chCell.muteImg.hidden        = YES;
            chCell.hUserChannelBtn.enabled = NO;
        }
        else if([postArray.isMute isEqualToString:@"1"] && [postArray.is_anonymous isEqualToString: @"1"])
        {
            chCell.anonyImg.hidden = NO;
            chCell.muteImg.hidden = NO;
            chCell.anonyNewImg.hidden = NO;
            chCell.muteImg.image  = [UIImage imageNamed:@"speaker-mute.png"];
            //            chCell.anonyImg.image  = [UIImage imageNamed:@"btanonymous.png"];
            chCell.hUserName.text  = NSLocalizedString(@"anonymous_small", nil);
            chCell.hUserChannelBtn.enabled = NO;
        }
        else if([postArray.isMute isEqualToString: @"1"] && [postArray.is_anonymous isEqualToString: @"0"]){
            chCell.anonyImg.hidden = NO;
            chCell.anonyImg.image  = [UIImage imageNamed:@"speaker-mute.png"];
            chCell.anonyNewImg.hidden = YES;
        }
        else if([postArray.is_anonymous isEqualToString:@"1"]){
            chCell.hUserName.text  = NSLocalizedString(@"anonymous_small", nil);
        }
        else{
            chCell.hUserChannelBtn.enabled = YES;
            chCell.anonyImg.hidden = YES;
            chCell.muteImg.hidden = YES;
            chCell.anonyNewImg.hidden = YES;
        }
        
        if(postArray.uploaded_date.length > 0){
            chCell.hTimeStamp.text = [Utils returnDate:postArray.uploaded_date];
        }else{
            chCell.hTimeStamp.text = @"";
        }
        
        if(postArray.beamType == 1){
            chCell.rebeamedImage.hidden = NO;
        }
        else if(postArray.beamType == 2){
            NSString *sharedby =  [postArray.originalBeamData objectForKey:@"full_name"];
            
            NSString *decodedString = [Utils getDecryptedTextFor:sharedby];
            
            NSString *decodedString2 = [Utils getDecryptedTextFor:postArray.userName];
            
            chCell.hUserName.text = [NSString stringWithFormat:@"%@ %@ with %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:decodedString],[Utils getTrimmedString:decodedString2]];
            chCell.rebeamedImage.hidden = YES;
        }
        else{
            chCell.rebeamedImage.hidden = YES;
        }
        chCell.hVideoLength.text = postArray.video_length;
        chCell.hTitle.text = [Utils decodeForEmojis:postArray.title];
        chCell.hTitle.text = [Utils decodeForEmojis:chCell.hTitle.text];
//        [chCell.hThumbnail sd_setImageWithURL:[NSURL URLWithString:postArray.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"pH"]];
        [chCell.hThumbnail sd_setImageWithURL:[NSURL URLWithString:postArray.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"pH"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if(postArray.video_thumbnail_gif != nil && [postArray.video_thumbnail_gif length]>0) {
//                [chCell.CH_video_thumbnail pin_setImageFromURL:[NSURL URLWithString:postArray.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
//                    postArray.animatedImage = result.animatedImage;
//                    [chCell.CH_video_thumbnail setAnimatedImage:postArray.animatedImage];
//                    chCell.hThumbnail.hidden = NO;
//                    chCell.CH_video_thumbnail.hidden = NO;
//                }];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    [chCell.CH_video_thumbnail sd_setImageWithURL:[NSURL URLWithString:postArray.video_thumbnail_gif] placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        dispatch_async(dispatch_get_main_queue(), ^{
            
                            if(chCell.CH_video_thumbnail.animatedImage == nil)
                            {
                                chCell.hThumbnail.hidden = NO;
                                chCell.CH_video_thumbnail.hidden = YES;
                                [chCell.CH_video_thumbnail stopAnimating];
                            }
                            else
                            {
                                chCell.hThumbnail.hidden = YES;
                                chCell.CH_video_thumbnail.hidden = NO;
                                [chCell.CH_video_thumbnail startAnimating];
                            }
                        });
                        
                    }];
                    
                });
            }
        }];
        if([postArray.like_by_me isEqualToString:@"1"]){
            [chCell.hLikeBTn setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
        }
        else{
            [chCell.hLikeBTn setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
        }
        [chCell.hLikeBTn addTarget:self action:@selector(likeComment:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.hEditBtn addTarget:self action:@selector(editBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.hPlayBtn addTarget:self action:@selector(MainPlayBtn:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.hGetLikesbtn addTarget:self action:@selector(getLikesPressed:) forControlEvents:UIControlEventTouchUpInside];
        [chCell.hUserChannelBtn addTarget:self action:@selector(btnUserChannel:) forControlEvents:UIControlEventTouchUpInside];
        
        [chCell setBackgroundColor:[UIColor clearColor]];
        chCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return chCell;
    }else{
        if (cell == nil) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects;
            if(IS_IPAD){
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells_iPad" owner:self options:nil];
            }else if(IS_IPHONE_5){
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
            }else{
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
            }
            cell = [topLevelObjects objectAtIndex:0];
            cell.leftreplImg.hidden =  NO;
            cell.rightreplImg.hidden = NO;
        }else{
            cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        }
        
        float tableheight  = 0;
        if(dataArray.count % 2 == 0)
            tableheight = dataArray.count/1.8;
        else
            tableheight = dataArray.count/1.3;
        VideoModel *tempVideos  = [dataArray objectAtIndex:currentIndex];
        if(tempVideos.beamType == 1){
            cell.rebeam1.hidden = NO;
        }
        else{
            cell.rebeam1.hidden = YES;
        }
        
        cell.CH_userName.text = tempVideos.userName;
        cell.Ch_videoLength.text = tempVideos.video_length;
        cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
        if(tempVideos.isLocal){
            cell.leftreplImg.hidden =  YES;
            cell.CH_VideoTitle.text = tempVideos.current_datetime;
            cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
            if(IS_IPHONE_6Plus){
                self.roundedPView = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x , cell.view1.frame.origin.y, cell.view1.frame.size.width + 11 , cell.view1.frame.size.height  + 12)];
            }
            else if(IS_IPHONE_5){
                self.roundedPView = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x , cell.view1.frame.origin.y, 95 , 90)];
            }
            else{
                self.roundedPView = [[PWProgressView alloc] initWithFrame:cell.view1.frame];
            }
            self.roundedPView.layer.cornerRadius = 20.0f;
            self.roundedPView.clipsToBounds = YES;
            [cell.contentView addSubview:self.roundedPView];
        }
        else{
            [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"loading"]];
        }
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.CH_CommentscountLbl.hidden = YES;
            cell.leftreplImg.hidden = YES;
        }
        else{
            cell.CH_CommentscountLbl.text = tempVideos.comments_count;
        }
        cell.CH_heartCountlbl.text = tempVideos.like_count;
        cell.CH_seen.text = tempVideos.seen_count;
        
        if([tempVideos.isMute isEqualToString:@"0"] && [tempVideos.is_anonymous isEqualToString: @"1"]){
            //            cell.dummyLeft1.image        = [UIImage imageNamed:@"btanonymous.png"];
            cell.annonyOverlayLeft.hidden = NO;
            cell.annonyImgLeft.hidden = NO;
            cell.CH_userName.text        = NSLocalizedString(@"anonymous_small", nil);;
            cell.userProfileView.enabled = false;
            cell.dummyLeft1.hidden       = NO;
        }
        else if([tempVideos.isMute isEqualToString:@"1"] && [tempVideos.is_anonymous isEqualToString: @"1"])
        {
            cell.dummyLeft1.hidden = NO;
            cell.dummyLeft2.hidden = NO;
            cell.dummyLeft2.image  = [UIImage imageNamed:@"speaker-mute.png"];
            //            cell.dummyLeft1.image  = [UIImage imageNamed:@"btanonymous.png"];
            cell.annonyOverlayLeft.hidden = NO;
            cell.annonyImgLeft.hidden = NO;
            cell.CH_userName.text  = NSLocalizedString(@"anonymous_small", nil);;
            cell.userProfileView.enabled = false;
        }
        else if([tempVideos.isMute isEqualToString: @"1"] && [tempVideos.is_anonymous isEqualToString: @"0"]){
            cell.dummyLeft1.hidden = NO;
            cell.dummyLeft1.image  = [UIImage imageNamed:@"speaker-mute.png"];
        }
        else{
            cell.annonyOverlayLeft.hidden = YES;
            cell.annonyImgLeft.hidden = YES;
            cell.dummyLeft2.hidden = YES;
            cell.dummyLeft1.hidden = YES;
        }
        
        if(IS_IPAD)
            cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /7.4f;
        cell.imgContainer.layer.masksToBounds = YES;
        [cell.CH_Video_Thumbnail roundCorners];
        
        [cell.CH_playVideo addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        cell.userProfileView.tag = currentIndex;
        [cell.CH_heart setTag:currentIndex];
        [cell.CH_heart addTarget:self action:@selector(LikeHearts:) forControlEvents:UIControlEventTouchUpInside];
        if ([tempVideos.like_by_me isEqualToString:@"1"]) {
            [cell.CH_heart setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
        }else{
            [cell.CH_heart setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
        }
        
        if (usersPost && [[[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] isEqualToString:tempVideos.user_id])
        {
            UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                  initWithTarget:self action:@selector(handleMuteAndDelete:)];
            tempVideos.canDeleteComment = 1;
            lpgr.minimumPressDuration = 1.0;
            [cell.CH_commentsBtn addGestureRecognizer:lpgr];
            [lpgr.view setTag:currentIndex];
        }
        else if(usersPost && ![[[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] isEqualToString:tempVideos.user_id ]){
            UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                  initWithTarget:self action:@selector(handleLongPress:)];
            tempVideos.canDeleteComment = 2;
            lpgr.minimumPressDuration = 1.0;
            [cell.CH_commentsBtn addGestureRecognizer:lpgr];
            [lpgr.view setTag:currentIndex];
        }
        else if (!usersPost  && [[[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] isEqualToString:tempVideos.user_id])
        {
            UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                  initWithTarget:self action:@selector(handledelete:)];
            tempVideos.canDeleteComment = 3;
            lpgr.minimumPressDuration = 1.0;
            [cell.CH_commentsBtn addGestureRecognizer:lpgr];
            [lpgr.view setTag:currentIndex];
        }
        
        [cell.CH_playVideo setTag:currentIndex];
        
        [cell.CH_flag setTag:currentIndex];
        cell.CH_commentsBtn.enabled = YES;
        cell.CH_RcommentsBtn.enabled = YES;
        [cell.CH_commentsBtn addTarget:self action:@selector(ReplyCommentpressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CH_commentsBtn setTag:currentIndex];
        
        currentIndex++;
        if(currentIndex < dataArray.count)
        {
            VideoModel *tempVideos  = [dataArray objectAtIndex:currentIndex];
            if(tempVideos.beamType == 1){
                cell.rebeam2.hidden = NO;
            }
            else{
                cell.rebeam2.hidden = YES;
            }
            [cell.CH_RcommentsBtn addTarget:self action:@selector(ReplyCommentpressed:) forControlEvents:UIControlEventTouchUpInside];
            cell.CH_RuserName.text = tempVideos.userName;
            [cell.CH_RcommentsBtn setTag:currentIndex];
            [cell.CH_RplayVideo addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CH_RplayVideo setTag:currentIndex];
            [cell.CH_Rheart setTag:currentIndex];
            [cell.CH_Rheart addTarget:self action:@selector(LikeHearts:) forControlEvents:UIControlEventTouchUpInside];
            cell.CH_RVideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
            cell.CH_Rseen.text = tempVideos.seen_count;
            cell.RimgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /6.2f;
            cell.rightTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
            if([tempVideos.isMute isEqualToString:@"0"] && [tempVideos.is_anonymous isEqualToString: @"1"]){
                //                cell.dummyright1.image        = [UIImage imageNamed:@"btanonymous.png"];
                cell.anonyImgRight.hidden = NO;
                cell.anonyOverlayRight.hidden = NO;
                cell.CH_RuserName.text        = NSLocalizedString(@"anonymous_small", nil);
                cell.userProfileView.enabled = false;
                cell.dummyright1.hidden       = NO;
            }
            else if([tempVideos.isMute isEqualToString:@"1"] && [tempVideos.is_anonymous isEqualToString: @"1"])
            {
                cell.dummyright1.hidden = NO;
                cell.dummyright2.hidden = NO;
                cell.dummyright2.image  = [UIImage imageNamed:@"speaker-mute.png"];
                //                cell.dummyright1.image  = [UIImage imageNamed:@"btanonymous.png"];
                cell.anonyImgRight.hidden = NO;
                cell.anonyOverlayRight.hidden = NO;
                cell.CH_RuserName.text  = NSLocalizedString(@"anonymous_small", nil);
                cell.userProfileView.enabled = false;
            }
            else if([tempVideos.isMute isEqualToString: @"1"] && [tempVideos.is_anonymous isEqualToString: @"0"]){
                cell.dummyright1.hidden = NO;
                cell.dummyright1.image  = [UIImage imageNamed:@"speaker-mute.png"];
            }
            else{
                cell.anonyImgRight.hidden = YES;
                cell.anonyOverlayRight.hidden = YES;
                cell.dummyright1.hidden = YES;
                cell.dummyright2.hidden = YES;
            }
            if(IS_IPAD)
                cell.RimgContainer.layer.cornerRadius  = cell.RimgContainer.frame.size.width /7.4f;
            cell.RimgContainer.layer.masksToBounds = YES;
            [cell.CH_RVideo_Thumbnail roundCorners];
            cell.CH_RheartCountlbl.text             = tempVideos.like_count;
            if([tempVideos.comments_count isEqualToString:@"0"])
            {
                cell.CH_RCommentscountLbl.hidden = YES;
                cell.rightreplImg.hidden = YES;
            }
            else{
                cell.CH_RCommentscountLbl.text = tempVideos.comments_count;
            }
            [cell.CH_RVideo_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"loading"]];
            
            if ([tempVideos.like_by_me isEqualToString:@"1"]) {
                [cell.CH_Rheart setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
            }else{
                [cell.CH_Rheart setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
            }
            if (usersPost && [[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] == tempVideos.user_id)
            {
                UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(handleMuteAndDelete:)];
                tempVideos.canDeleteComment = 1;
                lpgr.minimumPressDuration = 1.0;
                [cell.CH_RcommentsBtn addGestureRecognizer:lpgr];
                [lpgr.view setTag:currentIndex];
            }
            else if(usersPost && [[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] != tempVideos.user_id){
                UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(handleLongPress:)];
                tempVideos.canDeleteComment = 2;
                lpgr.minimumPressDuration = 1.0;
                [cell.CH_RcommentsBtn addGestureRecognizer:lpgr];
                [lpgr.view setTag:currentIndex];
            }
            else if(!usersPost  && [[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] == tempVideos.user_id)
            {
                UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(handledelete:)];
                tempVideos.canDeleteComment = 3;
                lpgr.minimumPressDuration = 1.0;
                [cell.CH_RcommentsBtn addGestureRecognizer:lpgr];
                [lpgr.view setTag:currentIndex];
            }
            
            currentIndex++;
        }
        else{
            cell.rightClock.hidden    = YES;
            cell.rightTimeStamp.hidden = YES;
            cell.CH_RprofileImage.hidden = YES;
            cell.CH_Rseen.hidden = YES;
            cell.CH_RcommentsBtn.hidden = YES;
            cell.CH_RuserName.hidden = YES;
            cell.CH_Rheart.hidden = YES;
            cell.RimgContainer.hidden = YES;
            cell.CH_RplayVideo.hidden = YES;
            cell.Rtransthumb.hidden = YES;
            cell.CH_RVideoTitle.hidden = YES;
            cell.rightreplImg.hidden = YES;
            cell.CH_RCommentscountLbl.hidden = YES;
            cell.playImage.hidden           = YES;
        }
        if(currentIndex < dataArray.count){
            VideoModel *tempVideos = [[VideoModel alloc]init];
            tempVideos  = [dataArray objectAtIndex:currentIndex];
            if(tempVideos.beamType == 1){
                cell.rebeam1.hidden = NO;
            }
            else{
                cell.rebeam1.hidden = YES;
            }
            cell.rightDate.text = [Utils returnDate:tempVideos.uploaded_date];
            cell.rightUsername.text = tempVideos.userName;
            [cell.rightThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
            [cell.rightThumbnail roundCorners];
            [cell.showCommentsBtn addTarget:self action:@selector(ReplyCommentpressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.showCommentsBtn setTag:currentIndex];
            if([tempVideos.comments_count isEqualToString:@"0"])
            {
                cell.replyRedBg.hidden = YES;
                cell.replyCountlbl.hidden = YES;
            }
            else{
                cell.replyRedBg.hidden = NO;
                cell.replyCountlbl.hidden = NO;
                cell.replyCountlbl.text = tempVideos.comments_count;
            }
            if([tempVideos.is_anonymous  isEqualToString: @"0"]){
                cell.dummyright1.hidden = YES;
                cell.anonyImgExtRight.hidden = YES;
                cell.anonyOverlayRight.hidden = YES;
            }
            else{
                cell.anonyImgExtRight.hidden = NO;
                cell.anonyOverlayRight.hidden = NO;
                cell.dummyright1.hidden = NO;
                cell.rightUsername.text = NSLocalizedString(@"anonymous", nil);
            }
            
            if([tempVideos.isMute isEqualToString:@"0"] && [tempVideos.is_anonymous isEqualToString: @"1"]){
                //            cell.dummyLeft1.image        = [UIImage imageNamed:@"btanonymous.png"];
                
                cell.dummy1.hidden       = NO;
            }
            else if([tempVideos.isMute isEqualToString:@"1"] && [tempVideos.is_anonymous isEqualToString: @"1"])
            {
                cell.dummy1.hidden = NO;
                cell.dummy2.hidden = NO;
                cell.dummy2.image  = [UIImage imageNamed:@"speaker-mute.png"];
                //            cell.dummyLeft1.image  = [UIImage imageNamed:@"btanonymous.png"];
                
            }
            else if([tempVideos.isMute isEqualToString: @"1"] && [tempVideos.is_anonymous isEqualToString: @"0"]){
                cell.dummy1.hidden = NO;
                cell.dummy1.image  = [UIImage imageNamed:@"speaker-mute.png"];
            }
            else{
                cell.dummy2.hidden = YES;
                cell.dummy1.hidden = YES;
            }
            
            
            if(tempVideos.beamType == 1){
                cell.CH_Rheart.hidden = NO;
            }
            else if(tempVideos.beamType == 2){
                NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
                cell.rightUsername.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            }
            if (usersPost && [[[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] isEqualToString:tempVideos.user_id])
            {
                UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(handleMuteAndDelete:)];
                tempVideos.canDeleteComment = 1;
                lpgr.minimumPressDuration = 1.0;
                [cell.CH_commentsBtn addGestureRecognizer:lpgr];
                [lpgr.view setTag:currentIndex];
            }
            else if(usersPost && ![[[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] isEqualToString:tempVideos.user_id ]){
                UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(handleLongPress:)];
                tempVideos.canDeleteComment = 2;
                lpgr.minimumPressDuration = 1.0;
                [cell.CH_commentsBtn addGestureRecognizer:lpgr];
                [lpgr.view setTag:currentIndex];
            }
            else if (!usersPost  && [[[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] isEqualToString:tempVideos.user_id])
            {
                UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(handledelete:)];
                tempVideos.canDeleteComment = 3;
                lpgr.minimumPressDuration = 1.0;
                [cell.CH_commentsBtn addGestureRecognizer:lpgr];
                [lpgr.view setTag:currentIndex];
            }
            
            currentIndex++;
        }
        else{
            cell.view3.hidden = YES;
        }
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        if (IS_IPAD){
            return 540;
        }
        tableHeight = 340;
    }
    else{
        if(IS_IPHONE_5)
            tableHeight = 110.0f;
        else if (IS_IPAD)
            tableHeight = 270.0f;
        else if(IS_IPHONE_6Plus){
            tableHeight = 145.0;
        }
        else
            tableHeight = 130.0f;
    }
    return tableHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    int rows = (int)([dataArray count]/3);
    //    if([dataArray count] % 3 == 1 || [dataArray count] % 3 == 2) {
    //        rows++;
    //    }
    //    return rows + 1;
    
    return 1;
}

-(void)playVideoComments:(UIButton*)sender{
    UIButton *playBtn = (UIButton *)sender;
    currentSelectedIndex = playBtn.tag;
    VideoModel *tempVideos = [dataArray objectAtIndex:currentSelectedIndex];
    appDelegate.videotoPlay = tempVideos.video_link;
    appDelegate.videoUploader = tempVideos.userName;
    appDelegate.videotitle = tempVideos.title;
    appDelegate.videotags = tempVideos.title;
    appDelegate.profile_pic_url = tempVideos.profile_image;
    //appDelegate.currentScreen = screen;
    postID = tempVideos.videoID;
    [self SeenPost];
    [[NavigationHandler getInstance]MoveToPlayer];
}

-(void) ReplyCommentpressed:(UIButton *)sender{
    UIButton *CommentsBtn = (UIButton *)sender;
    CommentsBtn.enabled = false;
    currentSelectedIndex = CommentsBtn.tag;
    videoModel  = [dataArray objectAtIndex:currentSelectedIndex];
    ParentCommentID = videoModel.videoID;
    isFirstComment                  = FALSE;
    [self GetReplyOnPost];
}

#pragma mark - PBJVideoPlayerControllerDelegate
- (void)videoPlayerReady:(PBJVideoPlayerController *)videoPlayer
{
    //    NSLog(@"Max duration of the video: %f", videoPlayer.maxDuration);
}

- (void)videoPlayerPlaybackStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
    switch (videoPlayer.playbackState) {
        case PBJVideoPlayerPlaybackStateStopped:
            self.strechBtn.enabled = NO;
            break;
        case PBJVideoPlayerPlaybackStatePlaying:
            self.strechBtn.enabled = YES;
            self.pauseBtn.hidden = YES;
            self.playnstopCheck = true;
            break;
        case PBJVideoPlayerPlaybackStatePaused:
            self.strechBtn.enabled = NO;
            self.pauseBtn.hidden = NO;
            self.playnstopCheck = false;
            break;
        default:
            break;
    }
}

- (void)videoPlayerPlaybackWillStartFromBeginning:(PBJVideoPlayerController *)videoPlayer
{
    //    NSLog(@"PLAYBACK videoPlayerPlaybackWillStartFromBeginning");
}

- (void)videoPlayerPlaybackDidEnd:(PBJVideoPlayerController *)videoPlayer
{
    //    NSLog(@"PLAYBACK DID END ");
    if (self.isFullscreenMode) {
        [UIView animateWithDuration:0.3f animations:^{
            _videoPlayerController.view.frame = self.originFrame;
            self.landscapeDiscriptionView.hidden = YES;
            self.isFullscreenMode = NO;
            self.landscapeDiscriptionView.layer.zPosition = 0;
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            _videoPlayerController.videoFillMode = AVLayerVideoGravityResizeAspectFill;
        } completion:^(BOOL finished) {
            if(self.shouldShowHeader){
                self.moveToParentHeader.hidden = NO;
            }
            self.potraitDiscriptionView.hidden = NO;
        }];
    }
    [_videoPlayerController playFromBeginning];
}

- (void)videoPlayerBufferringStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
    switch (videoPlayer.bufferingState) {
        case PBJVideoPlayerBufferingStateUnknown:
            //NSLog(@"Buffering state unknown!");
            [self.indicatorView startAnimating];
            self.playnstopBtn.enabled = NO;
            self.strechBtn.enabled = NO;
            break;
            
        case PBJVideoPlayerBufferingStateReady:
            [self.indicatorView stopAnimating];
            self.strechBtn.enabled = NO;
            self.playnstopBtn.enabled = YES;
            //NSLog(@"Buffering state Ready! Video will start/ready playing now.");
            break;
            
        case PBJVideoPlayerBufferingStateDelayed:
            self.strechBtn.enabled = NO;
            [self.indicatorView startAnimating];
            // NSLog(@"Buffering state Delayed! Video will pause/stop playing now.");
            break;
        default:
            break;
    }
}

- (void)videoPlayer:(PBJVideoPlayerController *)videoPlayer didUpdatePlayBackProgress:(CGFloat)progress {
//    NSLog(@"Completed of the video: %f", progress);
    //    self.progressView.progress = progress/duration;
    self.timerLabel.text = [NSString stringWithFormat:@"%@ / %@",[self.dateComponentsFormatter stringFromTimeInterval:progress],self.totaltime];
}

- (CMTime)videoPlayerTimeIntervalForPlaybackProgress:(PBJVideoPlayerController *)videoPlayer {
    return CMTimeMake(1, 20);
}

#pragma mark end
-(void) getCommentsOnPost{
    if(pID != nil)
    {
    serverCall = true;
    exist = NO;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",pageNum];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_COMMENTS_BY_PARENT_ID,@"method",token,@"Session_token",pageStr,@"page_no",pID,@"parent_id",cPostId,@"post_id", nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [activityIndicator stopAnimating];
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            int success = [[result objectForKey:@"success"] intValue];
            [refreshControl endRefreshing];
            if(success == 0) {
                NSString *message = [result objectForKey:@"message"];
                UIAlertController * view=   [UIAlertController
                                             alertControllerWithTitle:NSLocalizedString(@"app_name", nil)
                                             message:message
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"ok", nil)
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self.navigationController popViewControllerAnimated:YES];
                                     }];
              
                
                
                [view addAction:ok];
                                [self presentViewController:view animated:YES completion:nil];
                
            }
            if(success == 1) {
                NSDictionary *mainPostData =[result objectForKey:@"post"];
                if([mainPostData objectForKey:@"parent_id"]){
                    self.parentModel  = [[VideoModel alloc] initWithDictionary:mainPostData];
                    self.shouldShowHeader = YES;
                    self.parentRootId = [mainPostData objectForKey:@"parent_id"];
                    NSString *parentBeamer = [mainPostData objectForKey:@"parent_user_name"];
                    
                    parentBeamer = [Utils getDecryptedTextFor:parentBeamer];
                    
                    
                    if(parentBeamer.length > 7){
                        parentBeamer = [Utils getTrimmedString:parentBeamer];
                    }
                    NSString *childBeamer = [self.postArray.userName mutableCopy];
                
                    childBeamer = [Utils getDecryptedTextFor:childBeamer];
                    
                    
                    if(childBeamer.length > 7){
                        childBeamer = [Utils getTrimmedString:childBeamer];
                    }
                    if([postArray.is_anonymous isEqualToString:@"1"]){
                        childBeamer =  NSLocalizedString(@"anonymous_small", @"");
                    }
                    self.replyToSomeonelbl.text = [NSString stringWithFormat:@"%@ %@ %@",childBeamer,NSLocalizedString(@"Reply to", nil),parentBeamer];
                    self.gotoParentPostBtn.enabled = YES;
                    self.moveToParentHeader.hidden = NO;
                }
                //////Comments Videos Response //////
                
                
                CommentsArray = [result objectForKey:@"comments"];
                
//                [self.commentsTable reloadData];
//                [commentsCollectionView reloadData];
                
                
                
                CommentsModelObj.CommentsArray = [[NSMutableArray alloc] init];
                CommentsModelObj.mainArray = [[NSMutableArray alloc]init];
                CommentsModelObj.ImagesArray = [[NSMutableArray alloc]init];
                CommentsModelObj.ThumbnailsArray = [[NSMutableArray alloc]init];
                
                
                
                /////////////////////////////MERGING OFFLINE BEAMS IN ONLINE ARRAY
                
//                dataArray = [[NSMutableArray alloc] init];
                NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
//                //[dataArray addObjectsFromArray:offlineArray];
//                for (int i = 0; i < offlineArray.count; i++)
//                {
//                    OfflineDataModel *offlineVideoModel = offlineArray[i];
//                    VideoModel *_Videos     = [[VideoModel alloc] init];
//
//                    _Videos.video_thumbnail_link = offlineVideoModel.thumbnail;
//                    _Videos.video_link = offlineVideoModel.data;
//                    _Videos.is_anonymous = offlineVideoModel.anonyCheck;
//                    _Videos.uploaded_date = offlineVideoModel.currentTime;
//                    _Videos.comments_count = @"0";
//                    _Videos.current_datetime = offlineVideoModel.currentTime;
//                    // _Videos.uploaded_date = offlineVideoModel.
//                    [dataArray addObject:_Videos];
//
//                }
                
                
                //Change By FA
                if(offlineArray.count == 0 && [pageStr  isEqual: @"1"])
                {
                    dataArray = [[NSMutableArray alloc] init];
                } 
                

                //END
                
                if(CommentsArray.count > 0){
                    //Comment BY FA
                 // if(pageNum == 1)
                    //END
                    for(NSDictionary *tempDict in CommentsArray){
                        
                        VideoModel *_Videos     = [[VideoModel alloc] init];
                        _Videos.title           = [tempDict objectForKey:@"caption"];
                        _Videos.comments_count  = [tempDict objectForKey:@"comment_count"];
                        _Videos.userName        = [tempDict objectForKey:@"full_name"];
                        _Videos.topic_id        = [tempDict objectForKey:@"topic_id"];
                        _Videos.user_id         = [tempDict objectForKey:@"user_id"];
                        _Videos.profile_image   = [tempDict objectForKey:@"profile_link"];
                        _Videos.like_count      = [tempDict objectForKey:@"comment_like_count"];
                        _Videos.like_by_me      = [tempDict objectForKey:@"liked_by_me"];
                        _Videos.seen_count      = [tempDict objectForKey:@"seen_count"];
                        _Videos.video_angle     = [[tempDict objectForKey:@"video_angle"] intValue];
                        _Videos.beam_share_url  = [tempDict objectForKey:@"beam_share_url"];
                        _Videos.deep_link       = [tempDict objectForKey:@"deep_link"];
                        _Videos.isThumbnailReq = [[tempDict objectForKey:@"is_thumbnail_required"] intValue];
                        _Videos.video_link      = [tempDict objectForKey:@"video_link"];
                        _Videos.m3u8_video_link = [tempDict objectForKey:@"m3u8_video_link"];
                        _Videos.video_thumbnail_link = [tempDict objectForKey:@"video_thumbnail_link"];
                        _Videos.video_thumbnail_gif = [tempDict objectForKey:@"video_thumbnail_gif"];
                        _Videos.videoID         = [tempDict objectForKey:@"id"];
                        _Videos.Tags            = [tempDict objectForKey:@"tag_friends"];
                        _Videos.video_length    = [tempDict objectForKey:@"video_length"];
                        _Videos.is_anonymous    = [tempDict objectForKey:@"is_anonymous"];
                        _Videos.reply_count     = [tempDict objectForKey:@"reply_count"];
                        _Videos.current_datetime= [tempDict objectForKey:@"current_datetime"];
                        _Videos.uploaded_date   = [tempDict objectForKey:@"uploaded_date"];
                        _Videos.isMute          = [tempDict objectForKey:@"mute"];
                        _Videos.privacy         = [tempDict objectForKey:@"privacy"];
                        _Videos.isCelebrity = [tempDict objectForKey:@"is_celeb"];
                       [dataArray addObject:_Videos];
                        
                        
//                        for (int i = 0; i < dataArray.count; i++)
//                        {
//                            VideoModel *videoModel = dataArray[i];
//                            NSString *date1 = [Utils returnDate:videoModel.uploaded_date];
//
//                            NSString *date2 = [Utils returnDate:_Videos.uploaded_date];
//                            if ([date1 isEqualToString:date2]){
//                                exist = YES;
//                                break;
//                            }
//                            else{
//                                exist = NO;
//                            }
//                        }
//
//                        if(!exist){
//                             [dataArray addObject:_Videos];
//                        }
                        
                        
                    }
                    
                    
                    
                   
                    
                    for (int i = 0; i < offlineArray.count; i++)
                    {
                        OfflineDataModel *offlineVideoModel = offlineArray[i];
                        if ([offlineVideoModel.isComment isEqualToString:@"0"]){
                            [offlineArray removeObjectAtIndex:i];
                            i--;
                        }
                    }
                    
                    //Updateing Comment Counter (Change BY FA)
//                    NSInteger reply = CommentsArray.count+offlineArray.count;
//                    self.postArray.comments_count = [[NSString alloc] initWithFormat:@"%ld",(long)reply];
//                    if([self.postArray.comments_count integerValue] == 1){
//                        self.hReplies.text    =
//                        [NSString  stringWithFormat:@"%@ %@", self.postArray.comments_count,NSLocalizedString(@"reply", @"")];
//                    }else{
//                        self.hReplies.text    =
//                        [NSString  stringWithFormat:@"%@ %@",self.postArray.comments_count,NSLocalizedString(@"replies", @"")];
//                    }
                    
                    //END
//
                 
                    
                    serverCall = false;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.commentsTable reloadData];
                        [commentsCollectionView reloadData];
                    });
                }
                else{
                    cannotScrollMore = TRUE;
                }
            }
        }
        else{
            [activityIndicator stopAnimating];
        }
    }];
    }
}


-(void) GetPostbyPostId{
    // [activityIndicator startAnimating];
    serverCall = true;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",pageNum];
    
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_COMMENTS_BY_PARENT_ID,@"method",
                              token,@"Session_token",pID,@"parent_id",cPostId,@"post_id", pageStr,@"page_no",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        { [activityIndicator stopAnimating];
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            int success = [[result objectForKey:@"success"] intValue];
            NSString *msg = [result objectForKey:@"message"];
            if(success == 1) {
                NSDictionary *postData =[result objectForKey:@"post"];
                [self parseVideoModel:postData];
                [self.commentsTable reloadData];
                [commentsCollectionView reloadData];
                [self populateMainView];
            }
            else{
                UIAlertController * view=   [UIAlertController
                                             alertControllerWithTitle:@""
                                             message:msg
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* cancel = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [self.navigationController popViewControllerAnimated:YES];
                                             [view dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                
                [view addAction:cancel];
                [self presentViewController:view animated:YES completion:nil];
                
            }
        }
        else{
            [activityIndicator stopAnimating];
        }
    }];
}

-(void)parseVideoModel:(NSDictionary*)postDate{
    VideoModel *videomodl          = [[VideoModel alloc]init];
    videomodl.userName             = [postDate valueForKey:@"full_name"];
    videomodl.is_anonymous         = [postDate valueForKey:@"is_anonymous"];
    videomodl.title                = [postDate valueForKey:@"caption"];
    videomodl.comments_count       = [postDate valueForKey:@"comment_count"];
    videomodl.topic_id             = [postDate valueForKey:@"topic_id"];
    videomodl.user_id              = [postDate valueForKey:@"user_id"];
    videomodl.profile_image        = [postDate valueForKey:@"profile_link"];
    videomodl.like_count           = [postDate valueForKey:@"like_count"];
    videomodl.seen_count           = [postDate valueForKey:@"seen_count"];
    videomodl.video_link           = [postDate valueForKey:@"video_link"];
    videomodl.m3u8_video_link      = [postDate valueForKey:@"m3u8_video_link"];
    videomodl.video_thumbnail_link = [postDate valueForKey:@"video_thumbnail_link"];
    videomodl.videoID              = [postDate valueForKey:@"id"];
    videomodl.Tags                 = [postDate valueForKey:@"tag_friends"];
    videomodl.video_length         = [postDate valueForKey:@"video_length"];
    videomodl.like_by_me           = [postDate valueForKey:@"liked_by_me"];
    videomodl.reply_count          = [postDate objectForKey:@"reply_count"];
    videomodl.current_datetime     = [postDate objectForKey:@"current_datetime"];
    videomodl.uploaded_date        = [postDate objectForKey:@"uploaded_date"];
    videomodl.isMute               = [postDate objectForKey:@"mute"];
    videomodl.beam_share_url       = [postDate objectForKey:@"beam_share_url"];
    videomodl.deep_link            = [postDate objectForKey:@"deep_link"];
    videomodl.isCelebrity  = [postDate objectForKey:@"is_celeb"];
    postArray = videomodl;
}
-(void) resetPost{
    
    likes.text      = postArray.like_count;
    replies.text    = postArray.comments_count;
    views.text      = postArray.seen_count;
    
    [coverimgComments sd_setImageWithURL:[NSURL URLWithString:postArray.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"loading"]];
    
    postID = postArray.videoID;
    videoLengthComments.text = postArray.video_length;
    titleComments.text = postArray.title;
    NSInteger viewsCount = [postArray.seen_count intValue];
    NSInteger repliesCount = [postArray.comments_count intValue];
    NSInteger  likeCount = [postArray.like_count intValue];
    _timelabel.text      = [Utils returnDate:postArray.uploaded_date];
    if( likeCount == 1)
        likeslbl.text = @"Like";
    if(viewsCount == 1)
        viewslbl.text = @"View";
    if(repliesCount == 1)
        replieslbl.text = @"Reply";
    if(likeCount == 0)
        _getLikes.hidden = true;
    if([postArray.like_by_me isEqualToString:@"1"]){
        [likeBtn setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
    }
    else{
        [likeBtn setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
    }
    Postusername.text = postArray.userName;
    if([postArray.isMute isEqualToString:@"0"] && [postArray.is_anonymous isEqualToString: @"1"]){
        _anonyImage.image        = [UIImage imageNamed:@"btanonymous.png"];
        Postusername.text        = NSLocalizedString(@"anonymous", nil);
        _anonyImage.hidden       = NO;
        _muteImage.hidden        = YES;
    }
    else if([postArray.isMute isEqualToString:@"1"] && [postArray.is_anonymous isEqualToString: @"1"])
    {
        _anonyImage.hidden = NO;
        _muteImage.hidden = NO;
        _muteImage.image  = [UIImage imageNamed:@"speaker-mute.png"];
        _anonyImage.image  = [UIImage imageNamed:@"btanonymous.png"];
        Postusername.text  = NSLocalizedString(@"anonymous", nil);
    }
    else if([postArray.isMute isEqualToString: @"1"] && [postArray.is_anonymous isEqualToString: @"0"]){
        _anonyImage.hidden = NO;
        _anonyImage.image  = [UIImage imageNamed:@"speaker-mute.png"];
    }
    else{
        _anonyImage.hidden = YES;
        _muteImage.hidden = YES;
    }
    _anonyImage.hidden = YES;
    _muteImage.hidden = YES;
}


//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    NSArray *visibleRows = [self.commentsTable visibleCells];
//    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
//    NSIndexPath *path = [self.commentsTable indexPathForCell:lastVisibleCell];
//    if(path.section == 0 && path.row == dataArray.count/2 - 1)
//    {
//        if(!cannotScrollMore && !serverCall) {
//            pageNum++;
//            [self getCommentsOnPost];
//        }
//
//    }
//}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                    withVelocity:(CGPoint)velocity
             targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    if (velocity.y > 0){
        self.isDownwards = YES;
        //[self hideBottomBar];
    }
    if (velocity.y < 0){
        self.isDownwards = YES;
        //[self showBottomBar];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!serverCall && indexPath.row == dataArray.count/ 3 - 1 && !cannotScrollMore && dataArray.count >= 10) {
        pageNum++;
        [self getCommentsOnPost];
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView
      willDisplayCell:(UICollectionViewCell *)cell
   forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (!serverCall && indexPath.row == dataArray.count - 1 && !cannotScrollMore && dataArray.count >= 10) {
        pageNum++;
        [self getCommentsOnPost];
    }
    
}
#pragma mark HIDE AND SHOW BOTTOM BAR
-(void)showBottomBar{
    CGFloat padding;
    if(IS_IPHONE_6)
        padding = 100;
    else if(IS_IPHONE_6Plus)
        padding = 110;
    [UIView animateWithDuration:0.25 delay:0.0 usingSpringWithDamping:1.0
          initialSpringVelocity:1 options:UIViewAnimationOptionTransitionNone animations:^{
              self.bottomBarView.frame = CGRectMake(self.bottomBarView.frame.origin.x, self.view.frame.size.height - padding, self.bottomBarView.frame.size.width, self.bottomBarView.frame.size.height);
          } completion:^(BOOL finished) {
              
          }];
}
-(void)hideBottomBar{
    CGFloat padding;
    if(IS_IPHONE_6)
        padding = 100;
    else if(IS_IPHONE_6Plus)
        padding = 110;
    [UIView animateWithDuration:0.25 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionTransitionNone animations:^{
        self.bottomBarView.frame = CGRectMake(self.bottomBarView.frame.origin.x, self.view.frame.size.height + padding, self.bottomBarView.frame.size.width, self.bottomBarView.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}
- (CGRect)alertRect {
    UIScreen *mainScreen = [UIScreen mainScreen];
    if(IS_IPAD)
        return CGRectMake(98, 0,570, 168);
    else
        return CGRectMake(-20, -10, mainScreen.bounds.size.width + 40, 65);
}
-(void)GetReplyOnPost{
    if(!isFirstComment){
        CommentsVC *commentController ;
        if(IS_IPAD)
            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
        else  if (IS_IPHONEX){
            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
        }
        else
            commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
        commentController.commentsObj = Nil;
        commentController.postArray = videoModel;
        commentController.cPostId   = cPostId;
        commentController.isFirstComment = FALSE;
        commentController.isComment     = TRUE;
        commentController.pID           = ParentCommentID;
        if(ParentCommentID != nil)
        {
            [[self navigationController] pushViewController:commentController animated:YES];
        }
    }
}
#pragma mark - Beam Pressed

- (IBAction)beamPressed:(UIButton *)sender {
    if([sender tag] == 100){
        NSDate *lastAnonymusBeamDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"lastAnonymusBeam"];
        uploadAnonymous = true;
        uploadBeamTag = false;
        
    }
    else if([sender tag ] == 101)
    {
        uploadAnonymous = false;
        uploadBeamTag = true;
    }
    if([postArray.reply_count intValue] == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Comments are not allowed on this Beam" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else{
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            NSBundle *bundle = [NSBundle bundleForClass:HBRecorder.class];
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HBRecorder.bundle/HBRecorder" bundle:bundle];
            
            HBRecorder *recorder = [sb instantiateViewControllerWithIdentifier:@"HBRecorder"];
            recorder.delegate = self;
            if(uploadAnonymous) {
                UIImageView *anonymousMark = [[UIImageView alloc] initWithFrame:CGRectMake(10, 64, recorder.view.frame.size.width - 20, recorder.view.frame.size.height - 150)];
                anonymousMark.image = [UIImage imageNamed:@"ano_new_icon"];
                anonymousMark.alpha = 0.5;
                [recorder.view addSubview:anonymousMark];
            }
            recorder.topTitle = @"";
            recorder.bottomTitle = @"";
            recorder.maxRecordDuration = 60 * 1;
            recorder.movieName = @"MyAnimatedMovie";
            
            
            recorder.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self.navigationController pushViewController:recorder animated:YES];
            
        } else {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"I'm afraid there's no camera on this device!" delegate:nil cancelButtonTitle:@"Dang!" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}

#pragma mark - HBRECORDER Recording Methods

- (void)recorder:(HBRecorder *)recorder  didFinishPickingMediaWithUrl:(NSURL *)videoUrl {
    
    _videoPath = videoUrl;
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        AVAsset *asset = [AVAsset assetWithURL:_videoPath];// url= give your url video here
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        
        imageGenerator.appliesPreferredTrackTransform = YES;
        
        CMTime time = CMTimeMake(5, 10);//it will create the thumbnail after the 5 sec of video
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
        self.thumbnailToUpload = thumbnail;
        int i = 0;
        
        if(i == 0) {
            AVAsset *asset = [AVAsset assetWithURL:videoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime thumbTime = CMTimeMake(10,5);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTime actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
            self.thumbnailToUpload2 = thumbnail;
            
            i++;
        }
        
        if(i == 1) {
            AVAsset *asset = [AVAsset assetWithURL:videoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime thumbTimes = CMTimeMake(10,1);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTimes actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
            self.thumbnailToUpload3 = thumbnail;
        }
        
        
        profileData = UIImagePNGRepresentation(thumbnail);
        CmovieData = [NSData dataWithContentsOfURL:videoUrl];
        
        CMTime CMduration = asset.duration;
        int totalSeconds = CMTimeGetSeconds(CMduration);
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;
        NSString *duration = @"";
        if (hours>0) {
            NSString *hoursString = [NSString stringWithFormat:@"%d hour(s)",hours];
            duration = [duration stringByAppendingString:hoursString];
        }
        if (minutes>0) {
            NSString *minString = [NSString stringWithFormat:@"%d min(s)",minutes];
            duration = [duration stringByAppendingString:minString];
        }
        if (seconds>0) {
            NSString *secString = [NSString stringWithFormat:@"%d sec(s)",seconds];
            duration = [duration stringByAppendingString:secString];
        }
        video_duration = [[NSString alloc]initWithFormat:@"%02lu:%02lu",(unsigned long)minutes,(unsigned long)seconds];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self movetoUploadBeamController];
        });
    });
    
    
}

- (void)recorderDidCancel:(HBRecorder *)recorder {
    NSLog(@"Recorder did cancel..");
}

- (void)recorderOrientation:(NSInteger)orientation {
    orientationAngle = orientation;
}

#pragma mark - Delegate Methods

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    // user hit cancel
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSURL *chosenMovie = [info objectForKey:UIImagePickerControllerMediaURL];
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:chosenMovie options:nil];
    
    NSTimeInterval durationInSeconds = 0.00;
    if (asset)
        durationInSeconds = CMTimeGetSeconds(asset.duration);
    
    NSUInteger dTotalSeconds = durationInSeconds;
    
    NSUInteger dSeconds =(dTotalSeconds  % 60);
    NSUInteger dMinutes = (dTotalSeconds / 60 ) % 60;
    
    video_duration = [[NSString alloc]initWithFormat:@"%02lu:%02lu",(unsigned long)dMinutes,(unsigned long)dSeconds];
    
    CmovieData = [NSData dataWithContentsOfURL:chosenMovie];
    _videoPath = chosenMovie;
    // and dismiss the picker
    [self dismissViewControllerAnimated:YES completion:nil];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = [asset duration];
    time.value = 0;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
    CGImageRelease(imageRef);
    self.thumbnailToUpload = thumbnail;
    profileData = UIImagePNGRepresentation(thumbnail);
    [self movetoUploadBeamController];
}
-(void) movetoUploadBeamController{
    
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.videoPath = _videoPath;
    uploadController.dataToUpload = CmovieData;
    uploadController.video_duration = video_duration;
    uploadController.friendsArray   = appDelegate.friendsArray;
    uploadController.orientationAngle = orientationAngle;
    
    if(!self.isFromDeepLink){
        if(isFirstComment || appDelegate.parentIdStackController.count == 1){
            uploadController.ParentCommentID = @"-1";
        }
        else
        {
            uploadController.ParentCommentID = (NSString*)[appDelegate.parentIdStackController peekObject];
        }
    }
    else{
        uploadController.ParentCommentID = pID;
    }
    uploadController.postID = cPostId;
    uploadController.isAudio = false;
    uploadController.profileData = profileData;
    uploadController.isComment = TRUE;
    uploadController.thumbnailImage = self.thumbnailToUpload;
    uploadController.thumbnailImage2 = self.thumbnailToUpload2;
    uploadController.thumbnailImage3 = self.thumbnailToUpload3;
    uploadController.friendsArray   = appDelegate.friendsArray;
    if(uploadAnonymous)
        uploadController.isAnonymous = true;
    else
        uploadController.isAnonymous = false;
    [[self navigationController] pushViewController:uploadController animated:YES];
}
- (IBAction)back:(id)sender {
    [appDelegate.parentIdStackController popObject];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)RecorderPressed:(id)sender {
    if([postArray.reply_count intValue] == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Comments are not allowed on this Beam" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else{
        [_videoPlayerController pause];

        _uploadAudioView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        [self.view addSubview:_uploadAudioView];
    }
    
}
#pragma mark AUDIO RECORDING AND UPLOADING

-(void)setAudioRecordSettings
{
    NSArray *dirPaths;
    NSString *docsDir;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(
                                                   NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString *soundFilePath = [docsDir
                               stringByAppendingPathComponent:@"sound.caf"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    //[NSNumber numberWithInt:kAudioFormatMPEGLayer3], AVFormatIDKey,
                                    [NSNumber numberWithInt:AVAudioQualityHigh],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 1],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    
    NSError *error = nil;
    
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];
    _audioRecorder.delegate = self;
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
        
    } else {
        [_audioRecorder prepareToRecord];
    }
}

- (IBAction)recorderTapped:(id)sender {
    closeBtnAudio.hidden = true;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                        error:nil];
    if(!isRecording){
        [self animateImages];
        timerToupdateLbl = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
        audioTimeOut = [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self
                                                      selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: NO];
        [_audioRecorder record];
    }
    else{
        
        [_audioRecorder stop];
        [audioBtnImage stopAnimating];
    }
    isRecording = true;
}
- (IBAction)AudioClosePressed:(id)sender {
    [_videoPlayerController pause];
    [_uploadAudioView removeFromSuperview];
}
-(void) callAfterSixtySecond:(NSTimer*) t
{
    [_audioRecorder stop];
    [timerToupdateLbl invalidate];
    [audioTimeOut invalidate];
    
}
-(void) updateCountdown {
    int minutes, seconds;
    secondsLeft--;
    minutes = (secondsLeft / 60) % 60;
    seconds = (secondsLeft) % 60;
    countDownlabel.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    secondsConsumed = [NSString stringWithFormat:@"%02d:%02d", 00, 60 - secondsLeft];
}
-(void)animateImages{
    NSArray *loaderImages = @[@"state1.png", @"state2.png", @"state3.png"];
    NSMutableArray *loaderImagesArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < loaderImages.count; i++) {
        [loaderImagesArr addObject:[UIImage imageNamed:[loaderImages objectAtIndex:i]]];
    }
    audioBtnImage.animationImages = loaderImagesArr;
    audioBtnImage.animationDuration = 0.5f;
    [audioBtnImage startAnimating];
}
-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    [timerToupdateLbl invalidate];
    [audioTimeOut invalidate];
    closeBtnAudio.hidden = false;
    isRecording = false;
    countDownlabel.text = @"01:00";
    secondsLeft = 60;
    CaudioData = [NSData dataWithContentsOfURL:_audioRecorder.url];
    
    [_uploadAudioView removeFromSuperview];
    
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.dataToUpload = CaudioData;
    uploadController.videoPath = _audioRecorder.url;
    if([secondsConsumed length] == 0)
        secondsConsumed = @"00:01";
    uploadController.video_duration = secondsConsumed;
    
    uploadController.friendsArray   = appDelegate.friendsArray;
    if(!self.isFromDeepLink){
        if(isFirstComment || appDelegate.parentIdStackController.count == 1)
            uploadController.ParentCommentID = @"-1";
        else
        {
            
            uploadController.ParentCommentID = (NSString*)[appDelegate.parentIdStackController peekObject];
        }
    }
    else{
        uploadController.ParentCommentID = pID;
    }
    uploadController.postID = cPostId;
    uploadController.isAudio = true;
    uploadController.isComment = TRUE;
    self.thumbnailToUpload = [UIImage imageNamed: @"splash_audio_image.png"];
    uploadController.thumbnailImage = [UIImage imageNamed: @"splash_audio_image.png"];
    [[self navigationController] pushViewController:uploadController animated:YES];
    
}
-(void)audioRecorderEncodeErrorDidOccur:
(AVAudioRecorder *)recorder
                                  error:(NSError *)error
{  isRecording = false;
    closeBtnAudio.hidden = false;
    countDownlabel.text = @"00:00";
    secondsLeft = 60;
    secondsConsumed = 0;
//    NSLog(@"Encode Error occurred");
}

- (void)LikeHearts:(UIButton*)sender{
    //liked = nil;
    UIButton *LikeBtn = (UIButton *)sender;
    currentSelectedIndex = LikeBtn.tag;
    VideoModel *tempVideos = [[VideoModel alloc]init];
    tempVideos  = [dataArray objectAtIndex:currentSelectedIndex];
    postID = tempVideos.videoID;
    [self LikePost:currentSelectedIndex];
    //
    //    if (liked == YES) {
    //        [LikeBtn setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
    //    }else if (liked == NO){
    //        [LikeBtn setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
    //    }
}
-(void)playVideo:(UIButton*)sender{
    
    UIButton *playBtn = (UIButton *)sender;
    currentSelectedIndex = playBtn.tag;
    [videoObj removeAllObjects];
    
    for(int i = 0; i < commentsObj.CommentsArray.count ; i++){
        CommentsModel *model = [commentsObj.CommentsArray objectAtIndex:i];
        VideoModel *temp = [[VideoModel alloc] init];
        temp.is_anonymous           = model.is_anonymous;
        temp.title                  = model.title;
        temp.comments_count         = model.comments_count;
        temp.userName               = model.userName;
        temp.topic_id               = model.topic_id;
        temp.user_id                = model.user_id;
        temp.profile_image          = model.profile_link;
        temp.video_link             = model.video_link;
        temp.video_thumbnail_link   = model.video_thumbnail_link;
        temp.image_link             = model.image_link;
        temp.m3u8_video_link        = model.m3u8_video_link;
        temp.videoID                = model.VideoID;
        temp.video_length           = model.video_length;
        temp.like_count             = model.comment_like_count;
        temp.like_by_me             = model.liked_by_me;
        temp.seen_count             = model.seen_count;
        temp.reply_count            = model.reply_count;
        [videoObj addObject:temp];
    }
    
    VideoPlayerVC *videoPlayer;
    if(IS_IPAD)
        videoPlayer = [[VideoPlayerVC alloc] initWithNibName:@"VideoPlayerVC_iPad" bundle:nil];
    else if(IS_IPHONE_6Plus)
        videoPlayer = [[VideoPlayerVC alloc] initWithNibName:@"VideoPlayerVC_iPhonePlus" bundle:nil];
    else
        videoPlayer = [[VideoPlayerVC alloc] initWithNibName:@"VideoPlayerVC" bundle:nil];
    
    videoPlayer.videoObjs       = videoObj;
    videoPlayer.indexToDisplay  = currentSelectedIndex;
    videoPlayer.isComment       = true;
    videoPlayer.cPostId         = cPostId;
    videoPlayer.isFirst         = false;
    videoPlayer.view.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.view addSubview:videoPlayer.view];
                         videoPlayer.view.transform=CGAffineTransformMakeScale(1, 1);
                     }
                     completion:^(BOOL finished){
                         [videoPlayer.view removeFromSuperview];
                         [self.navigationController pushViewController:videoPlayer animated:NO];
                     }];
    
    [self SeenPost];
}
#pragma mark - Like Post
- (void) LikePost:(NSUInteger )indexToLike{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_LIKE_COMMENT,@"method",
                              token,@"session_token",postID,@"comment_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                if ([message isEqualToString:@"Comment is Successfully Liked."]) {
                    CommentsModel *_Videos = [[CommentsModel alloc]init];
                    _Videos  = [commentsObj.CommentsArray objectAtIndex:indexToLike];
                    NSInteger likeCount = [_Videos.comment_like_count intValue];
                    likeCount++;
                    _Videos.comment_like_count = [NSString stringWithFormat: @"%ld", likeCount];
                    _Videos.liked_by_me = @"1";
                    [commentsObj.CommentsArray replaceObjectAtIndex:indexToLike withObject:_Videos];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexToLike/2 inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [self.commentsTable beginUpdates];
                    [self.commentsTable reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                    [self.commentsTable endUpdates];
                }else if ([message isEqualToString:@"User have Successfully Unliked the comment"])
                {
                    //  liked = NO;
                    CommentsModel *_Videos = [[CommentsModel alloc]init];
                    _Videos  = [commentsObj.CommentsArray objectAtIndex:indexToLike];
                    NSInteger likeCount = [_Videos.comment_like_count intValue];
                    if(likeCount > 0)
                        likeCount--;
                    _Videos.comment_like_count = [NSString stringWithFormat: @"%ld", likeCount];
                    _Videos.liked_by_me = @"0";
                    [commentsObj.CommentsArray replaceObjectAtIndex:indexToLike withObject:_Videos];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexToLike/2 inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [self.commentsTable beginUpdates];
                    [self.commentsTable reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                    [self.commentsTable endUpdates];
                    
                }
            }
        }
        else{
        }
    }];
}

- (IBAction)MainPlayBtn:(id)sender {
    UIButton *playBtn = (UIButton *)sender;
    currentSelectedIndex = playBtn.tag;
    playBtn.enabled = FALSE;
    [videoObj removeAllObjects];
    
    PlayerViewController *videoPlayer; // = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
    
    if (IS_IPHONEX){
        videoPlayer = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController_IphoneX" bundle:nil];
    }else{
        videoPlayer = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
    }
    videoPlayer.vModel       = postArray;
    videoPlayer.isComment       = isComment;
    
    videoPlayer.postArray = postArray;
    videoPlayer.cPostId = cPostId;
    videoPlayer.isFirstComment = isFirstComment;
    
    [[self navigationController] pushViewController:videoPlayer animated:YES];
    
    if(!isComment){
        [self SeenPost];
    }else{
        [self CommentSeen];
    }
    
    //[[NavigationHandler getInstance]MoveToPlayer];
}

-(void)updateLikeCount:(BOOL)toIncrement{
    
    
    NSInteger likeCount = [self.postArray.like_count integerValue];
    if(!toIncrement){
        self.hLikeBTn.image = SET_IMAGE(@"likenew.png");
        if(likeCount > 0){
            likeCount -= 1;
        }
        else{
            likeCount = 0;
        }
    }
    else{
        self.hLikeBTn.image = SET_IMAGE(@"likeblue.png");
        likeCount += 1;
    }
    NSString *likeCountString = [NSString stringWithFormat:@"%ld",(long)likeCount];
    NSString *abbLikesCount = [Utils abbreviateNumber:likeCountString withDecimal:1];
    self.postArray.like_count = [NSString stringWithFormat:@"%ld",(long)likeCount];
    if(postArray.like_count == NULL){
        self.hLikes.text = [NSString  stringWithFormat:@"0 %@",NSLocalizedString(@"likes", @"")];
    }
    else if(likeCount == 1 ){
        self.hLikes.text
        =[NSString  stringWithFormat:@"%@ %@",abbLikesCount,NSLocalizedString(@"likes", @"")];
    }else{
        
        self.hLikes.text
        =[NSString  stringWithFormat:@"%@ %@",abbLikesCount,NSLocalizedString(@"likes", @"")];
    }
}

-(IBAction)likeComment:(id)sender{
    if(!isComment)
    {
        if([postArray.like_by_me isEqualToString:@"1"])
        {
            self.postArray.like_by_me = @"0";
            [self updateLikeCount:NO];
        }
        else{
            self.postArray.like_by_me = @"1";
            [self updateLikeCount:YES];
        }
        [self LikeTopPost];
    }
    else
    {
        if([postArray.like_by_me isEqualToString:@"1"])
        {
            self.postArray.like_by_me = @"0";
            [self updateLikeCount:NO];
        }
        else{
            self.postArray.like_by_me = @"1";
            [self updateLikeCount:YES];
        }
        [self LikeComment];
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.commentsTable reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
}
- (void) LikeComment{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_LIKE_COMMENT,@"method",
                              token,@"session_token",postID,@"comment_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            
            if(success == 1){
                if ([message isEqualToString:@"Comment is Successfully Liked."]) {
                    appDelegate.timeToupdateHome = TRUE;
                    //                    NSInteger likeCount =  [postArray.like_count integerValue];
                    //                    likeCount++;
                    //                    postArray.like_count = [NSString stringWithFormat:@"%ld",(long)likeCount];
                    _getLikes.hidden = NO;
                    
                }
                else if ([message isEqualToString:@"User have Successfully Unliked the comment"])
                {
                    appDelegate.timeToupdateHome = TRUE;
                    NSInteger likeCount =  [postArray.like_count integerValue];
                    // likeCount--;
                    //                    postArray.like_count = [NSString stringWithFormat:@"%ld",(long)likeCount];
                    if(likeCount == 0)
                        _getLikes.hidden = YES;
                }
            }
            else{
                //                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //                [alert show];
            }
        }
    }];
}
-(void)presentMuteAndDelet:(int)state{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* Share = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"share", nil)
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [self shareDialog];
                                [view dismissViewControllerAnimated:YES completion:nil];
                                
                            }];
    
    UIAlertAction* deleteCurrentPost = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"delete_beam", nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            [self DeleteBtn:self];
                                            [view dismissViewControllerAnimated:YES completion:nil];
                                            
                                        }];
    UIAlertAction* shareOnFb = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"share_on_facebook", nil)                              style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self fbshareBtn:self];
                                    [view dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    UIAlertAction* ShareonTwt = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"share_on_twitter", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self twitterShareBtn:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
    UIAlertAction* reportBeam = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"report_beam", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self reporttapped:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
    UIAlertAction* blockP = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"block_user", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self blockBtn:self];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* mute = [UIAlertAction
                           actionWithTitle:NSLocalizedString(@"mute_beam", nil)
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               [self MuteComment:NO];
                               [view dismissViewControllerAnimated:YES completion:nil];
                               
                           }];
    UIAlertAction* muteMainPost = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"mute_beam", nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self MuteComment:YES];
                                       [view dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"cancel", nil)
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    if((state == 8 || state == 9)&& !usersPost){
        
        [view addAction:Share];
        [view addAction:muteMainPost];
        [view addAction:reportBeam];
        [view addAction:blockP];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(620, 410, 200, 200);
    }
    else if((state == 8 || state == 9)&& usersPost){
        
        [view addAction:Share];
        [view addAction:muteMainPost];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(600, 50, 200, 200);
    }
    else if(!usersPost){
        [view addAction:Share];
        [view addAction:reportBeam];
        [view addAction:blockP];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(600, 50, 200, 200);
    }else {
        [view addAction:Share];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(600, 50, 200, 200);
    }
    //    view.popoverPresentationController.sourceView = self.view;
    //    view.popoverPresentationController.sourceRect = CGRectMake(600, 50, 200, 200);
    [self presentViewController:view animated:YES completion:nil];
}
-(void)presentAll:(int)state{
    int isCelebrity = [Utils userIsCeleb];
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* Share = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"share", @"")
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [self shareDialog];
                                [view dismissViewControllerAnimated:YES completion:nil];
                                
                            }];
    UIAlertAction *sharewithInApp = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"inbox_to_friend", @"")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         if(!isCelebrity) {
                                             [self showToshare:postArray.videoID];
                                             [view dismissViewControllerAnimated:YES completion:nil];
                                         }
                                     }];
    UIAlertAction *postOnFriendsCorner = [UIAlertAction
                                          actionWithTitle:NSLocalizedString(@"post_on_friend_corner", @"")
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              if(!isCelebrity) {
                                                  [self shareOnFriendsCorner:postArray.videoID];
                                                  [view dismissViewControllerAnimated:YES completion:nil];
                                              }
                                          }];
    UIAlertAction *rebeam = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"rebeam", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self shareOnMyCorner:postArray.videoID];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                             }];
    UIAlertAction* deleteCurrentPost = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"delete_beam", @"")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            [self DeleteBtn:self];
                                            [view dismissViewControllerAnimated:YES completion:nil];
                                            
                                        }];
    UIAlertAction* editCurrentBeam = [UIAlertAction
                                      actionWithTitle:NSLocalizedString(@"edit_beam", @"")
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          [self editBeam:self];
                                          [view dismissViewControllerAnimated:YES completion:nil];
                                          
                                      }];
    
    UIAlertAction* reportBeam = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"report_beam", @"")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self reporttapped:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
    UIAlertAction* blockP = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"block_user", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self blockBtn:self];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* mute = [UIAlertAction
                           actionWithTitle:NSLocalizedString(@"mute_beam", @"")
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               [self MuteComment:NO];
                               [view dismissViewControllerAnimated:YES completion:nil];
                               
                           }];
    UIAlertAction* muteMainPost = [UIAlertAction
                                   actionWithTitle:@"Mute this Beam"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self MuteComment:YES];
                                       [view dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
    UIAlertAction* delete = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"delete_beam", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self deleteComment:NO];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"cancel", @"")
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction *downloadBeam = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"save_to_gallery", @"")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action){
                                       [Utils DownloadVideo:postArray.video_link];
                                       [view dismissViewControllerAnimated:YES completion:nil];
                                   }];
    
    //3 == mute and delete  and 4 ==  neither post nor comment
    if(state == 0 && !usersPost){
        [view addAction:Share];
        [view addAction:rebeam];
        [view addAction:postOnFriendsCorner];
        [view addAction:sharewithInApp];
        [view addAction:downloadBeam];
        [view addAction:reportBeam];
        [view addAction:blockP];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(630, 400, 200, 200);
    }
    else if(state == 0 && usersPost){
        [view addAction:Share];
        [view addAction:rebeam];
        [view addAction:postOnFriendsCorner];
        [view addAction:sharewithInApp];
        [view addAction:downloadBeam];
        [view addAction:editCurrentBeam];
        [view addAction:deleteCurrentPost];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(630, 400, 200, 200);
    }
    else if(state == 1){
        [view addAction:mute];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height - 60, 200, 200);
    }else if(state == 3){
        [view addAction:Share];
        [view addAction:postOnFriendsCorner];
        [view addAction:sharewithInApp];
        [view addAction:downloadBeam];
        [view addAction:mute];
        [view addAction:delete];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height - 60, 200, 200);
    } else if(state == 4) {
        [view addAction:Share];
        [view addAction:postOnFriendsCorner];
        [view addAction:sharewithInApp];
        [view addAction:downloadBeam];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height - 60, 200, 200);
    }
    else{
        [view addAction:delete];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height - 60, 200, 200);
    }
    //    view.popoverPresentationController.sourceView = self.view;
    //    view.popoverPresentationController.sourceRect = CGRectMake(630, 400, 200, 200);
    [self presentViewController:view animated:YES completion:nil];
}

#pragma mark Get Sharing Content
-(void)shareDialog{
    if(postArray.beam_share_url){
        [self showShareSheet:postArray.beam_share_url];
    }
}

#pragma mark Sheet Actions
-(void)showToshare:(NSString *)pOstID{
    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = [Utils getFriendsToShareBeam];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 5;
    commentController.userId        = @"";
    commentController.postId = pOstID;
    commentController.isCommentVideo = @"0";
    commentController.isLocalSearch = YES;
    [[self navigationController] pushViewController:commentController animated:YES];
}
-(void)shareOnFriendsCorner:(NSString *)postId{
    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = [Utils getFriendsToShareBeam];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 6;
    commentController.userId        = @"";
    commentController.postId = postId;
    NSString *isCommentV = [NSString stringWithFormat:@"%d",isComment];
    commentController.isCommentVideo = isCommentV;
    commentController.isLocalSearch = YES;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(void)shareOnMyCorner:(NSString *)pOstID{
    NSString *isCommentV = [NSString stringWithFormat:@"%d",isComment];
    [Utils shareBeamOnCorner:pOstID isComment:isCommentV friendId:@"" sharingOnFriendsCorner:NO];
}

//-(void)shareUrlCall{
//    VideoModel *tempVideo;
//    __block NSString *urlToShare;
////    if(self.isThumbnailReq == 1){
////
////        serviceParams = [NSMutableDictionary dictionary];
////        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
////        [serviceParams setValue:postArray.videoID forKey:@"post_id"];
////        if(self.isComment)
////            [serviceParams setValue:@"1" forKey:@"is_comment"];
////        else
////            [serviceParams setValue:@"0" forKey:@"is_comment"];
////        [Utils downloadImageWithURL:[NSURL URLWithString:postArray.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
////            if (succeeded) {
////                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
////                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
////                NSData *imgData;
////                imgData = UIImagePNGRepresentation(im);
////                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
////
////                }name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
////                    int flag = [[data objectForKey:@"success"] intValue];
////                    if(flag){
////                        urlToShare = (NSString *)[data objectForKey:@"deep_link"];
////                        [self showShareSheet:urlToShare];
////                    }
////                } failure:^(NSError *error) {
////                }];
////            }
////        }];
////    }else{
////        urlToShare =  [NSString stringWithFormat:@"%@", postArray.deep_link];
////        [self showShareSheet:urlToShare];
////        //
////    }
//}

-(void)showShareSheet:(NSString *)shareURL{
    NSURL *url = [NSURL URLWithString:shareURL];
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[url]
     applicationActivities:nil];
    controller.excludedActivityTypes = @[UIActivityTypePrint,
                                         UIActivityTypeMail,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop,
                                         @"com.apple.mobilenotes.SharingExtension",
                                         @"com.apple.reminders.RemindersEditorExtension",
                                         ];
    controller.popoverPresentationController.sourceView = self.view;
    controller.popoverPresentationController.sourceRect = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300);
    [self presentViewController:controller animated:YES completion:nil];
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            // user shared an item
//            NSLog(@"We used activity type%@", activityType);
            if([activityType isEqualToString:@"com.apple.UIKit.activity.CopyToPasteboard"]){
                [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
            }
        } else {
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}
-(IBAction)editBtnPressed:(id)sender
{
    
    if(postArray.canDeleteComment == 0)
        [self presentAll:0];
    else if(postArray.canDeleteComment == 1){
        //Mute And Delete
        [self presentMuteAndDelet:8];
    }
    else if(postArray.canDeleteComment == 2){
        //Mute Beam
        [self presentMuteAndDelet:9];
    }
    else if(postArray.canDeleteComment == 3){
        //Delete
        [self presentMuteAndDelet:10];
    }
    
}
- (IBAction)CancelEditBtn:(id)sender{
    editView.hidden = YES;
}
-(IBAction)blockBtn:(id)sender{
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] == postArray.user_id){
        [self DeleteBtn:self];
    }
    else{
        [self BlockPerson:self];
    }
}
-(IBAction)reporttapped:(id)sender{
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] == postArray.user_id){
        [self editBeam:self];
    }
    else{
        [self ReportBtn:self];
    }
}
- (IBAction)editBeam:(id)sender{
    NSString *postIDs = cPostId;
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.video_thumbnail = postArray.video_thumbnail_link;
    
    uploadController.postID = postIDs;
    uploadController.friendsArray = appDelegate.friendsArray;
    uploadController.caption      = postArray.title;
    uploadController.isEdit = YES;
    appDelegate.hasbeenEdited     = TRUE;
    uploadController.privacyToShow = postArray.privacy;
    uploadController.replyContToShow = postArray.reply_count;
    
    if([postArray.reply_count isEqualToString:@"0"])
    {
        uploadController.repliesAllowed = false;
    }
    else
    {
        uploadController.repliesAllowed = true;
    }
    
    if([postArray.is_anonymous isEqualToString:@"1"])
    {
        uploadController.isAnonymousChecked = true;
    }
    else
    {
        uploadController.isAnonymousChecked = false;
    }
    [[self navigationController] pushViewController:uploadController animated:YES];
}
-(void)updateCommentsArray:(CommentsModel *)cObj{
    
    [commentsObj.CommentsArray addObject:cObj];
    [self.commentsTable reloadData];
    [commentsCollectionView reloadData];
}
-(IBAction)DeleteBtn:(id)sender{
    [CustomLoading showAlertMessage];
    NSString *postIDs = cPostId;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"deletePost",@"method",
                              token,@"session_token",postIDs,@"post_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            [CustomLoading DismissAlertMessage];
            if(success == 1){
                
                DataContainer *sharedManager = [DataContainer sharedManager];
                for(int i = 0; i < sharedManager.channelVideos.count;i++){
                    VideoModel *Vobj = [sharedManager.channelVideos objectAtIndex:i];
                    if([Vobj.videoID isEqualToString:cPostId]){
                        NSInteger beamCount = [[[NSUserDefaults standardUserDefaults]
                                                stringForKey:@"cachedBeamsCount"] integerValue] ;
                        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",beamCount-1] forKey:@"cachedBeamsCount"];
                        [sharedManager.channelVideos removeObjectAtIndex:i];
                        break;
                    }
                }
                sharedManager.isReloadMyCorner = YES;
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else{
            [CustomLoading DismissAlertMessage];
        }
    }];
}
- (IBAction)ReportBtn:(id)sender{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    editView.hidden = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"reportPost",@"method",
                              token,@"session_token",cPostId,@"post_id",@"For No Reason",@"reason",language,@"language",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
    
}

- (IBAction)btnCopyLink:(id)sender {
    __block NSString *urlToShare ;
    if(self.isThumbnailReq == 1){
        postArray.isThumbnailReq = 0;
        serviceParams = [NSMutableDictionary dictionary];
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:postArray.videoID forKey:@"post_id"];
        if(self.isComment)
            [serviceParams setValue:@"1" forKey:@"is_comment"];
        else
            [serviceParams setValue:@"0" forKey:@"is_comment"];
        [Utils downloadImageWithURL:[NSURL URLWithString:postArray.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                }
                                 name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                                     int flag = [[data objectForKey:@"success"] intValue];
                                     if(flag){
                                         urlToShare = (NSString *)[data objectForKey:@"deep_link"];
                                         UIPasteboard *pb = [UIPasteboard generalPasteboard];
                                         [pb setString: [NSString stringWithFormat:@"%@", urlToShare]];
                                         [editView setHidden:YES];
                                         [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
                                     }
                                 } failure:^(NSError *error) {
                                     
                                 }];
            }
        }];
    }else{
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString: [NSString stringWithFormat:@"%@", postArray.deep_link ]];
        [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
    }
}


-(IBAction)BlockPerson:(id)sender{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    editView.hidden = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"blockUser",@"method",
                              token,@"session_token",postArray.user_id,@"blocking_user_id",language,@"language",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                appDelegate.timeToupdateHome = TRUE;
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }
        
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
}
-(void) LikeTopPost{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_LIKE_POST,@"method",
                              token,@"session_token",postArray.videoID,@"post_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                if ([message isEqualToString:@"Post is Successfully liked."]) {
                    appDelegate.timeToupdateHome = TRUE;
                }
                else if ([message isEqualToString:@"Post is Successfully unliked by this user."])
                {
                    appDelegate.timeToupdateHome = TRUE;
                    NSInteger likeCount =  [postArray.like_count integerValue];
                    if(likeCount > 0){
                        _getLikes.hidden = NO;
                    }
                }
            }
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
    
}

- (void) SeenPost{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_POST_SEEN,@"method",
                              token,@"session_token",postID,@"post_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1){
                _sharedManager.isFriendViewChanged = YES;
                [self updateLikesCount];
            }
        }
        else{
            
        }
    }];
}

-(void)updateLikesCount{
    self.postArray.isViwedByMe = @"1";
    int i = [self.postArray.seen_count intValue];
    i+=1;
    self.postArray.seen_count = [NSString stringWithFormat:@"%d",i];
    NSString *abbViewsCount = [Utils abbreviateNumber:postArray.seen_count withDecimal:1];
    if(i == 1){
        self.hViews.text      =
        [NSString  stringWithFormat:@"%@ %@",abbViewsCount,NSLocalizedString(@"views", @"")];
    }else{
        self.hViews.text      =
        [NSString  stringWithFormat:@"%@ %@",abbViewsCount,NSLocalizedString(@"views", @"")];
    }
}

-(void)CommentSeen{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_COMMENT_SEEN,@"method",
                              token,@"session_token",postID,@"post_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1){
                [self updateLikesCount];
            }
        }
        else{
            
        }
    }];
}

-(UIViewController*) topMostController
{
    NSArray *controllers = [self.navigationController viewControllers];
    
    return [controllers lastObject];
}


- (IBAction)btnUserChannel:(id)sender {
    if(![postArray.is_anonymous boolValue]){
        
        UserChannel *commentController ;
        if(IS_IPAD){
            commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
        }else if (IS_IPHONEX){
            commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
        }
        else{
            commentController  = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
            
        }
        commentController.ChannelObj = nil;
        Followings *fData  = [[Followings alloc] init];
        BOOL isCl= [postArray.isCelebrity boolValue];
        fData.is_celeb = [NSString stringWithFormat:@"%d",isCl];
        commentController.friendID   = postArray.user_id;
        commentController.currentUser = fData;
        
        if(![[self topMostController] isKindOfClass:[UserChannel class]])
        {
            [[self navigationController] pushViewController:commentController animated:YES];
        }
        
    }
}

-(IBAction)moveToParentBeam:(id)sender{
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    commentController.commentsObj = Nil;
    commentController.postArray = nil;
    commentController.cPostId   = cPostId;
    commentController.isFirstComment = FALSE;
    commentController.isComment     = TRUE;
    commentController.pID           = self.parentRootId;
    [[self navigationController] pushViewController:commentController animated:YES];
}
- (IBAction)pauseBtnPressed:(id)sender {
    if(_playnstopCheck){
        [_videoPlayerController pause];
        _playnstopCheck = false;
    }
    else{
        [_videoPlayerController playFromCurrentTime];
        _playnstopCheck = true;
    }
}

-(NSString *)getParentPostID {
    return postArray.videoID;
}
-(NSMutableArray *)getCommentsArray {
    return dataArray;
}

-(void)addLocalObjectToCommentsArray:(VideoModel *)VObj{//
//    [dataArray insertObject:VObj atIndex:0];
//    NSInteger reply = [self.postArray.comments_count integerValue];
//    reply = reply + 1;
//    self.postArray.comments_count = [[NSString alloc] initWithFormat:@"%ld",(long)reply];
//
//    if([self.postArray.comments_count integerValue] == 1){
//        self.hReplies.text    =
//        [NSString  stringWithFormat:@"%@ %@", self.postArray.comments_count,NSLocalizedString(@"reply", @"")];
//    }else{
//        self.hReplies.text    =
//        [NSString  stringWithFormat:@"%@ %@",self.postArray.comments_count,NSLocalizedString(@"replies", @"")];
//    }
    [self.commentsTable reloadData];
    [commentsCollectionView reloadData];
}

-(void) setOfflineContent{
    //[dataArray removeAllObjects];
    //shareobj.commentsDict
    DataContainer *shareobj = [DataContainer sharedManager];
    
//    NSLog(@"pID %@", cPostId);
//    NSLog(@"ParentCommentID %@", ParentCommentID);
//    NSLog(@"_parentRootId %@", _parentRootId);
//
//    if(sharedManager.commentsDict!=nil){
//
//
//        if (sharedManager.commentsDict[cPostId] != nil){
//
//            NSMutableArray *comments = sharedManager.commentsDict[cPostId];
//
////            NSLog(@"%@", comments);
//
//            //comments=[[[comments reverseObjectEnumerator] allObjects] mutableCopy];
//
//
//            for(int i = 0; i<comments.count;i++){
//
//                VideoModel *v = comments[i];
//                [self addLocalObjectToCommentsArray:v];
//
//            }
//
//        }
//
//    }
    
    
    shareobj = [DataContainer sharedManager];
    
    NSArray *tempArray = [[DBCommunicator sharedManager] retrieveAllRows];
    NSMutableArray *offlineNewData = [NSMutableArray array];
    
    if(tempArray.count > 0){
        
        for (int i = 0; i < tempArray.count; i++)
        {
        
        OfflineDataModel *model = tempArray[i];
        
        if ([model.postId isEqualToString:cPostId]){
        VideoModel *videos     = [[VideoModel alloc] init];
        NSString *userId = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"id"];
         NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Name"];
        [videos setCurrent_datetime:model.currentTime];
        [videos setUser_id: userId];
        videos.is_anonymous = @"0";
        videos.userName             = userName;
        videos.user_id              = userId;
        videos.profileImageData     = profileData;
        videos.isLocal = true;
        
        
        
        commentAllowed = model.allowedReply;
        textToshare = model.caption;
        anony = model.anonyCheck;
        if([anony isEqualToString: @"1"]) {
            isAnonymous = YES;
        } else {
            isAnonymous = NO;
        }
        video_duration = model.duration;
        postID = model.postId;
        ParentCommentID = model.parentId;
        privacySelected = model.privacyCheck;
        timestamp = model.currentTime;
        NSData *videoData = [self getVideoFromDocuments:model.data];
        playBtnThumbnail = [self getThumbnailFromDocuments:model.thumbnail];
        profileData = playBtnThumbnail;
        
        
        if([model.isComment isEqualToString:@"1"]) {
            
            bool found = false;
            for(VideoModel *eachOfflineModel in dataArray)
            {
                if([eachOfflineModel.current_datetime isEqualToString:videos.current_datetime])
                {
                    found = true;
                    break;
                }
            }
                
                
                
                
            if(!found)
            {
                [dataArray insertObject:videos atIndex:0];
                [offlineNewData addObject:videos];
            }
            
            [self.commentsTable reloadData];
            [commentsCollectionView reloadData];
            
            
        }
    }
     
    }
        
        
//        NSInteger reply = [self.postArray.comments_count integerValue];
//        reply = reply + offlineNewData.count;
//        self.postArray.comments_count = [[NSString alloc] initWithFormat:@"%ld",(long)reply];
//
//        if([self.postArray.comments_count integerValue] == 1){
//            self.hReplies.text    =
//            [NSString  stringWithFormat:@"%@ %@", self.postArray.comments_count,NSLocalizedString(@"reply", @"")];
//        }else{
//            self.hReplies.text    =
//            [NSString  stringWithFormat:@"%@ %@",self.postArray.comments_count,NSLocalizedString(@"replies", @"")];
//        }
        
    }
    
    
    
    
}

- (void) setDataArray : (NSMutableArray*) dataArrayTemp {
    dataArray = dataArrayTemp;
}

- (void)setPageNum :(int) pageNumTemp {
    pageNum = pageNumTemp;
}

#pragma mark - uicollectionViewDelegates


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float returnValue;
    //    if(indexPath.row == 0){
    //        if(IS_IPAD){
    //            returnValue = 320;
    //        }else{
    //            returnValue = 220;
    //        }
    //    }
    //    else{
    //        if (IS_IPAD)
    //            returnValue = 260.0f;
    //        else if(IS_IPHONE_5)
    //            returnValue = 110.0f;
    //        else if(IS_IPHONE_6Plus){
    //            returnValue = 145.0;
    //        }
    //        else
    //            returnValue = 130.0f;
    
    //        UICollectionViewLayout *collectLayout = collectionViewLayout;
    
    
    
    returnValue = ((collectionView.frame.size.width-12)/3) ;
    //}
    
    
    //    if (indexPath.row == 0){
    //
    //        return CGSizeMake(collectionView.frame.size.width, returnValue);
    //
    //    }else{
    if(_sharedManager.isListView) {
        return CGSizeMake(commentsCollectionView.frame.size.width - 20, 250);
    }
    return CGSizeMake(returnValue, returnValue);
    
    //    }
    
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if(_sharedManager.isListView) {
        return UIEdgeInsetsMake(0, 5, 0, 0);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 6.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [dataArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"collectionCell";
    static NSString *CornerHeader = @"collectionHead";

    
    VideoViewCell *cell;
    
    currentIndex = indexPath.row;

    cell  = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];

    VideoModel *tempVideos = [[VideoModel alloc]init];
    tempVideos  = [dataArray objectAtIndex:indexPath.row];
    
    if(_sharedManager.isListView) {
        cell.CH_userName.font = [UIFont fontWithName:@"Montserrat-Regular" size:11];
        cell.CH_VideoTitle.font = [UIFont fontWithName:@"Montserrat-Regular" size:11];
        cell.durationLbl.hidden = NO;
        cell.durationLbl.text = tempVideos.video_length;
        cell.dotsImg.hidden = NO;
        
        
    } else {
        cell.CH_userName.font = [UIFont fontWithName:@"Montserrat-Regular" size:9];
        cell.CH_VideoTitle.frame = CGRectMake(cell.CH_VideoTitle.frame.origin.x, cell.CH_VideoTitle.frame.origin.y, cell.CH_VideoTitle.frame.size.width, cell.CH_VideoTitle.frame.size.height);
        cell.CH_VideoTitle.font = [UIFont fontWithName:@"Montserrat-Regular" size:9];
        cell.durationLbl.hidden = YES;
        cell.dotsImg.hidden = YES;
    }
    
    if(tempVideos.beamType == 1){
        if(_sharedManager.isListView) {
            cell.listRebeam.hidden = NO;
            cell.rebeam1.hidden = YES;
        } else {
            cell.listRebeam.hidden = YES;
            cell.rebeam1.hidden = NO;
        }
    }
    else{
        cell.listRebeam.hidden = YES;
        cell.rebeam1.hidden = YES;
    }
    
    if([tempVideos.isMute isEqualToString:@"1"])
    {
        cell.dummyLeftmute.hidden = NO;
    }
    else
    {
        cell.dummyLeftmute.hidden = YES;
    }

    NSString *decodedString = [Utils getDecryptedTextFor:tempVideos.userName];

    cell.CH_userName.text = [Utils decodeForEmojis:decodedString];

//    cell.CH_userName.text = [Utils decodeForEmojis:tempVideos.userName];
    if([cell.CH_userName.text isEqualToString:@"Anonymous"]) {
        cell.CH_userName.text = NSLocalizedString(@"anonymous_small", nil);
    }
    cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
    cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
    if([tempVideos.comments_count isEqualToString:@"0"])
    {
        cell.CH_CommentscountLbl.hidden = YES;
        cell.leftreplImg.hidden = YES;
    }
    else{
        cell.CH_CommentscountLbl.hidden = NO;
        cell.leftreplImg.hidden = NO;
        cell.CH_CommentscountLbl.text = tempVideos.comments_count;
    }
    
    cell.CH_heartCountlbl.text = tempVideos.like_count;
    cell.CH_seen.text = tempVideos.seen_count;
    cell.Ch_videoLength.text = tempVideos.video_length;
    if([tempVideos.is_anonymous  isEqualToString: @"0"]){
        cell.annonyOverlayLeft.hidden = YES;
        cell.annonyImgLeft.hidden = YES;
        //cell.CH_userName.text = @"Anonymous";
        cell.dummyLeft1.hidden  = YES;
    }
    else{
        //        cell.dummyLeft1.image = [UIImage imageNamed:@"btanonymous.png"];
        cell.annonyOverlayLeft.hidden = NO;
        cell.annonyImgLeft.hidden = NO;
//        cell.CH_userName.text = @"Anonymous";
        cell.dummyLeft1.hidden  = NO;
    }
    if(tempVideos.beamType == 1){
        cell.CH_heart.hidden = NO;
    }
    else if(tempVideos.beamType == 2){
        cell.CH_heart.hidden = YES;
        NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
    }
    else{
        cell.CH_heart.hidden = YES;
    }
    [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
    cell.CH_commentsBtn.enabled = YES;
    if (tempVideos.isLocal && !APP_DELEGATE.hasInet) {
        
        [cell.cellProgress removeFromSuperview];
        
        cell.CH_commentsBtn.backgroundColor = [UIColor colorWithRed:111/255 green:113/255 blue:121/255 alpha:1];
        cell.CH_commentsBtn.alpha = 0.55;
        cell.CH_VideoTitle.text = tempVideos.current_datetime;
        cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
        cell.CH_CommentscountLbl.text = @"!";
        cell.leftreplImg.hidden =  NO;
        cell.CH_commentsBtn.enabled = NO;
        cell.mainThumbnail.hidden = NO;
        //cell.mainThumbnail.image = [UIImage imageWithData:_offlineThumbnailArray[indexPath.row]];
        cell.mainThumbnail.image = [UIImage imageNamed:@"pH"];
        
        if (cell.cellProgress!=nil){
            
            cell.cellProgress.hidden = true;
            [cell.cellProgress removeFromSuperview];
            cell.cellProgress = nil;
        }
    }
    else if(tempVideos.isLocal){
 
        cell.CH_commentsBtn.enabled = NO;
        cell.leftreplImg.hidden =  YES;
        cell.CH_VideoTitle.text = tempVideos.current_datetime;
        cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
        
        if(![cell.cellProgress isDescendantOfView:cell.contentView]) {
//            if(IS_IPHONE_5 && !_sharedManager.isListView) {
//                cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, 95, 90)];
//            } else if(IS_IPAD && !_sharedManager.isListView){
//                cell.cellProgress = [[PWProgressView alloc] init];
//                cell.cellProgress.frame = cell.view1.frame;
//            } else if(IS_IPHONE_6Plus && !_sharedManager.isListView){
//                cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, 126, 121)];
//            }
//            else {
//                cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, cell.view1.frame.size.width, cell.view1.frame.size.height)];
//            }
//            if(!_sharedManager.isListView) {
//                cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 6.2f;
//            } else {
//                cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 14.2f;
//            }
//            cell.cellProgress.clipsToBounds = YES;
//
//            cell.cellProgress.hidden = false;
//            [cell.contentView addSubview:cell.cellProgress];
            
            if(!_sharedManager.isListView) {
                cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, ((commentsCollectionView.frame.size.width-12)/3) - 8, ((commentsCollectionView.frame.size.width-12)/3) - 16)];
                //                    }
                if(IS_IPHONE_6Plus) {
                    cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 7.0f;
                } else{
                    cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 6.2f;
                }
            } else {
                if(IS_IPHONE_6Plus) {
                    cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x-1.5, cell.view1.frame.origin.y-1.5, commentsCollectionView.frame.size.width - 26, 238)];
                    cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 17.0f;
                    
                } else {
                    cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x - 1.0, cell.view1.frame.origin.y, commentsCollectionView.frame.size.width - 28, 236)];
                    cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 14.2f;
                }
            }
            cell.cellProgress.clipsToBounds = YES;
            
            cell.cellProgress.hidden = false;
            [cell.contentView addSubview:cell.cellProgress];
        }
    }
    else{
        cell.CH_commentsBtn.enabled = YES;
        cell.CH_commentsBtn.backgroundColor = [UIColor clearColor];
        
        if (cell.cellProgress!=nil){
            
            cell.cellProgress.hidden = true;
            [cell.cellProgress removeFromSuperview];
            cell.cellProgress = nil;
        }
        
    }
    
    
    
    if(IS_IPAD)
       // cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /7.4f;
    cell.imgContainer.layer.masksToBounds = YES;
    if(_sharedManager.isListView) {
        
    } else {
//        [cell.CH_Video_Thumbnail roundCorners];
    }
    
    [cell.CH_playVideo addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    cell.userProfileView.tag = currentIndex;
    [cell.CH_heart setTag:currentIndex];
    [cell.CH_heart addTarget:self action:@selector(LikeHearts:) forControlEvents:UIControlEventTouchUpInside];
    if ([tempVideos.like_by_me isEqualToString:@"1"]) {
        [cell.CH_heart setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
    }else{
        [cell.CH_heart setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
    }
    
    cell.btnOptions.tag = indexPath.item;
    if (usersPost && [[[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] isEqualToString:tempVideos.user_id])
    {
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleMuteAndDelete:)];
        [cell.btnOptions addTarget:self action:@selector(btnOptionshandleMuteAndDelete:) forControlEvents:UIControlEventTouchUpInside];
        
        tempVideos.canDeleteComment = 1;
        lpgr.minimumPressDuration = 1.0;
        [cell.CH_commentsBtn addGestureRecognizer:lpgr];
        [lpgr.view setTag:currentIndex];
    }
    else if(usersPost && ![[[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] isEqualToString:tempVideos.user_id ]){
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPress:)];
        [cell.btnOptions addTarget:self action:@selector(btnOptionshandleLongPress:) forControlEvents:UIControlEventTouchUpInside];
        tempVideos.canDeleteComment = 2;
        lpgr.minimumPressDuration = 1.0;
        [cell.CH_commentsBtn addGestureRecognizer:lpgr];
        [lpgr.view setTag:currentIndex];
    }
    else if (!usersPost  && [[[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] isEqualToString:tempVideos.user_id])
    {
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handledelete:)];
        _optionType = 2;[cell.btnOptions addTarget:self action:@selector(btnOptionshandledelete:) forControlEvents:UIControlEventTouchUpInside];
        tempVideos.canDeleteComment = 3;
        lpgr.minimumPressDuration = 1.0;
        [cell.CH_commentsBtn addGestureRecognizer:lpgr];
        [lpgr.view setTag:currentIndex];
    } else {
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleCommentLongPress:)];
        [cell.btnOptions addTarget:self action:@selector(btnOptionshandleCommentLongPress:) forControlEvents:UIControlEventTouchUpInside];
        tempVideos.canDeleteComment = 2;
        lpgr.minimumPressDuration = 1.0;
        [cell.CH_commentsBtn addGestureRecognizer:lpgr];
        [lpgr.view setTag:currentIndex];
    }
    
    ///// GIF PLAYER /////
    if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
        
        cell.CH_Video_Thumbnail.hidden = YES;
        
        if (tempVideos.imageThumbnail && tempVideos.animatedImage) {
            cell.mainThumbnail.hidden = YES;
            [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
            cell.CH_Video_Thumbnail.hidden = NO;
        }else if (tempVideos.imageThumbnail && !tempVideos.animatedImage) {
            [cell.mainThumbnail setImage:tempVideos.imageThumbnail];
            [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                cell.mainThumbnail.hidden = NO;
                tempVideos.animatedImage = result.animatedImage;
                [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                cell.CH_Video_Thumbnail.hidden = NO;
            }];
        }else if (!tempVideos.imageThumbnail && tempVideos.animatedImage) {
            cell.mainThumbnail.hidden = YES;
            [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
            cell.CH_Video_Thumbnail.hidden = NO;
        }else if (!tempVideos.imageThumbnail && !tempVideos.animatedImage) {
            
            [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (!error) {
                    tempVideos.imageThumbnail = image;
                    [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                        tempVideos.animatedImage = result.animatedImage;
                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                        cell.mainThumbnail.hidden = NO;
                        cell.CH_Video_Thumbnail.hidden = NO;
                    }];
                }else{
                    [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                        tempVideos.animatedImage = result.animatedImage;
                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                        cell.mainThumbnail.hidden = NO;
                        cell.CH_Video_Thumbnail.hidden = NO;
                    }];
                }
            }];
            
            //                [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:@""] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            //
            //                    if (image && finished) {
            //
            //                    }
            //                }];
            
        }
        
        //            [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        //
        //            }];
        //            [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        
        //            [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
        //
        //                cell.mainThumbnail.hidden = YES;
        //
        //            }];
        
        //(^PINRemoteImageManagerImageCompletion)
        
        
    }else{
        
        
        cell.mainThumbnail.hidden = NO;
        cell.CH_Video_Thumbnail.hidden = YES;
        
        [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (!error) {
                tempVideos.imageThumbnail = image;
                //                    [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                //                        tempVideos.animatedImage = result.animatedImage;
                //                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                //                        cell.mainThumbnail.hidden = YES;
                //                        cell.CH_Video_Thumbnail.hidden = NO;
                //                    }];
            }else{
                
                NSLog(@"%@", error);
                
            }
            //else{
            //                    [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
            //                        tempVideos.animatedImage = result.animatedImage;
            //                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
            //                        cell.mainThumbnail.hidden = YES;
            //                        cell.CH_Video_Thumbnail.hidden = NO;
            //                    }];
            //                }
        }];
        
        //[cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        
        
        
    }

    ////// END /////
    [cell.CH_playVideo setTag:currentIndex];
    
    [cell.CH_flag setTag:currentIndex];
    cell.CH_commentsBtn.enabled = YES;
    cell.CH_commentsBtn.enabled = YES;
    [cell.CH_commentsBtn addTarget:self action:@selector(ReplyCommentpressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.CH_commentsBtn setTag:currentIndex];
    
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(!tempVideos.isLocal){
        
        cell.cellProgress.hidden = YES;
        
    }
    
    return cell;
    
    
    // }
    
    
    
    //return nil;
    
}

@end
