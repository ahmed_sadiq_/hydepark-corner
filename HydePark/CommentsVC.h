//
//  CommentsVC.h
//  HydePark
//
//  Created by Apple on 18/02/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentsModel.h"
#import "AppDelegate.h"
#import "AVFoundation/AVFoundation.h"
#import "AsyncImageView.h"
#import "VideoModel.h"
#import <MediaPlayer/MediaPlayer.h>
#import "WDUploadProgressView.h"
#import "PWProgressView.h"
#import "DataContainer.h"
@interface CommentsVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,AVAudioRecorderDelegate,MPMediaPickerControllerDelegate,WDUploadProgressDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    
    __weak IBOutlet UICollectionView *commentsCollectionView;
    AppDelegate *appDelegate;
    NSUInteger currentSelectedIndex;
    NSString *postID;
    NSString *ParentCommentID;
    CommentsModel *CommentsModelObj;
    VideoModel *videoModel;
    NSArray *CommentsArray;
    IBOutlet AsyncImageView *coverimgComments;
    IBOutlet AsyncImageView *UserImg;
    IBOutlet UILabel *Postusername;
    IBOutlet UILabel *videoLengthComments;
    IBOutlet UILabel *titleComments;
    IBOutlet UIView *cointerView;
    IBOutlet UILabel *likes;
    IBOutlet UILabel *views;
    IBOutlet UILabel *replies;
    IBOutlet UIButton *editButto;
    NSUInteger currentIndex;
    IBOutlet UIButton *likeBtn;
    NSMutableArray *videoObj;
    BOOL uploadBeamTag;
    BOOL uploadAnonymous;
    IBOutlet UIView *editView;
    NSString *video_duration;
    IBOutlet UIButton *closeBtnAudio;
    NSTimer *timerToupdateLbl;
    BOOL isRecording;
    NSTimer* audioTimeOut;
    IBOutlet UILabel *countDownlabel;
    IBOutlet UIImageView *audioBtnImage;
    int secondsLeft;
    NSString *secondsConsumed;
    int indexWithProgress;

    
    
    NSString *commentAllowed;
    NSString *privacySelected;
        NSString *textToshare;
    NSString *anony;
        NSString *timestamp;
    BOOL exist;
    
    UIGestureRecognizer *tapper;
    IBOutlet UIImageView *editImage;
    CGFloat tableHeight;
    IBOutlet UILabel *likeslbl;
    IBOutlet UILabel *viewslbl;
    IBOutlet UILabel *replieslbl;
    UIActivityIndicatorView *activityIndicator;
    BOOL usersPost;
    BOOL isNewlyVideoAdded;
    NSMutableArray *dataArray;
    int pageNum;
    BOOL cannotScrollMore;
    BOOL serverCall;
    IBOutlet UILabel *anonyLbl;
    UIView * footerView;
    NSInteger orientationAngle;
}
@property (nonatomic) BOOL isAnonymous;
@property (strong, nonatomic) DataContainer *sharedManager;
@property (assign) int isThumbnailReq;
@property (nonatomic, assign) BOOL isDownwards;
@property (strong, nonatomic) NSData *CmovieData;
@property (strong, nonatomic) NSData *profileData; // for Thumbnail selected
@property (strong, nonatomic) NSData *CaudioData;
@property (strong, nonatomic) WDUploadProgressView *progressView;
@property (strong, nonatomic) CommentsModel *commentsObj;
@property (strong, nonatomic) VideoModel *postArray;
@property (strong, nonatomic) NSString *cPostId;
@property (nonatomic) BOOL isComment;
@property (assign)    BOOL isFirstComment;
@property (assign)    BOOL isFromDeepLink;
@property (strong, nonatomic) NSString *pID;
@property (weak, nonatomic) IBOutlet UIButton *userChannelBtn;
@property (weak, nonatomic) IBOutlet UIButton *audioRecordBtn;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) IBOutlet UIView *uploadAudioView;
@property (weak, nonatomic) IBOutlet UILabel *reportThisbeamlbl;
@property (weak, nonatomic) IBOutlet UILabel *blockThispersonlbl;
@property (weak, nonatomic) IBOutlet UIButton *report;
@property (weak, nonatomic) IBOutlet UIButton *block;
@property (weak, nonatomic) IBOutlet UIImageView *flagImg;
@property (weak, nonatomic) IBOutlet UIImageView *blockImg;
@property (weak, nonatomic) IBOutlet UIView *viewToRound;
@property (weak, nonatomic) IBOutlet UIView *muteDialog;
@property (weak, nonatomic) IBOutlet UIImageView *muteImage;
@property (weak, nonatomic) IBOutlet UIImageView *anonyImage;
@property (weak, nonatomic) IBOutlet UIView *sharePersonalComment;
@property (weak, nonatomic) IBOutlet UILabel *timelabel;
@property (weak, nonatomic) IBOutlet UIView  *muteAndDelete;
@property (weak, nonatomic) IBOutlet UIButton *muteAndDeleteBtn;
@property (weak, nonatomic) IBOutlet UILabel  *muteLbl;
@property (weak, nonatomic) IBOutlet UIButton *getLikes;
@property (weak, nonatomic) IBOutlet UIButton *mainPlBtn;
@property (weak, nonatomic) IBOutlet UITableView *commentsTable;
@property (weak, nonatomic) IBOutlet UIView *bottomBarView;
@property (weak, nonatomic) IBOutlet UIView  *headerView;

@property (strong, nonatomic) UIImage *thumbnailToUpload;
@property (strong, nonatomic) UIImage *thumbnailToUpload2;
@property (strong, nonatomic) UIImage *thumbnailToUpload3;


@property (assign) int canDeleteCOmment;
- (NSString *)getParentPostID;
- (NSMutableArray *)GetCommentsArray;
- (void) setDataArray : (NSMutableArray*) dataArrayTemp;
- (IBAction)getLikesPressed:(id)sender;
- (IBAction)backToMyCorner:(id)sender;
- (IBAction)editBtnPressed:(id)sender;
- (IBAction)likeComment:(id)sender;
- (IBAction)back:(id)sender;
- (IBAction)MainPlayBtn:(id)sender;
- (IBAction)CancelEditBtn:(id)sender;
- (IBAction)ReportBtn:(id)sender;
- (IBAction)BlockPerson:(id)sender;
- (IBAction)beamPressed:(id)sender;
- (IBAction)RecorderPressed:(id)sender;
- (IBAction)AudioClosePressed:(id)sender;
- (void)updateCommentsArray:(CommentsModel*)cObj;
- (void)setPageNum :(int) pageNumTemp;
- (void)getCommentsOnPost;
@property (nonatomic, strong) PWProgressView *roundedPView;
-(void)addLocalObjectToCommentsArray:(VideoModel *)VObj;

@property (weak, nonatomic) IBOutlet UIView *potraitDiscriptionView;
@property (weak, nonatomic) IBOutlet UIView *tabBarBgView;
@property (weak, nonatomic) IBOutlet UIView *landscapeDiscriptionView;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *landscapeDesc;
@property (weak, nonatomic) IBOutlet UILabel *repliesHeader;
@property (weak, nonatomic) IBOutlet UILabel *hLikes;
@property (weak, nonatomic) IBOutlet UILabel *hViews;
@property (weak, nonatomic) IBOutlet UILabel *hReplies;
@property (weak, nonatomic) IBOutlet UIButton *fullScreenBtn;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UIButton *hGetLikesbtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@property (weak, nonatomic) IBOutlet UIImageView *hLikeBTn;
@property (weak, nonatomic) IBOutlet UIButton *hEditBtn;
@property (weak, nonatomic) IBOutlet UIImageView *anonyNewImg;
@property (weak, nonatomic) IBOutlet UIView *darkDescView;
@property (weak, nonatomic) IBOutlet UIButton *strechBtn;
@property (weak, nonatomic) IBOutlet UIButton *gotoParentPostBtn;
@property (strong, nonatomic) NSString *parentRootId;
@property (strong, nonatomic) VideoModel *parentModel;
@property (assign, nonatomic) BOOL needToRefreshMainPost;
@property (weak, nonatomic) IBOutlet UIView *moveToParentHeader;
@property (weak, nonatomic) IBOutlet UILabel *replyToSomeonelbl;
@property (weak, nonatomic) IBOutlet UIImageView *pauseBtn;
@property (weak, nonatomic) IBOutlet UIView *childContainer;
@property (weak, nonatomic) IBOutlet UIImageView *rebeam;
@property (weak, nonatomic) IBOutlet UIButton *playnstopBtn;
@property (weak, nonatomic) IBOutlet UIView *topBlackView;
@property (weak, nonatomic) IBOutlet UIButton *beamBtn;
@property (weak, nonatomic) IBOutlet UILabel *beamLbl;
@property (weak, nonatomic) IBOutlet UIButton *audioBtn;
@property (weak, nonatomic) IBOutlet UIButton *anonymousBtn;
@property (weak, nonatomic) IBOutlet UILabel *audioLbl;
@property (weak, nonatomic) IBOutlet UILabel *anonymousLbl;

@property (strong, nonatomic) NSMutableArray *offlineThumbnailArray;

@property (assign, nonatomic) BOOL playnstopCheck;
@property (assign, nonatomic) int optionType;
@end
