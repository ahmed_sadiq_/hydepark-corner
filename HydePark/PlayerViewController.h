//
//  PlayerViewController.h
//  HydePark
//
//  Created by Osama on 28/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoModel.h"
#import "AppDelegate.h"
#import <AVKit/AVKit.h>
#import "AVFoundation/AVFoundation.h"

@interface PlayerViewController : UIViewController<AVAudioRecorderDelegate>{
    AppDelegate *appDelegate;
    NSMutableDictionary *serviceParams;
    NSMutableArray *dataArray;
    NSUInteger currentSelectedIndex;
    BOOL usersPost;
    NSString *postID;

    BOOL uploadBeamTag;
    BOOL uploadAnonymous;
    NSString *video_duration;
    NSInteger orientationAngle;
    IBOutlet UIButton *closeBtnAudio;
    IBOutlet UILabel *countDownlabel;
    IBOutlet UIImageView *audioBtnImage;
    BOOL isRecording;
    NSTimer *timerToupdateLbl;
    NSTimer* audioTimeOut;
    int secondsLeft;
    NSString *secondsConsumed;
    IBOutlet UISlider *mySlider;
    IBOutlet UIView *mySliderBackground;
    NSTimer *timer;
    AVPlayer* playVideo;
    AVPlayerItem* playerItem;
    AVAsset *asset;
    NSTimer *timer2;
    BOOL shudClearPlayer;
    BOOL shudContinuePlayer;
}
@property (strong, nonatomic) AVPlayerViewController *playerViewController;

@property (assign) int isThumbnailReq;
@property (strong, nonatomic) VideoModel *vModel;
@property (nonatomic) BOOL isComment;
@property (assign) BOOL isfromChat;
@property (strong, nonatomic) NSString *cPostId;
@property (weak, nonatomic) IBOutlet UILabel *timeStamp;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *commentsCount;
@property (weak, nonatomic) IBOutlet UILabel *likeCount;
@property (weak, nonatomic) IBOutlet UIView *vHolder;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UIImageView *pauseBtn;
@property (weak, nonatomic) IBOutlet UIImageView *rebeamImage;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UIImageView *dots;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;


@property (weak, nonatomic) IBOutlet UIView *tabBarBgView;
@property (weak, nonatomic) IBOutlet UIButton *audioBtn;
@property (weak, nonatomic) IBOutlet UIButton *anonymousBtn;
@property (weak, nonatomic) IBOutlet UILabel *beamLbl;
@property (weak, nonatomic) IBOutlet UILabel *anonymousLbl;
@property (weak, nonatomic) IBOutlet UILabel *audioLbl;
@property (strong, nonatomic) VideoModel *postArray;
@property (strong, nonatomic) NSURL *videoPath;
@property (strong, nonatomic) UIImage *thumbnailToUpload;
@property (strong, nonatomic) UIImage *thumbnailToUpload2;
@property (strong, nonatomic) UIImage *thumbnailToUpload3;
@property (strong, nonatomic) NSData *CmovieData;
@property (strong, nonatomic) NSData *profileData;
@property (assign)    BOOL isFromDeepLink;
@property (assign)    BOOL isFirstComment;
@property (strong, nonatomic) NSString *pID;
@property (strong, nonatomic) IBOutlet UIView *uploadAudioView;
@property (weak, nonatomic) IBOutlet UIButton *audioRecordBtn;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) NSData *CaudioData;




-(IBAction)commentsPressed:(id)sender;
-(IBAction)likesPressed:(id)sender;
@end
