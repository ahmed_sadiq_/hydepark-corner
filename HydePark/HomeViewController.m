//
//  HomeViewController.m
//  HydePark
//
//  Created by Osama on 29/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "HomeViewController.h"
#import "FriendsVC.h"
#import "UIBarButtonItem+Badge.h"
#import "UIColor+Style.h"
#import "AVFoundation/AVFoundation.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "BeamUploadVC.h"
#import "AudioVC.h"
#import "Utils.h"
#import "UIImage+JS.h"
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <HBRecorder/HBRecorder.h>
#import <Photos/Photos.h>
#import "DBCommunicator.h"
#import "AppDelegate.h"


@interface HomeViewController ()<UIGestureRecognizerDelegate,HBRecorderProtocol>{
    BeamUploadVC *beamUpload;
    
}
@property (strong, nonatomic) NSURL *videoPath;
@end

@implementation HomeViewController
@synthesize CmovieData,profileData;
- (id) initExampleViewController {
    //Create list viewcontrollers
    self.speakersVC = [[SpeakersCornerVC alloc] init];
    self.myCornerVC  = [[MyCornerVC alloc] init];
    self.friendsVC = [[FriendsCornerVC alloc] init];
    
    NSArray *viewControllers = @[self.myCornerVC,self.speakersVC,self.friendsVC];
    //Segment titles
    NSArray *segmentTitles = @[NSLocalizedString(@"my_corner", @""), NSLocalizedString(@"speakers_corner", @""),NSLocalizedString(@"friends_corner", @"")];
    //Init
    self = [super initWithListViewController:viewControllers segmentTitle:segmentTitles];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    sharedManager = [DataContainer sharedManager];
    if(IS_IPAD){
        CGRect newFrame = CGRectMake( self.tbView.frame.origin.x, self.tbView.frame.origin.y-40, self.tbView.frame.size.width, 100);
        _tbView.frame = newFrame;
    }
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.tbView.delegate = self;
    [self.view bringSubviewToFront:self.tbView];
    beamUpload = [[BeamUploadVC alloc]init];
    NSString *deviceTokens = [AppDelegate getDeviceToken];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled  = YES;
    self.navigationController.interactivePopGestureRecognizer.cancelsTouchesInView = NO;
    if(deviceTokens.length > 0){
        [self sendDeviceToken];
    }
    [self getnotificationsCount];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showLastVideo" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLastVideo:) name:@"showLastVideo" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"goLiveNow" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goLiveNow:) name:@"goLiveNow" object:nil];
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
//{
//    return YES;
//}

-(void)sendDeviceToken{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *deviceTokens = [AppDelegate getDeviceToken];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:@"registerDevice" forKey:@"method"];
    [dict setValue:deviceTokens forKey:@"device_id"];
    [dict setValue:token forKey:@"Session_token"];
    [dict setValue:@"IOS" forKey:@"device_type"];
    [dict setValue:@"notAndroid" forKey:@"gcm_reg_id"];
    NSData *postData = [Utils encodeDictionary:dict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//            NSLog(@"%@",result);
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled  = YES;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self setNavButtons];
    
}

#pragma mark NavBarActions
- (void)updateNotificationCount:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@"plusNotification"])
    {
        [self getnotificationsCount];
    }
    else if([[notification name] isEqualToString:@"decrementNoti"]){
        [self getnotificationsCount];
    }
    else if([[notification name] isEqualToString:@"hideNotification"]){
        self.navigationItem.leftBarButtonItem.badgeValue = @"0";
    }
}
-(void)setNavButtons{
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotificationCount:) name:@"hideNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotificationCount:) name:@"plusNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotificationCount:) name:@"decrementNoti" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(VideofromGallery:)
                                                 name:@"UploadFromGallery"
                                               object:nil];
    //Left button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 22, 22);
    
    //MENU BUTTON SIZE ISSUE FIX FOR IOS 11
    if (@available(iOS 9, *)) {
        [button.widthAnchor constraintEqualToConstant: 22].active = YES;
        [button.heightAnchor constraintEqualToConstant: 22].active = YES;
    }
    
    [button setImage:[UIImage imageNamed:@"newestmenu.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(presentLeftMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButton=[[UIBarButtonItem alloc] init];
    [barButton setCustomView:button];
    self.navigationItem.leftBarButtonItem=barButton;
    self.navigationItem.leftBarButtonItem.badgeValue = @"0";
    self.navigationItem.leftBarButtonItem.badgeBGColor = [UIColor redColor];
    self.navigationItem.leftBarButtonItem.badgeFont =[UIFont fontWithName:@"Montserrat" size:12.0];
    self.navigationItem.leftBarButtonItem.badgeOriginX = 10.0f;
    //Middle Button
    UIView *buttonContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    buttonContainer.backgroundColor = [UIColor clearColor];
    UIButton *findCornerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [findCornerBtn setFrame:CGRectMake(0, 0, 200, 44)];
    [findCornerBtn setTitle:NSLocalizedString(@"find_other_corners", @"") forState:UIControlStateNormal];
    [findCornerBtn.titleLabel setFont:[UIFont fontWithName:@"Montserrat" size:16.0]];
    [findCornerBtn addTarget:self action:@selector(findCornerBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [findCornerBtn setShowsTouchWhenHighlighted:YES];
    [buttonContainer addSubview:findCornerBtn];
    self.navigationItem.titleView = buttonContainer;
    //Right bar Button
    UIButton *right = [UIButton buttonWithType:UIButtonTypeCustom];
    right.frame = CGRectMake(0, 0, 20, 20);
    [right setImage:[UIImage imageNamed:@"searchbold.png"] forState:UIControlStateNormal];
    [right addTarget:self action:@selector(findCornerBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtons=[[UIBarButtonItem alloc] init];
    [barButtons setCustomView:right];
    self.navigationItem.rightBarButtonItem =barButtons;
}

#pragma mark GET NOTIFICATIONS COUNT
-(void) getnotificationsCount {
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"getNotificationsCount",@"method",
                              token,@"session_token",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                NSString *totalNotifcations = [result objectForKey:@"total_notifications"];
                NSString *unseenCount       = [result objectForKey:@"unseen_count"];
                appDelegate.unseenCount      = [unseenCount integerValue];
                appDelegate.totalNotiCount   = [totalNotifcations integerValue];
                NSInteger unSeenC = [unseenCount integerValue];
                if(appDelegate.unseenCount > 0)
                {
                    self.navigationItem.leftBarButtonItem.badgeValue = [Utils abbreviateNumber:unseenCount withDecimal:1];
                    [[NSUserDefaults standardUserDefaults] setInteger:unSeenC forKey:@"LastNotificationCount"];
                }
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"showInSideBar"
                 object:nil];
            }
        }
    }];
}

#pragma mark UPLOAD FROM GALLERY
- (void)VideofromGallery:(NSNotification *) notification{
    uploadAnonymous = false;
    [[NSUserDefaults standardUserDefaults] synchronize];
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:imagePicker.sourceType];
    imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
    //imagePicker.videoMaximumDuration = 60; // duration in seconds
    imagePicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:NULL];
}


-(IBAction)findCornerBtnAction:(id)sender{
    FriendsVC *commentController; //    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
    
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    
    commentController.friendsArray  = nil;
    commentController.titles        = NSLocalizedString(@"action_search", nil);
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 4;
    commentController.userId        = @"";
    commentController.shudFetchFromServerOnly = true;
    commentController.isSearch = true;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(void)tabbarBtnClicked:(id)sender{
    UIButton *btn = (UIButton *)sender;
    if(btn.tag == 100 || btn.tag == 101){
        [self beamPressed:sender];
    }
    else if(btn.tag == 3){
        [self moveToaudiovc];
    }
}

-(void)moveToaudiovc{
    AudioVC *avc = [[AudioVC alloc] initWithNibName:@"AudioVC" bundle:nil];
    [[self navigationController] pushViewController:avc animated:YES];
}

#pragma mark - Beam Pressed
- (IBAction)beamPressed:(id)sender {
    if([sender tag] == 100){
        uploadAnonymous = true;
        uploadBeamTag = false;
    }
    else if([sender tag ] == 101)
    {
        uploadAnonymous = false;
        uploadBeamTag = true;
    }
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        NSBundle *bundle = [NSBundle bundleForClass:HBRecorder.class];
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HBRecorder.bundle/HBRecorder" bundle:bundle];
        
        HBRecorder *recorder = [sb instantiateViewControllerWithIdentifier:@"HBRecorder"];
        recorder.delegate = self;
        if(uploadAnonymous) {
            UIImageView *anonymousMark = [[UIImageView alloc] initWithFrame:CGRectMake(10, 64, recorder.view.frame.size.width - 20, recorder.view.frame.size.height - 150)];
            anonymousMark.image = [UIImage imageNamed:@"ano_new_icon"];
            anonymousMark.alpha = 0.5;
            [recorder.view addSubview:anonymousMark];
        }
        recorder.topTitle = @"";
        recorder.bottomTitle = @"";
        recorder.maxRecordDuration = 60 * 1;
        recorder.movieName = @"MyAnimatedMovie";
        
        
        recorder.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self.navigationController pushViewController:recorder animated:YES];
        
        //        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        //        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        //        picker.delegate = self;
        //        picker.allowsEditing = NO;
        //        NSArray *mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
        //
        //        picker.mediaTypes = mediaTypes;
        //        picker.videoQuality = UIImagePickerControllerQualityTypeMedium;
        //        picker.videoMaximumDuration = 60;
        //        picker.extendedLayoutIncludesOpaqueBars = YES;
        //        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"I'm afraid there's no camera on this device!" delegate:nil cancelButtonTitle:@"Dang!" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

#pragma mark - HBRECORDER Recording Methods

- (void)recorder:(HBRecorder *)recorder  didFinishPickingMediaWithUrl:(NSURL *)videoUrl {
    
    _videoPath = videoUrl;
    _url = videoUrl;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        AVAsset *asset = [AVAsset assetWithURL:_videoPath];// url= give your url video here
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        
        
   
        imageGenerator.appliesPreferredTrackTransform = YES;
        
        CMTime CMduration = asset.duration;
        int totalSeconds = CMTimeGetSeconds(CMduration);
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;
        
        CMTime time = CMTimeMake(1, 10);//it will create the thumbnail after the 0.1 sec of video
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
        self.thumbnailToUpload = thumbnail;
        int i = 0;
        
        if(i == 0) {
            AVAsset *asset = [AVAsset assetWithURL:videoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime thumbTime = CMTimeMake(10,5);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTime actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
            self.thumbnailToUpload2 = thumbnail;
            
            i++;
        }
        
        if(i == 1) {
            AVAsset *asset = [AVAsset assetWithURL:videoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime thumbTimes = CMTimeMake(10,1);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTimes actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
            self.thumbnailToUpload3 = thumbnail;
        }
        
        sharedManager.thumbToUpload = thumbnail;
        profileData = UIImagePNGRepresentation(thumbnail);
        CmovieData = [NSData dataWithContentsOfURL:videoUrl];
        
        
        NSString *duration = @"";
        if (hours>0) {
            NSString *hoursString = [NSString stringWithFormat:@"%d:",hours];
            duration = [duration stringByAppendingString:hoursString];
        }
        if (minutes>0) {
            NSString *minString = [NSString stringWithFormat:@"%02d:",minutes];
            duration = [duration stringByAppendingString:minString];
        }
        if (seconds>0) {
            NSString *secString = [NSString stringWithFormat:@"%02d:",seconds];
            duration = [duration stringByAppendingString:secString];
        }
        video_duration = [[NSString alloc]initWithFormat:@"%02lu:%02lu",(unsigned long)minutes,(unsigned long)seconds];
        dispatch_async(dispatch_get_main_queue(), ^{
            isFromGallery = NO;
            [self movetoUploadBeamController];
        });
    });
    
    
}

- (void)recorderDidCancel:(HBRecorder *)recorder {
//    NSLog(@"Recorder did cancel..");
}

-(void)recorderOrientation:(NSInteger)orientation {
    orientationAngle = orientation;
}

#pragma mark - Delegate Methods
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    // user hit cancel
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSURL *chosenMovie = [info objectForKey:UIImagePickerControllerMediaURL];
    _url = [NSString stringWithFormat:@"%@",chosenMovie];
    _videoPath = [NSURL URLWithString:_url];
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:chosenMovie options:nil];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    
    imageGenerator.appliesPreferredTrackTransform = YES;
    NSTimeInterval durationInSeconds = 0.00;
    if (asset){
        durationInSeconds = CMTimeGetSeconds(asset.duration);
    }

    NSUInteger dTotalSeconds = durationInSeconds;

    NSUInteger dSeconds =(dTotalSeconds  % 60);
    NSUInteger dMinutes = (dTotalSeconds / 60 ) % 60;

    video_duration = [[NSString alloc]initWithFormat:@"%02lu:%02lu",(unsigned long)dMinutes,(unsigned long)dSeconds];
    CmovieData = [NSData dataWithContentsOfURL:chosenMovie];
        //CmovieData = [NSData dataWithContentsOfURL:mp4Url];
//    CmovieData=[NSData dataWithContentsOfURL:[NSURL fileURLWithPath:videoPath]];


    [self dismissViewControllerAnimated:YES completion:nil];

    CMTime time = CMTimeMake(1, 10);//it will create the thumbnail after the 0.1 sec of video
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
    self.thumbnailToUpload = thumbnail;
    int i = 0;

    if(i == 0) {
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        imageGenerator.appliesPreferredTrackTransform = YES;

        CMTime thumbTime = CMTimeMake(10,5);
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTime actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
        self.thumbnailToUpload2 = thumbnail;

        i++;
    }

    if(i == 1) {
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        imageGenerator.appliesPreferredTrackTransform = YES;

        CMTime thumbTimes = CMTimeMake(10,1);
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTimes actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
        self.thumbnailToUpload3 = thumbnail;
    }


    sharedManager.thumbToUpload = thumbnail;
    self.thumbnailToUpload = thumbnail;
    profileData = UIImagePNGRepresentation(thumbnail);
    isFromGallery = YES;
    [self movetoUploadBeamController];
}

-(long)getRandomNumberBetween:(long)from to:(long)to {
    
    return (long)from + arc4random() % (to-from+1);
}

-(void) movetoUploadBeamController{
  
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.dataToUpload = CmovieData;
    uploadController.videoPath = _videoPath;
    uploadController.video_duration = video_duration;
    uploadController.ParentCommentID = @"-1";
    uploadController.postID = @"-1";
    uploadController.isAudio = false;
    uploadController.profileData = profileData;
    uploadController.thumbnailImage = self.thumbnailToUpload;
    uploadController.thumbnailImage2 = self.thumbnailToUpload2;
    uploadController.thumbnailImage3 = self.thumbnailToUpload3;
    uploadController.orientationAngle = orientationAngle;
    uploadController.url = _url;
    if(isFromGallery) {
        uploadController.isFromGallery = YES;
    } else {
        uploadController.isFromGallery = NO;
    }
    if(uploadAnonymous)
        uploadController.isAnonymous = true;
    else
        uploadController.isAnonymous = false;
    [[self navigationController] pushViewController:uploadController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadLast{
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:fetchOptions];
    PHAsset *lastAsset = [fetchResult lastObject];
    
    [[PHImageManager defaultManager] requestAVAssetForVideo:lastAsset options:nil resultHandler:^(AVAsset *  asset, AVAudioMix *  audioMix, NSDictionary *  info) {
        
        //let asset = asset as! AVURLAsset

        AVURLAsset *urlAsset = (AVURLAsset*)asset;
  

        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        
        imageGenerator.appliesPreferredTrackTransform = YES;
        
        CMTime CMduration = asset.duration;
        int totalSeconds = CMTimeGetSeconds(CMduration);
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;
        
        CMTime time = CMTimeMake(1, 10);//it will create the thumbnail after the 0.1 sec of video
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
        self.thumbnailToUpload = thumbnail;
        int i = 0;
        
        if(i == 0) {
            //AVAsset *asset = [AVAsset assetWithURL:videoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime thumbTime = CMTimeMake(10,5);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTime actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
            self.thumbnailToUpload2 = thumbnail;
            
            i++;
        }
        
        if(i == 1) {
            //AVAsset *asset = [AVAsset assetWithURL:videoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime thumbTimes = CMTimeMake(10,1);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTimes actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
            self.thumbnailToUpload3 = thumbnail;
        }
        
        sharedManager.thumbToUpload = thumbnail;
        profileData = UIImagePNGRepresentation(thumbnail);
        
        
        CmovieData = [NSData dataWithContentsOfURL:urlAsset.URL];
        
        
        NSString *duration = @"";
        if (hours>0) {
            NSString *hoursString = [NSString stringWithFormat:@"%d:",hours];
            duration = [duration stringByAppendingString:hoursString];
        }
        if (minutes>0) {
            NSString *minString = [NSString stringWithFormat:@"%02d:",minutes];
            duration = [duration stringByAppendingString:minString];
        }
        if (seconds>0) {
            NSString *secString = [NSString stringWithFormat:@"%02d:",seconds];
            duration = [duration stringByAppendingString:secString];
        }
        video_duration = [[NSString alloc]initWithFormat:@"%02lu:%02lu",(unsigned long)minutes,(unsigned long)seconds];
        dispatch_async(dispatch_get_main_queue(), ^{
            isFromGallery = NO;
            [self movetoUploadBeamController];
        });
        
        
        
    }];
    
    
    
}


- (void)showLastVideo:(NSNotification *)notification
{
    [self loadLast];
}

- (void)goLiveNow:(NSNotification *)notification
{
//    StreamViewController *streamController = [[StreamViewController alloc] initWithNibName:@"StreamViewController" bundle:nil];
//
//    streamController.delegate = self;
//    //[self presentViewController:streamController animated:YES completion:nil];
//
//    [[self navigationController] pushViewController:streamController animated:YES];
    
}

- (void)saveStream{


//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:@"showLastVideo"
//     object:nil];
    [self loadLast];
    
}

@end
