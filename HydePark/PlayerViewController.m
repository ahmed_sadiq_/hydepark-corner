//
//  PlayerViewController.m
//  HydePark
//
//  Created by Osama on 28/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "PlayerViewController.h"
#import "Utils.h"
#import "Constants.h"
#import "PBJVideoPlayerController.h"
#import "UIImageView+WebCache.h"
#import "UserChannel.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "BeamUploadVC.h"
#import "CustomLoading.h"
#import "UIImage+JS.h"
#import "ApiManager.h"
#import "FriendsVC.h"
#import "NSData+AES.h"
#import <HBRecorder.h>
#import "AVPlayerItem+MCCacheSupport.h"

@interface PlayerViewController ()<HBRecorderProtocol,PBJVideoPlayerControllerDelegate>{
    PBJVideoPlayerController *_videoPlayerController;
    UIActivityIndicatorView *activityIndicator;
    float duration;
}

@property (strong, nonatomic) NSDateComponentsFormatter *dateComponentsFormatter;
@property (strong, nonatomic) NSString *totaltime;
@end

@implementation PlayerViewController
@synthesize postArray,profileData,CmovieData,isFirstComment,pID,CaudioData;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.dateComponentsFormatter = [[NSDateComponentsFormatter alloc] init];
    self.dateComponentsFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
    self.dateComponentsFormatter.allowedUnits = (NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond);

    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    shudClearPlayer = false;
    mySliderBackground.hidden = true;
    shudContinuePlayer = true;
    if(isFirstComment){
        pID = @"-1";
    }
    secondsLeft = 60;
    [self setAudioRecordSettings];
    _uploadAudioView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    [self.view addSubview:_uploadAudioView];
    _uploadAudioView.hidden = YES;
    
    [appDelegate.parentIdStackController pushObject:postArray.videoID];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillBeomceActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearPlayingMedia) name:@"stopAllPlayingMedia" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopPlayer) name:@"stopPlayer" object:nil];

    
//    NSString *urlToStream;
//    if(self.vModel.m3u8_video_link.length > 1) {
//        urlToStream = self.vModel.m3u8_video_link;
//    }
//    else {
//        urlToStream = self.vModel.video_link;
//    }
//
//    NSURL *bipbopUrl = [[NSURL alloc] initWithString:urlToStream];
//    asset = [AVAsset assetWithURL:bipbopUrl];
//    playerItem = [[AVPlayerItem alloc] initWithAsset:asset];
//    playVideo = [[AVPlayer alloc] initWithPlayerItem:playerItem];
//    playVideo.automaticallyWaitsToMinimizeStalling = false;
//    NSError *error;
//    [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker
//                                                       error:&error];
//    if(error)
//    {
//        NSLog(@"Error: AudioSession cannot use speakers");
//    }
//
//    _playerViewController = [[AVPlayerViewController alloc] init];
//    _playerViewController.player = playVideo;
//    _playerViewController.showsPlaybackControls = YES;
//    playVideo.actionAtItemEnd = AVPlayerActionAtItemEndNone;
//    _playerViewController.view.frame = CGRectMake(_thumbnail.frame.origin.x, _thumbnail.frame.origin.y, _thumbnail.frame.size.width, _thumbnail.frame.size.height - 50); //self.view.bounds;
//    [self.view addSubview:_playerViewController.view];
//    [self.view bringSubviewToFront:_playerViewController.view];
//    [playVideo playImmediatelyAtRate:0.5];
//
//
//
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(playerItemDidReachEnd:)
//                                                 name:AVPlayerItemDidPlayToEndTimeNotification
//                                               object:[playVideo currentItem]];
//
//    [self setVideoPlayerState];

    
    
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            
            _tabBarBgView.alpha = 0.4;
            _tabBarBgView.backgroundColor = [UIColor lightGrayColor];
            
            self.background.hidden = true;
            _userName.textColor = [UIColor blackColor];
            _videoTitle.textColor = [UIColor blackColor];
            _timeStamp.textColor = [UIColor blackColor];
            _likeCount.textColor = [UIColor blackColor];
            _commentsCount.textColor = [UIColor blackColor];

            UIImage *newImage2 = [_commentBtn.currentBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIGraphicsBeginImageContextWithOptions(_commentBtn.currentBackgroundImage.size, NO, newImage2.scale);
            [[UIColor blackColor] set];
            [newImage2 drawInRect:CGRectMake(0, 0, _commentBtn.currentBackgroundImage.size.width, newImage2.size.height)];
            newImage2 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [_commentBtn setBackgroundImage:newImage2 forState:UIControlStateNormal];

//            UIImage *newImage3 = [_likeBtn.currentBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//            UIGraphicsBeginImageContextWithOptions(_likeBtn.currentBackgroundImage.size, NO, newImage3.scale);
//            [[UIColor blackColor] set];
//            [newImage3 drawInRect:CGRectMake(0, 0, _likeBtn.currentBackgroundImage.size.width, newImage3.size.height)];
//            newImage3 = UIGraphicsGetImageFromCurrentImageContext();
//            UIGraphicsEndImageContext();
//            [_likeBtn setBackgroundImage:newImage3 forState:UIControlStateNormal];
            [_likeBtn setBackgroundImage:[UIImage imageNamed:@"like_nightmode.png"] forState:UIControlStateNormal];

            UIImage *newImage4 = [_dots.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIGraphicsBeginImageContextWithOptions(_dots.image.size, NO, newImage4.scale);
            [[UIColor blackColor] set];
            [newImage4 drawInRect:CGRectMake(0, 0,_dots.image.size.width, newImage4.size.height)];
            newImage4 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            _dots.image = newImage4;
        }
    }
}

-(void)stopPlayer
{
    shudContinuePlayer = false;
}
-(void)clearPlayingMedia
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
//    [playVideo pause];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [playVideo pause];
    playVideo = nil;
    [timer invalidate];
    dispatch_async(dispatch_get_main_queue(), ^{
        [timer2 invalidate];
        timer2 = nil;
    });
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
//    AVPlayerItem *p = [notification object];
//    [p seekToTime:kCMTimeZero];
    [playVideo.currentItem seekToTime:kCMTimeZero];
    [playVideo play];
}

-(void)appWillResignActive:(NSNotification*)note
{
    [playVideo pause];
}

-(void)appWillBeomceActive:(NSNotification*)note
{
//    [playVideo performSelector:@selector(play) withObject:nil afterDelay:1];

    
//    if(_videoPlayerController.playbackState == PBJVideoPlayerPlaybackStatePaused)
//    {
//        NSLog(@"from current 2");
//        [_videoPlayerController playFromCurrentTime];
//    }
//    else
//    {
//        NSLog(@"issue found");
//    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    
    if(playVideo && shudContinuePlayer)
    {
        [playVideo performSelector:@selector(play) withObject:nil afterDelay:1];
    }
    
    if(!self.thumbnail.hidden)
    {
        [self setVideoPlayerState];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnUserChannel:(id)sender {
    
    UserChannel *commentController ;
    if(IS_IPAD){
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_iPad" bundle:nil];
    }else if (IS_IPHONEX){
        commentController = [[UserChannel alloc] initWithNibName:@"UserChannel_IphoneX" bundle:nil];
    }
    else{
        commentController  = [[UserChannel alloc] initWithNibName:@"UserChannel" bundle:nil];
        
    }
    commentController.ChannelObj = nil;
    
    if([self.vModel.is_anonymous isEqualToString:@"0"])
    {
        Followings *fData  = [[Followings alloc] init];
        BOOL isCl= [_vModel.isCelebrity boolValue];
        fData.is_celeb = [NSString stringWithFormat:@"%d",isCl];
        commentController.friendID   = _vModel.user_id;
        commentController.currentUser = fData;
        [self.navigationController pushViewController:commentController animated:YES];
    }
}

-(IBAction)editBtnPressed:(id)sender
{
    
    if(_vModel.canDeleteComment == 0)
        [self presentAll:0];
    else if(_vModel.canDeleteComment == 1){
        //Mute And Delete
        [self presentMuteAndDelet:8];
    }
    else if(_vModel.canDeleteComment == 2){
        //Mute Beam
        [self presentMuteAndDelet:9];
    }
    else if(_vModel.canDeleteComment == 3){
        //Delete
        [self presentMuteAndDelet:10];
    }
    
}
-(IBAction)reporttapped:(id)sender{
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] == _vModel.user_id){
        [self editBeam:self];
    }
    else{
        [self ReportBtn:self];
    }
}

- (IBAction)ReportBtn:(id)sender{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    editView.hidden = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"reportPost",@"method",
                              token,@"session_token",_vModel.videoID,@"post_id",@"For No Reason",@"reason",language,@"language",nil];

    NSData *postData = [Utils encodeDictionary:postDict];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];

}
- (IBAction)editBeam:(id)sender{
    NSString *postIDs = _cPostId;
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.video_thumbnail = _vModel.video_thumbnail_link;
    uploadController.postID = postIDs;
    uploadController.friendsArray = appDelegate.friendsArray;
    uploadController.caption      = _vModel.title;
    uploadController.isEdit = YES;
    appDelegate.hasbeenEdited     = TRUE;
    uploadController.privacyToShow = _vModel.privacy;
    uploadController.replyContToShow = _vModel.reply_count;
    [[self navigationController] pushViewController:uploadController animated:YES];
}
-(void)presentMuteAndDelet:(int)state{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* Share = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"share", nil)
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [self shareDialog];
                                [view dismissViewControllerAnimated:YES completion:nil];

                            }];

    UIAlertAction* deleteCurrentPost = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"delete_beam", nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            [self DeleteBtn:self];
                                            [view dismissViewControllerAnimated:YES completion:nil];

                                        }];
    UIAlertAction* shareOnFb = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"share_on_facebook", nil)                              style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self fbshareBtn:self];
                                    [view dismissViewControllerAnimated:YES completion:nil];

                                }];
    UIAlertAction* ShareonTwt = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"share_on_twitter", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self twitterShareBtn:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];

                                 }];
    UIAlertAction* reportBeam = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"report_beam", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self reporttapped:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];

                                 }];
    UIAlertAction* blockP = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"block_user", nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self blockBtn:self];
                                 [view dismissViewControllerAnimated:YES completion:nil];

                             }];
    UIAlertAction* mute = [UIAlertAction
                           actionWithTitle:NSLocalizedString(@"mute_beam", nil)
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               [self MuteComment:NO];
                               [view dismissViewControllerAnimated:YES completion:nil];

                           }];
    UIAlertAction* muteMainPost = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"mute_beam", nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self MuteComment:YES];
                                       [view dismissViewControllerAnimated:YES completion:nil];

                                   }];

    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"cancel", nil)
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];

                             }];
    if((state == 8 || state == 9)&& !usersPost){

        [view addAction:Share];
        [view addAction:muteMainPost];
        [view addAction:reportBeam];
        [view addAction:blockP];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(620, 410, 200, 200);
    }
    else if((state == 8 || state == 9)&& usersPost){

        [view addAction:Share];
        [view addAction:muteMainPost];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(600, 50, 200, 200);
    }
    else if(!usersPost){
        [view addAction:Share];
        [view addAction:reportBeam];
        [view addAction:blockP];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(600, 50, 200, 200);
    }else {
        [view addAction:Share];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(600, 50, 200, 200);
    }
    //    view.popoverPresentationController.sourceView = self.view;
    //    view.popoverPresentationController.sourceRect = CGRectMake(600, 50, 200, 200);
    [self presentViewController:view animated:YES completion:nil];
}

-(void)MuteComment:(BOOL)isMainPost{
    if(!isMainPost){
        VideoModel *tempVideos  = [dataArray objectAtIndex:currentSelectedIndex];
        tempVideos.isMute = @"1";
        [self MuteCommentRequest:tempVideos.videoID isMainPost:isMainPost];
    }else{
        [self MuteCommentRequest:_vModel.videoID isMainPost:isMainPost];
    }
}
-(void)MuteCommentRequest:(NSString *)commentId isMainPost:(BOOL)isMainPost{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"muteComment",@"method",
                              token,@"session_token",commentId,@"comment_id",nil];

    NSData *postData = [Utils encodeDictionary:postDict];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                if(isMainPost)
                {
                    _vModel.isMute = @"1";
                }
                Alert  *alert = [[Alert alloc] initWithTitle:message duration:(float)2.0f completion:^{
                    //
                }];
                [alert setShowStatusBar:YES];
                [alert setAlertType:AlertTypeSuccess];
                [alert setIncomingTransition:AlertIncomingTransitionTypeSlideFromTop];
                [alert setOutgoingTransition:AlertOutgoingTransitionTypeSlideToTop];
                [alert setBounces:YES];
                [alert showAlert];
//                [self.commentsTable reloadData];
//                [commentsCollectionView reloadData];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
}
-(IBAction)blockBtn:(id)sender{
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] == _vModel.user_id){
        [self DeleteBtn:self];
    }
    else{
        [self BlockPerson:self];
    }
}

-(IBAction)BlockPerson:(id)sender{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    editView.hidden = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *language = [Utils getLanguageCode];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"blockUser",@"method",
                              token,@"session_token",_vModel.user_id,@"blocking_user_id",language,@"language",nil];

    NSData *postData = [Utils encodeDictionary:postDict];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                appDelegate.timeToupdateHome = TRUE;
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }

        }

        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
}

-(IBAction)twitterShareBtn:(id)sender
{
//    [editView setHidden:YES];
    NSURL *imageURL = [NSURL URLWithString:_vModel.video_thumbnail_link];
    [CustomLoading showAlertMessage];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];

        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            UIImage *thumbnailImg = [UIImage imageWithData:imageData];
            [CustomLoading DismissAlertMessage];

            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {

                SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];

                [mySLComposerSheet setInitialText:@"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech."];
                [mySLComposerSheet addURL:[NSURL URLWithString:_vModel.beam_share_url]];
                [mySLComposerSheet addImage:thumbnailImg];
                [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {

                    switch (result) {
                        case SLComposeViewControllerResultCancelled:

                            break;
                        case SLComposeViewControllerResultDone:

                            break;

                        default:
                            break;
                    }
                }];

                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            }
            else {

                NSString *message;
                NSString *title;
                NSString *cancel;
                message = NSLocalizedString(@"setupTwitterAccount", @"");
                title   = NSLocalizedString(@"accountConfiguration", @"");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                               delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        });
    });


}
-(IBAction)fbshareBtn:(id)sender
{
    __block NSString *urlToShare ;
    if(self.isThumbnailReq == 1){
        _vModel.isThumbnailReq = 0;
        serviceParams = [NSMutableDictionary dictionary];
        [serviceParams setValue:@"uploadThumbnail" forKey:@"method"];
        [serviceParams setValue:_vModel.videoID forKey:@"post_id"];
        if(self.isComment)
            [serviceParams setValue:@"1" forKey:@"is_comment"];
        else
            [serviceParams setValue:@"0" forKey:@"is_comment"];
        [Utils downloadImageWithURL:[NSURL URLWithString:_vModel.video_thumbnail_link] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                UIImage *newImg =  [image imageWithScaledToSize:CGSizeMake(400, 400)];
                UIImage *im = [Utils drawImage:[UIImage imageNamed:@"play_button_small"] inImage:newImg atPoint:CGPointMake(250, 250)];
                NSData *imgData;
                imgData = UIImagePNGRepresentation(im);
                [ApiManager uploadURL:SERVER_URL parametes:serviceParams fileData:imgData progress:^(float progress) {
                }
                                 name:@"image" fileName:@"image.png" mimeType:@"image/png" success:^(id data) {
                                     int flag = [[data objectForKey:@"success"] intValue];
                                     if(flag){
                                         urlToShare = (NSString *)[data objectForKey:@"deep_link"];
                                         FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                                         content.contentURL = [NSURL URLWithString:urlToShare];
                                         //content.imageURL   = [NSURL URLWithString:urlToShare];
                                         content.contentTitle = _vModel.userName;
                                         content.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";

                                         FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
                                         dialog.fromViewController = self;
                                         dialog.shareContent = content;
                                         dialog.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
                                         if (![dialog canShow]) {
                                             // fallback presentation when there is no FB app
                                             dialog.mode = FBSDKShareDialogModeFeedBrowser;
                                         }
//                                         [editView setHidden:YES];
                                         [dialog show];
                                     }
                                 } failure:^(NSError *error) {
                                 }];
            }
        }];
    }else{
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentURL = [NSURL URLWithString:_vModel.deep_link];
        //content.imageURL   = [NSURL URLWithString:urlToShare];
        content.contentTitle = _vModel.userName;
        content.contentDescription = @"Hyde Park Corner is a visual mobile based social network which encourages freedom of speech.";
        FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
        dialog.fromViewController = self;
        dialog.shareContent = content;
        dialog.mode = FBSDKShareDialogModeFeedWeb; // if you don't set this before canShow call, canShow would always return YES
        if (![dialog canShow]) {
            // fallback presentation when there is no FB app
            dialog.mode = FBSDKShareDialogModeFeedBrowser;
        }
//        [editView setHidden:YES];
        [dialog show];
    }
}
-(IBAction)DeleteBtn:(id)sender{
    [CustomLoading showAlertMessage];
    NSString *postIDs = _cPostId;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"deletePost",@"method",
                              token,@"session_token",postIDs,@"post_id",nil];

    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {

            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            [CustomLoading DismissAlertMessage];
            if(success == 1){

                DataContainer *sharedManager = [DataContainer sharedManager];
                for(int i = 0; i < sharedManager.channelVideos.count;i++){
                    VideoModel *Vobj = [sharedManager.channelVideos objectAtIndex:i];
                    if([Vobj.videoID isEqualToString:_cPostId]){
                        NSInteger beamCount = [[[NSUserDefaults standardUserDefaults]
                                                stringForKey:@"cachedBeamsCount"] integerValue] ;
                        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",beamCount-1] forKey:@"cachedBeamsCount"];
                        [sharedManager.channelVideos removeObjectAtIndex:i];
                        break;
                    }
                }
                sharedManager.isReloadMyCorner = YES;
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else{
            [CustomLoading DismissAlertMessage];
        }
    }];
}
-(void)presentAll:(int)state{
    int isCelebrity = [Utils userIsCeleb];

    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* Share = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"share", @"")
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [self shareDialog];
                                [view dismissViewControllerAnimated:YES completion:nil];

                            }];
    UIAlertAction *sharewithInApp = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"inbox_to_friend", @"")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         if(!isCelebrity) {
                                             [self showToshare:_vModel.videoID];
                                             [view dismissViewControllerAnimated:YES completion:nil];
                                         }
                                     }];
    UIAlertAction *postOnFriendsCorner = [UIAlertAction
                                          actionWithTitle:NSLocalizedString(@"post_on_friend_corner", @"")
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              if(!isCelebrity) {
                                                  [self shareOnFriendsCorner:_vModel.videoID];
                                                  [view dismissViewControllerAnimated:YES completion:nil];
                                              }
                                          }];
    UIAlertAction *rebeam = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"rebeam", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self shareOnMyCorner:_vModel.videoID];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                             }];
    UIAlertAction* deleteCurrentPost = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"delete_beam", @"")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            [self DeleteBtn:self];
                                            [view dismissViewControllerAnimated:YES completion:nil];

                                        }];
    UIAlertAction* editCurrentBeam = [UIAlertAction
                                      actionWithTitle:NSLocalizedString(@"edit_beam", @"")
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          [self editBeam:self];
                                          [view dismissViewControllerAnimated:YES completion:nil];

                                      }];

    UIAlertAction* reportBeam = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"report_beam", @"")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self reporttapped:self];
                                     [view dismissViewControllerAnimated:YES completion:nil];

                                 }];
    UIAlertAction* blockP = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"block_user", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self blockBtn:self];
                                 [view dismissViewControllerAnimated:YES completion:nil];

                             }];
    UIAlertAction* mute = [UIAlertAction
                           actionWithTitle:NSLocalizedString(@"mute_beam", @"")
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               [self MuteComment:NO];
                               [view dismissViewControllerAnimated:YES completion:nil];

                           }];
    UIAlertAction* muteMainPost = [UIAlertAction
                                   actionWithTitle:@"Mute this Beam"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self MuteComment:YES];
                                       [view dismissViewControllerAnimated:YES completion:nil];

                                   }];
    UIAlertAction* delete = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"delete_beam", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self deleteComment:NO];
                                 [view dismissViewControllerAnimated:YES completion:nil];

                             }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"cancel", @"")
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];

                             }];
    UIAlertAction *downloadBeam = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"save_to_gallery", @"")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action){
                                       [Utils DownloadVideo:_vModel.video_link];
                                       [view dismissViewControllerAnimated:YES completion:nil];
                                   }];

    //3 == mute and delete  and 4 ==  neither post nor comment
    if(state == 0 && !usersPost){
        [view addAction:Share];
        [view addAction:rebeam];
        [view addAction:postOnFriendsCorner];
        [view addAction:sharewithInApp];
        [view addAction:downloadBeam];
        [view addAction:reportBeam];
        [view addAction:blockP];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(630, 400, 200, 200);
    }
    else if(state == 0 && usersPost){
        [view addAction:Share];
        [view addAction:rebeam];
        [view addAction:postOnFriendsCorner];
        [view addAction:sharewithInApp];
        [view addAction:downloadBeam];
        [view addAction:editCurrentBeam];
        [view addAction:deleteCurrentPost];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(630, 400, 200, 200);
    }
    else if(state == 1){
        [view addAction:mute];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height - 60, 200, 200);
    }else if(state == 3){
        [view addAction:Share];
        [view addAction:postOnFriendsCorner];
        [view addAction:sharewithInApp];
        [view addAction:downloadBeam];
        [view addAction:mute];
        [view addAction:delete];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height - 60, 200, 200);
    } else if(state == 4) {
        [view addAction:Share];
        [view addAction:postOnFriendsCorner];
        [view addAction:sharewithInApp];
        [view addAction:downloadBeam];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height - 60, 200, 200);
    }
    else{
        [view addAction:delete];
        [view addAction:cancel];
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(self.view.center.x - 100, self.view.frame.size.height - 60, 200, 200);
    }
    //    view.popoverPresentationController.sourceView = self.view;
    //    view.popoverPresentationController.sourceRect = CGRectMake(630, 400, 200, 200);
    [self presentViewController:view animated:YES completion:nil];
}

-(void)deleteComment:(BOOL)isMainPost{
    if(!isMainPost){
        VideoModel *tempVideos  = [dataArray objectAtIndex:currentSelectedIndex];
        [dataArray removeObjectAtIndex:currentSelectedIndex];
//        [self.commentsTable reloadData];
//        [commentsCollectionView reloadData];
        NSInteger commentsCount = [self.vModel.comments_count integerValue];
        commentsCount--;
        self.vModel.comments_count = [NSString stringWithFormat:@"%ld" ,(long)commentsCount];
        NSString *abbRepliesCount = [Utils abbreviateNumber:_vModel.comments_count withDecimal:1];
        if(commentsCount == 1){
//            self.hReplies.text    =
//            [NSString  stringWithFormat:@"%@ %@", abbRepliesCount,NSLocalizedString(@"reply", @"")];
        }else{
//            self.hReplies.text    =
//            [NSString  stringWithFormat:@"%@ %@",abbRepliesCount,NSLocalizedString(@"replies", @"")];
        }
        [self DeleteCommentRequest:tempVideos.videoID indextodelete:currentSelectedIndex];
    }
    else{
        
    }
}

-(void)DeleteCommentRequest:(NSString *)commentId indextodelete:(NSInteger)indextodelete{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"deleteComment",@"method",
                              token,@"session_token",commentId,@"comment_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
                Alert  *alert = [[Alert alloc] initWithTitle:message duration:(float)2.0f completion:^{
                    //
                }];
                [alert setShowStatusBar:YES];
                [alert setAlertType:AlertTypeSuccess];
                [alert setIncomingTransition:AlertIncomingTransitionTypeSlideFromTop];
                [alert setOutgoingTransition:AlertOutgoingTransitionTypeSlideToTop];
                [alert setBounces:YES];
                [alert showAlert];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"") otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else{
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            [alert show];
        }
    }];
    
}

#pragma mark Get Sharing Content
-(void)shareDialog{
    if(_vModel.beam_share_url){
        [self showShareSheet:_vModel.beam_share_url];
    }
}
-(void)showShareSheet:(NSString *)shareURL{
    NSURL *url = [NSURL URLWithString:shareURL];
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[url]
     applicationActivities:nil];
    controller.excludedActivityTypes = @[UIActivityTypePrint,
                                         UIActivityTypeMail,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop,
                                         @"com.apple.mobilenotes.SharingExtension",
                                         @"com.apple.reminders.RemindersEditorExtension",
                                         ];
    controller.popoverPresentationController.sourceView = self.view;
    controller.popoverPresentationController.sourceRect = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 300);
    [self presentViewController:controller animated:YES completion:nil];
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            // user shared an item
            //            NSLog(@"We used activity type%@", activityType);
            if([activityType isEqualToString:@"com.apple.UIKit.activity.CopyToPasteboard"]){
                [Utils showAlert:NSLocalizedString(@"link_copied", @"")];
            }
        } else {
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}
#pragma mark Sheet Actions
-(void)showToshare:(NSString *)pOstID{
    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = [Utils getFriendsToShareBeam];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 5;
    commentController.userId        = @"";
    commentController.postId = pOstID;
    commentController.isCommentVideo = @"0";
    commentController.isLocalSearch = YES;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(void)shareOnFriendsCorner:(NSString *)postId{
    FriendsVC *commentController;//    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = [Utils getFriendsToShareBeam];
    commentController.titles        = NSLocalizedString(@"friends", @"");
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= 6;
    commentController.userId        = @"";
    commentController.postId = postId;
    NSString *isCommentV = [NSString stringWithFormat:@"%d",_isComment];
    commentController.isCommentVideo = isCommentV;
    commentController.isLocalSearch = YES;
    [[self navigationController] pushViewController:commentController animated:YES];
}

-(void)shareOnMyCorner:(NSString *)pOstID{
    NSString *isCommentV = [NSString stringWithFormat:@"%d",_isComment];
    [Utils shareBeamOnCorner:pOstID isComment:isCommentV friendId:@"" sharingOnFriendsCorner:NO];
}

-(void)setVideoPlayerState{
//    self.videoTitle.numberOfLines = 0;
//    [self.videoTitle sizeToFit];
    self.videoTitle.text =  [Utils decodeForEmojis:self.vModel.title];
    self.videoTitle.text =  [Utils decodeForEmojis:self.videoTitle.text];
    
    self.timeStamp.text = [Utils returnDate:self.vModel.uploaded_date];
    self.likeCount.text = [Utils simpleAbbNumber:self.vModel.like_count withDecimal:1];
    self.commentsCount.text = [Utils simpleAbbNumber:self.vModel.comments_count withDecimal:1];
    if([self.vModel.is_anonymous isEqualToString:@"0"]){
        
        self.userName.text = [Utils getDecryptedTextFor:self.vModel.userName];

//        self.userName.text = self.vModel.userName;
    }
    else{
            self.userName.text = NSLocalizedString(@"anonymous_small", nil);
        
    }
//    if ([self.vModel.like_by_me isEqualToString:@"1"]) {
//        [self.likeBtn setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
//    }else{
//        [self.likeBtn setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
//    }
    NSString *urlToStream;
    if(self.vModel.m3u8_video_link.length > 1) {
        urlToStream = self.vModel.m3u8_video_link;
    }
    else {
        urlToStream = self.vModel.video_link;
    }
    if(self.isfromChat){
        [self.likeBtn setHidden:YES];
        [self.commentBtn setHidden:YES];
        [self.likeCount setHidden:YES];
        [self.commentsCount setHidden:YES];
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,65,375, 460)];
    if(IS_IPHONE_6Plus){
        bgView.frame = CGRectMake(0,65,414, 511);
        
    }else if (IS_IPHONEX){
        bgView.frame = CGRectMake(0,89,414, 511);
    }
    else if(IS_IPAD)
    {
        bgView.frame = CGRectMake(0,85,768, 700);
        
    }else if(IS_IPHONE_5)
    {
        bgView.frame = CGRectMake(0,65, 320, 380);
        
    }
    if(self.vModel.beamType == 1){
        self.rebeamImage.hidden = NO;
    }
    else if(self.vModel.beamType == 2){
        NSString *sharedby =  [self.vModel.originalBeamData objectForKey:@"full_name"];
//        self.userName.text = [NSString stringWithFormat:@"%@ %@ with %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby],[Utils getTrimmedString:self.vModel.userName]];
        self.userName.text = [NSString stringWithFormat:@"%@ %@ with %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]],[Utils getTrimmedString:[Utils getDecryptedTextFor:self.vModel.userName]]];

        self.rebeamImage.hidden = YES;
    }
    else{
        self.rebeamImage.hidden = YES;
    }
    [self.thumbnail sd_setImageWithURL:[NSURL URLWithString:self.vModel.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
    
    
    
    
    
    NSURL *bipbopUrl = [[NSURL alloc] initWithString:urlToStream];
    asset = [AVAsset assetWithURL:bipbopUrl];
//    playerItem = [[AVPlayerItem alloc] initWithAsset:asset];
    playerItem = [AVPlayerItem mc_playerItemWithRemoteURL:bipbopUrl error:nil];
    playVideo = [[AVPlayer alloc] initWithPlayerItem:playerItem];
    playVideo.automaticallyWaitsToMinimizeStalling = true;
    playVideo.currentItem.preferredForwardBufferDuration = 1;
    NSError *error;
    [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker
                                                       error:&error];
    if(error)
    {
        NSLog(@"Error: AudioSession cannot use speakers");
    }
    
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.player = playVideo;
    _playerViewController.showsPlaybackControls = NO;
    playVideo.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    _playerViewController.view.frame = CGRectMake(_thumbnail.frame.origin.x, _thumbnail.frame.origin.y, _thumbnail.frame.size.width, _thumbnail.frame.size.height - 50); //self.view.bounds;
    [self.view addSubview:_playerViewController.view];
    [self.view bringSubviewToFront:_playerViewController.view];
    _playerViewController.view.hidden = true;
    
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(playerItemDidReachEnd:)
//                                                     name:AVPlayerItemDidPlayToEndTimeNotification
//                                                   object:nil];
//    });
    
    
    
//    [playVideo.currentItem addObserver:self forKeyPath:@"status" options:0 context:nil];
    
    
    [playVideo performSelector:@selector(play) withObject:nil afterDelay:2];
    if(!timer2)
    {
        timer2 = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(checkIfPlayerReady) userInfo:nil repeats:YES];
    }
    
    
    
    
    
    
    
    
    
    
//    _videoPlayerController = [[PBJVideoPlayerController alloc] init];
//    _videoPlayerController.delegate = self;
//    _videoPlayerController.view.frame = bgView.frame;
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;

    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    if(![self.vModel.video_length isEqualToString:@""])
    {
        NSNumber *value =[self secondsForTimeString:self.vModel.video_length];
        duration = [value floatValue];
        self.totaltime = [self.dateComponentsFormatter stringFromTimeInterval:duration];
        self.timerLabel.text = [NSString stringWithFormat:@"%@ / %@",@"0:00:00",self.totaltime];
    }
    else
    {
        self.timerLabel.text = [NSString stringWithFormat:@"%@ / %@",@"0:00:00",@"0:00:00"];
    }
    [activityIndicator startAnimating];
    self.pauseBtn.hidden  = YES;
//    NSLog(@"URL TO STREAM : %@",urlToStream);
    
//    NSURL *bipbopUrl = [[NSURL alloc] initWithString:urlToStream];
//    _videoPlayerController.asset = [[AVURLAsset alloc] initWithURL:bipbopUrl options:nil];
    
    
//    CMTime duration = [_videoPlayerController._playerItem duration]; //total time
    mySlider.transform = CGAffineTransformMakeScale(0.6, 0.6);
    mySlider.frame = CGRectMake(0, -4, [[UIScreen mainScreen] bounds].size.width - 40, 30);
    mySlider.maximumValue = duration;
    mySlider.value = 0.0;
    [mySlider addTarget:self action:@selector(dragEndedForSlider)
          forControlEvents:UIControlEventTouchUpInside];
    
    [mySlider addTarget:self action:@selector(dragStartedForSlider)
       forControlEvents:UIControlEventTouchDown];
    
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTapped:)];
    [mySlider addGestureRecognizer:gr];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];\
//    NSLog(@"from beginning 1");
//    [_videoPlayerController playFromBeginning];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if (object == playVideo.currentItem && [keyPath isEqualToString:@"status"]) {
        if (playVideo.status == AVPlayerStatusReadyToPlay)
        {
            _playerViewController.view.hidden = false;
            [playVideo play];
            [activityIndicator removeFromSuperview];
            self.thumbnail.hidden = YES;
        }
        else if (playVideo.status == AVPlayerStatusFailed)
        {
        }
    }
}

-(void)checkIfPlayerReady
{
    NSLog(@"%f",CMTimeGetSeconds(playVideo.currentTime));
    if(CMTimeGetSeconds(playVideo.currentTime) > 0 && _playerViewController.view.hidden)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [timer2 invalidate];
            timer2 = nil;
        });
        
        _playerViewController.view.hidden = false;
        _playerViewController.showsPlaybackControls = YES;
        [activityIndicator removeFromSuperview];
        self.thumbnail.hidden = YES;
        
//        if(!shudContinuePlayer)
//        {
//            NSLog(@"VIDEO HAS BEEN PAUSED");
//            [playVideo pause];
//        }
//        else{
//            NSLog(@"VIDEO HAS BEEN PLAYED");
//            [playVideo play];
//        }
    }
}

-(void)dragStartedForSlider
{
    [timer invalidate];
    [_videoPlayerController stop];
}
-(void)dragEndedForSlider
{
//    timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];\
    [_videoPlayerController._playerItem seekToTime:CMTimeMakeWithSeconds((int)mySlider.value,1)];
    [_videoPlayerController playFromCurrentTime];
}

- (void)sliderTapped:(UIGestureRecognizer *)g
{
    UISlider* s = (UISlider*)g.view;
    if (s.highlighted)
        return; // tap on thumb, let slider deal with it
    CGPoint pt = [g locationInView: s];
    CGFloat percentage = pt.x / s.bounds.size.width;
    CGFloat delta = percentage * (s.maximumValue - s.minimumValue);
    CGFloat value = s.minimumValue + delta;
    [s setValue:value animated:YES];
    
    [_videoPlayerController._playerItem seekToTime:CMTimeMakeWithSeconds((int)mySlider.value,1)];
}


- (void)updateTime:(NSTimer *)timer {
//    mySlider.value = CMTimeGetSeconds([_videoPlayerController._playerItem currentTime]);
    
    NSUInteger dTotalSeconds = CMTimeGetSeconds(playVideo.currentTime);
    
    NSUInteger dHours = floor(dTotalSeconds / 3600);
    NSUInteger dMinutes = floor(dTotalSeconds % 3600 / 60);
    NSUInteger dSeconds = floor(dTotalSeconds % 3600 % 60);
    
    NSString *videoDurationText = [NSString stringWithFormat:@"%lu:%02lu:%02lu",(unsigned long)dHours, (unsigned long)dMinutes, (unsigned long)dSeconds];

    self.timerLabel.text = [NSString stringWithFormat:@"%@ / %@",videoDurationText,self.totaltime];
    if([videoDurationText isEqualToString:self.totaltime])
    {
        [playVideo.currentItem seekToTime:kCMTimeZero];
        [playVideo play];
    }
}
//
//- (NSString *)timeFormatted:(float )totalSeconds
//{
//    
//    float seconds = totalSeconds % 60;
//    float minutes = (totalSeconds / 60) % 60;
//    float hours = totalSeconds / 3600;
//    
//    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
//}

- (NSNumber *)secondsForTimeString:(NSString *)string {
    
    NSArray *components = [string componentsSeparatedByString:@":"];
    NSInteger seconds;
    NSInteger hours = 0;
    NSInteger minutes;
    
    if (components.count > 2){
        hours   = [[components objectAtIndex:0] integerValue];
        minutes = [[components objectAtIndex:1] integerValue];
        seconds = [[components objectAtIndex:2] integerValue];
    }
    else{
        minutes = [[components objectAtIndex:0] integerValue];
        seconds = [[components objectAtIndex:1] integerValue];
    }
    return [NSNumber numberWithInteger:(hours * 60 * 60) + (minutes * 60) + seconds];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    activityIndicator.center = CGPointMake (self.thumbnail.frame.size.width/2 ,self.thumbnail.frame.size.height/1.8 + activityIndicator.frame.size.height);
    self.pauseBtn.center = CGPointMake (self.thumbnail.frame.size.width/2 ,self.thumbnail.frame.size.height/1.6 );
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [playVideo pause];
    
    
    if(!self.thumbnail.hidden)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [timer2 invalidate];
            timer2 = nil;
        });
        [playVideo replaceCurrentItemWithPlayerItem:nil];
        playVideo = nil;
    }
    
    
    
    
    
    
    
    
    
    if(shudClearPlayer)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
        playVideo = nil;
        [timer invalidate];
        dispatch_async(dispatch_get_main_queue(), ^{
            [timer2 invalidate];
            timer2 = nil;
        });
    }
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
//    [playVideo pause];
//    [timer invalidate];
//    playVideo = nil;
}

- (IBAction)back:(id)sender {
    shudClearPlayer = true;
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)commentsPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - PBJVideoPlayerControllerDelegate
- (void)videoPlayerReady:(PBJVideoPlayerController *)videoPlayer
{
//    NSLog(@"Max duration of the video: %f", videoPlayer.maxDuration);
    
    if(!self.totaltime)
    {
        int totalSeconds = videoPlayer.maxDuration;
        
        int hours = totalSeconds / 3600;
        int minutes = totalSeconds / 60;
        int seconds = totalSeconds % 60;
        
        self.totaltime = [NSString stringWithFormat:@"%d:%02d:%02d",hours,minutes,seconds];
        self.timerLabel.text = [NSString stringWithFormat:@"%@ / %@",@"0:00:00",self.totaltime];
    }
    if(_uploadAudioView.hidden)
    {
        [self.view addSubview:_videoPlayerController.view];
        [self.view addSubview:self.pauseBtn];
        [self.view bringSubviewToFront:mySliderBackground];
    }
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (IBAction)moveSlider {
    
    [_videoPlayerController._playerItem seekToTime:CMTimeMakeWithSeconds((int)mySlider.value,1)];
}

- (void)videoPlayerPlaybackStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
    switch (videoPlayer.playbackState) {
        case PBJVideoPlayerPlaybackStateStopped:
//            NSLog(@"STOPPED BDDD!");
            break;
            
        case PBJVideoPlayerPlaybackStatePlaying:
//            NSLog(@"PLAYING BDDDD");
            self.pauseBtn.hidden = YES;
            break;
            
        case PBJVideoPlayerPlaybackStatePaused:
//            NSLog(@"PAUSED BDDD.");
            self.pauseBtn.hidden = NO;
            mySliderBackground.hidden = false;
            break;
        default:
            break;
    }
}

- (void)videoPlayerPlaybackWillStartFromBeginning:(PBJVideoPlayerController *)videoPlayer
{
//    NSLog(@"PLAYBACK videoPlayerPlaybackWillStartFromBeginning");
}

- (void)videoPlayerPlaybackDidEnd:(PBJVideoPlayerController *)videoPlayer
{
//    NSLog(@"PLAYBACK DID END ");
    NSLog(@"from beginning 2");
    [_videoPlayerController playFromBeginning];
}

- (void)videoPlayerBufferringStateDidChange:(PBJVideoPlayerController *)videoPlayer
{
    switch (videoPlayer.bufferingState) {
        case PBJVideoPlayerBufferingStateUnknown:
            //NSLog(@"Buffering state unknown!");
            break;
            
        case PBJVideoPlayerBufferingStateReady:
            [activityIndicator removeFromSuperview];
            if(videoPlayer.playbackState == PBJVideoPlayerPlaybackStatePaused){
                NSLog(@"from current 1");
                [_videoPlayerController playFromCurrentTime];
            }
            //NSLog(@"Buffering state Ready! Video will start/ready playing now.");
            break;
            
        case PBJVideoPlayerBufferingStateDelayed:
            [_videoPlayerController pause];
            //NSLog(@"Buffering state Delayed! Video will pause/stop playing now.");
            break;
        default:
            break;
    }
}

- (void)videoPlayer:(PBJVideoPlayerController *)videoPlayer didUpdatePlayBackProgress:(CGFloat)progress {
    //NSLog(@"Completed of the video: %f", progress);
    self.progressView.progress = progress/duration;
    if(self.totaltime != nil)
    {
        self.timerLabel.text = [NSString stringWithFormat:@"%@ / %@",[self.dateComponentsFormatter stringFromTimeInterval:progress],self.totaltime];
    }
}

- (CMTime)videoPlayerTimeIntervalForPlaybackProgress:(PBJVideoPlayerController *)videoPlayer {
    return CMTimeMake(1, 20);
}

-(IBAction)likesPressed:(id)sender{
    if([self.vModel.like_by_me isEqualToString:@"1"])
    {
        [self.likeBtn setBackgroundImage:[UIImage imageNamed:@"likenew.png"] forState:UIControlStateNormal];
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
            {
                [_likeBtn setBackgroundImage:[UIImage imageNamed:@"like_nightmode.png"] forState:UIControlStateNormal];
            }
        }
        self.vModel.like_by_me = @"0";
        NSInteger likeCount = [self.vModel.like_count intValue];
        if(likeCount >0){
            likeCount--;
        }
        self.vModel.like_count =  [NSString stringWithFormat: @"%ld", likeCount];
        self.likeCount.text = [Utils simpleAbbNumber:self.vModel.like_count withDecimal:1];
    }
    else{
        [self.likeBtn setBackgroundImage:[UIImage imageNamed:@"likeblue.png"] forState:UIControlStateNormal];
        self.vModel.like_by_me = @"1";
        NSInteger likeCount = [self.vModel.like_count intValue];
        likeCount++;
        self.vModel.like_count =  [NSString stringWithFormat: @"%ld", likeCount];
        self.likeCount.text = [Utils simpleAbbNumber:self.vModel.like_count withDecimal:1];
    }
    if(self.isComment){
        [self LikeComment];
    }
    else{
        [self LikePost];
    }
}

#pragma mark - Like Post
- (void) LikePost{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *postID = self.vModel.videoID;
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_LIKE_POST,@"method",
                              token,@"session_token",postID,@"post_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            //            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
            }
        }else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
}
#pragma mark - Like Post
- (void) LikeComment{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *postID = self.vModel.videoID;
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_LIKE_COMMENT,@"method",
                              token,@"session_token",postID,@"comment_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            //            NSString *message = [result objectForKey:@"message"];
            if(success == 1){
            }
            else{
                
            }
        }
        else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
}






#pragma mark - Beam Pressed

- (IBAction)beamPressed:(UIButton *)sender {
    
    [_videoPlayerController pause];
    shudContinuePlayer = false;
    if([sender tag] == 100){
        NSDate *lastAnonymusBeamDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"lastAnonymusBeam"];
        uploadAnonymous = true;
        uploadBeamTag = false;
        
    }
    else if([sender tag ] == 101)
    {
        uploadAnonymous = false;
        uploadBeamTag = true;
    }
    if([postArray.reply_count intValue] == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Comments are not allowed on this Beam" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else{
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            NSBundle *bundle = [NSBundle bundleForClass:HBRecorder.class];
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HBRecorder.bundle/HBRecorder" bundle:bundle];
            
            HBRecorder *recorder = [sb instantiateViewControllerWithIdentifier:@"HBRecorder"];
            recorder.delegate = self;
            if(uploadAnonymous) {
                UIImageView *anonymousMark = [[UIImageView alloc] initWithFrame:CGRectMake(10, 64, recorder.view.frame.size.width - 20, recorder.view.frame.size.height - 150)];
                anonymousMark.image = [UIImage imageNamed:@"ano_new_icon"];
                anonymousMark.alpha = 0.5;
                [recorder.view addSubview:anonymousMark];
            }
            recorder.topTitle = @"";
            recorder.bottomTitle = @"";
            recorder.maxRecordDuration = 60 * 1;
            recorder.movieName = @"MyAnimatedMovie";
            
            
            recorder.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self.navigationController pushViewController:recorder animated:YES];
            
        } else {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"I'm afraid there's no camera on this device!" delegate:nil cancelButtonTitle:@"Dang!" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}

#pragma mark - HBRECORDER Recording Methods

- (void)recorder:(HBRecorder *)recorder  didFinishPickingMediaWithUrl:(NSURL *)videoUrl {
    
    _videoPath = videoUrl;
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        AVAsset *asset = [AVAsset assetWithURL:_videoPath];// url= give your url video here
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        
        imageGenerator.appliesPreferredTrackTransform = YES;
        
        CMTime time = CMTimeMake(5, 10);//it will create the thumbnail after the 5 sec of video
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
        self.thumbnailToUpload = thumbnail;
        int i = 0;
        
        if(i == 0) {
            AVAsset *asset = [AVAsset assetWithURL:videoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime thumbTime = CMTimeMake(10,5);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTime actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
            self.thumbnailToUpload2 = thumbnail;
            
            i++;
        }
        
        if(i == 1) {
            AVAsset *asset = [AVAsset assetWithURL:videoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime thumbTimes = CMTimeMake(10,1);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:thumbTimes actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            thumbnail = [thumbnail imageWithScaledToSize:CGSizeMake(300, 300)];
            self.thumbnailToUpload3 = thumbnail;
        }
        
        
        profileData = UIImagePNGRepresentation(thumbnail);
        CmovieData = [NSData dataWithContentsOfURL:videoUrl];
        
        CMTime CMduration = asset.duration;
        int totalSeconds = CMTimeGetSeconds(CMduration);
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;
        NSString *duration = @"";
        if (hours>0) {
            NSString *hoursString = [NSString stringWithFormat:@"%d hour(s)",hours];
            duration = [duration stringByAppendingString:hoursString];
        }
        if (minutes>0) {
            NSString *minString = [NSString stringWithFormat:@"%d min(s)",minutes];
            duration = [duration stringByAppendingString:minString];
        }
        if (seconds>0) {
            NSString *secString = [NSString stringWithFormat:@"%d sec(s)",seconds];
            duration = [duration stringByAppendingString:secString];
        }
        video_duration = [[NSString alloc]initWithFormat:@"%02lu:%02lu",(unsigned long)minutes,(unsigned long)seconds];
        dispatch_async(dispatch_get_main_queue(), ^{
            shudContinuePlayer = false;
            if(playVideo)
            {
                [playVideo pause];
            }
            [self movetoUploadBeamController];
        });
    });
    
    
}

- (void)recorderDidCancel:(HBRecorder *)recorder {
    NSLog(@"Recorder did cancel..");
}

- (void)recorderOrientation:(NSInteger)orientation {
    orientationAngle = orientation;
}


-(void) movetoUploadBeamController{
    
    shudContinuePlayer = false;
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.videoPath = _videoPath;
    uploadController.dataToUpload = CmovieData;
    uploadController.video_duration = video_duration;
    uploadController.friendsArray   = appDelegate.friendsArray;
    uploadController.orientationAngle = orientationAngle;
    
    if(!self.isFromDeepLink){
        if(isFirstComment || appDelegate.parentIdStackController.count == 1){
            uploadController.ParentCommentID = @"-1";
        }
        else
        {
            uploadController.ParentCommentID = (NSString*)[appDelegate.parentIdStackController peekObject];
        }
    }
    else{
        uploadController.ParentCommentID = pID;
    }
    uploadController.postID = _cPostId;
    uploadController.isAudio = false;
    uploadController.profileData = profileData;
    uploadController.isComment = TRUE;
    uploadController.thumbnailImage = self.thumbnailToUpload;
    uploadController.thumbnailImage2 = self.thumbnailToUpload2;
    uploadController.thumbnailImage3 = self.thumbnailToUpload3;
    uploadController.friendsArray   = appDelegate.friendsArray;
    if(uploadAnonymous)
        uploadController.isAnonymous = true;
    else
        uploadController.isAnonymous = false;
    [[self navigationController] pushViewController:uploadController animated:YES];
}


- (IBAction)RecorderPressed:(id)sender {
    [_videoPlayerController pause];
    
    if([postArray.reply_count intValue] == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Comments are not allowed on this Beam" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else{
        [playVideo pause];
        
        if(!self.thumbnail.hidden)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [timer2 invalidate];
                timer2 = nil;
            });
            [playVideo replaceCurrentItemWithPlayerItem:nil];
            playVideo = nil;
        }
        
        _uploadAudioView.hidden = NO;
        [self.view bringSubviewToFront:_uploadAudioView];
    }
    
}
#pragma mark AUDIO RECORDING AND UPLOADING

-(void)setAudioRecordSettings
{
    NSArray *dirPaths;
    NSString *docsDir;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(
                                                   NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString *soundFilePath = [docsDir
                               stringByAppendingPathComponent:@"sound.caf"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    //[NSNumber numberWithInt:kAudioFormatMPEGLayer3], AVFormatIDKey,
                                    [NSNumber numberWithInt:AVAudioQualityHigh],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 1],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    
    NSError *error = nil;
    
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];
    _audioRecorder.delegate = self;
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
        
    } else {
        [_audioRecorder prepareToRecord];
    }
}
- (IBAction)AudioClosePressed:(id)sender {
    [_videoPlayerController pause];
    if(playVideo)
    {
        [playVideo play];
    }
    
    if(!self.thumbnail.hidden)
    {
        [self setVideoPlayerState];
    }
    
//    [_uploadAudioView removeFromSuperview];
    _uploadAudioView.hidden = YES;
}

- (IBAction)recorderTapped:(id)sender {
    closeBtnAudio.hidden = true;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                        error:nil];
    if(!isRecording){
        [self animateImages];
        timerToupdateLbl = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
        audioTimeOut = [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self
                                                      selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: NO];
        [_audioRecorder record];
    }
    else{
        
        [_audioRecorder stop];
        [audioBtnImage stopAnimating];
    }
    isRecording = true;
}
-(void)animateImages{
    NSArray *loaderImages = @[@"state1.png", @"state2.png", @"state3.png"];
    NSMutableArray *loaderImagesArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < loaderImages.count; i++) {
        [loaderImagesArr addObject:[UIImage imageNamed:[loaderImages objectAtIndex:i]]];
    }
    audioBtnImage.animationImages = loaderImagesArr;
    audioBtnImage.animationDuration = 0.5f;
    [audioBtnImage startAnimating];
}

-(void) updateCountdown {
    int minutes, seconds;
    secondsLeft--;
    minutes = (secondsLeft / 60) % 60;
    seconds = (secondsLeft) % 60;
    countDownlabel.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    secondsConsumed = [NSString stringWithFormat:@"%02d:%02d", 00, 60 - secondsLeft];
}

-(void) callAfterSixtySecond:(NSTimer*) t
{
    [_audioRecorder stop];
    [timerToupdateLbl invalidate];
    [audioTimeOut invalidate];
    
}

-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    [timerToupdateLbl invalidate];
    [audioTimeOut invalidate];
    closeBtnAudio.hidden = false;
    isRecording = false;
    countDownlabel.text = @"01:00";
    secondsLeft = 60;
    CaudioData = [NSData dataWithContentsOfURL:_audioRecorder.url];
    
    [_uploadAudioView removeFromSuperview];
    
    BeamUploadVC *uploadController = [[BeamUploadVC alloc] initWithNibName:@"BeamUploadVC1" bundle:nil];
    uploadController.dataToUpload = CaudioData;
    uploadController.videoPath = _audioRecorder.url;
    if([secondsConsumed length] == 0)
        secondsConsumed = @"00:01";
    uploadController.video_duration = secondsConsumed;
    
    uploadController.friendsArray   = appDelegate.friendsArray;
    if(!self.isFromDeepLink){
        if(isFirstComment || appDelegate.parentIdStackController.count == 1)
            uploadController.ParentCommentID = @"-1";
        else
        {
            
            uploadController.ParentCommentID = (NSString*)[appDelegate.parentIdStackController peekObject];
        }
    }
    else{
        uploadController.ParentCommentID = pID;
    }
    uploadController.postID = _cPostId;
    uploadController.isAudio = true;
    uploadController.isComment = TRUE;
    self.thumbnailToUpload = [UIImage imageNamed: @"splash_audio_image.png"];
    uploadController.thumbnailImage = [UIImage imageNamed: @"splash_audio_image.png"];
    [[self navigationController] pushViewController:uploadController animated:YES];
    
}
-(void)audioRecorderEncodeErrorDidOccur:
(AVAudioRecorder *)recorder
                                  error:(NSError *)error
{  isRecording = false;
    closeBtnAudio.hidden = false;
    countDownlabel.text = @"00:00";
    secondsLeft = 60;
    secondsConsumed = 0;
    //    NSLog(@"Encode Error occurred");
}



@end
