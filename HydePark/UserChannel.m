
//
//  UserChannel.m
//  HydePark
//
//  Created by Apple on 22/02/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "UserChannel.h"
#import "NavigationHandler.h"
#import "Utils.h"
#import "UIImageView+RoundImage.h"
#import "AVFoundation/AVFoundation.h"
#import "AsyncImageView.h"
#import "ChannelCell.h"
#import "Constants.h"
#import "CommentsVC.h"
#import "NewHomeCells.h"
#import "VideoModel.h"
#import "VideoPlayerVC.h"
#import "Followings.h"
#import "FriendsVC.h"
#import "FriendChatVC.h"
#import "UIImageView+WebCache.h"
#import "EXPhotoViewer.h"
#import "VideoViewCell.h"
#import "NSData+AES.h"

#import "HeaderViewCell.h"
@interface UserChannel (){
    float headerHieght;
}
@property (weak, nonatomic) IBOutlet UIView *adsStaticView;

@property (weak, nonatomic) IBOutlet UILabel *celebViewsLbl;
@property (weak, nonatomic) IBOutlet UILabel *celebFollowersLbl;
@property (weak, nonatomic) IBOutlet UILabel *celebFriendsLbl;

@property (weak, nonatomic) IBOutlet UILabel *normalFriendsLbl;
@property (weak, nonatomic) IBOutlet UILabel *normalBeamsLbl;
@end

@implementation UserChannel
@synthesize ChannelObj,friendID,currentUser;

- (void)viewDidLoad {
    [super viewDidLoad];
     [self addrefreshControl];
    _beamMeLbl.text = NSLocalizedString(@"message_me", nil);
    _goLiveLbl.text = NSLocalizedString(@"go_live", nil);
    _firstCheck = YES;
    footerIndicator.hidden = YES;
    _msgButton.hidden = YES;
    sharedManager = [DataContainer sharedManager];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    videomodel = [[VideoModel alloc]init];
    CommentsModelObj = [[CommentsModel alloc] init];
    userChannelObj = [[UserChannelModel alloc]init];
    videoObj = [[NSMutableArray alloc] init];
    FollowingsArray = [[NSArray alloc] init];
    FollowingsAM    = [[NSMutableArray alloc] init];
    dataArray       = [[NSMutableArray alloc] init];
    _viewToRound.layer.cornerRadius = _viewToRound.frame.size.width / 4;
    _viewToRound.layer.masksToBounds = YES;
    _viewToRound.clipsToBounds = YES;
    _picBorder.layer.cornerRadius = _picBorder.frame.size.width / 4;
    _picBorder.clipsToBounds = YES;
    _picBorder.layer.masksToBounds = NO;
    pageNum = 1;
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            self.background.hidden = YES;
            _separator.backgroundColor = [UIColor darkGrayColor];
            _lowerSeparator.hidden = false;
            _myView.layer.masksToBounds = NO;
            _myView.layer.shadowOffset = CGSizeMake(0, 1);
            _myView.layer.shadowRadius = 6;
            _myView.layer.shadowOpacity = 0.5;
        }
        else
        {
            self.background.hidden = NO;
            _separator.backgroundColor = [UIColor whiteColor];
            _lowerSeparator.hidden = true;
        }
    }
    else
    {
        self.background.hidden = NO;
        _separator.backgroundColor = [UIColor whiteColor];
        _lowerSeparator.hidden = true;
    }
//    if([currentUser.is_celeb isEqualToString:@"0"]){
//        self.headerView.frame = CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, self.headerView.frame.size.height - 51);
//    }
    friendsChannelTable.tableHeaderView = self.headerView;

    if(ChannelObj.trendingArray == nil){
        if(APP_DELEGATE.hasInet){
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
            {
                if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
                {
                    _actInd.color = [UIColor lightGrayColor];

                }
            }
            
            [self GetUsersChannel];
        } else {
            [self showNoInternetAlert];
            [_actInd stopAnimating ];
            _actInd.hidden = YES;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }
    else
        [self initViewWithData];
    if(IS_IPHONE_5)
    {
        friendsNamelbl.frame = CGRectMake(160, friendsNamelbl.frame.origin.y, friendsNamelbl.frame.size.width, friendsNamelbl.frame.size.height);
    }
    
    self.celebViewsLbl.text  = NSLocalizedString(@"views_caps", @"");
    self.celebFollowersLbl.text = NSLocalizedString(@"followers_caps", @"");
    self.celebFriendsLbl.text  = NSLocalizedString(@"following_caps", @"");
    self.celebBeamLbl.text  = NSLocalizedString(@"beams_caps", @"");
    
    self.normalFriendsLbl.text = NSLocalizedString(@"friendCaps", @"");
    self.normalBeamsLbl.text = NSLocalizedString(@"beams_caps", @"");
    
    self.gridLbl.text = NSLocalizedString(@"view", @"");
    self.bioLbl.text = userChannelObj.bio;
    
    if(sharedManager.isListView) {
        _gridImageView.image = [UIImage imageNamed:@"Grid"];
        _gridLbl.textColor = [UIColor lightGrayColor];
    } else {
        _gridImageView.image = [UIImage imageNamed:@"Grid_blue"];
        _gridLbl.textColor = [UIColor colorWithRed:54/255.0 green:78/255.0 blue:141/255.0 alpha:1.0];
        
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [_collectionView registerNib:[UINib nibWithNibName:@"VideoViewCell" bundle:nil] forCellWithReuseIdentifier:@"collectionCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"HeaderViewCell" bundle:nil] forCellWithReuseIdentifier:@"headerCell"];
    _profileViews.center = CGPointMake(self.view.center.x, friendsCover.center.y);
    _profileViews.frame = CGRectMake(_profileViews.frame.origin.x, 15, _profileViews.frame.size.width, _profileViews.frame.size.height);
    
    if(sharedManager.isReloadMyCorner) {
        pageNum = 1;
        [self GetUsersChannel];
        sharedManager.isReloadMyCorner = NO;
    }
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    headerHieght = self.headerView.frame.size.height - 51;
//    CAGradientLayer *layer = [CAGradientLayer layer];
//    layer.colors = @[(id)[UIColor clearColor].CGColor, (id)[UIColor blackColor].CGColor];
//    layer.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, friendsCover.frame.size.height);
//    [friendsCover.layer addSublayer:layer];
    
}

#pragma  mark FOOTER VIEW
-(void)initFooterView
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat width = mainScreen.bounds.size.width;
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, 100.0)];
    UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            actInd.color = [UIColor lightGrayColor];
        }
    }
    actInd.tag = 700;
    actInd.frame = CGRectMake(width/2 - 10 ,40.0, 20.0, 20.0);
    actInd.hidesWhenStopped = YES;
    [footerView addSubview:actInd];
    actInd = nil;
}
-(void)initViewWithData{
   
    bgView.hidden            = NO;
    _overlayView.hidden      = NO;
    friendsCover.hidden      = NO;
    _profileViews.hidden     = NO;
    //    friendsBeamcount.hidden  = NO;
    //    friendsFollowers.hidden  = NO;
    //    friendsFollowings.hidden = NO;
    _msgButton.hidden = NO;
    [friendsImage sd_setImageWithURL:[NSURL URLWithString:ChannelObj.profile_image] placeholderImage:[UIImage imageNamed:@"place_holder"]];
    
    appDelegate.IS_celeb = ChannelObj.is_celeb;
    friendId = ChannelObj.user_id;
    
    [friendsCover sd_setImageWithURL:[NSURL URLWithString:ChannelObj.cover_link] placeholderImage:[UIImage imageNamed:@"cover_holder"]];
    
   
    _msgButton.enabled= NO;
    if ([ChannelObj.state isEqualToString:@"ADD_FRIEND"] && [ChannelObj.is_celeb isEqualToString:@"1"]) {
        _Followlbl.text =  NSLocalizedString(@"follow", @"");
        _followlblMid.text = NSLocalizedString(@"follow", @"");
    }else if([ChannelObj.state isEqualToString:@"FRIEND"] && [ChannelObj.is_celeb isEqualToString:@"1"]){
        _Followlbl.text =  NSLocalizedString(@"following", @"");
        _msgImg.image = [UIImage imageNamed:@"speech-bubble-blue"];
        _beamMeLbl.textColor = [UIColor colorWithRed:54/255.0 green:78/255.0 blue:141/255.0 alpha:1.0];
        _msgButton.enabled = YES;
    }
    else if([ChannelObj.state isEqualToString:@"ADD_FRIEND"]){
        _Followlbl.text =  NSLocalizedString(@"add_friend", @"");
    }
    else if([ChannelObj.state isEqualToString:@"PENDING"]){
        _Followlbl.text =  NSLocalizedString(@"cancel", @"");
    }else if([ChannelObj.state isEqualToString:@"ACCEPT_REQUEST"]){
        _Followlbl.text =  NSLocalizedString(@"Respond", @"");
    }
    else if([ChannelObj.state isEqualToString:@"FRIEND"]){
        _Followlbl.text =  NSLocalizedString(@"friends", @"");
        _msgImg.image = [UIImage imageNamed:@"speech-bubble-blue"];
         _beamMeLbl.textColor = [UIColor colorWithRed:54/255.0 green:78/255.0 blue:141/255.0 alpha:1.0];
        _msgButton.enabled = YES;
    }
    else{
        _Followlbl.text = @"UNFOLLOW";
        _followlblMid.text = @"UNFOLLOW";
        _msgButton.enabled = NO;
    }
    
    NSString *decodedString = [Utils getDecryptedTextFor:ChannelObj.full_name];
    
    
    self.titleLbl.text = [NSString stringWithFormat:@"%@%@", decodedString,NSLocalizedString(@"user_corner", nil)];
    if(IS_IPHONE_6Plus){
        _countImage.frame = CGRectMake(_countImage.frame.origin.x - 8, _countImage.frame.origin.y + 5, _countImage.frame.size.width, _countImage.frame.size.width);
        _follwersCount.frame = _countImage.frame;
    }
    friendsNamelbl.hidden = YES;
    if([ChannelObj.is_celeb isEqualToString:@"1"])
    {
     //   _profileViews.center = CGPointMake(self.view.center.x, friendsCover.center.y);
        //        self.zoomImageBtn.center =CGPointMake(self.profileViews.center.x, _profileViews.center.y);
        _followersBtn.enabled = FALSE;
        _followingBtn.enabled = FALSE;
        _bbcbtn.enabled = YES;
        _emiratesbtn.enabled = YES;
        _redbullbtn.enabled = YES;
        bgView.hidden = YES;
        self.celebView.hidden = NO;
        friendsBeamcount.hidden = YES;
        friendsFollowings.hidden = YES;
        _tickImg.hidden = NO;
        _verifiedLbl.hidden = NO;
        _profileBlueView.hidden = NO;
        //_follwersCount.hidden = NO;
        //_countImage.hidden    = NO;
        //_follwersCount.text = ChannelObj.likes_count;
        _userNameMid.text = ChannelObj.full_name;
        //_userNameMid.hidden = NO;
        _adsView.hidden     = NO;
        
        _followersRangelbl.hidden = NO;
        _followersRangelbl.text = [NSString stringWithFormat:@"%@",[Utils abbreviateNumber:ChannelObj.totalBeamsViews withDecimal:1]];
    }
    else {
        //self.profileViews.center = CGPointMake(self.view.center.x, friendsCover.center.y);
        if(!_headerCheck){
            self.headerView.frame = CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, self.headerView.frame.size.height - 51);
            _headerCheck = true;
        }
        _followingBtn.enabled = YES;
        friendsNamelbl.center = CGPointMake(friendsNamelbl.center.x, friendsCover.center.y);
        _picBorderMid.hidden    = YES;
        _friendsImageMid.hidden = YES;
        _followBorderMid.hidden = YES;
        _followlblMid.hidden    = YES;
        _adsBar.hidden          = YES;
        _adsView.hidden         = YES;
        self.celebView.hidden = YES;
        _profileBlueView.hidden = YES;
    }
    friendsNamelbl.text = ChannelObj.full_name;
    if([ChannelObj.beams_count integerValue] == 1){
        friendsBeamcount.text = [[NSString alloc]initWithFormat:@"%@",[Utils abbreviateNumber:ChannelObj.beams_count withDecimal:1]];
        celebBeamcount.text = [[NSString alloc]initWithFormat:@"%@",[Utils abbreviateNumber:ChannelObj.beams_count withDecimal:1]];
    }else
    {
        friendsBeamcount.text = [[NSString alloc]initWithFormat:@"%@",[Utils abbreviateNumber:ChannelObj.beams_count withDecimal:1]];
        celebBeamcount.text = [[NSString alloc]initWithFormat:@"%@",[Utils abbreviateNumber:ChannelObj.beams_count withDecimal:1]];
    }
    if(ChannelObj.friends_count){
        friendsFollowings.text = [[NSString alloc]initWithFormat:@"%@",[Utils abbreviateNumber:ChannelObj.friends_count withDecimal:1]];
        self.cFollowersCount.text = [[NSString alloc]initWithFormat:@"%@",[Utils abbreviateNumber:ChannelObj.friends_count withDecimal:1]];
    }
    if([[NSUserDefaults standardUserDefaults] stringForKey:@"User_Id"] == ChannelObj.user_id){
        friendsStatusbtn.hidden = YES;
        _Followlbl.hidden = NO;
        _Followlbl.text = @"Edit";
        self.friendReqBtn.enabled = NO;
        _profileBlueView.hidden = YES;
        
    }
    else{

        _Followlbl.hidden = NO;
//        self.profileBlueView.hidden = NO;
    }
    NSString *fCount =[Utils abbreviateNumber:ChannelObj.likes_count withDecimal:1];
    friendsFollowers.text = fCount;
   
//    _collectionView.hidden = NO;
}

-(void)addrefreshControl{
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor whiteColor];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
        {
            refreshControl.tintColor = [UIColor lightGrayColor];
        }
    }
    [refreshControl addTarget:self
                       action:@selector(refreshCall)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    
}

- (void)refreshCall {
    pageNum = 1;
    [self GetUsersChannel];
}

-(IBAction)backToMyCorner:(id)sender{
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:@"MoveToMyCorner"
//     object:nil];
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:@"MoveToCornerIndex"
//     object:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"MoveToSpeakerCorner"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"MoveToSpeakerIndex"
     object:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
-(IBAction)openBBC:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.hsbc.com/"]];
}
-(IBAction)openEmirates:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.honda.com/"]];
}
-(IBAction)openREDBull:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.omegawatches.com/"]];
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)getProfile:(id)sender{
    appDelegate.loaduserProfiel = TRUE;
    UIButton *Senderid = (UIButton *)sender;
    currentSelectedIndex = Senderid.tag;
    appDelegate.userToView = ChannelObj.user_id;
    [[NavigationHandler getInstance]MoveToProfile];
}

#pragma mark ScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BOOL endOfTable = (scrollView.contentOffset.y >= ((dataArray.count/3 * 130.0f) - scrollView.frame.size.height)); // Here 150 is row height
    
    if (!cannotScrollMore && endOfTable && !scrollView.dragging && !scrollView.decelerating)
    {
        friendsChannelTable.tableFooterView = footerView;
        [(UIActivityIndicatorView *)[footerView viewWithTag:700] startAnimating];
    }
    else{
        friendsChannelTable.tableFooterView = Nil;
    }
}
-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollOffset = scrollView.contentOffset.y;
    if (scrollOffset >= headerHieght && [currentUser.is_celeb isEqualToString:@"1"])
    {
        self.adsStaticView.hidden = NO;
        [self.view bringSubviewToFront:self.adsStaticView];
    }
    else if (scrollOffset < headerHieght){
        self.adsStaticView.hidden = YES;
        [self.view sendSubviewToBack:self.adsStaticView];
    }
}

#pragma mark Header TABLE
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0;
//}

#pragma mark Table View Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    float returnValue;
    if (IS_IPAD)
        returnValue = 270.0f;
    else if(IS_IPHONE_5)
        returnValue = 110.0f;
    else if(IS_IPHONE_6Plus){
        returnValue = 145.0;
    }
    else
        returnValue = 130.0f;
    return returnValue;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

#pragma mark Table View DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int rows = (int)([dataArray count]/3);
    if([dataArray count] %3 == 1 || [dataArray count] % 3 == 2) {
        rows = rows + 1;
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"videoCells";
    NewHomeCells *cell ;
    currentIndex = (indexPath.row * 3);
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects;
        if(IS_IPAD){
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells_iPad" owner:self options:nil];
        }else if(IS_IPHONE_5){
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
        }else{
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewHomeCells" owner:self options:nil];
        }
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
        cell.leftreplImg.hidden =  NO;
        cell.rightreplImg.hidden = NO;
    }else{
        cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    
    
    VideoModel *tempVideos  = [dataArray objectAtIndex:currentIndex];
    cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
    cell.CH_userName.text = [Utils decodeForEmojis:tempVideos.title];
    cell.CH_userName.text = [Utils decodeForEmojis:cell.CH_userName.text];
    
    cell.Ch_videoLength.text = tempVideos.video_length;
    cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
    if([tempVideos.comments_count isEqualToString:@"0"])
    {
        cell.CH_CommentscountLbl.hidden = YES;
        cell.leftreplImg.hidden = YES;
    }
    else{
        cell.CH_CommentscountLbl.text = tempVideos.comments_count;
    }
    cell.CH_heartCountlbl.text = tempVideos.like_count;
    cell.CH_seen.text = tempVideos.seen_count;
    [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"loading"]];
    
    if([tempVideos.is_anonymous  isEqualToString: @"0"]){
        
    }
    else{
        //cell.CH_Video_Thumbnail.image = [UIImage imageNamed:@"anonymousDp.png"];
        cell.CH_userName.text = @"Anonymous";
        cell.userProfileView.enabled = false;
    }
    //cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /6.2f;
    cell.imgContainer.layer.masksToBounds = YES;
    [cell.CH_Video_Thumbnail roundCorners];
    [cell.CH_Video_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"loading"]];
    
    cell.userProfileView.tag = currentIndex;
    [cell.CH_heart setTag:currentIndex];
    if(tempVideos.beamType == 1){
        cell.CH_heart.hidden = NO;
    }
    else if(tempVideos.beamType == 2){
        cell.CH_heart.hidden = YES;
        NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
        cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];
    }
    else{
        cell.CH_heart.hidden = YES;
    }
    [cell.CH_playVideo setTag:currentIndex];
    
    [cell.CH_flag setTag:currentIndex];
    cell.CH_commentsBtn.enabled = YES;
    cell.CH_RcommentsBtn.enabled = YES;
    [cell.CH_commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.CH_commentsBtn setTag:currentIndex];
    
    currentIndex++;
    if(currentIndex < dataArray.count)
    {
        VideoModel *tempVideos  = [dataArray objectAtIndex:currentIndex];
        [cell.CH_RcommentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.rightTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
        [cell.CH_RcommentsBtn setTag:currentIndex];
        
        [cell.CH_RplayVideo setTag:currentIndex];
        [cell.CH_Rheart setTag:currentIndex];
        cell.CH_RVideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.CH_Rseen.text = tempVideos.seen_count;
        cell.RimgContainer.layer.masksToBounds = YES;
        [cell.CH_RVideo_Thumbnail roundCorners];
        cell.CH_RheartCountlbl.text  = tempVideos.like_count;
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.CH_RCommentscountLbl.hidden = YES;
            cell.rightreplImg.hidden = YES;
        }
        else{
            cell.CH_RCommentscountLbl.text = tempVideos.comments_count;
        }
        cell.CH_RuserName.text = [Utils decodeForEmojis:tempVideos.title];
        cell.CH_RuserName.text = [Utils decodeForEmojis:cell.CH_RuserName.text];
        //        cell.CH_RVideo_Thumbnail.imageURL = [NSURL URLWithString:tempVideos.video_thumbnail_link];
        //        NSURL *url = [NSURL URLWithString:tempVideos.video_thumbnail_link];
        //        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        [cell.CH_RVideo_Thumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@"loading"]];
        
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            
        }
        else{
            //cell.CH_RVideo_Thumbnail.image =[UIImage imageNamed:@"anonymousDp.png"];
            cell.CH_RuserName.text = @"Anonymous";
        }
        if(tempVideos.beamType == 1){
            cell.CH_Rheart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            cell.CH_Rheart.hidden = YES;
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.CH_RuserName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];
        }
        else{
            cell.CH_Rheart.hidden = YES;
        }
        currentIndex++;
    }
    else{
        cell.view2.hidden = YES;
        cell.rightreplImg.hidden = YES;
        cell.CH_RCommentscountLbl.hidden = YES;
    }
    if(currentIndex < dataArray.count){
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [dataArray objectAtIndex:currentIndex];
        cell.rightDate.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.rightUsername.text = [Utils decodeForEmojis:tempVideos.title];
        cell.rightUsername.text = [Utils decodeForEmojis:cell.rightUsername.text];

        
        [cell.rightThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
        [cell.rightThumbnail roundCorners];
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.replyRedBg.hidden = YES;
            cell.replyCountlbl.hidden = YES;
        }
        else{
            cell.replyRedBg.hidden = NO;
            cell.replyCountlbl.hidden = NO;
            cell.replyCountlbl.text = tempVideos.comments_count;
        }
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.dummyright1.hidden = YES;
            cell.anonyImgExtRight.hidden = YES;
            cell.anonyOverlayRight.hidden = YES;
        }
        else{
            cell.anonyImgExtRight.hidden = NO;
            cell.anonyOverlayRight.hidden = NO;
            cell.dummyright1.hidden = NO;
            cell.CH_RuserName.text = @"Anonymous";
        }
        if(tempVideos.beamType == 1){
            cell.CH_Rheart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.rightUsername.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        [cell.showCommentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.showCommentsBtn setTag:currentIndex];
        currentIndex++;
    }
    else{
        cell.view3.hidden = YES;
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}



-(void) GetUsersChannel{
    serverCall = true;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",pageNum];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_USERS_CHANNEL,@"method",
                              token,@"Session_token",pageStr,@"page_no",friendID,@"user_id", nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [refreshControl endRefreshing];
            [_actInd stopAnimating];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int success = [[result objectForKey:@"success"] intValue];
            NSDictionary *users = [result objectForKey:@"profile"];
            
            if(success == 1) {
                //////Profile Response //////
                ///Saving Data
                userChannelObj.state = [result objectForKey:@"state"];
                userChannelObj.beams_count = [users objectForKey:@"beams_count"];
                userChannelObj.friends_count = [users objectForKey:@"following_count"];
                userChannelObj.full_name = [users objectForKey:@"full_name"];
                userChannelObj.cover_link = [users objectForKey:@"cover_link"];
                userChannelObj.user_id = [users objectForKey:@"id"];
                userChannelObj.profile_image = [users objectForKey:@"profile_link"];
                userChannelObj.likes_count = [users objectForKey:@"followers_count"];
                userChannelObj.gender = [users objectForKey:@"gender"];
                userChannelObj.email = [users objectForKey:@"email"];
                userChannelObj.is_celeb = [users objectForKey:@"is_celeb"];
                userChannelObj.totalBeamsViews = [users objectForKey:@"total_beams_views"];
                userChannelObj.bio = [users objectForKey:@"bio"];
                _responseData = [[Followings alloc] init];
                
                _responseData.f_id = [friendID mutableCopy];
                _responseData.fullName = [userChannelObj.full_name mutableCopy];
                _responseData.profile_link =  [userChannelObj.profile_image mutableCopy];
                _responseData.is_celeb = [userChannelObj.is_celeb mutableCopy];
                
                
                NSString *isFollowed = [users objectForKey:@"is_followed"];
                _responseData.isFollowed = [isFollowed intValue];
                ////// Videos Response //////
                
                chPostArray = [result objectForKey:@"posts"];
                if(chPostArray.count > 0){
                    if(pageNum == 1)
                        dataArray = [[NSMutableArray alloc] init];
                    for(NSDictionary *tempDict in chPostArray){
                        VideoModel *_Videos = [[VideoModel alloc] init];
                        _Videos.title = [tempDict objectForKey:@"caption"];
                        _Videos.comments_count = [tempDict objectForKey:@"comment_count"];
                        _Videos.userName = [tempDict objectForKey:@"full_name"];
                        _Videos.topic_id = [tempDict objectForKey:@"topic_id"];
                        _Videos.user_id = [tempDict objectForKey:@"user_id"];
                        _Videos.profile_image = [tempDict objectForKey:@"profile_link"];
                        _Videos.like_count = [tempDict objectForKey:@"like_count"];
                        _Videos.like_by_me = [tempDict objectForKey:@"liked_by_me"];
                        _Videos.seen_count = [tempDict objectForKey:@"seen_count"];
                        _Videos.video_angle = [[tempDict objectForKey:@"video_angle"] intValue];
                        _Videos.video_thumbnail_gif = [tempDict objectForKey:@"video_thumbnail_gif"];
                        _Videos.beam_share_url = [tempDict objectForKey:@"beam_share_url"];
                        
                        _Videos.video_link = [tempDict objectForKey:@"video_link"];
                        _Videos.m3u8_video_link = [tempDict objectForKey:@"m3u8_video_link"];
                        _Videos.video_thumbnail_link = [tempDict objectForKey:@"video_thumbnail_link"];
                        _Videos.videoID = [tempDict objectForKey:@"id"];
                        _Videos.Tags = [tempDict objectForKey:@"tag_friends"];
                        _Videos.video_length = [tempDict objectForKey:@"video_length"];
                        _Videos.is_anonymous = [tempDict objectForKey:@"is_anonymous"];
                        _Videos.reply_count = [tempDict objectForKey:@"reply_count"];
                        _Videos.current_datetime= [tempDict objectForKey:@"current_datetime"];
                        _Videos.uploaded_date   = [tempDict objectForKey:@"uploaded_date"];
                        _Videos.isMute = @"0";
                        _Videos.beamType = [[tempDict objectForKey:@"beam_type"] intValue];
                        if([tempDict objectForKey:@"original_beam_data"]){
                            _Videos.originalBeamData = [tempDict objectForKey:@"original_beam_data"];
                        }
                        
                        bool found = false;
                        
                        for(VideoModel *eachModel in dataArray)
                        {
                            if([eachModel.videoID isEqualToString:_Videos.videoID])
                            {
                                found = true;
                                break;
                            }
                        }
                        
                        if(!found)
                        {
                            [dataArray addObject:_Videos];
                        }
                    }
                    
                    //[friendsChannelTable reloadData];
                }
                else
                {
                    cannotScrollMore = TRUE;
                    ChannelObj = userChannelObj;
                    //                    [self initViewWithData];
                }
                ChannelObj = userChannelObj;
                [self initViewWithData];
                serverCall = false;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [friendsChannelTable reloadData];
                    [_collectionView reloadData];
                    footerIndicator.hidden = YES;
                    _collectionView.hidden = NO;
                });
            }
            
        }
        else{
            [refreshControl endRefreshing];
            serverCall = false;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Network Problem. Try Again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            //            [alert show];
        }
    }];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!serverCall && indexPath.row == dataArray.count/3 - 1  && !cannotScrollMore) {
        pageNum++;
        [self GetUsersChannel];
    }
    else if(cannotScrollMore){
        friendsChannelTable.tableFooterView = Nil;
    }
}

#pragma mark - CollectionView Delegates

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    float returnValue;
    if(indexPath.row == 0) {
        if(IS_IPAD){
            if([ChannelObj.is_celeb isEqualToString:@"1"]) {
                returnValue = 520;
            } else {
                returnValue = 460;
            }
        } else {
            if([ChannelObj.is_celeb isEqualToString:@"1"]) {
                //returnValue = 280;
                returnValue = 390;
            } else {
                returnValue = 325;
            }
        }
    }
    else{

        if (IS_IPHONE_5)
            returnValue = ((collectionView.frame.size.width-12)/3) ;
        else
            returnValue = ((collectionView.frame.size.width-12)/3) ;
    }
    
    
    if (indexPath.row == 0){
        
        return CGSizeMake(collectionView.frame.size.width, returnValue);
        
    }else{
        if(sharedManager.isListView) {
            return CGSizeMake(collectionView.frame.size.width, 250);
        }
        return CGSizeMake(returnValue, returnValue+10);
        
    }
    
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 6.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (!serverCall && indexPath.row == dataArray.count - 1  && !cannotScrollMore) {
        [footerIndicator startAnimating];
        if(!_firstCheck){
//            footerIndicator.hidden = NO;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        } else {
            _firstCheck = NO;
        }
        pageNum++;
        [self GetUsersChannel];
    }
    else if(cannotScrollMore){
        friendsChannelTable.tableFooterView = Nil;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return dataArray.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0) {
        HeaderViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"headerCell" forIndexPath:indexPath];
        [cell.mainView addSubview:_headerView];
        return cell;
    } else {
        
        VideoViewCell *cell;
        _currentIndexHome = indexPath.row-1;
        
        cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
        
        VideoModel *tempVideos = [[VideoModel alloc]init];
        tempVideos  = [dataArray objectAtIndex:_currentIndexHome];
        
        if(sharedManager.isListView) {
            cell.CH_userName.font = [UIFont fontWithName:@"Montserrat-Regular" size:13];
            cell.CH_VideoTitle.font = [UIFont fontWithName:@"Montserrat-Regular" size:13];
            cell.durationLbl.hidden = NO;
            cell.durationLbl.text = tempVideos.video_length;
            //cell.dotsImg.hidden = NO;

        } else {
            cell.CH_userName.font = [UIFont fontWithName:@"Montserrat-Regular" size:9];
            cell.CH_VideoTitle.font = [UIFont fontWithName:@"Montserrat-Regular" size:9];
            cell.durationLbl.hidden = YES;
            cell.dotsImg.hidden = YES;
        }
        
     
        
        if(tempVideos.beamType == 1){
            if(sharedManager.isListView) {
                cell.listRebeam.hidden = NO;
                cell.rebeam1.hidden = YES;
            } else {
                cell.listRebeam.hidden = YES;
                cell.rebeam1.hidden = NO;
            }
        }
        else{
            cell.listRebeam.hidden = YES;
            cell.rebeam1.hidden = YES;
        }
        
        cell.CH_userName.text = [Utils decodeForEmojis:tempVideos.title];
        cell.CH_userName.text = [Utils decodeForEmojis:cell.CH_userName.text];

        
        cell.CH_VideoTitle.text = [Utils returnDate:tempVideos.uploaded_date];
        cell.leftTimeStamp.text = [Utils getTimeDifference:tempVideos.uploaded_date time2:tempVideos.current_datetime];
        if([tempVideos.comments_count isEqualToString:@"0"])
        {
            cell.CH_CommentscountLbl.hidden = YES;
            cell.leftreplImg.hidden = YES;
        }
        else{
            cell.CH_CommentscountLbl.hidden = NO;
            cell.leftreplImg.hidden = NO;
            cell.CH_CommentscountLbl.text = tempVideos.comments_count;
        }
        
        cell.CH_heartCountlbl.text = tempVideos.like_count;
        cell.CH_seen.text = tempVideos.seen_count;
        cell.Ch_videoLength.text = tempVideos.video_length;
        if([tempVideos.is_anonymous  isEqualToString: @"0"]){
            cell.annonyOverlayLeft.hidden = YES;
            cell.annonyImgLeft.hidden = YES;
            //cell.CH_userName.text = @"Anonymous";
            cell.dummyLeft1.hidden  = YES;
        }
        else{
            //        cell.dummyLeft1.image = [UIImage imageNamed:@"btanonymous.png"];
            cell.annonyOverlayLeft.hidden = NO;
            cell.annonyImgLeft.hidden = NO;
            //cell.CH_userName.text = @"Anonymous";
            cell.dummyLeft1.hidden  = NO;
        }
        if(tempVideos.beamType == 1){
            cell.CH_heart.hidden = NO;
        }
        else if(tempVideos.beamType == 2){
            cell.CH_heart.hidden = YES;
            NSString *sharedby =  [tempVideos.originalBeamData objectForKey:@"full_name"];
//            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:sharedby]];
            cell.CH_userName.text  = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"shared_by", nil),[Utils getTrimmedString:[Utils getDecryptedTextFor:sharedby]]];

        }
        else{
            cell.CH_heart.hidden = YES;
        }
        
        //        NSLog(@"%@",tempVideos.video_thumbnail_gif);
        if(tempVideos.video_thumbnail_gif!=nil && [tempVideos.video_thumbnail_gif length]>0){
            
            cell.CH_Video_Thumbnail.hidden = YES;
            
            if (tempVideos.imageThumbnail && tempVideos.animatedImage) {
                cell.mainThumbnail.hidden = YES;
                [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                cell.CH_Video_Thumbnail.hidden = NO;
            }else if (tempVideos.imageThumbnail && !tempVideos.animatedImage) {
                [cell.mainThumbnail setImage:tempVideos.imageThumbnail];
                [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                    cell.mainThumbnail.hidden = NO;
                    tempVideos.animatedImage = result.animatedImage;
                    [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                    cell.CH_Video_Thumbnail.hidden = NO;
                }];
            }else if (!tempVideos.imageThumbnail && tempVideos.animatedImage) {
                cell.mainThumbnail.hidden = YES;
                [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                cell.CH_Video_Thumbnail.hidden = NO;
            }else if (!tempVideos.imageThumbnail && !tempVideos.animatedImage) {
                
                [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    if (!error) {
                        tempVideos.imageThumbnail = image;
                        [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                            tempVideos.animatedImage = result.animatedImage;
                            [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                            cell.mainThumbnail.hidden = NO;
                            cell.CH_Video_Thumbnail.hidden = NO;
                        }];
                    }else{
                        [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                            tempVideos.animatedImage = result.animatedImage;
                            [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                            cell.mainThumbnail.hidden = NO;
                            cell.CH_Video_Thumbnail.hidden = NO;
                        }];
                    }
                }];
                
                //                [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:@""] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                //
                //                    if (image && finished) {
                //
                //                    }
                //                }];
                
            }
            
            //            [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            //
            //            }];
            //            [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
            
            //            [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
            //
            //                cell.mainThumbnail.hidden = YES;
            //
            //            }];
            
            //(^PINRemoteImageManagerImageCompletion)
            
            
        }else{
            
            
            cell.mainThumbnail.hidden = NO;
            cell.CH_Video_Thumbnail.hidden = YES;
            
            [cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (!error) {
                    tempVideos.imageThumbnail = image;
                    //                    [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                    //                        tempVideos.animatedImage = result.animatedImage;
                    //                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                    //                        cell.mainThumbnail.hidden = YES;
                    //                        cell.CH_Video_Thumbnail.hidden = NO;
                    //                    }];
                }else{
                    
                    NSLog(@"%@", error);
                    
                }
                //else{
                //                    [cell.CH_Video_Thumbnail pin_setImageFromURL:[NSURL URLWithString:tempVideos.video_thumbnail_gif] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
                //                        tempVideos.animatedImage = result.animatedImage;
                //                        [cell.CH_Video_Thumbnail setAnimatedImage:tempVideos.animatedImage];
                //                        cell.mainThumbnail.hidden = YES;
                //                        cell.CH_Video_Thumbnail.hidden = NO;
                //                    }];
                //                }
            }];
            
            //[cell.mainThumbnail sd_setImageWithURL:[NSURL URLWithString:tempVideos.video_thumbnail_link] placeholderImage:[UIImage imageNamed:@""]];
            
            
            
        }
        
        
        
        
        cell.CH_commentsBtn.enabled = YES;
        if (tempVideos.isLocal && !APP_DELEGATE.hasInet) {
            
            [cell.cellProgress removeFromSuperview];
            
            cell.CH_commentsBtn.backgroundColor = [UIColor colorWithRed:111/255 green:113/255 blue:121/255 alpha:1];
            cell.CH_commentsBtn.alpha = 0.55;
            cell.CH_VideoTitle.text = tempVideos.current_datetime;
            cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
            cell.CH_CommentscountLbl.text = @"!";
            cell.leftreplImg.hidden =  NO;
            cell.CH_commentsBtn.enabled = NO;
            if (cell.cellProgress!=nil){
                
                cell.cellProgress.hidden = true;
                [cell.cellProgress removeFromSuperview];
                cell.cellProgress = nil;
            }
        }
        else if(tempVideos.isLocal){
            
            //self.roundedPView.hidden = NO;
            
            
            cell.CH_commentsBtn.enabled = NO;
            cell.leftreplImg.hidden =  YES;
            cell.CH_VideoTitle.text = tempVideos.current_datetime;
            cell.CH_Video_Thumbnail.image = [UIImage imageWithData:tempVideos.profileImageData];
            
            if(![cell.cellProgress isDescendantOfView:cell.contentView]) {
                
                //                cell.cellProgress = [[PWProgressView alloc] initWithFrame:cell.view1.frame];
                
                if(IS_IPHONE_5) {
                    cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, 100, 90)];
                }   else if(IS_IPAD){
                    cell.cellProgress = [[PWProgressView alloc] init];
                    cell.cellProgress.frame = cell.view1.frame;
                }
                else {
                    cell.cellProgress = [[PWProgressView alloc] initWithFrame:CGRectMake(cell.view1.frame.origin.x, cell.view1.frame.origin.y, 123, 125)];
                }
                
                cell.cellProgress.layer.cornerRadius = cell.view1.frame.size.width / 6.2f;
                cell.cellProgress.clipsToBounds = YES;
                
                cell.cellProgress.hidden = false;
                
                
                //self.roundedPView = cell.cellProgress;
                
                [cell.contentView addSubview:cell.cellProgress];
            }else{
                
                
            }
            
            
        }
        else{
            cell.CH_commentsBtn.enabled = YES;
            cell.CH_commentsBtn.backgroundColor = [UIColor clearColor];
            
            if (cell.cellProgress!=nil){
                
                cell.cellProgress.hidden = true;
                [cell.cellProgress removeFromSuperview];
                cell.cellProgress = nil;
            }
            
        }
        
        
        
        if(IS_IPAD)
            //cell.imgContainer.layer.cornerRadius  = cell.imgContainer.frame.size.width /7.4f;
            cell.imgContainer.layer.masksToBounds = YES;
        if(sharedManager.isListView) {
            
        } else {
//            [cell.CH_Video_Thumbnail roundCorners];
        }
        [cell.view1 setBackgroundColor:[UIColor clearColor]];

        [cell.CH_heart setTag:_currentIndexHome];
        [cell.CH_playVideo setTag:_currentIndexHome];
        
        [cell.CH_flag setTag:_currentIndexHome];
        [cell.CH_commentsBtn addTarget:self action:@selector(ShowCommentspressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CH_commentsBtn setTag:_currentIndexHome];
        
        
        [cell setBackgroundColor:[UIColor clearColor]];

        if(!tempVideos.isLocal){
            
            [cell.cellProgress removeFromSuperview];
            
        }
        if(dataArray.count == 1 && !sharedManager.isListView) {
            cell.frame = CGRectMake(10, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
        }
        return cell;
    
}
}

-(void)presentActionSheet:(NSString *)tittleForBtn status:(int)status{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:Nil
                                 message:Nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* accept = [UIAlertAction
                             actionWithTitle:tittleForBtn
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if(status == 1){
                                     _Followlbl.text =  NSLocalizedString(@"cancel", @"");
                                     ChannelObj.state = @"PENDING";
                                     currentUser.status = @"PENDING";
                                     [self sendFriendRequest];
                                 }else if(status == 2){
                                     _Followlbl.text =  NSLocalizedString(@"add_friend", @"");
                                     ChannelObj.state = @"ADD_FRIEND";
                                     currentUser.status = @"ADD_FRIEND";
                                     [self rejectRequest];
                                 }else if(status == 3){
                                     _Followlbl.text =  NSLocalizedString(@"friends", @"");
                                     ChannelObj.state = @"FRIEND";
                                     currentUser.status = @"FRIEND";
                                     [self acceptRequest];
                                 }else if(status == 4){
                                     _Followlbl.text =  NSLocalizedString(@"add_friend", @"");
                                     ChannelObj.state = @"ADD_FRIEND";
                                     currentUser.status = @"ADD_FRIEND";
                                     [self sendDeleteFriend];
                                 }
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* reject = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"reject", @"")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 _Followlbl.text =  NSLocalizedString(@"add_friend", @"");
                                 ChannelObj.state = @"ADD_FRIEND";
                                 currentUser.status = @"ADD_FRIEND";
                                 [self rejectRequest];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* cancel  = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"cancel", @"")
                              style:UIAlertActionStyleCancel
                              handler:^(UIAlertAction * action)
                              {
                                  [view dismissViewControllerAnimated:YES completion:nil];
                                  
                              }];
    
    [view addAction:accept];
    if(status == 3)
        [view addAction:reject];
    [view addAction:cancel];
   
    view.popoverPresentationController.sourceView = self.view;
    view.popoverPresentationController.sourceRect = CGRectMake(30, 170, 700, 50);
    [view.popoverPresentationController setPermittedArrowDirections:0];
   
    [self presentViewController:view animated:YES completion:nil];
}
- (IBAction)UserStatusbtnPressed:(id)sender {
    if([ChannelObj.is_celeb isEqualToString:@"1"]){
        if([ChannelObj.state isEqualToString:@"ADD_FRIEND"]){
            NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"1" forKey:@"isFriend"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BecameFriend" object:nil userInfo:dictionary];
            _Followlbl.text =  NSLocalizedString(@"following", @"");
            ChannelObj.state = @"FRIEND";
            currentUser.status = @"FRIEND";
            [self sendFriendRequest];
        }else if([ChannelObj.state isEqualToString:@"ACCEPT_REQUEST"]){
            _Followlbl.text =  NSLocalizedString(@"following", @"");
            ChannelObj.state = @"FRIEND";
            currentUser.status = @"FRIEND";
            [self acceptRequest];
        }
        else{
            _Followlbl.text = NSLocalizedString(@"follow", @"");
            ChannelObj.state = @"ADD_FRIEND";
            currentUser.status = @"ADD_FRIEND";
            NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"0" forKey:@"isFriend"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BecameFriend" object:nil userInfo:dictionary];
            [self sendDeleteFriend];
        }
        
    }else{
        if ([ChannelObj.state isEqualToString:@"ADD_FRIEND"]) {
            [self presentActionSheet:NSLocalizedString(@"add_friend", @"") status:1];
        }
        else if([ChannelObj.state isEqualToString:@"ACCEPT_REQUEST"]) {
            [self presentActionSheet:NSLocalizedString(@"accept", @"") status:3];
        }
        else if([ChannelObj.state isEqualToString:@"PENDING"]){
            [self presentActionSheet:NSLocalizedString(@"delete_request", @"") status:2];
        }
        else if([ChannelObj.state isEqualToString:@"FRIEND"]){
            [self presentActionSheet:NSLocalizedString(@"Unfriend", @"") status:4];
        }
        else{
            _Followlbl.text = NSLocalizedString(@"follow", @"");
            NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"0" forKey:@"isFriend"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BecameFriend" object:nil userInfo:dictionary];
            [self sendDeleteFriend];
        }
    }
}
- (void) sendFriendRequest{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_SEND_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                if([_responseData.is_celeb isEqualToString:@"0"])
                    _responseData.status = @"PENDING";
                else{
                    _responseData.status = @"FRIEND";
                }
                if([Utils userIsCeleb] && _responseData.isFollowed == 0){
                    [sharedManager.followers insertObject:_responseData atIndex:0];
                }
                else if(![Utils userIsCeleb]){
                    [sharedManager.followers insertObject:_responseData atIndex:0];
                }
                [self isFriendAlreadyPresent:_responseData];
                // _msgButton.enabled=YES;
            }
        }else{
            //            _Followlbl.text = @"FOLLOW";
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
}
#pragma mark ISAlreadyPresent
-(BOOL)isFriendAlreadyPresent:(Followings*)fUser{
    for (int i =0; i< sharedManager.followers.count; i++){
        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
        if([tempObj.f_id isEqualToString:fUser.f_id]){
            tempObj.status = fUser.status;
            return YES;
        }
    }
    return NO;
}

- (void)rejectRequest{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_REJECT_REEQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                //                _responseData.status =   @"FRIEND";
                //                [sharedManager.followings addObject:_responseData];
                //                _msgButton.enabled=YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateRequestCount" object:nil];
                if(sharedManager.followers.count > 0){
                    NSInteger index = [sharedManager.followers indexOfObject:_responseData];
                    if (index != NSNotFound) {
                        if( [Utils userIsCeleb] && (_responseData.isFollowed == 0)){
                            [sharedManager.followers  removeObjectAtIndex:index];
                        }
                        else if(![Utils userIsCeleb]){
                            [sharedManager.followers  removeObjectAtIndex:index];
                        }
                    }
                }
                
            }
        }else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
}
- (void)sendDeleteFriend{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_DELETE_FRIEND,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                _msgButton.enabled=NO;
                _responseData.status =  @"ADD_FRIEND";
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"f_id != %@", _responseData.f_id];
                if( [Utils userIsCeleb] && _responseData.isFollowed == 0){
                    [sharedManager.followers  filterUsingPredicate:predicate];
                } else if(![Utils userIsCeleb]){
                    [sharedManager.followers  filterUsingPredicate:predicate];
                }
                
                NSString *countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.friends_count];
                NSInteger previousCount = [countSuffix integerValue];
                previousCount --;
                countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount];
                sharedManager._profile.friends_count = countSuffix;
                countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
                [Utils setChacedFriendsCount:countSuffix];
                
                NSInteger previousCount2 = [friendsFollowings.text integerValue];
                if(previousCount2 > 0)
                {
                    previousCount2 --;
                    countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount2];
                    friendsFollowings.text = [[NSString alloc]initWithFormat:@"%@",countSuffix];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshRequests" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshFriendList" object:nil];

            }
        }else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
    
}
-(void)acceptRequest{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_ACCEPT_REQUEST,@"method",
                              token,@"session_token",friendId,@"friend_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateRequestCount" object:nil];
                _msgButton.enabled=YES;
                _responseData.status =  @"FRIEND";
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"f_id != %@", _responseData.f_id];
                [sharedManager.followings filterUsingPredicate:predicate];
                [self isFriendAlreadyPresent:_responseData];
                
                NSString *countSuffix = [NSString stringWithFormat:@"%@",sharedManager._profile.friends_count];
                NSInteger previousCount = [countSuffix integerValue];
                previousCount ++;
                countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount];
                sharedManager._profile.friends_count = countSuffix;
                countSuffix = [Utils abbreviateNumber:countSuffix withDecimal:1];
                [Utils setChacedFriendsCount:countSuffix];
                
                NSInteger previousCount2 = [friendsFollowings.text integerValue];
                previousCount2 ++;
                countSuffix = [NSString stringWithFormat:@"%ld",(long)previousCount2];
                friendsFollowings.text = [[NSString alloc]initWithFormat:@"%@",countSuffix];

                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshRequests" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshFriendList" object:nil];

            }
        }else{
            //_Followlbl.text = @"UNFOLLOW";
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
    
}
#pragma mark Get Comments

-(void) ShowCommentspressed:(UIButton *)sender{
    
    UIButton *CommentsBtn = (UIButton *)sender;
    currentSelectedIndex = CommentsBtn.tag;
    VideoModel *tempVideos   = [dataArray objectAtIndex:currentSelectedIndex];
    postID                          = tempVideos.videoID;
    ParentCommentID = @"-1";
    CommentsVC *commentController ;
    if(IS_IPAD)
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPad" bundle:nil];
    else  if (IS_IPHONEX){
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC_iPhonex" bundle:nil];
    }
    else
        commentController = [[CommentsVC alloc] initWithNibName:@"CommentsVC" bundle:nil];
    
    commentController.commentsObj   = Nil;
    commentController.postArray     = tempVideos;
    commentController.cPostId       = postID;
    commentController.isFirstComment = TRUE;
    commentController.isComment     = FALSE;
    [[self navigationController] pushViewController:commentController animated:YES];
    
}
- (IBAction)getFollowings:(id)sender {
    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = nil;
    commentController.titles        = [NSString stringWithFormat:@"%@ %@",[Utils getDecryptedTextFor:ChannelObj.full_name],NSLocalizedString(@"friends", nil)];
    commentController.NoFriends     = FALSE;
    commentController.userId        = userChannelObj.user_id;
    commentController.loadFollowings= TRUE;
    commentController.isLocalSearch = YES;
    [[self navigationController] pushViewController:commentController animated:YES];
    
}

- (IBAction)getFollowers:(id)sender {
    FriendsVC *commentController; //   = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    if (IS_IPHONEX){
//        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC_IphoneX" bundle:nil];
//    }else{
        commentController    = [[FriendsVC alloc] initWithNibName:@"FriendsVC" bundle:nil];
//    }
    commentController.friendsArray  = nil;
    commentController.titles        = [NSString stringWithFormat:@"%@ %@",[Utils getDecryptedTextFor:ChannelObj.full_name],NSLocalizedString(@"followers", nil)];
    commentController.NoFriends     = FALSE;
    commentController.loadFollowings= FALSE;
    commentController.userId        = userChannelObj.user_id;
    [[self navigationController] pushViewController:commentController animated:YES];
}
- (IBAction)btnMsg:(id)sender {
    Followings *f =[[Followings alloc]init];
    [f setF_id:ChannelObj.user_id];
    [f setFullName:ChannelObj.full_name];
    FriendChatVC *frndvc;// = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    if (IS_IPHONEX){
        frndvc = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC_IphoneX" bundle:nil];
    }else{
        frndvc = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    }
    frndvc.isFriend = YES;
    frndvc.seletedFollowing = f;
    frndvc.isLocalSearch = YES;
    [[self navigationController] pushViewController:frndvc animated:YES];
}

- (IBAction)gridListBtnPressed:(id)sender {
    if(sharedManager.isListView) {
        sharedManager.isListView = NO;
        _gridImageView.image = [UIImage imageNamed:@"Grid_blue"];
        _gridLbl.textColor = [UIColor colorWithRed:54/255.0 green:78/255.0 blue:141/255.0 alpha:1.0];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"listView"];
    } else {
        sharedManager.isListView = YES;
        _gridImageView.image = [UIImage imageNamed:@"Grid"];
        _gridLbl.textColor = [UIColor lightGrayColor];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"listView"];
    }
    sharedManager.isMyCornerViewChanged = YES;
    sharedManager.isFriendViewChanged = YES;
    sharedManager.isSpeakerViewChanged = YES;
    [_collectionView reloadData];
}
#pragma mark ZoomImage
-(IBAction)zoomImage:(id)sender{
    [EXPhotoViewer showImageFrom:friendsImage];
}
-(NSString *)getFid{
    return friendID;
}
-(void)setFriendStatus:(int)status{
    if(status == 1){
        _Followlbl.text =  NSLocalizedString(@"cancel", @"");
        ChannelObj.state = @"PENDING";
        currentUser.status = @"PENDING";
    }else if(status == 2){
        _Followlbl.text =  NSLocalizedString(@"add_friend", @"");
        ChannelObj.state = @"ADD_FRIEND";
        currentUser.status = @"ADD_FRIEND";
    }else if(status == 3){
        _Followlbl.text =  NSLocalizedString(@"friends", @"");
        ChannelObj.state = @"FRIEND";
        currentUser.status = @"FRIEND";
    }else if(status == 4){
        _Followlbl.text =  NSLocalizedString(@"add_friend", @"");
        ChannelObj.state = @"ADD_FRIEND";
        currentUser.status = @"ADD_FRIEND";
        
    }
    
}

- (void)showNoInternetAlert {
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"no_internet", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
//    [alertView show];
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"no_internet", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [alertView addAction:ok];
    [self presentViewController:alertView animated:YES completion:nil];
}
@end
