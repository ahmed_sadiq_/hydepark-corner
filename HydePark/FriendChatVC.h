//
//  FriendChatVC.h
//  HydePark
//
//  Created by Samreen Noor on 28/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Followings.h"
#import <MediaPlayer/MediaPlayer.h>
#import "AVFoundation/AVFoundation.h"
#import "ASIFormDataRequest.h"
#import "FriendChat.h"
#import "PWProgressView.h"
@interface FriendChatVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,MPMediaPickerControllerDelegate,AVAudioRecorderDelegate,ASIHTTPRequestDelegate> {
    NSTimer *timerToupdateLbl;
    int secondsLeft;
    NSTimer* audioTimeOut;
    NSData *audioData;
//    NSString *secondsConsumed;
   // NSData *profileData;
    NSData *audioprofileData;
    BOOL isRecording;
    BOOL isAudio;
    BOOL isForRefreshOnly;
}
@property (strong, nonatomic) NSString *frndName;
@property (strong, nonatomic) IBOutlet UIImageView *background;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (nonatomic)BOOL isLocalSearch;
@property (weak, nonatomic) IBOutlet UIButton *audioRecordBtn;
@property (weak, nonatomic) IBOutlet UIButton *closeBtnAudio;
@property (strong, nonatomic) IBOutlet UIView *uploadAudioView;
@property (strong, nonatomic) NSString *frndId;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) FriendChat *selectedFriend;
@property (strong, nonatomic) Followings *seletedFollowing;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *lblFrndName;
@property (strong, nonatomic) NSData *movieData;
@property (strong, nonatomic) NSData *profileData;
@property (nonatomic, strong) PWProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *countDownlabel;
@property (weak, nonatomic) IBOutlet UIImageView *audioBtnImage;
@property (assign) BOOL isFriend;
@property (assign) BOOL isUpload; /// inbox from uploadbeamvc
@property (assign) BOOL isUploadAudio; /// inbox from uploadbeamvc
@property (assign ,nonatomic) BOOL reloadCheck;
@property (assign ,nonatomic) BOOL isPopToRoot;
@property (assign ,nonatomic) BOOL isUploading;
@property (strong, nonatomic) NSString *video_duration;
@property (strong, nonatomic) NSString *secondsConsumed;
@end
