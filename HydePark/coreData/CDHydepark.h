//
//  CDHydepark.h
//  HydePark
//
//  Created by Samreen Noor on 09/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface CDHydepark : NSManagedObject

@property (nullable, nonatomic, retain) NSString *privacySelected;
@property (nullable, nonatomic, retain) NSString *commentAllowed;
@property (nullable, nonatomic, retain) NSString *video_duration;
@property (nullable, nonatomic, retain) NSString *textToshare;
@property (nullable, nonatomic, retain) NSString *postID;
@property (nullable, nonatomic, retain) NSString *parentCommentID;
@property (nullable, nonatomic, retain) NSData *file;
@property (nullable, nonatomic, retain) NSString *userSession;
@property (nullable, nonatomic, retain) NSString *anony;
@property (nullable, nonatomic, retain) NSString  *isAudio;
@property (nullable, nonatomic, retain) NSData  *profileData;
@property (nullable, nonatomic, retain) NSString  *isAnonymous;
@property (nullable, nonatomic, retain) NSString  *test;
@property (nullable, nonatomic, retain) NSString  *isComment;
@property (nullable, nonatomic, retain) NSString  *timestamp;

@end
