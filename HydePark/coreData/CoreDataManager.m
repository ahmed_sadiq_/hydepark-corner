//
//  CoreDataManager.m
//  TestCoreData
//
//  Created by Samreen Noor on 08/08/2016.
//  Copyright © 2016 Apple Txlabz. All rights reserved.
//

#import "CoreDataManager.h"
#import "Constants.h"
#import "CDHydepark.h"
#import "NSMutableArray+QueueAdditions.h"
@interface CoreDataManager ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)setupManagedObjectContext;

@end

@implementation CoreDataManager

static CoreDataManager *coreDataManager;

+ (CoreDataManager *)sharedManager
{
    if (!coreDataManager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            coreDataManager = [[CoreDataManager alloc] init];
        });
        
    }
    
    return coreDataManager;
}

#pragma mark - setup

- (id)init
{
    self = [super init];
    
    if (self) {
        [self setupManagedObjectContext];
        //_offlineContentArray = [[NSMutableArray alloc]init];
    }
    
    return self;
}

- (void)setupManagedObjectContext
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *documentDirectoryURL = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *persistentURL = [documentDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", sProjectName]];
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:sProjectName withExtension:@"momd"];
    
    self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    self.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    
    NSError *error = nil;
    NSPersistentStore *persistentStore = [self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                                                       configuration:nil
                                                                                                 URL:persistentURL
                                                                                             options:nil
                                                                                               error:&error];
    
    
    if (persistentStore) {
        self.managedObjectContext = [[NSManagedObjectContext alloc] init];
        self.managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
    } else {
        NSLog(@"ERROR: %@", error.description);
    }
    
}

- (void)saveDataInManagedContextUsingBlock:(void (^)(BOOL saved, NSError *error))savedBlock
{
    NSError *saveError = nil;
    savedBlock([self.managedObjectContext save:&saveError], saveError);
}

- (NSFetchedResultsController *)fetchEntitiesWithClassName:(NSString *)className
                                           sortDescriptors:(NSArray *)sortDescriptors
                                        sectionNameKeyPath:(NSString *)sectionNameKeypath
                                                 predicate:(NSPredicate *)predicate


{
    
    NSArray *event = (NSArray *)[NSEntityDescription insertNewObjectForEntityForName:@"CDHydepark" inManagedObjectContext:_managedObjectContext];
    
    
    NSFetchedResultsController *fetchedResultsController;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:className
                                              inManagedObjectContext:self.managedObjectContext];
    fetchRequest.entity = entity;
    fetchRequest.sortDescriptors = sortDescriptors;
    fetchRequest.predicate = predicate;
    
    fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                   managedObjectContext:self.managedObjectContext
                                                                     sectionNameKeyPath:sectionNameKeypath
                                                                              cacheName:nil];
    
    NSError *error = nil;
    BOOL success = [fetchedResultsController performFetch:&error];
    
    if (!success) {
        NSLog(@"fetchManagedObjectsWithClassName ERROR: %@", error.description);
    }
    
    return fetchedResultsController;
}



- (id)createEntityWithClassName:(NSString *)className
           attributesDictionary:(NSDictionary *)attributesDictionary
{
    NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:className
                                                            inManagedObjectContext:self.managedObjectContext];
    [attributesDictionary enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
        
        [entity setValue:obj forKey:key];
        
    }];
    
    return entity;
}

- (void)deleteEntity:(NSManagedObject *)entity
{
    [self.managedObjectContext deleteObject:entity];
}

- (BOOL)uniqueAttributeForClassName:(NSString *)className
                      attributeName:(NSString *)attributeName
                     attributeValue:(id)attributeValue
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K like %@", attributeName, attributeValue];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:attributeName ascending:YES]];
    
    NSFetchedResultsController *fetchedResultsController = [self fetchEntitiesWithClassName:className
                                                                            sortDescriptors:sortDescriptors
                                                                         sectionNameKeyPath:nil
                                                                                  predicate:predicate];
    
    
    return fetchedResultsController.fetchedObjects.count == 0;
}

-(CDHydepark *)AddRecordToCoreDataFile:(NSData *)file isAudio:(NSString *)isaudio withparentCommentID:(NSString *)parentCommentID CommentAllowed:(NSString *)commentAllowed TextToshare:(NSString *)textToshare Anony:(NSString *)anony VideoDuration:(NSString *)video_duration PstID:(NSString *)postID PrivacySelected:(NSString *)privacySelected profileData:(NSData *)proflData AndUserSession:(NSString *)userSession AndIsComment:(NSString *)isComment AndTimeStamp:(NSString *)timestamp{
    
    CDHydepark *event = (CDHydepark *)[NSEntityDescription insertNewObjectForEntityForName:@"CDHydepark" inManagedObjectContext:_managedObjectContext];
    [event setIsAudio:isaudio];
    [event setFile:file];
    [event setAnony:anony];
    [event setPostID:postID];
    [event setTextToshare:textToshare];
    [event setUserSession:userSession];
    [event setCommentAllowed:commentAllowed];
    [event setVideo_duration:video_duration];
    [event setParentCommentID:parentCommentID];
    [event setPrivacySelected:privacySelected];
    [event setProfileData:proflData];
    [event setIsComment:isComment];
    [event setTimestamp:timestamp];
    
    NSError *error;
    if (![_managedObjectContext save:&error])
    {
        NSLog(@"Error..%@",error);
        
    }
    else
    {
        //[_offlineContentArray enqueue:event];
        
    }
    return event;
}


-(NSArray *)getRecords:(NSString *)projIDString
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CDHydepark"];
    
    NSError *error = nil;
    //[request setResultType:NSDictionaryResultType];
    
    //NSManagedObjectContext *context = [self managedObjectContext];
    //NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Message"];
    //[fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"conversationId == %@", mModel.conversationId]];
    
    [request setPredicate:[NSPredicate predicateWithFormat:@"isComment == 0"]];

    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CDHydepark" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (!results) {
        NSLog(@"Error fetching Employee objects: %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }
    if([results count] > 0)
    {
        
        //CDHydepark *person = (CDHydepark *)[results objectAtIndex:0];
        //[self.managedObjectContext deleteOxcbject:person];
        return results;
    }
    else
        return nil;
    
}

-(NSArray *)getComments:(NSString *)projIDString
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CDHydepark"];
    
    NSError *error = nil;
    
    [request setPredicate:[NSPredicate predicateWithFormat:@"isComment == 1"]];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CDHydepark" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (!results) {
        NSLog(@"Error fetching Employee objects: %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }
    if([results count] > 0)
    {
        return results;
    }
    else
        return nil;
    
}


-(NSArray *)getPosts:(NSString *)projIDString
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CDHydepark"];
    
    NSError *error = nil;
    //[request setResultType:NSDictionaryResultType];
    [request setPredicate:[NSPredicate predicateWithFormat:@"isComment == 0"]];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CDHydepark" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (!results) {
        NSLog(@"Error fetching Employee objects: %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }
    if([results count] > 0)
    {
        
        //CDHydepark *person = (CDHydepark *)[results objectAtIndex:0];
        //[self.managedObjectContext deleteObject:person];
        return results;
    }
    else
        return nil;
    
}

-(void) deleteSpecificRecord:(long )indexpath{
    NSError *error = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CDHydepark"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"isComment == 0"]];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CDHydepark" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if([results count] > 0)
    {
        
        CDHydepark *item = (CDHydepark *)[results objectAtIndex:indexpath];
        
        [self.managedObjectContext deleteObject:item];
        [_managedObjectContext save:&error];
        
    }
    
    
}


-(void) deleteSpecificComment:(long )indexpath{
    NSError *error = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CDHydepark"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"isComment == 1"]];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CDHydepark" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if([results count] > 0)
    {
        
        CDHydepark *item = (CDHydepark *)[results objectAtIndex:indexpath];
        
        [self.managedObjectContext deleteObject:item];
        [_managedObjectContext save:&error];
        
    }
    
    
}
-(void) deleteAllRecords{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"CDHydepark"];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    
    NSError *deleteError = nil;
    [_persistentStoreCoordinator executeRequest:delete withContext:_managedObjectContext error:&deleteError];
    
}

@end
