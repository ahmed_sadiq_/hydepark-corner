//
//  CDHydepark.m
//  HydePark
//
//  Created by Samreen Noor on 09/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CDHydepark.h"

@implementation CDHydepark
@dynamic privacySelected;
@dynamic commentAllowed;
@dynamic video_duration;
@dynamic textToshare;
@dynamic postID;
@dynamic parentCommentID;
@dynamic file;
@dynamic userSession;
@dynamic anony;
@dynamic isAudio;
@dynamic profileData;
@dynamic isAnonymous;
@dynamic test;
@dynamic isComment;
@dynamic timestamp;

@end
