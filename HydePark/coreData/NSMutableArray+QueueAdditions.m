//
//  NSMutableArray+QueueAdditions.m
//  HydePark
//
//  Created by Samreen Noor on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "NSMutableArray+QueueAdditions.h"

@implementation NSMutableArray (QueueAdditions)
- (CDHydepark *) dequeue {
    
    // if ([self count] == 0) return nil; // to avoid raising exception (Quinn)
    CDHydepark *headObject =(CDHydepark *)[self objectAtIndex:0];
    if (headObject != nil) {
       // [[headObject retain] autorelease]; // so it isn't dealloc'ed on remove
        [self removeObjectAtIndex:0];
     
    }
    return headObject;
}

// Add to the tail of the queue (no one likes it when people cut in line!)
- (void) enqueue:(CDHydepark *)anObject {
    
//    NSLog(@"%@",anObject);
    [self addObject:anObject];
    //this method automatically adds to the end of the array
}
-(CDHydepark *)peek{
    CDHydepark *item = nil;
    if([self count] != 0){
        item = [self objectAtIndex:0];
    }
    return item;
}

@end
