//
//  CoreDataManager.h
//  TestCoreData
//
//  Created by Samreen Noor on 08/08/2016.
//  Copyright © 2016 Apple Txlabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CDHydepark.h"
@interface CoreDataManager : NSObject
+ (CoreDataManager *)sharedManager;
@property (nonatomic, strong) NSMutableArray *offlineContentArray;
@property (nonatomic, strong) NSMutableArray *offlineCommentsArray;

- (void)saveDataInManagedContextUsingBlock:(void (^)(BOOL saved, NSError *error))savedBlock;

- (NSFetchedResultsController *)fetchEntitiesWithClassName:(NSString *)className
                                           sortDescriptors:(NSArray *)sortDescriptors
                                        sectionNameKeyPath:(NSString *)sectionNameKeypath
                                                 predicate:(NSPredicate *)predicate;

- (id)createEntityWithClassName:(NSString *)className
           attributesDictionary:(NSDictionary *)attributesDictionary;
- (void)deleteEntity:(NSManagedObject *)entity;
- (BOOL)uniqueAttributeForClassName:(NSString *)className
                      attributeName:(NSString *)attributeName
                     attributeValue:(id)attributeValue;


-(CDHydepark *)AddRecordToCoreDataFile:(NSData *)file isAudio:(NSString *)isaudio withparentCommentID:(NSString *)parentCommentID CommentAllowed:(NSString *)commentAllowed TextToshare:(NSString *)textToshare Anony:(NSString *)anony VideoDuration:(NSString *)video_duration PstID:(NSString *)postID PrivacySelected:(NSString *)privacySelected profileData:(NSData *)proflData AndUserSession:(NSString *)userSession AndIsComment:(NSString *)isComment AndTimeStamp:(NSString *)timestamp;

-(NSArray *)getRecords:(NSString *)projIDString;
-(NSArray *)getComments:(NSString *)projIDString;
-(void) deleteAllRecords;
-(void) deleteSpecificComment:(long )indexpath;
-(void) deleteSpecificRecord:(long )indexpath;

@end
