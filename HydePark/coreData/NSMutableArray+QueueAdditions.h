//
//  NSMutableArray+QueueAdditions.h
//  HydePark
//
//  Created by Samreen Noor on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDHydepark.h"
@interface NSMutableArray (QueueAdditions)

- (CDHydepark *) dequeue;
- (void) enqueue:(CDHydepark *)anObject;
-(CDHydepark *)peek;
@end
