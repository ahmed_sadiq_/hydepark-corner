//
//  CommentsHeaderCell.m
//  HydePark
//
//  Created by Osama on 05/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "CommentsHeaderCell.h"

@implementation CommentsHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.hviewToRound.layer.cornerRadius  = self.hThumbnail.frame.size.width /14.0f;
    self.hviewToRound.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
