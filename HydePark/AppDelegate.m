    //
//  AppDelegate.m
//  HydePark
//
//  Created by Mr on 21/04/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "Constants.h"
#import "HomeVC.h"
#import "NavigationHandler.h"
#import "CommentsVC.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Flurry.h"
#import "DataContainer.h"
#import "NotificationsModel.h"
#import "DEMOLeftMenuViewController.h"
#import "DEMORightMenuViewController.h"
#import "CoreDataManager.h"
#import "CDHydepark.h"
#import "BeamUploadVC.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "Branch.h"
#import "Utils.h"
#import "UserChannel.h"
#import "FriendsVC.h"
#import "MyCornerVC.h"
#import "AFNetworking.h"
#import "HomeViewController.h"
#import "NSBundle+Language.h"
#import "DBCommunicator.h"
#import "DataContainer.h";
#import "NotificationsVC.h"
#import "OfflineDataModel.h"
#import "ChatVC.h"

static NSString *_deviceToken = NULL;
static NSDictionary *branchParams = nil;
@interface AppDelegate ()
{
    Alert *alert;
    DataContainer *sharedManager;
    CoreDataManager *coreMngr;
    BeamUploadVC *beamUpload;
}
@end

@implementation AppDelegate
// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
@synthesize viewController,navigationController,strEmail,strFirstN,strLastN,strUserId,user_id,isLoggedIn,videotoPlay,videotitle,videotags,videoUploader,IS_celeb,currentScreen,commentObj,hasBeenUpdated,hasBlockedSomeOne,hasbeenEdited,emailGPLus,videObj,latestVideoAdded,navigationControllersCount,parentIdStackController,currentMyCornerIndex,videomodel,progressFloat,friendsArray,unseenCount,totalNotiCount,seen;

NSString *const FBSessionStateChangedNotification = @"FBSessionStateChangedNotification";


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    _shudStartUpload = false;
    
    NSInteger langCode = [[NSUserDefaults standardUserDefaults] integerForKey:@"languageCode"];
    NSString *isListView = [[NSUserDefaults standardUserDefaults] objectForKey:@"listView"];
    
    
    DataContainer *sharedInstance = [DataContainer sharedManager];
    if([isListView isEqualToString:@"1"]) {
        sharedInstance.isListView = YES;
    } else {
        sharedInstance.isListView = NO;
    }
    
    sharedInstance.languageCode = (int)langCode;
    
    if(sharedInstance.languageCode  == 0){
        [NSBundle setLanguage:@"en"];
    }
    else if (sharedInstance.languageCode  == 1){
        [NSBundle setLanguage:@"ar"];
    }
    else if (sharedInstance.languageCode  == 2){
        [NSBundle setLanguage:@"es"];
    }
    else if (sharedInstance.languageCode  == 3){
        [NSBundle setLanguage:@"fa"];
    }
    else if (sharedInstance.languageCode  == 4){
        [NSBundle setLanguage:@"pt"];
    }
    
    parentIdStackController = [[MJGStack alloc] init];
    [Fabric with:@[[Crashlytics class]]];
    
    ///////////////
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        if (!error && params) {
            branchParams = [params mutableCopy];
            [self fromBranchLink:params];
            /*
             ######### INSTRUCTIONS FOR DEEPLINKING #############
             
             // Parse "Params here, get all keys that are related to video model. If keys exist than create video model and do this [[NavigationHandler getInstance] MoveToLikeBeam:videomodel];"
             */
        }
    }];
    /////////////////////////
   // [self setUpRechability];
    
     ////////////// FOR OFFLINE MODULE /////////////////
    
    [self tempReachability];
    
    ///////////////////
    
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) )
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        //if( option != nil )
        //{
        //    NSLog( @"registerForPushWithOptions:" );
        //}
    }
    else
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] == nil)
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"nightMode"];
            [[NSNotificationCenter defaultCenter] postNotificationName:SET_NIGHT_MODE object:nil];
        }
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
         {
             if( !error )
             {
                 [[UIApplication sharedApplication] registerForRemoteNotifications];  // required to get the app to do anything at all about push notifications
                 NSLog( @"Push registration success." );
             }
             else
             {
                 NSLog( @"Push registration FAILED" );
                 NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                 NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
             }
         }];
    }
    NavigationHandler *navHandler = [[NavigationHandler alloc] initWithMainWindow:self.window];
    [navHandler loadFirstVC];
    [Flurry startSession:FLURRY_API_KEY];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"])
    {
        [self addOfflineBeams];
        [self getMyChannel];
    }
    
    //[FBLoginView class];
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    return YES;
}

-(void)addOfflineBeams
{
    sharedManager = [DataContainer sharedManager];
    [[DBCommunicator sharedManager] createDB];
    NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
    
    for (int i = 0; i < offlineArray.count; i++)
    {
        OfflineDataModel *offlineVideoModel = offlineArray[i];
        NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];

        
        if ([offlineVideoModel.isComment isEqualToString:@"1"] || [offlineVideoModel.privacyCheck rangeOfCharacterFromSet:notDigits].location == NSNotFound){
            [offlineArray removeObjectAtIndex:i];
            i--;
            sharedManager.offlineArrayCount = sharedManager.offlineArrayCount - 1;
            sharedManager.isFromBackground = NO;
        }
    }
    
    ///////////////////////////MERGING OFFLINE BEAMS IN ONLINE ARRAY
    
    for (int i = 0; i < offlineArray.count; i++)
    {
        OfflineDataModel *offlineVideoModel = offlineArray[i];
        VideoModel *_Videos     = [[VideoModel alloc] init];
        
        _Videos.video_thumbnail_link = offlineVideoModel.thumbnail;
        _Videos.video_link = offlineVideoModel.data;
        _Videos.is_anonymous = offlineVideoModel.anonyCheck;
        _Videos.uploaded_date = offlineVideoModel.currentTime;
        _Videos.comments_count = @"0";
        _Videos.isLocal = YES;
        _Videos.current_datetime = offlineVideoModel.currentTime;
        // _Videos.uploaded_date = offlineVideoModel.
        [sharedManager.channelVideos insertObject:_Videos atIndex:0];
    }
}

-(void)fromBranchLink:(NSDictionary *)params{
    NSString *type  = (NSString*)params[@"type"];
    BOOL isFirstSession = [params[@"+is_first_session"] boolValue];
    NSString *deepLinkUrl = (NSString *)params[@"~referring_link"];
    if (params[@"post_id"] && params[@"parent_comment_id"] && ([[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"] && [type isEqualToString:@"COMMENT_POST"])) {
        
        int postIDInt = [params[@"post_id"] intValue];
        int parentCmtIDInt = [params[@"parent_comment_id"] intValue];
        
        NSString *postId = [NSString stringWithFormat:@"%d",postIDInt];
        NSString *pCmntId = [NSString stringWithFormat:@"%d",parentCmtIDInt];
        
        [[NavigationHandler getInstance] MoveToBeamComment:postId andParentCommentID:pCmntId isComment:NO];
        [self deeplinkCallForAnalytics:isFirstSession deepLink:deepLinkUrl];
    }
    else if(params[@"post_id"] && params[@"parent_comment_id"] && ([[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"] && [type isEqualToString:@"COMMENT_COMMENT"])){
        int postIDInt = [params[@"post_id"] intValue];
        int parentCmtIDInt = [params[@"parent_comment_id"] intValue];
        
        NSString *postId = [NSString stringWithFormat:@"%d",postIDInt];
        NSString *pCmntId = [NSString stringWithFormat:@"%d",parentCmtIDInt];
        
        [[NavigationHandler getInstance] MoveToBeamComment:postId andParentCommentID:pCmntId isComment:YES];
        [self deeplinkCallForAnalytics:isFirstSession deepLink:deepLinkUrl];
    }
    else if(params[@"is_celeb"] && [[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"]){
        int uId = [params[@"user_id"] intValue];
        Followings *fData  = [[Followings alloc] init];
        BOOL isCl= [params[@"is_celeb"] boolValue];
        fData.is_celeb = [NSString stringWithFormat:@"%d",isCl];
        NSString *userid = [NSString stringWithFormat:@"%d",uId];
        [[NavigationHandler getInstance] MoveToUserChannel:userid friendModel:fData];
        [self deeplinkCallForAnalytics:isFirstSession deepLink:deepLinkUrl];
    }
    branchParams = nil;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
    BOOL handledByBranch = [[Branch getInstance] continueUserActivity:userActivity];
    return handledByBranch;
}

-(void)deeplinkCallForAnalytics:(BOOL )isFirstSession deepLink:(NSString *)deepLink{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *firstSession = [NSString stringWithFormat:@"%d",isFirstSession];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"updateUsersStats",@"method",
                              token,@"Session_token",deepLink,@"deep_link",firstSession,@"is_first_session",@"IOS",@"device_type",nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
}

-(void)addToNotificationsArray:(NSDictionary*)postData{
    NotificationsModel *_notification = [[NotificationsModel alloc] init];
    _notification.notificationsData   = [postData objectForKey:@"response"];
    _notification.notif_ID            = [postData objectForKey:@"id"];
    _notification.time                = [postData objectForKey:@"timestamp"];
    _notification.seen                = [postData objectForKey:@"seen"];
    _notification.notificationType    = [postData objectForKey:@"notification_type"];
    _notification.message             = [postData objectForKey:@"message"];
    _notification.postData            = [postData objectForKey:@"post"];
    _notification.friend_ID           = [NSString stringWithFormat:@"%@",[postData objectForKey:@"friend_id"]];
    _notification.post_ID             = [postData objectForKey:@"post_id"];
    _notification.parent_Id           = [postData objectForKey:@"parent_comment_id"];
    _notification.frndName           = [NSString stringWithFormat:@"%@",[Utils getDecryptedTextFor:[postData objectForKey:@"friend_name"]]];
    
    //_notification.chatMsg           = [postData objectForKey:@"post"];
    
    sharedManager = [DataContainer sharedManager];
    [sharedManager.notificationsArray insertObject:_notification atIndex:0];
}
-(VideoModel*)returnMeVideoModel:(NSDictionary*)postDate{
    VideoModel *videomodl          = [[VideoModel alloc]init];
    videomodl.userName             = [postDate valueForKey:@"full_name"];
    videomodl.is_anonymous         = [postDate valueForKey:@"is_anonymous"];
    videomodl.title                = [postDate valueForKey:@"caption"];
    videomodl.comments_count       = [postDate valueForKey:@"comment_count"];
    videomodl.topic_id             = [postDate valueForKey:@"topic_id"];
    videomodl.user_id              = [postDate valueForKey:@"user_id"];
    videomodl.profile_image        = [postDate valueForKey:@"profile_link"];
    videomodl.like_count           = [postDate valueForKey:@"like_count"];
    videomodl.seen_count           = [postDate valueForKey:@"seen_count"];
    videomodl.video_link           = [postDate valueForKey:@"video_link"];
    videomodl.m3u8_video_link      = [postDate valueForKey:@"m3u8_video_link"];
    videomodl.video_thumbnail_link = [postDate valueForKey:@"video_thumbnail_link"];
    videomodl.videoID              = [postDate valueForKey:@"id"];
    videomodl.Tags                 = [postDate valueForKey:@"tag_friends"];
    videomodl.video_length         = [postDate valueForKey:@"video_length"];
    videomodl.like_by_me           = [postDate valueForKey:@"liked_by_me"];
    videomodl.reply_count          = [postDate objectForKey:@"reply_count"];
    videomodl.current_datetime     = [postDate objectForKey:@"current_datetime"];
    videomodl.uploaded_date        = [postDate objectForKey:@"uploaded_date"];
    videomodl.isMute              = [postDate objectForKey:@"mute"];
    videomodl.isCelebrity = [postDate objectForKey:@"is_celeb"];
    
    videomodl.cpost_id         = [postDate objectForKey:@"post_id"];
    
    if([postDate objectForKey:@"beam_type"]){
        videomodl.beamType = [[postDate objectForKey:@"beam_type"] intValue];
    }
    if([postDate objectForKey:@"original_beam_data"]){
        videomodl.originalBeamData = [postDate objectForKey:@"original_beam_data"];
    }
    return videomodl;
}
-(Followings *) returnMeFollowing : (NSDictionary *) tempDict{
    
    Followings *responseData = [[Followings alloc] init];
    
    responseData.f_id = [tempDict objectForKey:@"friend_id"];
    responseData.fullName = [tempDict objectForKey:@"friend_name"];
    responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
    responseData.profile_link = [tempDict objectForKey:@"profile_link"];
    return responseData;
}
+(NSString *)getDeviceToken{
    
    return _deviceToken;
}

+(NSDictionary *)getBranchParams{
    return branchParams;
}
+(void)moveToBranchedBeam{
    if(branchParams)
    {
        NSString *type  = (NSString*)branchParams[@"type"];
        if (branchParams[@"post_id"] && branchParams[@"parent_comment_id"] && ([[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"] && [type isEqualToString:@"COMMENT_POST"])) {
            
            int postIDInt = [branchParams[@"post_id"] intValue];
            int parentCmtIDInt = [branchParams[@"parent_comment_id"] intValue];
            
            NSString *postId = [NSString stringWithFormat:@"%d",postIDInt];
            NSString *pCmntId = [NSString stringWithFormat:@"%d",parentCmtIDInt];
            
            [[NavigationHandler getInstance] MoveToBeamComment:postId andParentCommentID:pCmntId isComment:YES];
        }
        else if(branchParams[@"post_id"] && branchParams[@"parent_comment_id"] && ([[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"] && [type isEqualToString:@"COMMENT_COMMENT"])){
            int postIDInt = [branchParams[@"post_id"] intValue];
            int parentCmtIDInt = [branchParams[@"parent_comment_id"] intValue];
            
            NSString *postId = [NSString stringWithFormat:@"%d",postIDInt];
            NSString *pCmntId = [NSString stringWithFormat:@"%d",parentCmtIDInt];
            
            [[NavigationHandler getInstance] MoveToBeamComment:postId andParentCommentID:pCmntId isComment:NO];
        }
        //        NSLog(@"params: %@", branchParams.description);
    }
    branchParams = nil;
}
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        
        [application registerForRemoteNotifications];
    }
    
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *deviceToken1 = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceToken1 = [deviceToken1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    _deviceToken = deviceToken1;
    [Utils sendUserToken];
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    /////////////////////////////////////
    [[Branch getInstance] handlePushNotification:userInfo];
    ////////////////////////////////////////////////
    UIApplicationState state = [application applicationState];
    if(state == UIApplicationStateInactive){
        [self analyzePushNotification:userInfo stateOfApp:2];
    }
//    else if(state == UIApplicationStateActive){
//        [self analyzePushNotification:userInfo stateOfApp:1];
//    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    //    NSLog(@"%@", notification.request.content.userInfo);
    //NSString *message = [notification.request.content.userInfo objectForKey:@"message"];
    NSDictionary *userInfo = notification.request.content.userInfo;
    [self analyzePushNotification:userInfo stateOfApp:1];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler
{
    
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    [self analyzePushNotification:userInfo stateOfApp:2];
    
}

-(void)analyzePushNotification:(NSDictionary *)userInfo stateOfApp:(int)stateOfApp{
    //state = 1 = ACTIVE
    //state = 2 = Background/Inactive/CLosed
    self.seen = NO;
    NSString *message = [userInfo objectForKey:@"message"];
    NSString *type = [userInfo objectForKey:@"notification_type"];
    sharedManager = [DataContainer sharedManager];
    if(!sharedManager.notifcationsFetcedFirst)
        [self addToNotificationsArray:userInfo];
    
    if (stateOfApp == 2) {
        if ([type isEqualToString:@"LIKE_POST"]) {
            NSDictionary *postDate = [userInfo objectForKey:@"post"];
            videomodel = [[VideoModel alloc]init];
            videomodel = [self returnMeVideoModel:postDate];
            [[NavigationHandler getInstance] MoveToLikeBeam:videomodel];
            
        }else if ([type isEqualToString:@"LIKE_COMMENT"]) {
            NSDictionary *postDate = [userInfo objectForKey:@"post"];
            NSString *pID  = [userInfo objectForKey:@"parent_comment_id"];
            videomodel = [[VideoModel alloc]init];
            videomodel = [self returnMeVideoModel:postDate];
            [[NavigationHandler getInstance] MoveToCommentsOnCommentsNotifi:videomodel second:pID];
        }
        
        else if ([type isEqualToString:@"TAG_FRIENDS"]){
            NSDictionary *postDate = [userInfo objectForKey:@"post"];
            NSString *pID  = [userInfo objectForKey:@"parent_comment_id"];
            videomodel = [[VideoModel alloc]init];
            videomodel = [self returnMeVideoModel:postDate];
            [[NavigationHandler getInstance] MoveToCommentsNotifi:videomodel second:pID];
        }
        else if ([type isEqualToString:@"TAG_FRIENDS_COMMENT"]){
            NSDictionary *postDate = [userInfo objectForKey:@"post"];
            NSString *pID  = [userInfo objectForKey:@"parent_comment_id"];
            videomodel = [[VideoModel alloc]init];
            videomodel = [self returnMeVideoModel:postDate];
            [[NavigationHandler getInstance] MoveToCommentsOnCommentsNotifi:videomodel second:pID];
        }
        else if([type isEqualToString:@"COMMENT_COMMENT"]){
            NSDictionary *postDate = [userInfo objectForKey:@"post"];
            NSString *pID  = [userInfo objectForKey:@"parent_comment_id"];
            videomodel = [[VideoModel alloc]init];
            videomodel = [self returnMeVideoModel:postDate];
            NSLog(@"so the type is COMMENT_COMMENT");
            [[NavigationHandler getInstance] MoveToCommentsOnCommentsNotifi:videomodel second:pID];
        }
        else if ([type isEqualToString:@"COMMENT_POST"]){
            NSDictionary *postDate = [userInfo objectForKey:@"post"];
            NSString *pID  = [userInfo objectForKey:@"parent_comment_id"];
            videomodel = [[VideoModel alloc]init];
            videomodel = [self returnMeVideoModel:postDate];
            NSLog(@"so the type is COMMENT_POST");
            [[NavigationHandler getInstance] MoveToCommentsNotifi:videomodel second:pID];
        }
        else if([type isEqualToString:@"BEAM_UPLOAD_BY_CELEB"]){
            NSDictionary *postDate = [userInfo objectForKey:@"post"];
            NSString *pID  = [userInfo objectForKey:@"parent_comment_id"];
            videomodel = [[VideoModel alloc]init];
            videomodel = [self returnMeVideoModel:postDate];
            [[NavigationHandler getInstance] MoveToCommentsNotifi:videomodel second:pID];
        }
        else if ([type  isEqualToString:@"REQUEST_RECIEVED"]){
            NSString *friendID = [userInfo objectForKey:@"friend_id"];
            Followings *fData  = [[Followings alloc] init];
            BOOL isCl= [userInfo[@"is_celeb"] boolValue];
            fData.is_celeb = [NSString stringWithFormat:@"%d",isCl];
            [[NavigationHandler getInstance] MoveToUserChannel:friendID friendModel:fData];
        }else if([type isEqualToString:@"REQUEST_ACCEPTED"]){
            NSString *friendID = [userInfo objectForKey:@"friend_id"];
            Followings *fData  = [[Followings alloc] init];
            BOOL isCl= [userInfo[@"is_celeb"] boolValue];
            fData.is_celeb = [NSString stringWithFormat:@"%d",isCl];
            [[NavigationHandler getInstance] MoveToUserChannel:friendID friendModel:fData];
        }
        else if ([type isEqualToString:@"FOLLOWED"])
        {
            NSString *friendID = [userInfo objectForKey:@"friend_id"];
            Followings *fData  = [[Followings alloc] init];
            BOOL isCl= [userInfo[@"is_celeb"] boolValue];
            fData.is_celeb = [NSString stringWithFormat:@"%d",isCl];
            [[NavigationHandler getInstance] MoveToUserChannel:friendID friendModel:fData];
        }
        else if ([type isEqualToString:@"VIDEO_MESSAGE"])
        {
            Followings *frnd = [[Followings alloc]init];
            frnd = [self returnMeFollowing:userInfo];
            [self performSelector:@selector(refreshCornerCount) withObject:nil afterDelay:0.2];
            [[NavigationHandler getInstance] MoveToFriendChat:frnd];
        }
        else if([type isEqualToString:@"BEAM_SHARE"]){
            NSDictionary *postDate = [userInfo objectForKey:@"post"];
            NSString *pID  = @"-1";
            videomodel = [[VideoModel alloc]init];
            videomodel = [self returnMeVideoModel:postDate];
            [[NavigationHandler getInstance] MoveToCommentsNotifi:videomodel second:pID];
        }
    }
    else{
        sharedManager = [DataContainer sharedManager];
        
        if ([type isEqualToString:@"VIDEO_MESSAGE"]){
            int currentCount = [sharedManager._profile.inboxCount intValue];
            currentCount += 1;
//            int unreadCount = [sharedManager._profile.unreadInboxCount intValue];
//            unreadCount += 1;
//            sharedManager._profile.unreadInboxCount = [NSString stringWithFormat:@"%d",unreadCount];
            sharedManager._profile.inboxCount = [NSString stringWithFormat:@"%d",currentCount];
            UIViewController *topVC = [self getTopMostController];
            if([topVC isKindOfClass:[HomeViewController class]]){
                HomeViewController *home = (HomeViewController *)topVC;
                UIViewController *topPageController = [home.pageViewController.viewControllers lastObject];
                if([topPageController isKindOfClass:[MyCornerVC class]]){
                    MyCornerVC *cornerVC = (MyCornerVC *)topPageController;
                    [cornerVC getMyChannel];
//                    [cornerVC.collectionHome reloadData];
                }
                
            }
            
            if([topVC isKindOfClass:[ChatVC  class]])
            {
                [self performSelector:@selector(refreshCornerCount) withObject:nil afterDelay:0.2];

                ChatVC *cVC = (ChatVC *)topVC;
                [cVC.friends removeAllObjects];
                [cVC getFriends];
            }
            NSDictionary *postDate = [userInfo objectForKey:@"post"];
            NSString *friendID = [userInfo objectForKey:@"friend_id"];
            if (sharedManager.isChaVC) {
                if ([friendID isEqualToString:sharedManager.chatFriendId]) {
                    videomodel = [[VideoModel alloc]init];
                    videomodel = [self returnMeVideoModel:postDate];
                    NSDictionary *videoObj = @{@"videoObj": videomodel};
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"addNewMsg"
                     object:videoObj];
                }
            }
        }
        else if ([type isEqualToString:@"COMMENT_POST"]){
            // If type of message is "COMMENT_POST" check if user is on that post, if yes add that comment on the screen directly
            NSDictionary *postDate = [userInfo objectForKey:@"post"];
            NSString *pID  = [userInfo objectForKey:@"parent_comment_id"];
            videomodel = [[VideoModel alloc]init];
            videomodel = [self returnMeVideoModel:postDate];
            UIViewController *topVC = [self getTopMostController];
            if ([topVC isKindOfClass:[CommentsVC class]]) {
                // mean we should check parent post id and if it matches than should display it
                CommentsVC *commentVC = (CommentsVC*)topVC;
                if([videomodel.videoID intValue] == [[commentVC getParentPostID] intValue]) {
                    [commentVC setPageNum:1];
                    [commentVC getCommentsOnPost];
                }
            }
        }
        else if ([type isEqualToString:@"REQUEST_RECIEVED"] || [type isEqualToString:@"REQUEST_ACCEPTED"] || [type isEqualToString:@"FOLLOWED"]){
            UIViewController *topVC = [self getTopMostController];
            Followings *fObj = [[Followings alloc] init];
            int isCeleb = [[userInfo objectForKey:@"is_celeb"] intValue];
            fObj.is_celeb = [NSString stringWithFormat:@"%d",isCeleb];
            fObj.f_id = [userInfo objectForKey:@"friend_id"];
            fObj.fullName = [userInfo objectForKey:@"friend_name"];
            fObj.status = [userInfo objectForKey:@"state"];
            fObj.profile_link = [userInfo objectForKey:@"profile_link"];
            NSString *isFollowed = [userInfo objectForKey:@"is_followed"];
            fObj.isFollowed = [isFollowed intValue];
            if(![self isFriendAlreadyPresent:fObj.f_id]){
                [sharedManager.followers insertObject:fObj atIndex:0];
                
            }
            [self changeStatus:fObj];
            if ([topVC isKindOfClass:[UserChannel class]]) {
                // mean we should check parent post id and if it matches than should display it
                NSString *friendID = [userInfo objectForKey:@"friend_id"];
                UserChannel *userVC = (UserChannel *)topVC;
                if([friendID intValue] == [[userVC getFid] intValue]) {
                    if([type isEqualToString:@"REQUEST_RECIEVED"]){
                        userVC.currentUser.status = @"ACCEPT_REQUEST";
                        [userVC.Followlbl setText:@"Respond"];
                        userVC.ChannelObj.state = @"ACCEPT_REQUEST";
                    }
                    else{
                        userVC.currentUser.status = @"FRIEND";
                        [userVC.Followlbl setText:@"Friends"];
                        userVC.ChannelObj.state = @"FRIEND";
                        userVC.msgButton.enabled  = YES;
                    }
                }
            }
            if ([topVC isKindOfClass:[FriendsVC class]]) {
                
//                FriendsVC *userVC = (FriendsVC *)topVC;
//                BOOL isFriendPresent = false;
//                for (int i =0; i< userVC.friendsArray.count; i++){
//                    Followings *tempObj = [userVC.friendsArray objectAtIndex:i];
//                    if([tempObj.f_id isEqualToString:fObj.f_id]){
//                        isFriendPresent = true;
//                        break;
//                    }
//                }
//                [self changeStatus:fObj];
//                if(!isFriendPresent){
//                    [userVC.friendsArray insertObject:fObj atIndex:0];
//                    [userVC.follwersAndFollwings reloadData];
//                }
//                [userVC.follwersAndFollwings reloadData];
                
                [self performSelector:@selector(refreshCornerCount) withObject:nil afterDelay:0.2];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshRequests" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshFriendList" object:nil];
            }
            if([topVC isKindOfClass:[HomeViewController class]]){
                HomeViewController *home = (HomeViewController *)topVC;
                UIViewController *topPageController = [home.pageViewController.viewControllers lastObject];
                if([topPageController isKindOfClass:[MyCornerVC class]]){
                    MyCornerVC *cornerVC = (MyCornerVC *)topPageController;
                    //[cornerVC.tblHome reloadData];
//                    [cornerVC.collectionHome reloadData];
                    [cornerVC getMyChannel];
                }
                
            }
            else
            {
                sharedManager.isReloadMyCorner = YES;
            }
            
        }
        
        UIViewController *topVC = [self getTopMostController];
        if(![topVC isKindOfClass:[NotificationsVC class]])
        {
            //post Comment Notification for counterUpdate -by FA
             [[NSNotificationCenter defaultCenter] postNotificationName:@"Comment" object:nil userInfo:userInfo];
            
            //end
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"plusNotification"
             object:nil];
        }
        else
        {
            NotificationsVC *notVC = (NotificationsVC *)topVC;
            [notVC refreshCall];
        }
    
        [self showAlert:message];
    }
}
-(void)refreshCornerCount
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateRequestCount" object:nil];
}
-(BOOL)isFriendAlreadyPresent:(NSString *)friendId{
    for (int i =0; i< sharedManager.followers.count; i++){
        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
        if([tempObj.f_id isEqualToString:friendId]){
            return YES;
        }
    }
    return NO;
}
#pragma mark
-(BOOL)changeStatus:(Followings*)fUser{
    for (int i =0; i< sharedManager.followers.count; i++){
        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
        if([tempObj.f_id isEqualToString:fUser.f_id]){
            tempObj.status = fUser.status;
            return YES;
        }
    }
    return NO;
}
-(void) showAlert:(NSString *)message{
    alert = [[Alert alloc] initWithTitle:message duration:(float)2.0f completion:^{
        //
    }];
    [alert setDelegate:self];
    [alert setShowStatusBar:YES];
    [alert setAlertType:AlertTypeSuccess];
    [alert setIncomingTransition:AlertIncomingTransitionTypeSlideFromTop];
    [alert setOutgoingTransition:AlertOutgoingTransitionTypeSlideToTop];
    [alert setBounces:YES];
    [alert showAlert];
}

#pragma mark - Twitter SDK
- (void)getTwitterAccountOnCompletion:(void (^)(ACAccount *))completionHandler {
    ACAccountStore *store = [[ACAccountStore alloc] init];
    ACAccountType *twitterType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [store requestAccessToAccountsWithType:twitterType withCompletionHandler:^(BOOL granted, NSError *error) {
        if(granted) {
            // Remember that twitterType was instantiated above
            NSArray *twitterAccounts = [store accountsWithAccountType:twitterType];
            
            // If there are no accounts, we need to pop up an alert
            if(twitterAccounts == nil || [twitterAccounts count] == 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts"
                                                                message:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            } else {
                //Get the first account in the array
                ACAccount *twitterAccount = [twitterAccounts objectAtIndex:0];
                //Save the used SocialAccountType so it can be retrieved the next time the app is started.
                
                //Call the completion handler so the calling object can retrieve the twitter account.
                completionHandler(twitterAccount);
            }
        }
    }];
    
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if (![[Branch getInstance] handleDeepLink:url]) {
        // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
    }
    
    if ([_strSocial isEqualToString:@"Facebook"]){
        
        //        return [FBAppCall handleOpenURL:url
        //                      sourceApplication:sourceApplication];
        BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                      openURL:url
                                                            sourceApplication:sourceApplication
                                                                   annotation:annotation
                        ];
        // Add any custom logic here.
        return handled;
        
    } if ([_strSocial isEqualToString:@"GPlus"]) {
        //        return [GPPURLHandler handleURL:url
        //                      sourceApplication:sourceApplication
        //                             annotation:annotation];
        return [[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication
                                          annotation:annotation];
    }
    
    
    return NO;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the
    
       NSString *appClosed = [[NSUserDefaults standardUserDefaults] objectForKey:@"appClosed"]; /// to check if app was killed or not ( 1 = killed, 0 = not)
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet=YES;  }
    
        ////////////// FOR OFFLINE MODULE /////////////////
    
    if(self.hasInet) {
        [[DBCommunicator sharedManager] createDB];
        NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
        
        if(APP_DELEGATE.isUploading == YES)
        {
            if(offlineArray.count > 0)
            {
                [offlineArray removeObjectAtIndex:0];
            }
        }
        
        if(offlineArray.count > 0){
           // APP_DELEGATE.isUploading = YES;
            BeamUploadVC *vc = [[BeamUploadVC alloc] init];
            DataContainer *shareobj = [DataContainer sharedManager];
            shareobj.offlineArrayCount = offlineArray.count;
            vc.offlineUploadCount = offlineArray.count;
            
                [vc uploadOfflineBeam];

            NSLog(@"AppDelegate: offlineArray: %@",offlineArray);
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"internetDisconnected" object:nil]; // to be tested
        
    }
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //    if (FBSession.activeSession.state == FBSessionStateCreatedOpening) {
    //        [FBSession.activeSession close];
    //    }
    if(_shudStartUpload)
    {
         NSString *appClosed = [[NSUserDefaults standardUserDefaults] objectForKey:@"appClosed"]; /// to check if app was killed or not ( 1 = killed, 0 = not)
        if([appClosed isEqualToString:@"1"]) {
            [[DBCommunicator sharedManager] createDB];
            NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
           
            if(offlineArray.count > 0){
                BeamUploadVC *vc = [[BeamUploadVC alloc] init];
                if([appClosed isEqualToString:@"1"]) {
                    if(self.hasInet)
                    {
                        [vc performSelector:@selector(uploadOfflineBeamWhenInForeground) withObject:nil afterDelay:3.0];
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"appClosed"];
                    sharedManager.isFromBackground = YES;
                    sharedManager.isReloadFromBackground = YES;
                }
            }
            
            
        }  else if(!_hasInet){
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"fromBackground"];
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"fromBackgroundArraySize"];
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    /////////// FOR OFFLINE MODULE ////////\
    
    // when app is closed save local objects and show local objects when app become active
    [[DBCommunicator sharedManager] createDB];
    NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
    if(offlineArray.count > 0){
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"appClosed"];
    }
}


#pragma mark helper Methods
 ////////////// FOR OFFLINE MODULE /////////////////

-(void)tempReachability {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tempHandleNetworkChange:) name:kReachabilityChangedNotification object:nil];
//    NSString *appClosed = [[NSUserDefaults standardUserDefaults] objectForKey:@"appClosed"]; /// to check if app was killed or not ( 1 = killed, 0 = not)
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet=YES;  }
    
    if(self.hasInet) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"fromBackground"];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"fromBackground"];
        [[DBCommunicator sharedManager] createDB];
//        NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
//        if(offlineArray.count > 0){
//            BeamUploadVC *vc = [[BeamUploadVC alloc] init];
//            DataContainer *shareobj = [DataContainer sharedManager];
//            shareobj.offlineArrayCount = offlineArray.count;
//            vc.offlineUploadCount = offlineArray.count;
//            if([appClosed isEqualToString:@"1"]) {
//
//            } else {
//                [vc uploadOfflineBeam];
//            }
//            NSLog(@"AppDelegate: offlineArray: %@",offlineArray);
//        }
    }
    
}

-(void)startUploadProcess
{
    NSString *appClosed = [[NSUserDefaults standardUserDefaults] objectForKey:@"appClosed"];
    NSMutableArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
    if(offlineArray.count > 0){
        BeamUploadVC *vc = [[BeamUploadVC alloc] init];
        DataContainer *shareobj = [DataContainer sharedManager];
        shareobj.offlineArrayCount = offlineArray.count;
        vc.offlineUploadCount = offlineArray.count;
//        if([appClosed isEqualToString:@"1"]) {
//
//        } else {
            [vc uploadOfflineBeam];
//        }
        NSLog(@"AppDelegate: offlineArray: %@",offlineArray);
    }
}

//-(void)tempHandleNetworkChange:(NSNotification *)notification {
//    NSString *appClosed = [[NSUserDefaults standardUserDefaults] objectForKey:@"appClosed"]; /// to check if app was killed or not ( 1 = killed, 0 = not)
//    _reachability = [Reachability reachabilityForInternetConnection];
//    [_reachability startNotifier];
//
//    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
//
//    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet=NO;   }
//    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet=YES;  }
//    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet=YES;  }
//
//    if(self.hasInet) {
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"fromBackground"];
//        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"fromBackground"];
////        [[DBCommunicator sharedManager] createDB];
////        NSArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
////        if(offlineArray.count > 0){
////            BeamUploadVC *vc = [[BeamUploadVC alloc] init];
////            DataContainer *shareobj = [DataContainer sharedManager];
////            shareobj.offlineArrayCount = offlineArray.count;
////            vc.offlineUploadCount = offlineArray.count;
////            if([appClosed isEqualToString:@"1"]) {
////
////            } else {
////                [vc uploadOfflineBeam];
////            }
////            NSLog(@"AppDelegate: offlineArray: %@",offlineArray);
////        }
//        if([[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"])
//        {
//            sharedManager.channelVideos = [[NSMutableArray alloc] init];
//            sharedManager.myCornerPageNum = 1;
//            [self addOfflineBeams];
//            [self getMyChannel];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadSpeakers" object:nil];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadInbox" object:nil];
//        }
//    } else {
//        sharedManager.isReloadCornerCollection = YES;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"internetDisconnected" object:nil];
//    }
//}

-(void)tempHandleNetworkChange:(NSNotification *)notification {
    NSString *appClosed = [[NSUserDefaults standardUserDefaults] objectForKey:@"appClosed"]; /// to check if app was killed or not ( 1 = killed, 0 = not)
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet=YES;  }
    
    if(self.hasInet) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"fromBackground"];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"fromBackground"];
        //        [[DBCommunicator sharedManager] createDB];
        //        NSArray *offlineArray = [[DBCommunicator sharedManager] retrieveAllRows];
        //        if(offlineArray.count > 0){
        //            BeamUploadVC *vc = [[BeamUploadVC alloc] init];
        //            DataContainer *shareobj = [DataContainer sharedManager];
        //            shareobj.offlineArrayCount = offlineArray.count;
        //            vc.offlineUploadCount = offlineArray.count;
        //            if([appClosed isEqualToString:@"1"]) {
        //
        //            } else {
        //                [vc uploadOfflineBeam];
        //            }
        //            NSLog(@"AppDelegate: offlineArray: %@",offlineArray);
        //        }
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"])
        {
            if(sharedManager.channelVideos != nil && sharedManager.channelVideos.count > 0)
            {
                [self performSelector:@selector(startUploadProcess) withObject:nil afterDelay:0.1];
                _shudStartUpload = true;
            }
            else
            {
                sharedManager.channelVideos = [[NSMutableArray alloc] init];
                sharedManager.myCornerPageNum = 1;
                [self addOfflineBeams];
                [self getMyChannel];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadSpeakers" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadInbox" object:nil];
        }
    }
    else
    {
        sharedManager.isReloadCornerCollection = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"internetDisconnected" object:nil];
    }
}

//////////////////////////////////////////

-(void)setUpRechability
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];
    // coreMngr = [CoreDataManager sharedManager];
    // [coreMngr deleteAllRecords];
    beamUpload = [[BeamUploadVC alloc]init];

    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet=YES;  }
    if (self.hasInet) {
        
       
        
        coreMngr = [CoreDataManager sharedManager];
        
        NSArray *event =   [coreMngr getRecords:@""];
        if ([event count]>0) {
            NSDictionary *data = @{@"data": event};
            NSMutableArray *temp= [[NSMutableArray alloc]init];
            temp = data[@"data"];
            coreMngr.offlineContentArray = temp.mutableCopy;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showOflineContents"
                                                                object:data];
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"upLoadOflineContents"
             object:data];
        }
        
        //uploading here
        
        NSArray *eventComment =   [coreMngr getComments:@"-1"];
        if ([eventComment count]>0) {
            //api call
            
            NSLog(@"Uploading videos = %lu", (unsigned long)eventComment.count);
            
            NSDictionary *data = @{@"data": eventComment};
            
            NSMutableArray *temp= [[NSMutableArray alloc]init];
            temp = data[@"data"];
            coreMngr.offlineCommentsArray = temp.mutableCopy;
            
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"upLoadOflineComments"
             object:data];
            
        }
        
    }else{//show offline Content
        
        
        [[NSNotificationCenter defaultCenter] removeObserver:beamUpload name:@"showOflineContents" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:beamUpload
                                                 selector:@selector(upLoadOflineContents:)
                                                     name:@"showOflineContents"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:beamUpload name:@"showOflineComments" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:beamUpload
                                                 selector:@selector(upLoadOflineContents:)
                                                     name:@"showOflineComments"
                                                   object:nil];
        
        coreMngr = [CoreDataManager sharedManager];
        
        NSArray *event =   [coreMngr getRecords:@"-1"];
        if ([event count]>0) {
            //api call
            NSDictionary *data = @{@"data": event};
            
            NSMutableArray *temp= [[NSMutableArray alloc]init];
            temp = data[@"data"];
            coreMngr.offlineContentArray = temp.mutableCopy;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showOflineContents"
             object:data];
        }
        
        
        NSArray *eventComment =   [coreMngr getComments:@"-1"];
        if ([eventComment count]>0) {
            //api call
            
            NSLog(@"%@", eventComment);
            
            NSDictionary *data = @{@"data": eventComment};
            
            NSMutableArray *temp= [[NSMutableArray alloc]init];
            temp = data[@"data"];
            coreMngr.offlineCommentsArray = temp.mutableCopy;
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"showOflineComments"
             object:data];
            
        }
        
        
        
    }
    
}



- (void) handleNetworkChange:(NSNotification *)notice
{
    beamUpload = [[BeamUploadVC alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:beamUpload
                                             selector:@selector(upLoadOflineContents:)
                                                 name:@"upLoadOflineContents"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:beamUpload name:@"upLoadOflineComments" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:beamUpload
                                             selector:@selector(upLoadOflineContents:)
                                                 name:@"upLoadOflineComments"
                                               object:nil];
    
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if          (remoteHostStatus == NotReachable)      {
        NSLog(@"no");      self.hasInet=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet=YES;  }
    
    if (self.hasInet) {
        coreMngr = [CoreDataManager sharedManager];
        
        
        NSArray *event =   [coreMngr getRecords:@"-1"];
        if ([event count]>0) {
            //api call
            NSDictionary *data = @{@"data": event};
            
            NSMutableArray *temp= [[NSMutableArray alloc]init];
            temp = data[@"data"];
            coreMngr.offlineContentArray = temp.mutableCopy;
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"upLoadOflineContents"
             object:data];
        }
        
        NSArray *eventComment =   [coreMngr getComments:@"-1"];
        if ([eventComment count]>0) {
            //api call
            
            NSLog(@"%@", eventComment);
            
            NSDictionary *data = @{@"data": eventComment};
            
            NSMutableArray *temp= [[NSMutableArray alloc]init];
            temp = data[@"data"];
            coreMngr.offlineCommentsArray = temp.mutableCopy;
            
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"upLoadOflineComments"
             object:data];
            
        }
        
        
    }
}


-(UIWindow *) returnWindowWithWindowLevelNormal
{
    NSArray *windows = [UIApplication sharedApplication].windows;
    for(UIWindow *topWindow in windows)
    {
        if (topWindow.windowLevel == UIWindowLevelNormal)
            return topWindow;
    }
    return [UIApplication sharedApplication].keyWindow;
}

-(UIViewController *) getTopMostController
{
    UINavigationController *nav = [[NavigationHandler getInstance] getNavigationController];
    return [nav.viewControllers lastObject];
}


#pragma mark Server Call To make app faster
- (void) getMyChannel{
    
    CoreDataManager   *coreMngrs = [CoreDataManager sharedManager];
    sharedManager = [DataContainer sharedManager];
    //justChangedDp = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.fetchingContent = TRUE;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *pageStr = [NSString stringWithFormat:@"%d",sharedManager.myCornerPageNum];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_MY_CHENNAL,@"method",
                              token,@"session_token",pageStr,@"page_no",nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.fetchingContent = FALSE;
        int success = [[responseObject objectForKey:@"success"] intValue];
        if(success){
            int success = [[responseObject objectForKey:@"success"] intValue];
            NSDictionary *posts = [responseObject objectForKey:@"profile"];
            if(success == 1) {
                NSArray *tempArray = [responseObject objectForKey:@"posts"];
                sharedManager._profile = [[myChannelModel alloc] init];
                if(sharedManager.myCornerPageNum == 1){
                    NSArray *freidns = [responseObject objectForKey:@"friends"];
                    sharedManager.followers = [[NSMutableArray alloc] init];
                    if([freidns isKindOfClass:[NSArray class]]){
                        for(NSDictionary *tempDict in freidns){
                            Followings *_responseData = [[Followings alloc] init];
                            _responseData.f_id = [tempDict objectForKey:@"id"];
                            _responseData.fullName = [tempDict objectForKey:@"full_name"];
                            _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                            _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                            _responseData.status = [tempDict objectForKey:@"state"];
                            _responseData.isFollowed = [[tempDict objectForKey:@"is_followed"] intValue];
                            [sharedManager.followers addObject:_responseData];
                        }
                    }
                }
                NSString *bio = [posts objectForKey:@"bio"];
                if((NSString *)[NSNull null] != bio) {
                    sharedManager._profile.bio = [posts objectForKey:@"bio"];
                } else {
                    sharedManager._profile.bio = @"";
                }
                sharedManager._profile.profileDeepLink = [posts objectForKey:@"deep_link"];
                sharedManager._profile.beams_count = [posts objectForKey:@"beams_count"];
                sharedManager._profile.friends_count = [posts objectForKey:@"following_count"];
                sharedManager._profile.full_name = [posts objectForKey:@"full_name"];
                sharedManager._profile.user_id = [posts objectForKey:@"id"];
                sharedManager._profile.profile_image = [posts objectForKey:@"profile_link"];
                sharedManager._profile.likes_count = [posts objectForKey:@"followers_count"];
                sharedManager._profile.gender = [posts objectForKey:@"gender"];
                sharedManager._profile.email = [posts objectForKey:@"email"];
                sharedManager._profile.is_celeb = [posts objectForKey:@"is_celeb"];
                sharedManager._profile.cover_image = [posts objectForKey:@"cover_link"];
                sharedManager._profile.profile_view_count = [posts objectForKey:@"total_beams_views"];
                sharedManager._profile.inboxCount = [posts objectForKey:@"inbox_count"];
                sharedManager._profile.pendingReqsCount = [posts objectForKey:@"new_requests_count"];
                sharedManager._profile.unreadInboxCount = [posts objectForKey:@"unread_inbox_count"];
                sharedManager._profile.accountType = [posts objectForKey:@"account_type"];
                sharedManager._profile.inviteUrl = [posts objectForKey:@"invite_url"];
                sharedManager._profile.inviteText = [posts objectForKey:@"invite_text"];
                BOOL isC = [sharedManager._profile.is_celeb boolValue];
                [[NSUserDefaults standardUserDefaults] setBool:isC forKey:@"isCeleb"];
                [[NSUserDefaults standardUserDefaults] setObject:sharedManager._profile.profile_image forKey:@"user_img"];
                [[NSUserDefaults standardUserDefaults] setObject:sharedManager._profile.full_name forKey:@"User_Name"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                if(sharedManager._profile.userName != nil)
                {
                    [[NSUserDefaults standardUserDefaults] setObject:sharedManager._profile.userName forKey:@"User_Name"];
                }
                [[NSUserDefaults standardUserDefaults] setObject:sharedManager._profile.profile_image forKey:@"User_Img"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSArray *chPostArray = [NSArray new];
                if(tempArray.count == 0 && sharedManager.myCornerPageNum == 1){
                    if(sharedManager.channelVideos == nil)
                    {
                        sharedManager.channelVideos = [[NSMutableArray alloc] init];
                    }
                }
                if(tempArray.count > 0 || coreMngrs.offlineContentArray.count>0)
                {
                    chPostArray = [responseObject objectForKey:@"posts"];
                    if(sharedManager.myCornerPageNum == 1){
                        if(!sharedManager.channelVideos || sharedManager.channelVideos.count == 0)
                        {
                            sharedManager.channelVideos = [[NSMutableArray alloc] init];
                        }
                        if (coreMngrs.offlineContentArray.count>0) {
                            [[NSNotificationCenter defaultCenter]
                             postNotificationName:@"uploadData"
                             object:nil];
                        }
                    }
                    for(NSDictionary *tempDict in chPostArray){
                        VideoModel *_Videos = [[VideoModel alloc] initWithDictionary:tempDict];
//                        _Videos.title = [tempDict objectForKey:@"caption"];
//                        _Videos.comments_count = [tempDict objectForKey:@"comment_count"];
//                        _Videos.userName = [tempDict objectForKey:@"full_name"];
//                        _Videos.topic_id = [tempDict objectForKey:@"topic_id"];
//                        _Videos.user_id = [tempDict objectForKey:@"user_id"];
//                        _Videos.profile_image = [tempDict objectForKey:@"profile_link"];
//                        _Videos.like_count = [tempDict objectForKey:@"like_count"];
//                        _Videos.like_by_me = [tempDict objectForKey:@"liked_by_me"];
//                        _Videos.seen_count = [tempDict objectForKey:@"seen_count"];
//                        _Videos.video_angle = [[tempDict objectForKey:@"video_angle"] intValue];
//                        _Videos.beam_share_url = [tempDict objectForKey:@"beam_share_url"];
//                        _Videos.deep_link = [tempDict objectForKey:@"deep_link"];
//                        _Videos.video_link = [tempDict objectForKey:@"video_link"];
//                        _Videos.m3u8_video_link = [tempDict objectForKey:@"m3u8_video_link"];
//                        _Videos.video_thumbnail_link = [tempDict objectForKey:@"video_thumbnail_link"];
//                        _Videos.videoID = [tempDict objectForKey:@"id"];
//                        _Videos.Tags = [tempDict objectForKey:@"tag_friends"];
//                        _Videos.video_length = [tempDict objectForKey:@"video_length"];
//                        _Videos.is_anonymous = [tempDict objectForKey:@"is_anonymous"];
//                        _Videos.reply_count = [tempDict objectForKey:@"reply_count"];
//                        _Videos.current_datetime= [tempDict objectForKey:@"current_datetime"];
//                        _Videos.uploaded_date   = [tempDict objectForKey:@"uploaded_date"];
//                        _Videos.beam_share_url = [tempDict objectForKey:@"beam_share_url"];
//                        _Videos.isThumbnailReq =[[tempDict objectForKey:@"is_thumbnail_required"] intValue];
//                        _Videos.isMute = @"0";
//                        _Videos.privacy          = [tempDict objectForKey:@"privacy"];
//                        _Videos.likesArray      = [[NSMutableArray alloc] init];
//                        _Videos.isCelebrity = [tempDict objectForKey:@"is_celeb"];
//                        _Videos.isLocal = NO;
//                        _Videos.beamType = [[tempDict objectForKey:@"beam_type"] intValue];
//                        if([tempDict objectForKey:@"original_beam_data"]){
//                            _Videos.originalBeamData = [tempDict objectForKey:@"original_beam_data"];
//                        }
                        [sharedManager.channelVideos addObject:_Videos];
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"justReload" object:nil];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"appClosed"];
//                    [self performSelector:@selector(startUploadProcess) withObject:nil afterDelay:0.1];
//                    _shudStartUpload = true;
                }
                [self performSelector:@selector(startUploadProcess) withObject:nil afterDelay:0.1];
                _shudStartUpload = true;
                
                self.fetchingContent = FALSE;
                self.myCornerDataSuccess = true;
                
            }
            else{
                self.fetchingContent = FALSE;
            }
        }
        NSLog(@"success!");
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        self.myCornerCallFailed = YES;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"NO internet");
    }];
}

@end
