//
//  TabbarView.m
//  Wits
//
//  Created by TxLabz on 26/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "TabbarView.h"
#import "Constants.h"

@implementation TabbarView
@synthesize delegate;
- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        NSString *className = NSStringFromClass([self class]);
        [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
        [self addSubview:self.view];
        [self stretchToSuperView:self.view];
        self.audioLbl.text = NSLocalizedString(@"audio", nil);
        self.anonymousLbl.text = NSLocalizedString(@"anonymous_small", nil);
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(adjustForNightMode)
                                                     name:SET_NIGHT_MODE
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(clearNightMode)
                                                     name:CLEAR_NIGHT_MODE
                                                   object:nil];
        
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
            {
                _bgView.alpha = 0.8;
            }
            else
            {
                _bgView.alpha = 1.0;
            }
        }
        else
        {
            _bgView.alpha = 1.0;
        }
        
        return self;

    }
    return self;
}

-(void)adjustForNightMode
{
    _bgView.alpha = 0.8;
}

-(void)clearNightMode
{
    _bgView.alpha = 1.0;
}

- (void) stretchToSuperView:(UIView*) view {
    view.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *bindings = NSDictionaryOfVariableBindings(view);
    NSString *formatTemplate = @"%@:|[view]|";
    for (NSString * axis in @[@"H",@"V"]) {
        NSString * format = [NSString stringWithFormat:formatTemplate,axis];
        NSArray * constraints = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:bindings];
        [view.superview addConstraints:constraints];
    }
    
}
-(id)initWithFrame:(CGRect)frame{
    self  = [super initWithFrame:frame];
    if(self)
    {
        //initialization Code
    }
    return self;
}

- (IBAction)homePressed:(id)sender {
    [self.delegate tabbarBtnClicked:sender];
}

- (IBAction)friednsPressed:(id)sender {
    [self.delegate tabbarBtnClicked:sender];
}

- (IBAction)storePressed:(id)sender {
    [self.delegate tabbarBtnClicked:sender];
}

- (IBAction)settingsPressed:(id)sender {
    [self.delegate tabbarBtnClicked:sender];
}

- (IBAction)morePressed:(id)sender {
   [self.delegate tabbarBtnClicked:sender];
}
@end
