//
//  DataContainer.h
//  HydePark
//
//  Created by TxLabz on 30/06/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "myChannelModel.h"
#import <UIKit/UIKit.h>
@interface DataContainer : NSObject
{
    NSMutableArray *notificationsArray;
    NSMutableArray *myAchiveArray;
    NSMutableArray *whoToFollow;
    NSMutableArray *followings;
    NSMutableArray *followers;
}
@property int searchPageNum;
@property int forumPageNumber;
@property int pageNum;
@property int myCornerPageNum;

@property int newRequestsPageNum;
@property int sentRequestsPageNum;
@property int friendsPageNum;

@property (strong, nonatomic) myChannelModel *_profile;
@property (strong, nonatomic) NSMutableArray *notificationsArray;
@property (strong, nonatomic) NSMutableArray *myArchiveArray;
@property (strong, nonatomic) NSMutableArray *whoToFollow;
@property (strong, nonatomic) NSMutableArray *followings;
@property (strong, nonatomic) NSMutableArray *followers;
@property (strong, nonatomic) NSMutableArray *friends;
@property (strong, nonatomic) NSMutableArray *forumsVideo;
@property (strong, nonatomic) NSMutableArray *channelVideos;
@property (strong, nonatomic) NSMutableDictionary *commentsDict;
@property (strong, nonatomic) NSMutableArray *newsfeedsVideos;
@property (strong, nonatomic) NSMutableArray *uploadedChannlVideos;
@property (strong, nonatomic) NSMutableArray *friendsArray;
@property (strong, nonatomic) NSMutableArray *speakersStoriesArray;

@property (strong, nonatomic) NSMutableArray *friendsVcArray;
@property (strong, nonatomic) NSMutableArray *reqsArray;
@property (strong, nonatomic) NSMutableArray *pendingArray;


@property (assign, nonatomic) NSInteger offlineArrayCount;
@property (assign, nonatomic) NSInteger onlineVideoCount;
@property (assign, nonatomic) NSInteger commentsCount;
@property (assign, nonatomic) NSInteger commentsCurrentCount; // new code
@property (assign, nonatomic) NSString *bio;
@property (assign, nonatomic) BOOL isReloadMyCorner;
@property (assign, nonatomic) BOOL isListView;
@property (assign, nonatomic) BOOL isSpeakerViewChanged;
@property (assign, nonatomic) BOOL isFriendViewChanged;
@property (assign, nonatomic) BOOL isInboxViewChanged;
@property (assign, nonatomic) BOOL isMyCornerViewChanged;
@property (assign, nonatomic) BOOL isUploading;
@property (assign, nonatomic) NSInteger rotationOrientation; // 180 = landscape right, 270 = landscape left
@property (assign, nonatomic) BOOL _isOnlineVideo;
@property (assign, nonatomic) BOOL isReloadCornerCollection;

@property (strong, nonatomic) NSMutableArray *friendsForTagging;


@property (strong, nonatomic) NSMutableArray *topicsArray;

@property (nonatomic) BOOL moveFromMenu;

@property (strong, nonatomic) NSString *chatFriendId;
@property (nonatomic)         BOOL            isChaVC;

@property (nonatomic)         BOOL            famousUsersFetchedFirst;
@property (nonatomic)         BOOL            notifcationsFetcedFirst;
@property (nonatomic)         int             whoToFollowPageNum;
@property (nonatomic)         int             notificationsPageNum;
@property (nonatomic)         int             whoToFollowPreviousCount;
@property (nonatomic)         int             notificationsPreviousCount;

@property (assign) int  languageCode;
@property (nonatomic) BOOL isLangaugeSelected;
@property (nonatomic)         BOOL            isFromDeepLink;
@property (nonatomic)         BOOL            isFromBackground; // check offline array when app is active after killed
@property (nonatomic)         BOOL            isReloadFromBackground; // check offline array when app is active after killed and reload my corner
@property (nonatomic)         BOOL            changeFriendCount; // remove red badge when count is 0
@property (strong,nonatomic) UIImage *thumbToUpload;
@property (strong, nonatomic) UIApplication *application;
+ (id)sharedManager;
- (void)dellocDate;
@end
