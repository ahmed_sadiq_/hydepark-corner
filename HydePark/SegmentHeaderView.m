//
//  SegmentHeaderView.m
//  HydePark
//
//  Created by Ahmed Sadiq on 15/12/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "SegmentHeaderView.h"

@implementation SegmentHeaderView

+ (SegmentHeaderView *)headerView {
    SegmentHeaderView *headerView = [[NSBundle mainBundle] loadNibNamed:@"SegmentHeaderView" owner:self options:nil][0];
    return headerView;
}

@end
