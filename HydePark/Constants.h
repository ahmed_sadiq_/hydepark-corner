//
//  Constants.h
//  HydePark
//
//  Created by Ahmed Sadiq on 14/05/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#ifndef HydePark_Constants_h
#define HydePark_Constants_h

#pragma mark-
static NSString *sProjectName = @"HydeparkCoreDataModel";
#pragma mark Server Calling Constants
//LIVE
#define SERVER_URL @"https://hydeparkcorner.com/api/page.php" //https://hydeparkcorner.com/api/page.php (live url)
//#define SERVER_URL @"https://hydeparkcorner.com/dev_hydepark/api/page.php"  //(Development url)



//DEV
//#define SERVER_URL @"http://dev.hydeparkcorner.com/api/page.php"

// New Server
//#define SERVER_URL @"http://wetune.com/api/page.php"

#define METHOD_SIGN_UP @"userSignUp"
#define METHOD_LOG_IN @"userLogin"
#define BlueThemeColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define METHOD_UPLOAD_STATUS @"uploadStatusV3"
#define METHOD_LOGOUT @"userLogout"
#define METHOD_GET_PROFILE @"getProfile"
#define METHOD_GET_USERPROFILE @"getProfileById"
#define METHOD_UPDATE_PROFILE @"updateProfile"
#define METHOD_REJECT_REEQUEST @"rejectRequest"
#define METHOD_ACCEPT_REQUEST @"acceptRequest"
#define METHOD_SEND_REQUEST @"sendRequest"
#define METHOD_DELETE_REQUEST @"deleteFriend"
#define METHOD_DELETE_FRIEND @"deleteFriend"
#define METHOD_SEARCH_FRIEND @"searchFriend"
#define METHOD_LIKE_COMMENT @"likeComment"
#define METHOD_LIKE_POST @"likePost"
#define METHOD_POST_SEEN @"postSeen"
#define METHOD_COMMENT_SEEN @"CommentSeen"
#define METHOD_DELETE_POST @"deletePost"
#define METHOD_GET_BEAMS_BY_TOPICS @"getBeamsByTopicids"
#define METHOD_GET_MY_BEAMS @"getMyBeams"
#define METHOD_GET_HOME_CONTENTS @"getHomeContent"
#define METHOD_GET_CHAT @"getVideoChatMessages"

#define METHOD_GET_MY_CHENNAL @"getMyChannel"
#define METHOD_GET_POST_BY_ID @"getCommentsByParentId"
#define METHOD_EDIT_POST @"editpost"
#define METHOD_TRENDING_VIDEOS @"getTrendingVideos"
#define METHOD_COMMENTS_BY_PARENT_ID @"getCommentsByParentId"
#define METHOD_COMMENTS_POST @"commentPostV3"
#define METHOD_GET_TOPICS @"getTopics"
#define METHOD_SEARCH_TOPICS @"searchTopics"
#define METHOD_GET_FAMOUS_USERS @"getFamousUsers"
#define METHOD_GET_USERS_CHANNEL @"getUserChannelById"
#define METHOD_GET_NOTIFICATIONS @"getNotifications"
#define METHOD_GET_FOLLOWING_AND_FOLLOWERS @"getFollowersFollowing"
#define METHOD_NOTIFICATIONS_SEEN @"notificationSeen"
#define METHOD_GET_FRIENDS_CHAT @"getVideoChatThreads"
#define METHOD_DELETE_CHAT @"deleteVideoChatThread"
#define METHOD_SAHREBEAM_WITH_FRIEND @"shareBeamWithFriend"
#define METHOD_POST_SEEN @"postSeen"
#define METHOD_UPDATE_MY_CORNER @"updateOrderTimeStamp"
#define METHOD_GET_TOPICS_V3 @"getTopicsV3"
#define METHOD_SPEED_TEST @"uploadSpeedTest"



#define METHOD_GET_NEW_REQUESTS @"getRecievedFriendRequest"
#define METHOD_GET_FRIENDS @"getMyFriends"
#define METHOD_GET_SENT_REQUESTS @"getSentFriendRequest"

#define LOAD_NEW_REQUESTS @"loadNewRequests"
#define LOAD_SENT_REQUESTS @"loadSentRequests"
#define LOAD_FRIENDS @"loadFriends"


#define SET_NIGHT_MODE @"setNightMode"
#define CLEAR_NIGHT_MODE @"clearNightMode"

#pragma mark-
#pragma mark Server Response
#define SUCCESS 1

#pragma mark-
#pragma mark Screen Sizes
#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667)
#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568)
#define IS_IPHONE_5S ([[UIScreen mainScreen] bounds].size.height == 568)
#define IS_IPHONEX (([[UIScreen mainScreen] bounds].size.height-812)?NO:YES)

#define IS_IPHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480)
#define IS_IPAD ([[UIScreen mainScreen] bounds].size.height == 1024)
#define IS_IPHONE_6Plus ([[UIScreen mainScreen] bounds].size.height == 736)

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#pragma mark-
#pragma mark Social Logins
#define GOOGlE_SCHEME @"com.txlabz.hydeparks"
#define FACEBOOK_SCHEME @"fb669173346520177"

//#define Client_Id @"922745837972-pn2tsofqg8dqfk3eijjl63sgg15lrj2e.apps.googleusercontent.com"
#define Client_Id @"910966812831-tugtns7pb6oqmasmebqlbufu78k418nd.apps.googleusercontent.com"
///HydePark Instagram ////
#define KAUTHURL @"https://api.instagram.com/oauth/authorize/"
#define kAPIURl @"https://api.instagram.com/v1/users/"
#define KCLIENTID @"1fb26065dc6f4ed29a8c7f935b4e6324"
#define KCLIENTSERCRET @"a06b76abab31430f8a1539b34e3ea885"
#define kREDIRECTURI @"http://opensource.brickred.com/socialauthdemo/socialAuthSuccessAction.do"

#pragma mark -------- Flury Envents ---------------

#define FLURRY_API_KEY @"FPZKTDP9TVGHCTMK6PKT"
#define FLURRY_EVENT_SPLASH @"HPC_SPLASH";
#define FLURRY_EVENT_SIGN_UP @"HPC_SIGN_UP";
#define FLURRY_EVENT_SIGN_IN_EMAIL @"HPC_SIGN_IN_EMAIL";
#define FLURRY_EVENT_SIGN_IN_FB @"HPC_SIGN_IN_FB";
#define FLURRY_EVENT_SIGN_IN_G_PLUS @"HPC_SIGN_IN_G_PLUS";
#define FLURRY_EVENT_FAMOUS_CORNERS @"HPC_FAMOUS_CORNERS";
#define FLURRY_EVENT_HOME @"HPC_HOME";
#define FLURRY_EVENT_MY_ARCHIVE @"HPC_MY_ARCHIVE";
#define FLURRY_EVENT_TRENDING @"HPC_TRENDING";
#define FLURRY_EVENT_WHO_TO_FOLLOW @"HPC_WHO_TO_FOLLOW";
#define FLURRY_EVENT_NOTIFICATIONS @"HPC_NOTIFICATIONS";
#define FLURRY_EVENT_BEAM_FROM_GALLERY @"HPC_BEAM_FROM_GALLERY";
#define FLURRY_EVENT_NO_OF_BEAMS @"HPC_NO_OF_BEAMS";
#define FLURRY_EVENT_REPLIES @"HPC_REPLIES";
#define FLURRY_EVENT_PLAY_BEAM @"HPC_PLAY_BEAM";
#define FLURRY_EVENT_RECORD_BEAM @"HPC_RECORD_BEAM";
#define FLURRY_EVENT_RECORD_AUDIO @"HPC_RECORD_AUDIO";
#define FLURRY_EVENT_RECORD_ANONYMOUS @"HPC_RECORD_ANONYMOUS";
#define FLURRY_EVENT_FRIEND_CORNER @"HPC_FRIEND_CORNER_";
#define FLURRY_EVENT_SEARCH @"HPC_SEARCH_";



#define APP_DELEGATE ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define SET_IMAGE(image) [UIImage imageNamed:(image)]
#endif
