//
//  StreamViewController.m
//  HydePark
//
//  Created by Babar Hassan on 10/12/17.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "StreamViewController.h"

@interface StreamViewController ()
@property (weak, nonatomic) IBOutlet UIView *recorderView;

@end

@implementation StreamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpLiveStram];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpLiveStram{
    
    
    AXStreamaxiaSDK *sdk = [AXStreamaxiaSDK sharedInstance];
    [sdk setupSDKWithCompletion:^(BOOL success, AXError *error){
        
        
        [sdk debugPrintSDKStatus];
        
        
    }];
    
//    NSURL *bundleURL = [[NSBundle mainBundle] URLForResource:@"certificate" withExtension:@"bundle"];
//    NSBundle *bundle = [NSBundle bundleWithURL:bundleURL];
//    [sdk setupSDKWithURL:bundle.bundleURL withCompletion:^(BOOL success, AXError *error) {
//        
//        [sdk debugPrintSDKStatus];
//        
//    }];
    
    AXStreamInfo *info = [AXStreamInfo streamInfo];
    info.useSecureConnection = NO; info.serverAddress = @"23.235.227.213"; info.applicationName = @"test"; info.streamName = @"demo"; info.username = @"";
    
    info.password = @"";
    // The default recorder settings
    AXRecorderSettings *settings = [AXRecorderSettings recorderSettings];
    
    AXRecorder *recorder = [AXRecorder recorderWithStreamInfo:info settings:settings];
    recorder.recorderDelegate = self;
    AXError *error;
    // Enable adaptive bitrate
    // Video quality will be adjusted based on available network and hardware resources [recorder activateFeatureAdaptiveBitRateWithError:&error];
    if (error) {
        // Handle error
    } else {
        // Succes
    }
    // Enable local save
    // The broadcast will be saved to the users camera roll when finished [recorder activateFeatureSaveLocallyWithError:&error];
    if (error) {
        // Handle error
    } else {
        // Succes
    }
    
    [recorder setupWithView:self.recorderView];
    
    [recorder prepareToRecord];
    
    [recorder startStreamingWithCompletion:^(BOOL success, AXError *error) { // ...
    
    
    }];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
