//
//  Utils.m
//  HydePark
//
//  Created by Ahmed Sadiq on 14/05/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "Utils.h"
#import "MyCornerVC.h"
#import "HomeViewController.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "CustomLoading.h"
#import "Alert.h"
#import "Followings.h"
#import "DataContainer.h"
#import "AFNetworking.h"
#import "NavigationHandler.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "NSData+AES.h"

@implementation Utils
+(void)removeProfileImg{
    NSString *extension = @"png";
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    while ((filename = [e nextObject])) {
        
        if ([[filename pathExtension] isEqualToString:extension]) {
            
            [fileManager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:filename] error:NULL];
        }
    }
}
+ (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString*)getTimeDifference:(NSString *)time1 time2:(NSString *)time2{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
   
    
    NSDate *date1 = [dateFormatter dateFromString:time1];
    NSDate *date2 = [dateFormatter dateFromString:time2];
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    NSCalendarUnit unitFlags = NSCalendarUnitSecond | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth;
    NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:date1  toDate:date2  options:0];
    if([breakdownInfo month])
        return [NSString stringWithFormat:@"%ldM",(long)[breakdownInfo month]];
    else if([breakdownInfo day])
        return [NSString stringWithFormat:@"%ldd",(long)[breakdownInfo day]];
    else if([breakdownInfo hour])
        return [NSString stringWithFormat:@"%ldh",(long)[breakdownInfo hour]];
    else if([breakdownInfo minute])
        return [NSString stringWithFormat:@"%ldm",(long)[breakdownInfo minute]];
    else if([breakdownInfo second])
        return [NSString stringWithFormat:@"%lds",(long)[breakdownInfo second]];
    return @"1s";
}
+ (NSString*)getFollowersRange:(NSString *)followersCount{
    NSString *range;
    NSInteger Count = [followersCount integerValue];
    switch (Count) {
        case 1 ... 4:
            range = @"1 - 5";
            break;
        case 5 ... 9:
            range = @"5 - 10";
            break;
        case 10 ... 49:
            range = @"10 - 50";
            break;
        case 50 ... 99:
            range = @"50 - 100";
            break;
        case 100 ... 499:
            range = @"100 - 500";
            break;
        case 500 ... 999:
            range = @"500 - 1K";
            break;
        case 1000 ... 4999:
            range = @"1K - 5K";
            break;
            
        default:
            range = @"0";
            break;
    }
    return [NSString stringWithFormat:@ "%@  Followers",range];
}
+(NSString *)getLocalDateTimeFromUTC:(NSString *)strDate
{
    NSDateFormatter *dtFormat = [[NSDateFormatter alloc] init];
    [dtFormat setLocale:[NSLocale systemLocale]];
    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dtFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *aDate = [dtFormat dateFromString:strDate];
    
    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dtFormat setTimeZone:[NSTimeZone systemTimeZone]];
    
    return [dtFormat stringFromDate:aDate];
}

+(NSString *) getDecryptedTextFor : (NSString *)encryptedText
{
    @try {
        NSData *cipher = [[NSData alloc] init];
        cipher = [encryptedText dataUsingEncoding:NSUTF8StringEncoding];
        NSData *decryptedData = [NSData AES128DecryptedData:cipher];
        if(!decryptedData)
        {
            return encryptedText;
        }
        NSString *decodedString = [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
        return decodedString;
    }
    @catch (NSException *exception) {
        return encryptedText;
    }
}

+(NSString *) returnDateForInProgressOfMyCorner : (NSString*) dateToParse {
    NSLocale *arabicLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ar"];
    NSLocale *englishLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
    NSLocale *spanishLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
    NSLocale *persianLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fa"];
    NSLocale *portLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt"];
    
    
    //    dateToParse = [self getLocalDateTimeFromUTC:dateToParse];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *myString = dateToParse;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale systemLocale]];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"MMM-dd"];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat: @"HH:mm"];
    
    NSString *language = [Utils getLanguageCode];
    NSInteger langCode = [language integerValue];
    switch (langCode) {
        case 0:
            [formatter setLocale:englishLocale];
            [timeFormatter setLocale:englishLocale];
            break;
        case 1:
            [formatter setLocale:arabicLocale];
            [timeFormatter setLocale:arabicLocale];
            break;
        case 2:
            [formatter setLocale:spanishLocale];
            [timeFormatter setLocale:spanishLocale];
            break;
        case 3:
            [formatter setLocale:persianLocale];
            [timeFormatter setLocale:persianLocale];
            break;
        case 4:
            [formatter setLocale:portLocale];
            [timeFormatter setLocale:portLocale];
            break;
            
        default:
            [formatter setLocale:englishLocale];
            [timeFormatter setLocale:englishLocale];
            break;
    }
    
    NSString *stringFromDate = [formatter stringFromDate:yourDate];
    NSString *stringFromTime = [timeFormatter stringFromDate:yourDate];
    
    return [NSString stringWithFormat:@"%@ %@ %@",stringFromDate,NSLocalizedString(@"at", nil),stringFromTime];
}

+(NSString *) returnDate : (NSString*) dateToParse {
    NSLocale *arabicLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ar"];
    NSLocale *englishLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
    NSLocale *spanishLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
    NSLocale *persianLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fa"];
    NSLocale *portLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt"];
    
    
    dateToParse = [self getLocalDateTimeFromUTC:dateToParse];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *myString = dateToParse;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale systemLocale]];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"MMM-dd"];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat: @"HH:mm"];
    
    NSString *language = [Utils getLanguageCode];
    NSInteger langCode = [language integerValue];
    switch (langCode) {
        case 0:
            [formatter setLocale:englishLocale];
            [timeFormatter setLocale:englishLocale];
            break;
        case 1:
            [formatter setLocale:arabicLocale];
            [timeFormatter setLocale:arabicLocale];
            break;
        case 2:
            [formatter setLocale:spanishLocale];
            [timeFormatter setLocale:spanishLocale];
            break;
        case 3:
            [formatter setLocale:persianLocale];
            [timeFormatter setLocale:persianLocale];
            break;
        case 4:
            [formatter setLocale:portLocale];
            [timeFormatter setLocale:portLocale];
            break;
            
        default:
            [formatter setLocale:englishLocale];
            [timeFormatter setLocale:englishLocale];
            break;
    }
    
    NSString *stringFromDate = [formatter stringFromDate:yourDate];
    NSString *stringFromTime = [timeFormatter stringFromDate:yourDate];
    
    return [NSString stringWithFormat:@"%@ %@ %@",stringFromDate,NSLocalizedString(@"at", nil),stringFromTime];
}

+(NSString *) returnDateForInProgress : (NSString*) dateToParse {
    NSLocale *arabicLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ar"];
    NSLocale *englishLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
    NSLocale *spanishLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
    NSLocale *persianLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fa"];
    NSLocale *portLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt"];
    
    
//    dateToParse = [self getLocalDateTimeFromUTC:dateToParse];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *myString = dateToParse;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale systemLocale]];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"MMM-dd"];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat: @"HH:mm"];
    
    NSString *language = [Utils getLanguageCode];
    NSInteger langCode = [language integerValue];
    switch (langCode) {
        case 0:
            [formatter setLocale:englishLocale];
            [timeFormatter setLocale:englishLocale];
            break;
        case 1:
            [formatter setLocale:arabicLocale];
            [timeFormatter setLocale:arabicLocale];
            break;
        case 2:
            [formatter setLocale:spanishLocale];
            [timeFormatter setLocale:spanishLocale];
            break;
        case 3:
            [formatter setLocale:persianLocale];
            [timeFormatter setLocale:persianLocale];
            break;
        case 4:
            [formatter setLocale:portLocale];
            [timeFormatter setLocale:portLocale];
            break;
            
        default:
            [formatter setLocale:englishLocale];
            [timeFormatter setLocale:englishLocale];
            break;
    }
    
    NSString *stringFromDate = [formatter stringFromDate:yourDate];
    NSString *stringFromTime = [timeFormatter stringFromDate:yourDate];
    
    return [NSString stringWithFormat:@"%@ %@ %@",stringFromDate,NSLocalizedString(@"at", nil),stringFromTime];
}
+(NSString*)suffixNumber:(NSString*)numb
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterNoStyle;
    NSNumber *number = [f numberFromString:numb];
    if (!number)
        return @"";
    long long num = [number longLongValue];
    int s = ( (num < 0) ? -1 : (num > 0) ? 1 : 0 );
    NSString* sign = (s == -1 ? @"-" : @"" );
    num = llabs(num);
    if (num < 1000)
        return [NSString stringWithFormat:@"%@%lld",sign,num];
    
    int exp = (int) (log(num) / 3.f); //log(1000));
    
    NSArray* units = @[@"K",@"M",@"G",@"T",@"P",@"E"];
    
    return [NSString stringWithFormat:@"%@%.1f%@",sign, (num / pow(1000, exp)), [units objectAtIndex:(exp-1)]];
}
+(NSString *)abbreviateNumber:(NSString *)numb withDecimal:(int)dec {
    
    NSString *abbrevNum = @"0";
    int numI = [numb intValue];
    float number = (float)numI;
    dec = 1;
    DataContainer *sharedManager = [DataContainer sharedManager];
    NSArray *abbrev;
    if (sharedManager.languageCode == 1 ){
        abbrev = @[@"مليار",@"مليون",@"أَلف"];
    }
    else{
        abbrev = @[@"K", @"M", @"B"];
        
    }
    if (number >= 1000) {
        for (int i = abbrev.count - 1; i >= 0; i--) {
            // Convert array index to "1000", "1000000", etc
            int size = pow(10,(i+1)*3);
            if(size <= number) {
                // Here, we multiply by decPlaces, round, and then divide by decPlaces.
                // This gives us nice rounding to a particular decimal place.
                number = number/size;
                NSString *numberString = [NSString stringWithFormat:@"%.1f", number];
                // Add the letter for the abbreviation
                abbrevNum = [NSString stringWithFormat:@"%@%@", numberString, [abbrev objectAtIndex:i]];
            }
        }
    }
    else{
        if(numb == (id)[NSNull null] || numb.length == 0){
            return @"0";
        }
        else{
            abbrevNum = [NSString stringWithFormat:@"%@", numb];
        }
    }
    return abbrevNum;
}
+(NSString *)simpleAbbNumber:(NSString *)numb withDecimal:(int)dec {
    
    NSString *abbrevNum = @"0";
    int numI = [numb intValue];
    float number = (float)numI;
    dec = 1;
    DataContainer *sharedManager = [DataContainer sharedManager];
    NSArray *abbrev = @[@"K", @"M", @"B"];
    
    if (number >= 1000) {
        for (int i = abbrev.count - 1; i >= 0; i--) {
            // Convert array index to "1000", "1000000", etc
            int size = pow(10,(i+1)*3);
            if(size <= number) {
                // Here, we multiply by decPlaces, round, and then divide by decPlaces.
                // This gives us nice rounding to a particular decimal place.
                number = number/size;
                NSString *numberString = [self floatToString:number];
                // Add the letter for the abbreviation
                abbrevNum = [NSString stringWithFormat:@"%@%@", numberString, [abbrev objectAtIndex:i]];
            }
        }
    }
    else{
        if(numb == (id)[NSNull null] || numb.length == 0){
            return @"0";
        }
        else{
            abbrevNum = [NSString stringWithFormat:@"%@", numb];
        }
    }
    return abbrevNum;
}
+(NSString *) floatToString:(float) val {
    NSString *ret = [NSString stringWithFormat:@"%.1f", val];
    unichar c = [ret characterAtIndex:[ret length] - 1];
    while (c == 48 || c == 46) { // 0 or .
        ret = [ret substringToIndex:[ret length] - 1];
        c = [ret characterAtIndex:[ret length] - 1];
    }
    return ret;
}
//+ (NSString *)getLocalDateTimeFromUTC:(NSString *)strDate
//{
//    NSDateFormatter *dtFormat = [[NSDateFormatter alloc] init];
//    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    [dtFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
//    NSDate *aDate = [dtFormat dateFromString:strDate];
//    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    [dtFormat setTimeZone:[NSTimeZone systemTimeZone]];
//    return [dtFormat stringFromDate:aDate];
//}
//
//+(NSString *) returnDate : (NSString*) dateToParse {
//    dateToParse = [self getLocalDateTimeFromUTC:dateToParse];
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSString *myString = dateToParse;
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
//    NSDate *yourDate = [dateFormatter dateFromString:myString];
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat: @"MMM-dd"];
//    NSString *stringFromDate = [formatter stringFromDate:yourDate];
//    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
//    [timeFormatter setDateFormat: @"HH:mm"];
//    NSString *stringFromTime = [timeFormatter stringFromDate:yourDate];
//    return [NSString stringWithFormat:@"%@ at %@",stringFromDate,stringFromTime];
//}
+(UIImage*) drawImage:(UIImage*) watermarkImage
              inImage:(UIImage*) backgroundImage
              atPoint:(CGPoint)  point
{
    UIGraphicsBeginImageContext(backgroundImage.size);
    [backgroundImage drawInRect:CGRectMake(0, 0, backgroundImage.size.width, backgroundImage.size.height)];
    [watermarkImage drawInRect:CGRectMake(70 , 70, watermarkImage.size.width/2, watermarkImage.size.height/2)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}
+(void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    [CustomLoading showAlertMessage];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   [CustomLoading DismissAlertMessage];
                                   completionBlock(NO,nil);
                               }
                           }];
}
+(NSString *)decodeForEmojis:(NSString *)toDecode{
    NSData *data = [toDecode dataUsingEncoding:NSUTF8StringEncoding];
    NSString *decodeValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    return decodeValue;
}

+(NSString *)encodeForEmojis:(NSString *)toEncode{
    NSData *data = [toEncode dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *encodeValue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return encodeValue;
}
+(void)sendUserToken{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSString *deviceTokens = [AppDelegate getDeviceToken];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"registerDevice",@"method",deviceTokens,@"device_id",token,@"Session_token",@"IOS",@"device_type",@"notAndroid",@"gcm_reg_id",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
    }];
}
+(UIAlertController *) showCustomAlert:(NSString *) title andMessage:(NSString *)message yesButton:(NSString *)yesButton cancelButton:(NSString *)cancelButton{
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *y = [UIAlertAction
                        actionWithTitle:yesButton
                        style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction * action) {
                            //Handle your yes please button action here
                        }];
    
    UIAlertAction *n = [UIAlertAction
                        actionWithTitle:cancelButton
                        style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction * action) {
                            //Handle no, thanks button
                        }];
    
    [alert addAction:y];
    [alert addAction:n];
    
    return alert;
}
+(void)showAlert:(NSString *)message{
    Alert *alert = [[Alert alloc] initWithTitle:message duration:(float)3.0f completion:^{
        //
    }];
    [alert setDelegate:self];
    [alert setShowStatusBar:YES];
    [alert setAlertType:AlertTypeSuccess];
    [alert setIncomingTransition:AlertIncomingTransitionTypeSlideFromTop];
    [alert setOutgoingTransition:AlertOutgoingTransitionTypeSlideToTop];
    [alert setBounces:YES];
    [alert showAlert];
    
}
+(NSString *)currentDeviceVersion{
    NSString *version = [[UIDevice currentDevice] systemVersion];
    return version;
}

#pragma mark caching
+(BOOL)userIsCeleb{
    BOOL isC = [[NSUserDefaults standardUserDefaults] boolForKey:@"isCeleb"];
    return isC;
}
+(void)setCachedInboxCount:(NSString *)count{
    [[NSUserDefaults standardUserDefaults] setObject:count forKey:@"chachedInboxC"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(NSString *)getCachedInboxCount{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"chachedInboxC"];
    return savedValue;
}
+(void)setCachedPendingReqCount:(NSString *)count{
    [[NSUserDefaults standardUserDefaults] setObject:count forKey:@"cachedPendingReq"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(NSString *)getCachedPendingrReqCount{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"cachedPendingReq"];
    return savedValue;
}
+(void)setcachedViewsCount:(NSString *)count{
    [[NSUserDefaults standardUserDefaults] setObject:count forKey:@"cachedViewsCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(NSString *)getCachedViewsCount{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"cachedViewsCount"];
    return savedValue;
}
+(void)setCachedFollowersCount:(NSString *)count{
    [[NSUserDefaults standardUserDefaults] setObject:count forKey:@"chachedFollowersCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(NSString *)getCachedFollowersCount{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"chachedFollowersCount"];
    return savedValue;
}
+(void)setCachedBeamsCount:(NSString *)count{
    [[NSUserDefaults standardUserDefaults] setObject:count forKey:@"cachedBeamsCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(NSString *)getcachedBeamsCount{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"cachedBeamsCount"];
    return savedValue;
}
+(void)setChacedFriendsCount:(NSString *)count{
    [[NSUserDefaults standardUserDefaults] setObject:count forKey:@"cachedFriendsCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(NSString *)getchachedFriendsCount{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"cachedFriendsCount"];
    return savedValue;
}
+(void)clearUserDefaults{

    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        if([key isEqualToString:@"languageCode"] || [key isEqualToString:@"nightMode"])
        {
            NSLog(@"languageCode");
        }else{
            
            [defs removeObjectForKey:key];
        }
    }
    [defs synchronize];

}

+(NSMutableArray *)getSortedArray:(NSMutableArray *)arrayTobeSorted{
    NSMutableArray *respondArray = [[NSMutableArray alloc] init];
    NSMutableArray *cancelArray = [[NSMutableArray alloc] init];
    NSMutableArray *addFriendArray = [[NSMutableArray alloc] init];
    NSMutableArray *friendArr = [[NSMutableArray alloc] init];
    NSArray *items = @[@"ACCEPT_REQUEST", @"PENDING", @"FRIEND",@"ADD_FRIEND"];
    for(int i = 0; i < arrayTobeSorted.count; i++){
        Followings *tempObj = [arrayTobeSorted objectAtIndex:i];
//        tempObj.fullName = [self getDecryptedTextFor:tempObj.fullName];
        NSInteger item = [items indexOfObject:tempObj.status];
        switch (item) {
            case 0:
                [respondArray   addObject:tempObj];
                break;
            case 1:
                [cancelArray    addObject:tempObj];
                break;
            case 2:
                [friendArr      addObject:tempObj];
                break;
            case 3:
                [addFriendArray addObject:tempObj];
                break;
            default:
                break;
        }
    }
    NSMutableArray *parentArray = [[NSMutableArray alloc] init];
    if(respondArray.count > 0)
        [parentArray addObjectsFromArray:respondArray];
    if(cancelArray.count > 0)
        [parentArray addObjectsFromArray:cancelArray];
    if(addFriendArray.count > 0)
        [parentArray addObjectsFromArray:addFriendArray];
    if(friendArr.count > 0)
        [parentArray addObjectsFromArray:friendArr];
    return [parentArray mutableCopy];
}

+(NSMutableArray *)getSortedArrayDecrypted:(NSMutableArray *)arrayTobeSorted{
    NSMutableArray *respondArray = [[NSMutableArray alloc] init];
    NSMutableArray *cancelArray = [[NSMutableArray alloc] init];
    NSMutableArray *addFriendArray = [[NSMutableArray alloc] init];
    NSMutableArray *friendArr = [[NSMutableArray alloc] init];
    NSArray *items = @[@"ACCEPT_REQUEST", @"PENDING", @"FRIEND",@"ADD_FRIEND"];
    for(int i = 0; i < arrayTobeSorted.count; i++){
        Followings *tempObj = [arrayTobeSorted objectAtIndex:i];
        tempObj.fullName = [self getDecryptedTextFor:tempObj.fullName];
        NSInteger item = [items indexOfObject:tempObj.status];
        switch (item) {
            case 0:
                [respondArray   addObject:tempObj];
                break;
            case 1:
                [cancelArray    addObject:tempObj];
                break;
            case 2:
                [friendArr      addObject:tempObj];
                break;
            case 3:
                [addFriendArray addObject:tempObj];
                break;
            default:
                break;
        }
    }
    NSMutableArray *parentArray = [[NSMutableArray alloc] init];
    if(respondArray.count > 0)
        [parentArray addObjectsFromArray:respondArray];
    if(cancelArray.count > 0)
        [parentArray addObjectsFromArray:cancelArray];
    if(addFriendArray.count > 0)
        [parentArray addObjectsFromArray:addFriendArray];
    if(friendArr.count > 0)
        [parentArray addObjectsFromArray:friendArr];
    return [parentArray mutableCopy];
}


+(NSMutableDictionary *)getSortedDictionary:(NSMutableArray *)arrayTobeSorted{
    NSMutableArray *respondArray = [[NSMutableArray alloc] init];
    NSMutableArray *cancelArray = [[NSMutableArray alloc] init];
    NSMutableArray *addFriendArray = [[NSMutableArray alloc] init];
    NSMutableArray *friendArr = [[NSMutableArray alloc] init];
    NSArray *items = @[@"ACCEPT_REQUEST", @"PENDING", @"FRIEND",@"ADD_FRIEND"];
    for(int i = 0; i < arrayTobeSorted.count; i++){
        Followings *tempObj = [arrayTobeSorted objectAtIndex:i];
        NSInteger item = [items indexOfObject:tempObj.status];
        switch (item) {
            case 0:
                [respondArray   addObject:tempObj];
                break;
            case 1:
                [cancelArray    addObject:tempObj];
                break;
            case 2:
                [friendArr      addObject:tempObj];
                break;
            case 3:
                [addFriendArray addObject:tempObj];
                break;
            default:
                break;
        }
    }
    NSMutableDictionary *parentDict = [[NSMutableDictionary alloc] init];
    if(respondArray.count > 0)
        [parentDict setObject:respondArray forKey:@"accept"];
    if(cancelArray.count > 0)
        //[parentDict addObjectsFromArray:cancelArray];
        [parentDict setObject:cancelArray forKey:@"pending"];
    if(addFriendArray.count > 0)
        
        [parentDict setObject:addFriendArray forKey:@"friends"];
    if(friendArr.count > 0)
        [parentDict setObject:friendArr forKey:@"add_friends"];
    return [parentDict mutableCopy];
}

+(NSMutableArray *)getFriendsToShareBeam{
    NSMutableArray *filteredArray = [[NSMutableArray alloc] init];
    DataContainer *sharedManager = [DataContainer sharedManager];
    for (int i =0; i< sharedManager.followers.count ; i++){
        Followings *tempObj = [sharedManager.followers objectAtIndex:i];
        if([tempObj.status isEqualToString:@"FRIEND"] && tempObj.isFollowed == 1) {
            [filteredArray addObject:tempObj];
        }
    }
    return filteredArray;
}

+(void)shareBeamOnCorner:(NSString *)postId isComment:(NSString *)isComment friendId:(NSString *)friendId sharingOnFriendsCorner:(BOOL)sharingOnFriendsCorner{
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSDictionary *postDict;
    if(sharingOnFriendsCorner){
        postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"shareBeamOnCorner",@"method",
                    token,@"Session_token",postId,@"post_id",isComment,@"is_comment",friendId,@"friend_id",nil];
    }
    else{
        postDict = [NSDictionary dictionaryWithObjectsAndKeys:@"shareBeamOnCorner",@"method",
                    token,@"Session_token",postId,@"post_id",isComment,@"is_comment",nil];
    }
    
    [CustomLoading showAlertMessage];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [manager POST:SERVER_URL parameters:postDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [CustomLoading DismissAlertMessage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        int success = [[responseObject objectForKey:@"success"] intValue];
        if(success){
            NSDictionary *tempDict =  (NSDictionary *)[responseObject objectForKey:@"post"];
            VideoModel *vModel = [VideoModel new];
            vModel.title           = [tempDict objectForKey:@"caption"];
            vModel.comments_count  = [tempDict objectForKey:@"comment_count"];
            vModel.userName        = [tempDict objectForKey:@"full_name"];
            vModel.topic_id        = [tempDict objectForKey:@"topic_id"];
            vModel.user_id         = [tempDict objectForKey:@"user_id"];
            vModel.profile_image   = [tempDict objectForKey:@"profile_link"];
            vModel.like_count      = [tempDict objectForKey:@"like_count"];
            vModel.like_by_me      = [tempDict objectForKey:@"liked_by_me"];
            vModel.seen_count      = [tempDict objectForKey:@"seen_count"];
            vModel.video_angle     = [[tempDict objectForKey:@"video_angle"] intValue];
            vModel.beam_share_url = [tempDict objectForKey:@"beam_share_url"];
            vModel.deep_link = [tempDict objectForKey:@"deep_link"];
            vModel.video_link      = [tempDict objectForKey:@"video_link"];
            vModel.m3u8_video_link = [tempDict objectForKey:@"m3u8_video_link"];
            vModel.video_thumbnail_link = [tempDict objectForKey:@"video_thumbnail_link"];
            vModel.videoID         = [tempDict objectForKey:@"id"];
            vModel.Tags            = [tempDict objectForKey:@"tag_friends"];
            vModel.video_length    = [tempDict objectForKey:@"video_length"];
            vModel.is_anonymous    = [tempDict objectForKey:@"is_anonymous"];
            vModel.reply_count     = [tempDict objectForKey:@"reply_count"];
            vModel.current_datetime= [tempDict objectForKey:@"current_datetime"];
            vModel.uploaded_date   = [tempDict objectForKey:@"uploaded_date"];
            vModel.beam_share_url = [tempDict objectForKey:@"beam_share_url"];
            vModel.isMute          = @"0";
            vModel.privacy         = [tempDict objectForKey:@"privacy"];
            vModel.isThumbnailReq =[[tempDict objectForKey:@"is_thumbnail_required"] intValue];
            vModel.beamType = [[tempDict objectForKey:@"beam_type"] intValue];
            if([tempDict objectForKey:@"original_beam_data"]){
                vModel.originalBeamData = [tempDict objectForKey:@"original_beam_data"];
            }
            if(!sharingOnFriendsCorner){
                UINavigationController *nav = [[NavigationHandler getInstance] getNavigationController];
                NSArray *viewControllers = nav.viewControllers;
                for (UIViewController *anVC in viewControllers) {
                    if([anVC isKindOfClass:[HomeViewController class]]){
                        HomeViewController *home = (HomeViewController *)anVC;
                        NSArray *vccs = home.viewControllers;
                        UIViewController *topPageController = [vccs objectAtIndex:0];
                        if([topPageController isKindOfClass:[MyCornerVC class]]){
                            MyCornerVC *cornerVC = (MyCornerVC *)topPageController;
                            DataContainer *sharedManager = [DataContainer sharedManager];
                            [sharedManager.channelVideos insertObject:vModel atIndex:0];
                            int c = [sharedManager._profile.beams_count intValue];
                            c++;
                            sharedManager._profile.beams_count = [NSString stringWithFormat:@"%d",c];
                            sharedManager.isReloadMyCorner = YES;
                            [cornerVC.collectionHome reloadData];
                        }
                    }
                }
                
                [self showAlert:NSLocalizedString(@"rebeam_successfully", @"")];
            }else{
                [self showAlert:NSLocalizedString(@"beam_shared_success", @"")];
            }
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        [CustomLoading DismissAlertMessage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

+(NSString *)getTrimmedString:(NSString *)Name{
    NSRange spaceRange = [Name rangeOfString:@" "];
    if (spaceRange.location != NSNotFound) {
        NSString *trimmedString = [Name substringToIndex:spaceRange.location];
        return trimmedString;
    }
    return Name;
}

+(NSString *)getLanguageCode{
    NSInteger langCode = [[NSUserDefaults standardUserDefaults] integerForKey:@"languageCode"];
    DataContainer *shared = [DataContainer sharedManager];
    shared.languageCode = (int)langCode;
    NSString *langStr = [NSString stringWithFormat:@"%d",shared.languageCode];
    return langStr;
}

+(void)DownloadVideo:(NSString *)urlToDownload
{
    [Utils showAlert:NSLocalizedString(@"notified_completion", @"You will be notified when downloading has completed")];
    //    [ApiManager downloadFileWithUrl:urlToDownload completionBlock:^(NSString *filePath, BOOL isFinished) {
    //
    //        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    //        [library writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:filePath] completionBlock:^(NSURL *assetURL, NSError *error) {
    //            dispatch_async(dispatch_get_main_queue(), ^{
    //                if (error) {
    //                    NSLog(@"%@", error.description);
    //                }else {
    //                    [Utils showAlert:@"Beam saved to gallery."];
    //                }
    //            });
    //        }];
    //    }];
    
    //download the file in a seperate thread.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL  *url = [NSURL URLWithString:urlToDownload];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {
            NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"thefile.mp4"];
            [urlData writeToFile:filePath atomically:YES];
            
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            [library writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:filePath] completionBlock:^(NSURL *assetURL, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"File Saved !");
                    if (error) {
                        NSLog(@"%@", error.description);
                    }else {
//                        NSLog(@"Done :)");
                        [Utils showAlert:NSLocalizedString(@"beam_saved_to_gallery", @"Beam saved to gallery.")];
                    }
                });
            }];
            //saving is done on main thread
        }
    });
}

+ (void)moveViewPosition:(CGFloat)yPostition onView:(UIView *)view completion:(Completion) completion;
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [view setFrame:CGRectMake(view.frame.origin.x, yPostition, view.frame.size.width, view.frame.size.height)];
    [UIView commitAnimations];
    completion(YES);
}
@end
