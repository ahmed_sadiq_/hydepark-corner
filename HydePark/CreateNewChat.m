//
//  CreateNewChat.m
//  HydePark
//
//  Created by Samreen Noor on 25/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CreateNewChat.h"
#import "SearchCell.h"
#import "Constants.h"
#import "DataContainer.h"
#import "Followings.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+RoundImage.h"
#import "FriendChatVC.h"
#import "Utils.h"

@interface CreateNewChat ()
{
    DataContainer *sharedManager;
    NSUInteger        currentSelectedIndex;

}
@end

@implementation CreateNewChat

- (void)viewDidLoad {
    [super viewDidLoad];
    _friendsArray = [[NSMutableArray alloc] init];
    [self getFollowing];
}

-(void) getFollowing{
    [_friendsArray removeAllObjects];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *token = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"session_token"];
    NSString *userId = (NSString *)[[NSUserDefaults standardUserDefaults]objectForKey:@"id"];
    NSURL *url = [NSURL URLWithString:SERVER_URL];
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:METHOD_GET_FOLLOWING_AND_FOLLOWERS,@"method",
                              token,@"session_token",@"1",@"page_no",userId,@"user_id",@"1",@"following",nil];
    NSData *postData = [Utils encodeDictionary:postDict];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int success = [[result objectForKey:@"success"] intValue];
            if(success == 1)
            {
                NSArray *friends = [result objectForKey:@"following"];
                if([friends isKindOfClass:[NSArray class]]){
                    for(NSDictionary *tempDict in friends){
                        Followings *_responseData = [[Followings alloc] init];
                        _responseData.f_id = [tempDict objectForKey:@"id"];
                        _responseData.fullName = [tempDict objectForKey:@"full_name"];
                        _responseData.is_celeb = [tempDict objectForKey:@"is_celeb"];
                        _responseData.profile_link = [tempDict objectForKey:@"profile_link"];
                        _responseData.status = [tempDict objectForKey:@"state"];
                        _responseData.isFollowed = [[tempDict objectForKey:@"is_followed"] intValue];
                        [_friendsArray addObject:_responseData];
                    }
                }
                [_tblView reloadData];
            }
        }
    }];
}
#pragma mark TableView Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float returnValue;
    if (IS_IPAD)
        returnValue = 121.0f;
    else
        returnValue = 83.0f;
    
    return returnValue;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_friendsArray  count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _tblView.scrollEnabled = YES;
    NSLog(@"tblview Height %f",_tblView.frame.size.height);
    SearchCell *cell;
    float returnValue = 83;
    if(IS_IPAD)
        returnValue = 124;
    if (IS_IPAD) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchCell_iPad" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else{
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Followings *tempUsers = [[Followings alloc]init];
    tempUsers  = [_friendsArray  objectAtIndex:indexPath.row];
//    cell.friendsName.text = tempUsers.fullName;
    cell.friendsName.text = [Utils getDecryptedTextFor:tempUsers.fullName];
    //    cell.profilePic.imageURL = [NSURL URLWithString:tempUsers.profile_link];
    //    NSURL *url = [NSURL URLWithString:tempUsers.profile_link];
    //    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:tempUsers.profile_link] placeholderImage:[UIImage imageNamed:@"loading"]];
    
    [cell.profilePic roundImageCorner];
    
    cell.profilePic.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.profilePic.layer.shadowOpacity = 0.7f;
    cell.profilePic.layer.shadowOffset = CGSizeMake(0, 5);
    cell.profilePic.layer.shadowRadius = 5.0f;
    cell.profilePic.layer.masksToBounds = YES;
    
    cell.profilePic.layer.backgroundColor = [UIColor clearColor].CGColor;
    cell.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.profilePic.layer.borderWidth = 3.0f;
    cell.statusImage.hidden=YES;
    cell.activityInd.hidden=YES;
    cell.btnChat.hidden=NO;
    [cell.btnChat setTitle:@"Chat" forState:UIControlStateNormal];
    [cell.btnChat setTag:indexPath.row];
    [cell.btnChat addTarget:self action:@selector(startChat:) forControlEvents:UIControlEventTouchUpInside];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void) startChat:(UIButton *)sender{
    
    UIButton *statusBtn = (UIButton *)sender;
    currentSelectedIndex = statusBtn.tag;
    Followings *f = [_friendsArray objectAtIndex:currentSelectedIndex];
    FriendChatVC *frndvc; //= [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    if (IS_IPHONEX){
        frndvc = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC_IphoneX" bundle:nil];
    }else{
        frndvc = [[FriendChatVC alloc] initWithNibName:@"FriendChatVC" bundle:nil];
    }
    frndvc.seletedFollowing = f;
    frndvc.isLocalSearch = YES;
    frndvc.isFriend = YES;
    [[self navigationController] pushViewController:frndvc animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:true];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
