//
//  AudioVC.h
//  HydePark
//
//  Created by Osama on 03/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>
#import "AVFoundation/AVFoundation.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "DataContainer.h"
@interface AudioVC : UIViewController<AVAudioRecorderDelegate>
{
    NSTimer *timerToupdateLbl;
    BOOL isRecording;
    NSTimer* audioTimeOut;
    int secondsLeft;
    NSString *secondsConsumed;
    DataContainer *sharedManager;
}
@property (nonatomic, weak)  IBOutlet UILabel *countDownlabel;
@property (nonatomic, weak)  IBOutlet UIImageView *audioBtnImage;
@property (weak, nonatomic)  IBOutlet UIButton *audioRecordBtn;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (weak, nonatomic)  IBOutlet UIButton *closeBtnAudio;
@property (strong, nonatomic) NSData *audioData;
@property (weak, nonatomic)  UIImage *thumbnail;
@end
