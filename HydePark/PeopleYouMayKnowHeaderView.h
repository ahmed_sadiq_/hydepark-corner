//
//  PeopleYouMayKnowHeaderView.h
//  HydePark
//
//  Created by apple on 6/22/18.
//  Copyright © 2018 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeopleYouMayKnowHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lblSectionHeaderTitle;
@property (weak, nonatomic) IBOutlet UIButton *showAllBeamsButtonAction;

@end
