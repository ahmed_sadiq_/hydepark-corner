//
//  UserChannel.h
//  HydePark
//
//  Created by Apple on 22/02/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ViewController.h"
#import "UserChannelModel.h"
#import "VideoModel.h"
#import "CommentsModel.h"
#import "AsyncImageView.h"
#import "Followings.h"
#import "DataContainer.h"
@interface UserChannel : ViewController<UITableViewDataSource,UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>{
    IBOutlet UITableView *friendsChannelTable;
    IBOutlet UIView *FriendsProfileView;
    IBOutlet UILabel *friendsFollowings;
    IBOutlet UILabel *friendsFollowers;
    IBOutlet UILabel *friendsBeamcount;
    IBOutlet UILabel *celebBeamcount;
    IBOutlet UIButton *friendsStatusbtn;
    IBOutlet UIImageView *friendsCover;
    IBOutlet UIImageView *friendsImage;
    IBOutlet UILabel *friendsNamelbl;
    IBOutlet UIButton *friendsStatusbtnMid;
    IBOutlet UIView *bgView;
    IBOutlet UIActivityIndicatorView *footerIndicator;
    UIRefreshControl *refreshControl;
    NSUInteger currentSelectedIndex;
    NSString *friendId;
    VideoModel *videomodel;
    NSString *postID;
    NSString *ParentCommentID;
    CommentsModel *CommentsModelObj;
    NSArray *arrImages;
    NSArray *arrThumbnail;
    NSArray *CommentsArray;
    NSArray *commentsVideosArray;
    NSUInteger currentIndex;
    NSMutableArray *videoObj;
    NSArray *FollowingsArray;
    NSMutableArray *FollowingsAM;
    UserChannelModel *userChannelObj;
    NSArray *chPostArray;
    NSMutableArray *dataArray;
    int pageNum;
    BOOL cannotScrollMore;
    BOOL serverCall;
    DataContainer *sharedManager;
    UIView * footerView;

}
-(NSString *)getFid;
-(void)setFriendStatus:(int) status;
- (IBAction)getFollowings:(id)sender;
- (IBAction)getFollowers:(id)sender;
- (IBAction)getProfile:(id)sender;
- (IBAction)backToMyCorner:(id)sender;
@property (weak, nonatomic) IBOutlet  UIImageView *background;
@property (weak, nonatomic) IBOutlet  UIView *separator;
@property (weak, nonatomic) IBOutlet  UIView *lowerSeparator;
@property (weak, nonatomic) IBOutlet  UIView *myView;

@property (weak, nonatomic) IBOutlet  UIView *overlayView;
@property (weak, nonatomic) IBOutlet  UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet  UIView *picBorder;
@property (weak, nonatomic) IBOutlet  UIImageView *followBorder;
@property (weak, nonatomic) IBOutlet  UIView *picBorderMid;
@property (weak, nonatomic) IBOutlet  UIImageView *followBorderMid;
@property (weak, nonatomic) IBOutlet  UIImageView *friendsImageMid;
@property (weak, nonatomic) IBOutlet  UILabel *Followlbl;
@property (weak, nonatomic) IBOutlet  UILabel *followlblMid;
@property (weak, nonatomic) IBOutlet  UILabel *userNameMid;
@property (weak, nonatomic) IBOutlet  UILabel *celebBeamLbl;
@property (weak, nonatomic) IBOutlet  UILabel *gridLbl;
@property (weak, nonatomic) IBOutlet  UILabel *beamMeLbl;
@property (weak, nonatomic) IBOutlet  UILabel *goLiveLbl;
@property (weak, nonatomic) IBOutlet  UILabel *bioLbl;
@property (weak, nonatomic) IBOutlet  UIImageView *countImage;
@property (weak, nonatomic) IBOutlet  UILabel *follwersCount;
@property (weak, nonatomic) IBOutlet  UIImageView *adsBar;
@property (weak, nonatomic) IBOutlet  UIImageView *gridImageView;
@property (weak, nonatomic) IBOutlet  UIView *adsView;
@property (weak, nonatomic) IBOutlet  UIButton *followingBtn;
@property (weak, nonatomic) IBOutlet  UIButton *followersBtn;
@property (weak, nonatomic) IBOutlet  UIButton *emiratesbtn;
@property (weak, nonatomic) IBOutlet  UIButton *redbullbtn;
@property (weak, nonatomic) IBOutlet  UIButton *bbcbtn;
@property (weak, nonatomic) IBOutlet  UIView *viewToRound;
@property (weak, nonatomic) IBOutlet  UIView *profileViews;
@property (strong, nonatomic) UserChannelModel *ChannelObj;
@property (strong, nonatomic) NSString *friendID;
@property (weak, nonatomic)  IBOutlet UIActivityIndicatorView *actInd;
@property (weak, nonatomic)  IBOutlet  UILabel *followersRangelbl;
@property (weak, nonatomic)  IBOutlet UIView *viewMsg;
@property (strong, nonatomic)IBOutlet UIButton *msgButton;
@property (strong, nonatomic) Followings *responseData;
@property (strong, nonatomic) Followings *currentUser;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *zoomImageBtn;
@property (weak, nonatomic) IBOutlet UIButton *friendReqBtn;
@property (weak, nonatomic) IBOutlet  UIButton *gridListBtn;
@property (weak, nonatomic) IBOutlet UIView *celebView;
@property (weak, nonatomic) IBOutlet UILabel *cFollowersCount;
@property (weak, nonatomic) IBOutlet UILabel *verifiedLbl;
@property (weak, nonatomic) IBOutlet UIImageView *tickImg;
@property (weak, nonatomic) IBOutlet UIImageView *msgImg;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet  UIView *profileBlueView;
@property (assign, nonatomic) BOOL headerCheck;
@property (assign, nonatomic) BOOL firstCheck;
@property (assign, nonatomic) NSInteger currentIndexHome;
@end
