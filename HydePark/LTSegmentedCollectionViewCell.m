//
//  LTSegmentedCollectionViewCell.m
//  LTBlank
//
//  Created by Le Thang on 9/8/15.
//  Copyright (c) 2015 Le Thang. All rights reserved.
//

#import "LTSegmentedCollectionViewCell.h"

@interface LTSegmentedCollectionViewCell ()

@end

@implementation LTSegmentedCollectionViewCell

+ (UINib*) nib {
    return [UINib nibWithNibName:[self nibName] bundle:[NSBundle mainBundle]];
}

+ (NSString*) nibName {
    return NSStringFromClass([self class]);
}

- (void) configCellWithData:(id)data {
    lblTitle.text = data;
}

- (UIFont*) selectedFont {
    return [UIFont fontWithName:@"Montserrat" size:13.0];
}

- (UIFont*) normalFont {
    return [UIFont fontWithName:@"Montserrat" size:13.0];
}

- (void) setCellSelected:(BOOL)selected {
    if (selected) {
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"nightMode"] != nil)
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"nightMode"])
            {
                [lblTitle setTextColor:[UIColor blackColor]];
            }
            else
            {
                [lblTitle setTextColor:[UIColor whiteColor]];
            }
        }
        else
        {
            [lblTitle setTextColor:[UIColor whiteColor]];
        }
        [lblTitle setFont:[self selectedFont]];
    }
    else {
        [lblTitle setTextColor:[UIColor lightGrayColor]];
        [lblTitle setFont:[self normalFont]];
    }
}

@end
