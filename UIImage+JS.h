//
//  UIImage+JS.h
//  iOSApps
//
//  Created by Junaid Muhammad.
//  Copyright (c) 2015 Junaid Muhammad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (JS)


- (NSString *)base64String;

- (UIImage*)imageWithScaledToSize:(CGSize)newSize;
- (UIImage *)generateNonRotatedPhoto;
@end
