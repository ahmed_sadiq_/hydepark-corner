//
//  DEMOLeftMenuViewController.m
//  RESideMenuStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOLeftMenuViewController.h"
#import "HomeVC.h"
#import "UIViewController+RESideMenu.h"
#import "Topics.h"
#import "MyBeam.h"
#import "PopularUsersVC.h"
#import "NotificationsVC.h"
#import "NavigationHandler.h"
#import "Constants.h"
#import "MenuCell.h"
#import "ChatVC.h"
#import "AppDelegate.h"
#import "DataContainer.h"
#import "UIImageView+AFNetworking.h"
#import "EXPhotoViewer.h"
#import "Utils.h"
#import "NSData+AES.h"

@interface DEMOLeftMenuViewController ()
{
    DataContainer *shareMngr;
    AppDelegate *appDelegate;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DEMOLeftMenuViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.shouldShowNotification = YES;
    _userImg.layer.borderWidth = 4;
    _userImg.layer.borderColor = [[UIColor colorWithRed:241/255.0 green:248/255.0 blue:254/255.0 alpha:0.95] CGColor];
    _userImg.layer.cornerRadius = 25;
    _userImg.layer.masksToBounds = YES;
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Name"];
    NSString *userImg = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_img"];
    
    
    _lblUserName.text = [Utils getDecryptedTextFor:userName];
    //    _userImg.imageURL = [NSURL URLWithString:userImg];
    //    NSURL *url = [NSURL URLWithString:userImg];
    //    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    [self.userImg setImageWithURL:[NSURL URLWithString:userImg] placeholderImage:[UIImage imageNamed:@"place_holder"]];
    
}

- (UIImage *)stringToUIImage:(NSString *)string
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string
                                                      options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return [UIImage imageWithData:data];
}
-(void) updateUserProfile:(NSNotification *) notification{
    if ([notification.name isEqualToString:@"updateUserProfile"])
    {
        NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Name"];
        NSString *userImg = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_img"];
        
        
        _lblUserName.text = [Utils getDecryptedTextFor:userName];
        //        _userImg.imageURL = [NSURL URLWithString:userImg];
        //        NSURL *url = [NSURL URLWithString:userImg];
        //        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        [self.userImg setImageWithURL:[NSURL URLWithString:userImg] placeholderImage:[UIImage imageNamed:@"pic.png"]];
    }
    else if ([notification.name isEqualToString:@"updateUserProfileImg"]){
        NSDictionary* userInfo = notification.object;
        UIImage *userImg = userInfo[@"IMG"];
        [_userImg setImage:userImg];
        
    }
    else if ([notification.name isEqualToString:@"hideSideBarNotification"]){
        self.shouldShowNotification = FALSE;
        [self.tableView reloadData];
    } else if ([notification.name isEqualToString:@"showInSideBar"]){
        self.shouldShowNotification = YES;
        [self.tableView reloadData];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateUserProfile" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateUserProfileImg" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateUserProfile:)
                                                 name:@"updateUserProfile"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateUserProfile:)
                                                 name:@"updateUserProfileImg"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateUserProfile:)
                                                 name:@"hideSideBarNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateUserProfile:)
                                                 name:@"showInSideBar"
                                               object:nil];
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Name"];
    NSString *userImg = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_img"];

    
    _lblUserName.text = [Utils getDecryptedTextFor:userName];
    //    _userImg.imageURL = [NSURL URLWithString:userImg];
    //    NSURL *url = [NSURL URLWithString:userImg];
    //    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    //[_userImg sd_setImageWithURL:[NSURL URLWithString:userImg] placeholderImage:[UIImage imageNamed:@""]];
    
    [_tableView reloadData];
}
    
#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*HomeVC *homeVC1 = [[HomeVC alloc] init];
     Topics *topic = [[Topics alloc] init];
     MyBeam *mybeam = [[MyBeam alloc] init];
     PopularUsersVC *userVc = [[PopularUsersVC alloc] init];
     NotificationsVC *notifVc = [[NotificationsVC alloc] init];
     ChatVC *chatVc = [[ChatVC alloc] init];*/
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            
            /*[self.sideMenuViewController setContentViewController:homeVC1 animated:YES];
             [self.sideMenuViewController hideMenuViewController];
             */
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"MoveToMyCorner"
             object:nil];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"MoveToCornerIndex"
             object:nil];
            [[NavigationHandler getInstance] MoveToHomeScreen];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 1:
            [[NavigationHandler getInstance]MoveToMyBeam];
            [self.sideMenuViewController hideMenuViewController];
            
            break;
        case 2:
            [[NavigationHandler getInstance] MoveToChat];
            [self.sideMenuViewController hideMenuViewController];
            
            

            break;
        case 3:
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"UploadFromGallery"
             object:self];
            [self.sideMenuViewController hideMenuViewController];
           
            break;
        case 4:
            [[NavigationHandler getInstance] MoveToPopularUsers];
            [self.sideMenuViewController hideMenuViewController];
          
            
            break;
        case 5:
            [[NavigationHandler getInstance] MoveToNotifications];
            [self.sideMenuViewController hideMenuViewController];
      
            break;
        case 6:
            [[NavigationHandler getInstance] moveToSettingsVC];
            [self.sideMenuViewController hideMenuViewController];
//            appDelegate.myCornerDataSuccess = NO;
//            [Utils removeProfileImg];
//            [Utils clearUserDefaults];
//            shareMngr=[DataContainer sharedManager];
//            [shareMngr dellocDate];
//            [[NavigationHandler getInstance]LogoutUser];
//            [self.sideMenuViewController hideMenuViewController];
//            [[NavigationHandler getInstance] MoveToTopics];
//            [self.sideMenuViewController hideMenuViewController];
            break;
        case 7:
//            [[NavigationHandler getInstance] MoveToTopics];
//            [self.sideMenuViewController hideMenuViewController];
            [[NavigationHandler getInstance] MoveToHomeScreen];
            [self.sideMenuViewController hideMenuViewController];
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"goLiveNow"
             object:nil];
            
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD) {
        return 74;
    }
    else if (IS_IPHONE_5){
        return 40;
        
    }
    else
        return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menuCell";
    MenuCell * cell = (MenuCell *)[tableView dequeueReusableCellWithIdentifier:@"menuCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MenuCell" owner:self options:nil];
        if(IS_IPAD) {
            cell=[nib objectAtIndex:1];
        }
        else{
            cell=[nib objectAtIndex:0];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    
    //NSArray *titles = @[@"My Corner", @"My Archive",@"Inbox",@"Beam from Gallery",@"People You May Know",@"Notifications",@"Settings"];
//    NSArray *titles = @[NSLocalizedString(@"my_corner", @""), NSLocalizedString(@"my_archive", @""),NSLocalizedString(@"text_chat", @""),NSLocalizedString(@"beam_from_gallery", @""),NSLocalizedString(@"people_you_may_know", @""),NSLocalizedString(@"notifications", @""),NSLocalizedString(@"settings", @""),@"Go Live"];
      NSArray *titles = @[NSLocalizedString(@"my_corner", @""), NSLocalizedString(@"my_archive", @""),NSLocalizedString(@"text_chat", @""),NSLocalizedString(@"beam_from_gallery", @""),NSLocalizedString(@"people_you_may_know", @""),NSLocalizedString(@"notifications", @""),NSLocalizedString(@"settings", @"")];

    cell.lblItem.text = titles[indexPath.row];
    //cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
    if (indexPath.row == 5) {
        appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        if(appDelegate.unseenCount > 0 && self.shouldShowNotification)
        {
            cell.notiCountlbl.text      = [NSString stringWithFormat:@"%@",[Utils abbreviateNumber:[NSString stringWithFormat:@"%ld",(long)appDelegate.unseenCount] withDecimal:1]];
            cell.notiCountlbl.hidden    = NO;
            cell.notiCountImg.hidden  = NO;
        }
        else
        {
            cell.notiCountlbl.hidden    = YES;
            cell.notiCountImg.hidden  = YES;
        }
        
    }
    return cell;
}
#pragma mark ZoomImage
-(IBAction)zoomImage:(id)sender{
    [EXPhotoViewer showImageFrom:self.userImg];
}

@end
