//
//  MenuCell.h
//  HydePark
//
//  Created by Samreen Noor on 26/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblItem;
@property (weak, nonatomic) IBOutlet UIImageView *imgItem;
@property (weak, nonatomic) IBOutlet UILabel *notiCountlbl;
@property (weak, nonatomic) IBOutlet UIImageView *notiCountImg;

@end
